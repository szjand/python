import db_cnx
file_name = 'files/walk_notes.csv'
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy greg.walk_notes from stdin with csv header encoding 'latin-1 '""", io)