import psycopg2
import csv
import datetime
import uuid
import db_cnx
import string

# update the ads tables
# scrape the appropriate ads tables into files
# load the postgres tables
# thinking this should all be based on dependency, but not there yet
# need a shorter path to just getting the fucker done so greg has his "data" daily
# rudimentary log_cur
# added a log connection, separate
# do individual try...except for each query, but wrap the whole thing in a single try...except
# to surface the exact error

global pg_con, pg_cur, log_con, log_cur, ads_con, ads_cur, log_id, the_date
global from_time, thru_time, lvl1, lvl2, lvl3, err, io
try:
    pg_con = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
    pg_cur = pg_con.cursor()

    log_con = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
    log_cur = log_con.cursor()
    # sco connection
    # ads_con = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\advantage\\scotest\\sco.add',
    #                         userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
    #                         TrimTrailingSpaces='TRUE')
    ads_con = db_cnx.ads_sco()
    ads_cur = ads_con.cursor()
    ads_cur_2 = ads_con.cursor()
    # schema = 'greg'
    log_id = uuid.uuid4().urn[9:]
    the_date = datetime.date.today().strftime("%m/%d/%Y")
    # ads updates
    # try:
    #     from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     lvl1 = 'ads'
    #     lvl2 = 'ucinv_tmp_gross_1'
    #     log_sql = """
    #         insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
    #         values('%s','%s', '%s', '%s', '%s')
    #     """ % (log_id, the_date, lvl1, lvl2, from_time)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    #     ads_sql = """
    #         delete from """ + lvl2 + """
    #             where the_date between curdate() - 45 and curdate();
    #         """
    #     ads_cur.execute(ads_sql)
    #     ads_sql = """
    #         INSERT INTO """ + lvl2 + """
    #         SELECT a.gtdate, a.gtctl#, a.gtacct, left(al2,12), left(dl4,12),
    #           -1 * cast(round(coalesce(gttamt, 0), 0) as sql_integer) as gttamt
    #         FROM dds.stgarkonaglptrns a
    #         INNER JOIN dds.zcb d on a.gtacct = d.glaccount
    #           AND d.name = 'gmfs gross'
    #           AND d.dl5 = 'used'
    #         WHERE gtdate between curdate() - 45 and curdate();
    #     """
    #     ads_cur.execute(ads_sql)
    #     ads_con.commit()
    #     thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     log_sql = """
    #         update greg.ucinv_log
    #         set thru_time = '%s',
    #             pass_fail = 'Pass'
    #         where log_id = '%s'
    #           and the_date = '%s'
    #           and lvl1 = '%s'
    #           and lvl2 = '%s';
    #     """ % (thru_time, log_id, the_date, lvl1, lvl2)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    # except Exception, error:
    #     err = error
    #     raise
    # try:
    #     from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     lvl1 = 'ads'
    #     lvl2 = 'ucinv_tmp_gross_2'
    #     log_sql = """
    #         insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
    #         values('%s','%s', '%s', '%s', '%s')
    #     """ % (log_id, the_date, lvl1, lvl2, from_time)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    #     ads_sql = """delete from """ + lvl2
    #     ads_cur.execute(ads_sql)
    #     ads_sql = """
    #         INSERT INTO """ + lvl2 + """
    #         SELECT stocknumber,
    #           SUM(CASE WHEN department = 'sales' and account_type = 'sales' THEN amount ELSE 0 END) AS sale_amount,
    #           SUM(CASE WHEN department = 'sales' and account_type = 'COGS' THEN amount ELSE 0 END) AS cost_of_sale,
    #           SUM(CASE WHEN department = 'sales' THEN amount ELSE 0 END) AS sales_gross,
    #           SUM(CASE WHEN department = 'f&i' and account_type = 'sales' then amount else 0 end) as fi_sale_amount,
    #           SUM(CASE WHEN department = 'f&i' and account_type = 'COGS' then amount else 0 end) as fi_cost,
    #           SUM(CASE WHEN department = 'f&i' THEN amount ELSE 0 END) AS fi_gross
    #         -- SELECT *
    #         FROM ucinv_tmp_gross_1
    #         GROUP BY stocknumber;
    #     """
    #     ads_cur.execute(ads_sql)
    #     ads_con.commit()
    #     thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     log_sql = """
    #         update greg.ucinv_log
    #         set thru_time = '%s',
    #             pass_fail = 'Pass'
    #         where log_id = '%s'
    #           and the_date = '%s'
    #           and lvl1 = '%s'
    #           and lvl2 = '%s';
    #     """ % (thru_time, log_id, the_date, lvl1, lvl2)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    # except Exception, error:
    #     err = error
    #     raise
    # try:
    #     from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     lvl1 = 'ads'
    #     lvl2 = 'ucinv_tmp_avail_1'
    #     log_sql = """
    #         insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
    #         values('%s','%s', '%s', '%s', '%s')
    #     """ % (log_id, the_date, lvl1, lvl2, from_time)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    #     ads_sql = """delete from """ + lvl2
    #     ads_cur.execute(ads_sql)
    #     ads_sql = """
    #         INSERT INTO """ + lvl2 + """
    #         SELECT a.stocknumber,
    #           CASE a.owninglocationid
    #             WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
    #             WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
    #             WHEN '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'RY3'
    #             ELSE 'XXX'
    #           END AS storecode,
    #           left(f.shapeandsize, 24) AS shape_and_size,
    #           CAST(a.fromts AS sql_date) AS from_date,
    #           coalesce(CAST(g.soldts AS sql_date), CAST('12/31/9999' AS sql_date)) AS sale_date,
    #           coalesce(substring(g.typ, position('_' IN g.typ) + 1, 9), 'Inventory') AS sale_type,
    #           a.VehicleInventoryItemID
    #         FROM dps.VehicleInventoryItems a
    #         INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID
    #         INNER JOIN dps.MakeModelClassifications c on b.make = c.make
    #           AND b.model = c.model
    #         INNER JOIN dps.typDescriptions d on c.vehicleSegment = d.typ
    #         INNER JOIN dps.typDescriptions e on c.vehicleType = e.typ
    #         INNER JOIN ucinv_shapes_and_sizes f on upper(e.description) = upper(f.shape) collate ads_default_ci
    #           AND upper(d.description) = upper(f.size) collate ads_default_ci
    #         LEFT JOIN dps.vehiclesales g on a.VehicleInventoryItemID = g.VehicleInventoryItemID
    #           AND g.status = 'VehicleSale_Sold'
    #         -- limit to those vehicles sold after 1/1/14
    #         WHERE coalesce(CAST(a.thruts AS sql_date), CAST('12/31/9999' AS sql_date)) > '01/01/2014'
    #           AND a.stocknumber IS NOT NULL;-- 10/12/15 unbooked intramarket xfts
    #     """
    #     ads_cur.execute(ads_sql)
    #     ads_con.commit()
    #     thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     log_sql = """
    #         update greg.ucinv_log
    #         set thru_time = '%s',
    #             pass_fail = 'Pass'
    #         where log_id = '%s'
    #           and the_date = '%s'
    #           and lvl1 = '%s'
    #           and lvl2 = '%s';
    #     """ % (thru_time, log_id, the_date, lvl1, lvl2)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    # except Exception, error:
    #     err = error
    #     raise
    # try:
        # from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        # lvl1 = 'ads'
        # lvl2 = 'ucinv_tmp_avail_2'
        # log_sql = """
        #     insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
        #     values('%s','%s', '%s', '%s', '%s')
        # """ % (log_id, the_date, lvl1, lvl2, from_time)
        # log_cur.execute(log_sql)
        # log_con.commit()
        # ads_sql = """delete from """ + lvl2
        # ads_cur.execute(ads_sql)
        # ads_sql = """
        #     INSERT INTO """ + lvl2 + """
        #     # SELECT a.thedate, b.*, -- SELECT COUNT(*)
        #     #   coalesce(CAST(c.pricingts AS sql_date), CAST('12/31/9999' AS sql_date)) AS date_priced,
        #     #   coalesce(bestprice, -1) AS best_price, coalesce(invoice, -1) AS invoice,
        #     #   coalesce(dayssincepriced, 0) AS days_since_priced,
        #     #   coalesce(c.priceband, 'Not Priced') AS price_band,
        #     #   (a.thedate - b.from_date) + 1 AS days_owned
        #     # FROM dds.day a
        #     # LEFT JOIN ucinv_tmp_avail_1 b on a.thedate BETWEEN b.from_date AND b.sale_date
        #     # LEFT JOIN ucinv_price_by_day c on a.thedate = c.thedate
        #     #   AND b.stocknumber = c.stocknumber
        #     # WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1;
        # """
    #     ads_sql = """
    #         INSERT INTO """ + lvl2 + """
    #         SELECT b.thedate, a.*,
    #           coalesce(CAST(c.pricingts AS sql_date), CAST('12/31/9999' AS sql_date)) AS date_priced,
    #           coalesce(bestprice, -1) AS best_price, coalesce(invoice, -1) AS invoice,
    #           coalesce(dayssincepriced, 0) AS days_since_priced,
    #           coalesce(c.priceband, 'Not Priced') AS price_band,
    #           (b.thedate - a.from_date) + 1 AS days_owned
    #         FROM ucinv_tmp_avail_1 a
    #         LEFT JOIN dds.day b on b.thedate BETWEEN a.from_date AND CASE WHEN sale_date < curdate() THEN sale_date ELSE curdate() -1 END
    #         LEFT JOIN ucinv_price_by_day c on b.thedate = c.thedate
    #           AND a.stocknumber = c.stocknumber
    #         WHERE a.sale_date > curdate() - 7;
    #     """
    #     ads_cur.execute(ads_sql)
    #     ads_con.commit()
    #     thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     log_sql = """
    #         update greg.ucinv_log
    #         set thru_time = '%s',
    #             pass_fail = 'Pass'
    #         where log_id = '%s'
    #           and the_date = '%s'
    #           and lvl1 = '%s'
    #           and lvl2 = '%s';
    #     """ % (thru_time, log_id, the_date, lvl1, lvl2)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    # except Exception, error:
    #     err = error
    #     raise
    # try:
    #     from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     lvl1 = 'ads'
    #     lvl2 = 'ucinv_tmp_statuses'
    #     log_sql = """
    #         insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
    #         values('%s','%s', '%s', '%s', '%s')
    #     """ % (log_id, the_date, lvl1, lvl2, from_time)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    #     ads_sql = """delete from """ + lvl2
    #     ads_cur.execute(ads_sql)
    #     ads_sql = """
    #         INSERT INTO """ + lvl2 + """
    #         SELECT VehicleInventoryItemID, status, from_date, coalesce(thru_date, CAST('12/31/9999' AS sql_date))
    #         FROM (
    #           SELECT a.VehicleInventoryItemID, 'available' AS status, CAST(a.fromts AS sql_date) AS from_date,
    #             CAST(b.thruts AS sql_date) AS thru_date
    #           FROM (
    #             SELECT VehicleInventoryItemID, MAX(fromts) AS fromts
    #             FROM dps.VehicleInventoryItemStatuses a
    #             WHERE status = 'RMFlagAV_Available'
    #             GROUP BY VehicleInventoryItemID) a
    #             INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    #               AND a.fromts = b.fromts
    #               AND b.status = 'RMFlagAV_Available'
    #         UNION
    #         select a.VehicleInventoryItemID, 'pulled', CAST(a.fromts AS sql_date) AS from_date,
    #           CAST(b.thruts AS sql_date) AS thru_date
    #         FROM (
    #           SELECT VehicleInventoryItemID, MAX(fromts) AS fromts
    #           FROM dps.VehicleInventoryItemStatuses a
    #           WHERE status = 'RMFlagPulled_Pulled'
    #           GROUP BY VehicleInventoryItemID) a
    #           INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    #             AND a.fromts = b.fromts
    #             AND b.status = 'RMFlagPulled_Pulled') x;
    #     """
    #     ads_cur.execute(ads_sql)
    #     ads_con.commit()
    #     thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     log_sql = """
    #         update greg.ucinv_log
    #         set thru_time = '%s',
    #             pass_fail = 'Pass'
    #         where log_id = '%s'
    #           and the_date = '%s'
    #           and lvl1 = '%s'
    #           and lvl2 = '%s';
    #     """ % (thru_time, log_id, the_date, lvl1, lvl2)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    # except Exception, error:
    #     err = error
    #     raise
    # try:
    #     from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     lvl1 = 'ads'
    #     lvl2 = 'ucinv_tmp_avail_3'
    #     log_sql = """
    #         insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
    #         values('%s','%s', '%s', '%s', '%s')
    #     """ % (log_id, the_date, lvl1, lvl2, from_time)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    #     ads_sql = """delete from """ + lvl2
    #     ads_cur.execute(ads_sql)
    #     ads_sql = """
    #         INSERT INTO """ + lvl2 + """
    #         SELECT a.stocknumber, a.VehicleInventoryItemID, a.from_date, a.sale_date, a.sale_type,
    #           a.from_date AS raw_from_date,
    #           CASE sale_date
    #             WHEN '12/31/9999' THEN cast('12/31/9999' AS sql_date)
    #             ELSE sale_date
    #           END AS raw_thru_date,
    #           cast('12/31/9999' AS sql_date) AS pull_from_date,
    #           cast('12/31/9999' AS sql_date) AS pull_thru_date,
    #           cast('12/31/9999' AS sql_date) AS avail_from_date,
    #           cast('12/31/9999' AS sql_date) AS avail_thru_date
    #         -- SELECT COUNT(*)
    #         FROM ucinv_tmp_avail_1 a
    #         LEFT JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    #         WHERE b.VehicleInventoryItemID IS NULL
    #         UNION
    #         -- CASE 2
    #         SELECT a.stocknumber, a.VehicleInventoryItemID, a.from_date, a.sale_date, a.sale_type,
    #           a.from_date AS raw_from_date,
    #           CASE
    #             WHEN b.status_thru_date < a.sale_date THEN a.sale_date -- cancelled pulls
    #             ELSE b.status_from_date - 1
    #           END AS raw_thru_date,
    #           CASE
    #             WHEN b.status_thru_date < a.sale_date THEN cast('12/31/9999' AS sql_date) -- cancelled pulls
    #             ELSE b.status_from_date
    #           END AS pull_from_date,
    #           CASE
    #             WHEN b.status_thru_date < a.sale_date THEN cast('12/31/9999' AS sql_date) -- cancelled pul
    #             ELSE a.sale_date
    #           END AS pull_thru_date,
    #           cast('12/31/9999' AS sql_date) AS avail_from_date,
    #           cast('12/31/9999' AS sql_date) AS avail_thru_date
    #         -- SELECT *
    #         -- SELECT COUNT(DISTINCT a.stocknumber)
    #         FROM ucinv_tmp_avail_1 a
    #         INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    #         WHERE NOT EXISTS (
    #           SELECT 1
    #           FROM ucinv_tmp_statuses
    #           WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
    #             AND status = 'available')
    #         -- CASE 3 anomaly 1: mult pulls, last pull did NOT become avail, sold FROM pull
    #         UNION
    #         SELECT stocknumber, VehicleInventoryItemID, from_date, sale_date, sale_type,
    #           from_date as raw_from_date,
    #           pull_from_date -1 as raw_thru_date,
    #           pull_from_date,
    #           sale_Date as pull_thru_date,
    #           cast('12/31/9999' AS sql_date) AS avail_from_date,
    #           cast('12/31/9999' AS sql_date) AS avail_thru_date
    #         FROM (
    #           -- SELECT * FROM (
    #           SELECT a.stocknumber, a.from_date, a.sale_date, a.sale_type, a.VehicleInventoryItemID,
    #             b.status_from_date AS pull_from_date,
    #             b.status_thru_date AS pull_thru_date,
    #             c.status_from_date AS avail_from_date,
    #             c.status_thru_date AS avail_thru_date
    #           -- SELECT COUNT(*)
    #           FROM ucinv_tmp_avail_1 a
    #           INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    #             AND b.status = 'pulled'
    #           INNER JOIN ucinv_tmp_statuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID
    #             AND c.status = 'available') x WHERE pull_thru_date <> avail_from_date
    #         UNION
    #         -- CASE 3 everything except anomaly 1
    #         SELECT stocknumber, VehicleInventoryItemID, from_date, sale_date, sale_type,
    #           CASE -- anomaly 4
    #             WHEN from_date = pull_from_date THEN cast('12/31/9999' AS sql_date)
    #             ELSE from_date
    #           END AS raw_from_date,
    #           CASE -- anomaly 4
    #             WHEN from_date = pull_from_date THEN cast('12/31/9999' AS sql_date)
    #             ELSE pull_from_date -1
    #           END AS raw_thru_date,
    #           CASE -- anomaly 3
    #             WHEN pull_from_date = avail_from_date THEN cast('12/31/9999' AS sql_date)
    #             ELSE pull_from_date
    #           END AS pull_from_date,
    #           CASE -- anomaly 3
    #             WHEN pull_from_date = avail_from_date THEN cast('12/31/9999' AS sql_date)
    #             ELSE pull_thru_date -1
    #           END AS pull_thru_date,
    #           avail_from_date,
    #           sale_date AS avail_thru_date
    #         FROM (
    #           -- SELECT COUNT(*) FROM (
    #           SELECT a.stocknumber, a.sale_type, a.VehicleInventoryItemID,
    #             a.from_date,
    #             b.status_from_date AS pull_from_date,
    #             b.status_thru_date AS pull_thru_date,
    #             c.status_from_date AS avail_from_date,
    #             c.status_thru_date AS avail_thru_date,
    #             a.sale_date
    #           -- SELECT COUNT(*)
    #           FROM ucinv_tmp_avail_1 a
    #           INNER JOIN ucinv_tmp_statuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    #             AND b.status = 'pulled'
    #           INNER JOIN ucinv_tmp_statuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID
    #             AND c.status = 'available'
    #           ) x WHERE pull_thru_Date = avail_from_date; -- elim anomaly 1
    #     """
    #     ads_cur.execute(ads_sql)
    #     ads_con.commit()
    #     thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     log_sql = """
    #         update greg.ucinv_log
    #         set thru_time = '%s',
    #             pass_fail = 'Pass'
    #         where log_id = '%s'
    #           and the_date = '%s'
    #           and lvl1 = '%s'
    #           and lvl2 = '%s';
    #     """ % (thru_time, log_id, the_date, lvl1, lvl2)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    # except Exception, error:
    #     err = error
    #     raise
    # try:
    #     from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     lvl1 = 'ads'
    #     lvl2 = 'ucinv_tmp_avail_4'
    #     log_sql = """
    #         insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
    #         values('%s','%s', '%s', '%s', '%s')
    #     """ % (log_id, the_date, lvl1, lvl2, from_time)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    #     ads_sql = """delete from """ + lvl2
    #     ads_cur.execute(ads_sql)
    #     ads_sql = """
    #         INSERT INTO """ + lvl2 + """
    #         SELECT a.*,
    #           CASE
    #             WHEN a.thedate >= avail_from_date THEN (a.thedate - avail_from_date) + 1
    #             ELSE 0
    #           END AS days_avail,
    #           CASE
    #             WHEN thedate between raw_from_date AND raw_thru_date THEN 'raw'
    #             WHEN thedate BETWEEN pull_from_date AND pull_thru_date THEN 'pull'
    #             WHEN thedate BETWEEN avail_from_date AND avail_thru_date THEN
    #               CASE
    #                 WHEN thedate - avail_from_date < 31 THEN 'avail_fresh'
    #                 ELSE 'avail_aged'
    #               END
    #           END AS status,
    #           b.sold_from_status,
    #           b.disposition,
    #           coalesce(c.sale_amount, a.best_price)
    #         FROM ucinv_tmp_avail_2 a
    #         LEFT JOIN (
    #           SELECT a.*,
    #             CASE -- sold FROM status
    #               WHEN sale_type = 'inventory' THEN 'inventory'
    #               WHEN avail_thru_date = sale_date THEN
    #                 CASE
    #                   WHEN avail_thru_date - avail_from_date < 31 then 'avail_fresh'
    #                   ELSE 'avail_aged'
    #                 END
    #               WHEN pull_thru_date = sale_date THEN 'pulled'
    #               WHEN raw_thru_date = sale_date THEN 'raw'
    #             END AS sold_from_status,
    #             CASE -- disposition of avail vehicles
    #               WHEN sale_type = 'inventory' THEN 'NA'
    #               WHEN avail_from_date > curdate() THEN 'NA'
    #               WHEN sale_type = 'Retail' THEN
    #                 CASE
    #                   WHEN sale_date - avail_from_date < 31 THEN 'Sold 0-30'
    #                   WHEN sale_date - avail_from_date BETWEEN 31 AND 60 THEN 'Sold 31-60'
    #                   WHEN sale_date - avail_from_date > 60 THEN 'Sold > 60'
    #                 END
    #               WHEN sale_Type = 'Wholesale' THEN 'W/S from available'
    #             END AS Disposition
    #           FROM ucinv_tmp_avail_3 a) b on a.stocknumber = b.stocknumber
    #         LEFT JOIN ucinv_tmp_gross_2 c on a.stocknumber = c.stocknumber;
    #     """
    #     ads_cur.execute(ads_sql)
    #     ads_con.commit()
    #     thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    #     log_sql = """
    #         update greg.ucinv_log
    #         set thru_time = '%s',
    #             pass_fail = 'Pass'
    #         where log_id = '%s'
    #           and the_date = '%s'
    #           and lvl1 = '%s'
    #           and lvl2 = '%s';
    #     """ % (thru_time, log_id, the_date, lvl1, lvl2)
    #     log_cur.execute(log_sql)
    #     log_con.commit()
    # except Exception, error:
    #     err = error
    #     raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_avail_5'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            select d.*, coalesce(e.avail_aged, 0) AS avail_aged, coalesce(e.avail_fresh, 0) AS avail_fresh,
              coalesce(e.avail_total, 0) AS avail_total, coalesce(e.pull, 0) AS pull,
              coalesce(e.raw, 0) AS raw
              FROM (
              SELECT a.thedate, 'RY1' as storecode, left(b.shapeandsize, 24) AS shape_and_size,
                left(c.priceband, 10) AS price_band
              FROM dds.day a, ucinv_shapes_and_sizes b, ucinv_price_bands c
              WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1
              UNION ALL
              SELECT a.thedate, 'RY2' as storecode, left(b.shapeandsize, 24) AS shape_and_size,
                left(c.priceband, 10) AS price_band
              FROM dds.day a, ucinv_shapes_and_sizes b, ucinv_price_bands c
              WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1) d
            LEFT JOIN (
              SELECT thedate, storecode, shape_and_size, price_band,
                SUM(CASE WHEN status = 'avail_aged' THEN 1 ELSE 0 END) AS avail_aged,
                SUM(CASE WHEN status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_fresh,
                SUM(CASE WHEN status = 'avail_aged' or status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_total,
                SUM(CASE WHEN status = 'pull' THEN 1 ELSE 0 END) AS pull,
                SUM(CASE WHEN status = 'raw' THEN 1 ELSE 0 END) AS raw
              FROM ucinv_tmp_avail_4
              GROUP BY thedate, storecode, shape_and_size, price_band) e on d.thedate = e.thedate
                AND d.storecode = e.storecode collate ads_default_ci
                AND d.shape_and_size = e.shape_and_size
                AND d.price_band = e.price_band;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_avail_6'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            SELECT a.*,
              coalesce(c.prev_30_retail, 0) AS prev_30_retail,
              coalesce(c.prev_30_ws, 0) AS prev_30_ws,
              coalesce(d.prev_365_retail, 0) AS prev_365_retail,
              coalesce(e.prev_365_ws, 0) AS prev_365_ws,
              0,0,0,0,0,0,0,0,
              0,0,0,
              0,0,0,0,0
            FROM ucinv_tmp_avail_5  a
            LEFT JOIN (
              SELECT a.thedate, a.storecode, b.shape_and_size, b.price_band,
                SUM(CASE WHEN sale_type = 'retail' THEN 1 ELSE 0 END) AS prev_30_retail,
                SUM(CASE WHEN sale_type = 'wholesale' THEN 1 ELSE 0 END) AS prev_30_ws
              FROM (
                SELECT thedate, 'RY1' AS storecode
                FROM dds.day
                WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1
                union
                SELECT thedate, 'RY2' AS storecode
                FROM dds.day
                WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1) a
              LEFT JOIN (
                SELECT *
                FROM ucinv_tmp_avail_2
                WHERE thedate = sale_date) b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
                  AND a.storecode = b.storecode collate ads_default_ci
              GROUP BY a.thedate, a.storecode, b.shape_and_size, b.price_band) c on a.thedate = c.thedate
                  AND a.storecode = c.storecode collate ads_default_ci
                  AND a.shape_and_size = c.shape_and_size
                  AND a.price_band = c.price_band
            LEFT JOIN (
              SELECT thedate, storecode,
                TRIM(vehicleshape) + ' - ' + TRIM(vehiclesize) AS shape_and_size, priceband,
                round(1.0 * units/12, 1) AS prev_365_retail
              FROM ucinv_sales_previous_365
              WHERE saletype = 'retail') d on a.thedate = d.thedate
                  AND a.storecode = d.storecode
                  AND a.shape_and_size = d.shape_and_size
                  AND a.price_band = d.priceband
            LEFT JOIN (
              SELECT thedate, storecode,
                TRIM(vehicleshape) + ' - ' + TRIM(vehiclesize) AS shape_and_size, priceband,
                round(1.0 * units/12, 1) AS prev_365_ws
              FROM ucinv_sales_previous_365
              WHERE saletype = 'wholesale') e on a.thedate = e.thedate
                  AND a.storecode = e.storecode
                  AND a.shape_and_size = e.shape_and_size
                  AND a.price_band = e.priceband;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_wtf'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            SELECT a.thedate, b.storecode, b.shape_and_size, b.price_band,
              round(SUM(avail_fresh)/30.0, 1) AS avail_fresh_prev_30_avg,
              round(SUM(avail_aged)/30.0, 1)AS avail_aged_prev_30_avg,
              round(SUM(avail_total)/30.0, 1) AS avail_total_prev_30_avg
            --INTO #wtf
            FROM dds.day a
            LEFT JOIN ucinv_tmp_avail_5 b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
            WHERE a.thedate BETWEEN '01/31/2014' AND curdate() - 1
            GROUP BY a.thedate, b.storecode, b.shape_and_size, b.price_band;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_avail_6'
        lvl3 = 'update 1'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,lvl3,from_time)
            values('%s','%s', '%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, lvl3, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """
            UPDATE """ + lvl2 + """
            SET avail_fresh_prev_30_avg = x.avail_fresh_prev_30_avg,
                avail_aged_prev_30_avg = x.avail_aged_prev_30_avg,
                avail_total_prev_30_avg = x.avail_total_prev_30_avg
            FROM (
              SELECT b.*
              FROM ucinv_tmp_avail_6 a
              inner JOIN ucinv_tmp_wtf b on a.thedate = b.thedate
                AND a.storecode = b.storecode
                AND a.shape_and_size = b.shape_and_size
                AND a.price_band = b.price_band) x
            WHERE ucinv_tmp_avail_6.thedate = x.thedate
              AND ucinv_tmp_avail_6.storecode = x.storecode
              AND ucinv_tmp_avail_6.shape_and_size = x.shape_and_size
              AND ucinv_tmp_avail_6.price_band = x.price_band;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s'
              and lvl3 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2, lvl3)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_avail_6'
        lvl3 = 'update 2'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,lvl3,from_time)
            values('%s','%s', '%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, lvl3, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """
            UPDATE """ + lvl2 + """
            set sales_prev_30_retail_avail_fresh = x.sales_prev_30_retail_avail_fresh,
                sales_prev_30_ws_avail_fresh = x.sales_prev_30_ws_avail_fresh,
                sales_prev_30_retail_avail_aged = x.sales_prev_30_retail_avail_aged,
                sales_prev_30_ws_avail_aged = x.sales_prev_30_ws_avail_aged,
                sales_prev_30_retail_raw = x.sales_prev_30_retail_raw,
                sales_prev_30_ws_raw = x.sales_prev_30_ws_raw,
                sales_prev_30_retail_pulled = x.sales_prev_30_retail_pulled,
                sales_prev_30_ws_pulled = x.sales_prev_30_ws_pulled
            FROM (
              SELECT thedate, storecode, shape_and_size, price_band,
                sum(CASE WHEN sale_type = 'retail' and sold_from_status = 'avail_fresh'
                  THEN units ELSE 0 END) AS sales_prev_30_retail_avail_fresh,
                sum(CASE WHEN sale_type = 'wholesale' and sold_from_status = 'avail_fresh'
                  THEN units ELSE 0 END) AS sales_prev_30_ws_avail_fresh,
                sum(CASE WHEN sale_type = 'retail' and sold_from_status = 'avail_aged'
                  THEN units ELSE 0 END) AS sales_prev_30_retail_avail_aged,
                sum(CASE WHEN sale_type = 'wholesale' and sold_from_status = 'avail_aged'
                  THEN units ELSE 0 END) AS sales_prev_30_ws_avail_aged,
                sum(CASE WHEN sale_type = 'retail' and sold_from_status = 'raw'
                  THEN units ELSE 0 END) AS sales_prev_30_retail_raw,
                sum(CASE WHEN sale_type = 'wholesale' and sold_from_status = 'raw'
                  THEN units ELSE 0 END) AS sales_prev_30_ws_raw,
                sum(CASE WHEN sale_type = 'retail' and sold_from_status = 'pulled'
                  THEN units ELSE 0 END) AS sales_prev_30_retail_pulled,
                sum(CASE WHEN sale_type = 'wholesale' and sold_from_status = 'pulled'
                  THEN units ELSE 0 END) AS sales_prev_30_ws_pulled
              FROM (
                SELECT a.thedate, storecode, b.sale_type, shape_and_size, price_band,
                  sold_from_status, COUNT(*) AS units
                FROM dds.day a
                LEFT JOIN (
                  SELECT *
                  FROM ucinv_tmp_avail_4
                  WHERE sale_Date = thedate) b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
                WHERE a.thedate BETWEEN '01/31/2014' AND curdate() - 1
                group by a.thedate, storecode, b.sale_type, shape_and_size, price_band, sold_from_status) m
              GROUP BY thedate, storecode, shape_and_size, price_band) x
            WHERE ucinv_tmp_avail_6.thedate = x.thedate
              AND ucinv_tmp_avail_6.storecode = x.storecode
              AND ucinv_tmp_avail_6.shape_and_size = x.shape_and_size
              AND ucinv_tmp_avail_6.price_band = x.price_band;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s'
              and lvl3 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2, lvl3)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_wtf_2'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            SELECT a.thedate, b.storecode, b.shape_And_size, b.price_band,
              round(SUM(pull + raw)/30.0, 1) AS raw_pull_prev_30_avg,
              round(SUM(pull + raw + avail_total)/30.0, 1) AS owned_prev_30_avg
            -- INTO #wtf2
            FROM dds.day a
            LEFT JOIN ucinv_tmp_avail_5 b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
            WHERE a.thedate BETWEEN '01/31/2014' AND curdate() - 1
            GROUP BY a.thedate, b.storecode, b.shape_and_size, b.price_band;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_avail_6'
        lvl3 = 'update 3'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,lvl3,from_time)
            values('%s','%s', '%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, lvl3, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """
            UPDATE """ + lvl2 + """
            SET raw_pull_prev_30_avg = x.raw_pull_prev_30_avg,
                owned_prev_30_avg = x.owned_prev_30_avg
            FROM (
              SELECT b.*
              FROM ucinv_tmp_avail_6 a
              INNER JOIN ucinv_tmp_wtf_2 b on a.thedate = b.thedate
                AND a.storecode = b.storecode
                AND a.shape_and_size = b.shape_and_size
                AND a.price_band = b.price_band) x
            WHERE ucinv_tmp_avail_6.thedate = x.thedate
              AND ucinv_tmp_avail_6.storecode = x.storecode
              AND ucinv_tmp_avail_6.shape_and_size = x.shape_and_size
              AND ucinv_tmp_avail_6.price_band = x.price_band;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s'
              and lvl3 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2, lvl3)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_avail_6'
        lvl3 = 'update 4'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,lvl3,from_time)
            values('%s','%s', '%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, lvl3, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """
            UPDATE """ + lvl2 + """
            SET raw_pull_prev_30_avg = x.raw_pull_prev_30_avg,
                owned_prev_30_avg = x.owned_prev_30_avg
            FROM (
              SELECT b.*
              FROM ucinv_tmp_avail_6 a
              INNER JOIN ucinv_tmp_wtf_2 b on a.thedate = b.thedate
                AND a.storecode = b.storecode
                AND a.shape_and_size = b.shape_and_size
                AND a.price_band = b.price_band) x
            WHERE ucinv_tmp_avail_6.thedate = x.thedate
              AND ucinv_tmp_avail_6.storecode = x.storecode
              AND ucinv_tmp_avail_6.shape_and_size = x.shape_and_size
              AND ucinv_tmp_avail_6.price_band = x.price_band;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s'
              and lvl3 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2, lvl3)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_avail_6'
        lvl3 = 'update 5'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,lvl3,from_time)
            values('%s','%s', '%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, lvl3, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """
            UPDATE """ + lvl2 + """
            SET turns_avail_fresh =
                  CASE avail_fresh_prev_30_avg
                    WHEN 0 THEN 0
                    ELSE round(sales_prev_30_retail_avail_fresh/avail_fresh_prev_30_avg, 2)
                  END ,
                turns_raw_pull =
                  CASE raw_pull_prev_30_avg
                    WHEN 0 THEN 0
                    ELSE round(sales_prev_30_retail_raw + sales_prev_30_retail_pulled/raw_pull_prev_30_avg, 2)
                  END,
                turns_owned =
              CASE owned_prev_30_avg
                WHEN 0 THEN 0
                ELSE round(sales_prev_30_retail/owned_prev_30_avg, 2)
              END;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s'
              and lvl3 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2, lvl3)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_avail_disposition'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            SELECT a.*, b.sold_0_to_30, b.sold_31_to_60, b.sold_over_60, b.ws_from_avail
              FROM (
              SELECT thedate, storecode,
                SUM(CASE WHEN status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_fresh,
                SUM(CASE WHEN status = 'avail_aged' THEN 1 ELSE 0 END) AS avail_aged,
                COUNT(*) AS avail_total
              FROM ucinv_tmp_Avail_4
              WHERE status IN ('avail_fresh','avail_aged')
              GROUP BY thedate, storecode) a
            LEFT JOIN (
              SELECT thedate, storecode,
                SUM(CASE WHEN disposition = 'sold 0-30' THEN 1 ELSE 0 END) AS sold_0_to_30,
                SUM(CASE WHEN disposition = 'sold 31-60' THEN 1 ELSE 0 END) AS sold_31_to_60,
                SUM(CASE WHEN disposition = 'sold > 60' THEN 1 ELSE 0 END) AS sold_over_60,
                SUM(CASE WHEN disposition = 'W/S from available' THEN 1 ELSE 0 END) AS ws_from_avail
              FROM ucinv_tmp_Avail_4
              WHERE status IN ('avail_fresh','avail_aged')
              GROUP BY thedate, storecode) b on a.storecode = b.storecode
                AND a.thedate = b.thedate;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_sales_by_status'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            SELECT aa.*, coalesce(bb.sales_total, 0) AS sales_total,
              coalesce(bb.avail_fresh, 0) AS avail_fresh,
              coalesce(bb.avail_aged, 0) AS avail_aged,
              coalesce(bb.pulled, 0) AS pulled, coalesce(bb.raw, 0) AS raw,
              CASE
                WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
                ELSE round(1.0 * coalesce(bb.avail_fresh, 0)/coalesce(bb.sales_total, 0), 2)
              END AS avail_fresh_perc,
              CASE
                WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
                ELSE round(1.0 * coalesce(bb.avail_aged, 0)/coalesce(bb.sales_total, 0), 2)
              END AS avail_aged_perc,
              CASE
                WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
                ELSE round(1.0 * coalesce(bb.pulled, 0)/coalesce(bb.sales_total, 0), 2)
              END AS pulled_perc,
              CASE
                WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
                ELSE round(1.0 * coalesce(bb.raw, 0)/coalesce(bb.sales_total, 0), 2)
              END AS raw_perc
            FROM (
              SELECT thedate, 'RY1' AS storecode
              FROM dds.day
              WHERE thedate BETWEEN '01/01/2014' AND curdatE() - 1
              UNION
              SELECT thedate, 'RY2' AS storecode
              FROM dds.day
              WHERE thedate BETWEEN '01/01/2014' AND curdatE() - 1) aa
            LEFT JOIN (
              SELECT a.*, b.avail_fresh, b.avail_aged, b.pulled, b.raw
              FROM (
                SELECT thedate, storecode, COUNT(*) AS sales_total
                -- SELECT *
                FROM ucinv_tmp_avail_4
                WHERE thedate = sale_date
                  AND sale_type = 'retail'
                GROUP BY thedate, storecode) a
              LEFT JOIN (
                SELECT thedate, storecode,
                  SUM(CASE WHEN status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_fresh,
                  SUM(CASE WHEN status = 'avail_aged' THEN 1 ELSE 0 END) AS avail_aged,
                  SUM(CASE WHEN status = 'pull' THEN 1 ELSE 0 END) AS pulled,
                  SUM(CASE WHEN status = 'raw' THEN 1 ELSE 0 END) AS raw
                -- SELECT *
                FROM ucinv_tmp_avail_4
                WHERE thedate = sale_date
                  AND sale_type = 'retail'
                GROUP BY thedate, storecode) b on a.thedate = b.thedate AND a.storecode = b.storecode) bb
                  on aa.thedate = bb.thedate
                AND aa.storecode = bb.storecode collate ads_Default_ci
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        # Intranet SQL Scripts\crayon report\fixed_gross.sql
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_recon_ros_1'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            -- 5/15/16 gtctl#, gtdoc# now 17 (fucking arkona) ro is 9
            SELECT left(gtctl#, 9), left(gtdoc#, 9), sum(gttamt) AS gttamt
            FROM dds.stgArkonaGLPTRNS a
            INNER JOIN ucinv_tmp_gross_2 b on a.gtctl# = b.stocknumber
            WHERE
              CASE
                WHEN gtacct IN ( -- inventory accts
                  SELECT gl_account
                  FROM dds.gmfs_accounts
                  WHERE page = 1
                    AND line IN (25,26)
                    AND col = 2) THEN a.gtjrnl IN ('svi', 'pot')
                WHEN gtacct IN ( -- uc cogs accts
                  SELECT gl_account
                  FROM dds.gmfs_accounts
                  WHERE page = 16
                    AND department = 'uc'
                    AND account_type = '5') THEN a.gtjrnl IN ('svi','sca','pot')
              END
              AND EXISTS ( -- limit to those vehicles for which we have factRepairOrder records
                SELECT 1
                FROM dds.factRepairOrder
                WHERE ro = left(a.gtdoc#, 9))
            GROUP BY left(gtctl#, 9), left(gtdoc#, 9)
            HAVING SUM(gttamt) <> 0;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        # Intranet SQL Scripts\crayon report\fixed_gross.sql
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_recon_sales'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            SELECT a.stocknumber, a.ro, b.gtacct, c.description, c.department,
             -1 * cast(round(SUM(b.gttamt), 0) AS sql_integer) AS sales, c.cogs_account
            FROM ucinv_tmp_recon_ros_1  a
            LEFT JOIN dds.stgArkonaGLPTRNS b on a.ro = b.gtctl#
            LEFT JOIN dds.gmfs_Accounts c on b.gtacct = c.gl_account
              AND c.page = 16
            --  AND c.department IN ('bs','cw','ql','re','sd','pd')
              AND c.department IN ('bs','ql','re','sd','pd')
              AND c.account_type = '4' -- sales
            WHERE c.gl_account IS NOT NULL
            GROUP BY a.stocknumber, a.ro, b.gtacct, c.description, c.department, c.cogs_account;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        # Intranet SQL Scripts\crayon report\fixed_gross.sql
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_recon_cogs'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            SELECT a.stocknumber, a.ro, b.gtacct, c.description, c.department,
             -1 *  cast(round(SUM(b.gttamt), 0) AS sql_integer) AS cogs
            FROM ucinv_tmp_recon_ros_1   a
            LEFT JOIN dds.stgArkonaGLPTRNS b on a.ro = b.gtctl#
            LEFT JOIN dds.gmfs_Accounts c on b.gtacct = c.gl_account
              AND c.page = 16
            --  AND c.department IN ('bs','cw','ql','re','sd','pd')
              AND c.department IN ('bs','ql','re','sd','pd')
              AND c.account_type = '5' -- cogs
            WHERE c.gl_account IS NOT NULL
            GROUP BY a.stocknumber, a.ro, b.gtacct, c.description, c.department;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        # Intranet SQL Scripts\crayon report\fixed_gross.sql
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_fixed_gross_1'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            -- 7/26 leave out car wash
            SELECT a.stocknumber, -- , a.ro, b.department, b.amount AS sales, c.amount AS cogs
              SUM(CASE WHEN b.department = 'sd' THEN b.amount ELSE 0 END) AS sd_labor_sales,
              SUM(CASE WHEN b.department = 'sd' THEN c.amount ELSE 0 END) AS sd_labor_cogs,
              SUM(CASE WHEN b.department = 'sd' THEN b.amount + c.amount ELSE 0 END) AS sd_labor_gross,
              SUM(CASE WHEN b.department = 'bs' AND b.gl_account <> '147900' THEN b.amount ELSE 0 END) AS bs_labor_sales,
              SUM(CASE WHEN b.department = 'bs' AND c.gl_account <> '167900' THEN c.amount ELSE 0 END) AS bs_labor_cogs,
              SUM(CASE WHEN b.department = 'bs' AND c.gl_account <> '167900' THEN b.amount + c.amount ELSE 0 END) AS bs_labor_gross,
              SUM(CASE WHEN b.department = 'bs' AND b.gl_account = '147900' THEN b.amount ELSE 0 END) AS bs_paint_mat_sales,
              SUM(CASE WHEN b.department = 'bs' AND c.gl_account = '167900' THEN c.amount ELSE 0 END) AS bs_paint_mat_cogs,
              SUM(CASE WHEN b.department = 'bs' AND b.gl_account = '147900' THEN b.amount + c.amount ELSE 0 END) AS bs_paint_mat_gross,
              SUM(CASE WHEN b.department = 're' THEN b.amount ELSE 0 END) AS re_labor_sales,
              SUM(CASE WHEN b.department = 're' THEN c.amount ELSE 0 END) AS re_labor_cogs,
              SUM(CASE WHEN b.department = 're' THEN b.amount + c.amount ELSE 0 END) AS re_labor_gross,
            --  SUM(CASE WHEN b.department = 'cw' THEN b.amount ELSE 0 END) AS cw_labor_sales,
            --  SUM(CASE WHEN b.department = 'cw' THEN c.amount ELSE 0 END) AS cw_labor_cogs,
            --  SUM(CASE WHEN b.department = 'cw' THEN b.amount + c.amount ELSE 0 END) AS cw_labor_gross,
              SUM(CASE WHEN b.department = 'ql' THEN coalesce(b.amount, 0) ELSE 0 END) AS ql_labor_sales,
              SUM(CASE WHEN b.department = 'ql' THEN coalesce(c.amount, 0) ELSE 0 END) AS ql_labor_cogs,
              SUM(CASE WHEN b.department = 'ql' THEN coalesce(b.amount, 0) + coalesce(c.amount, 0) ELSE 0 END) AS ql_labor_gross,
              SUM(CASE WHEN b.department = 'pd' THEN b.amount ELSE 0 END) AS parts_sales,
              SUM(CASE WHEN b.department = 'pd' THEN c.amount ELSE 0 END) AS parts_cogs,
              SUM(CASE WHEN b.department = 'pd' THEN b.amount + c.amount ELSE 0 END) AS parts_gross
            -- SELECT *
            FROM ucinv_tmp_recon_ros_1 a
            LEFT JOIN ucinv_tmp_recon_sales b on a.stocknumber = b.stocknumber AND a.ro = b.ro
            LEFT JOIN ucinv_tmp_recon_cogs c on b.stocknumber = c.stocknumber AND b.ro = c.ro AND b.cogs_account = c.gl_account
            GROUP BY a.stocknumber;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        # Intranet SQL Scripts\crayon report\ucinv_tmp_sales_Activity.sqsl
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_sales_activity'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            SELECT a.storecode, a.stocknumber, c.yearmodel, left(c.make, 14),
              left(c.model, 26), c.vin,
              left(left(a.shape_and_size, position('-' in a.shape_and_size) - 1), 12) as shape,
              substring(a.shape_and_size,position('-' IN a.shape_and_size) + 1, 12) AS size,
              a.shape_and_size, a.from_Date, a.sale_date, a.sale_type,
              a.price_band, a.best_price, a.invoice, a.date_priced, a.days_since_priced,
              a.days_owned, a.days_avail, a.sold_from_status, a.disposition AS avail_disposition,
              coalesce(d.miles, -1) AS miles,
              coalesce(e.sale_amount, 0) AS sale_amount,
              coalesce(e.cost_of_sale, 0) AS cost_of_sale,
              coalesce(e.sales_gross, 0) AS sales_gross,
              coalesce(e.fi_sale_amount, 0) AS fi_sale_amount,
              coalesce(e.fi_cost, 0) AS fi_cost,
              coalesce(e.fi_gross, 0) AS fi_gross,
              coalesce(f.sd_labor_sales, 0),
              coalesce(f.sd_labor_cogs, 0),
              coalesce(f.sd_labor_gross, 0),
              coalesce(f.bs_labor_sales, 0),
              coalesce(f.bs_labor_cogs, 0),
              coalesce(f.bs_labor_gross, 0),
              coalesce(f.bs_paint_mat_sales, 0),
              coalesce(f.bs_paint_mat_cogs, 0),
              coalesce(f.bs_paint_mat_gross, 0),
              coalesce(f.re_labor_sales, 0),
              coalesce(f.re_labor_cogs, 0),
              coalesce(f.re_labor_gross, 0),
              coalesce(f.ql_labor_sales, 0),
              coalesce(f.ql_labor_cogs, 0),
              coalesce(f.ql_labor_gross, 0),
              coalesce(f.parts_sales, 0),
              coalesce(f.parts_cogs, 0),
              coalesce(f.parts_gross , 0),
              coalesce(f.sd_labor_sales, 0) + coalesce(f.bs_labor_sales, 0) +
                coalesce(f.bs_paint_mat_sales, 0) + coalesce(f.re_labor_sales, 0) +
                coalesce(f.ql_labor_sales, 0) + coalesce(f.parts_sales, 0),
              coalesce(f.sd_labor_cogs, 0) + coalesce(f.bs_labor_cogs, 0) +
                coalesce(f.bs_paint_mat_cogs, 0) + coalesce(f.re_labor_cogs, 0) +
                coalesce(f.ql_labor_cogs, 0) + coalesce(f.parts_cogs, 0),
              coalesce(f.sd_labor_gross, 0) + coalesce(f.bs_labor_gross, 0) +
                coalesce(f.bs_paint_mat_gross, 0) + coalesce(f.re_labor_gross, 0) +
                coalesce(f.ql_labor_gross, 0) + coalesce(f.parts_gross, 0)
              -- SELECT a.*, c.yearmodel, c.make, c.model, coalesce(d.miles, -1), c.vin
            FROM ucinv_tmp_avail_4 a
            INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
            INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID
            LEFT JOIN (
              select VehicleInventoryItemID, stocknumber, MAX(value) AS miles
              FROM (
                SELECT a.VehicleItemmileagets, a.value, b.stocknumber, b.VehicleInventoryItemID,
                  b.fromts, b.thruts
                FROM dps.vehicleitemmileages a
                INNER JOIN dps.VehicleInventoryItems b on a.VehicleItemID = b.VehicleItemID
                  AND a.VehicleItemmileagets BETWEEN b.fromts AND coalesce(b.thruts, CAST('12/31/9999 00:00:00' AS sql_timestamp))
                INNER JOIN ucinv_tmp_avail_4 c on b.VehicleInventoryItemID = c.VehicleInventoryItemID
                  AND c.sale_date = c.thedate) d
              GROUP BY VehicleInventoryItemID, stocknumber) d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
            LEFT JOIN ucinv_tmp_gross_2 e on a.stocknumber = e.stocknumber
            LEFT JOIN ucinv_tmp_fixed_gross_1 f on a.stocknumber = f.stocknumber
            WHERE a.thedate = a.sale_date;
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        # Intranet SQL Scripts\crayon report\ucinv_tmp_vehicles.sql
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'ads'
        lvl2 = 'ucinv_tmp_vehicles'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        ads_sql = """delete from """ + lvl2
        ads_cur.execute(ads_sql)
        ads_sql = """
            INSERT INTO """ + lvl2 + """
            SELECT a.stocknumber, CAST(a.fromts AS sql_date) AS from_date,
              cast(coalesce(a.thruts, cast('12/31/9999 01:00:00' as sql_timestamp)) AS sql_date) AS thru_date, b.vin, left(b.bodystyle, 30),
              left(b.TRIM , 40), left(b.interiorcolor, 30), left(b.exteriorcolor, 30),
              left(b.engine, 30), left(b.transmission, 10), left(b.make, 20),
              left(b.model, 30), b.yearmodel AS model_year,
              coalesce(c.value, 0) AS miles, left(g.shape, 10), left(g.size, 10),
              left(g.shapeandsize, 20),
              CASE
                WHEN BodyStyle LIKE  '%King%'
                  OR BodyStyle LIKE '%Access%'
                  OR BodyStyle LIKE '%Ext Cab%'
                  OR BodyStyle LIKE '%Supercab%'
                  OR BodyStyle LIKE '%Club%'
                  OR BodyStyle LIKE '%xtra%'
                  OR BodyStyle LIKE '%Double%' THEN 'Extended Cab'
                WHEN BodyStyle LIKE '%Reg%' THEN 'Regular Cab'
                WHEN BodyStyle LIKE '%crew%'
                  OR BodyStyle LIKE '%quad%'
                  OR BodyStyle LIKE 'mega%' THEN 'Crew Cab'
              END AS Cab,
              CASE
                WHEN BodyStyle LIKE '%AWD%' THEN 'AWD'
                WHEN BodyStyle LIKE '%FWD%' THEN 'FWD'
                WHEN BodyStyle LIKE '%4WD%' OR BodyStyle LIKE '%4X4%' THEN '4WD'
              END AS Drive
            FROM dps.VehicleInventoryItems a
            INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID
            LEFT JOIN (
              SELECT a.stocknumber, a.fromts, a.thruts, b.vin, c.value, c.value, c.VehicleItemMileagets
              FROM dps.VehicleInventoryItems a
              INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID
              LEFT JOIN dps.VehicleItemMileages c on b.VehicleItemID = c.VehicleItemID
              --  AND c.VehicleItemMileagets BETWEEN a.fromts AND a.thruts
              WHERE c.VehicleItemMileagets = (
                SELECT MAX(VehicleItemMileagets)
                FROM dps.VehicleItemMileages
                WHERE VehicleItemID = c.VehicleItemID
                  AND cast(VehicleItemMileagets AS sql_date) BETWEEN CAST(a.fromts AS sql_date) AND  coalesce(cast(a.thruts AS sql_date), CAST('12/31/9999' AS sql_date)))) c on a.stocknumber = c.stocknumber
            LEFT JOIN dps.MakeModelClassifications d on b.make = d.make
              AND b.model = d.model
            LEFT JOIN dps.typDescriptions e on d.vehicleSegment = e.typ
            LEFT JOIN dps.typDescriptions f on d.vehicleType = f.typ
            LEFT JOIN ucinv_shapes_and_sizes g on upper(f.description) = upper(g.shape) collate ads_default_ci
              AND upper(e.description) = upper(g.size) collate ads_default_ci
            WHERE a.stocknumber IS NOT NULL; -- 10/12/15 unresolved intramarket wholesales
        """
        ads_cur.execute(ads_sql)
        ads_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    # ads-> file -> pg
    try:
        tables = ["ucinv_tmp_avail_6", "ucinv_tmp_avail_disposition", "ucinv_tmp_sales_by_status",
                  "ucinv_tmp_sales_activity", "ucinv_tmp_avail_4", "ucinv_tmp_vehicles"]
        file_name = 'files/table_data.csv'
        for table in tables:
            ads_cur.execute("select * from " + table)
            with open(file_name, 'wb') as f:
                lvl1 = 'pg'
                lvl2 = table
                from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                log_sql = """
                    insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
                    values('%s','%s', '%s', '%s', '%s')
                """ % (log_id, the_date, lvl1, lvl2, from_time)
                log_cur.execute(log_sql)
                log_con.commit()
                csv.writer(f).writerows(ads_cur.fetchall())
                # ads_cur.close()
                f.close()
                pg_cur.execute("truncate greg." + table)
                pg_con.commit()
                io = open(file_name, 'r')
                pg_cur.copy_expert("""copy greg.""" + table + """ from stdin with(format csv)""", io)
                pg_con.commit()
                io.close()
                thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
                log_sql = """
                    update greg.ucinv_log
                    set thru_time = '%s',
                        pass_fail = 'Pass'
                    where log_id = '%s'
                      and the_date = '%s'
                      and lvl1 = '%s'
                      and lvl2 = '%s';
                """ % (thru_time, log_id, the_date, lvl1, lvl2)
                log_cur.execute(log_sql)
                log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        # populate greg.used_vehicle_daily_snapshot
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'pg'
        lvl2 = 'used_vehicle_daily_snapshot'
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        pg_cur.execute("truncate greg." + lvl2)
        pg_sql = """
            INSERT INTO greg.""" + lvl2 + """
              (store_code,the_date,stocknumber,vin,make,
              model,model_year,miles,body_style,trim_level,interior_color,exterior_color,engine,
              transmission,shape,size,shape_and_size,cab,drive,from_date,status,sale_date,
              sale_type,date_priced,best_price,invoice,days_since_priced,price_band,days_owned,
              days_avail,sold_from_status,disposition,sale_amount,cost_of_sale,sales_gross,
              fi_sale_amount,fi_cost,fi_gross,sd_labor_sales,sd_labor_cogs,sd_labor_gross,
              bs_labor_sales,bs_labor_cogs,bs_labor_gross,bs_paint_mat_sales,bs_paint_mat_cogs,
              bs_paint_mat_gross,re_labor_sales,re_labor_cogs,re_labor_gross,ql_labor_sales,
              ql_labor_cogs,ql_labor_gross,parts_sales,parts_cogs,parts_gross,recon_sales,
              recon_cogs,recon_gross,vdp_views)
            select a.storecode, a.thedate, a.stocknumber,
              b.vin, b.make, b.model, b.model_year, b.miles,
              b.body_style, b.trim_level, b.interior_color, b.exterior_color,
              b.engine, b.transmission, b.shape, b.size, b.shape_and_size,
              b.cab, b.drive,
              a.from_date, a.status, a.sale_date, a.sale_Type, a.date_priced, a.best_price, a.invoice,
              a.days_since_priced, a.price_band, a.days_owned, a.days_avail,
              a.sold_from_status, a.disposition,
              coalesce(c.sale_amount, 0), coalesce(c.cost_of_sale, 0), coalesce(c.sales_gross, 0),
              coalesce(c.fi_sale_amount, 0),
              coalesce(c.fi_cost, 0), coalesce(c.fi_gross, 0), coalesce(c.sd_labor_sales, 0),
              coalesce(c.sd_labor_cogs, 0),
              coalesce(c.sd_labor_gross, 0), coalesce(c.bs_labor_sales, 0), coalesce(c.bs_labor_cogs, 0),
              coalesce(c.bs_labor_gross, 0),
              coalesce(c.bs_paint_mat_sales, 0), coalesce(c.bs_paint_mat_cogs, 0), coalesce(c.bs_paint_mat_gross, 0),
              coalesce(c.re_labor_sales, 0), coalesce(c.re_labor_cogs, 0), coalesce(c.re_labor_gross, 0),
              coalesce(c.ql_labor_sales, 0), coalesce(c.ql_labor_cogs, 0), coalesce(c.ql_labor_gross, 0),
              coalesce(c.parts_sales, 0),
              coalesce(c.parts_cogs, 0), coalesce(c.parts_gross, 0), coalesce(c.recon_sales, 0),
              coalesce(c.recon_cogs, 0), coalesce(c.recon_gross, 0),
              coalesce(d.views, 0)
            from greg.ucinv_tmp_avail_4 a
            left join greg.ucinv_tmp_vehicles b on a.stocknumber = b.stocknumber
            left join greg.ucinv_tmp_sales_activity c on a.stocknumber = c.stocknumber
            left join ga.page_views d on a.thedate = d.thedate
              and b.vin = d.vin
            where b.vin is not null;
        """
        pg_cur.execute(pg_sql)
        pg_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        # populate temp table tmp_plg_data with potential lot gross data
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'pg'
        lvl2 = 'tmp_plg_data '
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        pg_cur.execute("drop table if exists " + lvl2)
        pg_sql = """
            CREATE TEMPORARY TABLE """ + lvl2 + """
            as
            select x.*,
              sum( plg_purchase) over w as plg_purchase_running,
              sum( plg_writedown) over w as plg_writedown_running,
              sum( plg_misc) over w as plg_misc_running,
              sum( plg_recon) over w as plg_recon_running,
              sum( plg_recon +  plg_purchase::integer + plg_writedown::integer + plg_misc::integer) over w as plg_running,
              case
                when best_price > 0 then
                  best_price - sum( plg_recon::integer +  plg_purchase::integer +  plg_writedown::integer +  plg_misc::integer) over w
                else 0
              END as plg
            from (
              select m.stocknumber, m.the_date,
                coalesce(o.plg_purchase, 0) as plg_purchase,
                coalesce(o.plg_writedown, 0) as plg_writedown,
                coalesce(o.plg_misc, 0) as plg_misc,
                coalesce(n.amount, 0) as plg_recon,
                m.best_price
              from greg.used_vehicle_daily_snapshot m
              left join ( -- recon
                select gtdate, gtctl_, sum(gttamt::integer) as amount
                from greg.ucinv_ext_glptrns
                where gtjrnl in ('SCA','SVI','AFM','POT') group by gtdate, gtctl_)n on m.stocknumber = n.gtctl_
                and m.the_date = n.gtdate
              left join (
                select a.*, coalesce(b.amount::integer, 0) as plg_purchase,
                  coalesce(c.amount::integer, 0) as plg_writedown, coalesce(d.amount::integer, 0) as plg_misc
                from (
                  select min(the_date) as the_date, stocknumber
                  from greg.used_vehicle_daily_snapshot
                  where store_code = 'RY1'
                  group by stocknumber) a
                left join ( -- purchase
                  select gtctl_, sum(amount) as amount
                  from (
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsu' and gtctl_ <> gtdoc_ group by gtctl_ having sum(gttamt) > 0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsn' group by gtctl_ having sum(gttamt) > 0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'eft' group by gtctl_ having sum(gttamt) > 1.0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'cdh' group by gtctl_ having sum(gttamt) <> 0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'gje' and gtdesc not like 'WRITE%' group by gtctl_ having sum(gttamt) > 0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'pvu' group by gtctl_ having sum(gttamt) > 0
                  ) a
                  group by gtctl_) b on a.stocknumber = b.gtctl_
                left join ( --writedown
                  select gtctl_, sum(gttamt) as amount
                  from greg.ucinv_ext_glptrns
                  where gtpost = 'y' and (gtjrnl = 'WTD' or (gtjrnl = 'GJE' and gtdesc like 'WRITE%'))
                  group by gtctl_
                  having sum(gttamt) < 0) c on a.stocknumber = c.gtctl_
                left join ( -- misc
                  select gtctl_, sum(gttamt) as amount from
                  greg.ucinv_ext_glptrns
                  where gtpost = 'y'
                    and gtjrnl in ('CRC','PCA')
                  group by gtctl_
                  having sum(gttamt) > 0) d on a.stocknumber = d.gtctl_) o on m.stocknumber = o.stocknumber
                  and m.the_date = o.the_date
              where m.store_code = 'RY1'
                and m.the_date <> m.sale_date -- no plg on sale date
                and m.the_date > ( -- already did backfill,
                  select min(the_date)
                  from greg.used_vehicle_daily_snapshot
                  where sale_date > current_date - 14
                  and store_code = 'RY1')) x
            window w as (partition by stocknumber order by the_date);
        """
        pg_cur.execute(pg_sql)
        pg_con.commit()
        pg_cur.execute("create unique index on tmp_plg_data(the_Date, stocknumber)")
        pg_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
        raise
    try:
        # update greg.used_Vehicle_daily_snapshot with values from temp table tmp_plg_data
        from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        lvl1 = 'pg'
        lvl2 = 'greg.used_Vehicle_daily_snapshot a '
        log_sql = """
            insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
            values('%s','%s', '%s', '%s', '%s')
        """ % (log_id, the_date, lvl1, lvl2, from_time)
        log_cur.execute(log_sql)
        log_con.commit()
        pg_sql = """
            UPDATE """ + lvl2 + """
            set plg_purchase = x.plg_purchase,
                plg_writedown = x.plg_writedown,
                plg_misc = x.plg_misc,
                plg_recon = x.plg_recon,
                plg_purchase_running = x.plg_purchase_running,
                plg_writedown_running = x.plg_writedown_running,
                plg_misc_running = x.plg_misc_running,
                plg_recon_running = x.plg_recon_running,
                plg_running = x.plg_running,
                plg = x.plg
            from tmp_plg_data x
            where a.stocknumber = x.stocknumber
              and a.the_date = x.the_date;
        """
        pg_cur.execute(pg_sql)
        pg_con.commit()
        thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
        log_sql = """
            update greg.ucinv_log
            set thru_time = '%s',
                pass_fail = 'Pass'
            where log_id = '%s'
              and the_date = '%s'
              and lvl1 = '%s'
              and lvl2 = '%s';
        """ % (thru_time, log_id, the_date, lvl1, lvl2)
        log_cur.execute(log_sql)
        log_con.commit()
    except Exception, error:
        err = error
except Exception, error:
    print string.replace(str(err), "'", "")
    log_con.rollback()
    log_sql = """
        update greg.ucinv_log
        set pass_fail = '%s'
        where log_id = '%s'
          and the_date = '%s'
          and lvl1 = '%s'
          and lvl2 = '%s';
    """ % (string.replace(str(err), "'", ""), log_id, the_date, lvl1, lvl2)
    # """ % (str(err), log_id, the_date, lvl1, lvl2)  # (str(err), log_id, the_date, lvl1, lvl2, lvl3)
    log_cur.execute(log_sql)
    log_con.commit()
finally:
    # io.close()
    pg_cur.close()
    log_cur.close()
    ads_cur.close()
    pg_con.close()
    ads_con.close()
    log_con.close()
