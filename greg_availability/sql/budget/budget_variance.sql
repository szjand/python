﻿drop table if exists fin.fact_budget_variance;
create table fin.fact_budget_variance (
  year_month integer not null,
  store_code citext NOT NULL,
  department citext NOT NULL,
  line_item citext NOT NULL,
  display_seq integer NOT NULL,
  budget integer NOT NULL DEFAULT 0,
  actual integer not null default 0,
  var_amount integer not null default 0,
  var_percent numeric(7,2) not null default 0,
  CONSTRAINT fact_budget_variance_pk PRIMARY KEY (store_code, department, line_item, year_month));

-- drop table if exists budget_variance;
-- create temp table budget_variance as
insert into fin.fact_budget_variance
select a.year_month, 'ry1', a.department, a.line_item, a.display_seq, 
  a.value as budget, 
  case
    when a.display_seq = 2 then coalesce(sum(-c.amount), 0) 
    else coalesce(sum(c.amount), 0)
  end as actual, 
  case 
    when a.display_seq = 2 then coalesce(sum(-c.amount), 0) - a.value
    else coalesce(sum(c.amount), 0) - a.value 
  end as var_amount, 
  case
    when a.value = 0 then 0
    else
      case 
        when a.display_seq = 2 then round(100.0 * (coalesce(sum(-c.amount), 0) - a.value)/a.value, 1)
        else round(100.0 * (coalesce(sum(c.amount), 0) - a.value)/a.value, 1) 
      end
  end as var_percent
from fin.budget a
inner join fin.dim_fs_org b on 
  case a.department 
    when 'body shop' then b.store = 'RY1' and b.department = 'body shop'
    when 'detail' then b.store = 'ry1' and b.sub_department = 'detail'
    when 'pdq' then b.store = 'ry1' and b.sub_department = 'quick lane'
    when 'sales' then b.store = 'ry1' and b.area = 'variable'
    when 'carwash' then b.store = 'ry1' and b.sub_department = 'car wash'
    when 'main shop' then b.store = 'ry1' and b.sub_department = 'mechanical'
    when 'parts' then b.store = 'ry1' and b.department = 'parts'
  end
left join fin.fact_fs c on a.year_month = c.year_month
  and b.fs_org_key = c.fs_org_key
--   and c.page in (3,4,16)
  and 
    case 
      when a.display_seq = 2 and a.department = 'sales' then (
        (c.page between 5 and 15)
        or 
        (c.page = 16 and c.line between 1 and 10)
        or
        (c.page = 17 and c.line between 1 and 19)) 
      when a.display_seq = 2 and a.department = 'parts' then c.page = 16 and c.line between 45 and 60
      when a.display_seq = 2 and a.department = 'body shop' then c.page = 16 and c.line between 35 and 42
      when a.display_seq = 2 then c.page = 16 and c.line between 21 and 33
      when a.display_seq = 3 then c.page in (3,4) and c.line = 4
      when a.display_seq = 4 then c.page in (3,4) and c.line = 5
      when a.display_seq = 5 then c.page in (3,4) and c.line = 6
      when a.display_seq = 6 then c.page in (3,4) and c.line between 4 and 6
      when a.display_seq = 7 then c.page in (3,4) and c.line = 8
      when a.display_seq = 8 then c.page in (3,4) and c.line = 9
      when a.display_seq = 9 then c.page in (3,4) and c.line = 10
      when a.display_seq = 10 then c.page in (3,4) and c.line = 11
      when a.display_seq = 11 then c.page in (3,4) and c.line = 12
      when a.display_seq = 12 then c.page in (3,4) and c.line = 14
      when a.display_seq = 13 then c.page in (3,4) and c.line = 15
      when a.display_seq = 14 then c.page in (3,4) and c.line = 16
      when a.display_seq = 15 then c.page in (3,4) and c.line between 8 and 16
      when a.display_seq = 16 then c.page in (3,4) and c.line = 18
      when a.display_seq = 17 then c.page in (3,4) and c.line = 19
      when a.display_seq = 18 then c.page in (3,4) and c.line = 20
      when a.display_seq = 19 then c.page in (3,4) and c.line = 21
      when a.display_seq = 20 then c.page in (3,4) and c.line = 22
      when a.display_seq = 21 then c.page in (3,4) and c.line = 23
      when a.display_seq = 22 then c.page in (3,4) and c.line = 24
      when a.display_seq = 23 then c.page in (3,4) and c.line = 24
      when a.display_seq = 24 then c.page in (3,4) and c.line = 25
      when a.display_seq = 25 then c.page in (3,4) and c.line = 26
      when a.display_seq = 26 then c.page in (3,4) and c.line = 27
      when a.display_seq = 27 then c.page in (3,4) and c.line = 28
      when a.display_seq = 28 then c.page in (3,4) and c.line = 30
      when a.display_seq = 29 then c.page in (3,4) and c.line = 31
      when a.display_seq = 30 then c.page in (3,4) and c.line = 32
      when a.display_seq = 31 then c.page in (3,4) and c.line = 33
      when a.display_seq = 32 then c.page in (3,4) and c.line = 34
      when a.display_seq = 33 then c.page in (3,4) and c.line = 35
      when a.display_seq = 34 then c.page in (3,4) and c.line = 36
      when a.display_seq = 35 then c.page in (3,4) and c.line = 37
      when a.display_seq = 36 then c.page in (3,4) and c.line = 38
      when a.display_seq = 37 then c.page in (3,4) and c.line = 39
      when a.display_seq = 38 then c.page in (3,4) and c.line between 18 and 39
      when a.display_seq = 39 then c.page in (3,4) and c.line = 41
      when a.display_seq = 40 then c.page in (3,4) and c.line = 42
      when a.display_seq = 41 then c.page in (3,4) and c.line = 43
      when a.display_seq = 42 then c.page in (3,4) and c.line = 44
      when a.display_seq = 43 then c.page in (3,4) and c.line = 45
      when a.display_seq = 44 then c.page in (3,4) and c.line = 46
      when a.display_seq = 45 then c.page in (3,4) and c.line = 47
      when a.display_seq = 46 then c.page in (3,4) and c.line = 48
      when a.display_seq = 47 then c.page in (3,4) and c.line = 50
      when a.display_seq = 48 then c.page in (3,4) and c.line = 51
      when a.display_seq = 49 then c.page in (3,4) and c.line = 52
      when a.display_seq = 50 then c.page in (3,4) and c.line = 53
      when a.display_seq = 51 then c.page in (3,4) and c.line = 65
      when a.display_seq = 52 then c.page in (3,4) and c.line between 41 and 54
      when a.display_seq = 53 then c.page in (3,4) and c.line between 4 and 54
--       when 54 then 0
    end  
-- where a.year_month = 201607
--   and a.department in ('sales', 'body shop')
--   and a.display_seq in (2)
group by a.year_month, a.department, a.line_item, a.display_seq, a.value 
order by a.department, a.year_month, a.display_seq;

-- select * from fin.fact_budget_variance where year_month = 201605 and department = 'parts' order by display_Seq
-- 
-- select * from budget_variance order by var_percent

-- the only issue, suprise, suprise is parts split
-- on the budget, parts split adds to the total gross profit of body shop, main shop and quick lane
-- and subtracts from parts total gross profit
-- of course, parts is going to be a little tricky

-- parts:
update fin.fact_budget_variance a
set actual = y.amount,
    var_amount = y.amount - budget, 
    var_percent = round(100.0 * (y.amount - budget)/budget, 1)
from (
  select year_month, sum(-amount) as amount
  from (
    select year_month, -sum(amount) as amount
    from fin.fact_fs a
    inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
      and b.store = 'ry1'
      and b.area = 'fixed'
      and b.sub_department not in ('car wash, detail')
    where page = 4 
      and line = 59 
      and a.year_month between 201601 and 201607
    group by year_month
    union
    select year_month,sum(amount) as amount
    from fin.fact_fs a
    inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
      and b.store = 'ry1'
    where page = 16
      and a.year_month between 201601 and 201607
      and a.year_month = 201607
    group by year_month) x
  group by year_month) y
where a.department = 'parts' 
  and a.year_month = y.year_month
  and a.display_seq = 2;

-- select * from budget_variance where department = 'parts' and display_Seq = 2 order by year_month
-- 
-- select * from budget_variance where display_Seq = 2 order by year_month
-- and fix the gross/net profit for body shop, main shop and quick lane
-- parts split numbers

update fin.fact_budget_variance a
set actual = actual + x.parts_split,
    var_amount = actual + x.parts_split - budget,
    var_percent = round(100.0 * (actual + x.parts_split - budget)/budget, 1)
from (
  select year_month, 
    case sub_department
      when 'mechanical' then 'main shop' 
      when 'quick lane' then 'pdq'
      when 'none' then 'body shop'
    end as department, 
    sum(-amount) as parts_split
  from fin.fact_fs a
  inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
    and b.store = 'ry1'
    and b.area = 'fixed'
    and b.sub_department not in ('car wash, detail', 'parts')
  where page = 4 
    and line = 59 
    and a.year_month between 201601 and 201607
  group by year_month, sub_department) x
where display_seq = 2
  and a.department = x.department
  and a.year_month = x.year_month;

-- select * from budget_variance where department = 'pdq' and display_seq = 2 order by year_month

-- ooh and also net profit for detail, sales and carwash, 
-- then it's done
--  net profit
-- do this last after all the parts split adjustments
update fin.fact_budget_variance a
set actual = x.net_profit,
    var_amount = x.var_amount,
    var_percent = x.var_percent
from (
  select a.year_month, a.department, a.gross_profit, b.total_exp,
    a.gross_profit - b.total_exp as net_profit, 
    a.gross_profit - b.total_exp - c.budget_net_profit as var_amount,
    round(100.0 * (a.gross_profit - b.total_exp - c.budget_net_profit)/budget_net_profit, 1) as var_percent
  from (
    select year_month, department, actual as gross_profit
    from fin.fact_budget_variance
    where display_seq = 2) a
  left join (
    select year_month, department, actual as total_exp
    from fin.fact_budget_variance
    where display_seq = 53) b on a.year_month = b.year_month and a.department = b.department  
  left join (
    select year_month, department, budget as budget_net_profit
    from fin.fact_budget_variance
    where display_seq = 54) c on a.year_month = c.year_month and a.department = c.department    
  where a.year_month between 201601 and 201607) x 
where a.display_seq = 54
  and a.department = x.department
  and a.year_month = x.year_month;   
 

-- select * from fin.fact_budget_variance where display_seq in (2,15,38,52,53,54) and year_month in (201601,201605) and department = 'body shop' order by year_month, display_seq

