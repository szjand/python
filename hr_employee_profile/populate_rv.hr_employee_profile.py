# encoding=utf-8
import csv
import db_cnx

pg_con = None
db2_con = None
ads_con = None
mysql_con = None
file_name = None
try:
    file_name = 'files/ext_pymast.csv'
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select a.pymast_company_number,
                  a.employee_name,
                  trim(a.pymast_employee_number) as employee_number,
                  b.description as department,
                  a.street_name, a.city_name, a.state_code_address_, a.zip_code
                from rydedata.pymast a
                left join rydedata.pypclkctl b on a.department_code = b.department_code
                  and a.pymast_company_number = b.company_number
                where a.active_code <> 'T'
                  and a.pymast_company_number in ('RY1','RY2')
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate rv.ext_pymast_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy rv.ext_pymast_tmp from stdin with csv encoding 'latin-1 '""", io)
            file_name = 'files/ext_employee_dim.csv'
            with db_cnx.ads_dds() as ads_con:
                with ads_con.cursor() as ads_cur:
                    sql = """
                        SELECT storecode, employeenumber, firstname, lastname, middlename
                        FROM edwEmployeeDim
                        WHERE currentrow = true
                          AND activecode = 'a'
                    """
                    ads_cur.execute(sql)
                    with open(file_name, 'wb') as f:
                        csv.writer(f).writerows(ads_cur)
            pg_cur.execute("truncate rv.ext_dim_employee_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy rv.ext_dim_employee_tmp from stdin with csv encoding 'latin-1 '""", io)
            file_name = 'files/ext_shoretel.csv'
            with db_cnx.mysql_shoretel_config() as mysql_con:
                with mysql_con.cursor() as mysql_cur:
                    sql = """
                        select jacknumber, currentdn
                        from ports
                        where jacknumber is not null
                        and jacknumber <> ''
                    """
                    mysql_cur.execute(sql)
                    with open(file_name, 'wb') as f:
                        csv.writer(f).writerows(mysql_cur)
            pg_cur.execute("truncate rv.ext_phone_extensions_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy rv.ext_phone_extensions_tmp from stdin with csv encoding 'latin-1 '""", io)
            pg_cur.execute("truncate rv.hr_employee_profile")
            sql = """
                insert into rv.hr_employee_profile (store_code, first_name, middle_name, last_name,
                  employee_number, phone_extension, department, address, city, state, zip)
                select a.store_code, b.first_name, b.middle_name, b.last_name,
                  a.employee_number, max(phone_extension), a.department, a.address, a.city, a.state, a.zip
                from rv.ext_pymast_tmp a
                inner join rv.ext_dim_employee_tmp b on a.employee_number = b.employee_number
                  and a.store_code = b.store_code
                left join rv.ext_phone_extensions_tmp c on a.employee_number = c.employee_number
                group by a.store_code, b.first_name, b.middle_name, b.last_name,
                      a.employee_number, a.department, a.address, a.city, a.state, a.zip
            """
            pg_cur.execute(sql)
except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()
    if db2_con:
        db2_con.close()
    if ads_con:
        ads_con.close()
    if mysql_con:
        mysql_con.close()
