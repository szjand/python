﻿some employees have multiple extensions

select a.store_code, b.first_name, b.middle_name, b.last_name,
  a.employee_number, max(phone_extension), a.department, a.address, 
  a.city, a.state, a.zip
from rv.ext_pymast_tmp a
inner join rv.ext_dim_employee_tmp b on a.employee_number = b.employee_number
  and a.store_code = b.store_code
left join rv.ext_phone_extensions_tmp c on a.employee_number = c.employee_number
group by a.store_code, b.first_name, b.middle_name, b.last_name,
      a.employee_number, a.department, a.address, a.city, a.state, a.zip
order by a.employee_number

      
select * from rv.hr_employee_profile
where employee_number in (
select employee_number
from rv.ext_phone_extensions_tmp
group by employee_number
having count(*) > 1)    

truncate rv.hr_employee_profile;

insert into rv.hr_employee_profile (store_code, first_name, middle_name, last_name,
  employee_number, phone_extension, department, address, city, state, zip)
select a.store_code, b.first_name, b.middle_name, b.last_name,
  a.employee_number, max(phone_extension), a.department, a.address, a.city, a.state, a.zip
from rv.ext_pymast_tmp a
inner join rv.ext_dim_employee_tmp b on a.employee_number = b.employee_number
  and a.store_code = b.store_code
left join rv.ext_phone_extensions_tmp c on a.employee_number = c.employee_number
group by a.store_code, b.first_name, b.middle_name, b.last_name,
      a.employee_number, a.department, a.address, a.city, a.state, a.zip