# encoding=utf-8
import db_cnx
import csv
from openpyxl import load_workbook
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os

file_name = 'files/open_purchase_orders.csv'
spreadsheet = 'files/open_purchase_orders.xlsx'
sheet_name = "Open PO"

with db_cnx.dealertrack_report2() as arkona_con:
    with arkona_con.cursor() as arkona_cur:
        sql = """
            Select trim(po_number) as po_number, po_date, trim(po_type) as po_type, 
              trim(vendor_number) as vendor_number, trim(vendor_name) as vendor, 
              po_amount, trim(a.user) as written_by, 
              b.user_name
            from RYDEDATA.GLPPOHD a
            left join rydedata.sepuser b on a.user = b.user
              and a.company_number = b.company_number
            where po_status = ''
            order by vendor_name, po_date        
        """
        arkona_cur.execute(sql)
        with open(file_name, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(["PO_Number", "PO_Date", "PO_Type", "Vendor_Number", "Vendor", "PO_Amount",
                              "Written_By", "Name"])
            writer.writerows(arkona_cur)
        wb = load_workbook(spreadsheet)
        wb.remove_sheet(wb.get_sheet_by_name(sheet_name))
        ws = wb.create_sheet(title=sheet_name)
        with open(file_name) as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                ws.append(row)
        wb.save(spreadsheet)
try:
    COMMASPACE = ', '
    sender = 'jandrews@cartiva.com'
    recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
    # recipients = ['test@cartiva.com', 'jandrews@cartiva.com', 'acctspay@rydellcars.com']
    outer = MIMEMultipart()
    outer['Subject'] = 'Open Purchase Orders'
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    attachments = [spreadsheet]
    for file in attachments:
        try:
            with open(file, 'rb') as fp:
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read())
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(file))
            outer.attach(msg)
        except Exception:
            raise
    composed = outer.as_string()
    e = smtplib.SMTP('mail.cartiva.com')
    try:
        e.sendmail(sender, recipients, composed)
    except smtplib.SMTPException:
        print("Error: unable to send email")
    e.quit()
except Exception as error:
    print(error)