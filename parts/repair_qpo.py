# coding=utf-8
import db_cnx

task = 'ext_pto_hours'
pg_con = None
file_name = 'files/repair_qpo.csv'

try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.repair_qpo_part_number_list_nov_2016")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.repair_qpo_part_number_list_nov_2016 from stdin with csv header
                    encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()
