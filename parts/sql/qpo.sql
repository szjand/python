﻿create table dds.repair_qpo_part_number_list_nov_2016 (
  product_line citext,
  part_number citext,
  part_name citext,
  part_description citext,
  first_yr_used integer,
  last_yr_used integer,
  clr_curr_dealer_price numeric(6,2));

create index on dds.repair_qpo_part_number_list_nov_2016 (part_number);

 create table dds.maintenance_qpo_part_number_list_nov_2016 (
  product_line citext,
  part_number citext,
  part_name citext,
  part_description citext,
  first_yr_used integer,
  last_yr_used integer,
  clr_curr_dealer_price numeric(6,2)); 
create index on dds.maintenance_qpo_part_number_list_nov_2016 (part_number);

create index on dds.ext_pdpmast(status);

select * 
from dds.repair_qpo_part_number_list_nov_2016 a 
inner join dds.ext_pdpmast b on a.part_number = b.part_number
  and b.status = 'A'


select *
from dds.ext_pdptdet a
inner join (
  select a.part_number 
  from dds.repair_qpo_part_number_list_nov_2016 a 
  inner join dds.ext_pdpmast b on a.part_number = b.part_number
    and b.status = 'A') b on a.ptpart = b.part_number

select a.ptpart, 
  sum(case when d.yearmonth = 201511 then ptqty else 0 end) as NOV,
  sum(case when d.yearmonth = 201512 then ptqty else 0 end) as Dec,
  sum(case when d.yearmonth = 201601 then ptqty else 0 end) as JAN,
  sum(case when d.yearmonth = 201602 then ptqty else 0 end) as FEB,
  sum(case when d.yearmonth = 201603 then ptqty else 0 end) as MAR,
  sum(case when d.yearmonth = 201604 then ptqty else 0 end) as APR,
  sum(case when d.yearmonth = 201605 then ptqty else 0 end) as MAY,
  sum(case when d.yearmonth = 201606 then ptqty else 0 end) as JUN,
  sum(case when d.yearmonth = 201607 then ptqty else 0 end) as JUL,
  sum(case when d.yearmonth = 201608 then ptqty else 0 end) as AUG,
  sum(case when d.yearmonth = 201609 then ptqty else 0 end) as SEP,
  sum(case when d.yearmonth = 201610 then ptqty else 0 end) as OCT
from dds.ext_pdptdet a
inner join dds.repair_qpo_part_number_list_nov_2016 b on a.ptpart = b.part_number
inner join dds.ext_pdpmast c on b.part_number = c.part_number
  and c.status = 'A'
inner join dds.day d on (substr(a.ptdate::citext,5, 2)||'-'||right(a.ptdate::citext, 2)||'-'||left(a.ptdate::citext, 4))::date = d.thedate
where a.ptdate between 20151101 and 20161031
group by a.ptpart
order by ptpart

-- repair
select a.part_number, a.part_name as name, a.part_description as description, 
  b.bin_location as bin, b.qty_on_hand as QOH, b.qty_on_order as QOO, c.*
from dds.repair_qpo_part_number_list_nov_2016 a 
inner join dds.ext_pdpmast b on a.part_number = b.part_number
  and b.status = 'A'
  and b.company_number = 'RY1'    
left join (
  select a.ptpart, 
    sum(case when d.yearmonth = 201610 then ptqty else 0 end) as "OCT-16",
    sum(case when d.yearmonth = 201609 then ptqty else 0 end) as "SEP-16",
    sum(case when d.yearmonth = 201608 then ptqty else 0 end) as "AUG-16",
    sum(case when d.yearmonth = 201607 then ptqty else 0 end) as "JUL-16",
    sum(case when d.yearmonth = 201606 then ptqty else 0 end) as "JUN-16",
    sum(case when d.yearmonth = 201605 then ptqty else 0 end) as "MAY-16",
    sum(case when d.yearmonth = 201604 then ptqty else 0 end) as "APR-16",
    sum(case when d.yearmonth = 201603 then ptqty else 0 end) as "MAR-16",
    sum(case when d.yearmonth = 201602 then ptqty else 0 end) as "FEB-16",
    sum(case when d.yearmonth = 201601 then ptqty else 0 end) as "JAN-16",
    sum(case when d.yearmonth = 201512 then ptqty else 0 end) as "Dec-15",
    sum(case when d.yearmonth = 201511 then ptqty else 0 end) as "NOV-15"
  from dds.ext_pdptdet a
  inner join dds.repair_qpo_part_number_list_nov_2016 b on a.ptpart = b.part_number
  inner join dds.ext_pdpmast c on b.part_number = c.part_number
    and c.status = 'A'
    and c.company_number = 'RY1'
  inner join dds.day d on (substr(a.ptdate::citext,5, 2)||'-'||right(a.ptdate::citext, 2)||'-'||left(a.ptdate::citext, 4))::date = d.thedate
  where a.ptdate between 20151101 and 20161031
    AND a.ptco_ = 'RY1'
    and ptcode in (
      'CP', /*RO Customer Pay Sale*/
      'IS', /*RO Internal Sale*/ 
      'SA', /*Counter Sale*/
      'WS', /*RO Warranty Sale*/
      'SC', /*RO Service Contract Sale*/ 
      'SR', /*RO Return*/ 
      'CR', /*RO Correction*/ 
      'RT', /*Counter Return*/ 
      'LS', /*Lost Sales*/
      'FR') /*Factory Return*/  
     and pttgrp in ('S','O', 'I', 'W', 'R') -- S:201608-11588915 4 ordered by ptcode is SA, O:201603-11518863, I:201605-11518863 
     and not exists ( -- 12471641 - voids
      select 1
      from fin.fact_gl aa
      inner join dds.day bb on aa.date_key = bb.datekey
        and bb.yearmonth > 201510
      where aa.post_status = 'V'
        and aa.control = a.ptinv_
        and not exists (
          select 1
          from fin.fact_gl
          where post_status = 'Y'
            and control = aa.control))
  group by a.ptpart) c on a.part_number = c.ptpart  



-- maintenance
select a.part_number, a.part_name as name, a.part_description as description, 
  b.bin_location as bin, b.qty_on_hand as QOH, b.qty_on_order as QOO, c.*
from dds.maintenance_qpo_part_number_list_nov_2016 a 
inner join dds.ext_pdpmast b on a.part_number = b.part_number
  and b.status = 'A'
  and b.company_number = 'RY1'    
left join (
  select a.ptpart, 
    sum(case when d.yearmonth = 201610 then ptqty else 0 end) as "OCT-16",
    sum(case when d.yearmonth = 201609 then ptqty else 0 end) as "SEP-16",
    sum(case when d.yearmonth = 201608 then ptqty else 0 end) as "AUG-16",
    sum(case when d.yearmonth = 201607 then ptqty else 0 end) as "JUL-16",
    sum(case when d.yearmonth = 201606 then ptqty else 0 end) as "JUN-16",
    sum(case when d.yearmonth = 201605 then ptqty else 0 end) as "MAY-16",
    sum(case when d.yearmonth = 201604 then ptqty else 0 end) as "APR-16",
    sum(case when d.yearmonth = 201603 then ptqty else 0 end) as "MAR-16",
    sum(case when d.yearmonth = 201602 then ptqty else 0 end) as "FEB-16",
    sum(case when d.yearmonth = 201601 then ptqty else 0 end) as "JAN-16",
    sum(case when d.yearmonth = 201512 then ptqty else 0 end) as "Dec-15",
    sum(case when d.yearmonth = 201511 then ptqty else 0 end) as "NOV-15"
  from dds.ext_pdptdet a
  inner join dds.maintenance_qpo_part_number_list_nov_2016 b on a.ptpart = b.part_number
  inner join dds.ext_pdpmast c on b.part_number = c.part_number
    and c.status = 'A'
    and c.company_number = 'RY1'
  inner join dds.day d on (substr(a.ptdate::citext,5, 2)||'-'||right(a.ptdate::citext, 2)||'-'||left(a.ptdate::citext, 4))::date = d.thedate
  where a.ptdate between 20151101 and 20161031
    AND a.ptco_ = 'RY1'
    and ptcode in (
      'CP', /*RO Customer Pay Sale*/
      'IS', /*RO Internal Sale*/ 
      'SA', /*Counter Sale*/
      'WS', /*RO Warranty Sale*/
      'SC', /*RO Service Contract Sale*/ 
      'SR', /*RO Return*/ 
      'CR', /*RO Correction*/ 
      'RT', /*Counter Return*/ 
      'LS', /*Lost Sales*/
      'FR') /*Factory Return*/  
     and pttgrp in ('S','O', 'I', 'W', 'R') -- S:201608-11588915 4 ordered by ptcode is SA, O:201603-11518863, I:201605-11518863 
     and not exists ( -- 12471641 - voids
      select 1
      from fin.fact_gl aa
      inner join dds.day bb on aa.date_key = bb.datekey
        and bb.yearmonth > 201510
      where aa.post_status = 'V'
        and aa.control = a.ptinv_
        and not exists (
          select 1
          from fin.fact_gl
          where post_status = 'Y'
            and control = aa.control))
  group by a.ptpart) c on a.part_number = c.ptpart  




select *
from dds.ext_pdptdet
where ptpart= '24435052'
  and ptdate between 20161001 and 20161031
  and left(ptinv_, 1) <> '1'
  and pttgrp in ('S','O', 'I', 'W', 'R')
order by ptdate desc   

select *
from dds.repair_qpo_part_number_list_nov_2016 a 
inner join dds.ext_pdpmast b on a.part_number = b.part_number
  and b.status = 'A'  
where a.part_number = '11518863' 

select pttgrp, count(*)
from dds.ext_pdptdet
group by pttgrp

select control
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth > 201510
where post_status = 'V'


select b.thedate, a.*
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
where control = '16216459'
  

'W';      912721
'<NULL>'; 134636
'I';      45132
'O';      1814216
'R';      451941
'S';      1591240

11518863 shows up with 2 bin and 2 qoh
select * 
from dds.ext_pdpmast
where part_number = '11518863'

