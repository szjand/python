# coding=utf-8
import db_cnx
pg_con = None
file_name = 'files/maintenance_qpo.csv'

try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.maintenance_qpo_part_number_list_nov_2016")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.maintenance_qpo_part_number_list_nov_2016 from stdin with csv header
                    encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()