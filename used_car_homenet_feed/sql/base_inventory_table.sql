/*
reworking of PROCEDURE InternetFeedsInventoryTableByMarketName to generate the 
					actual data, rather than a temp TABLE name
					
*/					
SELECT
  (SELECT o.FullName FROM Organizations o WHERE o.PartyID = 'A4847DFC-E00E-42E7-89EE-CD4368445A82') as Market, -- hard code for GF market
  (SELECT o.FullName FROM Organizations o WHERE o.PartyID = vii.OwningLocationID) as Location,
  vi.YearModel AS Year,
  vi.Make ,
  vi.Model AS Model,
  vi.Trim AS TrimLevel,
  vi.BodyStyle as BodyStyle,
  vi.ExteriorColor AS Color,
  vi.InteriorColor AS InteriorColor,
  vi.Engine as Engine,
  vi.Transmission as Transmission,
  (SELECT Top 1 Value 
    FROM VehicleItemMileages vim 
	WHERE vim.VehicleItemID = vii.VehicleItemID
	ORDER BY VehicleItemMileageTS DESC) AS Mileage,
  replace(vii.StockNumber, '*', '') AS StockNumber,
  vi.VIN,
  (SELECT TOP 1 vpd.Amount
	 FROM VehiclePricings vp
	 inner join VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
	 WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
     ORDER BY VehiclePricingTS DESC) as Price,
  (SELECT top 1 typ 
    FROM selectedreconpackages 
    WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID 
      AND BasisTable <> 'VehicleEvaluations'
    ORDER BY SelectedReconPackageTS desc) as ReconPackage,
  (SELECT viiic.InternetComment FROM VehicleInventoryItemInternetComments viiic 
    WHERE viiic.VehicleInventoryItemID = vii.VehicleInventoryItemID) as InternetComment,  
  vii.VehicleInventoryItemID,
  vii.OwningLocationID,
  vii.VehicleItemID,
  CASE td.Description
    WHEN 'Crossover' THEN 'SUV'
    ELSE td.Description
  END AS VehicleType,
  x.cert,
  CASE 
    WHEN vii.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'G'
    WHEN vii.locationid = '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'H'
    WHEN vii.locationid = '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'O'
		WHEN vii.locationid = '77a18168-998f-8747-ae24-3efc540cd069' THEN 'T'
  END AS Comment1,
	CAST(cast(xx.imcost AS sql_double) AS sql_integer) AS VehicleCost,
	aaa.vdp_link
  into #IFIT
  FROM VehicleInventoryItems vii
  inner join VehicleItems vi on vi.VehicleItemID = vii.VehicleItemID	
  LEFT JOIN MakeModelClassifications mmcx ON vi.Make = mmcx.Make
    AND vi.Model = mmcx.Model
  LEFT JOIN TypDescriptions td ON mmcx.VehicleType = td.typ    
  LEFT JOIN FactoryCertifications x on vii.VehicleInventoryItemID = x.VehicleInventoryItemID     
	LEFT JOIN dds.stgarkonainpmast xx on vii.stocknumber = xx.imstk# 
	LEFT JOIN vdp_links aaa on vi.vin = aaa.vin
  WHERE vii.ThruTS IS NULL
    AND vii.OwningLocationID IN (SELECT PartyID2 
	                               FROM PartyRelationships 
								   WHERE typ = 'PartyRelationship_MarketLocations' 
								     AND PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82') -- grand forks market
    AND  Exists (SELECT 1 
                FROM VehicleInventoryItemStatuses viis 
				where viis.VehicleInventoryItemID = vii.VehicleInventoryItemID
				  and viis.ThruTS is Null and Status in ('RawMaterials_RawMaterials'))
														 				
  AND NOT Exists(SELECT 1 
                FROM VehicleInventoryItemStatuses viis 
				where viis.VehicleInventoryItemID = vii.VehicleInventoryItemID
				  and viis.ThruTS is Null and Status in ('RMFlagSB_SalesBuffer',
                                                         'RMFlagWSB_WholesaleBuffer',
				                                         'RMFlagPIT_PurchaseInTransit',
														 'RMFlagTNA_TradeNotAvailable',
														 'RMFlagIP_InspectionPending',
														 'RMFlagWP_WalkPending'));