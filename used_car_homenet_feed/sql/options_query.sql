CREATE PROCEDURE GetVehicleItemLongOptionNamesString
   ( 
      VehicleItemID CHAR ( 38 ),
      LongOptionNamesString Memo OUTPUT
   ) 
BEGIN 

/*
DROP PROCEDURE GetVehicleItemLongOptionNamesString;
EXECUTE PROCEDURE GetVehicleItemLongOptionNamesString('CBEAA662-7400-45DC-8B84-048FADBFA254')
*/
DECLARE @ShortOptionString String;
DECLARE @ShortOption String;
DECLARE @CommaPos Integer;
DECLARE @Cur CURSOR;

SELECT Utilities.DropTablesIfExist('#GVILONS') FROM system.iota;

CREATE TABLE #GVILONS (
  ShortOption char ( 22 )
);

@ShortOptionString = (SELECT Trim(VehicleOptions) FROM VehicleItemOptions WHERE VehicleItemID = (SELECT VehicleItemID FROM __INPUT));

if @ShortOptionString is null then
  @ShortOptionString = '';
end;


WHILE True=True DO
  IF Length(@ShortOptionString) = 0 THEN 
    LEAVE; 
  END;
  @CommaPos =  Locate(',', @ShortOptionString);
  IF @CommaPos = 0 THEN
    @ShortOptionString = TRIM(@ShortOptionString);
    INSERT INTO #GVILONS VALUES(@ShortOptionString);
    LEAVE;
  ELSE 
    @ShortOption = SubString(@ShortOptionString, 1, @CommaPos - 1);
    @ShortOptionString = SubString(@ShortOptionString, @CommaPos + 1, Length(@ShortOptionString));
    @ShortOption = Trim(@ShortOption);
    @ShortOptionString = TRIM(@ShortOptionString);
    INSERT INTO #GVILONS VALUES(@ShortOption);
  END;
END;

if @ShortOptionString <> '' THEN

  @ShortOptionString = '';

  OPEN @Cur AS SELECT ao.OptionName AS Option
      FROM AppraisalOptions ao 
      WHERE ao.OptionShortName IN (SELECT ShortOption 
                                     FROM #GVILONS)
      ORDER BY ao.Sequence;


  TRY                                   
    WHILE FETCH @CUR DO
      IF @ShortOptionString = '' THEN
        @ShortOptionString = Trim(@Cur.Option);
      ELSE
        @ShortOptionString = @ShortOptionString + ', ' + Trim(@Cur.Option);
      ENDIF;  
    ENDWHILE;
  FINALLY
    CLOSE @Cur;
  END;
END;                                  

INSERT INTO __output VALUES (@ShortOptionString);


   
   






END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'GetVehicleItemLongOptionNamesString', 
   'COMMENT', 
   '');

