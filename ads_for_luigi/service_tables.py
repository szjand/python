# coding=utf-8
import db_cnx
import csv
import ops
import string

"""
    ext_dim_tech
    ext_dim_service_writer
    ext_dim_service_type
    ext_dim_payment_type
    ext_dim_ro_status
    bs_appointments
    scheduler_appointments
    dimccc
    dimrocomment

3/11/20
    removed scheduler_honda_appointments(): the server is no longer in use
06/28/20
    add dimccc    
07/04/20
    add dimrocomment     

08/02/22
    the only thing left active is ron scheduler appointments    
"""

pg_server = '173'


# def dim_tech():
#     pg_con = None
#     ads_con = None
#     task = 'ext_dim_tech'
#     try:

#         file_name = 'files/ext_dim_tech.csv'
#         with db_cnx.ads_dds() as ads_con:
#             with ads_con.cursor() as ads_cur:
#                 sql = """
#                     SELECT *
#                     from dimTech
#                 """
#                 ads_cur.execute(sql)
#                 with open(file_name, 'wb') as f:
#                     csv.writer(f).writerows(ads_cur.fetchall())
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 pg_cur.execute("truncate ads.ext_dim_tech")
#                 with open(file_name, 'r') as io:
#                     pg_cur.copy_expert("""copy ads.ext_dim_tech from stdin with csv encoding 'latin-1 '""", io)
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', TRUE
#                 """.format(task)
#                 pg_cur.execute(sql)
#     except Exception, error:
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', FALSE
#                 """.format(task)
#                 pg_cur.execute(sql)
#         ops.email_error(task, 123, error)
#     finally:
#         if ads_con:
#             ads_con.close()
#         if pg_con:
#             pg_con.close()


# def dim_service_writer():
#     pg_con = None
#     ads_con = None
#     task = 'ext_dim_service_writer'
#     try:

#         file_name = 'files/ext_dim_service_writer.csv'
#         with db_cnx.ads_dds() as ads_con:
#             with ads_con.cursor() as ads_cur:
#                 sql = """
#                 SELECT ServiceWriterKey,StoreCode,EmployeeNumber,Name,Description,WriterNumber,dtUserName,
#                     active,DefaultServiceType,CensusDept,CurrentRow,RowChangeDate,RowChangeDateKey,
#                     RowFromTS,RowThruTS,RowChangeReason,ServiceWriterKeyFromDate,ServiceWriterKeyFromDateKey,
#                     ServiceWriterKeyThruDate,ServiceWriterKeyThruDateKey
#                 from dimServiceWriter
#                 """
#                 ads_cur.execute(sql)
#                 with open(file_name, 'wb') as f:
#                     csv.writer(f).writerows(ads_cur.fetchall())
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 pg_cur.execute("truncate ads.ext_dim_service_writer")
#                 with open(file_name, 'r') as io:
#                     pg_cur.copy_expert("""copy ads.ext_dim_service_writer from stdin
#                         with csv encoding 'latin-1 '""", io)
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', TRUE
#                 """.format(task)
#                 pg_cur.execute(sql)
#     except Exception, error:
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', FALSE
#                 """.format(task)
#                 pg_cur.execute(sql)
#         ops.email_error(task, 123, error)
#     finally:
#         if ads_con:
#             ads_con.close()
#         if pg_con:
#             pg_con.close()


# def dim_service_type():
#     pg_con = None
#     ads_con = None
#     task = 'ext_dim_service_type'
#     try:

#         file_name = 'files/ext_dim_service_writer.csv'
#         with db_cnx.ads_dds() as ads_con:
#             with ads_con.cursor() as ads_cur:
#                 sql = """
#                 SELECT * from dimservicetype
#                 """
#                 ads_cur.execute(sql)
#                 with open(file_name, 'wb') as f:
#                     csv.writer(f).writerows(ads_cur.fetchall())
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 pg_cur.execute("truncate ads.ext_dim_service_type")
#                 with open(file_name, 'r') as io:
#                     pg_cur.copy_expert("""copy ads.ext_dim_service_type from stdin
#                         with csv encoding 'latin-1 '""", io)
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', TRUE
#                 """.format(task)
#                 pg_cur.execute(sql)
#     except Exception, error:
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', FALSE
#                 """.format(task)
#                 pg_cur.execute(sql)
#         ops.email_error(task, 123, error)
#     finally:
#         if ads_con:
#             ads_con.close()
#         if pg_con:
#             pg_con.close()


# def dim_payment_type():
#     pg_con = None
#     ads_con = None
#     task = 'ext_dim_payment_type'
#     try:

#         file_name = 'files/ext_dim_payment_type.csv'
#         with db_cnx.ads_dds() as ads_con:
#             with ads_con.cursor() as ads_cur:
#                 sql = """
#                 SELECT * from dimpaymenttype
#                 """
#                 ads_cur.execute(sql)
#                 with open(file_name, 'wb') as f:
#                     csv.writer(f).writerows(ads_cur.fetchall())
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 pg_cur.execute("truncate ads.ext_dim_payment_type")
#                 with open(file_name, 'r') as io:
#                     pg_cur.copy_expert("""copy ads.ext_dim_payment_type from stdin
#                         with csv encoding 'latin-1 '""", io)
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', TRUE
#                 """.format(task)
#                 pg_cur.execute(sql)
#     except Exception, error:
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', FALSE
#                 """.format(task)
#                 pg_cur.execute(sql)
#         ops.email_error(task, 123, error)
#     finally:
#         if ads_con:
#             ads_con.close()
#         if pg_con:
#             pg_con.close()


# def dim_ro_status():
#     pg_con = None
#     ads_con = None
#     task = 'ext_dim_ro_status'
#     try:

#         file_name = 'files/ext_dim_ro_status.csv'
#         with db_cnx.ads_dds() as ads_con:
#             with ads_con.cursor() as ads_cur:
#                 sql = """
#                 SELECT * from dimrostatus
#                 """
#                 ads_cur.execute(sql)
#                 with open(file_name, 'wb') as f:
#                     csv.writer(f).writerows(ads_cur.fetchall())
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 pg_cur.execute("truncate ads.ext_dim_ro_status")
#                 with open(file_name, 'r') as io:
#                     pg_cur.copy_expert("""copy ads.ext_dim_ro_status from stdin
#                         with csv encoding 'latin-1 '""", io)
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', TRUE
#                 """.format(task)
#                 pg_cur.execute(sql)
#     except Exception, error:
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', FALSE
#                 """.format(task)
#                 pg_cur.execute(sql)
#         ops.email_error(task, 123, error)
#     finally:
#         if ads_con:
#             ads_con.close()
#         if pg_con:
#             pg_con.close()


def bs_appointments():
    pg_con = None
    ads_con = None
    task = 'ext_bs_appointments'
    try:
        file_name = 'files/ext_bs_appointments.csv'
        with db_cnx.ads_bs() as ads_con:
            with ads_con.cursor() as ads_cur:
                sql = """
                    SELECT *
                    from bsappointmenthistory
                """
                ads_cur.execute(sql)
                with open(file_name, 'wb') as f:
                    csv.writer(f).writerows(ads_cur.fetchall())
        with db_cnx.new_pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate ads.ext_bs_appointment_history")
                with open(file_name, 'r') as io:
                    pg_cur.copy_expert("""copy ads.ext_bs_appointment_history from stdin 
                        with csv encoding 'latin-1 '""", io)
                sql = """
                    insert into ops.ads_extract (the_date, task, complete)
                    select current_date, '{}', TRUE
                """.format(task)
                pg_cur.execute(sql)
    except Exception, error:
        with db_cnx.new_pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    insert into ops.ads_extract (the_date, task, complete)
                    select current_date, '{}', FALSE
                """.format(task)
                pg_cur.execute(sql)
        ops.email_error(task, 123, error)
    finally:
        if ads_con:
            ads_con.close()
        if pg_con:
            pg_con.close()


def scheduler_appointments():
    pg_con = None
    ads_con = None
    task = 'ext_scheduler_appointments'
    try:
        file_name = 'files/ext_scheduler_appointments.csv'
        with db_cnx.ads_service_scheduler() as ads_con:
            with ads_con.cursor() as ads_cur:
                sql = """
                    SELECT appointmentid,created,createdbyid
                    FROM appointments
                    WHERE CAST(created AS sql_date) > '09/30/2019'
                """
                ads_cur.execute(sql)
                with open(file_name, 'wb') as f:
                    csv.writer(f).writerows(ads_cur.fetchall())
        with db_cnx.new_pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate ads.ext_ron_appointments")
                with open(file_name, 'r') as io:
                    pg_cur.copy_expert(
                        """copy ads.ext_ron_appointments from stdin with csv encoding 'latin-1 '""", io)
                sql = """
                    insert into ops.ads_extract (the_date, task, complete)
                    select current_date, '{}', TRUE
                """.format(task)
                pg_cur.execute(sql)
    except Exception, error:
        with db_cnx.new_pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    insert into ops.ads_extract (the_date, task, complete)
                    select current_date, '{}', FALSE
                """.format(task)
                pg_cur.execute(sql)
        ops.email_error(task, 123, error)
    finally:
        if ads_con:
            ads_con.close()
        if pg_con:
            pg_con.close()


# def dim_ccc():
#     pg_con = None
#     ads_con = None
#     task = 'ext_dim_ccc'
#     try:
#         file_name = 'files/ext_dim_ccc.csv'
#         clean_file_name = 'files/ext_dim_ccc_clean.csv'

#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 sql = """
#                     select max(ccc_key) from dds.dim_ccc;
#                 """
#                 pg_cur.execute(sql)
#                 ccc_key = pg_cur.fetchone()[0]

#         with db_cnx.ads_dds() as ads_con:
#             with ads_con.cursor() as ads_cur:
#                 sql = """
#                     SELECT *
#                     FROM dimccc where ccckey > {};
#                 """.format(ccc_key)
#                 ads_cur.execute(sql)
#                 with open(file_name, 'wb') as f:
#                     csv.writer(f).writerows(ads_cur.fetchall())
#         # https://stackoverflow.com/questions/8689795/how-can-i-remove-non-ascii-characters-but-leave-periods-and-spaces-using-python
#         printable = set(string.printable)
#         with open(clean_file_name, 'wb') as f:
#             with open(file_name, 'rb') as lines:
#                 for line in lines.readlines():
#                     # get rid of the carriage return
#                     line = line.replace('\n', '')
#                     # get rid of the unprintable characters and write it to a file
#                     f.writelines(filter(lambda x: x in printable, line))
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 with open(clean_file_name, 'r') as io:
#                     pg_cur.copy_expert("""copy ads.ext_dim_ccc from stdin with csv encoding 'latin-1 '""", io)
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', TRUE
#                 """.format(task)
#                 pg_cur.execute(sql)
#     except Exception, error:
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', FALSE
#                 """.format(task)
#                 pg_cur.execute(sql)
#         ops.email_error(task, 123, error)
#     finally:
#         if ads_con:
#             ads_con.close()
#         if pg_con:
#             pg_con.close()


# def dim_ro_comment():
#     pg_con = None
#     ads_con = None
#     task = 'ext_dim_ro_comment'
#     try:
#         file_name = 'files/ext_dim_ro_comment.csv'

#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 sql = """
#                     select max(ro_comment_key) from dds.dim_ro_comment;
#                 """
#                 pg_cur.execute(sql)
#                 ro_comment_key = pg_cur.fetchone()[0]

#         with db_cnx.ads_dds() as ads_con:
#             with ads_con.cursor() as ads_cur:
#                 sql = """
#                     SELECT *
#                     FROM dimrocomment where rocommentkey > {};
#                 """.format(ro_comment_key)
#                 ads_cur.execute(sql)
#                 with open(file_name, 'wb') as f:
#                     csv.writer(f).writerows(ads_cur.fetchall())

#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 with open(file_name, 'r') as io:
#                     pg_cur.copy_expert("""copy ads.ext_dim_ro_comment from stdin with csv encoding 'latin-1 '""", io)
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', TRUE
#                 """.format(task)
#                 pg_cur.execute(sql)
#     except Exception, error:
#         with db_cnx.new_pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 sql = """
#                     insert into ops.ads_extract (the_date, task, complete)
#                     select current_date, '{}', FALSE
#                 """.format(task)
#                 pg_cur.execute(sql)
#         ops.email_error(task, 123, error)
#     finally:
#         if ads_con:
#             ads_con.close()
#         if pg_con:
#             pg_con.close()


def main():
    try:
        #     dim_tech()
        #     dim_service_writer()
        #     dim_service_type()
        #     dim_payment_type()
        #     dim_ro_status()
        bs_appointments()
        scheduler_appointments()
        # dim_ccc()
        # dim_ro_comment()
    except Exception, error:
        print(error)


if __name__ == '__main__':
    main()
