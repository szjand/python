# coding=utf-8
import db_cnx
import csv
import ops
"""
-- bspp_xfm_fact_clock_hours
sls_xfm_fact_clock_hours
ads_ext_fact_vehicle_sale
ads_ext_fact_repair_order
ads_ext_dim_vehicle :: 6/26/19 moved to service_tables.py
ads_ext_dim_service_writer
ads_ext_dim_opcode
ads_ext_dim_customer
ads_ext_dim_car_deal_info
ads.ext_edw_employee_dim -- 4/16/18
ads.ext_pto_requests: 11/30/2019
afton.ext_pto_compli_users: 01/17/20

1/7/22
    removed b spp_xfm_fact_clock_hours, clock hours now come from ukg.clock_hours
"""
pg_con = None
ads_con = None
run_id = None
pg_server = '173'


# task = 'sls_xfm_fact_clock_hours'
# try:
#     file_name = 'files/sls_xfm_fact_clock_hours.csv'
#     with db_cnx.ads_dds() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                     SELECT c.employeenumber,b.thedate,clockhours,regularhours,overtimehours,
#                       vacationhours,ptohours,holidayhours
#                     from edwClockHoursFact a
#                     inner join day b on a.datekey = b.datekey
#                       -- and b.thedate between curdate() - 40 and curdate()
#                       and b.thedate between '04/01/2017' and curdate()
#                     inner join edwEmployeeDim c on a.employeekey = c.employeekey
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 delete
#                 from sls.xfm_fact_clock_hours
#                 where the_date between '04/01/2017' and current_date
#             """
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy sls.xfm_fact_clock_hours from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, 'sls_xfm_fact_clock_hours', TRUE
#             """
#             pg_cur.execute(sql)
#     print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss sls_xfm_fact_clock_hours'
# except Exception, error:
#     ops.email_error(task, 'ads_for_luigi', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, 'sls_xfm_fact_clock_hours', FALSE
#             """
#             pg_cur.execute(sql)

#  ads_ext_fact_vehicle_sale
task = 'ads_ext_fact_vehicle_sale'
try:
    file_name = 'files/vehicle_sale.csv'
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                from factvehiclesale
                where stocknumber is not null
                  and stocknumber <> ''
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_fact_vehicle_sale")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_fact_vehicle_sale from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ext_fact_vehicle_sale', TRUE
            """
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss ads_ext_fact_vehicle_sale'
except Exception, error:
    ops.email_error(task, 'ads_for_luigi', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ext_fact_vehicle_sale', FALSE
            """
            pg_cur.execute(sql)

#  ads_ext_fact_repair_order
task = 'ads_ext_fact_repair_order'
try:
    file_name = 'files/fact_repair_order.csv'
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT a.*
                from factrepairorder a
                inner join day b on a.opendatekey = b.datekey
                where b.thedate > curdate() - 365
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_fact_repair_order_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_fact_repair_order_tmp from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                delete
                from ads.ext_fact_repair_order
                where ro in (
                  select ro
                  from ads.ext_fact_repair_order_tmp);
            """
            pg_cur.execute(sql)
            sql = """
                insert into ads.ext_fact_repair_order
                select * from ads.ext_fact_repair_order_tmp
            """
            pg_cur.execute(sql)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_fact_repair_order', TRUE
                    """
            pg_cur.execute(sql)
        print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss ads_ext_fact_repair_order'
except Exception, error:
    ops.email_error(task, 'ads_for_luigi', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_fact_repair_order', FALSE
            """
            pg_cur.execute(sql)


# ads_ext_dim_vehicle
task = 'ads_ext_dim_vehicle'
try:
    file_name = 'files/dim_vehicle.csv'
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                select * from dimvehicle
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_dim_vehicle")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_dim_vehicle from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_vehicle', TRUE
            """
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss ads_ext_dim_vehicle'
except Exception, error:
    ops.email_error(task, 'ads_for_luigi', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_vehicle', FALSE
            """
            pg_cur.execute(sql)

#  ads_ext_dim_service_writer
task = 'ads_ext_dim_service_writer'
try:
    file_name = 'files/dim_service_writer.csv'
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT ServiceWriterKey,StoreCode,EmployeeNumber,Name,Description,WriterNumber,dtUserName,
                    active,DefaultServiceType,CensusDept,CurrentRow,RowChangeDate,RowChangeDateKey,
                    RowFromTS,RowThruTS,RowChangeReason,ServiceWriterKeyFromDate,ServiceWriterKeyFromDateKey,
                    ServiceWriterKeyThruDate,ServiceWriterKeyThruDateKey
                from dimServiceWriter
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_dim_service_writer")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_dim_service_writer from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_service_writer', TRUE
            """
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss ads_ext_dim_service_writer'
except Exception, error:
    ops.email_error(task, 'ads_for_luigi', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_service_writer', FALSE
            """
#             pg_cur.execute(sql)

#  ads_ext_dim_opcode
task = 'ads_ext_dim_opcode'
try:
    file_name = 'files/dim_opcode.csv'
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT opcodekey,storecode,opcode,
                  case when description = '' then 'NONE' else description end,
                  pdqcat1,pdqcat2,pdqcat3
                from dimopcode
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_dim_opcode")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_dim_opcode from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_opcode', TRUE
            """
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss ads_ext_dim_opcode'
except Exception, error:
    ops.email_error(task, 'ads_for_luigi', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_opcode', FALSE
            """
            pg_cur.execute(sql)

#  ads_ext_dim_customer
task = 'ads_ext_dim_customer'
try:
    file_name = 'files/dim_customer.csv'
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                select CustomerKey, StoreCode,BNKEY,CustomerTypeCode,
                  CustomerType,FullName,LastName,FirstName,MiddleName,
                  HomePhone,BusinessPhone,CellPhone,Email,
                  emailValid,email2,email2Valid,City,County,State,
                  Zip,HasValidEmail,do_not_call
                from dimcustomer
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_dim_customer")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_dim_customer from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_customer', TRUE
            """
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss ads_ext_dim_customer'
except Exception, error:
    ops.email_error(task, 'ads_for_luigi', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_customer', FALSE
            """
            pg_cur.execute(sql)

#  ads_ext_dim_car_deal_info
task = 'ads_ext_dim_car_deal_info'
try:
    file_name = 'files/dim_car_deal_info.csv'
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT * from dimcardealinfo
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_dim_car_deal_info")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_dim_car_deal_info from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_car_deal_info', TRUE
            """
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss ads_ext_dim_car_deal_info'
except Exception, error:
    ops.email_error(task, 'ads_for_luigi', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_car_deal_info', FALSE
            """
            pg_cur.execute(sql)

#  ads_ext_edw_employee_dim
task = 'ads_ext_edw_employee_dim'
try:
    file_name = 'files/edw_employee_dim.csv'
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT * from edwEmployeeDim
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_edw_employee_dim")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_edw_employee_dim from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_edw_employee_dim', TRUE
            """
            pg_cur.execute(sql)
    print ('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss ads_ext_edw_employee_dim')
except Exception, error:
    ops.email_error(task, 'ads_for_luigi', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_edw_employee_dim', FALSE
            """
            pg_cur.execute(sql)



finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
