# coding=utf-8
import db_cnx
import csv
import ops

pg_con = None
ads_con = None
pg_server = '173'

#  ext_vehicle_sales
task = 'uc_inventory_ext_vehicle_sales'
try:
    file_name = 'files/ext_vehicle_sales.csv'
    table_name = 'ads.ext_vehicle_sales'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT *
            from vehiclesales
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
