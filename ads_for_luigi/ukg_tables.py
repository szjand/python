# coding=utf-8
import db_cnx
import csv
import ops
"""
1/2/22
    nightly copy of postgresql tables ukg.employees & ukg.clock_hours to advantage db ukg tables employees and
        clock_hours
"""

pg_con = None
ads_con = db_cnx.ads_ukg()
ads_cur = ads_con.cursor()
run_id = None
pg_server = '173'

# # ---------------------------------------------------------
# works
# task = 'employees'
# try:
file_name = 'files/ukg_employees.csv'
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        sql = """
            select *
            from ukg.employees;
        """
        pg_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(pg_cur.fetchall())
with db_cnx.ads_ukg() as ads_con:
    with ads_con.cursor() as ads_cur:
        ads_cur.execute("delete from employees")
        with open(file_name, 'r') as io:
            for row in csv.reader(io):
                sql = """"
                    insert into employees values('{}','{}','{}','{}','{}',{},{},'{}','{}','{}','{}','{}','{}','{}',
                    '{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')
                """.format(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10],
                           row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18], row[19], row[20],
                           row[21], row[22], row[23], row[24], row[25], row[26], row[27], row[28], row[29])
                sql = sql.replace('\n', "")    # remove new lines, not actually required, insert works without it
                sql = sql.replace('"', '')    # remove weird initial "
                sql = sql.replace(",'O'", ",'O''")  # fix O'Neil, etc
                ads_cur.execute(sql)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, 'ukg employees', TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'ukg.employees to ADS', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, 'ukg employees', FALSE
#             """
#             pg_cur.execute(sql)
# # ---------------------------------------------------------

# task = 'clock_hours'
# try:
    file_name = 'files/ukg_clock_hours.csv'
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select *
                from ukg.clock_hours
                where the_date between current_date - 31 and current_date - 1;
            """
            pg_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(pg_cur.fetchall())
    with db_cnx.ads_ukg() as ads_con:
        with ads_con.cursor() as ads_cur:
            ads_cur.execute("delete from clock_hours where the_date between curdate() - 31 and curdate() - 1;")
            with open(file_name, 'r') as io:
                for row in csv.reader(io):
                    sql = """"
                        insert into clock_hours values('{}','{}','{}',{},'{}','{}',{},{},{},{},{},{});
                    """.format(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10],
                               row[11])
                    sql = sql.replace('\n', "")    # remove new lines, not actually required, insert works without it
                    sql = sql.replace('"', '')    # remove weird initial "
                    sql = sql.replace(",'O'", ",'O''")  # fix O'Neil, etc
                    ads_cur.execute(sql)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, 'ukg clock hours', TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'ukg.clock_hours to ADS', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, 'ukg clock hours', FALSE
#             """
#             pg_cur.execute(sql)
