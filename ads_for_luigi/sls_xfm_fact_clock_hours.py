# coding=utf-8
import db_cnx
import csv

task = 'sls_xfm_fact_clock_hours'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/sls_xfm_fact_clock_hours.csv'
pg_server = '174'
try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                    SELECT c.employeenumber,b.thedate,clockhours,regularhours,overtimehours,
                      vacationhours,ptohours,holidayhours
                    from edwClockHoursFact a
                    inner join day b on a.datekey = b.datekey
                      and b.thedate between curdate() - 40 and curdate()
                      -- and b.thedate between '04/01/2017' and curdate()
                    inner join edwEmployeeDim c on a.employeekey = c.employeekey
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                delete
                from sls.xfm_fact_clock_hours
                -- where the_date between '04/01/2017' and current_date
                where the_date between current_date - 40 and current_date
            """
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy sls.xfm_fact_clock_hours from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'sls_xfm_fact_clock_hours', TRUE
            """
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'sls_xfm_fact_clock_hours', FALSE
            """
            pg_cur.execute(sql)
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
