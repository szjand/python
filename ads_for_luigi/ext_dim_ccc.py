# coding=utf-8
import db_cnx
import csv
import string

"""
these are known rows with unprintable characters
    and ccckey not in(815874,854670,872441,892323,895046,917231,930918,935996,937375,
    942634,1119214,1296498,1297293,1297624,1299274,1299854,1300021,1300917,1302131,1304059,1304121,1304361,
    1304505,1304731,1305041);
"""
task = 'ext_dim_ccc'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_dim_ccc.csv'
clean_file_name = 'files/ext_dim_ccc_clean.csv'
# test_file_name = 'files/ext_dim_ccc_test.csv'
pg_server = '173'

try:
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select max(ccc_key) from dds.dim_ccc;
            """
            pg_cur.execute(sql)
            ccc_key = pg_cur.fetchone()[0]

    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                FROM dimccc where ccckey > {};
            """.format(ccc_key)
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())

    # https://stackoverflow.com/questions/8689795/how-can-i-remove-non-ascii-characters-but-leave-periods-and-spaces-using-python
    printable = set(string.printable)
    with open(clean_file_name, 'wb') as f:
        with open(file_name, 'rb') as lines:
            for line in lines.readlines():
                # get rid of the carriage return
                line = line.replace('\n', '')
                # get rid of the unprintable characters and write it to a file
                f.writelines(filter(lambda x: x in printable, line))


    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            with open(clean_file_name, 'r') as io:
                    pg_cur.copy_expert("""copy ads.ext_dim_ccc from stdin with csv encoding 'latin-1 '""", io)






# except Exception, error:
#     print error
except Exception as error:
    print(error)
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
