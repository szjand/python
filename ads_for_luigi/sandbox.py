# # coding=utf-8
# import string
#
# the_string = '[WILL NOT BLOW AIR - CAN FEEL COLD AIR COMING OUT A LITTLE BUT BLOWER FAN WILL NOT MOVE.� WORKED FOR 2 DAYS THEN QUIT. CHECK AND ADVISE.� REF RO 16402846]'
#
# print(the_string)
# printable = set(string.printable)
# print(filter(lambda x: x in printable, the_string))

# prints a comma separated list of '' demarked lines
# writes the file with a carriage return after each line




# coding=utf-8
import db_cnx
import csv
import string

task = 'ext_dim_ccc'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/bad_file.csv'
clean_file_name = 'files/clean_file.csv'

pg_server = '173'

try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            # these record all contain at least one field with non printable characters
            # (815874, 854670, 872441, 892323, 895046, 917231, 930918, 935996, 937375,
            #  942634, 1119214, 1296498, 1297293, 1297624, 1299274, 1299854, 1300021, 1300917, 1302131, 1304059,
            #  1304121, 1304361, 1304505, 1304731, 1305041);
            sql = """
                SELECT *
                FROM dimccc where ccckey in (1305041,1304505,1304731);
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())

    with open(clean_file_name, 'wb') as f:
        with open(file_name, 'rb') as lines:
            for line in lines.readlines():
                f.writelines(line.replace('\n', ' ').replace('\r', ' '))
                print(line)

except Exception as error:
    print(error)
finally:
    if ads_con:
        ads_con.close()
