# coding=utf-8
import db_cnx
import csv

task = 'ext_appeal_matrix_elements'
pg_con = None
ads_con = None
run_id = None
file_name = 'ext_appeal_matrix_elements.csv'
pg_server = '173'

try:
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                select *
                FROM appealmatrixelements
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_appeal_matrix_elements")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_appeal_matrix_elements from stdin with csv encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()