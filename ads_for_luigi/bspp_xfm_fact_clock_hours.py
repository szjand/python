# coding=utf-8
import db_cnx
import csv

task = 'bspp_xfm_fact_clock_hours'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/bspp_xfm_fact_clock_hours.csv'
pg_server = '174'
try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                    SELECT c.employeenumber,b.thedate,clockhours,regularhours,overtimehours,
                      vacationhours,ptohours,holidayhours
                    from edwClockHoursFact a
                    inner join day b on a.datekey = b.datekey
                      and b.thedate between curdate() -18 and curdate()
                    inner join edwEmployeeDim c on a.employeekey = c.employeekey
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate bspp.xfm_fact_clock_hours")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy bspp.xfm_fact_clock_hours from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'bspp_xfm_fact_clock_hours', TRUE
            """
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'bspp_xfm_fact_clock_hours', FALSE
            """
            pg_cur.execute(sql)
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
