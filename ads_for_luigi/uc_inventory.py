# coding=utf-8
import db_cnx
import csv
import ops
"""
9/25/17:
  vehicleInventoryItemStatuses: change to full scrape (back thru 2011)
11/13/17
    add vehicleevaluations
01/17/18
    add dds.dimsalesperson
    add dps.people
01/18/18
    add dps.vehicleItemStatuses

ext_vehicle_items
ext_vehicle_inventory_items
ext_vehicle_pricings
ext_vehicle_pricing_details
ext_make_model_classifications
ext_vehicle_inventory_item_statuses
ext_vehicle_item_mileages
ext_vehicle_sales
ext_vehicle_evaluations
ext_vehicle_acquisitions
ext_dim_salesperson
ext_people
ext_vehicle_item_statuses
8/11/18
ext_selected_recon_packages
ext_recon_authorizations
ext_vehicle_recon_items
ext_authorized_recon_items
ext_vehicle_inventory_item_notes
8/21/18
ext_vehicle_third_party_values
9/9/18
ext_vehicle_due_bill_authorizations
ext_vehicle_due_bill_authorized_items
12/28/19
ext_vehicle_walks
01/21/20
ext_parts_orders
1/22/20
ext_vehicles_to_move
04/13/20: updated selectedreconpackages
05/27/21: vehicle_price_components, appeal_scorecards, acvs
03/04/22: ext_vehicle_inventory_consignments, ext organizations
05/11/22: crayon report: ext_party_relationships
"""
pg_con = None
ads_con = None
pg_server = '173'

# #  ext_organizations
# task = 'uc_inventory_ext_organizations'
# try:
#     file_name = 'files/ext_organizations.csv'
#     table_name = 'ads.ext_organizations'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT partyid,thruts,fullname,name,fromts
#                 from organizations
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
#
# #  ext_vehicle_inventory_consignments
# task = 'uc_inventory_ext_vehicle_inventory_consignments'
# try:
#     file_name = 'files/ext_vehicle_inventory_consignments.csv'
#     table_name = 'ads.ext_vehicle_inventory_consignments'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT vehicleinventoryitemid,consignmentperiodenddate,consignedbyid,fromts,thruts,
#                 consignedlocationid,vehicleitemid,consignmentreceivedts,consignmentreturnedts,dispositiontyp
#                 from vehicleinventoryconsignments
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# #  ext_vehicle_items
# task = 'uc_inventory_ext_vehicle_items'
# try:
#     file_name = 'files/ext_vehicle_items.csv'
#     table_name = 'ads.ext_vehicle_items'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT vehicleitemid,vin,bodystyle,trim,interiorcolor,exteriorcolor,engine,
#                   transmission,make,model,yearmodel,vinresolved
#                 from vehicleitems
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# #  ext_vehicle_inventory_items
# task = 'uc_inventory_ext_vehicle_inventory_items'
# try:
#     file_name = 'files/ext_vehicle_inventory_items.csv'
#     table_name = 'ads.ext_vehicle_inventory_items'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#             SELECT vehicleinventoryitemid,tablekey,basistable,stocknumber,fromts,
#               coalesce(thruts, cast('12/31/9999 01:00:00' AS sql_timestamp)),
#               locationid,vehicleitemid,bookerid,currentpriority,owninglocationid
#             from vehicleinventoryitems
#             """
#             ads_cur.execute(sql)
#             # with open(file_name, 'wb') as f:
#             #     csv.writer(f).writerows(ads_cur.fetchall())
#             with open(file_name, 'w') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             # sql = """
#             #     insert into ops.ads_extract (the_date, task, complete)
#             #     select current_date, """ + "'" + task + "'" + """ , TRUE
#             # """
#             # pg_cur.execute(sql)
# except Exception:
#     raise
# # except Exception, error:
# #     ops.email_error(task, 'uc_inventory', error)
# #     with db_cnx.new_pg(pg_server) as pg_con:
# #         with pg_con.cursor() as pg_cur:
# #             sql = """
# #                 insert into ops.ads_extract (the_date, task, complete)
# #                 select current_date, """ + "'" + task + "'" + """ , FALSE
# #             """
# #             pg_cur.execute(sql)
#
# #  ext_vehicle_pricings
# task = 'uc_inventory_ext_vehicle_pricings'
# try:
#     file_name = 'files/ext_vehicle_pricings.csv'
#     table_name = 'ads.ext_vehicle_pricings'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#             SELECT *
#             from vehiclepricings
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_pricing_details
# task = 'uc_inventory_ext_vehicle_pricing_details'
# try:
#     file_name = 'files/ext_vehicle_pricing_details.csv'
#     table_name = 'ads.ext_vehicle_pricing_details'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#             SELECT *
#             from vehiclepricingdetails
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# #  ext_make_model_classifications
# task = 'uc_inventory_ext_make_model_classifications'
# try:
#     file_name = 'files/ext_make_model_classifications.csv'
#     table_name = 'ads.ext_make_model_classifications'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#             SELECT make,model,vehiclesegment,vehicletype,luxury,commercial,
#               sport,fromts, coalesce(thruts, cast('12/31/9999 01:00:00' as sql_timestamp)),
#               mfgorigintyp
#             from MakeModelClassifications
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_inventory_item_statuses
# task = 'uc_inventory_ext_vehicle_inventory_item_statuses'
# try:
#     file_name = 'files/ext_vehicle_inventory_item_statuses.csv'
#     table_name = 'ads.ext_vehicle_inventory_item_statuses'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#             SELECT vehicleinventoryitemid,status,category,fromts,
#               coalesce(thruts, cast('12/31/9999 01:00:00' as sql_timestamp)),
#               basistable,tablekey,userid
#             from vehicleinventoryitemstatuses
#             where year(cast(fromts as sql_date)) > 2010
#             --where fromts >= (
#             --  select min(fromts)
#             --  from vehicleinventoryitemstatuses
#             --  where thruts is null)
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# #  ext_vehicle_item_mileages
# task = 'uc_inventory_ext_vehicle_item_mileages'
# try:
#     file_name = 'files/ext_vehicle_item_mileages.csv'
#     table_name = 'ads.ext_vehicle_item_mileages'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#             SELECT *
#             from vehicleitemmileages
#             where basistable <> 'BodyShopJobs'
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# #  ext_vehicle_sales
# task = 'uc_inventory_ext_vehicle_sales'
# try:
#     file_name = 'files/ext_vehicle_sales.csv'
#     table_name = 'ads.ext_vehicle_sales'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#             SELECT *
#             from vehiclesales
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_evaluations
# task = 'uc_inventory_ext_vehicle_evaluations'
# try:
#     file_name = 'files/ext_vehicle_evaluations.csv'
#     table_name = 'ads.ext_vehicle_evaluations'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 from vehicleevaluations
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_acquisitions
# task = 'uc_inventory_ext_vehicle_acquisitions'
# try:
#     file_name = 'files/ext_vehicle_acquisitions.csv'
#     table_name = 'ads.ext_vehicle_acquisitions'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 from vehicleacquisitions
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_dim_salesperson
# task = 'uc_inventory_ext_dim_salesperson'
# try:
#     file_name = 'files/ext_dim_salesperson.csv'
#     table_name = 'ads.ext_dim_salesperson'
#     with db_cnx.ads_dds() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 from dimsalesperson
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
#
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_people
# task = 'uc_inventory_ext_people'
# try:
#     file_name = 'files/ext_people.csv'
#     table_name = 'ads.ext_people'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 from people
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
#
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_item_statuses
# task = 'uc_inventory_ext_vehicle_item_statuses'
# try:
#     file_name = 'files/ext_vehicle_item_statuses.csv'
#     table_name = 'ads.ext_vehicle_item_statuses'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from VehicleItemStatuses
#                 where cast(fromts as sql_date) >= '01/01/2017'
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             # sql = """ truncate """ + table_name
#             sql = """ delete from """ + table_name + """ where extract(year from fromts) > 2016 """
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_selected_recon_packages
# task = 'uc_inventory_ext_selected_recon_packages'
# try:
#     file_name = 'files/ext_selected_recon_packages.csv'
#     table_name = 'ads.ext_selected_recon_packages'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from selectedreconpackages
#                 where cast(selectedReconPackageTS as sql_date) >= '01/01/2020'
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = "delete from " + table_name + " where selectedReconPackageTS::date >= '01/01/2020'"
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_recon_authorizations
# task = 'uc_inventory_ext_recon_authorizations'
# try:
#     file_name = 'files/ext_recon_authorizations.csv'
#     table_name = 'ads.ext_recon_authorizations'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from reconauthorizations
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = " truncate " + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_recon_items
# task = 'uc_inventory_ext_vehicle_recon_items'
# try:
#     file_name = 'files/ext_vehicle_recon_items.csv'
#     table_name = 'ads.ext_vehicle_recon_items'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from vehiclereconitems
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = " truncate " + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_authorized_recon_items
# task = 'uc_inventory_ext_authorized_recon_items'
# try:
#     file_name = 'files/ext_authorized_recon_items.csv'
#     table_name = 'ads.ext_authorized_recon_items'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from authorizedreconitems
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = " truncate " + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_inventory_item_notes
# task = 'uc_inventory_ext_vehicle_inventory_item_notes'
# try:
#     file_name = 'files/ext_vehicle_inventory_item_notes.csv'
#     table_name = 'ads.ext_vehicle_inventory_item_notes'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from vehicleinventoryitemnotes
#                 where cast(notesTS as sql_date) >= '01/01/2017'
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = " truncate " + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_third_party_values
# task = 'uc_inventory_ext_vehicle_third_party_values'
# try:
#     file_name = 'files/ext_vehicle_third_party_values.csv'
#     table_name = 'ads.ext_vehicle_third_party_values'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from vehiclethirdpartyvalues
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = " truncate " + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_due_bill_authorizations
# task = 'uc_inventory_ext_vehicle_due_bill_authorizations'
# try:
#     file_name = 'files/ext_vehicle_due_bill_authorizations.csv'
#     table_name = 'ads.ext_vehicle_due_bill_authorizations'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT vehicleduebillauthorizationid,vehiclesaleid,vehicleitemid,fromts,thruts
#                 from vehicleduebillauthorizations
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = " truncate " + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_due_bill_authorized_items
# task = 'uc_inventory_ext_vehicle_due_bill_authorized_items'
# try:
#     file_name = 'files/ext_vehicle_due_bill_authorized_items.csv'
#     table_name = 'ads.ext_vehicle_due_bill_authorized_items'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT vehicleduebillauthorizationid,vehiclereconitemid,typ,status,startts,thruts
#                 from vehicleduebillauthorizeditems
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = " truncate " + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
# # ext_vehicle_walks
# task = 'uc_inventory_ext_vehicle_walks'
# try:
#     file_name = 'files/uc_inventory_ext_vehicle_walks.csv'
#     table_name = 'ads.ext_vehicle_walks'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 from vehiclewalks
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
#
# # ext_parts_orders
# task = 'ext_parts_orders'
# try:
#     file_name = 'files/ext_parts_orders.csv'
#     table_name = 'ads.ext_parts_orders'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 FROM partsorders
#                 WHERE CAST(requestedts AS sql_date) > '12/31/2018'
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
#
# # ext_vehicles_to_move
# task = 'ext_vehicles_to_move'
# try:
#     file_name = 'files/ext_vehicles_to_move.csv'
#     table_name = 'ads.ext_vehicles_to_move'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 FROM vehiclestomove
#                 WHERE cast(fromts AS sql_date) > '12/31/2018'
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
#
# ext_vehicles_price_components
task = 'ext_vehicle_price_components'
try:
    file_name = 'files/ext_vehicle_price_components.csv'
    table_name = 'ads.ext_vehicle_price_components'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            # sql = """
            #     select *
            #     FROM vehiclepricecomponents
            #     where cast(fromts as sql_date) > curdate() -365
            # """
            sql = """
                select *
                FROM vehiclepricecomponents
                where cast(fromts as sql_date) > '12/31/2017'
            """
            ads_cur.execute(sql)
            # with open(file_name, 'wb') as f:
            #     csv.writer(f).writerows(ads_cur.fetchall()
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            # sql = """
            #     insert into ops.ads_extract (the_date, task, complete)
            #     select current_date, """ + "'" + task + "'" + """ , TRUE
            # """
            # pg_cur.execute(sql)
except Exception:
    raise
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
#
# # ext_appeal_score_cards
# task = 'ext_appeal_scorecards'
# try:
#     file_name = 'files/ext_appeal_scorecards.csv'
#     table_name = 'ads.ext_appeal_scorecards'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select a.*
#                 FROM appealscorecards a
#                 JOIN VehicleEvaluations b on a.tablekey = b.VehicleEvaluationid
#                   AND cast(b.VehicleEvaluationts AS sql_date) > curdate() - 365
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)
#
#
# # ext_acvs
# task = 'ext_acvs'
# try:
#     file_name = 'files/ext_acvs.csv'
#     table_name = 'ads.ext_acvs'
#     with db_cnx.ads_dps() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select a.*
#                 FROM acvs a
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """ truncate """ + table_name
#             pg_cur.execute(sql)
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , TRUE
#             """
#             pg_cur.execute(sql)
# except Exception, error:
#     ops.email_error(task, 'uc_inventory', error)
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, """ + "'" + task + "'" + """ , FALSE
#             """
#             pg_cur.execute(sql)

finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()


