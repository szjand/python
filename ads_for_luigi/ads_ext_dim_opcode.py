# coding=utf-8
import db_cnx
import csv

task = 'ads_ext_dim_opcode'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/dim_opcode.csv'
pg_server = '174'

try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT opcodekey,storecode,opcode,
                  case when description = '' then 'NONE' else description end,
                  pdqcat1,pdqcat2,pdqcat3
                from dimopcode
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_dim_opcode")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_dim_opcode from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_opcode', TRUE
            """
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_dim_opcode', FALSE
            """
            pg_cur.execute(sql)
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
