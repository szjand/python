# coding=utf-8
import db_cnx
import csv

file_name = 'files/pto_files.csv'
pg_server = '173'

with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from ptoEmployees """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.employees")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.employees from stdin with csv encoding 'latin-1 '""", io)


with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from pto_employee_pto_allocation """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.employee_pto_allocation")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.employee_pto_allocation from stdin with csv encoding 'latin-1 '""", io)


with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from pto_adjustments """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.adjustments")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.adjustments from stdin with csv encoding 'latin-1 '""", io)


# with db_cnx.ads_sco() as ads_con:
#     with ads_con.cursor() as ads_cur:
#         sql = """ select * from pto_used_sources """
#         ads_cur.execute(sql)
#         with open(file_name, 'wb') as f:
#             csv.writer(f).writerows(ads_cur.fetchall())
# with db_cnx.new_pg(pg_server) as pg_con:
#     with pg_con.cursor() as pg_cur:
#         # pg_cur.execute("truncate pto.used_pto_sources")
#         with open(file_name, 'r') as io:
#             pg_cur.copy_expert("""copy pto.used_pto_sources from stdin with csv encoding 'latin-1 '""", io)


with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from pto_used """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.used_pto")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.used_pto from stdin with csv encoding 'latin-1 '""", io)


with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from pto_requests """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.requests")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.requests from stdin with csv encoding 'latin-1 '""", io)


with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from ptoDepartments """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.departments")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.departments from stdin with csv encoding 'latin-1 '""", io)


with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from ptoPositions """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.positions")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.positions from stdin with csv encoding 'latin-1 '""", io)


with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from ptoDepartmentPositions """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.department_positions")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.department_positions from stdin with csv encoding 'latin-1 '""", io)


with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from ptoPositionFulfillment """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.position_fulfillment")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.position_fulfillment from stdin with csv encoding 'latin-1 '""", io)



with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """ select * from ptoAuthorization """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.authorization")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.authorization from stdin with csv encoding 'latin-1 '""", io)


with db_cnx.ads_sco() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """select * from pto_exclude"""
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        # pg_cur.execute("truncate pto.exclude_employees")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy pto.exclude_employees from stdin with csv encoding 'latin-1 '""", io)


# # this table is populated in real time from luigi module compli.py, don't need data from advantage
# with db_cnx.ads_sco() as ads_con:
#     with ads_con.cursor() as ads_cur:
#         sql = """ select * from pto_compli_users """
#         ads_cur.execute(sql)
#         with open(file_name, 'wb') as f:
#             csv.writer(f).writerows(ads_cur.fetchall())
# with db_cnx.new_pg(pg_server) as pg_con:
#     with pg_con.cursor() as pg_cur:
#         # pg_cur.execute("truncate pto.compli_users")
#         with open(file_name, 'r') as io:
#             pg_cur.copy_expert("""copy pto.compli_users from stdin with csv encoding 'latin-1 '""", io)


# with db_cnx.ads_sco() as ads_con:
#     with ads_con.cursor() as ads_cur:
#         sql = """ select * from pto_categories """
#         ads_cur.execute(sql)
#         with open(file_name, 'wb') as f:
#             csv.writer(f).writerows(ads_cur.fetchall())
# with db_cnx.new_pg(pg_server) as pg_con:
#     with pg_con.cursor() as pg_cur:
#         # pg_cur.execute("truncate pto.categories")
#         with open(file_name, 'r') as io:
#             pg_cur.copy_expert("""copy pto.categories from stdin with csv encoding 'latin-1 '""", io)

