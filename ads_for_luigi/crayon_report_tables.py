# coding=utf-8
import db_cnx
import csv
import ops

"""
  partyRelationships 
  typDescriptions
  vehicleItemOptions
"""

pg_con = None
ads_con = None
run_id = None
pg_server = '173'

# task = 'ext_party_relationships'
# print(task)
# try:
#
#     file_name = 'files/ext_party_relationships.csv'
#     with db_cnx.ads_dpsvseries() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 from partyRelationships
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate ads.ext_party_relationships")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy ads.ext_party_relationships from stdin with csv encoding 'latin-1 '""",
#                                    io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', TRUE
#             """.format(task)
#             pg_cur.execute(sql)
# except Exception, error:
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', FALSE
#             """.format(task)
#             pg_cur.execute(sql)
#     ops.email_error(task, 123, error)
# finally:
#     if ads_con:
#         ads_con.close()
#     if pg_con:
#         pg_con.close()
#
# task = 'ext_typ_descriptions'
# print(task)
# try:
#
#     file_name = 'files/ext_typ_descriptions.csv'
#     with db_cnx.ads_dpsvseries() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 from typDescriptions
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate ads.ext_typ_descriptions")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy ads.ext_typ_descriptions from stdin with csv encoding 'latin-1 '""",
#                                    io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', TRUE
#             """.format(task)
#             pg_cur.execute(sql)
# except Exception, error:
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', FALSE
#             """.format(task)
#             pg_cur.execute(sql)
#     ops.email_error(task, 123, error)
# finally:
#     if ads_con:
#         ads_con.close()
#     if pg_con:
#         pg_con.close()
#
# task = 'ext_vehicle_item_options'
# print(task)
# try:
#
#     file_name = 'files/ext_vehicle_item_options.csv'
#     with db_cnx.ads_dpsvseries() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT  *
#                 from vehicleItemOptions
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate ads.ext_vehicle_item_options")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy ads.ext_vehicle_item_options from stdin with csv encoding 'latin-1 '""",
#                                    io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', TRUE
#             """.format(task)
#             pg_cur.execute(sql)
# except Exception, error:
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', FALSE
#             """.format(task)
#             pg_cur.execute(sql)
#     ops.email_error(task, 123, error)
# finally:
#     if ads_con:
#         ads_con.close()
#     if pg_con:
#         pg_con.close()

# task = 'ext_gross_log_table'
# print(task)
# try:
#
#     file_name = 'files/ext_gross_log_table.csv'
#     with db_cnx.ads_dpsvseries() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT  *
#                 from grosslogtable
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate ads.ext_gross_log_table")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy ads.ext_gross_log_table from stdin with csv encoding 'latin-1 '""",
#                                    io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', TRUE
#             """.format(task)
#             pg_cur.execute(sql)
# except Exception, error:
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', FALSE
#             """.format(task)
#             pg_cur.execute(sql)
#     ops.email_error(task, 123, error)
# finally:
#     if ads_con:
#         ads_con.close()
#     if pg_con:
#         pg_con.close()

# task = 'ext_glump_limits'
# print(task)
# try:
#
#     file_name = 'files/ext_glump_limits.csv'
#     with db_cnx.ads_dpsvseries() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT  *
#                 from glumplimits
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate ads.ext_glump_limits")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy ads.ext_glump_limits from stdin with csv encoding 'latin-1 '""",
#                                    io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', TRUE
#             """.format(task)
#             pg_cur.execute(sql)
# except Exception, error:
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', FALSE
#             """.format(task)
#             pg_cur.execute(sql)
#     ops.email_error(task, 123, error)
# finally:
#     if ads_con:
#         ads_con.close()
#     if pg_con:
#         pg_con.close()
#
task = 'ext_black_book_values'
print(task)
try:

    file_name = 'files/ext_black_book_values.csv'
    with db_cnx.ads_dpsvseries() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT  *
                from blackbookvalues
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_black_book_values")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_black_book_values from stdin with csv encoding 'latin-1 '""",
                                   io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, '{}', TRUE
            """.format(task)
            pg_cur.execute(sql)
except Exception, error:
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, '{}', FALSE
            """.format(task)
            pg_cur.execute(sql)
    ops.email_error(task, 123, error)
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()

# task = 'ext_status_descriptions'
# print(task)
# try:
#
#     file_name = 'files/ext_status_descriptions.csv'
#     with db_cnx.ads_dpsvseries() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 SELECT *
#                 from statusDescriptions
#                 where description <> ''
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate ads.ext_status_descriptions")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy ads.ext_status_descriptions from stdin with csv encoding 'latin-1 '""", io)
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', TRUE
#             """.format(task)
#             pg_cur.execute(sql)
# except Exception, error:
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             sql = """
#                 insert into ops.ads_extract (the_date, task, complete)
#                 select current_date, '{}', FALSE
#             """.format(task)
#             pg_cur.execute(sql)
#     ops.email_error(task, 123, error)
# finally:
#     if ads_con:
#         ads_con.close()
#     if pg_con:
#         pg_con.close()