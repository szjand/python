# coding=utf-8
import db_cnx
import csv

pg_server = '173'
task = 'uc_inventory_ext_selected_recon_packages'
try:
    file_name = 'files/ext_selected_recon_packages.csv'
    table_name = 'ads.ext_selected_recon_packages'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                select *
                from selectedreconpackages
                where cast(selectedReconPackageTS as sql_date) >= '01/01/2020'
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = "delete from " + table_name + " where cast(selectedReconPackageTS as sql_date) >= '01/01/2020'"
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1'""", io)
except Exception, error:
    print(error)
