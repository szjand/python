# coding=utf-8
import db_cnx
import csv
import ops
from datetime import datetime
from random import seed
from random import randint
"""
08/15/22
Yem wants to include VDP Links in the homenet feeds
To do that in used cars it will be necessary to update the req'd tables in postgresql throughout the day in 
anticipation of the feed which occurs as 12:30AM, 11:20AM, 3:20PM & 5:20PM
This will permit me to generate the VDP links in a table which can then be exported into advantage and 
subsequently included in the stored proc InternetFeedsInventoryTableByMarketName

need to check on conflicts between running this and when them main advantage to postgresql scrape runs:
    ads_for_luigi_uc_inventory runs @ 2:05AM
    ads_for_luigi_ukg_tables runs @ 3:00AM

no, make some separate pg tables    
    
changes from the orig scripts
    change task name to enable the same logging into pg ops.adds_extract 
    
1. scrape advantage tables into postgresql   
2. generate the postgresql table of vdp links
3. scrape the postgresql vdp links table into advantage    
"""

pg_server = '173'
the_hour = datetime.now().strftime("%H")
seed(1)

#  ext_vehicle_items, generate relevant task names for logging
if the_hour == '0':
    task = 'VDP_vehicle_items_1'
elif the_hour == '11':
    task = 'VDP_vehicle_items_2'
elif the_hour == '15':
    task = 'VDP_vehicle_items_3'
elif the_hour == '18':
    task = 'VDP_vehicle_items_4'
else:
    task = 'VDP_vehicle_items_' + str(randint(5, 100))

try:
    file_name = 'files/vdp_vins.csv'
    table_name = 'ads.vdp_vins'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT a.vehicleitemid, a.vin
                from vehicleitems a
                JOIN VehicleInventoryItems b on a.VehicleItemID = b.VehicleItemID
                  AND b.thruts IS null
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'vdp', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

#  ext_vehicle_inventory_items
if the_hour == '0':
    task = 'VDP_vehicle_inventory_items_1'
elif the_hour == '11':
    task = 'VDP_vehicle_inventory_items_2'
elif the_hour == '15':
    task = 'VDP_vehicle_inventory_items_3'
elif the_hour == '18':
    task = 'VDP_vehicle_inventory_items_4'
else:
    task = 'VDP_vehicle_inventory_items_' + str(randint(5, 100))

try:
    file_name = 'files/vdp_vii.csv'
    table_name = 'ads.vdp_vii'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT a.vehicleinventoryitemid,a.stocknumber,cast(a.fromts AS sql_date),
              cast('12/31/9999' AS sql_date),vehicleitemid,owninglocationid
            FROM vehicleinventoryitems a
            WHERE a.thruts IS null	
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'vdp', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)


# ext_vehicle_inventory_item_statuses
if the_hour == '0':
    task = 'VDP_vehicle_inventory_item_statuses_1'
elif the_hour == '11':
    task = 'VDP_vehicle_inventory_item_statuses_2'
elif the_hour == '15':
    task = 'VDP_vehicle_inventory_item_statuses_3'
elif the_hour == '18':
    task = 'VDP_vehicle_inventory_item_statuses_4'
else: task = 'VDP_vehicle_inventory_item_statuses_' + str(randint(5,100))
try:
    file_name = 'files/vdp_vii_statuses.csv'
    table_name = 'ads.vdp_vii_statuses'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT a.vehicleinventoryitemid, a.status, cast(a.fromts AS sql_date),
              cast('12/31/9999' AS sql_date)
            from vehicleinventoryitemstatuses a
            JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
			  AND b.thruts IS null	
		    WHERE a.thruts is null
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'vdp', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

# pg used_cary_vdp_links
if the_hour == '0':
    task = 'VDP_pg_table_1'
elif the_hour == '11':
    task = 'VDP_pg_table_2'
elif the_hour == '15':
    task = 'VDP_pg_table_3'
elif the_hour == '18':
    task = 'VDP_pg_table_4'
else:
    task = 'VDP_pg_table_' + str(randint(5,100))
try:
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select ifd.update_used_car_vdp_links();
            """
            pg_cur.execute(sql)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'vdp', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

# advantage used_car_vdp_links
if the_hour == '0':
    task = 'VDP_advantage_table_1'
elif the_hour == '11':
    task = 'VDP_advantage_table_2'
elif the_hour == '15':
    task = 'VDP_advantage_table_3'
elif the_hour == '18':
    task = 'VDP_advantage_table_4'
else:
    task = 'VDP_advantage_table_' + str(randint(5,100))
try:
    file_name = 'files/pg_vdp_data.csv'
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select *
                from ifd.used_vdp_links;
            """
            pg_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(pg_cur.fetchall())
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            ads_cur.execute("delete from vdp_links")
            with open(file_name, 'r') as io:
                for row in csv.reader(io):
                    sql = """"
                        insert into vdp_links values('{}','{}','{}')
                    """.format(row[0], row[1], row[2])
                    sql = sql.replace('\n', "")    # remove new lines, not actually required, insert works without it
                    sql = sql.replace('"', '')    # remove weird initial "
                    # sql = sql.replace(",'O'", ",'O''")  # fix O'Neil, etc
                    ads_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'vdp', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
