# coding=utf-8
import db_cnx
import csv

task = 'ads_ext_fact_repair_order'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/fact_repair_order.csv'
pg_server = '173'
try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT a.*
                from factrepairorder a
                inner join day b on a.opendatekey = b.datekey
                where b.thedate > curdate() - 365
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_fact_repair_order_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_fact_repair_order_tmp from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                delete
                from ads.ext_fact_repair_order
                where ro in (
                  select ro
                  from ads.ext_fact_repair_order_tmp);
            """
            pg_cur.execute(sql)
            sql = """
                insert into ads.ext_fact_repair_order
                select * from ads.ext_fact_repair_order_tmp
            """
            pg_cur.execute(sql)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_fact_repair_order', TRUE
                    """
            pg_cur.execute(sql)
        print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'ads_ext_fact_repair_order', FALSE
            """
            pg_cur.execute(sql)
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
