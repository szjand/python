# coding=utf-8
import db_cnx
import csv

task = 'ext_dim_employee'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/edw_employee_dim.csv'
pg_server = '173'

try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT * from edwEmployeeDim
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("ads.ext_edw_employee_dim")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_edw_employee_dim from stdin with csv encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()

