# coding=utf-8
import db_cnx
import csv
import ops
"""
tp.tech_values
tp.team_techs
tp.team_values
tp.teams
tp.techs
tp.data
12/30/21
    as part of the migration from arkona to ukg, it has become necessary to copy the above tpdata tables from 
    advantage to postgresql
    this runs nightly on 10.130.196.22
    tpData logs in pg ops.ads_extract for dependency check in luigi
"""
pg_con = None
ads_con = None
run_id = None
pg_server = '173'
file_name = 'files/team_pay.csv'

# task = 'tpTeamTechs'
# print(task)
# try:
#     with db_cnx.ads_sco() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from tpteamtechs
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate tp.team_techs")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy tp.team_techs from stdin with csv encoding 'latin-1 '""", io)
# except Exception, error:
#     ops.email_error(task, 'Export tpData to PG', error)
#     raise Exception('failed tpTeamTechs')
#
# task = 'tpTeamValues'
# print(task)
# try:
#     with db_cnx.ads_sco() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from tpteamvalues
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate tp.team_values")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy tp.team_values from stdin with csv encoding 'latin-1 '""", io)
# except Exception, error:
#     ops.email_error(task, 'Export tpData to PG', error)
#     raise Exception('failed tpTeamValues')
#
#
# task = 'tpTeams'
# print(task)
# try:
#     with db_cnx.ads_sco() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from tpteams
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate tp.teams")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy tp.teams from stdin with csv encoding 'latin-1 '""", io)
# except Exception, error:
#     ops.email_error(task, 'Export tpData to PG', error)
#     raise Exception('failed tpTeams')
#
#
# task = 'tpTechValues'
# print(task)
# try:
#     with db_cnx.ads_sco() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from tptechvalues
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate tp.tech_values")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy tp.tech_values from stdin with csv encoding 'latin-1 '""", io)
# except Exception, error:
#     ops.email_error(task, 'Export tpData to PG', error)
#     raise Exception('failed tpTechValues')
#
#
# task = 'tpTechs'
# print(task)
# try:
#     with db_cnx.ads_sco() as ads_con:
#         with ads_con.cursor() as ads_cur:
#             sql = """
#                 select *
#                 from tptechs
#             """
#             ads_cur.execute(sql)
#             with open(file_name, 'wb') as f:
#                 csv.writer(f).writerows(ads_cur.fetchall())
#     with db_cnx.new_pg(pg_server) as pg_con:
#         with pg_con.cursor() as pg_cur:
#             pg_cur.execute("truncate tp.techs")
#             with open(file_name, 'r') as io:
#                 pg_cur.copy_expert("""copy tp.techs from stdin with csv encoding 'latin-1 '""", io)
# except Exception, error:
#     ops.email_error(task, 'Export tpData to PG', error)
#     raise Exception('failed tpTechs')


task = 'tpData'
print(task)
try:
    with db_cnx.ads_sco() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                select TheDate,PayPeriodStart,PayPeriodEnd,PayPeriodSeq,DayOfPayPeriod,DayName,DepartmentKey,
                  TechKey,TechNumber,EmployeeNumber,FirstName,LastName,TechTeamPercentage,TechHourlyRate,
                  TechTFRRate,TechClockHoursDay,TechClockHoursPPTD,TechFlagHoursDay,TechFlagHoursPPTD,
                  TechFlagHourAdjustmentsDay,TechFlagHourAdjustmentsPPTD,TeamKey,TeamName,TeamCensus,TeamBudget,
                  TeamClockHoursDay,TeamClockHoursPPTD,TeamFlagHoursDay,TeamFlagHoursPPTD,TeamProfDay,
                  TeamProfPPTD,
                  TechVacationHoursDay,
                  TechVacationHoursPPTD,
                  TechPTOHoursDay,
                  TechPTOHoursPPTD,
                  TechHolidayHoursDay,
                  TechHolidayHoursPPTD,
                  TechOtherHoursDay,
                  TechOtherHoursPPTD,
                  TechTrainingHoursDay,
                  TechTrainingHoursPPTD,
                  TeamOtherHoursDay,
                  TeamOtherHoursPPTD,
                  TeamTrainingHoursDay,
                  TeamTrainingHoursPPTD,
                  TeamFlagHourAdjustmentsDay,
                  TeamFlagHourAdjustmentsPPTD,
                  payPeriodSelectFormat
                from tpdata
                where thedate > '09/25/2021'
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("delete from tp.data where the_date > '12/31/2021'")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy tp.data from stdin with csv encoding 'latin-1 '""", io)
            # sql = """
            #     insert into ops.ads_extract (the_date, task, complete)
            #     select current_date, 'tpData', TRUE
            # """
            # pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'Export tpData to PG', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, 'tpData', FALSE
            """
            pg_cur.execute(sql)
