# coding=utf-8
import db_cnx
import csv

file_name = 'files/aftermarket_ros.csv'
pg_server = '173'

with db_cnx.ads_dds() as ads_con:
    with ads_con.cursor() as ads_cur:
        sql = """
            select a.ro, a.line, b.thedate, h.fullname, trim(d.correction) AS correction, 
              e.opcode as cor_code, trim(e.description) as cor_desc, 
              f.opcode, trim(f.description) as op_desc, g.name AS writer
            from factrepairorder a
            join day b on a.opendatekey =  b.datekey
              and b.thedate > curdate() - 730
            join dimservicetype c on a.servicetypekey = c.servicetypekey  
              and c.servicetypecode = 'AM'
            join dimccc d on a.ccckey = d.ccckey  
            join dimopcode e on a.corcodekey = e.opcodekey
            join dimopcode f on a.opcodekey = f.opcodekey
            JOIN dimservicewriter g on a.servicewriterkey = g.servicewriterkey
            JOIN dimcustomer h on a.customerkey = h.customerkey
            where a.storecode = 'RY1'
        """
        ads_cur.execute(sql)
        with open(file_name, 'wb') as f:
            csv.writer(f, quoting=csv.QUOTE_ALL).writerows(ads_cur.fetchall())
with db_cnx.new_pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate af.ros")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy af.ros from stdin with csv encoding 'latin-1 '""", io)


