# encoding=utf-8
"""
"""
from selenium import webdriver
import time
import os
# import codecs
# import fnmatch
from bs4 import BeautifulSoup
import csv
import utilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

download_dir = 'downloads/'
driver = None

def setup_driver():
    try:
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": download_dir}
        options.add_experimental_option("prefs", profile)
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver
    except Exception as error:
        print(error)


def main():
    try:
        driver = setup_driver()
        driver.get('https://docs.google.com/spreadsheets/d/1FBGApIeQk53T68rBhAEQubjk6UsANwmbTFmlzYzaciM/'
                   'edit?usp=sharing_eip&ts=5d88e5fa')
        file_menu = driver.find_elements_by_xpath('//*[@id="docs-file-menu"]')
        file_menu.click()
    finally:
        driver.quit()

if __name__ == '__main__':
    main()