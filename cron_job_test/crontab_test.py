# encoding=utf-8
"""
06/25/21
couldn't get it to work
stack overflow to the rescue, i had installed crontab instead of python-crontab
https://gitlab.com/doctormo/python-crontab/

install croniter to facilitate the schedule functionality
"""
from crontab import CronTab
import datetime

# https://blog.appliedinformaticsinc.com/managing-cron-jobs-with-python-crontab
# this script generated this in crontab: * * * * * echo hello_world
with CronTab(user='jon') as cron:
    job = cron.new(command='echo hello_world')
    job.minute.every(1)
    print('cron.write() was just executed')

    schedule = job.schedule(date_from=datetime.datetime.now())
    print('next run time: ' + str(schedule.get_next()))
    print('previous run time: ' + str(schedule.get_prev()))


# # prints everything in the jon crontab
# empty_cron    = CronTab()
# my_user_cron  = CronTab(user=True)
# users_cron    = CronTab(user='jon')
# for job in users_cron:
#     print(job)