# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_users'
sql_srv_con = None
pg_con = None
run_id = None
file_name = 'files/ext_user.csv'
try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.drive_centric() as sql_srv_con:
        with sql_srv_con.cursor() as sql_srv_cur:
            sql = """
                select pkUserID,fkStoreID,Email,Password,FirstName,LastName,UserType,DateCreated,DateModified,IsDeleted,
                    IsActive,IsReceivingAllSurveys,SessionStoreID,Biography,ImageURL,Phone,Title,ReminderTypeTask,
                    ReminderTypeAppointment,ReminderTimeTask,ReminderTimeAppointment,IsPublic,DateLastPing,DMSImportID,
                    DriveVelocityEmailAlias,ForwardEmailTo,fkOrphanedToUserID,IsWebSpotlight,IsShowingExtended,IsBDC,
                    DateInactive,CellPhone,IsAllowedToSeeDupes,PIN,TextNumber,GUID,AuthenticationToken,IsClockedIn,
                    EmailSignature,IsOnVacation,VacationResponse,GalaxyUserGUID,ProfileURL,DrivePhoneNumber,Tags,
                    TextNumberProvider
                from [user]
                """
            sql_srv_cur.execute(sql)
            with open(file_name, 'wb') as op:
                csv.writer(op).writerows(sql_srv_cur)
    with db_cnx.pg() as database:
        with database.cursor() as pg_cursor:
            pg_cursor.execute("truncate dc.ext_user")
            with open(file_name, 'r') as io:
                pg_cursor.copy_expert("""copy dc.ext_user from stdin with csv header encoding 'latin-1' """, io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if sql_srv_con:
        sql_srv_con.close()
    if pg_con:
        pg_con.close()