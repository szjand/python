# encoding=utf-8
"""

"""
import db_cnx

file_path = 'from_dfx/'
file_name = 'DFX_RDG_AppointmentItem_20200505_051500_DAY.txt'
# https://www.laurivan.com/load-a-csv-file-with-header-in-postgres-via-psycopg/
SQL_STATEMENT = """
    COPY %s FROM STDIN WITH
        CSV
        HEADER
        DELIMITER AS '|'
    """
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        with open(file_path + file_name, 'r') as io:
            pg_cur.copy_expert(sql = SQL_STATEMENT % 'dfx.dfx_rdg_appointment_item', file=io)



