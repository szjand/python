﻿
/*
Hey Jon,

Can you pull me a list for the following op codes?  I have put the actual op codes in bold.
Tires – 1NT, 2NT, 3NT, 4NT
Brakes – H0042 for the front
Brakes – H0043 for the rear
Batteries – N0110

Attached is the information I would need for each op code.  Let me know if you have any questions
*/
drop table if exists wtf cascade;
create temp table wtf as
select a.ro, b.the_date, d.firstname as customer_first_name, d.lastname as customer_last_name, 
  e.modelyear, e.make, e.model,
  g.employee_first_name as advisor_first_name, g.employee_last_name as advisor_last_name,
  dd.address_1 as address,
  d.fullname as customer_name, d.email, d.homephone as home_phone,
  a.miles as odometer, 
  d.cellphone as mobile_phone, dd.state_code as state, e.vin, d.zip
from ads.ext_fact_repair_order a
join dds.dim_date b on a.closedatekey = b.date_key
  and b.the_date between current_date - 90 and current_date - 1
join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
  and c.opcode in ('1NT','2NT','3NT','4NT','H0042','H0043','N0110')
join ads.ext_dim_customer d on a.customerkey = d.customerkey
  and d.customerkey not in (2)
  and d.lastname not like 'RYDELL%'
join arkona.xfm_bopname dd on d.bnkey = dd.bopname_record_key
  and dd.current_row
join ads.ext_dim_vehicle e on a.vehiclekey = e.vehiclekey
join ads.ext_dim_service_writer f on a.servicewriterkey = f.servicewriterkey  
join arkona.xfm_pymast g on f.employeenumber = g.pymast_employee_number
  and g.current_row
where a.storecode = 'RY1';  

create index on wtf(vin);
create index on wtf(ro);


select * 
from wtf a
left join ads.ext_ron_appointments b on a.ro = b.ronumber
  and a.vin = b.vin
  and b.status <> 'no show'
  and a.the_date = b.starttime::date

drop table if exists appointments cascade;
create temp table appointments as
select status, createdbyid, ronumber, vin, customer, starttime::date as starttime
from ads.ext_ron_appointments 
where status <> 'no show'
  and starttime::date > current_date - 120
group by  status, createdbyid, ronumber, vin, customer, starttime::date;
create index on appointments(ronumber);
create index on appointments(vin);
create index on appointments(starttime);

select * 
from wtf a
left join appointments b on a.ro = b.ronumber
  and a.vin = b.vin
  and a.the_date = b.starttime