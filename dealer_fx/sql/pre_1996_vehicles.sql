﻿select a.vin, arkona.db2_integer_to_date(open_date), b.make, b.model, b.year
from arkona.ext_sdprhdr a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
order by arkona.db2_integer_to_date(open_date) desc   
limit 10


select c.vin, c.make, c.model, c.modelyear, min(the_date), max(the_date), count(*)
from ads.ext_Fact_repair_order a
join dds.dim_Date b on a.opendatekey = b.date_key
join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
  and c.modelyear <> 'UNKN'
  and c.modelyear <> 'NA'
  and c.make not in ('BODY SHOP','UNKNOWN','ANY')
  and c.vin not like '1111%'
where b.the_date between current_date - 370 and current_date
--   and c.modelyear::integer < 1996
--   and c.modelyear::integer > 1920
  and c.make like 'olds%'
group by c.vin, c.make, c.model, c.modelyear  
order by modelyear 

------------------------------------------------------------------
-- 03/28 a list of all time, pre 96 vehicles sales or service, VIN, Year, Make, Model & date of most recent visit

select 'sales', a.bopmast_vin, b.year, b.make, b.model, max(a.date_capped) as the_date
from arkona.xfm_bopmast a
join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
  and b.current_row
  and b.year < 1996
  and length(a.bopmast_vin) = 17
  and a.bopmast_vin not like '00%'
where a.record_status = 'U'
  and a.current_row
group by a.bopmast_vin, b.year, b.make, b.model  

select 'service', a.vin, b.year, b.make, b.model, max((select arkona.db2_integer_to_date(a.open_date))) as the_date
from arkona.ext_sdprhdr a
join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
  and b.current_row
  and b.year < 1996
  and length(a.vin) = 17
  and a.vin not like '0%'
group by a.vin, b.year, b.make, b.model  


drop table if exists wtf;
create temp table wtf as
select vin, model_year, make, model, max(the_date) as the_date
from (
  select a.bopmast_vin as vin, b.year as model_year, b.make, b.model, max(a.date_capped) as the_date
  from arkona.xfm_bopmast a
  join arkona.xfm_inpmast b on a.bopmast_vin = b.inpmast_vin
    and b.current_row
    and b.year < 1996
    and length(a.bopmast_vin) = 17
    and a.bopmast_vin not like '00%'
  where a.record_status = 'U'
    and a.current_row
  group by a.bopmast_vin, b.year, b.make, b.model  
  union
  select a.vin, b.year, b.make, b.model, max((select arkona.db2_integer_to_date(a.open_date))) as the_date
  from arkona.ext_sdprhdr a
  join arkona.xfm_inpmast b on a.vin = b.inpmast_vin
    and b.current_row
    and b.year < 1996
    and length(a.vin) = 17
    and a.vin not like '0%'
  group by a.vin, b.year, b.make, b.model) x 
group by vin, model_year, make, model


select vin
from wtf
group by vin
having count(*) > 1

select *
from wtf