﻿
-----------------------------------------------------------------
--< honda nice care
-----------------------------------------------------------------
/*
create index on arkona.xfm_bopname(bopname_search_name);
create index on arkona.xfm_bopname(current_row);
create index on dfx.ext_honda_nissan_nice_care(name);
create unique index on dfx.ext_honda_nissan_nice_care(name,city,phone);

select * --66
from dfx.ext_honda_nissan_nice_care
order by name

-- fix reversed names
update dfx.ext_honda_nissan_nice_care x
set name = y.new_name
from (
  select name, substring(name, position(' ' in name) + 1, 25) || ', ' || left(name, position(' ' in name)) as new_name
  from dfx.ext_honda_nissan_nice_care
  where position(',' in name) = 0) y
where x.name = y.name  

drop table if exists honda_nice_care cascade;
create temp table honda_nice_care as
select a.name, b.bopname_search_name, a.city as dfx_city, b.city, a.phone, b.phone_number, b.bopname_record_key, similarity(a.name, b.bopname_search_name)
from dfx.ext_honda_nissan_nice_care a
left join arkona.xfm_bopname b on similarity(a.name, b.bopname_search_name) > .5
  and b.current_row;


select *
from honda_nice_care 
order by name

select *
from honda_nice_care 
where name not in (
select a.name
from honda_nice_care a
where similarity = 1
group by a.name, a.dfx_city, a.phone
union all
select a.name
from honda_nice_Care a
where similarity <> 1
  and (a.dfx_city = city or a.phone::bigint = phone_number)
group by a.name, a.dfx_city, a.phone)

drop table if exists honda_nice_care_bopname;
create temp table honda_nice_care_bopname as
  select aa.name, aa.dfx_city, aa.phone, max(bopname_record_key) as bopname_record_key
  from (
    select a.name, a.dfx_city, a.phone, max(bopname_record_key) as bopname_record_key --33
    from honda_nice_care a
    where similarity = 1
    group by a.name, a.dfx_city, a.phone
    union
    select a.name, a.dfx_city, a.phone, max(bopname_record_key) as bopname_record_key -- 7
    from honda_nice_Care a
    where similarity <> 1
      and (a.dfx_city = city or a.phone::bigint = phone_number)
    group by a.name, a.dfx_city, a.phone) aa
  group by aa.name, aa.dfx_city, aa.phone;


select * from honda_nice_care_bopname order by name

insert into honda_nice_care_bopname values('BEGER, KYLE ','','',1131395);

select a.name, a.city, a.phone, a.model_year, a.make, a.model, a.vin, a.campaign,
  c.address_1 as address, c.state_code as state, c.zip_code,c.email_address
from dfx.ext_honda_nissan_nice_care a
left join honda_nice_care_bopname b on a.name = b.name
left join arkona.xfm_bopname c on b.bopname_record_key = c.bopname_record_key
  and c.current_row
order by a.name
*/
-----------------------------------------------------------------
--/> honda nice care
-----------------------------------------------------------------


-----------------------------------------------------------------
--/> gm maintenance
-----------------------------------------------------------------
delete from dfx.ext_gm_maintenance where name is null;
create unique index on dfx.ext_gm_maintenance(name, vehicle);
create index on dfx.ext_gm_maintenance(name);

drop table if exists gm;
create temp table gm as
select *
from dfx.ext_gm_maintenance
order by name

update gm x
set name = y.new_name
from (
select name, trim(substring(name, position(' ' in name) + 1, 25) || ', ' || left(name, position(' ' in name))) as new_name
from dfx.ext_gm_maintenance) y
where x.name = y.name

select * from gm


drop table if exists gm_1;
create temp table gm_1 as
select a.name, b.bopname_search_name, a.phone, b.phone_number, a.vehicle, b.bopname_record_key, similarity(a.name, b.bopname_search_name)
from gm a
left join arkona.xfm_bopname b on similarity(a.name, b.bopname_search_name) > .5
  and b.current_row;

select * from gm_1  

-- these are keepers
drop table if exists gm_2;
create temp table gm_2 as
select a.name, a.phone, a.vehicle, max(bopname_record_key) as bopname_record_key
from gm_1 a
where a.similarity > .5
  and a.phone = a.phone_number::citext
group by a.name, a.phone, a.vehicle  
order by a.name;


insert into gm_2
select a.name, a.phone, a.vehicle, max(a.bopname_record_key)
from gm_1 a 
join arkona.ext_bopvref b on a.bopname_record_key = b.customer_key
join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row
  and a.vehicle = c.year || ' ' || c.model
where not exists (
  select 1
  from gm_2
  where name = a.name)
group by a.name, a.phone, a.vehicle;

-- good enuf
select *
from gm a
left join gm_2 b on a.name = b.name and a.vehicle = b.vehicle
where b.name is null
order by a.name

-- final 
drop table if exists gm_3;
create temp table gm_3 as
select a.name, a.phone, a.vehicle, a.campaign,
  b.phone_number, b.address_1 as address, b.city, b.state_code as state, b.zip_code, b.email_address
from gm a  
left join  gm_2 aa on a.name = aa.name and a.vehicle = aa.vehicle
left join arkona.xfm_bopname b on aa.bopname_record_key = b.bopname_record_key
  and b.current_row
order by a.name

-- need to get rid of a few
select a.*
from gm_3 a
join (
  select name
  from gm_3
  group by name
  having count(*) > 1) b on a.name = b.name
order by a.name  

delete 
from gm_3
where (
  (name = 'PESCH, DEAN' and state = 'PA')
  or
  (name = 'JASMER, RONALD' and email_address is null)
  or 
  (name = 'ERICKSON, SHANE' and email_address is null)
  or
  (name = 'ENGER, CALLIE' and email_address is null)
  or
  (name = 'DUPPONG, DEAN' and email_address is null))

update gm_3 set phone = null where phone = 'danhol@gra.midco.net'

-- the spreadsheet
select * from gm_3

-----------------------------------------------------------------
--/> gm maintenance
-----------------------------------------------------------------

-----------------------------------------------------------------
--< gm nice care
-----------------------------------------------------------------
delete from dfx.ext_nice_care_gm_store where name is null;
create unique index on dfx.ext_nice_care_gm_store(name, vin);
create index on dfx.ext_nice_care_gm_store(name);

drop table if exists gmnc;
create temp table gmnc as
select *
from dfx.ext_nice_care_gm_store
order by name


update gmnc x
set name = y.new_name
from (
select name, trim(substring(name, position(' ' in name) + 1, 25) || ', ' || left(name, position(' ' in name))) as new_name
from dfx.ext_nice_care_gm_store
where position(',' in name) = 0) y
where x.name = y.name

drop table if exists gmnc_1;
create temp table gmnc_1 as
select a.name, b.bopname_search_name, a.phone, b.phone_number, a.vin, b.bopname_record_key, similarity(a.name, b.bopname_search_name)
from gmnc a
left join arkona.xfm_bopname b on similarity(a.name, b.bopname_search_name) > .5
  and b.current_row;

select *
from gmnc_1  


-- these are keepers
drop table if exists gmnc_2;
create temp table gmnc_2 as
select a.name, a.phone, a.vin, max(bopname_record_key) as bopname_record_key
from gmnc_1 a
where a.similarity > .5
  and a.phone = a.phone_number::citext
group by a.name, a.phone, a.vin  
order by a.name;

select *
from gmnc_2
order by name

-- only missing 4, good enough
select *
from gmnc a
where not exists (
  select 1
  from gmnc_2
  where name = a.name)

-- spreadsheet
select a.name, b.phone_number, a.phone, b.email_address as email, b.address_1 as address,
  a.city, b.state_code as state, b.zip_code, a.model_year, a.make, a.model, a.vin, a.campaign
from gmnc a
left join gmnc_2 aa on a.name = aa.name and a.vin = aa.vin
left join arkona.xfm_bopname b on aa.bopname_record_key = b.bopname_record_key
  and b.current_row
  and b.bopname_Company_number = 'RY1'
order by a.name  

-----------------------------------------------------------------
--/> gm nice care
-----------------------------------------------------------------

-----------------------------------------------------------------
-- onstar
-----------------------------------------------------------------


drop table if exists os cascade;
create temp table os as
select name, array_agg(distinct phone) as phone, array_agg(distinct vehicle) as vehicle
from dfx.ext_onstars_for_december_1211
group by name
order by name;

delete
from dfx.ext_onstars_for_december_1211
where name is null;

create unique index on os(name);

select * --330
from os

update os x
set name = y.new_name
from (
select name, trim(substring(name, position(' ' in name) + 1, 25) || ', ' || left(name, position(' ' in name))) as new_name
from os
where position(',' in name) = 0) y
where x.name = y.name


drop table if exists os_1;
create temp table os_1 as
select a.name, b.bopname_search_name, a.phone, b.phone_number, a.vehicle, b.bopname_record_key, similarity(a.name, b.bopname_search_name)
from os a
left join arkona.xfm_bopname b on similarity(a.name, b.bopname_search_name) > .5
  and b.current_row
  and b.bopname_company_number = 'RY1';

delete
from os_1
where name is null;

-- keepers
drop table if exists os_2;
create temp table os_2 as
select name, (left(array_to_string(phone,'-'),10))::citext, (array_to_string(vehicle,'-'))::citext, max(bopname_record_key) as bopname_record_key
from os_1
where similarity > .5
  and phone_number::text = any(phone)
group by name, phone, vehicle;

-- a few  more
insert into os_2
select a.name, left(array_to_string(a.phone,'-'), 10), array_to_string(a.vehicle,'-'), max(bopname_record_key)
from os_1 a
join arkona.ext_bopvref b on a.bopname_record_key = b.customer_key
join arkona.xfm_inpmast c on b.vin = c.inpmast_vin
  and c.current_row
  and c.year::citext || ' ' || c.model = trim(a.vehicle[1])
where not exists (
  select 1
  from os_2
  where name = a.name)
group by a.name,array_to_string(a.phone,'-'), array_to_string(a.vehicle,'-');


-- spreadsheet
select a.name, a.phone, a.vehicle,a.campaign, a.notes, c.phone_number as home_phone, c.address_1 as address,
  c.city, c.state_code as state, c.zip_code, c.email_address as email
from dfx.ext_onstars_for_december_1211 a
left join os_2 b on trim(substring(a.name, position(' ' in a.name) + 1, 25) || ', ' || left(a.name, position(' ' in a.name)))  = b.name
left join arkona.xfm_bopname c on b.bopname_record_key = c.bopname_record_key
  and c.current_row
  and c.bopname_company_number = 'RY1'
order by a.name  
-----------------------------------------------------------------
--/> onstar
-----------------------------------------------------------------

-----------------------------------------------------------------
--< service smarts
-----------------------------------------------------------------

drop table if exists ss cascade;
create temp table ss as
select *
from dfx.ext_service_smarts
order by name;

create unique index on ss(name,vin);

select *
from dfx.ext_service_smarts
where position(',' in name) = 0

drop table if exists ss_1;
create temp table ss_1 as
select a.name, a.city as ss_city, a.vin, a.model_year, a.model as ss_model, b.bopname_search_name, b.city, d.year, d.model, similarity(a.name, b.bopname_search_name) 
from ss a
left join arkona.xfm_bopname b on similarity(a.name, b.bopname_search_name) > .5
  and b.current_row
  and b.bopname_company_number = 'RY1'
left join arkona.ext_bopvref c on b.bopname_record_key = c.customer_key
  and a.vin = c.vin  
left join arkona.xfm_inpmast d on c.vin = d.inpmast_vin
  and d.current_row;

select *
from ss_1
where similarity > .5
  and (ss_city = city or ss_model = model)

1GCGTEEN8K1193728

select *
from arkona.ext_bopvref
where vin = '1GCGTEEN8K1193728'

-- this query proved to be way more efficient and and gave better results
select *
from (
  select a.vin, a.model_year, a.model, a.name, a.city, b.customer_key, b.start_date, b.end_date, b.type_code, c.bopname_Search_name, c.city, similarity(a.name, c.bopname_search_name),
    row_number() over (partition by a.name order by similarity(a.name, c.bopname_search_name) desc)
  from ss a
  left join arkona.ext_bopvref b on a.vin = b.vin
  left join arkona.xfm_bopname c on b.customer_key = c.bopname_record_key
    and c.current_row) aa
where row_number = 1  
order by name

-- spreadsheet
select *
from (
  select a.vin, a.model_year as year, a.model, a.name, a.city, a.state, a.zip_code, a.notes, a.campaign,
    c.phone_number as home_phone, c.address_1 as address, c.email_address as email,
    row_number() over (partition by a.name order by similarity(a.name, c.bopname_search_name) desc)
  from ss a
  left join arkona.ext_bopvref b on a.vin = b.vin
  left join arkona.xfm_bopname c on b.customer_key = c.bopname_record_key
    and c.current_row) aa
where row_number = 1  
order by name


-----------------------------------------------------------------
--/> service smarts
-----------------------------------------------------------------