﻿/*
Attached is spreadsheet for the info needed for us to provide to Rydell Nice Care
 
Morgan Hibma
04/08/20

Vin	First Name	Last Name	Phone Number	Email 	Address	City 	State	Zip	Vehicle Purchase Date	Miles at date of purchased
*/

select min(selectedreconpackagets):: date, max(selectedreconpackagets)::date
from ads.ext_selected_recon_packages
limit 100

select min(start_date)
from arkona.ext_bopvref
where start_date <> 0

select *
from arkona.ext_bopvref
where company_number in ('RY1','RY2')
order by start_date

create schema tool;
comment on schema tool is 'schema for extracted tool data';

drop table if exists tool.vehicles cascade;
create table tool.vehicles (
  vii_id citext,
  stock_number citext primary key,
  vin citext,
  from_date date,
  thru_date date);
create unique index on tool.vehicles(vii_id);
comment on table tool.vehicles is 'core attributes from vehicleinventoryitems and vehicleitems';  
insert into tool.vehicles
select a.vehicleinventoryitemid, a.stocknumber, b.vin, a.fromts::date, a.thruts::date
from ads.ext_vehicle_inventory_items a
join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid

drop table if exists tool.retail_sales;
create table tool.retail_sales (
  vii_id citext references tool.vehicles(vii_id),
  sale_date date,
  customer citext,
  primary key (vii_id,sale_date));
comment on table tool.retail_sales is 'scrape of ads.vehiclesales,, retail sales only';
insert into tool.retail_sales 
select vehicleinventoryitemid, soldts::date, soldto
from ads.ext_vehicle_sales a
where typ = 'VehicleSale_Retail'
  and status <> 'VehicleSale_SaleCanceled'
  and exists (
    select 1
    from tool.vehicles
    where vii_id = a.vehicleinventoryitemid);  

drop table if exists tool.recon_packages;
create table tool.recon_packages (
  vii_id citext primary key,
  recon_package citext,
  the_date date);
comment on table tool.recon_packages is 'last recon package for vehicles in tool.vehicles';
insert into tool.recon_packages
select a.vehicleinventoryitemid, trim(substring(typ, position('_' in a.typ) + 1)), a.selectedreconpackagets::date
from ads.ext_selected_recon_packages a
join (
  select vehicleinventoryitemid, max(selectedreconpackagets) as ts
  from ads.ext_selected_recon_packages
  group by vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid and a.selectedreconpackagets = b.ts
join tool.vehicles c on a.vehicleinventoryitemid = c.vii_id
where a.vehicleinventoryitemid not in (
  select vehicleinventoryitemid
  from (
  select a.vehicleinventoryitemid, trim(substring(typ, position('_' in a.typ) + 1)), a.selectedreconpackagets::date
  from ads.ext_selected_recon_packages a
  join (
    select vehicleinventoryitemid, max(selectedreconpackagets) as ts
    from ads.ext_selected_recon_packages
    group by vehicleinventoryitemid) b on a.vehicleinventoryitemid = b.vehicleinventoryitemid and a.selectedreconpackagets = b.ts
  join tool.vehicles c on a.vehicleinventoryitemid = c.vii_id) x
  group by vehicleinventoryitemid having count(*) > 1);

select vii_id
from tool.recon_packages
group by vii_id
having count(*) > 1

-- 9874 rows
drop table if exists nice_sales;
create temp table nice_sales as
select b.stock_number, b.vin, a.customer, a.sale_date
from tool.retail_sales a
join tool.vehicles b on a.vii_id = b.vii_id
join tool.recon_packages c on a.vii_id = c.vii_id
  and c.recon_package = 'nice'
where a.sale_date > '09/30/2009';

--9830
select a.*, b.bopmast_stock_number, b.bopmast_vin, b.bopmast_search_name, b.buyer_number, b.date_capped, b.odometer_at_sale
from nice_sales a
join arkona.xfm_bopmast b on a.stock_number = b.bopmast_stock_number
  and b.current_row
  and b.record_status = 'U'
  and b.vehicle_type = 'U'
order by similarity(a.customer, b.bopmast_search_name)

i want to dig up the work i did with bopvref (nissan i think) to narrow the list down to
as far as we can know, customer still owns the vehicle

/* BOPVREF */
BVTYPE (TYPE_CODE) Customer/Vehicle Reference
S = Buyer on the Deal
2 = Co-Buyer on the Deal
C = Service Customer
T = Trade In
4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)

-- from .../marketing_bdc/nissan_customers.sql
-- don't do a join on nice sales, same vin sold multiple times
drop table if exists bopvref;
create temp table bopvref as
select a.*
from arkona.ext_bopvref a
where a.end_date = 0 
  and a.type_code in ('S','C')
  and a.start_date > 20090000
  and company_number in ('RY1','RY2','RY3')
  and exists (
    select 1
    from nice_sales
    where vin = a.vin)
  and a.vin not in ( -- exclude double vins
  select vin from (
    select a.*
    from arkona.ext_bopvref a
    where a.end_date = 0 
      and a.type_code in ('S','C')
      and a.start_date > 20090000
      and company_number in ('RY1','RY2','RY3')
      and exists (
        select 1
        from nice_sales
        where vin = a.vin)
  ) x group by vin having count(*) > 1);
create unique index on bopvref(vin);   
create index on bopvref(type_code);  
create index on bopvref(customer_key);  

select * from bopvref limit 10

-- too frustrated with multiple customer numbers for customers
bought as: 1041264 CLOW, STACEY ANN
2 weeks later serviced as: 1088427 CLOW, STACY

--  7793 on inner join
drop table if exists nice_bopmast cascade;
create temp table nice_bopmast as
select distinct b.bopmast_company_number, b.bopmast_stock_number, b.bopmast_vin, b.bopmast_search_name, b.buyer_number, b.date_capped, b.odometer_at_sale
from nice_sales a
join arkona.xfm_bopmast b on a.stock_number = b.bopmast_stock_number
  and b.current_row
  and b.record_status = 'U'
  and b.vehicle_type = 'U'
join bopvref c on a.vin = c.vin
  and b.buyer_number = c.customer_key  


drop table if exists no_bopvref cascade;
create temp table no_bopvref as
select b.bopmast_company_number, b.bopmast_stock_number, b.bopmast_vin, b.bopmast_search_name, b.buyer_number, b.date_capped, b.odometer_at_sale
from nice_sales a
join arkona.xfm_bopmast b on a.stock_number = b.bopmast_stock_number
  and b.current_row
  and b.record_status = 'U'
  and b.vehicle_type = 'U'
left join bopvref c on a.vin = c.vin
  and b.buyer_number = c.customer_key  
where c.vin is null

-- these are the ones i can salvage
insert into nice_bopmast
select distinct a.*
from no_bopvref a
join arkona.ext_bopvref b on a.bopmast_vin = b.vin
  and b.end_date = 0 
  and b.type_code in ('s','c')
  and b.company_number in ('RY1','RY2','RY3')
left join arkona.xfm_bopname c on b.customer_key = c.bopname_record_key
  and c.current_row 
where similarity(a.bopmast_search_name, c.bopname_search_name) > .4999;

create index on nice_bopmast(bopmast_search_name);
create index on nice_bopmast(buyer_number);

--7786
-- this is the spreadsheet sent 4/19/20: nice_care_deals
select bopmast_vin as vin, first_name, last_company_name as last_name, phone_number, email_address as email, 
  address_1 as address, city, state_code as state, zip_code as zip, a.date_capped as purchase_date, 
  a.odometer_at_sale as miles_at_sale
from nice_bopmast a
join arkona.xfm_bopname b on a.buyer_number = b.bopname_Record_key
  and b.current_row
  and b.bopname_company_number in ('RY1','RY2','RY3')
where similarity(bopmast_search_name,bopname_search_name) > .49 
  