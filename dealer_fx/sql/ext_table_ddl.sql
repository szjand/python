﻿create schema dfx;
comment on schema dfx is 'schema for DealerFX data, initially generating dms data from different departments';

12/18/19
got several spreadsheets from justin (bdc), added the bopname_record_key column header and saved them as a csv
imported the csv into these tables( ...ext_arkona/x_import_csv.py)

do a search on similarity in sql to get the trigram usage, looks like i used it on a service smarts
.../misc_sql/service_smart_list.sql

create table dfx.ext_gm_maintenance (
  name citext,
  phone citext,
  vehicle citext,
  campaign citext,
  home_phone citext,
  address citext,
  city citext,
  state citext,
  zip_code citext,
  email citext,
  bopname_record_key integer);
comment on table dfx.ext_gm_maintenance is 'spreadsheet provided by bdc, data supplied:name,phone,vehicle,campaign, 
    data needed: home_phone,address,city,state,zip_code,email.  additional data i added: bopname_record_key';

select * from dfx.ext_gm_maintenance


create table dfx.ext_honda_nissan_nice_care (
  name citext,
  city citext,
  phone citext,
  model_year integer,
  make citext,
  model citext,
  vin citext,
  campaign citext,
  address citext,
  state citext,
  zip_code citext,
  email citext,
  bopname_record_key integer);
comment on table dfx.ext_honda_nissan_nice_care is 'spreadsheet provided by bdc, data supplied:name,city,phone,model_year,make,model,vin,campaign, 
    data needed: address,city,state,zip_code,email.  additional data i added: bopname_record_key';

select *
from  dfx.ext_honda_nissan_nice_care    

create table dfx.ext_nice_care_gm_store (
  name citext,
  home_phone citext,
  phone citext,  
  email citext,  
  address citext,
  city citext,  
  state citext,
  zip_code citext,  
  model_year integer,
  make citext,
  model citext,
  vin citext,
  campaign citext,
  bopname_record_key integer);

comment on table dfx.ext_nice_care_gm_store is 'spreadsheet provided by bdc, data supplied:name,phone,city,year,make,model,vin,campaign.
    data needed: home_phone,eamil,address,state,zip_code.  additional data i added: bopname_record_key';

select * from dfx.ext_nice_care_gm_store;    

create table dfx.ext_onstars_for_december_1211 (
  name citext, 
  phone citext,  
  vehicle citext,
  campaign citext,
  notes citext,
  home_phone citext,
  address citext,
  city citext,  
  state citext,
  zip_code citext,  
  email citext,  
  bopname_record_key integer);

comment on table dfx.ext_nice_care_gm_store is 'spreadsheet provided by bdc, data supplied:name,phone,vehicle,campaing,notes.
    data needed: home_phone,address,state,city,zip_code,email.  additional data i added: bopname_record_key';

select * from dfx.ext_onstars_for_december_1211    

create table dfx.ext_service_smarts (
  vin citext,
  model_year integer,
  model citext,
  name citext,
  city citext,
  state citext,
  zip_code citext,
  notes citext,
  campaign citext,
  home_phone citext,
  address citext,
  email citext,
  bopname_record_key integer);

comment on table dfx.ext_service_smarts is 'spreadsheet provided by bdc, data supplied:vin,year,model,name,city,state,zip_code,notes,campaign.
    data needed: home_phone,address,email.  additional data i added: bopname_record_key';  

select * from dfx.ext_service_smarts;    
  