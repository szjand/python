﻿drop table if exists dfx.advisor_checkin_usage_report_ext;
create table dfx.advisor_checkin_usage_report_ext(
  field_1 citext,
  field_2 citext,
  first_name citext,
  field_3 citext,
  last_name citext,
  dmsid citext,
  push_count citext,
  numbers_of_maintenance_menus_generated citext,
  total_dollars_pushed citext,
  field_4 citext,
  average_dollars_per_ro citext,
  total_hours_sold citext,
  field_5 citext,
  field_6 citext,
  utlization_percentage citext,
  field_7 citext,
  maintenance_menu_generation_percentage citext);


select * from dfx.advisor_checkin_usage_report_ext 

drop table if exists dfx.advisor_checkin_usage_report;
create table dfx.advisor_checkin_usage_report(
  writer_name citext references sad.writers(writer_name),
  dmsid citext not null,
  push_count integer,
  numbers_of_maintenance_menus_generated integer not null default '0',
  total_dollars_pushed numeric(8,2) not null default '0',
  average_dollars_per_ro numeric(8,2) not null default '0',
  total_hours_sold numeric(8,2) not null default '0',
  maintenance_menu_generation_percentage integer not null default '0',
  report_date date not null,
  primary key (writer_name,report_date));

select * from dfx.advisor_checkin_usage_report


CREATE OR REPLACE FUNCTION dfx.advisor_checkin_usage_report_update(_the_date citext)
  RETURNS void AS
$BODY$
/*
09/04/20 handle empty spaces for numbers
select dfx.advisor_checkin_usage_report_update(_the_date citext)
*/
insert into dfx.advisor_checkin_usage_report
select writer_name, dmsid, 
  case when push_count = '' then 0 else left(push_count, position('.' in push_count)-1)::integer end,
  left(numbers_of_maintenance_menus_generated, position('.' in numbers_of_maintenance_menus_generated)-1)::integer,
  case when total_dollars_pushed = '' then 0 else total_dollars_pushed::numeric end, 
  case when average_dollars_per_ro = '' then 0 else average_dollars_per_ro::numeric end, 
  case when total_hours_sold = '' then 0 else total_hours_sold::numeric end,
  (maintenance_menu_generation_percentage::numeric * 100)::integer, 
  _the_date::date
from dfx.advisor_checkin_usage_report_ext a
join sad.writers b on a.last_name = (left(b.writer_name, position(',' in b.writer_name) - 1))::citext;
$BODY$
LANGUAGE sql;


-------------------------------------------------------------------------------------------------------
drop table if exists dfx.dynamic_mpi_usage_report_ext;
create table dfx.dynamic_mpi_usage_report_ext(
  field_1 citext,
  advisor_name citext,
  field_2 citext,  
  mpi_completed_number citext,
  mpi_completed_perc citext,
  dmpi_sent_number citext,
  dmpi_sent_perc citext,
  field_3 citext,
  field_4 citext,
  field_5 citext,
  field_6 citext);

select * from dfx.dynamic_mpi_usage_report_ext  

select advisor_name, split_part(advisor_name, ' ', 2)
from dfx.dynamic_mpi_usage_report_ext  
where advisor_name is not null

select b.writer_name, 
  left(mpi_completed_number, position('.' in mpi_completed_number)-1)::integer,
  (mpi_completed_perc::numeric*100)::integer,
  left(dmpi_sent_number, position('.' in dmpi_sent_number)-1)::integer,
  case
    when dmpi_sent_perc = '0%' then 0
    else (dmpi_sent_perc::numeric*100)::integer  
  end
from dfx.dynamic_mpi_usage_report_ext a
join sad.writers b on (split_part(a.advisor_name, ' ', 2))::citext = (left(writer_name, position(',' in writer_name) - 1))::citext;

drop table if exists dfx.dynamic_mpi_usage_report;
create table dfx.dynamic_mpi_usage_report(
  writer_name citext references sad.writers(writer_name),
  mpi_completed_number integer,
  mpi_completed_perc integer,
  dmpi_sent_number integer,
  dmpi_sent_perc integer,
  report_date date,
  primary key (writer_name,report_date));
  

drop function dfx.dynamic_mpi_usage_report_update(date);
CREATE OR REPLACE FUNCTION dfx.dynamic_mpi_usage_report_update(_the_date citext)
  RETURNS void AS
$BODY$
/*
to generate history using this function, have to pass in the date
*/  
insert into dfx.dynamic_mpi_usage_report
select b.writer_name, 
  left(mpi_completed_number, position('.' in mpi_completed_number)-1)::integer,
  (mpi_completed_perc::numeric*100)::integer,
  left(dmpi_sent_number, position('.' in dmpi_sent_number)-1)::integer,
  case
    when dmpi_sent_perc = '0%' then 0
    else (dmpi_sent_perc::numeric*100)::integer  
  end, _the_date::date
from dfx.dynamic_mpi_usage_report_ext a
join sad.writers b on (split_part(a.advisor_name, ' ', 2))::citext = (left(writer_name, position(',' in writer_name) - 1))::citext;
$BODY$
LANGUAGE sql;


select * from dfx.dynamic_mpi_usage_report

select report_date, count(*) from dfx.dynamic_mpi_usage_report group by report_Date order by report_date

select '2020-03-01'::date

select writer_name, sum(mpi_completed_number) as completed, sum(dmpi_sent_number) as sent,
  case
    when sum(mpi_completed_number) = 0 then 0
    else round(sum(dmpi_sent_number)*100.0/sum(mpi_completed_number), 1)
  end as perc
from dfx.dynamic_mpi_usage_report
where extract(month from report_date) = 8
group by writer_name