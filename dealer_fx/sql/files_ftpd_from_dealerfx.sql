﻿drop table if exists dfx.dfx_rdg_appointment;
create table dfx.dfx_rdg_appointment (
  Dealer_ID citext,
  Appointment_ID citext,
  Repair_Order_ID citext, 
  Appointment_Created_date timestamp,
  Appointment_Scheduled_date timestamp,
  Appointment_Source citext,
  Appointment_Status citext,
  Assigned_Service_Advisor_ID citext,
  Transportation_Type citext,
  Scheduled_By_ID citext,
  Vehicle_Mileage citext,
  BAS_Keyword citext,
  Browser_Type citext,
  Campaign_ID citext,
  Pricing_Displayed citext,
  Customer_Concerns_Text citext,
  Symptom_Survey citext,
  row_checksum  citext);

create index on dfx.dfx_rdg_appointment(appointment_id);

select * from dfx.dfx_rdg_appointment 

select count(*) from dfx.dfx_rdg_appointment 

 DETAIL:  Key (appointment_id)=(20746412) already exists.

select * from dfx.dfx_rdg_appointment where appointment_id = '20746412'

select a.*
from dfx.dfx_rdg_appointment a
join (
  select appointment_id
  from dfx.dfx_rdg_appointment
  group by appointment_id
  having count(*) > 1) b on a.appointment_id = b.appointment_id
 order by a.appointment_id 


drop table if exists dfx.dfx_rdg_appointment_item; 
create table dfx.dfx_rdg_appointment_item (
  Dealer_ID citext,
  Appointment_ID citext,
  Appointment_Op_Code_ID citext,
  Dealer_Op_Code_ID citext,
  Appointment_Item_Origin citext,
  Package citext,
  Op_Code_Extended_Price citext,
  Recall_Booked_Indicator citext,
  FRS_Booked_Indicator citext,
  DRS_Booked_Indicator citext,
  row_checksum citext);

select *
from dfx.dfx_rdg_appointment_item


  