# encoding=utf-8
import db_cnx
import csv
import datetime
from openpyxl import load_workbook
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os

file_name = 'files/sales_scorecard_ry2.csv'
spreadsheet = 'files/sales_scorecard_ry2.xlsx'
year_month = (int((datetime.date.today() - datetime.timedelta(days=1)).strftime("%Y"))*100 + int((datetime.date.today()
              - datetime.timedelta(days=1)).strftime("%m")))
# year_month = 201712
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        sql = "select * from sls.get_sales_scorecard_ry2(" + str(year_month) + ")"
        pg_cur.execute(sql)
        with open(file_name, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(["Last Name", "First Name", "Total Sales", "CPO Sales", "New Sales",
                             "Total Over Allow", "Total F&I Gross", "Look", "Book"])
            writer.writerows(pg_cur)
        wb = load_workbook(spreadsheet)
        # new month, create a worksheet
        if not str(year_month) in wb.sheetnames:
            wb.create_sheet(str(year_month))
        else:
            # delete and re-create current month worksheet
            cur_month = wb.get_sheet_by_name(str(year_month))
            wb.remove_sheet(cur_month)
            wb.create_sheet(str(year_month))
        ws = wb[str(year_month)]
        with open(file_name) as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                ws.append(row)
        wb.save(spreadsheet)

try:
    COMMASPACE = ', '
    sender = 'jandrews@cartiva.com'
    recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
    # recipients = ['test@cartiva.com', 'jandrews@cartiva.com', 'bcahalan@rydellcars.com']
    outer = MIMEMultipart()
    outer['Subject'] = 'Sales Scorecard Honda/Nissan'
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    attachments = [spreadsheet]
    for file in attachments:
        try:
            with open(file, 'rb') as fp:
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read())
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(file))
            outer.attach(msg)
        except Exception:
            raise
    composed = outer.as_string()
    # this works with Python2
    e = smtplib.SMTP('mail.cartiva.com')
    try:
        e.sendmail(sender, recipients, composed)
    except smtplib.SMTPException:
        print("Error: unable to send email")
    e.quit()
except Exception as error:
    print(error)
