﻿/*
part of sales scorecard
contracts in transit acct 120500
created: positive amount
payed: negative amount
*/
select b.the_date,a.*, c.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
where a.post_status = 'Y'
  and a.control in ('32615','32390')
  and (
    c.account_type = 'sale'
    or
    c.account = '120500')
order by a.control, c.account_type, b.the_date 


-- sale date defined as date sale account posted
-- is that always the same as the date when the CIT was created?

drop table if exists test_1;
create temp table test_1 as
select b.the_date,a.control, c.account, c.account_type, d.journal_code, sum(a.amount) as amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
where a.post_status = 'Y'
  and b.year_month > 201706
  and (
    (c.account_type = 'sale' and d.journal_code in ('VSU','VSN')
      and c.account in (select gl_account from sls.deals_Accounts_routes where store_code = 'RY1' and page < 17))
    or
    c.account = '120500')
group by b.the_date,a.control, c.account, c.account_type, d.journal_code
order by control;

select *
from test_1
where control = '31243'

drop table if exists test_2;
create temp table test_2 as
select control,
  max(case when journal_code in ('VSN','VSU') and account_type = 'Sale' then the_date end) as sale_date,
  max(case when account = '120500' and amount > 0 then the_date end) as cit_begin_date,
  max(case when account = '120500' and amount < 0 then the_Date end) as cit_paid_date
from test_1  
group by control
order by control;

-- anomalies, unwinds, etc: sale date after cit_begin_date
select *
from test_2
where sale_date is not null
  and cit_begin_date is not null
  and sale_date <> cit_begin_date

-- exclude anomalies, deals w/o CIT
select a.*, cit_paid_date - cit_begin_date
from test_2 a
where sale_date is not null
  and cit_begin_date is not null
  and sale_date = cit_begin_date
  and cit_begin_date <= coalesce(cit_paid_date, '12/31/9999')

-- exclude back ons
drop table if exists test_3;
create temp table test_3 as
select a.year_month, a.stock_number, a.fi_last_name, a.fi_first_name, a.unit_count, b.*
from sls.deals_by_month a
inner join (
  select a.*, coalesce(cit_paid_date, current_date) - cit_begin_date as days_in_transit
  from test_2 a
  where sale_date is not null
    and cit_begin_date is not null
    and sale_date = cit_begin_date
    and cit_begin_date <= coalesce(cit_paid_date, '12/31/9999')) b on a.stock_number = b.control
where a.year_month > 201706 
  and a.store_code = 'RY1'
  and unit_count > 0
order by fi_last_name, sale_date


select year_month, stock_number, fi_last_name as fi, sale_date, cit_paid_date, days_in_transit
from test_3


select fi_last_name, year_month, count(*) as deals_with_cit, round(avg(days_in_transit), 1) as avg_days_in_transit
from test_3
group by fi_last_name, year_month
order by year_month, fi_last_name

select year_month, count(*) as deals_with_cit, round(avg(days_in_transit), 1) as avg_days_in_transit
from test_3
group by year_month
order by year_month

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
1/22/18
jeri wants the lender and if f/i is hse, do sales consultant
lender info is brought down in sls.ext_bopmast_partial.lending_source & leasing_source
which are keys referencing arkona.ext_boplsrc (lending sources) and arkona.ext_bopssrc (leasing sources)

alter table sls.ext_bopmast_partial
add column leasing_source integer,
add column lending_source integer;


select b.the_date,a.control, c.account, c.account_type, d.journal_code, a.amount, e.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and b.year_month = 201801
  and c.account = '120500'
order by control



select * from arkona.ext_boplsrc
select * from arkona.ext_bopssrc
select * from sls.ext_bopmast_partial limit 100

data is (typically) a bit goofy, every deal has a leasing source whether it is a lease or not

select store, leasing_source, count(*)
from sls.ext_bopmast_partial
group by store, leasing_source
order by store, leasing_source

so it will have to be a combination of sale_type with lessor/lendor
if sale_type = L then lessor
if sale_type = R and deal_type = F then lendor

select a.store, a.capped_date, a.deal_status, a.deal_type, a.sale_type, 
  a.sale_group, a.stock_number, a.bopmast_id, a.leasing_source, a.lending_source,
  b.lendor_name, c.lessor_name
from sls.ext_bopmast_partial a
left join arkona.ext_boplsrc b on a.store = b.company_number
  and a.lending_source = b.key
left join arkona.ext_bopssrc c on a.store = c.company_number
  and a.leasing_source = c.key  
where a.store = 'ry1'
  and a.vehicle_type = 'N'
  and lendor_name is not null
  and deal_type <> 'L'
-- order by lendor_name  
order by a.capped_date desc
limit 1000


select a.*,
  case when a.fi_last_name = 'hse' then e.last_name else fi_last_name end as fi,
  case
    when b.sale_type = 'L' then d.lessor_name
    when b.sale_type = 'R' and deal_type = 'F' then c.lendor_name
  end as fin
from test_3 a
left join sls.ext_bopmast_partial b on a.stock_number = b.stock_number
left join arkona.ext_boplsrc c on b.store = c.company_number
  and b.lending_source = c.key
left join arkona.ext_bopssrc d on b.store = d.company_number
  and b.leasing_source = d.key  
left join sls.personnel e on b.primary_sc = e.ry1_id 
order by sale_date

-- v2 spreadsheet
select a.year_month, a.stock_number, a.fi_last_name as fi, e.last_name as sc, a.sale_date, a.cit_paid_date, a.days_in_transit, 
--   case when a.fi_last_name = 'hse' then e.last_name else fi_last_name end as fi,
  case
    when b.sale_type = 'L' then d.lessor_name
    when b.sale_type = 'R' and deal_type = 'F' then c.lendor_name
  end as bank
from test_3 a
left join sls.ext_bopmast_partial b on a.stock_number = b.stock_number
left join arkona.ext_boplsrc c on b.store = c.company_number
  and b.lending_source = c.key
left join arkona.ext_bopssrc d on b.store = d.company_number
  and b.leasing_source = d.key  
left join sls.personnel e on b.primary_sc = e.ry1_id 
order by a.sale_date

-- jeri prefers single column for f/i

select a.year_month, a.stock_number, 
--   a.fi_last_name as fi, e.last_name as sc, 
  case when a.fi_last_name = 'hse' then e.last_name else fi_last_name end as fi,
  a.sale_date, a.cit_paid_date, a.days_in_transit
--   case
--     when b.sale_type = 'L' then d.lessor_name
--     when b.sale_type = 'R' and deal_type = 'F' then c.lendor_name
--   end as bank
from test_3 a
left join sls.ext_bopmast_partial b on a.stock_number = b.stock_number
left join arkona.ext_boplsrc c on b.store = c.company_number
  and b.lending_source = c.key
left join arkona.ext_bopssrc d on b.store = d.company_number
  and b.leasing_source = d.key  
left join sls.personnel e on b.primary_sc = e.ry1_id 
order by a.sale_date