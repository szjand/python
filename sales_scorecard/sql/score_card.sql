﻿Requires: consultants
          total sales (count)
          certified pre owned sales (count)
          new sales (count)
          total over allowance
          total f&i gross
---------------------------------------------------------------------------------------------------------------
--< consultants -----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
-- need a list of active consultants, some are on teams, some are not (olderbak...)
-- distinguish based on combination of distrib code and job
-- jobs offers nothing of use

-- ok, this turns out to be the same as team population, plus olderbak and dockendorf
drop table if exists consultants;
create temp table consultants as
select a.*, b.distcode, b.termdate, c.*
from sls.personnel a
left join ads.ext_edw_employee_dim b on a.employee_number = b.employeenumber
  and b.currentrow = true
left join jobs c on a.employee_number = c.employee_number  
where a.store_code = 'RY1'
  and termdate > current_date
  and distcode not in ('TEAM', 'BEN', 'STHP')
  and a.employee_number not in ('1148583','1151450', '1112410')-- exclude allen preston, wendi wheeler, george yunker)
order by last_name

-- need to include a house row
drop table if exists consultants;
create temp table consultants as
select a.*
from sls.personnel a
left join ads.ext_edw_employee_dim b on a.employee_number = b.employeenumber
  and b.currentrow = true
where a.store_code = 'RY1'
  and termdate > current_date
  and distcode not in ('TEAM', 'BEN', 'STHP')
  -- exclude allen preston, wendi wheeler, george yunker)
  and a.employee_number not in ('1148583','1151450', '1112410')
union
select *
from sls.personnel
where store_code = 'RY1'
  and employee_number = 'HSE';  
---------------------------------------------------------------------------------------------------------------
--/> consultants ----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------
--< total sales -----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
-- based on sls.deals_By_month 
-- nah, need to do individual deal rows
drop table if exists deals;
create table deals as
select a.store_code, a.year_month, a.employee_number, a.bopmast_id, a.stock_number, a.unit_count, 
  a.sale_group, a.vehicle_type, 
  b.over_allow, c.fi_gross
from (  
  select store_code, year_month, bopmast_id, stock_number,  unit_count, vehicle_type, sale_group, 
    psc_employee_number as employee_number
  from sls.deals_by_month 
  where year_month >= 201711    
  union all
  select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, vehicle_type, sale_group, 
    psc_employee_number
  from sls.deals_by_month 
  where year_month >= 201711    
    and ssc_employee_number <> 'none'
  union all
  select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, vehicle_type, sale_group, 
    ssc_employee_number
  from sls.deals_by_month 
  where year_month >= 201711    
    and ssc_employee_number <> 'none') a
-- over allow    
left join sls.ext_bopmast_partial b on a.store_code = b.store
  and a.bopmast_id = b.bopmast_id    
-- f/i gross  
left join sls.deals_gross_by_month c on a.year_month = c.year_month
  and a.stock_number = c.control; 
---------------------------------------------------------------------------------------------------------------
--/> total sales ----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------    
select * from deals limit 10

select a.last_name, a.first_name, b.total_sales, b.cpo_sales, b.new_sales,
  b.total_over_allowance, b.total_fi_gross
from consultants a
left join (
	select employee_number, 
	  sum(unit_count) as total_sales,
	  count(1 * unit_count) filter (where sale_group like '%CERT%') as cpo_sales,
	  count(1 * unit_count) filter (where vehicle_type = 'New') as new_sales,
	  sum(over_allow * unit_count)::integer as total_over_allowance,
	  sum(fi_gross)::integer as total_fi_gross -- already adjusted for backons
	from deals
	where year_month = 201801
	group by employee_number) b on a.employee_number = b.employee_number
order by a.last_name


select *
from deals a
inner join consultants b on a.employee_number = b.employee_number
where year_month = 201801
  and last_name = 'bedney'

-- 1/8/18
-- ben wants team names with consultants
-- convert to a function to replace sql script in py file

select * from sls.get_sales_scorecard(201711);
do
$$
declare 
  _year_month integer := 201801;
begin
  drop table if exists wtf;
  create temp table wtf as
select coalesce(bb.team, 'no team') as team, aa.last_name, aa.first_name, cc.total_sales, cc.cpo_sales, cc.new_sales,
  cc.total_over_allowance, cc.total_fi_gross
  from ( -- consultants
    select a.*
    from sls.personnel a
    left join ads.ext_edw_employee_dim b on a.employee_number = b.employeenumber
      and b.currentrow = true
    where a.store_code = 'RY1'
      and termdate > current_date
      and distcode not in ('TEAM', 'BEN', 'STHP')
      -- exclude allen preston, wendi wheeler, george yunker)
      and a.employee_number not in ('1148583','1151450', '1112410')
    union
    select *
    from sls.personnel
    where store_code = 'RY1'
      and employee_number = 'HSE') aa   
left join sls.team_personnel bb on aa.employee_number = bb.employee_number
  and bb.from_date < (select the_date from dds.dim_date where year_month = _year_month and last_day_of_month = true)
  and bb.thru_date >= (select the_date from dds.dim_date where year_month = _year_month and last_day_of_month = true)
  left join ( -- deals
    select employee_number, 
      sum(unit_count) as total_sales,
      count(1 * unit_count) filter (where sale_group like '%CERT%') as cpo_sales,
      count(1 * unit_count) filter (where vehicle_type = 'New') as new_sales,
      sum(over_allow * unit_count)::integer as total_over_allowance,
      sum(fi_gross)::integer as total_fi_gross -- already adjusted for unit count
    from (           
      select a.store_code, a.year_month, a.employee_number, a.bopmast_id, a.stock_number, a.unit_count, 
        a.sale_group, a.vehicle_type, 
        b.over_allow, c.fi_gross
      from (  
        select store_code, year_month, bopmast_id, stock_number,  unit_count, vehicle_type, sale_group, 
          psc_employee_number as employee_number
        from sls.deals_by_month 
        where year_month >= _year_month
        union all
        select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, 
          vehicle_type, sale_group, psc_employee_number
        from sls.deals_by_month 
        where year_month >= _year_month  
          and ssc_employee_number <> 'none'
        union all
        select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, 
          vehicle_type, sale_group, ssc_employee_number
        from sls.deals_by_month 
        where year_month >= _year_month 
          and ssc_employee_number <> 'none') a
      -- over allow    
      left join sls.ext_bopmast_partial b on a.store_code = b.store
        and a.bopmast_id = b.bopmast_id    
      -- f/i gross  
      left join sls.deals_gross_by_month c on a.year_month = c.year_month
        and a.stock_number = c.control) x 
    where year_month = _year_month
    group by employee_number) cc on aa.employee_number = cc.employee_number;       
end
$$;        
select * 
from wtf
order by team, last_name;   
      