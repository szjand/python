﻿-- ben wants the same thing for honda
-- consultants

alter table ads.dim_salesperson
rename to ext_dim_salesperson;     


select e.*, f.salespersonid as ry2_id
from (
  select b.storecode, b.employeenumber, b.lastname, b.firstname, c.salespersonid as ry1_id
  from ads.ext_edw_employee_dim b 
  inner join ads.ext_dim_salesperson c on b.employeenumber = c.employeenumber
  where b.currentrow = true
    and b.storecode = 'RY2'
    and b.termdate > current_date
    and b.distcode = 'SALE'
    and c.salespersontypecode = 'S'
    and c.storecode = 'RY2') e
left join (     
  select b.*, c.salespersonid
  from ads.ext_edw_employee_dim b 
  inner join ads.ext_dim_salesperson c on b.employeenumber = c.employeenumber
  where b.currentrow = true
    and b.storecode = 'RY2'
    and b.termdate > current_date
    and b.distcode = 'SALE'
    and c.salespersontypecode = 'S'
    and c.storecode = 'RY1') f on e.employeenumber = f.employeenumber
union
select 'RY2', 'HSE','HOUSE','HOUSE', 'HSE', 'HSE'            


do
$$
declare 
  _year_month integer := 201712;
begin
  drop table if exists wtf;
  create temp table wtf as
select -- coalesce(bb.team, 'no team') as team, 
  aa.last_name, aa.first_name, cc.total_sales, cc.cpo_sales, cc.new_sales,
  cc.total_over_allowance, cc.total_fi_gross
  from ( -- consultants
    select e.storecode, e.employeenumber as employee_number, e.lastname as last_name,
      e.firstname as first_name, e.ry1_id, f.salespersonid as ry2_id
    from (
      select b.storecode, b.employeenumber, b.lastname, b.firstname, c.salespersonid as ry1_id
      from ads.ext_edw_employee_dim b 
      inner join ads.ext_dim_salesperson c on b.employeenumber = c.employeenumber
      where b.currentrow = true
        and b.storecode = 'RY2'
        and b.termdate > current_date
        and b.distcode = 'SALE'
        and c.salespersontypecode = 'S'
        and c.storecode = 'RY2') e
    left join (     
      select b.*, c.salespersonid
      from ads.ext_edw_employee_dim b 
      inner join ads.ext_dim_salesperson c on b.employeenumber = c.employeenumber
      where b.currentrow = true
        and b.storecode = 'RY2'
        and b.termdate > current_date
        and b.distcode = 'SALE'
        and c.salespersontypecode = 'S'
        and c.storecode = 'RY1') f on e.employeenumber = f.employeenumber
     union
     select 'RY2', 'HSE','HOUSE','HOUSE', 'HSE', 'HSE') aa   
-- left join sls.team_personnel bb on aa.employee_number = bb.employee_number
--   and bb.from_date < (select the_date from dds.dim_date where year_month = _year_month and last_day_of_month = true)
--   and bb.thru_date >= (select the_date from dds.dim_date where year_month = _year_month and last_day_of_month = true)
  left join ( -- deals
    select employee_number, 
      sum(unit_count) as total_sales,
      count(1 * unit_count) filter (where sale_group like '%CERT%') as cpo_sales,
      count(1 * unit_count) filter (where vehicle_type = 'New') as new_sales,
      sum(over_allow * unit_count)::integer as total_over_allowance,
      sum(fi_gross)::integer as total_fi_gross -- already adjusted for unit count
    from (           
      select a.store_code, a.year_month, a.employee_number, a.bopmast_id, a.stock_number, a.unit_count, 
        a.sale_group, a.vehicle_type, 
        b.over_allow, c.fi_gross
      from (  
        select store_code, year_month, bopmast_id, stock_number,  unit_count, vehicle_type, sale_group, 
          psc_employee_number as employee_number
        from sls.deals_by_month 
        where year_month >= _year_month
          and store_code = 'RY2'
        union all
        select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, 
          vehicle_type, sale_group, psc_employee_number
        from sls.deals_by_month 
        where year_month >= _year_month  
          and ssc_employee_number <> 'none'
          and store_code = 'RY2'
        union all
        select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, 
          vehicle_type, sale_group, ssc_employee_number
        from sls.deals_by_month 
        where year_month >= _year_month 
          and ssc_employee_number <> 'none'
          and store_code = 'RY2') a
      -- over allow    
      left join sls.ext_bopmast_partial b on a.store_code = b.store
        and a.bopmast_id = b.bopmast_id    
      -- f/i gross  
      left join sls.deals_gross_by_month c on a.year_month = c.year_month
        and a.stock_number = c.control) x 
    where year_month = _year_month
    group by employee_number) cc on aa.employee_number = cc.employee_number;       
end
$$;        
select * 
from wtf
-- order by team, last_name;   
select sum(total_sales) from wtf

drop table if exists wtf1;
create temp table wtf1 as
select * from (
        select store_code, year_month, bopmast_id, stock_number,  unit_count, vehicle_type, sale_group, 
          psc_employee_number as employee_number
        from sls.deals_by_month 
        where year_month = 201712
          and store_code = 'RY2'
        union all
        select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, 
          vehicle_type, sale_group, psc_employee_number
        from sls.deals_by_month 
        where year_month = 201712
          and ssc_employee_number <> 'none'
          and store_code = 'RY2'
        union all
        select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, 
          vehicle_type, sale_group, ssc_employee_number
        from sls.deals_by_month 
        where year_month = 201712
          and ssc_employee_number <> 'none'    
          and store_code = 'RY2'  
) x where store_code = 'ry2'


-- not liking what i am getting for counts
-- try fs 
-- which is great for a closed month, but not so much for open month
-- well, let's go ahead and see if i can compare the 2 for a closed month
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201712 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201712 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;


select *
from step_1 a
left join sls.ext_bopmast_partial b on a.control = b.stock_number
where a.store = 'ry2'
order by 


select * from sls.ext_bopmast_partial limit 10

-- ahh, of course, it is all the intramarket wholesales !!
select *
from step_1 a
left join wtf b on a.control = b.stock_number
where a.store = 'ry2'
  and b.stock_number is null


seems to work ok
may need to fine tune the count ...
select * from sls.get_sales_scorecard_ry2(201801)  

-- 1/17/18
-- missing tanner trosen
-- ben wants consultant look to book added to spreadsheet


-- tanner
-- the issue is ext_dim_salesperson, employeenumber is still ry1, ry2: 2141642
select table_schema, table_name from information_schema.columns where column_name = 'employee_number' order by table_schema, table_name

when did he actually move to honda?: 12/1/17

select *
from ads.ext_edw_employee_dim
where lastname = 'trosen'

update ads.ext_dim_salesperson
set employeenumber = '2141642'
-- select * from ads.ext_dim_salesperson
where lastname = 'trosen'
-- got to fix up the sls. tables
update sls.personnel 
set employee_number = '2141642'
-- select * from sls.personnel
where last_name = 'trosen';

update sls.deals_by_month
set psc_employee_number = '2141642'
-- select * from sls.deals_by_month 
where psc_last_name = 'trosen'
  and year_month > 201711;

 

-- look to book -------------------------------------------------------------------------------------------------------------
ah shit, vehicleevaluationts gets updated with each change in status, so i do not necessarily know
when the eval was started, looks like i need vehicleitemstatuses
select a.vehicleevaluationts, 
from ads.ext_vehicle_evaluations a
left join ads.ext_people b on a.salesconsultantid = b.partyid
left join ads.ext_vehicle_inventory_item_statuses c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
where locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
order by vehicleevaluationts desc
limit 100

-- need vehicleitemstatuses in ads_for_luigi.uc_inventory in w10_python

-- look -- check for double counting
SELECT lastname, year_month, count(*) as look
from (
  SELECT a.vehicleitemid, d.lastname, aa.year_month 
  FROM ads.ext_vehicle_item_statuses a
  inner join dds.dim_date aa on a.fromts::date = aa.the_date
  INNER JOIN ads.ext_vehicle_evaluations b on a.tablekey = b.VehicleEvaluationID 
    AND b.locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
  LEFT JOIN ads.ext_people d on b.salesconsultantid = d.partyid
  WHERE a.category = 'VehicleEvaluation' 
    AND aa.year_month > 201710
  group by a.vehicleitemid, d.lastname, aa.year_month) x 
group by lastname, year_month
  
-- book
SELECT d.lastname, aa.year_month, count(*) as book
FROM ads.ext_vehicle_item_statuses a
inner join dds.dim_date aa on a.fromts::date = aa.the_date
INNER JOIN ads.ext_vehicle_evaluations b on a.tablekey = b.VehicleEvaluationID 
  AND b.locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
left JOIN ads.ext_vehicle_inventory_items c on b.VehicleInventoryItemID = c.VehicleInventoryItemID   
LEFT JOIN ads.ext_people d on b.salesconsultantid = d.partyid
WHERE a.category = 'VehicleEvaluation' 
  AND aa.year_month > 201710
  AND a.status = 'VehicleEvaluation_Booked'
GROUP BY d.lastname, aa.year_month

-- this looks ok
-- make sure vins are not being double counted
 select x.*, y.book, z.employee_number
 from ( -- look
  SELECT lastname, firstname, year_month, count(*) as look
  from ( -- group to get single row per vehicle
    SELECT a.vehicleitemid, d.lastname, d.firstname, aa.year_month 
    FROM ads.ext_vehicle_item_statuses a
    inner join dds.dim_date aa on a.fromts::date = aa.the_date
    INNER JOIN ads.ext_vehicle_evaluations b on a.tablekey = b.VehicleEvaluationID 
      AND b.locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
    LEFT JOIN ads.ext_people d on b.salesconsultantid = d.partyid
    WHERE a.category = 'VehicleEvaluation' 
      AND aa.year_month > 201710
    group by a.vehicleitemid, d.lastname, d.firstname, aa.year_month) d 
  group by lastname, firstname, year_month) x
left join ( -- book
  SELECT d.lastname, d.firstname, aa.year_month, count(*) as book
  FROM ads.ext_vehicle_item_statuses a
  inner join dds.dim_date aa on a.fromts::date = aa.the_date
  INNER JOIN ads.ext_vehicle_evaluations b on a.tablekey = b.VehicleEvaluationID 
    AND b.locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
  left JOIN ads.ext_vehicle_inventory_items c on b.VehicleInventoryItemID = c.VehicleInventoryItemID   
  LEFT JOIN ads.ext_people d on b.salesconsultantid = d.partyid
  WHERE a.category = 'VehicleEvaluation' 
    AND aa.year_month > 201710
    AND a.status = 'VehicleEvaluation_Booked'
  GROUP BY d.lastname, d.firstname, aa.year_month) y on x.lastname = y.lastname and x.firstname = y.firstname and x.year_month = y.year_month
left join sls.personnel z on x.lastname = z.last_name  and x.firstname = z.first_name

select * from sls.get_sales_scorecard_ry2(201711)
===============================================================================================================================
-- this is the body of the new function that includes look to book
select aa.last_name, aa.first_name, cc.total_sales, 
  cc.cpo_sales::numeric(3,1), cc.new_sales::numeric(3,1),
  cc.total_over_allowance, cc.total_fi_gross, look, book
  from ( -- consultants
    select e.storecode, e.employeenumber as employee_number, e.lastname as last_name,
      e.firstname as first_name, e.ry1_id, f.salespersonid as ry2_id
    from (
      select b.storecode, b.employeenumber, b.lastname, b.firstname, c.salespersonid as ry1_id
      from ads.ext_edw_employee_dim b 
      inner join ads.ext_dim_salesperson c on b.employeenumber = c.employeenumber
      where b.currentrow = true
        and b.storecode = 'RY2'
        and b.termdate > current_date
        and b.distcode = 'SALE'
        and c.salespersontypecode = 'S'
        and c.storecode = 'RY2') e
    left join (     
      select b.*, c.salespersonid
      from ads.ext_edw_employee_dim b 
      inner join ads.ext_dim_salesperson c on b.employeenumber = c.employeenumber
      where b.currentrow = true
        and b.storecode = 'RY2'
        and b.termdate > current_date
        and b.distcode = 'SALE'
        and c.salespersontypecode = 'S'
        and c.storecode = 'RY1') f on e.employeenumber = f.employeenumber
     union
     select 'RY2', 'HSE','HOUSE','HOUSE', 'HSE', 'HSE') aa   
  left join ( -- deals
    select employee_number, 
      sum(unit_count) as total_sales,
      count(1 * unit_count) filter (where sale_group like '%CERT%') as cpo_sales,
      count(1 * unit_count) filter (where vehicle_type = 'New') as new_sales,
      sum(over_allow * unit_count)::integer as total_over_allowance,
      sum(fi_gross)::integer as total_fi_gross, -- already adjusted for unit count
      max(look) as look, max(book) as book
    from (           
      select a.store_code, a.year_month, a.employee_number, a.bopmast_id, a.stock_number, a.unit_count, 
        a.sale_group, a.vehicle_type, 
        b.over_allow, c.fi_gross, look, book
      from (  
        select store_code, year_month, bopmast_id, stock_number,  unit_count, vehicle_type, sale_group, 
          psc_employee_number as employee_number
        from sls.deals_by_month 
        where year_month = _year_month
          and store_code = 'RY2'
        union all
        select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, 
          vehicle_type, sale_group, psc_employee_number
        from sls.deals_by_month 
        where year_month = _year_month
          and ssc_employee_number <> 'none'
          and store_code = 'RY2'
        union all
        select store_code, year_month, bopmast_id, stock_number,  unit_count * .5 as unit_count, 
          vehicle_type, sale_group, ssc_employee_number
        from sls.deals_by_month 
        where year_month = _year_month
          and ssc_employee_number <> 'none'
          and store_code = 'RY2') a
      -- over allow    
      left join sls.ext_bopmast_partial b on a.store_code = b.store
        and a.bopmast_id = b.bopmast_id    
      -- f/i gross  
      left join sls.deals_gross_by_month c on a.year_month = c.year_month
        and a.stock_number = c.control       
      left join ( -- look to book
         select x.*, y.book, z.employee_number
         from ( -- look
          SELECT lastname, firstname, year_month, count(*) as look
          from (
            SELECT a.vehicleitemid, d.lastname, d.firstname, aa.year_month 
            FROM ads.ext_vehicle_item_statuses a
            inner join dds.dim_date aa on a.fromts::date = aa.the_date
            INNER JOIN ads.ext_vehicle_evaluations b on a.tablekey = b.VehicleEvaluationID 
              AND b.locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
            LEFT JOIN ads.ext_people d on b.salesconsultantid = d.partyid
            WHERE a.category = 'VehicleEvaluation' 
              AND aa.year_month = _year_month
            group by a.vehicleitemid, d.lastname, d.firstname, aa.year_month) d 
          group by lastname, firstname, year_month) x
        left join ( -- book
          SELECT d.lastname, d.firstname, aa.year_month, count(*) as book
          FROM ads.ext_vehicle_item_statuses a
          inner join dds.dim_date aa on a.fromts::date = aa.the_date
          INNER JOIN ads.ext_vehicle_evaluations b on a.tablekey = b.VehicleEvaluationID 
            AND b.locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
          left JOIN ads.ext_vehicle_inventory_items c on b.VehicleInventoryItemID = c.VehicleInventoryItemID   
          LEFT JOIN ads.ext_people d on b.salesconsultantid = d.partyid
          WHERE a.category = 'VehicleEvaluation' 
            AND aa.year_month = _year_month
            AND a.status = 'VehicleEvaluation_Booked'
          GROUP BY d.lastname, d.firstname, aa.year_month) y on x.lastname = y.lastname and x.firstname = y.firstname and x.year_month = y.year_month
        left join sls.personnel z on x.lastname = z.last_name  and x.firstname = z.first_name) zz on a.year_month = zz.year_month
          and a.employee_number = zz.employee_number) x 
    where year_month = _year_month
    group by employee_number) cc on aa.employee_number = cc.employee_number 
order by last_name;    