# GM SCD test server
# Server: gmmft-pp.ext.gm.com
# Username: mmftprydell
# Password: rfcJdxEFH1l
# Destination Folder: MFTP13830


# Below is your Production NEW SFTP information, effective immediately:
# Server: gmmft.ext.gm.com
# Username: pmftprydell
# Password: MX1xdenHW78
# Mailbox: MFTP13830

# FileName: G1RYD3270W4S.csv

# Headers: VIN,"STOCK NUMBER",YEAR,MAKE,MODEL,"MODEL CODE",TRIM,"BODY STYLE",TRANSMISSION,
# DRIVE_TRAIN,DOORS,EXTERIOR_COLOR,INTERIOR_COLOR,MSRP,INVOICE,PRICE,"CITY MPG",
# "HIGHWAY MPG",HORSE_POWER,MILEAGE,TYPE,CREATE,LAST_MODIFIED,"IMAGE URL",CERTIED,MISC_PRICE

# finally got the fucking file to generate with the correct double quoting, ie, an attribute with
# a space in it gets double quoted
# http://stackoverflow.com/questions/22485940/python-csv-writer-is-adding-quotes-when-not-needed
# You have to change the quotechar to s.th. else than " :

# import adsdb
import pyodbc
import csv
import paramiko

# print csv.list_dialects()

# class gmscd(csv.excel):
#     doublequote = False

# csv.register_dialect('gmscd')
#                      quoting=csv.QUOTE_NONE)
# , delimiter=',', doublequote=False, quotechar="", lineterminator='\r\n',
#                      quoting=csv.QUOTE_NONE)
# adsCon = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\advantage\\dds\\dds.add',
#                        userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
#                        TrimTrailingSpaces='TRUE')
adsCon = pyodbc.connect('DSN=dds; UID=adssys; PWD=cartiva')

adsCursor = adsCon.cursor()

filepath = '/MFTP13830/G1RYD3270W4S.csv'
localpath = 'files/G1RYD3270W4S.csv'

adsCursor.execute("""
    SELECT imvin, imstk#, imyear, immake,
      iif(position(' ' in trim(immodl)) > 0, '"' + trim(immodl) + '"', immodl),
      -- immodl,
      immcode,'',
      iif(position(' ' in trim(imbody)) > 0, '"' + trim(imbody) + '"', imbody),
      --imbody,
      '','','',
      iif(position(' ' in trim(imcolr)) > 0, '"' + trim(imcolr) + '"', imcolr),
      --imcolr,
      '',
      cast(msrp AS sql_integer) AS msrp, round(cast(invoice AS sql_integer),0) AS invoice,
      cast(coalesce(bestprice,0) + coalesce(rebate,0) as sql_integer) AS price,
      '','','',
      imodom, 'N', '', '', '', '', ''
    FROM (
      SELECT a.imvin,a.imstk#,a.imyear,a.immake,a.immcode,a.immodl,a.imbody,a.imcolr,
        a.imodom,a.impric AS msrp,
        sum(CASE WHEN b.iotype = 'c' AND b.ioseq# = 5 THEN ionval END) AS invoice,
        sum(CASE WHEN b.iotype = 'n' AND b.ioseq# = 9 THEN ionval END) AS bestprice,
        sum(CASE WHEN b.iotype = 'n' AND b.ioseq# = 12 THEN ionval END) AS rebate
      FROM stgarkonainpmast a
      INNER JOIN stgArkonainpoptf b ON a.imvin = b.iovin
      WHERE a.imtype = 'n'
        AND a.imstat = 'i'
        AND LEFT(a.imstk#,1) <> 'H'
        -- and a.immodl = 'malibu limited'
      GROUP BY a.imvin,a.imstk#,a.imyear,a.immake,a.immcode,a.immodl,a.imbody,a.imcolr,
        a.imodom,a.impric) x
""")
# for row in adsCursor.fetchall():
# row[4] = 'fuck you'
# print row.immodl
# if row.immodl.rstrip().find(' ') > 0:
#     row.immodl = '"' + row.immodl.rstrip() + '"'
#     with open(localpath, 'wb') as f:
#         csv.writer(f).writerows(row)s
# print row
# print adsCursor.description
# print row.imvin

with open(localpath, 'wb') as f:
    # header row
    csv.writer(f, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL).writerow(
        ['VIN', '"STOCK NUMBER"', 'YEAR', 'MAKE', 'MODEL', '"MODEL CODE"', 'TRIM', '"BODY STYLE"',
         'TRANSMISSION', 'DRIVE_TRAIN', 'DOORS', 'EXTERIOR_COLOR', 'INTERIOR_COLOR', 'MSRP',
         'INVOICE', 'PRICE', '"CITY MPG"', '"HIGHWAY MPG"', 'HORSE_POWER', 'MILEAGE', 'TYPE',
         'CREATE', 'LAST_MODIFIED', '"IMAGE URL"', 'CERTIFIED', 'MISC_PRICE'])
    csv.writer(f, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL).writerows(adsCursor.fetchall())

# with open(localpath, 'rb') as f:
#     for row in csv.reader(f):
#         print row
#         print ','.join(row)

# with open(localpath, 'w') as f:
#     for row in adsCursor.fetchall():
#         print row
#         f.write("%s\n" % str(row))

adsCursor.close()
adsCon.close()

transport = paramiko.Transport('gmmft-pp.ext.gm.com', 22)
transport.connect(username='mmftprydell', password='rfcJdxEFH1l')
sftp = paramiko.SFTPClient.from_transport(transport)

sftp.put(localpath, filepath)

sftp.close()
transport.close()
