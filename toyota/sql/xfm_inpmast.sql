﻿DROP INDEX arkona.xfm_inpmast_inpmast_vin_idx1;



DROP INDEX arkona.xfm_inpmast_inpmast_vin_row_thru_date_idx

CREATE UNIQUE INDEX ON arkona.xfm_inpmast(inpmast_company_number, inpmast_vin, row_thru_date);

select inpmast_company_number, inpmast_vin
from arkona.ext_inpmast
group by inpmast_company_number, inpmast_vin
having count(*) > 1


select inpmast_company_number, inpmast_vin, row_thru_date
from arkona.xfm_inpmast
group by inpmast_company_number, inpmast_vin, row_thru_date
having count(*) > 1

comment on table arkona.xfm_inpmast is '7/12/22 with the addition of the toyota store as RY8, it became necessary to change the unique index on
inpmast_vin/row_thru_date to inpmast_company_number/inpmast_vin/row_thru_date';


now after adding the uniquely RY8 vins
where are we
i am tempted to exclude the vins from ry8 that already exist in inpmast, the data is less complete by far
every one will trigger a change because of the different data (at least the company number for sure), and i dont think i want
the ry8 version to be the most recent version, although the real problem is i have no way to tell, initially,
which of the 2 versions is the most recent
the thought of trying to wedge the ry8 version into the sequene of xmf_inpmast is ridiculous

and going forward, if i can figure how to do it, it wont be a problem

-------------------------------------------------------------------------------------------
--< go ahead and make the change to include inpmast_company number in arkona.xfm_inpmast_changed_rows and the unique index in xfm_inpmast
-------------------------------------------------------------------------------------------

DROP TABLE arkona.xfm_inpmast_changed_rows;

CREATE TABLE arkona.xfm_inpmast_changed_rows
(
  store citext not null,
  vin citext NOT NULL,
  CONSTRAINT xfm_inpmast_changed_rows_pkey PRIMARY KEY (store,vin)
);

Then modified Function: arkona.xfm_inpmast()

---------- everything below here is convincing me to do it
-- so my fuzzys notion is to simply change the structure of arkona.xfm_inpmast_changed_rows to include the
-- company number
-- but it is still too fuzzy
-- i am stuck on the updating the current_row attribute in arkona.xfm_inpmast to false
-- how do i identify the rows
-- if the vin exists in both stores,  i would necessarily need to just update the one that matches the 
-- vin AND store in arkona.xfm_inpmast_changed_rows
-- so it looks like i am stuck on refactoring this query
-- update current version of changed row 
update arkona.xfm_inpmast 
set current_row = false,
    row_thru_date = current_date - 1
where inpmast_vin in (select vin from arkona.xfm_inpmast_changed_rows)
  and current_row = true; 

-- try it with 2 vins
-- 3N1CB51D76L622308 exists in ry1
-- 3TMCZ5AN8JM169319 exists in ry1 & ry8
-- looks like this works
update arkona.xfm_inpmast a
set current_row = false,
    row_thru_date = current_date - 1
from (
  select *
  from changed_rows
  where vin in ('3N1CB51D76L622308','3TMCZ5AN8JM169319'))  b
where a.inpmast_company_number = b.inpmast_company_number
  and a.inpmast_vin = b.vin
  and a.current_row;

update arkona.xfm_inpmast
set current_row = true, row_thru_date = '12/31/9999'
where inpmast_key in (1229632,1112238)

  
drop table if exists changed_rows;
create temp table changed_rows as
select a.inpmast_vin as vin, a.inpmast_company_number
from (
  select inpmast_vin, inpmast_company_number,
      (
        select md5(z::text) as hash
        from (
          select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
            status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
            year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
            turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
            radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
            date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
            warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
            gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
            key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
            inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
            sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
            common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
            updated_timestamp,stocked_company,engine_hours
          from arkona.ext_inpmast
          where inpmast_company_number = aa.inpmast_company_number
            and inpmast_vin = aa.inpmast_vin) z)
  from arkona.ext_inpmast aa)  a
inner join arkona.xfm_inpmast b on a.inpmast_vin = b.inpmast_vin
  and a.inpmast_company_number = b.inpmast_company_number
  and a.hash <> b.hash
  and b.current_row = true;

-------------------------------------------------------------------------------------------
--/> go ahead and make the change to include inpmast_company number in arkona.xfm_inpmast_changed_rows and the unique index in xfm_inpmast
-------------------------------------------------------------------------------------------
  
------------------------------------------------------------------------------
--< thinking of doing an initial load of ry8 vins 
------------------------------------------------------------------------------
07/12/22 did it
select count(distinct inpmast_vin) from arkona.xfm_inpmast where inpmast_company_number = 'RY8'
-- only those that do not currentle exist in xfm_inpmast
select inpmast_vin
from arkona.ext_inpmast  a
where inpmast_company_number = 'RY8'
  and not exists (
    select 1
    from arkona.ext_inpmast
    where inpmast_company_number = 'RY1'
      and inpmast_vin = a.inpmast_vin)

-- verifies that the only 2 company numbers are RY1 & RY8
select inpmast_company_number, count(*)
from arkona.ext_inpmast
group by inpmast_company_number

-- new rows
insert into arkona.xfm_inpmast (
  inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  row_from_Date, current_row,hash)
select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
  status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
  year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
  turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
  radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
  date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
  warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
  gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
  key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
  inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
  sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
  common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
  updated_timestamp,stocked_company,engine_hours,
  -- skip inpmast_key: serial
  current_date - 1 as row_from_date,  -- make yesterday the from date
  -- skip row_thru_date: default value
  true as current_row,
    (
      select md5(z::text) as hash
      from (
        select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
          status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
          year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
          turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
          radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
          date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
          warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
          gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
          key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
          inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
          sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
          common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
          updated_timestamp,stocked_company,engine_hours
        from arkona.ext_inpmast
        where inpmast_company_number = a.inpmast_company_number
          and inpmast_vin = a.inpmast_vin) z)
from arkona.ext_inpmast a
where inpmast_vin in (
	select inpmast_vin
	from arkona.ext_inpmast  a
	where inpmast_company_number = 'RY8'
		and not exists (
			select 1
			from arkona.ext_inpmast
			where inpmast_company_number = 'RY1'
				and inpmast_vin = a.inpmast_vin))
------------------------------------------------------------------------------
--/> thinking of doing an initial load of ry8 vins 
------------------------------------------------------------------------------