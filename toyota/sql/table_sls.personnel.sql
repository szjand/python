﻿select * 
from sls.personnel limit 5

select * from sls.personnel where last_name = 'michael'

MIC          F ANTHONY MICHAEL  8100200   gsm                               
BCO          S BRADLEY CONSOLE  8100204   sc                           
CSU          S COLE SUEDEL      8100229   sc                          
DRI          F DAVID RIDDLE     8100222   fin mgr                                
HSE          S HOUSE DEAL                                         
999          F HOUSE DEAL                                         
JMA          S JACE MARION      8100218   sc                                  
JME          M JASON MELTON     8100219   sls mgr                                
JTU          S JAY TURNER                                         
JAT          F JAY TURNER                                         
JDG          F JORDYN GREER                                       
JGR          S JORDYN GREER  
MHA          S MASON HAGEN      8100207  sc                                
TMO          F THOMAS MOORE  		8100220  fin mgr

alter table sls.personnel
add column ry8_id citext;

update sls.personnel
set ry8_id = 'JGR'
where employee_number = '169628';

update sls.personnel
set store_code = 'RY8',
    ry8_id = 'none',
    employee_number = '8100200'
where employee_number = '195460';

insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,
  ry1_id,ry2_id,fi_id,is_senior,anniversary_date,first_full_month_emp,ry8_id)
select 'RY8',employee_number,last_name,first_name,'07/12/2022','none','none','none',false,'07/12/2022',202208,'BCO'
from ukg.employees
where employee_number = '8100204';

insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,
  ry1_id,ry2_id,fi_id,is_senior,anniversary_date,first_full_month_emp,ry8_id)
select 'RY8',employee_number,last_name,first_name,'07/12/2022','none','none','none',false,'07/12/2022',202208,'CSU'
from ukg.employees
where employee_number = '8100229';

insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,
  ry1_id,ry2_id,fi_id,is_senior,anniversary_date,first_full_month_emp,ry8_id)
select 'RY8',employee_number,last_name,first_name,'07/12/2022','none','none','DRI',false,'07/12/2022',202208,'none'
from ukg.employees
where employee_number = '8100222';	

insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,
  ry1_id,ry2_id,fi_id,is_senior,anniversary_date,first_full_month_emp,ry8_id)
select 'RY8',employee_number,last_name,first_name,'07/12/2022','none','none','none',false,'07/12/2022',202208,'JMA'
from ukg.employees
where employee_number = '8100218';	

insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,
  ry1_id,ry2_id,fi_id,is_senior,anniversary_date,first_full_month_emp,ry8_id)
select 'RY8',employee_number,last_name,first_name,'07/12/2022','none','none','none',false,'07/12/2022',202208,'JME'
from ukg.employees
where employee_number = '8100219';	

insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,
  ry1_id,ry2_id,fi_id,is_senior,anniversary_date,first_full_month_emp,ry8_id)
select 'RY8',employee_number,last_name,first_name,'07/12/2022','none','none','none',false,'07/12/2022',202208,'MHA'
from ukg.employees
where employee_number = '8100207';	

insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,
  ry1_id,ry2_id,fi_id,is_senior,anniversary_date,first_full_month_emp,ry8_id)
select 'RY8',employee_number,last_name,first_name,'07/12/2022','none','none','none',false,'07/12/2022',202208,'TMO'
from ukg.employees
where employee_number = '8100220';			

select * from sls.personnel where store_code = 'RY8'																									