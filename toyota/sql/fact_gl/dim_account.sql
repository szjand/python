﻿-- need to get the toyota accounts into dim_Account
i think the pattern will be to
1. do a one time catch up load
2. modify fact_gl.py to include toyota

select *
-- select  count(*)
from arkona.ext_glpmast
where company_number = 'RY8'
limit 10

select company_number, count(*)
from arkona.ext_glpmast
group by company_number

select distinct company_number
from arkona.ext_glpmast
where account_sub_type is null

select company_number, count(*)
from arkona.ext_glpmast
where account_number is not null 
  and coalesce(account_sub_type, 'X') <> 'C'
  and year = 2022
group by company_number 

-- sql_xfm
-- this looks like a reasonable mod for the sql_xfm query which populates arkona.xfm_glpmast
-- checked counts by store
select * from arkona.xfm_glpmast limit 20
drop table if exists xfm_query cascade;
create temp table xfm_query as
            select account_number, account_type, account_desc,
              case -- this handles the lack of intercompany, eg, no account_sub_type for toyota
                when account_sub_type = 'A' then 'RY1'
                when account_sub_type = 'B' then 'RY2'
                when company_number = 'RY8' then 'RY8'
                else 'XXX'
              end as store_code,
              department,
              case typical_balance
                when 'D' then 'Debit'
                when 'C' then 'Credit'
                else 'X'
              end
            from arkona.ext_glpmast
            where coalesce(account_sub_type, 'X') <> 'C' -- crookston accounts
              and company_number in('RY1','RY2','RY8') -- exclude RY6, RY7
              and account_number is not null
              and -- some doubled accounts in orig query
                case
                  when company_number = 'RY8' then year > 2021
                  else true
               end
            group by company_number,  account_number, account_type, account_desc, account_sub_type,
              department, typical_balance    
order by company_number, account_number;                

select * from xfm_query where store_code = 'RY8' and department is null
update xfm_query set department = 'GN'
where account_number in ('2471','2473','3122','3132','3223','9136');
update xfm_query set department = 'NC'
where account_number = '9144';

-- and the one time back fill
-- issue with null dept code
select distinct department from xfm_query

insert into arkona.xfm_glpmast
select * from xfm_query where store_code = 'RY8'  

----------------------------------------------------------
-- dim_account
-- looks clean, just add toyota to the store case
            insert into fin.dim_account (account, account_type_code, account_type, description,
              store_code, store, department_code, department, typical_balance, current_row,
              row_from_date, row_reason)
            select a.account, a.account_type_code, c.account_type,
              a.description, a.store_code,
              case a.store_code
                when 'RY1' then 'Rydell GM'
                when 'RY2' then 'Honda Nissan'
                When 'RY8' Then 'Toyota'
              end,
              a.department_code, 
              case
                when a.store_code = 'RY2' and a.department_code = 'RE' then 'Detail'
                else b.dept_description
              end,
              a.typical_balance,
              true,
              (select first_of_month from dds.dim_date where the_date = current_date - 1),
              'new account created by controller'
            from (
              select account, account_type_code, description, store_code, department_code, typical_balance
              from arkona.xfm_glpmast z
              where not exists (
                select 1
                from fin.dim_account
                where account = z.account)) a          
            left join arkona.ext_glpdept b on a.department_code = b.department_code
              and a.store_code = b.company_number
            left join dds.lkp_account_types c on a.account_type_code = c.account_type_code; 


select * from fin.dim_account where store_code = 'RY8'  

-------------------------------------------------------------------------------------
-- glptrns          
select * from fin.fact_gl limit 100

select min(trans), max(trans) from fin.fact_gl 

select b.the_date, a.*
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
where a.trans between 1446413 and 1447413

-- so, approx 11.5 years to do 4 million transactions 
-- looking at db2, ~2.25 mill transactions/year
-- so toyota starting with transaction 1000005, should not run into a collision in transaction numbers

select * from arkona.ext_glptrns_tmp limit 100

select distinct gtco_ from arkona.ext_glptrns_tmp

select min(gttrn_), max(gttrn_), gtco_
from arkona.ext_glptrns_tmp
group by gtco_

select distinct left(account,1) from fin.dim_Account where store_code = 'RY8'
-- toyota has accounts that start with a 3,  so i can't continue to use th crookston exemption
select b.the_date, c.account, a.*
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
join fin.dim_account c on a.account_key = c.account_key
  and left(c.account, 1) = '3'
order by b.the_date desc
limit 10  

-- add RY8 to where clause
-- since the nightly scrape is only doing 45 days, no need to filter for crookston (3xxxx account numbers)
-- sql_xfm
--             insert into {0}
-- the initial load, 2500 rows inserted
						insert into arkona.xfm_glptrns
            select a.*, md5(a::text) as hash
            from (
              select gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none') as gtjrnl, gtdate, gtacct,
                coalesce(gtctl_,'none'), coalesce(gtdoc_,'none'), coalesce(gtref_,'none'), 
                gttamt::numeric(12,2), coalesce(gtdesc, 'NONE')
              from arkona.ext_glptrns_tmp
              where gtco_ in ('RY8')
                and gtpost in ('Y','V')
              group by gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, gtacct, gtctl_,
                gtdoc_, gtref_, gttamt, gtdesc) a;
              


---------------------------------------------------------------------------
-- dim_gl_description
-- need arkona.xfm_glpmast first
select * from arkona.xfm_glptrns limit 10 where company_number = 'RY8' limit 100
-- for the initial feed, use the date from xfm_glptrns, no changes required in the python query
-- initial feed, multiple rows with vin as description, use min(the_date)
-- which means this query needs to be grouped rather than distinct
                    Insert into fin.dim_gl_description (description, row_from_date, current_row, row_reason)
                    select description, min(the_date), true, 'added from nightly etl'
                    from arkona.xfm_glptrns a
                    where not exists (
                      select 1
                      from fin.dim_gl_description
                      where description = a.description)
                    group by (description)

----------------------------------------------------------------
-- fact_gl
-- looks like no changes required either for the inital load or luigi
                    insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
                      account_key,control,doc,ref,amount,gl_description_key)
                    select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
                      e.post_status, g.journal_key, h.datekey,
                      coalesce(i.account_key, j.account_key) as account_key,
                      e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
                    from arkona.xfm_glptrns e
                    left join fin.fact_gl ee on e.trans = ee.trans
                      and e.seq = ee.seq
                    left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
                      and e.the_date between f.row_from_date and f.row_thru_date
                    left join fin.dim_doc_type ff on 1 = 1
                      and ff.doc_type_code = 'none'
                    left join fin.dim_journal g on e.journal_code = g.journal_code
                      and e.the_date between g.row_from_date and g.row_thru_date
                    left join dds.day h on e.the_date = h.thedate
                    left join fin.dim_account i on e.account = i.account
                      and e.the_date between i.row_from_date and i.row_thru_date
                    left join fin.dim_account j on 1 = 1
                      and j.account = 'none'
                    left join fin.dim_gl_description m on e.description = m.description
                    where ee.trans is null;

!! Done !! 07/21/22 2:09PM                