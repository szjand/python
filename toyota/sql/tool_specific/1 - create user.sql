06/17/22
made a copy of dpsvseries AND stored it IN D:\advantage\toyota
i will WORK against that copy AS i TRY to figure out what ALL will be necessary
to ADD toyota AS a new location IN the tool
the first approach i am taking IS to start with doing an evaluation
need a toyota sc, but first need to ADD rows to parties,organizations & partyrelationships

SELECT * FROM parties
INSERT INTO parties (partyid,typ)
SELECT (SELECT newidstring(d) FROM system.iota), 'Party_Organization' FROM system.iota;
SELECT * FROM organizations


SELECT * FROM partyrelationships WHERE partyid2 = 'B6183892-C79D-4489-A58C-B526DF948B06'
--------------------------------------------------------------------------------
--< first ADD the new party, organization, PartyRelationship
--------------------------------------------------------------------------------
DECLARE @PartyID string;
DECLARE @FullName string;
DECLARE @NowTS timestamp;
DECLARE @Typ string;
DECLARE @Market string;
DECLARE @MarketID string;
DECLARE @PartyRelationshipTyp string;

@FullName = 'GF-Toyota';
@Market = 'GF-Grand Forks';
@PartyID = (SELECT newidstring(d) FROM system.iota);
@NowTS = Now();
@Typ = 'Party_Organization';
@MarketID = (SELECT partyid FROM organizations WHERE fullname = @Market);
@PartyRelationshipTyp = 'PartyRelationship_MarketLocations'; 

   
BEGIN TRANSACTION;
TRY
  INSERT INTO parties VALUES(@PartyID, @Typ);
  INSERT INTO Organizations(PartyID, FullName, Name, FromTS) 
    VALUES(@PartyID, @FullName, 'Toyota', @NowTS);
  INSERT INTO PartyRelationships(PartyID1, PartyID2, Typ, FromTS)
    VALUES(@MarketID, @PartyID, @PartyRelationshipTyp, @NowTS);
COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END;

-- SELECT * FROM organizations WHERE fullname = 'GF-Toyota'

-- delete FROM partyrelationships WHERE partyid2 = '5a1ee3ac-6c7c-8c43-8e00-ebfda8209eee';
-- DELETE FROM organizations WHERE partyid = '5a1ee3ac-6c7c-8c43-8e00-ebfda8209eee';
-- DELETE FROM parties WHERE partyid = '5a1ee3ac-6c7c-8c43-8e00-ebfda8209eee';
--------------------------------------------------------------------------------
--/> first ADD the new party, organization, PartyRelationship
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--< CREATE a toyota sc:  jtoyota / qcdfis5y
--------------------------------------------------------------------------------
/*
'GF-Crookston'
'GF-Honda Cartiva'
'GF-Rydells'
'Privilege_Internet'
'Privilege_InvMgr'
'Privilege_Recon'
'Privilege_SC'
'Privilege_SalesManager'
'Privilege_AdjustRecon'
'Privilege_Biller'
'Privilege_WholesaleWithinMarket'
Market: 'GF-Grand Forks' 
*/


DECLARE @NewpartyID string;
DECLARE @Firstname string;
DECLARE @Lastname string;
DECLARE @Fullname string;
DECLARE @email string;
DECLARE @NowTS timestamp;
DECLARE @COUNT integer;
DECLARE @Username string;
DECLARE @Marketname string;
DECLARE @MarketID string;
DECLARE @Location string;
DECLARE @LocationID string;
DECLARE @Privilege string;

@NewpartyID = newidstring(D);
@Firstname = 'Jon';
@Lastname = 'Toyota';
@Fullname = 'Jon Toyota';
@email = ''; 
@Username = 'jtoyota';
@Marketname = 'GF-Grand Forks';
@Location = 'GF-Toyota';
@Privilege = 'Privilege_SC';
@MarketID = (SELECT PartyId FROM Organizations WHERE fullname = @Marketname);
@LocationID = (SELECT PartyID FROM Organizations WHERE fullname = @Location);
@NowTS = Now();

  IF @Firstname IS NULL OR @Firstname = '' THEN
    RAISE PartyException( 101, 'Missing first name');
  END IF;
  IF @Lastname IS NULL OR @Lastname = '' THEN
    RAISE PartyException( 102, 'Missing last name');
  END IF;  
  IF @Fullname IS NULL OR @Fullname = '' THEN
    RAISE UserException( 103, 'Missing full name');
  END IF;
  IF @Fullname <> TRIM(@FirstName) + ' ' + TRIM(@LastName) THEN
    RAISE UserException(104, 'FullName does not match First & Last');
  END IF;
  IF UPPER(@UserName) <> UPPER(LEFT(TRIM(@FirstName),1) + TRIM(@LastName)) THEN 
    RAISE UserException(115, 'UserName NOT first initial AND last name');
  END IF;
  IF @Username IS NULL OR @Username = '' THEN
    RAISE UserException( 105, 'Missing user name');
  END IF; 
  IF @Marketname IS NULL OR @Marketname = '' THEN
    RAISE UserException( 106, 'Missing market name');
  END IF;
  IF @Location IS NULL OR @Location = '' THEN
    RAISE UserException(107, 'Missing Location');
  END IF;  
  IF (SELECT fullname FROM People WHERE fullname = @Fullname) IS NOT NULL THEN
    RAISE PartyException(109, 'This person''s fullname already exists');
  END IF;    
  IF (SELECT username FROM users WHERE username = @Username) IS NOT NULL THEN 
    RAISE UserException( 110, 'Username already exists'); 
  END IF; 
  IF (SELECT Description FROM ContactMechanisms WHERE Description = @email) IS NOT NULL THEN
    RAISE UserException(112, 'This email is already in use');
  END IF;
     
  IF @MarketID IS NULL THEN
    RAISE UserException( 113, 'Market name does not decode to an existing organization');
  END IF;  
  IF @LocationID IS NULL THEN
    RAISE UserException( 114, 'Location name does not decode to an existing organization');
  END IF; 
  
BEGIN TRANSACTION; 
TRY
  INSERT INTO Parties 
    VALUES(@NewpartyID, 'Party_Person');
    
  INSERT INTO People(PartyID, Fullname, Firstname, Lastname, FromTS)
    VALUES(@NewPartyID, @Fullname, @Firstname, @Lastname, @NowTS);  
       
  IF @email IS NOT NULL AND @email <> '' THEN
    INSERT INTO ContactMechanisms (Typ, PartyID, Description, FromTS)
	  VALUES('ContactMechanism_WorkEmail', @NewpartyID, @email, @NowTS);
  END IF;
  
  INSERT INTO Users (PartyID, UserName, Password, DefaultMarketID, TimeOutMinutes, Active)
    VALUES(@NewPartyID, @Username, 'password', @MarketID, 120, True);
    
  INSERT INTO ApplicationUsers(PartyID, AppName, AppSecurityLevel, AppTimeOutMinutes, FromTS)
    VALUES(@NewPartyID, 'portalvseries', 2, 120, @NowTS);
  INSERT INTO ApplicationUsers(PartyID, AppName, AppSecurityLevel, AppTimeOutMinutes, FromTS)
    VALUES(@NewPartyID, 'inventory', 2, 120, @NowTS);	
    
  INSERT INTO PartyRelationShips (PartyID1, PartyID2, Typ, FromTS)
    VALUES(@MarketID, @NewPartyID, 'PartyRelationship_MarketToolUsers', @NowTS); 
    
  INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
    VALUES (@NewpartyID, @LocationID, @Privilege, @NowTS);
  COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END;  

--------------------------------------------------------------------------------
--/> CREATE a toyota sc:  jtoyota / qcdfis5y
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--< ADD Privilege_SalesManager to toyota sc jtoyota
--------------------------------------------------------------------------------
started an evaluation AS jtoyota, but WHEN i go to evals, only 1 shows
TRY adding access to the other stores for jtoyota
that made ALL the evals show up
so, ADD honda AS well
but with only sc privilege, can't finish the eval
ADD sales manager AS well, first only toyota
but, with only toyota, only the toyota eval shows up, so ADD ALL 3

-- ADD privilege
DECLARE @Username string;
DECLARE @PartyID string;
DECLARE @Privilege string;
DECLARE @LocationID string; 
DECLARE @NowTS timestamp;
DECLARE @Location string;

@NowTS = Now();
@Username = 'jtoyota';
-- @Privilege = 'Privilege_SC';
@Privilege = 'Privilege_SalesManager';
-- @Location = 'GF-Rydells';
@Location = 'GF-Honda Cartiva';
-- @Location = 'GF-Toyota';
@LocationID = (SELECT PartyID FROM Organizations WHERE Fullname = @Location);
@PartyID = (SELECT PartyID FROM users WHERE username = @Username AND active = true);

  IF (SELECT COUNT(*) FROM Users WHERE username = @username) = 0 THEN
    RAISE UserException( 101, 'Bad Username');
  END IF;
  IF (SELECT COUNT(*) FROM Privileges WHERE privilege = @privilege) = 0 THEN
    RAISE UserException( 102, 'Bad Privilege');
  END IF; 
  IF @LocationID IS NULL OR @LocationID = '' THEN
    RAISE UserException( 103, 'Missing LocationID');
  END IF;   
  IF @PartyID IS NULL OR @PartyID = '' THEN
    RAISE UserException( 104, 'No PartyID');
  END IF;
-- UPDATE partyPrivileges SET thruTS = @nowTS WHERE partyID = @partyID 
--   AND privilege = @privilege AND locationid = @locationid;  
  INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
    VALUES (@PartyID, @LocationID, @Privilege, @NowTS);
--------------------------------------------------------------------------------
--/> ADD Privilege_SalesManager to toyota sc jtoyota
--------------------------------------------------------------------------------

