--------------------------------------------------------------------------------
--< 07/11/22 walks explode on pricing->package DROP down
/*
07/06/2022
time to start populating the production database with toyota data
based on the queries:
  1 - create user.sql
	2 - evaluation.sql
	3 - inspection - walk.sql
	users_needing_access.sql
*/

--------------------------------------------------------------------------------
--< 07/18-20/22 UPDATE window sticker data for Toyota
--------------------------------------------------------------------------------
/*
!!! SUMMARY !!!
project for adding images to window sticker: E:\python_projects\toyota\delphi_advantage_pictures\ToyotaWindowStickers.dproj
the 2 location images are: 
  E:\python_projects\toyota\delphi_advantage_pictures\toyota_store_logos\rydell-toyota_v1.1_sqr-prim_200x60.jpg
	E:\python_projects\toyota\delphi_advantage_pictures\toyota_store_logos\rydell-toyota_v1.1_sqr-prim_400x140.jpg

the store hours are an rtf stored AS a blob IN LocationWindowStickerInfo
*/
/*
toyota logos
Vector: https://www.dropbox.com/sh/uztpnn5f3tbs1fs/AAABYSOYJCU4oCTFx4cJn0Qea?dl=0
Print: https://www.dropbox.com/sh/tq26iybyy75zo1d/AABUo4rPq08bVhGWUZgQwgjca?dl=0
Digital: https://www.dropbox.com/sh/9i4fwqosvoietcs/AADakp5ZlV2KR9hd9QIxV0Fma?dl=0
*/
SELECT * 
FROM LocationWindowStickerInfo

UPDATE locationWindowStickerInfo
SET locationphonenumber = '(701)787-6259'
where locationid = '77a18168-998f-8747-ae24-3efc540cd069'; 

UPDATE locationWindowStickerInfo
SET locationid = 'toyota'
where locationid = '77a18168-998f-8747-ae24-3efc540cd069'; 

UPDATE locationWindowStickerInfo
SET locationid = '77a18168-998f-8747-ae24-3efc540cd069'
where locationid = 'toyota'; 

UPDATE locationWindowStickerInfo
SET locationdisclaimer = (SELECT locationdisclaimer FROM locationWindowStickerInfo WHERE locationid = 'B6183892-C79D-4489-A58C-B526DF948B06')
where locationid = '77a18168-998f-8747-ae24-3efc540cd069'; 

DELETE FROM locationWindowStickerInfo where locationid = '77a18168-998f-8747-ae24-3efc540cd069';
DELETE FROM locationWindowStickerInfo where locationid = 'toyota';



now i have t remember how to deal with pictures IN advantage
first download the existing picture to a file
TRY it IN Win 7 VM XE-vm10 with the projects IN ancient_history\data\source\VehiclePictures

SELECT * from LocationImages where LocationID = 'B6183892-C79D-4489-A58C-B526DF948B06' and ThruTS is null

select * 
FROM locationimages
WHERE thruts IS NULL
  AND locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
	AND typ = 'ImageType_StoreLogo'
	AND typ = 'ImageType_GMCERTIFIED'
	
select * 
FROM locationimages
WHERE thruts IS NULL
  AND locationid = '77a18168-998f-8747-ae24-3efc540cd069'
		
-- 1st attempt at store logo, size = 200x200		
-- it did NOT show up on the window sticker, so TRY the other sizes that exist
-- for the gm storeLogo
-- 400x400 nope
UPDATE locationimages
SET size = '200x60'
WHERE locationid = '77a18168-998f-8747-ae24-3efc540cd069'
AND typ = 'ImageType_StoreLogo';

UPDATE locationimages
SET size = '300x105'
WHERE locationid = '77a18168-998f-8747-ae24-3efc540cd069'
AND typ = 'ImageType_StoreLogo';

UPDATE locationimages
SET size = '400x140'
WHERE locationid = '77a18168-998f-8747-ae24-3efc540cd069'
AND typ = 'ImageType_StoreLogo';

-- going round AND round, lets tryt USING gm store logo AND see IF it prints
-- on toyota window sticker
DELETE FROM locationimages
WHERE locationid = '77a18168-998f-8747-ae24-3efc540cd069'
  AND typ = 'ImageType_StoreLogo';
	
yep, it worked, both images largish at the top smallish at the bottom LEFT T10041P	
INSERT INTO locationimages(locationid,typ,size,image,fromts)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',typ,size,image,fromts
FROM locationimages
WHERE locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
	AND typ = 'ImageType_StoreLogo'
	
-- save the image AS a jpeg: rydell-toyota_v1.1_sqr-prim.jpg
-- yep, that worked, at least for the top pic, though it IS too tall, so dick with the sizing	
-- resized the "large" pic to 278 x 140, but saved AS 400x140
-- now save the 200x60
-- AND the window sticker now prints with both logos, kind of crappy, but, at least its toyota
--------------------------------------------------------------------------------
--< 07/18/22 UPDATE window sticker data for Toyota
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--< 07/12/22 no images on window stickers AND hang tags
--------------------------------------------------------------------------------

SELECT * FROM organizations WHERE name = 'toyota'  -- '77a18168-998f-8747-ae24-3efc540cd069'

-- first suspect table
SELECT * 
FROM LocationWindowStickerInfo
-- copying honda, this got nice care, servidept phone & link verbiage, but no  nice care picture
INSERT INTO LocationWindowStickerInfo
SELECT '77a18168-998f-8747-ae24-3efc540cd069',  locationphonenumber,locationdisclaimer,locationservicesalesinfo,locationurl
FROM LocationWindowStickerInfo
WHERE locationid = '4CD8E72C-DC39-4544-B303-954C99176671'

SELECT * FROM locationimages WHERE typ LIKE '%NiceCare' 
--copy honda
-- this also took care of the nice car logo on the hang tags
INSERT INTO locationimages(locationid,typ,size,image,fromts)
SELECT '77a18168-998f-8747-ae24-3efc540cd069', typ, size, image,now()
FROM locationimages 
WHERE thruts IS NULL 
  AND typ LIKE '%NiceCare' 
	AND locationID = '4CD8E72C-DC39-4544-B303-954C99176671'
	
--------------------------------------------------------------------------------
--< 07/12/22 no images on window stickers AND hang tags
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--< 07/11/22 walks explode on pricing->package DROP down
--------------------------------------------------------------------------------
looks LIKE i need to populate TABLE packcerts

this IS FROM SP GetDefaultStrategyNameForLocationPackageMake AND returns null
	     SELECT DefaultStrategy 
      FROM PacksCerts pc 
      WHERE pc.Typ = 'ReconPackage_Nice'
        AND pc.LocationID = '77a18168-998f-8747-ae24-3efc540cd069'
        AND pc.ThruTS IS NULL;
				
copy honda
SELECT * 
FROM PacksCerts				
WHERE locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
  AND thruts IS NULL
	
INSERT INTO PacksCerts (locationid,typ,pack,certificationfee,defaultstrategy,packpriceminimum, fromts)
SELECT '77a18168-998f-8747-ae24-3efc540cd069', typ,pack,certificationfee,defaultstrategy,packpriceminimum, now()
FROM PacksCerts				
WHERE locationid = '4CD8E72C-DC39-4544-B303-954C99176671'
  AND thruts IS NULL

--------------------------------------------------------------------------------
--/> 07/11/22 walks explode on pricing->package DROP down
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--< first ADD the new party, organization, PartyRelationship
--------------------------------------------------------------------------------
DECLARE @PartyID string;
DECLARE @FullName string;
DECLARE @NowTS timestamp;
DECLARE @Typ string;
DECLARE @Market string;
DECLARE @MarketID string;
DECLARE @PartyRelationshipTyp string;

@FullName = 'GF-Toyota';
@Market = 'GF-Grand Forks';
@PartyID = (SELECT newidstring(d) FROM system.iota);
@NowTS = Now();
@Typ = 'Party_Organization';
@MarketID = (SELECT partyid FROM organizations WHERE fullname = @Market);
@PartyRelationshipTyp = 'PartyRelationship_MarketLocations'; 

   
BEGIN TRANSACTION;
TRY
  INSERT INTO parties VALUES(@PartyID, @Typ);
  INSERT INTO Organizations(PartyID, FullName, Name, FromTS) 
    VALUES(@PartyID, @FullName, 'Toyota', @NowTS);
  INSERT INTO PartyRelationships(PartyID1, PartyID2, Typ, FromTS)
    VALUES(@MarketID, @PartyID, @PartyRelationshipTyp, @NowTS);
COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END;


SELECT * FROM organizations WHERE fullname = 'GF-Toyota'
Name: Toyota
PartyID: '77a18168-998f-8747-ae24-3efc540cd069'
--------------------------------------------------------------------------------
--/> first ADD the new party, organization, PartyRelationship
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--< Reconditioning SELECT Recon Package DROP down need factory for Toyota
--------------------------------------------------------------------------------	
on AppraisalDataCollection.aspx there IS a Location dropdown populated BY sp GetLocationsByPartyIDPrivileges
The locationid IS the field organizations.PartyID
-- TODO: made up the ImageType AND defaultStrategy
INSERT INTO locationfranchisemakes(locationid,make,certificationfee,pack,newfranchise,
  usedfranchise,defaultstrategy,fromts,imagetype)
SELECT (SELECT partyid FROM organizations WHERE fullname = 'GF-Toyota'),'TOYOTA',399,500,true,
  true,'Competitive',now(), 'ImageType_NoImage'
FROM system.iota;	


SELECT * FROM locationfranchisemakes

-- Needs a record IN LocationDifferentiatorPictures		
-- TODO dummied up a DifferentiatorDescription BY copying part of GM		
INSERT INTO LocationDifferentiatorPictures(locationid,reconpackage,make,DifferentiatorDescription)
SELECT '77a18168-998f-8747-ae24-3efc540cd069','ReconPackage_Factory','Toyota',
  'Toyota Certified Pre-Owned. 2-Year/30,000-Mile Standard CPO Maintenance Plan. 12-Month/12,000-Mile'
FROM system.iota;	
--------------------------------------------------------------------------------
--/> Reconditioning SELECT Recon Package
--------------------------------------------------------------------------------	

--------------------------------------------------------------------------------
--< Booking Parked At DROP down, need locations for Toyota
--------------------------------------------------------------------------------	
-- per cahalan, start out with a single toyota location: Toyota
-- copy the GM locations that are used at Honda
-- 7/13/22 more location suggested BY anthony
DpsDM.GetVehiclePhysicalLocationsByLocationID(LocationID)
execute procedure GetVehiclePhysicalLocationsByLocationID

SELECT * FROM LocationPhysicalVehicleLocations WHERE locationPartyID = '4CD8E72C-DC39-4544-B303-954C99176671' and active

INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Toyota' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Honda South' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Demo at GM' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Demo at Honda Nissan' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Rydell Body Shop' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Midas' FROM system.iota;
-- new locations requested BY Anthony
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Toyota new car lot' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Toyota East of building' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Toyota South (back) of building' FROM system.iota;
-- i think Anthony IS asking for these toyota location to also show up for GM/Honda
-- gm
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT 'B6183892-C79D-4489-A58C-B526DF948B06',false,true,'Toyota new car lot' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT 'B6183892-C79D-4489-A58C-B526DF948B06',false,true,'Toyota East of building' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT 'B6183892-C79D-4489-A58C-B526DF948B06',false,true,'Toyota South (back) of building' FROM system.iota;
--honda
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '4CD8E72C-DC39-4544-B303-954C99176671',false,true,'Toyota new car lot' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '4CD8E72C-DC39-4544-B303-954C99176671',false,true,'Toyota East of building' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '4CD8E72C-DC39-4544-B303-954C99176671',false,true,'Toyota South (back) of building' FROM system.iota;
-- toyota requests
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Rydell Detail' FROM system.iota;
INSERT INTO LocationPhysicalVehicleLocations (locationpartyid,detailrequired,active,locationshortname)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',false,true,'Toyota Service' FROM system.iota;
--------------------------------------------------------------------------------
--/> Booking Parked At DROP down, need locations for Toyota
--------------------------------------------------------------------------------	

SELECT * FROM VehicleInventoryItems WHERE stocknumber = 'G44208B'
--------------------------------------------------------------------------------
--< walk pricing strategy DROP down
--------------------------------------------------------------------------------
UsedCarsDM.GetVehiclePricingStrategiesByLocation(ViewState[LocationID].ToString)
EXECUTE PROCEDURE GetVehiclePricingStrategiesByLocation

SELECT * FROM pricingstrategies WHERE thruts IS null
/*
-- Tget rid of bogus pricing strategies (need to DO this IN production, get rid of the KBB pricing strategy)
delete FROM pricingstrategies WHERE pricingstrategyid IS NULL OR pricingstrategyid = ''
*/

-- TODO just copied honda nissan strategy numbers
INSERT INTO pricingStrategies(pricingstrategyid,pricingstrategyname,locationid,
  lowendmargin,highendmargin,fromts)
SELECT newidstring("d"),'Nuts','77a18168-998f-8747-ae24-3efc540cd069',1000,2000,now()
FROM system.iota;
INSERT INTO pricingStrategies(pricingstrategyid,pricingstrategyname,locationid,
  lowendmargin,highendmargin,fromts)
SELECT newidstring("d"),'Competitive','77a18168-998f-8747-ae24-3efc540cd069',3000,3800,now()
FROM system.iota;
INSERT INTO pricingStrategies(pricingstrategyid,pricingstrategyname,locationid,
  lowendmargin,highendmargin,fromts)
SELECT newidstring("d"),'Aggressive','77a18168-998f-8747-ae24-3efc540cd069',2800,3200,now()
FROM system.iota;

	
SELECT PricingStrategyName, PricingStrategyID 
FROM PricingStrategies
WHERE LocationID = '77a18168-998f-8747-ae24-3efc540cd069'
  AND ThruTS IS null
ORDER BY LowEndMargin;

--------------------------------------------------------------------------------
--/> walk pricing strategy DROP down
--------------------------------------------------------------------------------

