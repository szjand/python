--------------------------------------------------------------------------------
--< ADD Privilege_Recon & Privilege_InvMgr to toyota user jtoyota
--------------------------------------------------------------------------------
-- ADD privilege
DECLARE @Username string;
DECLARE @PartyID string;
DECLARE @Privilege string;
DECLARE @LocationID string; 
DECLARE @NowTS timestamp;
DECLARE @Location string;

@NowTS = Now();
@Username = 'jtoyota';
-- @Privilege = 'Privilege_Recon';
@Privilege = 'Privilege_InvMgr';
@Location = 'GF-Toyota';
@LocationID = (SELECT PartyID FROM Organizations WHERE Fullname = @Location);
@PartyID = (SELECT PartyID FROM users WHERE username = @Username AND active = true);

  IF (SELECT COUNT(*) FROM Users WHERE username = @username) = 0 THEN
    RAISE UserException( 101, 'Bad Username');
  END IF;
  IF (SELECT COUNT(*) FROM Privileges WHERE privilege = @privilege) = 0 THEN
    RAISE UserException( 102, 'Bad Privilege');
  END IF; 
  IF @LocationID IS NULL OR @LocationID = '' THEN
    RAISE UserException( 103, 'Missing LocationID');
  END IF;   
  IF @PartyID IS NULL OR @PartyID = '' THEN
    RAISE UserException( 104, 'No PartyID');
  END IF;
  INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
    VALUES (@PartyID, @LocationID, @Privilege, @NowTS);
--------------------------------------------------------------------------------
--/> ADD Privilege_Recon to toyota user jtoyota
--------------------------------------------------------------------------------		

--------------------------------------------------------------------------------
--< walk pricing strategy DROP down
--------------------------------------------------------------------------------
UsedCarsDM.GetVehiclePricingStrategiesByLocation(ViewState[LocationID].ToString)
EXECUTE PROCEDURE GetVehiclePricingStrategiesByLocation

SELECT * FROM pricingstrategies WHERE thruts IS null
/*
-- TODO get rid of bogus pricing strategies (need to DO this IN production, get rid of the KBB pricing strategy)
delete FROM pricingstrategies WHERE pricingstrategyid IS NULL OR pricingstrategyid = ''
*/

-- TODO just copied honda nissan strategy numbers
INSERT INTO pricingStrategies(pricingstrategyid,pricingstrategyname,locationid,
  lowendmargin,highendmargin,fromts)
SELECT newidstring("d"),'Nuts','2debbfbe-a6af-5545-b012-4bd536d16181',1000,2000,now()
FROM system.iota;
INSERT INTO pricingStrategies(pricingstrategyid,pricingstrategyname,locationid,
  lowendmargin,highendmargin,fromts)
SELECT newidstring("d"),'Competitive','2debbfbe-a6af-5545-b012-4bd536d16181',3000,3800,now()
FROM system.iota;
INSERT INTO pricingStrategies(pricingstrategyid,pricingstrategyname,locationid,
  lowendmargin,highendmargin,fromts)
SELECT newidstring("d"),'Aggressive','2debbfbe-a6af-5545-b012-4bd536d16181',2800,3200,now()
FROM system.iota;

	
SELECT PricingStrategyName, PricingStrategyID 
FROM PricingStrategies
WHERE LocationID = '2debbfbe-a6af-5545-b012-4bd536d16181'
  AND ThruTS IS null
ORDER BY LowEndMargin;


