/*
06/30/22
per ben cahalan, ADD toyota access to ALL currently existing users with 
the same level of access that they currently have IN other stores

07/06/22
  this becomes the production query
	just talked to Wilkie, he IS planning on evaluating ALL the toyota inventory
	today, so i need to DO his access first
*/

----------------------------------------------------------------------
--< 07/09/22 production, time to DO it for real
----------------------------------------------------------------------
-- users with access to toyota: to exclude FROM mass update
SELECT b.username, a.*
FROM partyprivileges a
JOIN users b on a.partyid = b.partyid
WHERE locationid = '77a18168-998f-8747-ae24-3efc540cd069';

-- DROP TABLE #temp_users;
SELECT DISTINCT a.username, a.partyid
INTO #temp_users
--SELECT COUNT(*)
FROM users a
--JOIN people b on a.partyid = b.partyid
JOIN partyprivileges c on a.partyid = c.partyid AND c.thruts IS null
WHERE cast(a.lastaccess AS sql_date) > '05/31/2022' -- = curdate()
  and a.username NOT IN ('jon','dwilkie','bknudson')
	AND a.active;
	
SELECT * FROM #temp_users;
	
-- this gives each user with a privilege IN any store the same privilege IN toyota
-- playing IN db toyota first
-- ADD privilege
DECLARE @LocationID string;
DECLARE @now timestamp;
@now = now();
@LocationID = (SELECT PartyID FROM Organizations WHERE name = 'toyota');
--DROP TABLE #test1;
INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
SELECT a.partyid, @LocationID AS locationid, c.privilege, @now AS fromts --, a.username
-- SELECT COUNT(*)
-- INTO #test1
FROM users a
--JOIN people b on a.partyid = b.partyid
JOIN partyprivileges c on a.partyid = c.partyid AND c.thruts IS null
WHERE cast(a.lastaccess AS sql_date) > '05/31/2022' -- = curdate()
  and a.username NOT IN ('jon','dwilkie','bknudson')
	AND a.active
GROUP BY a.partyid, c.privilege, a.username



-- after executing the above, this IS what it looks like
SELECT distinct u.username, left(pe.fullname, 25) AS name, 
  pp.Privilege, 
  (SELECT name FROM organizations WHERE partyid = pp.locationid) AS location
FROM Parties p
JOIN People pe ON pe.PartyID = p.PartyID
JOIN users u ON u.partyid = p.partyid
  AND u.active = true
JOIN applicationusers au ON au.partyid = u.partyid AND au.ThruTS IS NULL 
JOIN PartyPrivileges pp ON pp.partyid = p.partyid AND pp.ThruTS IS NULL 
wHERE username IN (
  SELECT username
	FROM #temp_users)  ORDER BY name, location, privilege 	
	
GOOD ENOUGH FOR GETTING STARTED	
----------------------------------------------------------------------
--/> 07/09/22 production, time to DO it for real
----------------------------------------------------------------------	
	
	
	
SELECT * FROM vehicleitems WHERE vin = '4T1G11AK9LU889195'

i need to clean up the users, get rid of termed, etc
for current users, need to show their current access

-- FROM allTables
SELECT distinct p.PartyID, u.username, left(pe.fullname, 25) AS name, 
  pp.Privilege, 
  (SELECT name FROM organizations WHERE partyid = pp.locationid) AS location
FROM Parties p
LEFT JOIN People pe ON pe.PartyID = p.PartyID
LEFT JOIN users u ON u.partyid = p.partyid
  AND u.active = true
LEFT JOIN applicationusers au ON au.partyid = u.partyid AND au.ThruTS IS NULL 
LEFT JOIN PartyPrivileges pp ON pp.partyid = p.partyid AND pp.ThruTS IS NULL 
wHERE username = 'dwilkie' ORDER BY location, privilege 


-- ADD privilege
DECLARE @Username string;
DECLARE @PartyID string;
DECLARE @Privilege string;
DECLARE @LocationID string; 
DECLARE @NowTS timestamp;
DECLARE @Location string;

@NowTS = Now();
@Username = 'jon';
@Privilege = 'Privilege_InvMgr';
@Location = 'GF-Toyota';
@LocationID = (SELECT PartyID FROM Organizations WHERE Fullname = @Location);
@PartyID = (SELECT PartyID FROM users WHERE username = @Username AND active = true);

  IF (SELECT COUNT(*) FROM Users WHERE username = @username) = 0 THEN
    RAISE UserException( 101, 'Bad Username');
  END IF;
  IF (SELECT COUNT(*) FROM Privileges WHERE privilege = @privilege) = 0 THEN
    RAISE UserException( 102, 'Bad Privilege');
  END IF; 
  IF @LocationID IS NULL OR @LocationID = '' THEN
    RAISE UserException( 103, 'Missing LocationID');
  END IF;   
  IF @PartyID IS NULL OR @PartyID = '' THEN
    RAISE UserException( 104, 'No PartyID');
  END IF;
  INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
    VALUES (@PartyID, @LocationID, @Privilege, @NowTS);


--------------------------------------------------------------------------------
--< clean up the people/users tables
--------------------------------------------------------------------------------
SELECT a.username, CAST(a.lastaccess AS sql_date), a.active, 
  b.firstname, b.lastname, 
	c.first_name, c.last_name, c.term_date
-- SELECT COUNT(*)
FROM users a
LEFT JOIN people b on a.partyid = b.partyid
LEFT JOIN ukg.employees c on b.firstname = c.first_name
  AND b.lastname = c.last_name
WHERE a.active
  AND c.first_name IS NULL 

select * FROM people WHERE lastname = 'symons'	
--UPDATE firstname AND lastname IN people TABLE FROM ukg.employees
UPDATE people SET firstname = 'Steven' 
where partyid = (select partyid from users where username = 'ssymons' and active);
UPDATE people SET firstname = 'Katherine', lastname = 'Blumhagen'
where partyid = (select partyid from users where username = 'kblumhagen' and active);
UPDATE people SET firstname = 'James' 
where partyid = (select partyid from users where username = 'jwarmack' and active);
UPDATE people SET firstname = 'David' 
where partyid = (select partyid from users where username = 'dwilkie' and active);
UPDATE people SET firstname = 'Thomas' 
where partyid = (select partyid from users where username = 'taubol' and active);
UPDATE people SET firstname = 'Benjamin' 
where partyid = (select partyid from users where username = 'bknudson' and active);
UPDATE people SET firstname = 'Michael' 
where partyid = (select partyid from users where username = 'mdelohery' and active);
UPDATE people SET firstname = 'Benjamin' 
where partyid = (select partyid from users where username = 'bcahalan' and active);
UPDATE people SET lastname = 'Pederson' 
where partyid = (select partyid from users where username = 'dpeterson' and active);
UPDATE people SET lastname = 'Van Heste II' 
where partyid = (select partyid from users where username = 'fvanheste' and active);
UPDATE people SET lastname = 'Barta' 
where partyid = (select partyid from users where username = 'jtandeski' and active);
UPDATE people SET lastname = 'Sutherland' 
where partyid = (select partyid from users where username = 'bsutherland' and active);

--------------------------------------------------------------------------------
--/> clean up the people/users tables
--------------------------------------------------------------------------------

-- likely active users	
SELECT a.username, CAST(a.lastaccess AS sql_date), a.active, 
  b.firstname, b.lastname, 
	c.first_name, c.last_name, c.term_date
-- SELECT COUNT(*)
FROM users a
LEFT JOIN people b on a.partyid = b.partyid
LEFT JOIN ukg.employees c on b.firstname = c.first_name
  AND b.lastname = c.last_name
WHERE a.active
ORDER BY lastaccess

		
-- folks that have transferred BETWEEN stores		
SELECT a.username, CAST(a.lastaccess AS sql_date), a.active, 
  b.firstname, b.lastname, 
	c.first_name, c.last_name, c.term_date
-- SELECT COUNT(*)
FROM users a
LEFT JOIN people b on a.partyid = b.partyid
LEFT JOIN ukg.employees c on b.firstname = c.first_name
  AND b.lastname = c.last_name
WHERE a.active
  AND coalesce(c.term_date, curdate()) < curdate()

SELECT a.username, CAST(a.lastaccess AS sql_date), a.active, 
  b.firstname, b.lastname, 
	c.first_name, c.last_name, c.term_date
-- SELECT COUNT(*)
FROM users a
LEFT JOIN people b on a.partyid = b.partyid
LEFT JOIN ukg.employees c on b.firstname = c.first_name
  AND b.lastname = c.last_name
WHERE a.active
ORDER BY CAST(a.lastaccess AS sql_date)

SELECT distinct u.username, left(pe.fullname, 25) AS name, 
  pp.Privilege, 
  (SELECT name FROM organizations WHERE partyid = pp.locationid) AS location
FROM Parties p
JOIN People pe ON pe.PartyID = p.PartyID
JOIN users u ON u.partyid = p.partyid
  AND u.active = true
JOIN applicationusers au ON au.partyid = u.partyid AND au.ThruTS IS NULL 
JOIN PartyPrivileges pp ON pp.partyid = p.partyid AND pp.ThruTS IS NULL 
wHERE username = 'dwilkie' ORDER BY location, privilege 



start with a list based on most recent access
AND get the access (privileges) for each user
it doesnt matter for which store(s) the user has privileges, they will each be
	 getting the same privilege for toyota
	 
	 
SELECT b.fullname, a.username, c.privilege
-- SELECT COUNT(*)
FROM users a
JOIN people b on a.partyid = b.partyid
-- JOIN applicationusers c on a.partyid = c.partyid AND c.thruts IS NULL
JOIN partyprivileges c on a.partyid = c.partyid AND c.thruts IS null
WHERE cast(a.lastaccess AS sql_date) = curdate()
  and a.username NOT IN ('jon')
GROUP BY b.fullname, a.username, c.privilege
ORDER BY a.username

-- 07/09/22 production

SELECT b.username, a.*
FROM partyprivileges a
JOIN users b on a.partyid = b.partyid
WHERE locationid = '77a18168-998f-8747-ae24-3efc540cd069'

select * FROM users
-- these are the users
-- DROP TABLE #temp_users;
SELECT DISTINCT a.username, a.partyid
INTO #temp_users
--SELECT COUNT(*)
FROM users a
--JOIN people b on a.partyid = b.partyid
JOIN partyprivileges c on a.partyid = c.partyid AND c.thruts IS null
WHERE cast(a.lastaccess AS sql_date) > '05/31/2022' -- = curdate()
  and a.username NOT IN ('jon','dwilkie','bknudson')
	AND a.active

	-- this IS their store/privilege
	-- so after running the script below, these users should have the same privileges IN toyota
SELECT b.username, a.privilege, c.name
FROM partyprivileges a
JOIN #temp_users b on a.partyid = b.partyid
JOIN organizations c on a.locationid = c.partyid
WHERE a.thruts IS NULL
ORDER BY b.username, a.privilege, c.name
	
-- these are the only privileges IN dpsvseries currently for toyota
-- probably best to DELETE these before running the script
-- nononono already have toyota evaluations with these partyids 
-- exclude them WHEN i DO the bulk UPDATE THEN ADD inidividual stuff IF needed
SELECT b.username, a.* 
FROM partyprivileges a
JOIN users b on a.partyid = b.partyid
WHERE a.locationid = '77a18168-998f-8747-ae24-3efc540cd069'
	
-- this gives each user with a privilege IN any store the same privilege IN toyota
-- playing IN db toyota first
-- ADD privilege
DECLARE @LocationID string;
DECLARE @now timestamp;
@now = now();
@LocationID = (SELECT PartyID FROM Organizations WHERE name = 'toyota');
--DROP TABLE #test1;
INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
SELECT a.partyid, @LocationID AS locationid, c.privilege, @now AS fromts --, a.username
-- SELECT COUNT(*)
-- INTO #test1
FROM users a
--JOIN people b on a.partyid = b.partyid
JOIN partyprivileges c on a.partyid = c.partyid AND c.thruts IS null
WHERE cast(a.lastaccess AS sql_date) > '05/31/2022' -- = curdate()
  and a.username NOT IN ('jon','dwilkie')
	AND a.active
GROUP BY a.partyid, c.privilege, a.username
ORDER BY a.username


-- after executing the above, this IS what it looks like
SELECT distinct u.username, left(pe.fullname, 25) AS name, 
  pp.Privilege, 
  (SELECT name FROM organizations WHERE partyid = pp.locationid) AS location
FROM Parties p
JOIN People pe ON pe.PartyID = p.PartyID
JOIN users u ON u.partyid = p.partyid
  AND u.active = true
JOIN applicationusers au ON au.partyid = u.partyid AND au.ThruTS IS NULL 
JOIN PartyPrivileges pp ON pp.partyid = p.partyid AND pp.ThruTS IS NULL 
wHERE username IN (
  SELECT username
	FROM #temp_users)  ORDER BY name, location, privilege 



DELETE  
FROM partyprivileges 
WHERE locationid = '2debbfbe-a6af-5545-b012-4bd536d16181'

SELECT b.username, a.* 
FROM partyprivileges a
JOIN users b on a.partyid = b.partyid
WHERE locationid = '77a18168-998f-8747-ae24-3efc540cd069'

select * FROM users WHERE username = 'bknudson'  -- ad1eb5b8-b95e-d444-b5f9-ef909f1577d4
-- of course, ben knudson needs access right away
INSERT INTO partyprivileges (partyid,locationid,privilege,fromts)
SELECT 'ad1eb5b8-b95e-d444-b5f9-ef909f1577d4','77a18168-998f-8747-ae24-3efc540cd069','Privilege_InvMgr', now()
FROM system.iota

