﻿/*
so the issue is an overlap of bopmast.bopmast_record_key between RY1 and RY8
dim_customer has a unique index on attribute bnkey (bopmast.bopmast_record_key)
luckily, currently, dim customer only gets records from RY1 & RY2
otherwise, existing records would have been overwritten by the introduction of bnkey values for RY8
so, both bopmast.ext_bopname and xfm_bopname have unique indexes on company_number/bopname_record_key
*/
--------------------------------------------------------------------------
--< the story
--------------------------------------------------------------------------
1018021 does not exist in dim_customer, so, sls.deals_by_month have a null for the buyer
select * 
from arkona.ext_bopmast
where bopmast_stock_number = 'T10086'

select * 
from arkona.xfm_bopname 
where bopname_record_key = 1018021

select bopname_company_number, count(*)
from arkona.ext_bopname
group by bopname_company_number

select * 
from dds.dim_customer
where bnkey = 1018021	

1024120 & 1024121 exist in dim_customer (RY1) therefore the wrong buyer shows in sls.deals_by_month
select * 
from arkona.ext_bopmast
where bopmast_stock_number = 'T10072P'

select * 
from arkona.xfm_bopname 
where bopname_record_key in ( 1024120,1024121)

select * 
from dds.dim_customer
where bnkey in ( 1024120,1024121)
--------------------------------------------------------------------------
--/> the story
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--< the solution
--------------------------------------------------------------------------
i believe the solution will be to drop the unique index on dds.dim_customer(bnkey)
add a unique index on dds.dim_customer(store_code,bnkey)
modify FUNCTION dds.dim_customer_update() to include RY8 and do on conflict(store_code,bnkey)
dds.dim_customer does not retain history, but does type 1 updates
so, make these changes
do a 1 time manual load of just RY8 
then i have to fix the instances of FK references to customer_key in fact_repair_order and sls.deals

DROP INDEX dds.dim_customer_bnkey_idx;
create unique index on dds.dim_customer(store_code,bnkey);

-- hopefully so i don't forget this when i do realtime
alter FUNCTION dds.dim_customer_update_today()
rename to zunused_dim_customer_update_today;

comment on table dds.dim_customer is 'Type 1 updates, does not maintain history, with the addition of toyota, RY8,
  ran into the problem of bnkey collision, previously bnkey was a unique index, now store_code/bnkey is a unique index';

-- initial load of RY8, 22886 rows
-- using exst_bopnmae
  insert into dds.dim_customer (store_code,bnkey,customer_type_code,customer_type,
    full_name,last_name,first_name,middle_name,home_phone,business_phone,
    cell_phone,email,email_valid,email_2,email_2_valid,city,county,state,zip,
    has_valid_email,do_not_call)
  select a.bopname_company_number, a.bopname_record_key as bnkey, a.company_individ, 
    case a.company_individ
      when 'I' then 'Person'
      when 'C' then 'Company'
    end as customer_type,
    a.bopname_search_name, a.last_company_name, a.first_name, a.middle_init, 
    a.phone_number, a.business_phone, a.cell_phone, 
    a.email_address, 
    CASE 
      when a.email_address is null then false
      WHEN length(a.email_address) = 0 THEN False
      WHEN position('@' IN a.email_address) = 0 THEN False
      WHEN position ('.' IN a.email_address) = 0 THEN False
      WHEN position('wng' IN a.email_address) > 0 THEN False
      WHEN position('dnh' IN a.email_address) > 0 THEN False
      WHEN position('noemail' IN a.email_address) > 0 THEN False
      WHEN position('none@' IN a.email_address) > 0 THEN False
      WHEN position('dng' IN a.email_address) > 0 THEN False
      ELSE True
    END AS email_valid,   
    a.second_email_address2, 
    CASE 
      when a.second_email_address2 is null then false
      WHEN length(a.second_email_address2) = 0 THEN False
      WHEN position('@' IN a.second_email_address2) = 0 THEN False
      WHEN position ('.' IN a.second_email_address2) = 0 THEN False
      WHEN position('wng' IN a.second_email_address2) > 0 THEN False
      WHEN position('dnh' IN a.second_email_address2) > 0 THEN False
      WHEN position('noemail' IN a.second_email_address2) > 0 THEN False
      WHEN position('none@' IN a.second_email_address2) > 0 THEN False
      WHEN position('dng' IN a.second_email_address2) > 0 THEN False
      ELSE True
    END AS email_2_valid,  
    a.city, a.county, a.state_code, a.zip_code,
    case
      when (
        case
          when a.email_address is null then false
          WHEN length(a.email_address) = 0 THEN False
          WHEN position('@' IN a.email_address) = 0 THEN False
          WHEN position ('.' IN a.email_address) = 0 THEN False
          WHEN position('wng' IN a.email_address) > 0 THEN False
          WHEN position('dnh' IN a.email_address) > 0 THEN False
          WHEN position('noemail' IN a.email_address) > 0 THEN False
          WHEN position('none@' IN a.email_address) > 0 THEN False
          WHEN position('dng' IN a.email_address) > 0 THEN False
          ELSE True
        end)
      or (
        case
          when a.second_email_address2 is null then false
          WHEN length(a.second_email_address2) = 0 THEN False
          WHEN position('@' IN a.second_email_address2) = 0 THEN False
          WHEN position ('.' IN a.second_email_address2) = 0 THEN False
          WHEN position('wng' IN a.second_email_address2) > 0 THEN False
          WHEN position('dnh' IN a.second_email_address2) > 0 THEN False
          WHEN position('noemail' IN a.second_email_address2) > 0 THEN False
          WHEN position('none@' IN a.second_email_address2) > 0 THEN False
          WHEN position('dng' IN a.second_email_address2) > 0 THEN False
          ELSE True      
        end) = true then true
      else false
    end as has_valid_email,
    case 
      when a.allow_contact_by_phone = 'N' then true
      else false
    end as do_not_call
  from arkona.ext_bopname a 
--   where a.current_row 
--     and a.row_from_date > current_date - 7
--     and a.row_from_date between current_date - 7 and current_date - 1   
    -- restriction brought over from ads
   WHERE a.bopname_company_number in ('RY8')
    and a.bopname_search_name is not null
    and a.company_individ is not null

20348 overlaps
select a.bnkey
from ry8_customers a
join dds.dim_customer b on a.bnkey = b.bnkey

select * from ry8_customers where bnkey = 1018725
select * from dds.dim_customer where bnkey = 1018725

select * 
from sls.deals
where store_code = 'RY8'

select * from sls.deals_by_month where stock_number = 'T10001P'


--------------------------------------------------------------------------
--/> the solution
--------------------------------------------------------------------------