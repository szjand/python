﻿
--------------------------------------------------------------
--< 07/28/22
--------------------------------------------------------------
turns out messing with chrome for dim_vehicle is a thankless task
keep it simple, for now
start with just an insert of RY8 vins that dont already exist in dim_vehicle
make it valid vins only
18809 rows

insert into dds.dim_vehicle(vin,model_year,make,model,model_code,body,color)
select distinct inpmast_vin, year, make, model, model_code, body_style, color
from arkona.ext_inpmast a
where a.inpmast_company_number = 'RY8'
  and not exists (
    select 1
    from dds.dim_vehicle
    where vin = a.inpmast_vin)
	and length(inpmast_vin) = 17
	and position('i' in a.inpmast_vin) = 0
	and position('o' in a.inpmast_vin) = 0
	and position('q' in a.inpmast_vin) = 0
	and (select * from cra.vin_check_sum(a.inpmast_vin));   

--------------------------------------------------------------
--< 07/28/22
--------------------------------------------------------------



select inpmast_vin, year, make, model, model_code, body_style, color
from arkona.xfm_inpmast a
where a.current_row
  and a.row_from_date > current_date - 7 
  and a.inpmast_company_number = 'RY1' -- from advantage

select * from dds.dim_vehicle limit 10

18913
select distinct inpmast_vin, year, make, model, model_code, body_style, color
from arkona.ext_inpmast a
where not exists (
  select 1
  from dds.dim_vehicle
  where vin = a.inpmast_vin)

select inpmast_company_number, inpmast_vin, year, make, model, model_code, body_style, color
from arkona.ext_inpmast a
where inpmast_vin in (
select inpmast_vin
from (
select distinct inpmast_vin, year, make, model, model_code, body_style, color
from arkona.ext_inpmast a
where a.inpmast_company_number in ('RY1','RY8')
  and not exists (
    select 1
    from dds.dim_vehicle
    where vin = a.inpmast_vin)) b
group by inpmast_vin
having count(*) > 1)    
order by a.inpmast_vin


select * 
from arkona.ext_inpmast
where inpmast_company_number = 'ry8'
  and type_n_u = 'n'
  and status = 'i'


select * 
from chr.describe_vehicle
where vin = '3TMCZ5AN4MM377265'


select * 
from arkona.ext_inpmast 
where inpmast_vin = '5TDGZRBH2LS017251'

select * 
from arkona.xfm_inpmast 
where inpmast_vin = '5TDGZRBH2LS017251'

drop table if exists not_in_dim_vehicle;
create temp table not_in_dim_vehicle as
select distinct inpmast_company_number as store, inpmast_vin, year, make, model, model_code, body_style, color
from arkona.ext_inpmast a
where not exists (
  select 1
  from dds.dim_vehicle
  where vin = a.inpmast_vin);

select make, year, count(*)
from not_in_dim_vehicle  
group by make, year
order by count(*) desc

-- 15508 of 18913 are toyotas
select store, make, count(*)
from not_in_dim_vehicle  
group by store, make
order by count(*) desc

-- none of them exist in inpmast under a non RY8 store
select * 
from not_in_dim_vehicle a
where exists (
  select 1
  from arkona.ext_inpmast
  where inpmast_company_number <> 'RY8'
    and inpmast_vin = a.inpmast_vin)
    

------------------------------------------------------------------
--< 7/26/22
------------------------------------------------------------------ 
ok, it is clear, all the RY8 vehicles missing from  dds.dim_vehicle are exclusively RY8 vehicles
but model_code and body_style is missing on most of them, colors are weird and missing

the first question i have is, what is the correlation between DT body_style and ?? in chrome

select * from arkona.ext_inpmast limit 5

select inpmast_company_number as store, inpmast_vin, year, make, model, model_code, body_style, color, chrome_Style_id
from arkona.ext_inpmast a
join chr.describe_Vehicle b on a.inpmast_vin = b.vin
where chrome_Style_id is not null
limit 1000

select * 
from nc.vehicles
where vin = 'KL4MMCSL5NB017185'

select count(*) 
from not_in_dim_vehicle a
join chr.describe_Vehicle b on a.inpmast_vin = b.vin

-- thinking just get started on the 15508 toyotas, no  build data
drop table if exists jon.toyota_vins;  --15482 - 35 that failed sum check, for 15447
create table jon.toyota_vins as
select distinct inpmast_vin as vin
from arkona.ext_inpmast a
where make = 'toyota'
	and not exists (
		select 1
		from dds.dim_vehicle
		where vin = a.inpmast_vin)
	and length(inpmast_vin) = 17
	and not exists (
	  select 1
	  from chr.describe_vehicle
	  where vin = a.inpmast_vin)
	and position('i' in a.inpmast_vin) = 0
	and position('i' in a.inpmast_vin) = 0
	and position('i' in a.inpmast_vin) = 0
	and substring(a.inpmast_vin, 9, 1) in ('0','1','2','3','4','5','6','7','8','9','X')
	and (select * from cra.vin_check_sum(a.inpmast_vin)); 
 

ok, got all 15447 vin in chr.describe_vehicle
now to get them into dds.dim_vehicle
select * from dds.dim_Vehicle limit 100

-- fuck me, not even 1/2 of them decode with style count = 1
-- so get what i can from chrome, then 
-- insert into dds.dim_vehicle(vin,model_year,make,model,model_code,body,color)
select distinct a.inpmast_vin, a.year, a.make, a.model, a.model_code, a.body_style, a.color, b.style_count, c.model_code
from arkona.ext_inpmast a
join chr.describe_vehicle b on a.inpmast_vin = b.vin
  and b.style_count = 1
left join arkona.ext_inpmast c on a.inpmast_vin = c.inpmast_vin -- this did not return a single model_code out of all 15446
  and c.inpmast_company_number = 'RY1' 
where a.inpmast_company_number = 'RY8'
  and not exists (
    select 1
    from dds.dim_vehicle
    where vin = a.inpmast_vin)

	
-------------------------------------------------------------------
--< 07/27/22 cra.vin_check_sum()
-------------------------------------------------------------------
there is a bug in function cra.vin_check_sum()
when attempting to check all these toyota vins, get an error:
ERROR:  invalid input syntax for integer: "B"
CONTEXT:  SQL function "vin_check_sum" statement 1
-- found and excluded the 2 vins where the 9th character was B, but now it is failing on X as well

-- does not appear to be a bad check_digit
select distinct check_digit
from (	
select aa.vin, (
  	select distinct mod((sum(c.vin_value*d.factor) over ()), 11) as check_digit
		from (
			select a.*, row_number() over () as the_position
			from (
				select regexp_split_to_table(aa.vin, '')::citext as the_char) a) b
			join cra.vin_transliteration c on the_char = vin_char
			join cra.vin_weighting d on b.the_position = d.vin_position)
from toyota_vins aa) bb		

-- here is the problem:
select distinct substring(vin, 9, 1)
from toyota_vins
order by  substring(vin, 9, 1)

select * from toyota_vins where substring(vin, 9, 1) = 'B'
select * from arkona.ext_inpmast where inpmast_vin in ('1T2D6RFVBNU080451','4T1BG22KBWU289529')


-- the X issue
-- fuck me, when i test individual X vins, the pass
select * from cra.vin_check_sum('1NXBR32E8Z959172')
-- but when i run the function on all the vins, it throws the error
the problem was mixing and matching text and integers in the case statement
modified the function
-------------------------------------------------------------------
--/> 07/27/22 cra.vin_check_sum()
-------------------------------------------------------------------



