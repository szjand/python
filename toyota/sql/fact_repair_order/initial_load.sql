﻿--------------------------------------------------------------------------------------
-- 07/28/22, now that dim_vehicle has been updated, time to go after fact_repair_order
this is the entire FUNCTION dds.fact_repair_order_update() up to inserting into dds.fact_repair_order
see if i can get this all working for just ry8 to do in initial insert, then modify the function to 
include RY8
this initial load IS NOT doing a full history, just the past 45 days

initial load done
--------------------------------------------------------------------------------------  

--------------------------------------------------------------------------------------
-- 0. cursor for generating data for dds.tmp_cor_group
--------------------------------------------------------------------------------------  
declare 
  counter integer = 1;
  _table dds.tmp_cor_group;
  _cursor cursor for ( 
  select * 
  from(
    SELECT 'flag' AS flagcor, ro_number, line_number, sequence_number
    FROM arkona.ext_sdprdet
    WHERE transaction_code = 'tt'
    AND labor_hours <> 0
    GROUP BY company_number, ro_number, line_number, sequence_number
    UNION 
    SELECT 'flag' AS flagcor, a.document_number AS ro_number, b.line_number, b.sequence_number
    FROM arkona.ext_pdpphdr a
    INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
    WHERE transaction_code = 'tt'
    AND labor_hours <> 0  
    GROUP BY a.document_number, b.line_number, b.sequence_number
    union
    SELECT 'cor' AS flagcor, ro_number, line_number, sequence_number
    FROM arkona.ext_sdprdet
    WHERE transaction_code = 'cr'
    GROUP BY  ro_number, line_number, sequence_number
    union
    SELECT 'cor' AS flagcor, a.document_number, b.line_number, b.sequence_number
    FROM arkona.ext_pdpphdr a
    INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
    WHERE transaction_code = 'cr'
    GROUP BY  a.document_number, b.line_number, b.sequence_number
    ORDER BY ro_number, line_number, sequence_number) x);

begin
-- --------------------------------------------------------------------------------------
-- -- 2. delete void ros
-- --------------------------------------------------------------------------------------  
--   delete 
--   from dds.fact_repair_order
--   where ro in (
--     select ro
--     from arkona.ext_void_ros);
    
--------------------------------------------------------------------------------------
-- 3. populate dds.tmp_ro 7/28 *
--------------------------------------------------------------------------------------
  truncate dds.tmp_ro;
-- SDPRHDR    
  insert into dds.tmp_ro 
  select company_number, ro_number, service_writer_id, customer_key, dds.db2_integer_to_date(open_date), 
    dds.db2_integer_to_date(close_date), dds.db2_integer_to_date(final_close_date), vin, 
    case
      WHEN odometer_in < 0 THEN
        CASE 
          WHEN odometer_out < 0 THEN 0
          ELSE odometer_out
        END
      WHEN odometer_in = odometer_out THEN odometer_in
      WHEN odometer_in < odometer_out THEN odometer_out
      WHEN odometer_in > odometer_out THEN odometer_in
    END, parts_total, labor_total, coupon_discount, cust_pay_hzrd_mat, 
    (cust_pay_shop_sup + warranty_shop_sup + internal_shop_sup + svc_cont_shop_sup), coalesce(tag_number, 'N/A'),
    doc_create_timestamp, 'Closed'
  from arkona.ext_sdprhdr
  where cust_name <> '*VOIDED REPAIR ORDER*'
    and company_number = 'RY8' -- in ('RY1', 'RY2')
    and ro_number not in ('412955','413514','413620','413629');
-- PDPPHDR where ro does not already exists in dds.tmp_ro
  insert into dds.tmp_ro 
  select company_number, document_number, service_writer_id, customer_key, dds.db2_integer_to_date(open_tran_date), 
    '12/31/9999','12/31/9999', vin, 
    CASE 
      WHEN odometer_in < 0 THEN
        CASE 
          WHEN odometer_out < 0 THEN 0
          ELSE odometer_out
        END
      WHEN odometer_in = odometer_out THEN odometer_in
      WHEN odometer_in < odometer_out THEN odometer_out
      WHEN odometer_in > odometer_out THEN odometer_in
    END, parts_total, labor_total, coupon_discount, cust_pay_hzrd_mat, 
    (cust_pay_shop_sup + warranty_shop_sup + internal_shop_sup + svc_cont_shop_sup), coalesce(tag_number, 'N/A'),
    doc_create_timestamp, c.ro_status
  -- select *    
  from arkona.ext_pdpphdr b
  left join dds.dim_ro_status c on b.ro_status = c.ro_status_code
  where b.document_type = 'RO'
    and b.company_number = 'RY8' -- in ('RY1','RY2')
    and b.document_number is not null
    and exists ( -- exclude ros with no lines
      select 1
      from arkona.ext_pdppdet
      where pending_key = b.pending_key)
    and cust_name <> '*VOIDED REPAIR ORDER*'
    and not exists ( -- exclude ros already in dds.tmp_ro
      select 1
      from dds.tmp_ro
      where ro = b.document_number);
-- UPDATE values IN tmpRO WHERE ro EXISTS IN both SDPRHDR & PDPPHDR  
  update dds.tmp_ro aa
    set ro_parts = bb.ro_parts,
        ro_labor = bb.ro_labor,
        discount = bb.discount,
        hazardous = bb.hazardous,
        shop_supplies = bb.shop_supplies,
        ro_status = bb.ro_status
  from ( 
    select a.ro,
      case when a.ro_parts = 0 then d.parts_total else a.ro_parts end as ro_parts,
      case when a.ro_labor = 0 then d.labor_total else a.ro_labor end as ro_labor,
      case when a.discount = 0 then d.coupon_discount else a.discount end as discount, 
      case when a.hazardous = 0 then d.cust_pay_hzrd_mat else a.hazardous end as hazardous,
      case when a.shop_supplies = 0 then d.shop_supplies else a.shop_supplies end as shop_supplies, 
      coalesce(d.ro_status, a.ro_status) as ro_status  
    from dds.tmp_ro a
    join (
      select company_number, document_number, parts_total, labor_total, coupon_discount, cust_pay_hzrd_mat, 
        (cust_pay_shop_sup + warranty_shop_sup + internal_shop_sup + svc_cont_shop_sup) as shop_supplies, c.ro_status
      from arkona.ext_pdpphdr b
      left join dds.dim_ro_status c on b.ro_status = c.ro_status_code
      where b.document_type = 'RO'
        and b.company_number = 'RY8' -- in ('RY1','RY2')
        and b.document_number is not null
        and exists ( -- exclude ros with no lines
          select 1
          from arkona.ext_pdppdet
          where pending_key = b.pending_key)
        and cust_name <> '*VOIDED REPAIR ORDER*') d on a.ro = d.document_number
      WHERE (a.ro_parts <> d.parts_total
        OR a.ro_labor <> d.labor_total 
        OR a.discount <> d.coupon_discount
        OR a.hazardous <> d.cust_pay_hzrd_mat
        OR a.shop_supplies <> d.shop_supplies
        OR a.ro_status <> d.ro_status)) bb 
  where aa.ro = bb.ro;    
-- dim_customer.bnkey = 0 for inventory ros
-- so, when ros are created for inventory vehicles but are given a customer number,
-- need to update that value to 0
  update dds.tmp_ro
  set customer = 0
  where ro in (
    select a.ro
    from dds.tmp_ro a
    join arkona.ext_bopname b on a.customer = b.bopname_record_key
      and b.last_company_name like '%INVENTORY%');

-- limit to RY8
delete 
from dds.tmp_ro
where store_code <> 'RY8'; 
--------------------------------------------------------------------------------------
-- 4. populate dds.tmp_ro_keys  7/28 *
--------------------------------------------------------------------------------------
  truncate dds.tmp_ro_keys;
  insert into dds.tmp_ro_keys (store_code,ro,tag,open_date,close_date,final_close_date,
    service_writer_key,customer_key,vehicle_key,ro_comment_key,ro_status_key,ro_labor_sales,
    ro_parts_sales,miles,create_ts,ro_shop_supplies,ro_discount,ro_hazardous)
  select a.store_code, a.ro, a.tag, a.open_date, a.close_date, a.final_close_date,
    coalesce(e.service_writer_key, m.service_writer_key) as service_writer_key, 
    coalesce(f.customer_key, k.customer_key) as customer_key, 
    coalesce(g.vehicle_key, n.vehicle_key) as vehicle_key,
    coalesce(h.ro_comment_key, i.ro_comment_key) as ro_comment_key, ro_status_key, 
    a.ro_labor, a.ro_parts, a.miles, a.create_ts, a.shop_supplies, a.discount,
    a.hazardous
  FROM dds.tmp_ro  a
  LEFT JOIN dds.dim_service_writer e ON a.store_code = e.store_code
    AND a.writer = e.writer_number
    AND e.active = true
    AND a.open_date BETWEEN e.row_from_date AND e.row_thru_date
  LEFT JOIN dds.dim_service_writer m ON a.store_code = m.store_code
    AND m.writer_number = 'UNK'  
  LEFT JOIN dds.dim_customer f ON a.customer = f.bnkey 
    and a.store_code = f.store_code ------------------------------------------------------------ *a*
  LEFT JOIN dds.dim_customer k ON 1 = 1
    AND k.full_name = 'unknown'
-- *i*    
    and k.bnkey <> 1171298
  LEFT JOIN dds.dim_vehicle g ON a.vin = g.vin 
  LEFT JOIN dds.dim_vehicle n ON 1 = 1 
    AND n.vin = 'UNKNOWN'
  LEFT JOIN dds.dim_ro_comment h ON a.store_code = h.store_code AND a.ro = h.ro
  LEFT JOIN dds.dim_ro_comment i ON a.store_code = i.store_code
    AND i.ro = 'N/A'
  LEFT JOIN dds.dim_ro_status j ON a.ro_status = j.ro_status;  

--------------------------------------------------------------------------------------
-- 5. populate dds.tmp_ro_line  7/28 *
--------------------------------------------------------------------------------------
  truncate dds.tmp_ro_line;
-- SDPRDET
  insert into dds.tmp_ro_line (store_code,ro,line,line_date,sublet,paint_materials,serv_type,pay_type,opcode,status)
  SELECT a.company_number AS store_code, a.ro_number AS ro, a.line_number AS line,
    -- there a goofy few with no A line
    coalesce(min(CASE WHEN a.line_type = 'A' THEN dds.db2_integer_to_date(a.transaction_date) ELSE '12/31/9999'::date END), '12/31/9999'::date) AS line_date,
    coalesce(SUM(CASE WHEN a.transaction_code = 'SL' AND a.line_type = 'N' THEN labor_amount END), 0) AS sublet, 
    coalesce(SUM(CASE WHEN a.transaction_code = 'PM' AND a.line_type = 'M' THEN labor_amount END), 0) AS paint_materials, 
    MAX(CASE WHEN a.line_type = 'A' THEN a.service_type END) AS serv_type,
    MAX(CASE WHEN a.line_type = 'A' THEN a.line_payment_method END) AS pay_type,
    MAX(CASE WHEN a.line_type = 'A' THEN a.labor_operation_code END) AS opcode,
    'Closed' AS status
  FROM arkona.ext_sdprdet a  
  INNER JOIN dds.tmp_ro b ON a.ro_number = b.ro 
  GROUP BY a.company_number, a.ro_number, a.line_number;  
--PDPPDET
  insert into dds.tmp_ro_line (store_code,ro,line,line_date,sublet,paint_materials,serv_type,pay_type,opcode,status)
  SELECT b.company_number AS store_code, a.document_number AS ro, b.line_number AS line,
    -- there a goofy few with no A line
    coalesce(min(CASE WHEN b.line_type = 'A' THEN dds.db2_integer_to_date(b.transaction_date) ELSE '12/31/9999'::date END), '12/31/9999'::date) AS line_date,
    coalesce(SUM(CASE WHEN b.transaction_code = 'SL' AND b.line_type = 'N' THEN net_price END), 0) AS sublet, 
    coalesce(SUM(CASE WHEN b.transaction_code = 'PM' AND b.line_type = 'M' THEN net_price END), 0) AS paint_materials, 
    MAX(CASE WHEN b.line_type = 'A' THEN b.service_type END) AS serv_type,
    MAX(CASE WHEN b.line_type = 'A' THEN b.line_payment_method END) AS pay_type,
    MAX(CASE WHEN b.line_type = 'A' THEN b.labor_operation_code END) AS opcode,
    MAX(CASE WHEN b.line_type = 'A' AND b.line_status = 'I' THEN 'Open' ELSE 'Closed' END) AS status 
  FROM arkona.ext_pdpphdr a  
  join arkona.ext_pdppdet b on a.pending_key = b.pending_key
  where a.document_type = 'RO'
    and a.document_number is not null
    and a.cust_name <> '*VOIDED REPAIR ORDER*'
    and b.line_number < 900
    and b.company_number = 'RY8' -- in ('RY1','RY2')
    and not exists (
      select 1
      from dds.tmp_ro_line
      where ro = a.document_number
      and line = b.line_number)
  GROUP BY b.company_number, a.document_number, b.line_number 
  order by a.document_number, b.line_number; 
-- update values on lines that exist in both sdprdet and pdppdet
  update dds.tmp_ro_line aa
    set line_date = bb.line_date,
        sublet = bb.sublet,
        paint_materials = bb.paint_materials,
        serv_type = bb.serv_type,
        pay_type = bb.pay_type,
        opcode = bb.opcode,
        status = bb.status
  from (      
    select a.store_code, a.ro, a.line,
      d.line_date,
      case when a.sublet > d.sublet then a.sublet else d.sublet end,
      case when a.paint_materials > d.paint_materials then a.paint_materials else d.paint_materials end,
      d.serv_type, d.pay_type, d.opcode, d.status
    from dds.tmp_ro_line a
    join (
      SELECT b.company_number AS store_code, a.document_number AS ro, b.line_number AS line,
        -- there a goofy few with no A line
        coalesce(min(CASE WHEN b.line_type = 'A' THEN dds.db2_integer_to_date(b.transaction_date) ELSE '12/31/9999'::date END), '12/31/9999'::date) AS line_date,
        coalesce(SUM(CASE WHEN b.transaction_code = 'SL' AND b.line_type = 'N' THEN net_price END), 0) AS sublet, 
        coalesce(SUM(CASE WHEN b.transaction_code = 'PM' AND b.line_type = 'M' THEN net_price END), 0) AS paint_materials, 
        MAX(CASE WHEN b.line_type = 'A' THEN b.service_type END) AS serv_type,
        MAX(CASE WHEN b.line_type = 'A' THEN b.line_payment_method END) AS pay_type,
        MAX(CASE WHEN b.line_type = 'A' THEN b.labor_operation_code END) AS opcode,
        MAX(CASE WHEN b.line_type = 'A' AND b.line_status = 'I' THEN 'Open' ELSE 'Closed' END) AS status 
      FROM arkona.ext_pdpphdr a  
      join arkona.ext_pdppdet b on a.pending_key = b.pending_key
      where a.document_type = 'RO'
        and a.document_number is not null
        and a.cust_name <> '*VOIDED REPAIR ORDER*'
        and b.line_number < 900
        and b.company_number = 'RY8' -- in ('RY1','RY2')
      GROUP BY b.company_number, a.document_number, b.line_number) d on a.store_code = d.store_code 
        and a.ro = d.ro
        and a.line = d.line
    where a.line_date <> d.line_Date
      or a.sublet <> d.sublet
      or a.paint_materials <> d.paint_materials  
      or a.serv_type <> d.serv_type
      or a.pay_type <> d.pay_type
      or a.opcode <> d.opcode
      or a.status <> d.status) bb
  where aa.store_code = bb.store_code
    and aa.ro = bb.ro
    and aa.line = bb.line;   

select distinct store_code from dds.tmp_ro_line    
--------------------------------------------------------------------------------------
-- 6. populate dds.tmp_ro_line_keys  7/28 *
--------------------------------------------------------------------------------------
  truncate dds.tmp_ro_line_keys;
  insert into dds.tmp_ro_line_keys (store_code,ro,line,line_date,service_type_key,payment_type_key,ccc_key,
    opcode_key,status_key,sublet,ro_paint_materials)
  select a.store_Code, a.ro, a.line, 
    a.line_date,
    coalesce(e.service_type_key, ee.service_type_key) as service_type_key,
    coalesce(f.payment_type_key, ff.payment_type_key) as payment_type_key,
    coalesce(g.ccc_key, gg.ccc_key) as ccc_key,
    coalesce(h.opcode_key, hh.opcode_key) as opcode_key,
    m.line_status_key, a.sublet, c.ro_paint_materials
  from dds.tmp_ro_line a  
  left join (
    select store_code, ro, sum(paint_materials) as ro_paint_materials
    from dds.tmp_ro_line
    group by store_code, ro) c on a.store_code = c.store_code and a.ro = c.ro
  LEFT JOIN dds.dim_service_type e ON a.serv_type = e.service_type_code
  LEFT JOIN dds.dim_service_type ee ON ee.service_type_code = 'UN'
  LEFT JOIN dds.dim_payment_type f ON a.pay_type = f.payment_type_code
  LEFT JOIN dds.dim_payment_type ff on ff.payment_type_code = 'U'
  LEFT JOIN dds.dim_ccc g ON a.store_code = g.store_code AND a.ro = g.ro AND a.line = g.line
  LEFT JOIN dds.dim_ccc gg ON a.store_code = gg.store_code AND gg.ro = 'N/A'
  LEFT JOIN dds.dim_opcode h ON a.store_code = h.store_code AND a.opcode = h.opcode
  LEFT JOIN dds.dim_opcode hh ON a.store_code = hh.store_code AND hh.opcode = 'N/A'            
  LEFT JOIN dds.dim_line_status m ON a.status = m.line_status;  
  
select distinct store_code from dds.tmp_ro_line_keys  
--------------------------------------------------------------------------------------
-- 7. populate dds.tmp_cor_group 7/28 *
--------------------------------------------------------------------------------------
DO $$
declare 
  counter integer = 1;
  _table dds.tmp_cor_group;
  _cursor cursor for ( 
  select * 
  from(
    SELECT 'flag' AS flagcor, ro_number, line_number, sequence_number
    FROM arkona.ext_sdprdet
    WHERE transaction_code = 'tt'
    AND labor_hours <> 0
    GROUP BY company_number, ro_number, line_number, sequence_number
    UNION 
    SELECT 'flag' AS flagcor, a.document_number AS ro_number, b.line_number, b.sequence_number
    FROM arkona.ext_pdpphdr a
    INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
    WHERE transaction_code = 'tt'
    AND labor_hours <> 0  
    GROUP BY a.document_number, b.line_number, b.sequence_number
    union
    SELECT 'cor' AS flagcor, ro_number, line_number, sequence_number
    FROM arkona.ext_sdprdet
    WHERE transaction_code = 'cr'
    GROUP BY  ro_number, line_number, sequence_number
    union
    SELECT 'cor' AS flagcor, a.document_number, b.line_number, b.sequence_number
    FROM arkona.ext_pdpphdr a
    INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
    WHERE transaction_code = 'cr'
    GROUP BY  a.document_number, b.line_number, b.sequence_number
    ORDER BY ro_number, line_number, sequence_number) x);
begin    
  truncate dds.tmp_cor_group;  
  open _cursor; 
    loop
      fetch _cursor into _table;
        if _table.flag_cor = 'cor' then
          counter = counter + 1;
        end if;
        insert into dds.tmp_cor_group values(_table.flag_cor,_table.ro,_table.line,_table.seq,counter);
        exit when _table.ro is null;
    end loop;
  close _cursor;   
end $$;

-- select * from dds.tmp_cor_group
--------------------------------------------------------------------------------------
-- 8. populate dds.tmp_ro_tech_flag_cor_1   7/28 *
--------------------------------------------------------------------------------------
  truncate dds.tmp_ro_tech_flag_cor_1;
  insert into dds.tmp_ro_tech_flag_cor_1(store_code,ro,line,flag_date,technician_id,labor_hours,flag_seq_group,
    correction_code,cor_seq_group,sequence_number,labor_amount)
  select distinct aa.store_code, aa.ro, aa.line, bb.transaction_date, bb.technician_id, bb.labor_hours,
    bb.seq_group, cc.correction_code, cc.seq_group, bb.sequence_number, bb.labor_amount
  from dds.tmp_ro_line aa
  left join (
    select a.*, b.seq_group
    from (
      SELECT 'flag' AS flagcor, company_number, ro_number, line_number, sequence_number,
        dds.db2_integer_to_date(transaction_date) as transaction_date, technician_id, sum(labor_hours) as labor_hours,
        sum(labor_amount) as labor_amount 
      FROM arkona.ext_sdprdet
      WHERE transaction_code = 'tt'
      AND labor_hours <> 0
      GROUP BY company_number, ro_number, line_number, sequence_number,
        dds.db2_integer_to_date(transaction_date), technician_id
      UNION 
      SELECT 'flag' AS flagcor, b.company_number, a.document_number, b.line_number, b.sequence_number,
        dds.db2_integer_to_date(transaction_date), technician_id, sum(labor_hours), sum(net_price)
      FROM arkona.ext_pdpphdr a
      INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
      WHERE transaction_code = 'tt'
      AND labor_hours <> 0  
      GROUP BY b.company_number, a.document_number, b.line_number, b.sequence_number,
        dds.db2_integer_to_date(b.transaction_date), b.technician_id) a
    left join dds.tmp_cor_group b on a.ro_number = b.ro
      and a.line_number = b.line
      and a.sequence_number = b.seq
      and b.flag_cor = 'flag') bb on aa.store_code = bb.company_number
        and aa.ro = bb.ro_number 
        and aa.line = bb.line_number
  left join (
    select a.*, b.seq_group
    from (
      SELECT 'cor' AS flagcor, company_number, ro_number, line_number, sequence_number,
        dds.db2_integer_to_date(transaction_date) as transaction_date, max(correction_code) as correction_code
      FROM arkona.ext_sdprdet
      WHERE transaction_code = 'cr'
      GROUP BY company_number, ro_number, line_number, sequence_number,
        dds.db2_integer_to_date(transaction_date)
      UNION 
      SELECT 'cor' AS flagcor, b.company_number, a.document_number, b.line_number, b.sequence_number,
        dds.db2_integer_to_date(b.transaction_date), max(b.correction_code)
      FROM arkona.ext_pdpphdr a
      INNER JOIN arkona.ext_pdppdet b on a.pending_key = b.pending_key
      WHERE transaction_code = 'cr'
      GROUP BY b.company_number, a.document_number, b.line_number, b.sequence_number,
        dds.db2_integer_to_date(b.transaction_date)) a
    left join dds.tmp_cor_group b on a.ro_number = b.ro
      and a.line_number = b.line
      and a.sequence_number = b.seq
      and b.flag_cor = 'cor') cc on aa.store_code = cc.company_number
        and aa.ro = cc.ro_number 
        and aa.line = cc.line_number
        and
          case
            when bb.seq_group is null then 1 = 1
            else coalesce(bb.seq_group, - 1) = coalesce(cc.seq_group, -1)
          end;  

select distinct store_code from dds.tmp_ro_tech_flag_cor_1          
--------------------------------------------------------------------------------------
-- 9. populate dds.tmp_ro_tech_flag_cor_2  7/28 *
--------------------------------------------------------------------------------------
  truncate dds.tmp_ro_tech_flag_cor_2;
  insert into dds.tmp_ro_tech_flag_cor_2(store_code,ro,line,flag_date,technician_id,correction_code,labor_hours,labor_amount)
  select store_code, ro, line, flag_date, technician_id, correction_code, sum(labor_hours), sum(labor_amount)
  from (
    select store_code, ro, line, coalesce(flag_date, '12/31/9999'::date) as flag_date,
      coalesce(technician_id, 'N/A') as technician_id, sum(coalesce(labor_hours, 0)) as labor_hours,
      sum(coalesce(labor_amount, 0)) as labor_amount, coalesce(correction_code, 'N/A') as correction_code,
      coalesce(flag_seq_group, cor_seq_group)
    from dds.tmp_ro_tech_flag_cor_1
    group by store_code, ro, line, coalesce(flag_date, '12/31/9999'::date), 
      coalesce(technician_id, 'N/A'), coalesce(correction_code, 'N/A'), coalesce(flag_seq_group, cor_seq_group)) a
  group by store_code, ro, line, flag_date, technician_id, correction_code;  

select distinct store_code from dds.tmp_ro_tech_flag_cor_2;  
--------------------------------------------------------------------------------------
-- 10. populate dds.tmp_ro_tech_flag_cor_keys 7/28 *
--------------------------------------------------------------------------------------

  truncate dds.tmp_ro_tech_flag_cor_keys;
  insert into dds.tmp_ro_tech_flag_cor_keys(store_code,ro,line,flag_date,
    tech_key,cor_code_key,labor_hours,labor_amount)
  select a.store_code, a.ro, a.line, a.flag_date,
    case
--       when a.store_code = 'RY1' then
--         case when c.tech_key is not null then c.tech_key else d.tech_key end
--       when a.store_code = 'RY2' then
--         case when c.tech_key is not null then c.tech_key else d.tech_key end
      when a.store_code = 'RY8' then
        case when c.tech_key is not null then c.tech_key else d.tech_key end        
      end,
    coalesce(e.opcode_key, f.opcode_key), a.labor_hours, a.labor_amount
  from dds.tmp_ro_tech_flag_cor_2 a
  left join dds.dim_tech c on a.store_code = c.store_code
    and a.technician_id = c.tech_number
    and a.flag_date between c.row_from_date and c.row_thru_date
  left join (
    select store_code, tech_key
    from dds.dim_tech
    where tech_number = 'UNK') d on a.store_code = d.store_code
  left join dds.dim_opcode e on a.store_code = e.store_code
    and a.correction_code = e.opcode
  left join dds.dim_opcode f on a.store_code = f.store_code
    and f.opcode = 'N/A';  

select distinct store_code from dds.tmp_ro_tech_flag_cor_2    
--------------------------------------------------------------------------------------
-- 11. populate dds.tmp_fact_repair_order 7/28 *
--------------------------------------------------------------------------------------
  truncate dds.tmp_fact_repair_order;
  insert into dds.tmp_fact_repair_order(store_code,ro,line,tag,open_date,close_date,
    final_close_date,line_date,flag_date,service_writer_key,customer_key,
    vehicle_key,service_type_key,payment_type_key,ccc_key,ro_comment_key,opcode_key,
    ro_status_key,line_status_key,ro_labor_sales,ro_parts_sales,flag_hours,ro_flag_hours,
    miles,ro_created_ts,sublet,ro_shop_supplies,ro_discount,ro_hazardous_materials,
    ro_paint_materials,cor_code_key,tech_key,labor_sales)
  select a.store_code, a.ro, b.line, a.tag, a.open_date, a.close_date, a.final_close_date,
    b.line_date, c.flag_date, a.service_writer_key, a.customer_key, a.vehicle_key, 
    b.service_type_key, b.payment_type_key, b.ccc_key, a.ro_comment_key, b.opcode_key, a.ro_status_key,
    b.status_key, d.ro_labor_sales, a.ro_parts_sales, c.labor_hours, d.ro_flag_hours, a.miles,
    a.create_ts, b.sublet, a.ro_shop_supplies, a.ro_discount, a.ro_hazardous, b.ro_paint_materials, 
    c.cor_code_key, c.tech_key, c.labor_amount
  from dds.tmp_ro_keys a
  join dds.tmp_ro_line_keys b on a.store_code = b.store_code
    and a.ro = b.ro
    and b.line < 900
  join dds.tmp_ro_tech_flag_cor_keys c on b.store_code = c.store_code
    and b.ro = c.ro
    and b.line = c.line
  left join ( -- generate ro_flag_hours, ro_labor_sales
    select store_code, ro, sum(labor_hours) as ro_flag_hours,
      sum(labor_amount) as ro_labor_sales
    from dds.tmp_ro_tech_flag_cor_keys
    group by store_code, ro) d on a.store_code = d.store_code
      and a.ro = d.ro;

select distinct store_code from dds.tmp_fact_repair_order      
-- --------------------------------------------------------------------------------------
-- -- 12. populate dds.fact_repair_order
-- --------------------------------------------------------------------------------------      
  select * 
  FROM dds.fact_repair_order
  WHERE ro IN (
    SELECT ro
    FROM dds.tmp_fact_repair_order);
    
  INSERT INTO dds.fact_repair_order (store_code,ro,line,tag,open_date,close_date,final_close_date,line_date,flag_date,service_writer_key,
    customer_key,vehicle_key,service_type_key,payment_type_key,ccc_key,ro_comment_key,opcode_key,ro_status_key,
    line_status_key,ro_labor_sales,ro_parts_sales,flag_hours,ro_flag_hours,miles,ro_created_ts,sublet,
    ro_shop_supplies,ro_discount,ro_hazardous_materials,ro_paint_materials,cor_code_key,tech_key,labor_sales)
  SELECT store_code,ro,line,tag,open_date,close_date,final_close_date,line_date,flag_date,service_writer_key,
    customer_key,vehicle_key,service_type_key,payment_type_key,ccc_key,ro_comment_key,opcode_key,ro_status_key,
    line_status_key,ro_labor_sales,ro_parts_sales,flag_hours,ro_flag_hours,miles,ro_created_ts,sublet,
    ro_shop_supplies,ro_discount,ro_hazardous_materials,ro_paint_materials,cor_code_key,tech_key,labor_sales 
  FROM dds.tmp_fact_repair_order;  
  
