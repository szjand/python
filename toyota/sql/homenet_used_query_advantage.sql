07/12/22 ended up not using this, just took a regular used car feed, deleted all the non toyota vehicles 
  and added a T for Comment1
this query did not hanle the warranty and option stuff, plus generating the csv from advantage gave me a file
formatted differently than the used car feed which is | delimited
so, that was actually a quick edit of the existing feed ans will serve for the initial load of toyota used cars

-- -- this appears to WORK for the base toyota data

-- SELECT * FROM organizations WHERE name = 'toyota'  -- '77a18168-998f-8747-ae24-3efc540cd069'
SELECT top 1 typ 
       FROM selectedreconpackages 
       WHERE VehicleInventoryItemID = '8ca0af89-b403-4659-a2ab-63b231d51557'
         AND BasisTable <> 'VehicleEvaluations'
       ORDER BY SelectedReconPackageTS desc
SELECT * FROM VehicleInventoryItems WHERE stocknumber = 'T10019P'
EXECUTE PROCEDURE GetVehicleItemLongOptionNamesString('eba2fa75-a9ca-9743-84ae-8db4b73026b6')		 
--modify the SELECT statement to match the actual feed

SELECT 
	'Rydell GM Autocenter',	
  vi.VIN,
	replace(vii.StockNumber, '*', '') AS StockNumber,
	'Used',
	
--  (SELECT o.FullName FROM Organizations o WHERE o.PartyID = 'A4847DFC-E00E-42E7-89EE-CD4368445A82') as Market,
--  (SELECT o.FullName FROM Organizations o WHERE o.PartyID = vii.OwningLocationID) as Location,
  vi.YearModel AS Year,
  vi.Make ,
  vi.Model AS Model,
  
	'' AS model_number,
  vi.BodyStyle as BodyStyle,
	vi.Transmission as Transmission,
	vi.Trim AS TrimLevel,
	'' AS doors,
  (SELECT Top 1 Value 
    FROM VehicleItemMileages vim 
	WHERE vim.VehicleItemID = vii.VehicleItemID
	ORDER BY VehicleItemMileageTS DESC) AS Mileage,	
	vi.Engine as Engine,
	'' AS displacement,
	'' AS drivetrain,
  vi.ExteriorColor AS Color,
  vi.InteriorColor AS InteriorColor,
  CAST(cast(xx.imcost AS sql_double) AS sql_integer) AS VehicleCost,
	'' AS msrp,
	'' AS bookvalue,
  (SELECT TOP 1 vpd.Amount
	 FROM VehiclePricings vp
	 inner join VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
	 WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
     ORDER BY VehiclePricingTS DESC) as Price,
  '' AS dateinstock,		 
	CASE
	  WHEN 
     (SELECT top 1 typ 
       FROM selectedreconpackages 
       WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID 
         AND BasisTable <> 'VehicleEvaluations'
       ORDER BY SelectedReconPackageTS desc) = 'ReconPackage_Factory' THEN 'YES'
		ELSE 'NO'
  end as certified,
	'' as warranty,
	'' as options,
	'' AS pics,
	x.cert,
	CASE
	  WHEN 
     (SELECT top 1 typ 
       FROM selectedreconpackages 
       WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID 
         AND BasisTable <> 'VehicleEvaluations'
       ORDER BY SelectedReconPackageTS desc) = 'ReconPackage_Nice' THEN 'YES'
		ELSE 'NO'
  end as NiceCare,
  CASE 
    WHEN vii.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'G'
    WHEN vii.locationid = '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'H'
    WHEN vii.locationid = '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' THEN 'O'
		WHEN vii.locationid = '77a18168-998f-8747-ae24-3efc540cd069' THEN 'T'
  END AS Comment1
/*
  vii.VehicleInventoryItemID,
  vii.OwningLocationID,
  vii.VehicleItemID,
  CASE td.Description
    WHEN 'Crossover' THEN 'SUV'
    ELSE td.Description
  END AS VehicleType,
*/	
--  into #IFIT
-- SELECT vii.*
  FROM VehicleInventoryItems vii
	 
  inner join VehicleItems vi on vi.VehicleItemID = vii.VehicleItemID	
	   
  LEFT JOIN MakeModelClassifications mmcx ON vi.Make = mmcx.Make
    AND vi.Model = mmcx.Model
  LEFT JOIN TypDescriptions td ON mmcx.VehicleType = td.typ    
  LEFT JOIN FactoryCertifications x on vii.VehicleInventoryItemID = x.VehicleInventoryItemID     
	LEFT JOIN dds.stgarkonainpmast xx on vii.stocknumber = xx.imstk# 
  WHERE vii.ThruTS IS NULL
    AND vii.OwningLocationID IN (SELECT PartyID2 
	                               FROM PartyRelationships 
								   WHERE typ = 'PartyRelationship_MarketLocations' 
								     AND PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82')
    AND  Exists (SELECT 1 
                FROM VehicleInventoryItemStatuses viis 
				where viis.VehicleInventoryItemID = vii.VehicleInventoryItemID
				  and viis.ThruTS is Null and Status in ('RawMaterials_RawMaterials'))
														 				
  AND NOT Exists(SELECT 1 
                FROM VehicleInventoryItemStatuses viis 
				where viis.VehicleInventoryItemID = vii.VehicleInventoryItemID
				  and viis.ThruTS is Null and Status in ('RMFlagSB_SalesBuffer',
                                                         'RMFlagWSB_WholesaleBuffer',
				                                         'RMFlagPIT_PurchaseInTransit',
														 'RMFlagTNA_TradeNotAvailable',
														 'RMFlagIP_InspectionPending',
														 'RMFlagWP_WalkPending'))
and vii.locationid = '77a18168-998f-8747-ae24-3efc540cd069'														 
	

   
   






