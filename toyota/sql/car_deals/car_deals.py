﻿/*
to complete nc.vehicles (...\nc_inventory\all_nc_inventory_nightly.sql)
need to step the car deals
again, a one time load
and also update the luigi for nightly updates

turns out, no changes in luigi required, did the one time load in this script
*/
---------------------------------------------------------------------------------
-- sls.ext_accounting_base requires fact_gl, which i only just now have
truncate sls.ext_accounting_base;

insert into sls.ext_accounting_base
select d.the_date, a.control, amount,
	b.account, b.description as account_description, e.description
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
	and b.current_row = true
	and b.account <> '144500'
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where b.account_type = 'sale'
	and b.department_code in ('nc','uc','ao')
	and c.journal_code in ('vsn','vsu')
	and a.post_status = 'Y'
	and
		case
			when a.control = 'H12469A' then b.account = '244800'
			when a.control = 'H12485TA' then b.account = '245200'
			when a.control = 'H15146' then account = '240201'
			when a.control = 'H15141' then account = '240201'
			when a.control = 'H15129' then account = '240201'
			else 1 = 1
		end                      
	and d.the_date between '01/01/2022' and current_date;
--ok
select * from sls.ext_accounting_base where control like 'T%'	

---------------------------------------------------------------------------------
-- sls.ext_accounting_deals no toyotas yet, so this just inserts them
                    insert into sls.ext_accounting_deals
                    select current_date, the_date, control, amount, unit_count,
                      account, account_description, gl_description
                    from (  -- ccc all rows
                      select aa.the_date, aa.control, sum(aa.amount) as amount, aa.account,
                        aa.account_description, max(aa.description) as gl_description,
                        sum(case when aa.amount < 0 then 1 else -1 end) as unit_count
                      from sls.ext_accounting_base aa
                      left join (-- multiple rows per date to exclude
                        select x.control, x.the_date
                        from ( -- assign unit count to rows w/multiple rows per date
                          select a.control, a.the_date,
                            case when a.amount < 0 then 1 else -1 end as the_count
                          from sls.ext_accounting_base a
                          inner join ( -- multiple rows per date
                            select control, the_date
                            from sls.ext_accounting_base
                            group by control, the_date
                            having count(*) > 1) b on a.control = b.control and a.the_date = b.the_date) x
                        group by x.control, x.the_date
                        having sum(the_count) = 0) bb on aa.the_date = bb.the_date and aa.control = bb.control
                      where bb.control is null -- this is the line that excludes the multiple entry vehicles
                      group by aa.the_date, aa.control, aa.account, aa.account_description) ccc
                    where not exists (
                      select 1
                      from sls.ext_accounting_deals
                      where control = ccc.control
                        and gl_date = ccc.the_date);

---------------------------------------------------------------------------------
-- XfmDealsNewRows sls.xfm_deals initial load, new rows only        

                    insert into sls.xfm_deals (run_date, store_code, bopmast_id, deal_status,
                      deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_Date, approved_date,
                      capped_date, delivery_date, gap, service_contract, total_care,
                      row_type, seq,
                      gl_date, gl_count, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status,
                      a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
                      a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
                      'new', 1,
                      coalesce(b.gl_date, '12/31/9999'), coalesce(b.unit_count, 0),
                      ( -- generate the hash
                        select md5(z::text) as hash
                        from (
                          select store_code, bopmast_id, deal_status,
                            deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                            odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                            primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                            capped_date, delivery_date, gap, service_contract,total_care,
                            coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
                          from sls.ext_deals x
                          left join sls.ext_accounting_deals y on x.stock_number = y.control
                            and y.gl_date = (
                              select max(gl_date)
                              from sls.ext_accounting_deals
                              where control = y.control)
                          where x.store_code = a.store_code
                            and x.bopmast_id = a.bopmast_id) z)
                    from sls.ext_deals a
                    left join sls.ext_accounting_deals b on a.stock_number = b.control
                      and b.gl_date = (
                        select max(gl_date)
                        from sls.ext_accounting_deals
                        where control = b.control)
                    where gl_date >= '01/01/2022'
                      and a.vin is not null
                      and not exists (
                        select *
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id) order by stock_number;         

--------------------------------------------------------------------------------
-- XfmDealsNewRows              

                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      1 as seq,
                      case
                        when a.gl_date is null then
                          (100* extract(year from a.capped_date) + extract(month from a.capped_date)):: integer
                        else
                          (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
                      end as year_month,
                      a.gl_count,
                      case -- delivery date may figure into this, eventually
                        when a.gl_date is null then a.capped_date
                        else a.gl_date
                      end as gl_date,
                      case
                        when a.deal_status = 'U' then 'capped'
                        when a.deal_status = 'A' then 'accepted'
                        else 'none'
                      end as deal_status, 'new row', a.hash
                    from sls.xfm_deals a
                    left join sls.deals b on a.store_code = b.store_code
                      and a.bopmast_id = b.bopmast_id
                    where a.deal_status = 'U'
                      and coalesce(a.gl_date, a.capped_date) > '12/31/2021' 
                      and b.store_code is null;     