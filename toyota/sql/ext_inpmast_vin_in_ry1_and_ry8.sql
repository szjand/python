﻿select * from dds.dim_vehicle limit 10

-- these are the fields of issue from the existsing dim vehicle
    select string_agg(column_name, ',' order by ordinal_position)
    from information_schema.columns
    where table_schema = 'dds'
      and table_name = 'dim_vehicle'

-- an issue will be correlating dms attributes to chrome attributes


drop table if exists double_vins;  -- 7090, 3534 vins
create temp table double_vins as
select a.inpmast_vin, a.make as ry1_make, b.make as ry8_make, a.year as ry1_year, b.year as ry8_year
from (
	select inpmast_vin, make, year from arkona.ext_inpmast where inpmast_company_number = 'RY1' and length(inpmast_vin) = 17) a
join (
	select inpmast_vin, make, year from arkona.ext_inpmast where inpmast_company_number = 'RY8' and length(inpmast_vin) = 17) b on a.inpmast_vin = b.inpmast_vin


select a.inpmast_company_number as store, a.inpmast_vin, a.year, a.make, a.model, a.model_code, a.body_style, a.color, coalesce(c.style_count, 666)
from arkona.ext_inpmast a
join double_vins b on a.inpmast_vin = b.inpmast_vin -- and b.inpmast_vin = '4T4BF1FK0ER344595'
left join chr.describe_vehicle c on a.inpmast_vin = c.vin
-- where coalesce(c.style_count, -1) > 1
where style_count = 1
order by a.inpmast_vin, a.inpmast_company_number

-- style count = 4, honda civic Si, model_code pins it down
select * from chr.describe_Vehicle where vin = '2HGFG21536H700399'
-- style count = 8, 2015 ford f-150, trim is defferent between the 2 candidate styles, but i am unsure of which one i have
select * from chr.describe_Vehicle where vin = '1FTFW1EG0FFA29766'
-- try build, nope, source is catalog, don't know what i am going to do about fords
select * from chr.build_data_describe_vehicle where vin = '1FTFW1EG0FFA29766'
-- style count = 10, 2010 camry, name is different, 2014.5 xxx, this pins it down
select * from chr.describe_Vehicle where vin = '4T4BF1FK0ER344595'
	
-- 7067, 6897 (build), 6419 (chr) 
select a.inpmast_vin, a.make, b.make 
from (
	select inpmast_vin, make from arkona.ext_inpmast where inpmast_company_number = 'RY1' and length(inpmast_vin) = 17) a
join (
	select inpmast_vin, make from arkona.ext_inpmast where inpmast_company_number = 'RY8' and length(inpmast_vin) = 17) b on a.inpmast_vin = b.inpmast_vin
-- where (select * from cra.vin_check_sum(a.inpmast_vin)) -- they all pass
  and not exists (
    select 1
    from chr.build_data_describe_vehicle
    where vin = a.inpmast_vin)
  and not exists (
    select 1
    from chr.describe_vehicle
    where vin = a.inpmast_vin)    

-- 171 in build data, 1 nissan frontier excluded due to style_count
select a.inpmast_vin, (r.style ->>'id')::citext as chr_style_id,
  (r.style->>'modelYear')::integer as chr_model_year,
  (r.style ->'division'->>'_value_1')::citext as chr_make,
  (r.style ->'model'->>'_value_1'::citext)::citext as chr_model,
  (r.style ->>'mfrModelCode')::citext as chr_model_code,
  case
    when r.style ->>'drivetrain' like 'Rear%' then 'RWD'
    when r.style ->>'drivetrain' like 'Front%' then 'FWD'
    when r.style ->>'drivetrain' like 'Four%' then '4WD'
    when r.style ->>'drivetrain' like 'All%' then 'AWD'
    else 'XXX'
  end as chr_drive,
  case
    when u.cab->>'_value_1' like 'Crew%' then 'crew' 
    when u.cab->>'_value_1' like 'Extended%' then 'double'  
    when u.cab->>'_value_1' like 'Regular%' then 'reg'  
    else 'n/a'   
  end as chr_cab,
  coalesce(r.style ->>'trim', 'none')::citext as chr_trim,
  t->>'_value_1' as engine_displacement,
--    a.color, null::integer as configuration_id,
  case
    when v.bed->>'_value_1' like '%Bed' then substring(bed->>'_value_1', 1, position(' ' in bed->>'_value_1') - 1)
    else 'n/a'
  end as box_size,
  c.source, c.style_count   
from double_vins a
join chr.build_data_describe_vehicle c on a.inpmast_vin = c.vin
join jsonb_array_elements(c.response->'style') as r(style) on true
left join jsonb_array_elements(r.style->'bodyType') with ordinality as u(cab) on true
  and u.ordinality =  2
left join jsonb_array_elements(r.style->'bodyType') with ordinality as v(bed) on true
  and v.ordinality =  1    
left join jsonb_array_elements(c.response->'engine') as s(engine) on true  
  and (s.engine->'installed'->>'cause' = 'OptionCodeBuild' or coalesce(s.engine->'installed'->>'cause', 'xxx') = 'VIN')
left join jsonb_array_elements(s.engine->'displacement'->'value') as t(displacement) on true
  and t.displacement ->>'unit' = 'liters'  
where source = 'build'
  and style_count = 1  


select year, inpmast_vin,inpmast_stock_number, chrome_Style_id from arkona.ext_inpmast where body_style is null and year > 2021 and inpmast_Stock_number is not null and inpmast_company_number
 = 'RY1' and make <> 'nissan'


------------------------------------------------------------------------------
--< bopvref
------------------------------------------------------------------------------
/* BOPVREF */
BVTYPE (TYPE_CODE) Customer/Vehicle Reference
S = Buyer on the Deal
2 = Co-Buyer on the Deal
C = Service Customer
T = Trade In
4 = ?? (Of the 212409 rows in BOPVREF table, only 59 are type 4)

-- vin in bopvref under both ry1 an dry8
select a.vin, count(*)
from (
select * from arkona.ext_bopvref where company_number = 'RY8') a
join (
select * from arkona.ext_bopvref where company_number = 'RY1') b on a.vin = b.vin
group by a.vin
order by count(*) desc

select a.company_number, a.type_code, a.customer_key, a.vin, a.start_date, a.end_date, b.bopname_search_name 
from arkona.ext_bopvref a
join arkona.ext_bopname b on a.company_number = b.bopname_company_number
  and a.customer_key = b.bopname_record_key
where vin = '3GCUKSEC9FG527940'
-- group by a.company_number, a.type_code, a.customer_key, a.vin, a.start_date, a.end_date, b.bopname_search_name 
order by b.bopname_search_name, customer_key, a.start_date, end_date