﻿-- -- from ben cahalan
-- The dollar amount listed is a daily guarantee and is based on a 5 day work week.
-- 
-- Technicians:
-- 
-- Jose DeLa Cruz - Tech 401 - $20 per flag hour - $171.13
-- 
-- Ray Cole - Tech 402 - $20 per flag hour - $152.16
-- 
-- Dylan Zimmer - Tech 403 - $18 per flag hour - $136.19
-- 
-- Jacob Huffaker - Tech 404 - $18 per flag hour - $180.89

create schema ts;
comment on schema ts is 'Toyota main shop flat rate pay plan';

think i will do it a little differently than honda
maintain a daily record, truncated and reloaded each day of the pay period
as well as payroll data

in addition in the tech table, keep the end date as well as thru date, thru date is the date thru which  they are paid
	the end date is the term date
	so, if a tech terms in the middle of a pay period, that is the end date, the thru date is the last day of the pay 
	period in which he terms
	
	that is, now, not making any sense
	fuck 
	


select * from  hs.hn_main_shop_flat_rate_payroll_data limit 120

select *
from ukg.employees a
join dds.dim_tech b on a.employee_number = b.employee_number
  and b.store_code = 'RY8'

drop table if exists ts.main_shop_flat_rate_techs cascade;
CREATE TABLE ts.main_shop_flat_rate_techs (
  last_name citext not null,
  first_name citext not null,
  employee_number citext NOT NULL,
  tech_number citext NOT NULL,
  flat_rate numeric(6,2) NOT NULL,
  daily_guarantee numeric (6,2) not null,
  tool_allowance numeric (6,2) not null,
  from_date date NOT NULL,
  thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  end_date date not null default '9999-12-31'::date,
  CONSTRAINT main_shop_flat_rate_techs_pkey PRIMARY KEY (employee_number, thru_date));

insert into ts.main_shop_flat_rate_techs(last_name, first_name, employee_number, 
	tech_number, flat_rate, daily_guarantee, tool_allowance, from_date)
select * 
from (
	select a.last_name, a.first_name, a.employee_number, b.tech_number, 
		case b.tech_number
			when '401' then 20
			when '402' then 20
			when '403' then 18
			when '404' then 18
		end as flat_rate,
		case b.tech_number
			when '401' then 171.13
			when '402' then 152.16
			when '403' then 136.19
			when '404' then 180.89
		end,69.42,	
		'07/12/2022'::date
	from ukg.employees a
	join dds.dim_tech b on a.employee_number = b.employee_number
		and b.store_code = 'RY8') c 
where flat_rate is not null;

select * from ts.main_shop_flat_rate_techs



-- DROP TABLE ts.hn_main_shop_flat_rate_payroll_data cascade;

CREATE TABLE ts.ty_main_shop_flat_rate_payroll_data
(
  pay_period_start date NOT NULL,
  pay_period_end date NOT NULL,
  pay_period_seq integer NOT NULL,
  pay_period_select_format citext NOT NULL,
  first_saturday date NOT NULL,
  last_name citext NOT NULL,
  first_name citext NOT NULL,
  employee_number citext NOT NULL,
  hire_date date NOT NULL,
  term_date date NOT NULL,
  flag_hours numeric NOT NULL,
  adjustments numeric NOT NULL,
  total_flag_hours numeric NOT NULL,
  clock_hours numeric NOT NULL,
--   pto_hours numeric NOT NULL,
--   hol_hours numeric NOT NULL,
--   total_hours numeric NOT NULL,
  prof numeric NOT NULL,
  flat_rate numeric NOT NULL,
--   bonus_rate numeric NOT NULL,
--   comm_hours numeric NOT NULL,
--   bonus_hours numeric NOT NULL,
  commission_pay numeric NOT NULL,
--   bonus_pay numeric NOT NULL,
  daily_guarantee numeric not null default '0',
  days_worked integer not null default '0',
  guarantee numeric not null default '0',
  tool_allow numeric NOT NULL,
  total_pay numeric NOT NULL,
  PRIMARY KEY (pay_period_seq, employee_number));

drop table if exists ts.pay_period_clock_hours;
create table ts.pay_period_clock_hours (
  pay_period_seq integer not null,
  from_date date not null,
  thru_date date not null,
  the_date date not null,
  employee_number citext not null,
  vision_id uuid not null,
  clock_hours numeric(5,2) not null default '0',
  pay_period_clock_hours numeric(5,2) not null default '0',
  primary key (employee_number,the_date));
  
drop table if exists ts.pay_period_ros;
create table ts.pay_period_ros (
  pay_period_seq integer not null,
  from_date date not null,
  thru_date date not null,
  the_date date not null,
  employee_number citext not null,
  vision_id uuid not null,
  ro citext not null,
  flag_hours numeric(6,2) not null,
  pay_period_flag_hours numeric(5,2) not null default '0',
  primary key (employee_number,ro,the_date));

select * from ts.ty_main_shop_flat_rate_payroll_data;

drop table if exists ts.pay_period_adjustments;
create table ts.pay_period_adjustments (
  pay_period_seq integer not null,
  employee_number citext not null,
  ro citext not null,
  adjustments numeric(6,2) not null,
  pay_period_adjustments numeric(5,2) not null default '0',
  primary key (employee_number,ro,pay_period_seq));  



  