﻿/*
08/13/22
i don't like using all these cte's too big a pia to troubleshoot
do discreet tables

-- look into generating techs via ukg tables 
-- looks like it will work for toyota, looks like it will be a good test to run nightly
*/

create or replace function ts.update_pay_period_clock_hours()
returns void as
$BODY$
/*
  select ts.update_pay_period_clock_hours()
*/
declare
	_pay_period_seq integer := 356; -- (
-- 	  select distinct biweekly_pay_period_sequence
-- 	  from dds.dim_date
-- 	  where the_date = current_date - 1);
	_from_date date := '07/31/2022'; --(
-- 	  select distinct biweekly_pay_period_start_date
-- 	  from dds.dim_date 
-- 	  where biweekly_pay_period_sequence = _pay_period_seq);
	_thru_date date := '08/13/2022'; --(
-- 	  select distinct biweekly_pay_period_end_date
-- 	  from dds.dim_date 
-- 	  where biweekly_pay_period_sequence = _pay_period_seq);	
begin
  delete 
  from ts.pay_period_clock_hours	
  where pay_period_seq = _pay_period_seq;

  insert into ts.pay_period_clock_hours(pay_period_seq,from_date,thru_date,the_date,
    employee_number,vision_id,clock_hours) 
  select _pay_period_seq, _from_date, _thru_date, b.the_date,
    a.employee_number, c.user_key, b.clock_hours
  from ts.main_shop_flat_rate_techs a
  join ukg.clock_hours b on a.employee_number = b.employee_number
    and b.the_date between _from_date and _thru_date 
  join  nrv.users c on a.employee_number = c.employee_number
    and c.is_active
  where a.thru_date > _thru_date;  

  update ts.pay_period_clock_hours a
  set pay_period_clock_hours = x.running_total
  from (
  select employee_number, the_date, 
		sum(clock_hours) over (partition by employee_number, pay_period_seq order by the_date) as running_total
  from ts.pay_period_clock_hours) x
  where a.employee_number = x.employee_number
    and a.the_date = x.the_date;
end;     
$BODY$
language plpgsql;

select * from ts.pay_period_clock_hours

create or replace function ts.update_pay_period_ros()
returns void as
$BODY$
/*
  select ts.update_pay_period_ros()
*/
declare
	_pay_period_seq integer := 356; -- (
-- 	  select distinct biweekly_pay_period_sequence
-- 	  from dds.dim_date
-- 	  where the_date = current_date - 1);
	_from_date date := '07/31/2022'; --(
-- 	  select distinct biweekly_pay_period_start_date
-- 	  from dds.dim_date 
-- 	  where biweekly_pay_period_sequence = _pay_period_seq);
	_thru_date date := '08/13/2022'; --(
-- 	  select distinct biweekly_pay_period_end_date
-- 	  from dds.dim_date 
-- 	  where biweekly_pay_period_sequence = _pay_period_seq);	
begin
  delete 
  from ts.pay_period_ros	
  where pay_period_seq = _pay_period_seq;

  insert into ts.pay_period_ros(pay_period_seq,from_date,thru_date,the_date,
    employee_number,vision_id,ro,flag_hours) 
  select _pay_period_seq, _from_date, _thru_date, a.flag_date,
    c.employee_number, d.user_key, a.ro, sum(a.flag_hours) as flag_hours
  from dds.fact_repair_order a
  join dds.dim_tech b on a.tech_key =  b.tech_key
  join ts.main_shop_flat_rate_techs c on b.employee_number = c.employee_number
    and c.thru_date > _thru_date
  join  nrv.users d on c.employee_number = d.employee_number
    and d.is_active
  where a.flag_date between _from_date and _thru_date
  group by a.ro, a.flag_date, c.employee_number, d.user_key;

  update ts.pay_period_ros a
  set pay_period_flag_hours = x.running_flag_hours
  from (
  select employee_number, the_date,
		sum(flag_hours) over (partition by employee_number, pay_period_seq order by the_date) as running_flag_hours
  from ts.pay_period_ros) x
  where a.employee_number = x.employee_number
    and a.the_date = x.the_date;
end;     
$BODY$
language plpgsql;

select employee_number, sum(flag_hours) from ts.pay_period_ros group by employee_number

select * from ts.pay_period_ros order by employee_number, the_date

select hs.get_honda_main_shop_flat_rate_tech ('5bd4fcb8-dbbc-4002-9698-d5020c1ef452', -1);
select * from nrv.users where last_name = 'johnson' and first_name = 'brandon'


create or replace function ts.update_pay_period_adjustments()
returns void as
$BODY$
/*
  select ts.update_pay_period_adjustments()
*/
declare
	_pay_period_seq integer := 356; -- (
-- 	  select distinct biweekly_pay_period_sequence
-- 	  from dds.dim_date
-- 	  where the_date = current_date - 1);
	_from_date date := '07/31/2022'; --(
-- 	  select distinct biweekly_pay_period_start_date
-- 	  from dds.dim_date 
-- 	  where biweekly_pay_period_sequence = _pay_period_seq);
	_thru_date date := '08/13/2022'; --(
-- 	  select distinct biweekly_pay_period_end_date
-- 	  from dds.dim_date 
-- 	  where biweekly_pay_period_sequence = _pay_period_seq);	
begin
  delete 
  from ts.pay_period_adjustments
  where pay_period_seq = _pay_period_seq;

  insert into ts.pay_period_adjustments(pay_period_seq,employee_number,ro,adjustments) 
	select _pay_period_seq, 
		a.employee_number, 
		b.ro_number as ro, sum(labor_hours) as flag_hours
	from ts.main_shop_flat_rate_techs a
	inner join arkona.ext_sdpxtim b on a.tech_number = b.technician_id
		and arkona.db2_integer_to_date_long(b.trans_date) between _from_date and _thru_date
	where a.thru_date > _thru_date
	group by a.employee_number, b.ro_number
	having sum(labor_hours) <> 0;

  update ts.pay_period_adjustments a
  set pay_period_adjustments = x.running_adjustments
  from(
  select employee_number, 
		sum(adjustments) over (partition by employee_number, pay_period_seq) as running_adjustments
  from ts.pay_period_adjustments) x
  where a.employee_number = x.employee_number;
end;     
$BODY$
language plpgsql;


select * from ts.pay_period_adjustments

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
i am a little queasy about the whole pay_period_ro/adj thing





































/*
select employee_number, user_key, first_name, last_name from nrv.users where employee_number in (select employee_number from ts.main_shop_flat_rate_techs);
select the_date, biweekly_pay_period_Start_date, biweekly_pay_period_end_date, biweekly_pay_period_sequence from dds.dim_date where biweekly_pay_period_sequence is not null
  and the_date = current_date
*/
CREATE OR REPLACE FUNCTION ts.get_toyota_main_shop_flat_rate_tech(
    _id text,
    _pay_period_indicator integer)
  RETURNS SETOF json AS
$BODY$  
/*
i don't know what the fuck
ran it as a function, got nothing
substituted values got results
select ts.get_toyota_main_shop_flat_rate_tech('e1d8d17e-0fa2-4fdd-b8bc-f807a6080e92', 356)
*/
declare
  _from_date date;
  _thru_date date;
  _current_pay_period_seq integer;
  _employee_number citext;
begin
  _current_pay_period_seq = (  --356
    select biweekly_pay_period_sequence
    from dds.dim_date
    where the_Date = current_date);  
  _from_date = ( --'07/31/2022'
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date = ( -- '08/13/2022'
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);    
  _employee_number = (
    select employee_number
    from nrv.users
    where user_key::text = _id);     
return query 
with 
  clock_hours as (
--   create temp table clock_hours as
    select a.the_date, d.employee_number, sum(clock_hours) as clock_hours
    from dds.dim_date a
    left join ukg.clock_hours b on a.the_date = b.the_date
    inner join ts.main_shop_flat_rate_techs d on b.employee_number = d.employee_number 
      and d.thru_date > '08/13/2022'
    where a.the_date between '07/31/2022' and '08/13/2022'
    group by a.the_date, d.employee_number
    order by a.the_date),
  pto_hours as (
-- create temp table pto_hours as  
    select a.the_date, d.employee_number, 
      sum(pto_hours + hol_hours) as pto_hours
    from dds.dim_date a
    left join ukg.clock_hours b on a.the_date = b.the_date
    inner join ts.main_shop_flat_rate_techs d on b.employee_number = d.employee_number
      and d.thru_date > '08/13/2022'
    where a.the_date between '07/31/2022' and '08/13/2022'
      and d.employee_number = '8100230' -- _employee_number
    group by a.the_date, d.employee_number
    having sum(pto_hours + hol_hours) > 0
    order by a.the_date),        
  ros as (
-- create temp table ros as  
    select a.the_date, d.employee_number, b.ro, sum(b.flag_hours) as flag_hours
    from dds.dim_date a
    left join dds.fact_repair_order b on a.the_date = b.flag_date
    inner join dds.dim_tech c on b.tech_key = c.tech_key
    inner join ts.main_shop_flat_rate_techs d on c.employee_number = d.employee_number 
      and d.thru_date > '08/13/2022'
    where a.the_date between '07/31/2022' and '08/13/2022'
      and d.employee_number = '8100230' -- _employee_number
    group by a.the_date, d.employee_number, b.ro),       
  production as (
--   create temp table production as
    select a.the_date as date, coalesce(b.clock_hours, 0) as clock_hours, coalesce(c.flag_hours, 0) as flag_hours
    from dds.dim_date a
    left join clock_hours b on a.the_date = b.the_date
      and b.employee_number = '8100230' -- _employee_number
    left join (
      select the_date, employee_number, sum(flag_hours) as flag_hours
      from ros
      where employee_number = '8100230' -- _employee_number
      group by the_date, employee_number) c on a.the_date = c.the_date
    where a.the_date between '07/31/2022' and '08/13/2022'), 
  adjustments as (
-- create temp table adjustments as  
    select a.tech_number, arkona.db2_integer_to_date_long(b.trans_date) as date, 
      b.ro_number as ro, 'Adjust after Close' as type, sum(labor_hours) as flag_hours
    from ts.main_shop_flat_rate_techs a
    inner join arkona.ext_sdpxtim b on a.tech_number = b.technician_id
    where arkona.db2_integer_to_date_long(b.trans_date) between '07/31/2022' and '08/13/2022'
      and a.employee_number = '8100230' -- _employee_number
      and a.thru_date > '08/13/2022'
    group by a.tech_number, arkona.db2_integer_to_date_long(b.trans_date), b.ro_number
    having sum(labor_hours) <> 0)       
select row_to_json(z)
from (
  select row_to_json(y) as toyota_main_shop_flat_rate_tech
  from (
    select bb.*,/*_id as employee,*/ total_flat_rate_pay as total_gross_pay
    from ( -- bb: added a level to generate total pay
      select aa.*, 
        case 
          when aa.total_clock_hours = 0 then 0
          else round(aa.total_flag_hours/aa.total_clock_hours, 2) 
        end as proficiency,
        aa.flat_rate * aa.total_flag_hours as total_flat_rate_pay,
        ( -- array of json objects, one row per day, clock hours and flag hours
          select coalesce(array_to_json(array_agg(row_to_json(bb))), '[]')
          from (
            select *
            from production) bb) as production,
        ( -- array of json objects, one row per day for which there are pto hours
          select coalesce(array_to_json(array_agg(row_to_json(cc))), '[]')
          from (
            select *
            from pto_hours) cc) as pto_pay,
        ( -- array of json objects, one row per ro
          select coalesce(array_to_json(array_agg(row_to_json(dd))), '[]')
          from (
            select the_date as date, ro, flag_hours
            from ros) dd) as ros,
        ( -- array of json objects, one row per ro
          select coalesce(array_to_json(array_agg(row_to_json(ee))), '[]')
          from (
            select date, ro, type, flag_hours
            from adjustments) ee) as adjustments  
      from ( -- aa top level tech data
        select --_id as id, _pay_period_indicator as pay_period_indicator, a.flat_rate, -- a.pto_rate, 
--           2 * a.flat_rate as bonus_rate,
          a.flat_rate,
          (select sum(clock_hours) from clock_hours where employee_number = '8100230') as total_clock_hours,
          coalesce((select sum(flag_hours) from ros where employee_number = '8100230'),0) 
          + 
          coalesce((select sum(flag_hours) from adjustments), 0) as total_flag_hours,
          coalesce((select sum(pto_hours) from pto_hours where employee_number = '8100230'), 0) as total_pto_hours,
          coalesce((select sum(flag_hours) from adjustments), 0) as total_adjustments
        from ts.main_shop_flat_rate_techs a
        inner join nrv.users b on a.employee_number = b.employee_number
        where b.user_key::text = 'e1d8d17e-0fa2-4fdd-b8bc-f807a6080e92' -- _id
          and a.thru_date > '08/13/2022') aa) bb) y) z;
end;    
$BODY$
  LANGUAGE plpgsql;