﻿select d.day_name, a.first_name, a.last_name, a.employee_number, d.the_date, sum(c.clock_hours), sum(c.pto_hours), sum(c.hol_hours)
from ukg.employees a
join ts.main_shop_flat_rate_techs b on a.employee_number = b.employee_number
join ukg.clock_hours c on b.employee_number = c.employee_number
join dds.dim_date d on c.the_date = d.the_date
  and d.biweekly_pay_period_sequence = (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1)
group by a.first_name, a.last_name, a.employee_number, d.the_date, d.day_name  

select * from ukg.clock_hours limit 10

select * from dds.dim_Date where the_date = current_date

select *
from dds.dim_date a
where a.biweekly_pay_period_sequence = (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1)

-- this looks close
-- per ben cahalan, any clock hours on a work day makes it a day worked
select a.first_name, a.last_name, a.employee_number, 
  count(*), 8 * b.daily_guarantee
from ukg.employees a
join ts.main_shop_flat_rate_techs b on a.employee_number = b.employee_number
join ukg.clock_hours c on b.employee_number = c.employee_number
join dds.dim_date d on c.the_date = d.the_date
  and d.biweekly_pay_period_sequence = (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1)
group by a.first_name, a.last_name, a.employee_number, b.daily_guarantee
having sum(clock_hours) > 0 

fucking guarantee
assuming 10 working days in the pay period
how many hours constitute a day worked? 
	eg, if the emp has 5 clock hours does it count as a day worked, 4 clock hours?
situation                            guarantee
tech works 11/12 days (1/2 saturday(s))			10 * guarantee

-- use working day department service to capture 10 working days per normal pay period and accounts for holidays
select *
from dds.working_days
where department = 'service'
  and biweekly_pay_period_sequence = 358
order by department, day_of_year

-- get the number of days worked based on any clock hours = day worked
/*
the maximum working days for the guarantee is the number of working days (not including holidays)
in the pay period
*/

do $$
declare 
	_pay_period_seq integer := (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1);
  _working_days integer := (
    select distinct wd_in_biweekly_pay_period
    from dds.working_days
    where department = 'service'
      and biweekly_pay_period_sequence = _pay_period_seq);

begin
drop table if exists wtf;
create temp table wtf as     
with
  employee_days_worked as (
    select a.employee_number, count(*) as days_worked
    from ts.main_shop_flat_rate_techs a
    join ukg.clock_hours b on a.employee_number = b.employee_number
    join dds.dim_date c on b.the_date = c.the_date
      and c.biweekly_pay_period_sequence = _pay_period_seq
    group by a.employee_number) 
select a.first_name, a.last_name, a.employee_number, 
  b.daily_guarantee, (select days_worked from employee_days_worked where employee_number = a.employee_number), _working_days,
  b.daily_guarantee *
		case
			when (select days_worked from employee_days_worked where employee_number = a.employee_number) > _working_days then _working_days 
			else (select days_worked from employee_days_worked where employee_number = a.employee_number)
   end as guarantee
from ukg.employees a
join ts.main_shop_flat_rate_techs b on a.employee_number = b.employee_number
join ukg.clock_hours c on b.employee_number = c.employee_number
join dds.dim_date d on c.the_date = d.the_date
  and d.biweekly_pay_period_sequence = (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1)
group by a.first_name, a.last_name, a.employee_number, 
  b.daily_guarantee, (select days_worked from employee_days_worked where employee_number = a.employee_number), _working_days,
  b.daily_guarantee
having sum(clock_hours) > 0;
end $$;
select * from wtf;

create or replace function ts.get_flat_rate_tech_guarantee()
returns table (employee_number citext,daily_guarantee numeric(5,2),days_worked integer,working_days integer,guarantee numeric(6,2)) as
$BODY$
/*
until october 2022 (if i remember correctly) the flate rate techs have a daily guarantee,
so for each day worked (> 0 clock hours on a given day), they are guaranteed that amount
the daily_guarantee is stored in ts.main_shop_flat_rate_techs

select * from ts.get_flat_rate_tech_guarantee()
*/
declare 
	_pay_period_seq integer := (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1);
  _working_days integer := (
    select distinct wd_in_biweekly_pay_period
    from dds.working_days
    where department = 'service'
      and biweekly_pay_period_sequence = _pay_period_seq);
begin 
  return query
	with
		employee_days_worked as (
			select a.employee_number as emp_number, count(*)::integer as the_days_worked
			from ts.main_shop_flat_rate_techs a
			join ukg.clock_hours b on a.employee_number = b.employee_number
			join dds.dim_date c on b.the_date = c.the_date
				and c.biweekly_pay_period_sequence = _pay_period_seq
			group by a.employee_number) 
	select a.employee_number, b.daily_guarantee, 
		(select the_days_worked from employee_days_worked where emp_number = a.employee_number), _working_days,
		b.daily_guarantee *
			case
				when (select the_days_worked from employee_days_worked where emp_number = a.employee_number) > _working_days then _working_days 
				else (select the_days_worked from employee_days_worked where emp_number = a.employee_number)
		 end as guarantee
	from ukg.employees a
	join ts.main_shop_flat_rate_techs b on a.employee_number = b.employee_number
	join ukg.clock_hours c on b.employee_number = c.employee_number
	join dds.dim_date d on c.the_date = d.the_date
		and d.biweekly_pay_period_sequence = (
			select distinct biweekly_pay_period_sequence
			from dds.dim_date
			where the_date = current_date - 1)
	group by a.employee_number, b.daily_guarantee, 
		(select the_days_worked from employee_days_worked where emp_number = a.employee_number), _working_days,
		b.daily_guarantee
	having sum(clock_hours) > 0;
end;
$BODY$
language plpgsql;

