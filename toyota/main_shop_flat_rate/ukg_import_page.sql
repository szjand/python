﻿
CREATE OR REPLACE FUNCTION ts.update_ty_main_shop_flat_rate_payroll_data()
  RETURNS void AS
$BODY$
/*
this populates a table which has all the data needed for UKG Imports

select ts.update_ty_main_shop_flat_rate_payroll_data()

select * from ts.ty_main_shop_flat_rate_payroll_data
*/
declare 
	_pay_period_seq integer := (
	  select distinct biweekly_pay_period_sequence
	  from dds.dim_date
	  where the_date = current_date - 1);
	_from_date date := (
	  select distinct biweekly_pay_period_start_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);
	_thru_date date := (
	  select distinct biweekly_pay_period_end_date
	  from dds.dim_date 
	  where biweekly_pay_period_sequence = _pay_period_seq);	
  _working_days integer := (
    select distinct wd_in_biweekly_pay_period
    from dds.working_days
    where department = 'service'
      and biweekly_pay_period_sequence = _pay_period_seq);
        
begin 		
  delete 
  from  ts.ty_main_shop_flat_rate_payroll_data
  where pay_period_seq = _pay_period_seq;
  
	insert into ts.ty_main_shop_flat_rate_payroll_data (pay_period_start,pay_period_end,pay_period_seq,pay_period_select_format,
		first_saturday,last_name,first_name,employee_number,hire_date,term_date,flag_hours,adjustments,total_flag_hours,
		clock_hours,prof,flat_rate,commission_pay,daily_guarantee,days_worked,guarantee,tool_allow,total_pay)
-- drop table if exists wtf;
-- create temp table wtf as
	select aa.pay_period_start, aa.pay_period_end, aa.pay_period_seq, aa.pay_period_select_format, aa.first_saturday,
	  aa.last_name, aa.first_name, aa.employee_number, aa.hire_date, aa.term_date, aa.flag_hours, aa.adjustments,
	  aa.total_flag_hours, aa.clock_hours, aa.prof, aa.flat_rate,
		aa.flat_rate * (aa.flag_hours + adjustments) as comm_pay,
		aa.daily_guarantee, aa.days_worked, aa.guarantee,
		aa.tool_allowance,
		case
		  when aa.guarantee > (aa.flat_rate * aa.flag_hours) then aa.guarantee + aa.tool_allowance
		  else (aa.flat_rate * aa.flag_hours) + aa.tool_allowance
		end as total_pay
	from (
		with 
			clock_hours as ( 
				select d.employee_number,
					sum(coalesce(clock_hours, 0)) as clock_hours, 
					sum(coalesce(pto_hours, 0)) as pto_hours,
					sum(coalesce(hol_hours, 0)) as hol_hours,
					sum(coalesce(clock_hours, 0) + coalesce(pto_hours, 0) + coalesce(hol_hours, 0)) as total_hours
				from dds.dim_date a
				join ukg.clock_hours b on a.the_date = b.the_date
				inner join ts.main_shop_flat_rate_techs d on b.employee_number = d.employee_number
					and d.thru_date > _thru_date
				where a.the_date between _from_date and _thru_date
				group by d.employee_number),
			flag_hours as (
		--     create temp table flag_hours as 
				select d.employee_number, sum(b.flag_hours) as flag_hours
				from dds.dim_date a
				inner join dds.fact_repair_order b on a.the_date = b.flag_date
				inner join dds.dim_tech c on b.tech_key = c.tech_key
				inner join ts.main_shop_flat_rate_techs d on c.employee_number = d.employee_number
					and d.thru_date > _thru_date
				where a.the_date between _from_date and _thru_date
				group by d.employee_number),
			adjustments as (
		--     create temp table adjustments as
				select a.tech_number, a.employee_number, sum(labor_hours) as flag_hours
				from ts.main_shop_flat_rate_techs a
				inner join arkona.ext_sdpxtim b on a.tech_number = b.technician_id
				where arkona.db2_integer_to_date_long(b.trans_date) between _from_date and _thru_date
					and a.thru_date > _thru_date
				group by a.tech_number, a.employee_number
				having sum(labor_hours) <> 0)
		select 
			_from_date as pay_period_start, _thru_date as pay_period_end, _pay_period_seq as pay_period_seq,
			trim(to_char(_from_date, 'Month')) || ' ' || extract(day from _from_date) || ' - ' || trim(to_char(_thru_date, 'Month')) || ' ' || extract(day from _thru_date) || ', ' || extract(year from _from_date) as pay_period_select_format,
			_from_date + 6 as first_saturday,
			aa.last_name, aa.first_name, a.employee_number, 
			aa.hire_date, aa.term_date,  
			c.flag_hours, coalesce(d.flag_hours, 0) as adjustments,
			c.flag_hours + coalesce(d.flag_hours,0) as total_flag_hours,
			b.clock_hours, b.pto_hours, b.hol_hours, b.total_hours,
			case
				when b.clock_hours = 0 then 0
				else coalesce(c.flag_hours + coalesce(d.flag_hours,0), 0) / b.clock_hours 
			end as prof,
			a.flat_rate, a.tool_allowance,
			e.daily_guarantee, 
			case
			  when e.days_worked  > e.working_days then e.working_days
			  else e.days_worked
			end as days_worked,
			e.guarantee
		from ts.main_shop_flat_rate_techs a
		join ukg.employees aa on a.employee_number = aa.employee_number
		join clock_hours b on a.employee_number = b.employee_number
		join flag_hours c on a.employee_number = c.employee_number
		left join adjustments d on a.employee_number = d.employee_number
		left join ts.get_flat_rate_tech_guarantee() e on a.employee_number = e.employee_number
		where a.thru_date > _thru_date) aa;
end
$BODY$
  LANGUAGE plpgsql;

