object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Toyota Window Stickers'
  ClientHeight = 645
  ClientWidth = 990
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 64
    Top = 88
    Width = 711
    Height = 57
    Caption = 
      'Store hours are stored as a Blob, but an RTF'#13#10'in LocationWindowS' +
      'tickerInfo.LocationServiceSalesInfo'#13#10'File is stored in working d' +
      'irectory as 77a18168-998f-8747-ae24-3efc540cd069.rtf'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 344
    Top = 405
    Width = 57
    Height = 13
    Caption = 'LodcationID'
  end
  object Label3: TLabel
    Left = 344
    Top = 453
    Width = 57
    Height = 13
    Caption = 'Image Type'
  end
  object Label4: TLabel
    Left = 344
    Top = 501
    Width = 52
    Height = 13
    Caption = 'Image Size'
  end
  object Button1: TButton
    Left = 64
    Top = 49
    Width = 185
    Height = 33
    Caption = 'Test: Download Store Hours Blob'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 64
    Top = 176
    Width = 161
    Height = 33
    Caption = 'Replace Store Hours Blob'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 64
    Top = 256
    Width = 177
    Height = 33
    Caption = 'Blob Sizes from  LocationImages'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 64
    Top = 320
    Width = 177
    Height = 41
    Caption = 'Download LocationImage images'
    TabOrder = 3
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 64
    Top = 416
    Width = 177
    Height = 41
    Caption = 'Append to LocationImages'
    TabOrder = 4
    OnClick = Button5Click
  end
  object LocationIDTB: TEdit
    Left = 344
    Top = 424
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'LocationIDTB'
  end
  object SizeTB: TEdit
    Left = 344
    Top = 520
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object TypTB: TEdit
    Left = 344
    Top = 472
    Width = 121
    Height = 21
    TabOrder = 7
    Text = 'TypTB'
  end
  object AdsConnection1: TAdsConnection
    AliasName = 'DpsVSeries'
    IsConnected = True
    LoginPrompt = False
    Username = 'adssys'
    Password = 'cartiva'
    Left = 480
    Top = 24
  end
  object AdsQuery1: TAdsQuery
    DatabaseName = 'AdsConnection1'
    AdsConnection = AdsConnection1
    Left = 552
    Top = 24
    ParamData = <>
  end
  object AdsTable1: TAdsTable
    AdsConnection = AdsConnection1
    TableName = 'LocationWindowStickerInfo'
    Left = 648
    Top = 32
  end
  object AdsTable2: TAdsTable
    AdsConnection = AdsConnection1
    TableName = 'LocationImages'
    Left = 784
    Top = 32
  end
end
