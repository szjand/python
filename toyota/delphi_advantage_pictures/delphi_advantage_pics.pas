unit delphi_advantage_pics;
// This app is a copy of ResizeExistPics modified to update the tool with
// Toyota data for Window Stickers

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, adsdata, adsfunc, adstable, adscnnct, Grids, DBGrids,
  dateutils,
  jpeg,
  dbtables,
  StdCtrls,
  ExtCtrls, adsdictionary,
  StrUtils, ExtDlgs;

type
  TForm2 = class(TForm)
    AdsConnection1: TAdsConnection;
    AdsQuery1: TAdsQuery;
    Button1: TButton;
    Label1: TLabel;
    Button2: TButton;
    AdsTable1: TAdsTable;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    LocationIDTB: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    SizeTB: TEdit;
    Label4: TLabel;
    AdsTable2: TAdsTable;
    TypTB: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
var
//  this code successfully downloads and names the blob of store hours from
//    from LocationServiceSalesInfo
  MyBlobStream: TStream;
begin
  with adsquery1 do
  begin
    AdsQuery1.Close;
    AdsQuery1.SQL.Text := 'SELECT * FROM LocationWindowStickerInfo WHERE ' +
    'locationid = ''4CD8E72C-DC39-4544-B303-954C99176671''';
    adsquery1.Open;

    begin
      MyBlobStream := AdsQuery1.CreateBlobStream(adsquery1.FieldByName('LocationServiceSalesInfo'),bmread);
      try
        begin
          TBlobField(adsquery1.FieldByName('LocationServiceSalesInfo')).SaveToFile('Z:\E\python_projects\toyota\delphi_advantage_pictures\' +
          AdsQuery1.FieldByName('LocationID').AsString + '.rtf');
        end
      finally
        MyBlobStream.Free;
      end;
    end;
  end;
end;


procedure TForm2.Button2Click(Sender: TObject);
// this was a bit of a cluster fuck, disclaimer wouldn't work had to update
// that in sequel, but this got the store hours in
var
  locationid: string;
  phone_number: string;
  url: string;
  disclaimer: string;
begin
  locationid := '77a18168-998f-8747-ae24-3efc540cd069';
  phone_number := '(701)787-6259';
  url := 'www.Rydellcars.com';
  with AdsQuery1 do
    begin
      AdsQuery1.Close;
      AdsQuery1.SQL.Text := 'SELECT * FROM locationWindowStickerInfo p WHERE locationid = ''B6183892-C79D-4489-A58C-B526DF948B06''';
      AdsQuery1.Open;
      disclaimer := 'the disclaimer'; //AdsQuery1.FieldByName('LocationDisclaimer').Text;
      AdsConnection1.BeginTransaction;
      try
        AdsTable1.Open;
        AdsTable1.Append;
        AdsTable1.FieldByName('LocationID').AsString := locationid;
        AdsTable1.FieldByName('LocationPhoneNumber').AsString := phone_number;
//        AdsTable1.FieldByName('LocationDisclaimer').AsString := disclaimer;
        TBlobField(AdsTable1.FieldByName('LocationServiceSalesInfo')).LoadFromFile('Z:\E\python_projects\toyota\delphi_advantage_pictures\toyota_hours.rtf');
        AdsTable1.FieldByName('LocationURL').AsString := url;
        AdsTable1.Post;
        AdsConnection1.Commit;
        AdsTable1.Close;
      finally

      end;
    end;
end;

procedure TForm2.Button3Click(Sender: TObject);
// copy of GetBlobFieldSize
var
  FileName: String;
  S: String;
  SList: TSTringList;
  F: TextFile;
  LocationFld: TField;
  TypFld: TField;
  SizeFld: TField;
  PicSizeString: String;
begin
  try
    SList := TStringList.Create;
    FileName := '..\PicSizes.csv';

    AssignFile(F, FileName);
    if FileExists(FileName) then
      DeleteFile(FileName);
    ReWrite(F);
    adsquery1.SQL.Text := 'SELECT * from LocationImages where ' +
    'LocationID = ''B6183892-C79D-4489-A58C-B526DF948B06'' and ThruTS is null';
    with AdsQuery1 do  //for each item in the inventory
    begin
      Open;
      LocationFld := FieldByName('locationid');
      TypFld := FieldByName('typ');
      SizeFld := FieldByName('size');
      First;
      while not Eof do
        begin
          PicSizeString := IntToStr(TBlobField(AdsQuery1.FieldByName('image')).BlobSize);
          SList.Clear;
          SList.Add(LocationFld.AsString);
          SList.Add(TypFld.AsString);
          SList.Add(SizeFld.AsString);
          SList.Add(PicSizeString);
          S := SList.CommaText;
          WriteLn(F, S);
          Next;
        end;
      end;
  finally
    SList.Free;
  end;
end;

procedure TForm2.Button4Click(Sender: TObject);
var
//  MyJpeg: TJpegImage;
  MyBlobStream: TStream;
begin
  with adsquery1 do
  begin
    AdsQuery1.Close;
    AdsQuery1.SQL.Text := 'SELECT * from LocationImages where LocationID = ''B6183892-C79D-4489-A58C-B526DF948B06'' and ThruTS is null';
    adsquery1.Open;
    First;
    while not Eof do
    begin
      MyBlobStream := AdsQuery1.CreateBlobStream(adsquery1.FieldByName('image'),bmread);
      try
        if TBlobField(AdsQuery1.FieldByName('image')).BlobSize > 1 then
        begin
          TBlobField(adsquery1.FieldByName('image')).SaveToFile('Z:\E\python_projects\toyota\delphi_advantage_pictures\location_images_pictures\' +
          AdsQuery1.FieldByName('locationid').AsString + '-' + AdsQuery1.FieldByName('size').AsString + '.jpg');
          next;
        end
        else
          next;
      finally
        MyBlobStream.Free;
      end;
    end;
  end;
end;

procedure TForm2.Button5Click(Sender: TObject);
// need to figure out why logo doesn't show on window sticker, maybe the size field
var
  FromTS: TDateTime;
begin
  with AdsQuery1 do
    begin
      FromTS := Now;
//      AdsQuery1.Close;
//      AdsQuery1.SQL.Text := 'SELECT * FROM locationWindowStickerInfo p WHERE locationid = ''B6183892-C79D-4489-A58C-B526DF948B06''';
//      AdsQuery1.Open;
      AdsConnection1.BeginTransaction;
      try
        AdsTable2.Open;
        AdsTable2.Append;
        AdsTable2.FieldByName('LocationID').AsString := LocationIDTB.Text;
        AdsTable2.FieldByName('Typ').AsString := TypTB.Text;
        AdsTable2.FieldByName('Size').AsString := SizeTB.Text;
        TBlobField(AdsTable2.FieldByName('Image')).LoadFromFile('Z:\E\python_projects\toyota\delphi_advantage_pictures\toyota_store_logos\rydell-toyota_v1.1_sqr-prim_200x60.jpg');
        AdsTable2.FieldByName('FromTS').AsDateTime := FromTS;
        AdsTable2.Post;
        AdsConnection1.Commit;
        AdsTable2.Close;
      finally

      end;
    end;
end;

end.
