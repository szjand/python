import db_cnx
import requests
"""
5/20/18: this worked loaded 2536 vins & describe-vehicle responses into chr.describe_vehicle
5/16/19
    deleted all from chr.describe-vehicle and repoplulated it from jon.describe-vehicle
    this is now populated from the endpoint: https://beta.rydellvision.com:8888/chrome/describe-vehicle-options/' + vin
    which includes the ShowAvailableEquipment switch
"""
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        # sql = """select vin from jon.vins;"""
        # sql = "select '2T3A1RFV9NW324592'"
        sql = """
            select vin
            from nc.stg_availability a
            where not exists (
                select 1
                from chr.describe_vehicle
                where vin = a.vin);
        """
        # # open orders
        # sql = """
        #     select a.vin
        #     from nc.open_orders a
        #     where a.vin is not null
        #       and not exists (
        #         select 1
        #         from chr.describe_vehicle
        #         where vin = a.vin)
        # """
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin = row[0]
            # url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle/' + vin
            url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-options/' + vin
            data = requests.get(url)
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into chr.describe_vehicle(vin,response)
                    values ('{0}','{1}');
                """.format(vin, resp)
                chr_cur.execute(sql)
        with pg_con.cursor() as chr_cur:
            sql = """
                update chr.describe_vehicle y
                set style_count = x.style_count
                from (
                  select vin, count(*) as style_count
                  from (
                    select vin, jsonb_array_elements(response->'style')
                    from chr.describe_vehicle
                    where style_count is null) a
                  group by vin) x
                where x.vin = y.vin;  
            """
            chr_cur.execute(sql)
