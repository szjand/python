﻿in the ext_chrome project, there are now 2 files
describe_vehicle.py
  endpoint: 'https://beta.rydellvision.com:8888/chrome/describe-vehicle/' + vin
  calls describeVehicle without the ShowAllEquipment switch
jon_describe_vehicle.py
  endpoint: 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-options/' + vin 
  calls describeVehicle with the ShowAllEquipment switch

the describe-vehicle-options is for colors, option codes etc

  
select b.*, r.colors->'attributes'->>'colorName'
from chr.describe_vehicle a
join jsonb_array_elements(a.response->'exteriorColor') as r(colors) on true
join nc.stg_availability b on a.vin = b.vin
  and b.color_code  = r.colors->'attributes'->>'colorCode'
where a.vin = '5N1AT2MV8KC746612'




select a.vin, r.colors->'attributes'->>'colorName', r.colors->'attributes'->>'colorCode'
from chr.describe_vehicle a
join jsonb_array_elements(a.response->'exteriorColor') as r(colors) on true
  and r.colors->'attributes'->>'colorName' like 'Storm%'

select vin
from (
  select vin, count(r.colors->'attributes'->>'colorName') as has_colors
  from chr.describe_vehicle a
  left join jsonb_array_elements(a.response->'exteriorColor') as r(colors) on true
  group by vin) b
where has_colors = 0  


select count(*)  -- 8037
from chr.describe_vehicle


select vin, count(r.options->'header'->>'$value') 
from chr.describe_vehicle a
left join jsonb_array_elements(a.response->'factoryOption') as r(options) on true
group by vin
order by count(r.options->'header'->>'$value') desc


select * 
from chr.describe_vehicle
where vin = '1G1Y12D70J5102429'


select a.vin, r.options->'header'->>'$value', count(*)
from chr.describe_vehicle a
join jsonb_array_elements(a.response->'factoryOption') as r(options) on true
where a.vin = '1G1Y12D70J5102429'
group by a.vin, r.options->'header'->>'$value'


select vin
from (
  select vin, count(r.options->'header'->>'$value') as factory_options
  from chr.describe_vehicle a
  left join jsonb_array_elements(a.response->'factoryOption') as r(options) on true
  group by vin) b
where factory_options > 2


delete from jon.describe_vehicle

select count(*) from jon.describe_vehicle

-- this turns out to be the best partitioning parceling out groups to run
            select right(vin, 1), count(*)
            from (
              select vin, count(r.options->'header'->>'$value') as factory_options
              from chr.describe_vehicle a
              left join jsonb_array_elements(a.response->'factoryOption') as r(options) on true
              group by vin) b
            where factory_options < 3
              and not exists (
                select 1
                from jon.describe_vehicle
                where vin = b.vin)
            group by right(vin, 1)
            order by right(vin, 1)


select * from nc.vehicles a
where not exists (
  select 1
  from jon.describe_vehicle
  where vin = a.vin)


select '3GCUKSEC2JG637124',  '3GCUKREC2JG518272'

select * from jon.describe_vehicle where vin = '3GCUKSEC2JG637124'

select * from jon.describe_vehicle limit 100

delete 
from chr.describe_vehicle
where vin in ('5J6RW2H56JL025898','1G1Y12D70J5102429','3GNCJKSB1KL255340','5N1AT2MV8KC746612','1N4BL4DW6KN309487','5GAEVBKW4KJ195278') 


---- colors
select b.*
from jon.describe_vehicle a
left join jsonb_array_elements(a.response->'exteriorColor') with ordinality as b(colors) on true
where a.vin = '3GCUKSEC2JG637124'


select c.style_id, b.colors->'attributes'->>'colorCode', b.colors->'attributes'->>'colorName'
from jon.describe_vehicle a
left join jsonb_array_elements(a.response->'exteriorColor') with ordinality as b(colors) on true
left join jsonb_array_elements(b.colors->'styleId') as c(style_id)on true
-- where a.vin = '3GCUKSEC2JG637124'
limit 100      




---- colors
select a.vin, 
  r.style ->'attributes'->>'modelYear' as model_year,
  r.style ->'division'->>'$value' as make,
  r.style ->'model'->>'$value' as model,
  b.colors->'attributes'->>'colorCode' as color_code,
  b.colors->'attributes'->>'colorName' as color,
  b.ordinality
-- select count(*)
from jon.describe_vehicle a
join jsonb_array_elements(a.response->'exteriorColor') with ordinality as b(colors) on true
join jsonb_array_elements(a.response->'style') as r(style) on true
limit 100      

drop table if exists nc.chrome_colors;
create table nc.chrome_colors (
  model_year integer not null,
  make citext not null,
  model citext not null,
  color_code citext not null,
  color citext not null,
  primary key (model_year,make,model,color_code,color));
insert into nc.chrome_colors  
select model_year::integer, make, model, color_code, color
from (
  select a.vin, 
    r.style ->'attributes'->>'modelYear' as model_year,
    r.style ->'division'->>'$value' as make,
    r.style ->'model'->>'$value' as model,
    b.colors->'attributes'->>'colorCode' as color_code,
    b.colors->'attributes'->>'colorName' as color,
    b.ordinality
  -- select count(*)
  from jon.describe_vehicle a
  join jsonb_array_elements(a.response->'exteriorColor') with ordinality as b(colors) on true
  join jsonb_array_elements(a.response->'style') as r(style) on true) x
where model_year in ('2018','2019')
group by model_year, make, model, color_code, color;

select model_year, make, model, color
from nc.chrome_colors  
group by model_year, make, model, color
having count(*) > 1

select model_year, make, model, color_code, color
from nc.chrome_colors  
group by model_year, make, model, color_code, color
having count(*) > 1

select * from nc.chrome_colors where model_year = 2018 and make = 'honda' and model = 'pilot' order by color

select * from nc.chrome_colors where color like 'BRILLIANT SILVE%' order by color

select * from nc.exterior_colors where mfr_color_name like 'BRILLIANT SILVE%' 

select count(distinct color) from nc.chrome_colors

select count(*) from nc.chrome_colors

select *
from nc.vehicles a
left join nc.chrome_colors b on a.color = b.color
where b.color is null
  and a.model_year > 2017

select *
from nc.vehicles a
left join nc.exterior_colors b on a.color = b.mfr_color_name
where b.mfr_color_name is null
  and a.model_year > 2017

update nc.vehicles
set color = 'Satin Steel Metallic'
where color = 'SATIN STEEL GRAY METALLIC';

select * from nc.chrome_colors where color like '%satin%' and model = 'encore'
select * from nc.exterior_colors where mfr_color_name like '%platinum%' and model = 'accord'
