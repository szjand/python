﻿-- using sandbox chrome data (10.130.196.174)
-- 2017 is most recent year

select *
from chr.styles a 
join chr.models b on a.model_id = b.model_id
  and b.model_year = 2017
  and b.model_name = 'traverse'

select *
from chr.models
where model_name = 'traverse'  

select * 
from chr.colors
where style_id in (383100,383101,383108)


select * from chr.opt_kinds

select *
from chr.options 
where option_kind_id = 6
  and style_id in (383100,383101,383102,383103,383104,383105,383106,383107,383108)

select * 
from chr.opt_headers
where header_id = 1160

select *
from chr.models
where model_name = 'Silverado 1500'
  and model_year = 2017


select * 
from chr.styles
where model_id = 29271


  