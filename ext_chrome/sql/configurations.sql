﻿
-- ****************** need validations check on nc.allocation_groups **************************
-- using code fom nc_inventory/sql/interiors.sql, ext_chrome/scripts/chrome_scratchpad_201905.sql/engine_scratchpad.sql, 
-- all tables based on chr.get_describe_vehicle_by_style_id.chrome_style_id
-- configurations
-- engines

8/18/19
ran the first luigi extract of version info
version changed from 361467 to 361532
but no changes to the tables of interest: models, divisions, styles, describe by styleid w/ShowAvailableEquipment
at this point, thinking weekly is good enough, but still need to automate it

10/24/19 may want to consider including fleet only, then make that an attribute for filtering
------------------------------------------------------------------------------------------------------------
--< chr.configurations
------------------------------------------------------------------------------------------------------------
-- need to figure out what to do about null trim_level
-- need to assert if make in chev,buick,gmc,cad peg and alloc_group <> n/a 
drop table if exists chr.configurations cascade;
create table if not exists chr.configurations (
  chrome_style_id citext primary key,
  model_year integer not null,
  make citext not null,
  model citext not null,
  alloc_group citext not null,
  cab citext not null,
  drive citext not null,
  model_code citext not null,
  box citext not null,
  trim_level citext,
  peg citext not null,
  style_name citext not null,
  subdivision citext not null,
  market_class citext not null);
comment on table chr.configurations is 'chrome configurations at the level of chrome_style_id, does
  not include engines, interiors, colors. Successful responses from chr.get_describe_vehicle_by_style_id,
  non fleet, 2018 and above, honda, nissan, chevrolet, buick, gmc, cadillac only.
  Need to figure out what to do about null trim_level';
create unique index on chr.configurations(model_year, model_code, peg);

select * from chr.configurations

-- drop table if exists config;
-- create temp table config as
-- -- successful responses from chr.get_describe_vehicle_by_style_id
-- -- non fleet only
-- -- core attributes 
-- -- at the end add, subdivion, marketClass
-- -- do engines, interiors in separate tables, multiple values per chrome_Style_id
-- -- 2018 and above, honda, nissan, chevrolet, buick, gmc, cadillac only

-----------------------------------------------------------------------
--< first do a test of the new data for any changed rows
-----------------------------------------------------------------------
drop table if exists tmp_configurations;
create temp table tmp_configurations as
select aa.chrome_style_id,aa.model_year::integer, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
  aa.drive, aa.model_code, aa.box, aa.trim_level, coalesce(aa.peg, 'n/a') as peg, aa.style_name, 
  aa.subdivision, aa.marketclass
from (
  select e.chrome_style_id,
    f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'attributes' ->> 'mfrModelCode' as model_code,
    f.style -> 'attributes' ->> 'trim' as trim_level,
   case f.style -> 'attributes' ->> 'drivetrain'
      when 'Front Wheel Drive' then 'FWD'
      when 'All Wheel Drive' then 'AWD'
      when 'Rear Wheel Drive' then 'RWD'
      when 'Four Wheel Drive' then '4WD'
      else 'XXXXXX'
    end::citext as drive,     
-- changed extended to double
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then 
          case
            when trim(
              left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1)) = 'Extended' then 'Double'
            else trim(left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
          end
      when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
        then trim(
          left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
      else 'n/a'
    end::citext as cab,
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then trim(
          replace(
            substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
              position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
      else 'n/a'
    end::citext as box,        
    h.pegs -> 'attributes' ->> 'oemCode' as peg,
    f.style -> 'attributes' ->> 'name' as style_name,
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'marketClass' ->> '$value' as marketClass
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
    and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false') aa
left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
  and aa.model_code = bb.model_code;

do 
$$
begin
assert (
  select count(*)
  from (
    select chrome_style_id, md5(tmp_configurations::text) as hash
    from tmp_configurations) aa
  join (
    select chrome_style_id, md5(a::text) as hash
    from chr.configurations a) bb on aa.chrome_style_id = bb.chrome_style_id 
      and aa.hash <> bb.hash) = 0, 'Uh Oh, changed data';
end
$$;      

-----------------------------------------------------------------------
--/> first do a test of the new data for any changed rows
-----------------------------------------------------------------------
  

insert into chr.configurations
select aa.chrome_style_id,aa.model_year::integer, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
  aa.drive, aa.model_code, aa.box, aa.trim_level, coalesce(aa.peg, 'n/a') as peg, aa.style_name, 
  aa.subdivision, aa.marketclass
from (
  select e.chrome_style_id,
    f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'attributes' ->> 'mfrModelCode' as model_code,
    f.style -> 'attributes' ->> 'trim' as trim_level,
   case f.style -> 'attributes' ->> 'drivetrain'
      when 'Front Wheel Drive' then 'FWD'
      when 'All Wheel Drive' then 'AWD'
      when 'Rear Wheel Drive' then 'RWD'
      when 'Four Wheel Drive' then '4WD'
      else 'XXXXXX'
    end::citext as drive,     
-- changed extended to double
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then 
          case
            when trim(
              left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1)) = 'Extended' then 'Double'
            else trim(left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
          end
      when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
        then trim(
          left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
      else 'n/a'
    end::citext as cab,
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then trim(
          replace(
            substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
              position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
      else 'n/a'
    end::citext as box,        
    h.pegs -> 'attributes' ->> 'oemCode' as peg,
    f.style -> 'attributes' ->> 'name' as style_name,
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'marketClass' ->> '$value' as marketClass
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
    and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false') aa
left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
  and aa.model_code = bb.model_code
on conflict(chrome_style_id) do nothing
returning *;


unique:
select chrome_style_id from config group by chrome_style_id having count(*) > 1
select model_year, model_code, peg from config group by model_year, model_code,peg having count(*) > 1

-- 6/2, 7/4, 7/7, 7/14,                                                                7/14       7/22      7/29    8/12    8/17    8/25    9/2     9/7     9/16    9/22    10/14
select count(*) from chr.models  --262/267/267/                                         270       272       275     277     277     279     285     285     287     288     291
select count(*) from chr.styles --2439/2478/2478/2495/                                  2498      2501      2524    2540    2540    2553    2592    2592    2596    2605    2621
select count(*) from chr.get_describe_vehicle_by_style_id --2405/2418/2418/2466/        2483      2486      2503    2538    2540    2553    2553    2553    2596    2605    2621
select count(*) from chr.configurations --2374//                                        2422      2442      2459    2494    2496    2509    2509    2509    2552    2561    2577
select vers -> 'attributes' ->> 'date', vers -> 'attributes' ->> 'build'  --            359221    3597171   360151  361115  361467  362112  362605  362950  363015  364054  365671
-- select *
from chr.get_version_info
join jsonb_array_elements(response->'data') as a(vers) on true
where vers -> 'attributes' ->> 'country' = 'US'
order by vers -> 'attributes' ->> 'date'



------------------------------------------------------------------------------------------------------------
--/> chr.configurations
------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
--< chr.engines
------------------------------------------------------------------------------------------------------------
-- drop table if exists chr.gm_engines cascade;

drop table if exists chr.engines cascade;
create table chr.engines (
  chrome_style_id citext not null references chr.configurations(chrome_style_id),
  option_code citext not null,
  description citext not null,
  displacement citext not null,
  diesel boolean not null,
  primary key (chrome_style_id,option_code));
comment on table chr.engines is 'option_code only available on gm engines, for honda nissan, 
    engines are unique per chrome style id. data derived from chr.get_describe_vehicle_by_style_id';

insert into chr.engines
select aa.*
from (
  select a.chrome_style_id, 
    f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
    replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '') as description,
    case
      when position('L' in replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '')) = 0
        then replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '')
      else replace(
        substring(
          replace(
            replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''), 'DIESEL',''),
        position('L' in replace(
          replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''), 'DIESEL','')) -3, 4), 'L', '')
    end as displacement,
    case
      when position('DIESEL' in upper(replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''))) <> 0 then true
      else false
    end as diesel     
  from chr.get_describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'style') as r(style) on true
    and r.style ->'attributes'->>'fleetOnly' = 'false'
    and (r.style ->'attributes'->>'modelYear')::integer > 2017
    and r.style ->'division'->>'$value' in ('Buick','Chevrolet','GMC','Cadillac')
  join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true  
    and f.factory_option -> 'header' ->> '$value' = 'ENGINE'  
  union -- honda/nissan: no option_code
  select a.chrome_Style_id, 'n/a',
    replace(j.eng_descr ->> 'description', 'Engine: ', ''),
    -- no displacement on electric
    coalesce(i.hn_engines -> 'displacement' -> 'value' -> 0 ->> '$value', 'n/a') as displacement,
      case
      when position('DIESEL' in upper(replace(j.eng_descr ->> 'description', 'Engine: ', ''))) <> 0 then true
      else false
    end as diesel 
  from chr.get_describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'style') as r(style) on true
    and r.style ->'attributes'->>'fleetOnly' = 'false'
    and r.style ->'attributes'->>'modelYear' in ('2018','2019','2020')
    and r.style ->'division'->>'$value' in ('Honda','Nissan')
  left join jsonb_array_elements(a.response -> 'engine') as i(hn_engines) on true  
  left join jsonb_array_elements(a.response -> 'standard') as j(eng_descr) on true
    and j.eng_descr ->> 'description' like '%Engine:%') aa
left join (    
  select distinct chrome_style_id, option_code
  from chr.engines)  bb on aa.chrome_Style_id = bb.chrome_Style_id
    and aa.option_code = bb.option_code
where bb.chrome_style_id is null;


------------------------------------------------------------------------------------------------------------
--/> chr.engines
------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
--< transmissions: GM only, from .../ext_chrome/scripts/body_types_transmissions.sql
------------------------------------------------------------------------------------------------------------
drop table if exists chr.transmissions;
create table chr.transmissions (
  chrome_style_id citext not null references chr.configurations(chrome_style_id),
  option_code citext not null,
  description citext not null,
  primary key (chrome_style_id, option_code));

insert into chr.transmissions  
select a.chrome_style_id, 
--   r.style -> 'attributes' ->> 'modelYear',
--   r.style -> 'division' ->> '$value',
--   r.style -> 'model' ->> '$value',
  f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
--   f.factory_option -> 'attributes' ->> 'standard' as standard,
  replace(f.factory_option -> 'description'  ->> 0, 'TRANSMISSION, ', '')
from chr.get_describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and (r.style ->'attributes'->>'modelYear')::integer > 2017
join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true  
  and f.factory_option -> 'header' ->> '$value' = 'TRANSMISSION';  


------------------------------------------------------------------------------------------------------------
--/> transmissions: GM only, from .../ext_chrome/scripts/body_types_transmissions.sql
------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------
--< chr.exterior_colors
------------------------------------------------------------------------------------------------------------
drop table if exists chr.exterior_colors cascade;
CREATE TABLE chr.exterior_colors (
  chrome_style_id citext NOT NULL,
  color_code citext NOT NULL,
  color citext NOT NULL,
  model_year integer,
  model_code citext,
  CONSTRAINT exterior_colors_pkey1 PRIMARY KEY (chrome_style_id, color_code));
COMMENT ON TABLE chr.exterior_colors
  IS 'based on chrome ADS service with the ShowAvailableEquipment switch which returns
all available equipment, including colors. table is truncated and loaded daily';  

insert into chr.exterior_colors  
select aa.*
from (
  select a.chrome_style_id,
    b.colors->'attributes'->>'colorCode' as color_code,
    b.colors->'attributes'->>'colorName' as color,
    (r.style -> 'attributes' ->> 'modelYear')::integer as model_year, 
    r.style -> 'attributes' ->> 'mfrModelCode' as model_code  
  from chr.get_describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'exteriorColor') as b(colors) on true
  join jsonb_array_elements(a.response->'style') as r(style) on true
    and r.style ->'attributes'->>'fleetOnly' = 'false'
    and (r.style ->'attributes'->>'modelYear')::integer > 2017) aa
left join (
  select chrome_style_id, color_code
  from chr.exterior_colors) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.color_code = bb.color_code
where bb.chrome_style_id is null;     


10/4/19
2020 Trax Mosaic Black Metallic is now color code PDK, our data shows only GB0 & GB8
Chrome says their data has been updated

create table jon.trax as
select * from chr.configurations where model_year = 2020 and model = 'trax'

to get the data to update, had to delete all of this:
delete from chr.get_describe_vehicle_by_style_id where chrome_style_id in (select chrome_style_id from jon.trax)
delete from chr.styles where model_id = 32328 
delete from chr.models where model_id = 32328
delete from chr.engines where chrome_style_id in (select chrome_style_id from jon.trax)
delete from chr.interiors where chrome_style_id in (select chrome_style_id from jon.trax)
delete from chr.configurations where model_year = 2020 and model = 'trax'    

10/17/2019
after running this script, rerun chrome.py
2020 Honda Pilot, exterior colors does not have BK:Crystal Black Pearl, but chrome does
create table jon.pilots as
  select * 
  from chr.configurations 
  where model_year = 2020 and model = 'pilot';
delete from chr.get_describe_vehicle_by_style_id where chrome_style_id in (select chrome_style_id from jon.pilots);
delete from chr.styles where model_id in ( 
  select distinct model_id 
  from chr.styles 
  where chrome_style_id in (
    select 
    chrome_style_id 
    from jon.pilots));
delete from chr.models where model_id in (   
  select distinct model_id 
  from chr.styles 
  where chrome_style_id in (
    select 
    chrome_style_id 
    from jon.pilots)); 
delete from chr.engines where chrome_style_id in (select chrome_style_id from jon.pilots); 
delete from chr.interiors where chrome_style_id in (select chrome_style_id from jon.pilots);
delete from chr.configurations where model_year = 2020 and model = 'pilot'; 
------------------------------------------------------------------------------------------------------------
--/> chr.exterior_colors
------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
--< chr.interiors
------------------------------------------------------------------------------------------------------------

drop table if exists chr.interiors cascade;
create table chr.interiors (
  chrome_style_id citext not null references chr.configurations(chrome_style_id),
  option_code citext not null,
  color citext not null,
  material citext not null,
  primary key (chrome_style_id,option_code));
comment on table chr.engines is 'data derived from chr.get_describe_vehicle_by_style_id, 
  material and color derived from parsing the descripion in factoryOptions. at the
  time of creation only 2 chrome style ids with null values, guessed at these.';  

insert into chr.interiors
select aa.*
from (
  select a.chrome_style_id, 
    case
      when chrome_style_id in ('400631','400629') then 'G'
      else b.interiors -> 'attributes' ->> 'oemCode'
    end as option_code, 
    case
      when chrome_style_id in ('400631','400629') then 'Black'
      else left(b.interiors -> 'description' ->> 0, position(',' in b.interiors -> 'description' ->> 0) -1) 
    end as color,
    case 
      when b.interiors -> 'description' ->> 0 like '%LEATHER%' then 'Leather'
      when b.interiors -> 'description' ->> 0 like '%LTHR%' then 'Leather'
      when b.interiors -> 'description' ->> 0 like '%ROCK CREEK%' then 'Leather'
      when a.chrome_style_id in ('400631','400629') then 'Cloth'
      else 'Cloth'
    end as material  
  from chr.get_describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'style') as aa(style) on true
    and aa.style ->'attributes'->>'fleetOnly' = 'false'
    and (aa.style ->'attributes'->>'modelYear')::integer > 2017
  left join jsonb_array_elements(a.response -> 'factoryOption') as b(interiors) on true
    and b.interiors -> 'header' ->> '$value' = 'SEAT TRIM'
  where a.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful') aa
left join (
  select chrome_style_id, option_code
  from chr.interiors) bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.option_code = bb.option_code
where bb.chrome_style_id is null;      

select * from chr.interiors

-- the missing material info in chrome
select jsonb_pretty(response) from chr.get_describe_vehicle_by_style_id where chrome_style_id in ('400631','400629')

select * from nc.vehicle_configurations where chrome_style_id in ('400631','400629')

select * from nc.vehicle_configurations where model like 'titan%'

select * from chr.configurations where model_year = 2019 and model = 'titan xd' order by chrome_style_id



-- gm only - join on gmgl.vehicle_option_codes
select a.stock_number, c.*, d.*
from nc.vehicle_acquisitions a
join gmgl.vehicle_option_codes b on a.vin = b.vin
join nc.vehicles c on a.vin = c.vin
join chr.interiors d on c.chrome_style_id = d.chrome_style_id
  and b.option_code = d.oem_code
where a.thru_date > current_date

-- nissan
select a.stock_number, a.vin, b.interior_color, e.*
from nc.vehicle_acquisitions a
join hn.nissan_vehicle_details b on a.vin = b.vin
  and b.lookup_date = (
    select max(lookup_date)
    from hn.nissan_vehicle_details
    where vin = b.vin)
join nc.vehicles c on a.vin = c.vin
join chr.interiors e on c.chrome_style_id = e.chrome_style_id
  and b.interior_color = e.color
where a.thru_date > current_date

-- missing lots of vehicles from nissan_vehicle_details
select a.stock_number, a.vin, a.ground_date, c.*
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
  and b.make = 'nissan'
left join hn.nissan_vehicle_details c on a.vin = c.vin
  and c.lookup_date = (
    select max(lookup_date)
    from hn.nissan_vehicle_details
    where vin = c.vin)  
where a.thru_date > current_date

-- nothing on honda yet
------------------------------------------------------------------------------------------------------------
--/> chr.interiors
------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
--< allocation groups
------------------------------------------------------------------------------------------------------------
-- these are all the source i know for allocation groups
create temp table sources as
select distinct model_year, alloc_group, 'orders' from gmgl.vehicle_orders 
union
select distinct 2019, alloc_Group, 'build out 2019' from nc.build_out_2019
union
select distinct 2020, alloc_group, 'start up 2020' from nc.start_up_2020
union
select distinct model_year, alloc_group, 'constraints' from nc.dealer_constraints
order by alloc_group, model_year

select * 
from chr.configurations a
left join (
  select distinct model_year, alloc_group from gmgl.vehicle_orders 
  union
  select distinct 2019, alloc_Group from nc.build_out_2019
  union
  select distinct 2020, alloc_group from nc.start_up_2020
  union
  select distinct model_year, alloc_group from nc.dealer_constraints) b on a.model_year = b.model_year
    and a.alloc_group = b.alloc_group
where a.alloc_Group <> 'n/a'
  and b.alloc_group is null;

  
select * from sources where alloc_group like 'tra%'

select *
from chr.model_years a
join chr.divisions b on a.model_year = b.model_year



------------------------------------------------------------------------------------------------------------
--/> allocation groups
------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------
--< packages
------------------------------------------------------------------------------------------------------------
drop table if exists chr.packages cascade;
create table chr.packages (
  chrome_style_id citext not null references chr.configurations(chrome_style_id),
  option_code citext not null,
  description citext not null,
  primary key(chrome_style_id,option_code));
insert into chr.packages  
select a.chrome_style_id, c.packages -> 'attributes' ->> 'oemCode',
  c.packages -> 'description'  ->> 0 
from chr.get_Describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'factoryOption') as c(packages) on true  
left join jsonb_array_elements(a.response->'style') as f(style) on true 
  and (
    c.packages -> 'header' ->> '$value' = 'ADDITIONAL EQUIPMENT'  
    or
    c.packages -> 'header' ->> '$value' = 'REQUIRED OPTION')
  and (
    c.packages -> 'description'  ->> 0 like '%PACKAGE%'
    or 
    c.packages -> 'description'  ->> 0 like '%EDITION%')
  and c.packages -> 'description'  ->> 0 not like '%SIRIUSXM%'
  and c.packages -> 'description'  ->> 0 not like '%STORAGE PACKAGE%'
  and c.packages -> 'description'  ->> 0 not like 'LPO%'
where c.packages -> 'attributes' ->> 'oemCode' is NOT null 
  and f.style -> 'attributes' ->> 'fleetOnly' = 'false';
create index on chr.packages(chrome_style_id);
create index on chr.packages(option_code);

  
------------------------------------------------------------------------------------------------------------
--< packages
------------------------------------------------------------------------------------------------------------

