﻿


-- 540


select a.year, a.inpmast_stock_number, a.inpmast_vin, a.make, a.model
-- select a.*  
from arkona.xfm_inpmast a
where  a.current_row
  and type_n_u = 'N'
  and status = 'I'




drop table if exists test;
create temp table test as
select a.year, a.inpmast_stock_number, a.inpmast_vin, a.make, a.model,
  d.bt ->> '$value' as body_type
-- select a.*  
from arkona.xfm_inpmast a
join chr.describe_vehicle b on a.inpmast_vin = b.vin
join jsonb_array_elements(b.response->'style') as c(style) on true
left join jsonb_array_elements(c.style->'bodyType') with ordinality as d(bt) on true  
  and d.bt -> 'attributes' ->> 'primary' = 'true'
where b.style_count = 1
  and a.current_row
  and type_n_u = 'N'
  and status = 'I'
order by a.make, a.model;

select inpmast_vin
from test
group by inpmast_vin
having count(*) > 1

select * 
from test
where inpmast_vin = '5FNRL6H73KB137960'

fucking multiple style counts

select jsonb_pretty(response) from chr.describe_vehicle where vin = '1G1FH3D71K0157477'

-- select * 
-- from chr.configurations

-- style_count = 1
select a.year, a.inpmast_stock_number, a.inpmast_vin, a.make, a.model,
  d.bt ->> '$value' as body_type
-- select a.*  
from arkona.xfm_inpmast a
join chr.describe_vehicle b on a.inpmast_vin = b.vin
join jsonb_array_elements(b.response->'style') as c(style) on true
left join jsonb_array_elements(c.style->'bodyType') with ordinality as d(bt) on true  
  and d.bt -> 'attributes' ->> 'primary' = 'true'
join jsonb_array_elements(b.response->'standard') as e(tran) on true  
where b.style_count = 1
  and a.current_row
  and type_n_u = 'N'
  and status = 'I'

  
union -- style_count > 1  
select a.year, a.inpmast_stock_number, a.inpmast_vin, a.make, a.model,
  d.bt ->> '$value' as body_type
-- select a.*  
from arkona.xfm_inpmast a
join chr.describe_vehicle b on a.inpmast_vin = b.vin
join jsonb_array_elements(b.response->'style') as c(style) on true
left join jsonb_array_elements(c.style->'bodyType') with ordinality as d(bt) on true  
  and d.bt -> 'attributes' ->> 'primary' = 'true'
left join gmgl.vehicle_orders dd on a.inpmast_vin = dd.vin     
where b.style_count > 1
  and a.current_row
  and a.type_n_u = 'N'
  and a.status = 'I'
  and a.model_code = (c.style ->'attributes'->>'mfrModelCode')::citext
  and c.style ->'attributes'->>'fleetOnly' = 'false' -- resolves the traverse problem
  and -- resolves 2019 Silverado 1500 LD & Escalade ESV based on trim
    case
      when (c.style ->'attributes'->>'modelYear')::integer = '2019' 
        and (c.style ->'model'->>'$value'::citext)::citext in('Silverado 1500 LD','Escalade ESV') then 
        case
          when dd.peg = '2LT' then (c.style ->'attributes'->>'id')::citext = '398577'
          when dd.peg = '1LT' then (c.style ->'attributes'->>'id')::citext = '398576'
          when dd.peg = '1SA' then (c.style ->'attributes'->>'id')::citext = '398555'
          when dd.peg = '1SB' then (c.style ->'attributes'->>'id')::citext = '398556'
          when dd.peg = '1SD' then (c.style ->'attributes'->>'id')::citext = '398558'          
          else 1 = 1
        end
      else 1 = 1
    end;  

select a.year, a.inpmast_stock_number, a.inpmast_vin, a.make, a.model,
  d.bt ->> '$value' as body_type
-- select a.*  
from arkona.xfm_inpmast a
join chr.describe_vehicle b on a.inpmast_vin = b.vin
join jsonb_array_elements(b.response->'style') as c(style) on true
left join jsonb_array_elements(c.style->'bodyType') with ordinality as d(bt) on true  
  and d.bt -> 'attributes' ->> 'primary' = 'true'
join jsonb_array_elements(b.response->'standard') as e(tran) on true  
where b.style_count = 1
  and a.current_row
  and type_n_u = 'N'
  and status = 'I'

  
-- transmission
select inpmast_vin, count(*) from (
select a.year, a.inpmast_stock_number, a.inpmast_vin, a.make, a.model,a.model_code, a.body_style,
  e.tran ->> 'description'
-- select a.*  
from arkona.xfm_inpmast a
join chr.describe_vehicle b on a.inpmast_vin = b.vin
join jsonb_array_elements(b.response->'standard') as e(tran) on true  
  and e.tran -> 'header' ->> '$value' = 'MECHANICAL'
  and e.tran ->> 'description' like '%Transmission%'
where b.style_count = 1
  and a.current_row
  and type_n_u = 'N'
  and status = 'I'    
) x group by inpmast_vin having count(*) > 1


select jsonb_pretty(response) from chr.describe_vehicle where vin = '1GCZGHFG0K1310927'

-- transmissions in the style of chr.engines  

select a.chrome_style_id, 
  r.style -> 'attributes' ->> 'modelYear',
  r.style -> 'division' ->> '$value',
  r.style -> 'model' ->> '$value',
  f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
  f.factory_option -> 'attributes' ->> 'standard' as standard,
  f.factory_option -> 'description'  ->> 0
-- select *
from chr.get_describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and (r.style ->'attributes'->>'modelYear')::integer > 2017
left join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true  
  and f.factory_option -> 'header' ->> '$value' = 'TRANSMISSION'  
-- where a.chrome_style_id = '390756'
order by r.style -> 'division' ->> '$value',r.style -> 'model' ->> '$value', a.chrome_style_id
order by a.chrome_style_id


select * 
from chr.engines

select jsonb_pretty(response) from chr.get_describe_vehicle_by_style_id where chrome_style_id = '406007'

select transmission_code, make, count(*)
from arkona.xfm_inpmast
where current_row
  and transmission_code is not null
group by transmission_code, make

drop table if exists chr.category_definitions;
create table chr.category_definitions (
  response jsonb);

select jsonb_pretty(response)
from chr.category_definitions


select aa -> 'group' ->> '$value', aa -> 'header' ->> '$value', aa -> 'category'  ->> '$value'
from chr.category_definitions a
left join jsonb_array_elements(a.response) as aa on true 


select 
  aa -> 'group' ->> '$value' as the_group, 
  aa -> 'type' ->> '$value' as the_type, 
  aa -> 'header' ->> '$value'as the_header, 
  aa -> 'category' ->> '$value' as the_category, aa -> 'category' -> 'attributes' ->> 'id' as category_id
from chr.category_definitions a
left join jsonb_array_elements(a.response) as aa on true 
-- order by aa -> 'group' ->> '$value', aa -> 'type' ->> '$value', aa -> 'header' ->> '$value'  
order by aa -> 'category' -> 'attributes' ->> 'id'
-- -- transmission types: 
--   type: Transmission - Type
--   header: Transmission
--   categroy: M/t : 1131
--             A/T : 1130


-- transmissions in the style of chr.engines  
-- this gives me nothing for honda nissan
-- gm multiple transmissions for a style_id

11/11/19
for taylors page
moved this to a table in configurations
select a.chrome_style_id, 
  r.style -> 'attributes' ->> 'modelYear',
  r.style -> 'division' ->> '$value',
  r.style -> 'model' ->> '$value',
  f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
  f.factory_option -> 'attributes' ->> 'standard' as standard,
  replace(f.factory_option -> 'description'  ->> 0, 'TRANSMISSION, ', '')
-- select *
from chr.get_describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and (r.style ->'attributes'->>'modelYear')::integer > 2017
left join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true  
  and f.factory_option -> 'header' ->> '$value' = 'TRANSMISSION'  
-- where a.chrome_style_id = '390756'
-- order by r.style -> 'division' ->> '$value',r.style -> 'model' ->> '$value', a.chrome_style_id
order by a.chrome_style_id


nissan titan xd  
select jsonb_pretty(response) from chr.get_describe_Vehicle_by_style_id where chrome_Style_id = '400637'

-- 10/24/19
-- this may do, 1 row per csi, 
select a.chrome_style_id, 
  r.style -> 'attributes' ->> 'modelYear',
  r.style -> 'division' ->> '$value',
  r.style -> 'model' ->> '$value',
--   f.standard,
--   f.standard -> 'category',
--   f.standard -> 'category' -> 1,
  f.standard -> 'category' -> 0 -> 'attributes' ->> 'id',
  f.standard -> 'category' -> 1 -> 'attributes' ->> 'id',
  f.standard -> 'category' -> 2 -> 'attributes' ->> 'id',
  f.standard -> 'category' @> '{"attributes": {"id": "1130"}}', -- all false ???
  case 
    when coalesce(f.standard -> 'category' -> 0 -> 'attributes' ->> 'id', '0') = '1130' or
         coalesce(f.standard -> 'category' -> 1 -> 'attributes' ->> 'id', '0') = '1130' or
         coalesce(f.standard -> 'category' -> 2 -> 'attributes' ->> 'id', '0') = '1130' then 'A/T'
    when coalesce(f.standard -> 'category' -> 0 -> 'attributes' ->> 'id', '0') = '1131' or
         coalesce(f.standard -> 'category' -> 1 -> 'attributes' ->> 'id', '0') = '1131' or
         coalesce(f.standard -> 'category' -> 2 -> 'attributes' ->> 'id', '0') = '1131' then 'M/T' 
  end as transmission        
-- select a.chrome_style_id
from chr.get_describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and (r.style ->'attributes'->>'modelYear')::integer > 2017
left join jsonb_array_elements(a.response->'standard') as f(standard) on true  
  and (
    f.standard -> 'category' -> 0 -> 'attributes' ->> 'id' in ('1130','1131') or
    f.standard -> 'category' -> 1 -> 'attributes' ->> 'id' in ('1130','1131') or
    f.standard -> 'category' -> 2 -> 'attributes' ->> 'id' in ('1130','1131'))
-- where chrome_Style_id = '400637'
where r.style -> 'division' ->> '$value' in ('Honda','Nissan')

select '1' = '2'
