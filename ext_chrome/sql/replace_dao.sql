﻿//////////////////////////////////////////////////////////////////////

sandbox (10.130.196.174) connection
where i still have old chrome data

try to determine BEFORE spending the dough on chrome data, 
does it give me what i need for configurations, at least comparable to
what i am using from dataone

//////////////////////////////////////////////////////////////////////
select *
from chr.models
where model_id = 28578

select *
from chr.styles
where style_id = 378248

select *
from chr.options
where style_id = 378248

select * from chr.opt_headers where header_id = 1292  -- PREFERRED EQUIPMENT GROUP

select * from chr.opt_kinds where option_kind_id = 16 -- PEG


select a.availability, a.model_year, a.full_style_code, a.style_name, a.trim_level,
  a.cf_model_name, a.cf_style_name, a.cf_body_type, aa.option_code, a.front_wd, a.rear_wd, a.four_wd
from chr.styles a
join chr.options aa on a.style_id = aa.style_id
join chr.opt_kinds b on aa.option_kind_id = b.option_kind_id
  and b.option_kind = 'PEG'
join chr.opt_headers c on aa.header_id = c.header_id
  and c.opt_header = 'PREFERRED EQUIPMENT GROUP'
where a.model_year = 2017
  and a.availability not in ('F','P') -- excl fleet only and police only
and full_style_code = 'TK25753'

-- colors
select *
from chr.styles a
where a.model_year = 2017
  and a.availability not in ('F','P') -- excl fleet only and police only

select *
from chr.manufacturers

-- drop schema if exists jon_chr;
create schema jon_chr;
drop table if exists jon_chr.makes_models cascade;
create table jon_chr.makes_models (
  make citext not null,
  model citext not null,
  primary key(make,model));
insert into jon_chr.makes_models  
select distinct a.division_name, b.model_name
from chr.divisions a
join chr.models b on a.division_id = b.division_id
  and b.model_year > 2015
where a.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')  ;

drop table if exists jon_chr.year_make_models cascade;
create table jon_chr.year_make_models (
  model_year integer not null,
  make citext not null,
  model citext not null,
  foreign key (make, model) references jon_chr.makes_models(make,model),
  primary key (model_year,make,model));
insert into jon_chr.year_make_models
select distinct b.model_year, a.division_name, b.model_name
from chr.divisions a
join chr.models b on a.division_id = b.division_id
  and b.model_year > 2015
where a.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')  ;  

drop table if exists jon_chr.exterior_colors cascade;
create table jon_chr.exterior_colors(
  model_year integer not null,
  make citext not null,
  model citext not null,
  color_code citext not null,
  color citext not null,
  foreign key (model_year,make,model) references jon_chr.year_make_models(model_year,make,model),
  primary key (model_year,make,model,color_code,color));
insert into jon_chr.exterior_colors  
select distinct c.model_year, d.division_name, c.model_name, b.ext1_man_code, ext1_desc
from chr.styles a
join chr.colors b on a.style_id = b.style_id
join chr.models c on a.model_id = c.model_id
  and c.model_year > 2015
--   and c.model_name = 'traverse'
join chr.divisions d on c.division_id = d.division_id  
where d.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')
  and a.availability not in ('F','P');


drop table if exists jon_chr.model_codes cascade;
create table jon_chr.model_codes(
  model_year integer not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  foreign key (model_year,make,model) references jon_chr.year_make_models(model_year,make,model),
  primary key (model_year,make,model,model_code));
insert into jon_chr.model_codes  
select distinct c.model_year, d.division_name, c.model_name, a.full_style_code
from chr.styles a
join chr.models c on a.model_id = c.model_id
  and c.model_year > 2015
join chr.divisions d on c.division_id = d.division_id  
where d.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')
  and a.availability not in ('F','P');

-- peg_to_trim
drop table if exists peg_to_trim;
create temp table peg_to_trim as
select b.model_year, c.division_name, b.model_name,
  a.full_style_code, a.style_name, a.trim_level,
--   a.cf_style_name, a.cf_body_type, 
  aa.option_code as peg
from chr.styles a
join chr.models b on a.model_id = b.model_id
  and b.model_year > 2015
join chr.divisions c on b.division_id = c.division_id  
join chr.options aa on a.style_id = aa.style_id
join chr.opt_kinds bb on aa.option_kind_id = bb.option_kind_id
  and bb.option_kind = 'PEG'
join chr.opt_headers cc on aa.header_id = cc.header_id
  and cc.opt_header = 'PREFERRED EQUIPMENT GROUP'
where c.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')
  and a.availability not in ('F','P')
order by b.model_year, c.division_name, b.model_name,
  a.model_year, a.trim_level, aa.option_code

select b.model_year, c.division_name, b.model_name, a.trim_level, aa.option_code as peg
from chr.styles a
join chr.models b on a.model_id = b.model_id
  and b.model_year > 2015
join chr.divisions c on b.division_id = c.division_id  
join chr.options aa on a.style_id = aa.style_id
join chr.opt_kinds bb on aa.option_kind_id = bb.option_kind_id
  and bb.option_kind = 'PEG'
join chr.opt_headers cc on aa.header_id = cc.header_id
  and cc.opt_header = 'PREFERRED EQUIPMENT GROUP'
where c.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')
  and a.availability not in ('F','P')
group by b.model_year, c.division_name, b.model_name, a.trim_level, aa.option_code 
having count(*) > 1

-- for this enclave, there are 2 model codes, the difference being FWD vs AWD, but the peg & trim are the same
-- nor is peg tied to a single trim
select b.model_year, c.division_name, b.model_name,
  a.availability, a.model_year, a.full_style_code, a.style_code,
  a.style_name, a.trim_level,
  a.cf_style_name, a.cf_body_type, aa.option_code as peg
from chr.styles a
join chr.models b on a.model_id = b.model_id
  and b.model_year > 2015
join chr.divisions c on b.division_id = c.division_id  
join chr.options aa on a.style_id = aa.style_id
join chr.opt_kinds bb on aa.option_kind_id = bb.option_kind_id
  and bb.option_kind = 'PEG'
join chr.opt_headers cc on aa.header_id = cc.header_id
  and cc.opt_header = 'PREFERRED EQUIPMENT GROUP'
where c.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')
  and a.availability not in ('F','P')
  and b.model_year = 2016
  and c.division_name = 'buick'
  and b.model_name = 'enclave'
  and a.trim_level = 'leather'
  and aa.option_code = '1sl'

-- no dups
select model_year, division_name, model_name, full_style_code, peg 
from peg_to_trim
group by model_year, division_name, model_name, full_style_code, peg 
having count(*) <> 1

-- has dups
select model_year, division_name, model_name, full_style_code, trim_level
from peg_to_trim
group by model_year, division_name, model_name, full_style_code, trim_level 
having count(*) <> 1

-------------------------------------------------------------------
-- engine option codes
-------------------------------------------------------------------

select *
from chr.models
where model_id = 28578

select *
from chr.styles
where style_id = 378248

select *
from chr.options
where style_id = 378248

select * from chr.opt_headers where header_id = 1160  -- ENGINE

select * from chr.opt_kinds where option_kind_id = 6 -- ENGINE  


select a.availability, a.model_year, a.full_style_code, a.style_name, a.trim_level,
  a.cf_model_name, a.cf_style_name, a.cf_body_type, aa.option_code, a.front_wd, a.rear_wd, a.four_wd
from chr.styles a
join chr.options aa on a.style_id = aa.style_id
join chr.opt_kinds b on aa.option_kind_id = b.option_kind_id
  and b.option_kind = 'ENGINE'
join chr.opt_headers c on aa.header_id = c.header_id
  and c.opt_header = 'ENGINE'
where a.model_year = 2017
  and a.availability not in ('F','P') -- excl fleet only and police only
limit 1000


select * from chr.options where option_code = 'L96'  limit 100

select * from chr.vin_pattern limit 100

-- 3/23/19
what is in my head is some notion of vehicle configurations

-- lets try vinmatch

select a.chrome_Style_id, a.model_year, a.division_name, a.model_name, a.style_name, 
  a.trim_name, a.mfr_style_code, available_in_nvd,
  c.vin_model_name, c.vin_style_name, c.engine_size
-- select *  
from chr.year_make_model_style a
join chr.vin_pattern_Style_mapping b on a.chrome_style_id = b.chrome_style_id
join chr.vin_pattern c on b.vin_pattern_id = c.vin_pattern_id
where a.model_year > 2015
  and a.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')
  and a.fleet_only = 'N'
  and a.chrome_Style_id = 382956

-- hmm style_name looks interesting
-- cab is not broken out, neither is box: cab and box are in styles.cf_drive_train, cf_body_type

first issue is 2017 acadia, chrome_Style_id = 382956, in the above query, this style_id returns 2 rows
the only difference is vin pattern and gvwr_range
but that styleid returns a single row from styles
select *
from chr.styles
where style_id = 382956


select a.chrome_Style_id, a.model_year, a.division_name, a.model_name, a.style_name, 
  a.trim_name, a.mfr_style_code, available_in_nvd,
  c.vin_model_name, c.vin_style_name, c.engine_size, count(*)
-- select *  
from chr.year_make_model_style a
join chr.vin_pattern_Style_mapping b on a.chrome_style_id = b.chrome_style_id
join chr.vin_pattern c on b.vin_pattern_id = c.vin_pattern_id
where a.model_year > 2015
  and a.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')
  and a.fleet_only = 'N'
group by a.chrome_Style_id, a.model_year, a.division_name, a.model_name, a.style_name, 
  a.trim_name, a.mfr_style_code, available_in_nvd,
  c.vin_model_name, c.vin_style_name, c.engine_size
having count(*) > 1  
order by a.chrome_Style_id


rogue has 9 rows per style_id
only difference is vin_pattern
select *  
from chr.year_make_model_style a
join chr.vin_pattern_Style_mapping b on a.chrome_style_id = b.chrome_style_id
join chr.vin_pattern c on b.vin_pattern_id = c.vin_pattern_id
where a.model_year > 2015
  and a.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')
  and a.fleet_only = 'N'
  and a.chrome_Style_id = 376927

need vin_pattern table for engine size (maybe, if i am not using nc data)  

so maybe something like
drop table if exists engines;
create temp table engines as
select a.chrome_style_id, b.engine_size
from chr.vin_pattern_style_mapping a 
join chr.vin_pattern b on a.vin_pattern_id = b.vin_pattern_id
group by a.chrome_style_id, b.engine_size


select *  
from chr.year_make_model_style a
join engines b on a.chrome_style_id = b.chrome_style_id
where a.model_year > 2015
  and a.division_name in ('chevrolet','gmc','cadillac','buick','cadillac', 'honda', 'nissan')
  and a.fleet_only = 'N'
--   and a.chrome_Style_id = 382956
  and model_name = 'acadia'
order by a.chrome_style_id


-- this is feeling hinky  

-- 4/25/19 call with chrome today, will we be getting data? will data do what i need it to do?
--unique
select style_id
from chr.styles
group by style_id
having count(*) > 1

select * from chr.models limit 1000

select model_name, array_agg(model_year order by model_year)
from chr.models
group by model_name
order by model_name
