﻿create TABLE chr.ci_types(
    type_id integer primary key,
    ci_type citext)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.ci_types.ci_type IS 'orig: type';

create TABLE chr.norm_ci_labels(
    label_id integer primary key,
    norm_ci_label citext,
    type_id integer not null references chr.ci_types,
    label_sequence integer not null)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.norm_ci_labels.norm_ci_label IS 'orig: label';

create TABLE chr.mkt_class(
    mkt_class_id integer primary key,
    market_class citext not null)
WITH (OIDS=FALSE);

create TABLE chr.opt_kinds(
    option_kind_id integer primary key,
    option_kind citext)
WITH (OIDS=FALSE);

create TABLE chr.tech_title_header(
    tech_title_header_id integer primary key,
    tech_title_header_text citext NOT NULL,
    tech_title_header_sequence integer not null)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.tech_title_header.tech_title_header_sequence IS 'orig: sequence';

create TABLE chr.opt_headers(
    header_id integer primary key,
    opt_header citext)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.opt_headers.opt_header IS 'orig: header';

create TABLE chr.std_headers(
    header_id integer primary key,
    std_header citext)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.std_headers.std_header IS 'orig: header';

create TABLE chr.category_headers(
    category_header_id integer primary key,
    category_header citext,
    category_header_sequence integer not null)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.category_headers.category_header_sequence IS 'orig: sequence';

create TABLE chr.Manufacturers(
    manufacturer_id integer primary key,
    manufacturer_name citext)
WITH (OIDS=FALSE);

create TABLE chr.tech_titles(
    title_id integer primary key,
    tech_title_sequence integer not null,
    title citext NOT NULL,
    tech_title_header_id integer not null references chr.tech_title_header)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.tech_titles.tech_title_sequence IS 'orig: sequence';

create TABLE chr.divisions(
    division_id integer primary key,
    manufacturer_id integer not null references chr.manufacturers,
    division_name citext)
WITH (OIDS=FALSE);

create TABLE chr.categories(
    category_id integer primary key,
    category citext,
    category_type_filter citext,
    category_header_id integer references chr.category_headers,
    user_friendly_name citext)
WITH (OIDS=FALSE);

create TABLE chr.subdivisions(
    model_year integer not null,
    division_id integer not null references chr.divisions,
    subdivision_id integer primary key,
    hist_subdivision_id integer,
    subdivision_name citext)
WITH (OIDS=FALSE);

create TABLE chr.models(
    model_id integer primary key,
    hist_model_id integer,
    model_year integer not null,
    division_id integer not null references chr.divisions,
    subdivision_id integer not null references chr.subdivisions,
    model_name citext,
    effective_date date,
    model_comment citext,
    availability citext not null)
WITH (OIDS=FALSE);

create TABLE chr.Styles(
    style_id integer primary key,
    hist_style_id integer,
    model_id integer not null references chr.models,
    model_year integer,
    style_sequence integer not null,
    style_code citext not null,
    full_style_code citext,
    style_name citext,
    tru_base_price citext not null,
    invoice numeric (12,2),
    msrp numeric (12,2),
    destination numeric (12,2),
    style_cvc_list citext,
    h integer not null references chr.mkt_class,
    style_name_wo_trim citext,
    trim_level citext,
    passenger_capacity integer,
    passenger_doors integer,
    manual_trans citext,
    auto_trans citext,
    front_wd citext,
    rear_wd citext,
    all_wd citext,
    four_wd citext,
    step_side citext,
    caption citext,
    availability citext not null,
    price_State citext not null,
    auto_builder_style_id citext not null,
    cf_model_name citext,
    cf_style_name citext,
    cf_drive_train citext,
    cf_body_type citext)
 WITH (OIDS=FALSE);
 COMMENT ON COLUMN chr.styles.style_sequence IS 'orig: sequence';
 COMMENT ON COLUMN chr.styles.trim_level IS 'orig: trim';

create TABLE chr.jpgs(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    jpg_name citext NOT NULL,
    subdivision_name citext not null,
    model_name citext not null,
    style_name citext not null,
    model_year integer not null)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.jpgs.model_year IS 'orig: year';

 create TABLE chr.Options(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    header_id integer not null references chr.opt_headers,
    option_sequence integer not null,
    opction_code citext not null,
    option_desc citext,
    option_kind_id integer not null references chr.opt_kinds,
    category_list citext,
    availability citext not null,
    pon citext,
    ext_description citext,
    supported_logic citext,
    unsupported_logic citext,
    price_notes citext)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.options.option_sequence IS 'orig: sequence';

create TABLE chr.prices(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    price_sequence integer not null,
    option_code citext not null,
    price_rule_desc citext,
    condition citext,
    invoice numeric (12,2),
    msrp numeric (12,2),
    price_state citext not null)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.prices.price_sequence IS 'orig: sequence';

create TABLE chr.norm_cons_info(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    label_id integer not null references chr.norm_ci_labels,
    norm_cons_info_value citext,
    qualifier citext,
    qualifier_sequence integer,
    norm_cons_info_group integer not null)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.norm_cons_info.norm_cons_info_value IS 'orig: value';
COMMENT ON COLUMN chr.norm_cons_info.norm_cons_info_group IS 'orig: group';

CREATE TABLE chr.cons_info(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    type_id integer not null references chr.ci_types,
    cons_info_text citext)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.cons_info.cons_info_text IS 'orig: text';

create TABLE chr.Colors(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    ext1_code citext not null,
    ext_code citext,
    int_code citext,
    ext1_man_code citext,
    ext2_man_code citext,
    int_man_code citext,
    order_code citext,
    as_two_tone citext not null,
    ext1_Desc citext,
    ext2_desc citext,
    int_Desc citext,
    condition citext,
    generic_ext_color citext,
    generic_ext2_color citext,
    ext1_rgb_hex citext,
    ext2_rgb_hex citext,
    ext1_mfr_full_code citext,
    ext2_mfr_full_code citext)
WITH (OIDS=FALSE);

create TABLE chr.body_styles(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    body_style citext,
    is_primary citext not null)
WITH (OIDS=FALSE);

 create TABLE chr.standards(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    header_id integer not null references chr.std_headers,
    standards_sequence integer NOT NULL,
    Standard citext,
    CategoryList citext) 
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.standards.standards_sequence IS 'orig: sequence';

create TABLE chr.style_cats(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    category_id integer not null references chr.categories,
    feature_type citext not null,
    style_cats_sequence integer not null,
    state citext)
WITH (OIDS=FALSE); 
COMMENT ON COLUMN chr.style_cats.style_cats_sequence IS 'orig: sequence';
    
create TABLE chr.tech_specs(
    style_id integer not null references chr.styles on DELETE CASCADE ON UPDATE CASCADE,
    title_id integer not null references chr.tech_titles,
    tech_specs_sequence integer not null,
    tech_specs_text citext,
    condition citext)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.tech_specs.tech_specs_sequence IS 'orig: sequence';
COMMENT ON COLUMN chr.tech_specs.tech_specs_text IS 'orig: text';

create TABLE chr.version( 
    file_name citext primary key,
    product citext NOT NULL,
    data_version timestamptz not null,
    data_release_id integer NOT NULL,
    schema_name citext not null,
    schema_version citext not null,
    country citext NOT NULL,
    version_language citext not null)
WITH (OIDS=FALSE);
COMMENT ON COLUMN chr.version.version_language IS 'orig: language';

delete from chr.version;
insert into chr.version(file_name,product,data_version,data_release_id,
  schema_name,schema_version,country,version_language)
values 
('nvd_v3_1997.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_1998.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_1999.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2000.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2001.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2002.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2003.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2004.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2005.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2006.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2007.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2008.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2009.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2010.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2011.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2012.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2013.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2014.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2015.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2016.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('nvd_v3_2017.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a'),
('vindata.zip','a','1914-10-23 10:36:06.469858-05'::timestamptz,0,'a','a','a','a');

alter table chr.version
add column last_updated date;

update chr.version
set last_updated = '10/20/2015';


-- in an attempt to speed up the etl
CREATE INDEX model_year_idx ON chr.styles(model_year);
-- what i really needed was indexes on foreign keys
CREATE INDEX norm_ci_labels_type_id_idx ON chr.norm_ci_labels(type_id);
CREATE INDEX tech_title_header_id_idx ON chr.tech_titles(tech_title_header_id);
CREATE INDEX manufacturer_id_idx ON chr.divisions(manufacturer_id);
CREATE INDEX category_header_id_idx ON chr.categories(category_header_id);
CREATE INDEX division_id_idx ON chr.subdivisions(division_id);
CREATE INDEX models_division_id_idx ON chr.models(division_id);
CREATE INDEX models_subdivision_id_idx ON chr.models(subdivision_id);
CREATE INDEX styles_model_id_idx ON chr.styles(model_id);
CREATE INDEX styles_mkt_class_id_year_idx ON chr.styles(mkt_class_id);
CREATE INDEX jpgs_style_id_idx ON chr.jpgs(style_id);
CREATE INDEX options_style_id_idx ON chr.options(style_id);
CREATE INDEX options_header_idx ON chr.options(header_id);
CREATE INDEX options_option_kind_id_idx ON chr.options(option_kind_id);
CREATE INDEX prices_style_id_idx ON chr.prices(style_id);
CREATE INDEX norm_cons_info_style_id_idx ON chr.norm_cons_info(style_id);
CREATE INDEX norm_cons_info_label_id_idx ON chr.norm_cons_info(label_id);
CREATE INDEX cons_info_style_id_idx ON chr.cons_info(style_id);
CREATE INDEX cons_info_type_id_idx ON chr.cons_info(type_id);
CREATE INDEX colors_style_id_idx ON chr.colors(style_id);
CREATE INDEX body_styles_style_id_idx ON chr.body_styles(style_id);
CREATE INDEX standards_style_id_idx ON chr.standards(style_id);
CREATE INDEX standards_header_id_idx ON chr.standards(header_id);
CREATE INDEX style_cats_style_id_idx ON chr.style_cats(style_id);
CREATE INDEX style_cats_category_id_idx ON chr.style_cats(category_id);
CREATE INDEX tech_specs_style_id_idx ON chr.tech_specs(style_id);
CREATE INDEX tech_specs_title_id_idx ON chr.tech_specs(title_id);
