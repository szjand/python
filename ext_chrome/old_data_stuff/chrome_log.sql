﻿create Table chr.chrome_log (
  log_id uuid,
  the_date date,
  the_time time,
  file_name citext,
  table_name citext,
  action_taken citext,
  load_start_ts time,
  load_end_ts time);


select current_time

select *
from chr.maint_nvd

select *
from chr.tmp_ci_types a
inner join chr.ci_types b on a.type_id = b.type_id

select *
from chr.tmp_models a
inner join chr.models b on a.model_id = b.model_id

select *
from chr.tmp_styles a
inner join chr.styles b on a.style_id = b.style_id


--insert into chr.models
select *
from chr.tmp_models a
where not exists (
  select 1
  from chr.models
  where model_id = a.model_id)

select *
from chr.body_Styles
where style_id in (
  select style_id
  from chr.body_styles 
  group by style_id 
  having count(*) >  1)

select *
from chr.options a
inner join (
 select header_id, style_id, option_kind_id
 from chr.options 
 group by header_id, style_id, option_kind_id
 having count(*) > 1) b on a.header_id = b.header_id
   and a.style_id = b.style_id
   and a.option_kind_id = b.option_kind_id

select * from chr.mkt_class   

select *
from chr.year_make_model_style a
where model_id in (
  select model_id
  from chr.year_make_model_style
  group by model_id
  having count(*) > 1)

select count(*) from chr.styles;

select count(*) from chr.tmp_styles;

select count(*)
from (
select *
from chr.styles
union
select *
from chr.tmp_styles) x

select count(*)
from (
  select *
  from chr.styles
  union
  select *
  from chr.tmp_styles) x
where not exists (
  select 1
  from chr.styles
  where style_id = x.style_id)  


select *
from chr.tmp_styles a
where not exists (
  select 1
  from chr.models
  where model_id = a.model_id)

  select ext1_desc, count(*) from chr.colors group by ext1_desc having count(*) > 1

select * from chr.maint_nvd  

select style_id, header_id
from chr.standards
group by style_id, header_id
having count(*) > 1

select * 
from chr.standards a
inner join (
  select style_id, header_id
  from chr.standards
  group by style_id, header_id
  having count(*) > 1) b on a.style_id = b.style_id and a.header_id = b.header_id

select *
from chr.year_make_model_style
limit 1000

insert into chr.ci_types (type_id, ci_type)
select type_id, ci_type
from chr.tmp_ci_types a
where not exists (
  select 1
  from chr.ci_types
  where type_id = a.type_id)

-- which is all fucking good and everything, but as i saw in the 10/24 vs 10/20
-- 2016 file, the fucking sequence changes! 
-- so sequence can not be part of the primary key
-- there is no fucking primary key
-- same thing for style_Cats and standards and options

--  so, delete where style_id is for the year in question?

select style_id, title_id, tech_specs_sequence
from chr.tech_specs
group by style_id, title_id, tech_specs_sequence
having count(*) > 1

select *
from chr.tech_specs a
inner join (
  select style_id, title_id
  from chr.tech_specs
  group by style_id, title_id
  having count(*) > 1) b on a.style_id = b.style_id and a.title_id = b.title_id


select *
from chr.norm_ci_labels  

select type_id, label_id
from chr.norm_ci_labels  
group by type_id, label_id having count(*) > 1



select log_id, the_date, file_name, min(load_start_ts), max(load_end_ts),
  max(load_end_ts) - min(load_start_ts)
-- select *
from chr.chrome_log
--where file_name = 'nvd_v3_2015.zip'
  WHERE load_start_ts is not null 
group by log_id, the_date, file_name
order by file_name, the_Date, min(load_start_ts)

select * from chr.version

select *
from chr.chrome_log