﻿/*
-- drop table chr.maint_vindata;
create table if not exists chr.maint_vindata(
    table_name citext unique not null,
    file_name citext unique not null,
    load_sequence integer unique not null,
    truncate_sequence integer unique not null)
with (OIDS=FALSE);   
*/

-- DROP TABLE chr.maint_vindata;

CREATE TABLE chr.maint_vindata
(
  table_name citext NOT NULL,
  file_name citext NOT NULL,
  load_sequence integer NOT NULL,
  truncate_sequence integer NOT NULL,
  CONSTRAINT maint_vindata_file_name_key UNIQUE (file_name),
  CONSTRAINT maint_vindata_load_sequence_key UNIQUE (load_sequence),
  CONSTRAINT maint_vindata_table_name_key UNIQUE (table_name),
  CONSTRAINT maint_vindata_truncate_sequence_key UNIQUE (truncate_sequence)
);

insert into chr.maint_vindata(table_name,file_name,load_sequence,truncate_sequence)
values
('year_make_model_style','YearMakeModelStyle.txt',1,7),
('category','Category.txt',2,6),
('vin_pattern','VINPattern.txt',3,5),
('style_wheel_base','StyleWheelBase.txt',4,4),
('vin_pattern_style_mapping','VINPatternStyleMapping.txt',5,3),
('style_generic_equipment','StyleGenericEquipment.txt',6,2),
('vin_equipment','VINEquipment.txt',7,1);

