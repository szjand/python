import db_cnx
import requests
"""
5/20/18: this worked loaded 2536 vins & describe-vehicle responses into chr.describe_vehicle
"""
# with db_cnx.pg() as pg_con:
#     with pg_con.cursor() as pg_cur:
#         # sql = """select vin from jon.vins;"""
#         # sql = "select '2GNAXUEV8K6191387'"
#         sql = """
#             select vin
#             from nc.stg_availability a
#             where not exists (
#                 select 1
#                 from chr.describe_vehicle
#                 where vin = a.vin);
#         """
#         # # equinox
#         # sql = """
#         #     select inpmast_vin
#         #     from arkona.xfm_inpmast a
#         #     where a.current_row
#         #       and a.status = 'I'
#         #       and a.model = 'equinox'
#         #       and a.type_n_u = 'n'
#         #       and not exists (
#         #         select 1
#         #         from chr.describe_vehicle
#         #         where vin = a.inpmast_vin)
#         # """
#         # # honda/nissan
#         # sql = """
#         #     select distinct a.vin
#         #     from sls.deals_by_month a
#         #     where a.year_month > 201712
#         #       and a.store_code = 'ry2'
#         #       and a.vehicle_type = 'new'
#         # """
#         # new inventory
#         # sql = """
#         #     select inpmast_vin
#         #     from arkona.xfm_inpmast a
#         #     where a.current_row
#         #       and a.status = 'I'
#         #       and a.type_n_u = 'N'
#         #       and not exists (
#         #         select 1
#         #         from chr.describe_vehicle
#         #         where vin = a.inpmast_vin)
#         # """
#         pg_cur.execute(sql)
#         for row in pg_cur.fetchall():
# vin = row[0]

# url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle/3GCUKREC8JG470793'
# data = requests.get(url)
# print(data)
# resp = data.text
# resp = resp.replace("'", "''")
# print(resp)
            # with pg_con.cursor() as chr_cur:
            #     sql = """
            #         insert into chr.describe_vehicle(vin,response)
            #         values ('{0}','{1}');
            #     """.format(vin, resp)
            #     chr_cur.execute(sql)
        # with pg_con.cursor() as chr_cur:
        #     sql = """
        #         update chr.describe_vehicle y
        #         set style_count = x.style_count
        #         from (
        #           select vin, count(*) as style_count
        #           from (
        #             select vin, jsonb_array_elements(response->'style')
        #             from chr.describe_vehicle
        #             where style_count is null) a
        #           group by vin) x
        #         where x.vin = y.vin;
        #     """
        #     chr_cur.execute(sql)



url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-show-available/410744'
data = requests.get(url)
print(data)
resp = data.text
resp = resp.replace("'", "''")
print(resp)