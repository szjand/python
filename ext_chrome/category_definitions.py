import requests
import db_cnx
"""
"""

url = 'https://beta.rydellvision.com:8888/chrome/category-definitions'
data = requests.get(url)
# resp = data.text
# resp = resp.replace("'", "''")
# print(resp)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as table_cur:
        sql = """
            insert into chr.category_definitions(response)
            values ('{0}');
        """.format( data.text)
        table_cur.execute(sql)
