# encoding=utf-8
"""
this will become the luigi module
in the describe vehicle section received the following error:
      File "/mnt/hgfs/E/python projects/ext_chrome/chrome.py", line 139, in <module>
            response = response.replace("'", "''")
        TypeError: a bytes-like object is required, not 'str'
changed the erroring line with:
    response = response.decode('unicode-escape').replace("'", "''")
5/28 one i got the script to work again, the above threw random psycopg2 invalid json errors
     so back to simple, without the encode/decode, and, today at least, it worked
"""
import db_cnx
import requests

# model-years
url = 'https://beta.rydellvision.com:8888/chrome/model-years'
data = requests.get(url)
# insert the raw data into the get_ table
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as chr_cur:
        chr_cur.execute("truncate chr.get_model_years")
        sql = """
            insert into chr.get_model_years(model_years)
            values ('{}');
        """.format(data.text)
        chr_cur.execute(sql)
        chr_cur.execute("select chr.xfm_model_years()")

# divisions
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as model_year_cur:
        sql = """
            select model_year
            from chr.model_years
            where model_year > 2017;
        """
        model_year_cur.execute(sql)
        for model_year in model_year_cur:
            url = 'https://beta.rydellvision.com:8888/chrome/divisions/' + str(model_year[0])
            data = requests.get(url)
            with pg_con.cursor() as chr_cur:
                chr_cur.execute("truncate chr.get_divisions")
                sql = """
                    insert into chr.get_divisions(model_year,divisions)
                    values ({0},'{1}');
                """.format(model_year[0], data.text)
                chr_cur.execute(sql)
                chr_cur.execute("select chr.xfm_divisions()")

# # subdivisions
# with db_cnx.pg() as pg_con:
#     with pg_con.cursor() as model_year_cur:
#         sql = """
#             select model_year
#             from chr.model_years
#             where model_year > 2017;
#         """
#         model_year_cur.execute(sql)
#         for model_year in model_year_cur:
#             url = 'https://beta.rydellvision.com:8888/chrome/subdivisions/' + str(model_year[0])
#             data = requests.get(url)
#             with pg_con.cursor() as chr_cur:
#                 chr_cur.execute("truncate chr.get_subdivisions")
#                 sql = """
#                     insert into chr.get_subdivisions(model_year,subdivisions)
#                     values ({0},'{1}');
#                 """.format(model_year[0],data.text)
#                 chr_cur.execute(sql)
#                 chr_cur.execute("select chr.xfm_subdivisions()")

# models by division
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as get_models_cur:
        get_models_cur.execute("truncate chr.get_models_by_division")
    with pg_con.cursor() as model_year_cur:
        model_year_sql = """
            select *
            from chr.model_years
            where model_year > 2017;
        """
        model_year_cur.execute(model_year_sql)
        for model_year in model_year_cur:
            with pg_con.cursor() as division_cur:
                division_sql = """
                    select division_id
                    from chr.divisions
                    where division in('chevrolet','buick','gmc','cadillac','honda','nissan')
                        and model_year = """ + str(model_year[0])
                division_cur.execute(division_sql)
                for division in division_cur:
                    payload = {"year": model_year[0], "divisionId": division[0]}
                    url = 'https://beta.rydellvision.com:8888/chrome/models'
                    json_data = requests.post(url, data=payload)
                    with pg_con.cursor() as table_cur:
                        sql = """
                            insert into chr.get_models_by_division(model_year,division_id,models)
                            values ({0},{1},'{2}');
                        """.format(model_year[0], division[0], json_data.text)
                        table_cur.execute(sql)
    with pg_con.cursor() as models_cur:
        models_cur.execute("select chr.xfm_models()")

# styles
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as get_styles_cur:
        get_styles_cur.execute("truncate chr.get_styles")
    with pg_con.cursor() as model_cur:
        model_sql = """
            select model_id
            from chr.models a
            where not exists (
              select 1
              from chr.styles
              where model_id = a.model_id);
        """
        model_cur.execute(model_sql)
        for model_id in model_cur:
            url = 'https://beta.rydellvision.com:8888/chrome/styles/' + str(model_id[0])
            data = requests.get(url)
            with pg_con.cursor() as table_cur:
                sql = """
                    insert into chr.get_styles(model_id,styles)
                    values ({0},'{1}');
                """.format(model_id[0], data.text)
                table_cur.execute(sql)
    with pg_con.cursor() as styles_cur:
        styles_cur.execute("select chr.xfm_styles()")


# describe vehicle by style id with ShowAvailableEquipment
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as style_cur:
        style_sql = """
            select chrome_style_id
            from chr.styles a
            where not exists (
              select 1
              from chr.get_describe_vehicle_by_style_id
              where chrome_style_id = a.chrome_style_id);
        """
        style_cur.execute(style_sql)
        for stlye_id in style_cur:
            url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-show-available/' + str(stlye_id[0])
            # response = requests.get(url).text
            # response = response.encode("utf-8")
            # response = response.decode('unicode-escape').replace("'", "''")
            response = requests.get(url)
            response = response.text.replace("'", "''")
            with pg_con.cursor() as table_cur:
                sql = """
                    insert into chr.get_describe_vehicle_by_style_id(chrome_style_id,response)
                    values ('{0}','{1}');
                """.format(stlye_id[0], response)
                table_cur.execute(sql)
    with pg_con.cursor() as cleanup_cur:
        cleanup_cur.execute("select chr.remove_unsuccessful_requests()")

# VersionInformation
url = 'https://beta.rydellvision.com:8888/chrome/version-info'
data = requests.get(url)
data = data.text.replace("'", "''")  # .encode("utf-8")
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as table_cur:
        sql = """
            insert into chr.get_version_info(response)
            values ('{}');
        """.format(data)
        table_cur.execute(sql)
