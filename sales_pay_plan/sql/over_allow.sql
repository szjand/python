﻿-- from accounting
select a.amount, a.control, b.the_Date, c.account, c.account_type, d.journal_code, e.description
from fin.fact_gl a
inner join dds.dim_Date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and control = '32178a'
order by a.amount  

-- 1/4/18 per jeri, no way to get it from accounting

select trade_allowance, trade_acv
from arkona.ext_bopmast_tmp
limit 100


select max(date_approved)
from arkona.ext_bopmast_tmp
limit 100

so i need the as yet extracted values from bopmast, 

1 time extraction, ext_bopmast_overallow
add attributes to sls.ext_bopmast_partial: trade_allowance, trade_acv, over_allow
update from ext_bopmast_overallow
alter car_deals.py ExtDeals to include those fields

create table sls.ext_bopmast_overallow (
  bopmast_company_number citext,
  record_key integer,
  trade_allowance numeric(9,2),
  trade_acv numeric(9,2));

alter table sls.ext_bopmast_partial
add column trade_allowance numeric(9,2),
add column trade_acv numeric(9,2),
add column over_allow numeric(9,2);

update sls.ext_bopmast_partial a
set trade_allowance = coalesce(b.trade_allowance, 0),
    trade_acv = coalesce(b.trade_acv, 0)
from sls.ext_bopmast_overallow b 
where a.store = b.bopmast_company_number
    and a.bopmast_id = b.record_key;

update sls.ext_bopmast_partial
set trade_allowance = coalesce(trade_allowance, 0),
    trade_acv = coalesce(trade_acv, 0);

update sls.ext_bopmast_partial
set over_allow = trade_allowance - trade_acv;    
    
select * from sls.ext_bopmast_partial where over_allow <> 0 and extract(year from delivery_date) = 2017 order by over_allow desc

modified car_deals.py to include overallow in the generation of sls.ext_bopmast_partial
think i will just leave it there for now.
