﻿-- include make, buyer, co-buyer year_month
-- exclude buyer_bopname_id
-- include but don't display bopmast_id, seq
/*
5/23 thinking this table serves 2 purposes: 
  1. display plain english list of deal data
  2. calculate payroll
        so, thinking for 2, need to add employee_number of primary, seconday, fi_mgr for joining to sls.folks
        rather than joining on last names only ,names are only cool for display
     add psc_first_name, psc_last_name, psc_employee_number..., ssc, fi
*/

select b.*
from (
  select store_code, bopmast_id, stock_number, year_month, sum(unit_count) as unit_count
  from sls.deals
--   where year_month = 201704
  group by store_code, bopmast_id, stock_number, year_month
  having sum(unit_count) <> 0) a
left join sls.deals b on a.store_code = b.store_code and a.bopmast_id = b.bopmast_id and a.year_month = b.year_month
  and b.seq = (
    select max(seq)
    from sls.deals
    where store_code = b.store_code and bopmast_id = b.bopmast_id)  
order by a.unit_count

drop table if exists sls.deals_by_month;
create table sls.deals_by_month (
  year_month integer not null, 
  store_code citext not null,  
  bopmast_id integer not null, 
  seq integer not null, 
  unit_count integer not null, 
  sale_type citext not null, 
  vehicle_type citext not null, 
  sale_group citext not null,
  stock_number citext not null, 
  vin citext not null, 
  odometer_at_sale integer not null, 
  model_year integer not null, 
  make citext not null, 
  model citext not null, 
  buyer citext not null, 
  cobuyer citext not null, 
--   primary_sc citext not null, 
--   secondary_sc citext not null, 
--   fi_manager citext not null, 
  psc_first_name citext not null,
  psc_last_name citext not null,
  psc_employee_number citext not null,
  ssc_first_name citext not null,
  ssc_last_name citext not null,
  ssc_employee_number citext not null,
  fi_first_name citext not null,
  fi_last_name citext not null,
  fi_employee_number citext not null,
  total_care integer not null, 
  service_contract integer not null, 
  gap integer not null, 
  executive_qualified integer not null,
  constraint deals_by_month_pkey primary key (store_code,bopmast_id,year_month));

drop table if exists sls.deals_by_month_tmp;
create table sls.deals_by_month_tmp (
  year_month integer not null, 
  store_code citext not null,  
  bopmast_id integer not null, 
  seq integer not null, 
  unit_count integer not null, 
  sale_type citext not null, 
  vehicle_type citext not null, 
  sale_group citext not null,
  stock_number citext not null, 
  vin citext not null, 
  odometer_at_sale integer not null, 
  model_year integer not null, 
  make citext not null, 
  model citext not null, 
  buyer citext not null, 
  cobuyer citext not null, 
--   primary_sc citext not null, 
--   secondary_sc citext not null, 
--   fi_manager citext not null, 
  psc_first_name citext not null,
  psc_last_name citext not null,
  psc_employee_number citext not null,
  ssc_first_name citext not null,
  ssc_last_name citext not null,
  ssc_employee_number citext not null,
  fi_first_name citext not null,
  fi_last_name citext not null,
  fi_employee_number citext not null,
  total_care integer not null, 
  service_contract integer not null, 
  gap integer not null, 
  executive_qualified integer not null,
  constraint deals_by_month_tmp_pkey primary key (store_code,bopmast_id,year_month));  
  
insert into sls.deals_by_month_tmp (year_month,store_code,bopmast_id,seq,unit_count,sale_type,
  vehicle_type,sale_group,stock_number,vin,odometer_at_sale,model_year,make,model,buyer,
  cobuyer,psc_first_name,psc_last_name, psc_employee_number,ssc_first_name,ssc_last_name,
  ssc_employee_number,fi_first_name,fi_last_name,fi_employee_number,total_care,
  service_contract,gap,executive_qualified)
select a.year_month, a.store_code, a.bopmast_id, b.seq, a.unit_count,
c.saletype, c.vehicletype,
case -- one outlier H10091L
  when b.sale_group_code ='UHC' then 'HONDA USED CAR'
  else g.sale_group
end,
b.stock_number, b.vin, b.odometer_at_sale,
d.modelyear::integer, d.make, d.model,
e.fullname as buyer, coalesce(f.fullname, 'none') as cobuyer,
case
  when b.store_code = 'ry1' then (select first_name from sls.personnel where ry1_id = b.primary_sc)
  when b.store_code = 'ry2' then (select first_name from sls.personnel where ry2_id = b.primary_sc)
end as psc_first_name,
case
  when b.store_code = 'ry1' then (select last_name from sls.personnel where ry1_id = b.primary_sc)
  when b.store_code = 'ry2' then (select last_name from sls.personnel where ry2_id = b.primary_sc)
end as psc_last_name,
case
  when b.store_code = 'ry1' then (select employee_number from sls.personnel where ry1_id = b.primary_sc)
  when b.store_code = 'ry2' then (select employee_number from sls.personnel where ry2_id = b.primary_sc)
end as psc_employee_number,
coalesce(
  case
    when b.store_code = 'ry1' and b.secondary_sc <> 'none'
      then (select first_name from sls.personnel where ry1_id = b.secondary_sc and store_code = b.store_code)
    when b.store_code = 'ry2' and b.secondary_sc <> 'none'
      then (select first_name from sls.personnel where ry2_id = b.secondary_sc and store_code = b.store_code)
  end, 'none') as ssc_first_name,
coalesce(
  case
    when b.store_code = 'ry1' and b.secondary_sc <> 'none'
      then (select last_name from sls.personnel where ry1_id = b.secondary_sc and store_code = b.store_code)
    when b.store_code = 'ry2' and b.secondary_sc <> 'none'
      then (select last_name from sls.personnel where ry2_id = b.secondary_sc and store_code = b.store_code)
  end, 'none') as ssc_last_name,
coalesce(
  case
    when b.store_code = 'ry1' and b.secondary_sc <> 'none'
      then (select employee_number from sls.personnel where ry1_id = b.secondary_sc and store_code = b.store_code)
    when b.store_code = 'ry2' and b.secondary_sc <> 'none'
      then (select employee_number from sls.personnel where ry2_id = b.secondary_sc and store_code = b.store_code)
  end, 'none') as ssc_employee_number,
coalesce(
  (select first_name
    from sls.personnel
    where fi_id = b.fi_manager and store_code = b.store_code), 'none') as fi_first_name,
coalesce(
  (select last_name
    from sls.personnel
    where fi_id = b.fi_manager and store_code = b.store_code), 'none') as fi_last_name,
            coalesce(
  (select employee_number
    from sls.personnel
    where fi_id = b.fi_manager and store_code = b.store_code), 'none') as fi_employee_number,
case when b.total_care > 1599 then a.unit_count else 0 end as total_care,
case when b.service_contract > 0 then a.unit_count else 0 end as service_contract,
case when b.gap > 0 then a.unit_count else 0 end as gap,
case when b.odometer_at_sale < 100000 then 1 else 0 end as executive_qualified -- 1 -> 200/unit, 0 -> 280/unit
from ( -- one row per deal (bopmast_id/store) per year_month
select store_code, bopmast_id, stock_number, year_month, sum(unit_count)::integer as unit_count
from sls.deals
group by store_code, bopmast_id, stock_number, year_month
having sum(unit_count) <> 0) a
-- for the given month, the sls.deals data for the bopmast_id row with the max seq
left join sls.deals b on a.store_code = b.store_code and a.bopmast_id = b.bopmast_id and a.year_month = b.year_month
and b.seq = (
  select max(seq)
  from sls.deals
  where store_code = b.store_code and bopmast_id = b.bopmast_id and year_month = b.year_month)
left join (
select distinct saletypecode,saletype, vehicletypecode, vehicletype
from ads.ext_dim_car_deal_info) c on b.sale_type_code = c.saletypecode
  and b.vehicle_type_code = c.vehicletypecode
left join ads.ext_dim_vehicle d on b.vin = d.vin
left join ads.ext_dim_customer e on b.buyer_bopname_id = e.bnkey
left join ads.ext_dim_customer f on b.cobuyer_bopname_id = f.bnkey
and f.bnkey <> 0
left join sls.sale_groups g on b.store_code = g.store_code and b.sale_group_code = g.code
  where not exists (
    select 1
    from sls.deals_by_month
    where store_code = a.store_code
      and bopmast_id = a.bopmast_id
      and year_month = a.year_month);


-- 5/20 put these (new/changed row)s in function sls.update_deals_by_month()
-- only open month? all history ?
-- payroll itself will be a non changing snapshot
-- all history, needs to be as currently correct as possible, can the be used, if wanted,
-- to compare to payroll and see what as changed over time
-- so, add a hash to the table, or just calculate it each time
-- fuck, let's just do it each time and see how it goes
-- rows that have changed
-- shit, updates will be a lot easier with a _tmp table, go ahead and do it, 


-- new rows
insert into sls.deals_by_month (year_month,store_code,bopmast_id,seq,unit_count,sale_type,
  vehicle_type,sale_group,stock_number,vin,odometer_at_sale,model_year,make,model,buyer,
  cobuyer,psc_first_name,psc_last_name, psc_employee_number,ssc_first_name,ssc_last_name,
  ssc_employee_number,fi_first_name,fi_last_name,fi_employee_number,
  total_care,service_contract,gap,executive_qualified)
select year_month,store_code,bopmast_id,seq,unit_count,sale_type,
  vehicle_type,sale_group,stock_number,vin,odometer_at_sale,model_year,make,model,buyer,
  cobuyer,psc_first_name,psc_last_name, psc_employee_number,ssc_first_name,ssc_last_name,
  ssc_employee_number,fi_first_name,fi_last_name,fi_employee_number,
  total_care,service_contract,gap,executive_qualified
from sls.deals_by_month_tmp a
where not exists (
  select 1
  from sls.deals_by_month
  where store_code = a.store_code
    and bopmast_id = a.bopmast_id
    and year_month = a.year_month);

-- changed rows
update sls.deals_by_month w
set seq = x.seq,
    unit_count = x.unit_count,
    sale_type = x.sale_type,
    vehicle_type = x.vehicle_type,
    sale_group = x.sale_group,
    stock_number = x.stock_number,
    vin = x.vin,
    odometer_at_sale = x.odometer_at_sale,
    model_year = x.model_year,
    make = x.make,
    model = x.model,
    buyer = x.buyer,
    cobuyer = x.cobuyer,
    psc_first_name = x.psc_first_name,
    psc_last_name = x.psc_last_name,
    psc_employee_number = x.psc_employee_number,
    ssc_first_name = x.ssc_first_name,
    ssc_last_name = x.ssc_last_name,
    ssc_employee_number = x.ssc_employee_number,
    fi_first_name = x.fi_first_name,
    fi_last_name = x.fi_last_name,
    fi_employee_number = x.fi_employee_number,
    total_care = x.total_care,
    service_contract = x.service_contract,
    gap = x.gap,
    executive_qualified = x.executive_qualified
from (
  select c.store_code, c.bopmast_id, c.year_month, 
    c.seq, c.unit_count, c.sale_type, c.vehicle_type, 
    c.sale_group, c.stock_number, c.vin, c.odometer_at_sale, 
    c.model_year, c.make, c.model, c.buyer, c.cobuyer, 
    c.psc_first_name, c.psc_last_name, c.psc_employee_number,
    c.ssc_first_name, c.ssc_last_name, c.ssc_employee_number,
    c.fi_first_name, c.fi_last_name, c.fi_employee_number,
    c.total_care, c.service_contract, c.gap, c.executive_qualified
  from (
    select a.*, md5(a::text) as hash
    from sls.deals_by_month_tmp a) c  
  inner join (
    select b.*, md5(b::text) as hash
    from sls.deals_by_month b) d on c.store_code = d.store_code
      and c.bopmast_id = d.bopmast_id
      and c.year_month = d.year_month
      and c.hash <> d.hash) x
where w.store_code = x.store_code
  and w.bopmast_id = x.bopmast_id
  and w.year_month = x.year_month      

select 'base' as source, a.* from sls.deals_by_month a where bopmast_id in (40999,42077,41659)  
union
select 'tmp' as source, a.* from sls.deals_by_month_tmp a where bopmast_id in (40999,42077,41659)  
order by bopmast_id, source