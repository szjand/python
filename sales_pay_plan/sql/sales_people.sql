﻿
/*
5/18
changes in teams from KC list

Austin Janzen missing
Phil Yunker to Langenstein from Eden
Craig Croaker to Aubol from Barker
Steve Flaat ???
Team Corey ???
Alan Mulhern to Barker
Nikoklai Holland to Weber
Curtis Olson ???

from kc 5/19:
austin and steve are on anthonys team.  
 
Nicholai will still be hourly $16.00 hourly
Donovan wil be on a Standard Sales Consultant pay plan.
Corey will be paid 6500 this month
Bryn will be paid 6k
Sam will be paid 5k
Jim W and Craig C will be paid at 330 per unit base per unit instead of 280 going forward.  (Senior pay plan)  
Do you need everyones draw and base if they have it?

from kc 5/19:
Scotty should be on senior pay as well.

from kc 5/19:
Curtis is on long term leave.  Not on anyone’s team
Steve is on annthonys team until his new hires starts middle of next month.  
He has steve f, arden l, donanvan e, Austin j.  
Dylan is on team barker.  Sorry that was an error. 
All guarantees that I have should be set up in deaolertrack.  Tom has a min of 10k and jared l of 7k.  
That looks good for the senior pay plan. 



new qualifier: Senior, it is not a pay plan, it affects only the per unit base

implementing guarantees


*/

select a.*, b.fullname as psc, b.employeenumber as psc_emp, 
  d.fullname as fi, d.employeenumber as fi_emp,
  c.fullname as ssc, c.employeenumber as ssc_emp
from (
  select store_code, primary_sc, secondary_sc, fi_manager, count(*)
  from sls.deals
  group by store_code, primary_sc, secondary_sc, fi_manager) a
left join (
  select *
  from ads.dim_salesperson
  where active = true
    and salespersontypecode = 's') b on a.store_code = b.storecode
      and a.primary_sc = b.salespersonid
left join (
  select *
  from ads.dim_salesperson
  where active = true
    and salespersontypecode = 's') c on a.store_code = c.storecode
      and a.secondary_sc = c.salespersonid
left join (
  select *
  from ads.dim_salesperson
  where active = true
    and salespersontypecode = 'f') d on a.store_code = d.storecode
      and a.fi_manager = d.salespersonid
order by count desc      


drop table if exists sls.personnel;
create table sls.personnel (
  store_code citext not null, -- store at which employed
  employee_number citext not null,
  last_name citext not null,
  first_name citext not null, 
  hire_date date not null,
  ry1_id citext not null,
  ry2_id citext not null,
  fi_id citext not null,
  constraint personnel_pkey primary key (employee_number));

-- only one missing is jared langenstein
drop table if exists personnel;
create temp table personnel as 
select a.company_number, a.sales_person_id, a.sales_person_name, a.sales_person_type, 
  b.*
from arkona.ext_bopslsp a
left join ads.dim_salesperson b on a.company_number = b.storecode
  and a.sales_person_id = b.salespersonid
where a.active is null  
  and a.company_number in ('ry1','ry2') order by sales_person_name

-- unique
select company_number, sales_person_id
from arkona.ext_bopslsp
group by company_number, sales_person_id
having count(*) > 1

-- fi that also have the same id as consultaut
select *
from arkona.ext_bopslsp a
where sales_person_type = 'F'
  and active is null
  and company_number in ('ry1','ry2')
  and exists (
    select 1
    from arkona.ext_bopslsp
    where sales_person_id = a.sales_person_id
      and sales_person_type = 'S')


-- active folks with same id as fi & consultant      
select *
from arkona.ext_bopslsp a
where active is null
  and company_number in ('ry1','ry2')
  and exists (
    select 1
    from arkona.ext_bopslsp
    where sales_person_id = a.sales_person_id
      and active is null
      and sales_person_type = 'S')      
  and exists (
    select 1
    from arkona.ext_bopslsp
    where sales_person_id = a.sales_person_id
      and active is null
      and sales_person_type = 'F')       
order by sales_person_name       


drop table if exists sls.personnel;
create table sls.personnel (
  store_code citext not null, -- store at which employed
  employee_number citext not null,
  last_name citext not null,
  first_name citext not null, 
  start_date date not null,
  ry1_id citext not null,
  ry2_id citext not null,
  fi_id citext not null,
  constraint personnel_pkey primary key (employee_number));

insert into sls.personnel
select company_number, employeenumber, lastname, firstname, hiredate,
  replace(ry1_id, 'zzz', 'none')::citext as ry1_id,
  replace(ry2_id, 'zzz', 'none')::citext as ry2_id,
  replace(fi_id, 'zzz', 'none')::citext as fi_id
from (  
  select company_number, employeenumber, lastname, firstname, hiredate,
    min(coalesce(ry1_id, 'zzz')) as ry1_id,
    min(coalesce(ry2_id, 'zzz')) as ry2_id,
    min(coalesce(fi_id, 'zzz')) as fi_id
  from (  
    select 
        case
          when left(a.employeenumber, 1) = '1' then 'RY1'
          when left(a.employeenumber, 1) = '2' then 'RY2'
          else a.company_number
        end, 
      a.employeenumber, 
      case a.employeenumber
        when 'N/A' then 'HSE'
        else b.lastname
      end as lastname, 
      case a.employeenumber
        when 'N/A' then 'HSE'
        else b.firstname
      end as firstname, 
      coalesce(b.hiredate, '12/31/9999') as hiredate,
      case when a.company_number = 'RY1' and a.sales_person_type = 'S' then a.sales_person_id end as ry1_id,
      case when a.company_number = 'RY2' and a.sales_person_type = 'S' then a.sales_person_id end as ry2_id,
      case when a.sales_person_type = 'F' then a.sales_person_id end as fi_id
    from personnel a  
    left join ads.ext_dds_edwEmployeeDim b on a.employeenumber = b.employeenumber
      and b.currentrow = true
    where sales_person_id <> 'jls'
      and b.lastname is not null) x
  group by company_number, employeenumber, lastname, firstname, hiredate) y;

insert into sls.personnel values
('RY1','184625','Langenstein','Jared','02/19/2016','none','none','JLS'),
('RY1','1212688','Eastman','Donovan','04/10/2017','EAS','none','none'),
('RY1','111232','Holland','Nikolai','03/27/2017','NHO','none','none'),
('RY1','1130101','Solie','Joshua','03/20/2017','SOL','none','none'),
('RY1','1112233','Jeanotte','Jarrett','04/24/2017','JJE','none','none');

create table sls.team_roles (
  team_role citext primary key);
insert into sls.team_roles (team_role) values
('team_leader'),
('consultant');  

drop table if exists sls.personnel_roles cascade;
create table sls.personnel_roles (
  employee_number citext not null references sls.personnel(employee_number),
  team_role citext not null references sls.team_roles(team_role),
  from_date date not null,
  thru_date date not null default '12/31/9999',
  constraint personnel_roles_pkey primary key(employee_number,thru_date));
  
insert into sls.personnel_roles (employee_number,team_role,from_date) 
select a.employee_number, 'consultant', '03/01/2017'
from sls.personnel a 
where employee_number in ('171150','123425','123425','1151450','1106225',
  '161325','128530','189100','123600','117205','1147250','150040',
  '151702','1106223','163700','133017','124120','145840','1124625',
  '1124625','1112410','1212688','1130690','111352','1109815','148080',
  '1130101','111232','1135518','1141642') on conflict do nothing;

insert into sls.personnel_roles (employee_number,team_role,from_date) 
select a.employee_number, 'team_leader', '03/01/2017'
from sls.personnel a 
where employee_number in ('195460','179380','184625','17534',
  '137220','1132700','1147810','110052');  

-- drop table sls.teams cascade;
create table sls.teams (
  team citext primary key);
insert into sls.teams (team) values 
('team anthony'),  
('team ben'),
('team langenstein'),
('team tom'),
('team eden'),
('team stout'),
('team weber'),
('team barker');

drop table if exists sls.team_personnel cascade;
create table sls.team_personnel (
  team citext not null references sls.teams(team),
  employee_number citext not null references sls.personnel(employee_number),
  from_date date not null,
  thru_date date not null default '12/31/9999',
  constraint team_personnel_pkey primary key(employee_number,thru_date));

insert into sls.team_personnel(team,employee_number,from_date) values 
('team anthony','171150','03/01/17'),
('team anthony','189100','03/01/17'),
('team anthony','163700','03/01/17'),
('team anthony','1212688','03/01/17'),
('team anthony','195460','03/01/17'),
('team ben','123425','03/01/17'),
('team ben','123600','03/01/17'),
('team ben','133017','03/01/17'),
('team ben','1130690','03/01/17'),
('team ben','179380','03/01/17'),
('team langenstein','117205','03/01/17'),
('team langenstein','124120','03/01/17'),
('team langenstein','111352','03/01/17'),
('team langenstein','184625','03/01/17'),
('team tom','1135518','03/01/17'),
('team tom','1147250','03/01/17'),
('team tom','145840','03/01/17'),
('team tom','1109815','03/01/17'),
('team tom','17534','03/01/17'),
('team eden','1151450','03/01/17'),
('team eden','137220','03/01/17'),
('team stout','1106225','03/01/17'),
('team stout','150040','03/01/17'),
('team stout','1124625','03/01/17'),
('team stout','148080','03/01/17'),
('team stout','1132700','03/01/17'),
('team weber','161325','03/01/17'),
('team weber','151702','03/01/17'),
('team weber','1141642','03/01/17'),
('team weber','1147810','03/01/17'),
('team barker','128530','03/01/17'),
('team barker','1106223','03/01/17'),
('team barker','1112410','03/01/17'),
('team barker','111232','03/01/17'),
('team barker','110052','03/01/17')
on conflict do nothing;

create table sls.payplans (
  payplan citext primary key);
insert into sls.payplans (payplan) values
('team_leader'),
('regular'),
('hourly'),
('executive'),
('cadillac');

drop table if exists sls.personnel_payplans cascade;
create table sls.personnel_payplans (
  employee_number citext not null references sls.personnel(employee_number),
  payplan citext not null references sls.payplans(payplan),
  from_date date not null,
  thru_date date not null default '12/31/9999',
  constraint personnel_payplans_pkey primary key(employee_number,thru_date));

insert into sls.personnel_payplans (employee_number,payplan,from_date) values 
('195460', 'team_leader', '03/01/2017'), 
('179380', 'team_leader', '03/01/2017'), 
('184625', 'team_leader', '03/01/2017'), 
('17534', 'team_leader', '03/01/2017'), 
('137220', 'team_leader', '03/01/2017'), 
('1132700', 'team_leader', '03/01/2017'), 
('1147810', 'team_leader', '03/01/2017'), 
('110052', 'team_leader', '03/01/2017'), 
('171150', 'regular', '03/01/2017'), 
('189100', 'regular', '03/01/2017'), 
('163700', 'regular', '03/01/2017'), 
('123425', 'regular', '03/01/2017'), 
('123600', 'regular', '03/01/2017'), 
('133017', 'regular', '03/01/2017'), 
('1130690', 'regular', '03/01/2017'), 
('117205', 'regular', '03/01/2017'), 
('124120', 'regular', '03/01/2017'), 
('111352', 'regular', '03/01/2017'), 
('1135518', 'regular', '03/01/2017'), 
('1147250', 'regular', '03/01/2017'), 
('145840', 'regular', '03/01/2017'), 
('1151450', 'regular', '03/01/2017'), 
('161325', 'regular', '03/01/2017'), 
('151702', 'regular', '03/01/2017'), 
('128530', 'regular', '03/01/2017'), 
('1106223', 'regular', '03/01/2017'), 
('1112410', 'regular', '03/01/2017'), 
('111232', 'regular', '03/01/2017'),
('1212688', 'hourly', '03/01/2017'),
('1130101', 'hourly', '03/01/2017'),
('148080', 'executive', '03/01/2017'),
('150040', 'executive', '03/01/2017'),
('1124625', 'executive', '03/01/2017'),
('1106225', 'executive', '03/01/2017'),
('1109815', 'cadillac', '03/01/2017'),
('1141642', 'cadillac', '03/01/2017')
on conflict do nothing;

-- personnel with an assigned role
select b.* 
from sls.personnel_roles a
inner join sls.personnel b on a.employee_number = b.employee_number
order by b.last_name

drop table if exists sls.folks;
create table sls.folks (
  team citext not null references sls.teams(team),
  employee_number citext not null,
  last_name citext not null,
  first_name citext not null,
  ry1_id citext not null,
  ry2_id citext not null,
  fi_id citext not null,
  payplan citext not null references sls.payplans(payplan),
  is_senior boolean not null default false,
  constraint folks_pkey primary key(employee_number));

-- 6/5/17, clean it up for inclusion in luigi sales_consultant_payroll.ClockHoursByMonth
-- insert into sls.folks
-- select a.team, b.employee_number, c.last_name, c.first_name, c.ry1_id, 
--   c.ry2_id, c.fi_id, d.payplan, c.is_senior
-- from sls.teams a
-- inner join sls.team_personnel b on a.team = b.team
--   and b.thru_date > current_date
-- inner join sls.personnel c on b.employee_number = c.employee_number
-- inner join sls.personnel_payplans d on c.employee_number = d.employee_number
--   and d.thru_date > current_date
-- inner join sls.payplans e on d.payplan = e.payplan
-- order by team, last_name;

select c.team, a.employee_number, a.last_name, a.first_name, 
  a.ry1_id, a.ry2_id, a.fi_id, d.payplan, a.is_senior
from sls.personnel a
inner join sls.team_personnel c on a.employee_number = c.employee_number  
  and c.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and c.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')
inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
  and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and d.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open');

-- 5/22 reconfigure teams for 201705
alter table sls.personnel_payplans
drop constraint personnel_payplans_payplan_fkey;

alter table sls.folks
drop constraint folks_payplan_fkey;

-- update sls.personnel_payplans
-- set payplan = 'standard'
-- where payplan = 'regular';

update sls.payplans
set payplan = 'standard'
where payplan = 'regular';

alter table sls.personnel_payplans
add constraint personnel_payplans_payplan_fkey foreign key (payplan) references sls.payplans (payplan) on update cascade;

alter table sls.folks
add constraint folks_payplan_fkey foreign key (payplan) references sls.payplans (payplan) on update cascade;

1. Curtis Olson not currently employed
update sls.team_personnel
set thru_date = '04/30/2017'
-- select * from sls.team_personnel
where employee_number = '1106223'
  and thru_date > current_date;
  
update sls.personnel_roles
set thru_date = '04/30/2017'
-- select * from sls.team_personnel
where employee_number = '1106223'
  and thru_date > current_date;
  
update sls.personnel_payplans
set thru_date = '04/30/2017'
-- select * from sls.team_personnel
where employee_number = '1106223'
  and thru_date > current_date;

2. Phil Yunker moved to team langenstein from eden
update sls.team_personnel
set thru_date = '04/30/2017'
-- select * from sls.team_personnel
where employee_number = '1151450'
  and thru_date > current_date;

insert into sls.team_personnel (team, employee_number, from_date)
values('team langenstein', '1151450', '05/01/2017');

3. craig croaker to aubol from barker
update sls.team_personnel
set thru_date = '04/30/2017'
-- select * from sls.team_personnel
where employee_number = '128530'
  and thru_date > current_date;
-- delete from sls.team_personnel where employee_number = '128530' and from_date = '05/01/2017'
insert into sls.team_personnel (team, employee_number, from_date)
values('team tom', '128530', '05/01/2017');

4. steve flaat from aubul to anthony
update sls.team_personnel
set thru_date = '04/30/2017'
-- select * from sls.team_personnel
where employee_number = '145840'
  and thru_date > current_date;

insert into sls.team_personnel (team, employee_number, from_date)
values('team anthony', '145840', '05/01/2017');

5. mulhern from weber to barker
update sls.team_personnel
set thru_date = '04/30/2017'
-- select * from sls.team_personnel
where employee_number = '151702'
  and thru_date > current_date;

insert into sls.team_personnel (team, employee_number, from_date)
values('team barker', '151702', '05/01/2017');

6. holland from barker to holland
   payplan to hourly
update sls.team_personnel
set thru_date = '04/30/2017'
-- select * from sls.team_personnel
where employee_number = '111232'
  and thru_date > current_date;

insert into sls.team_personnel (team, employee_number, from_date)
values('team weber', '111232', '05/01/2017');  

update sls.personnel_payplans
set thru_date = '04/30/2017'
where employee_number = '111232'
  and thru_date > current_date;

insert into sls.personnel_payplans (employee_number, payplan, from_date)
values('111232', 'hourly', '05/01/2017');    

select * from sls.folks
where last_name = 'holland'
order by team, last_name

select * from sls.teams

7. eastman from hourly to standard

update sls.personnel_payplans
set thru_date = '04/30/2017'
where employee_number = '1212688'
  and thru_date > current_date;

insert into sls.personnel_payplans (employee_number, payplan, from_date)
values('1212688', 'standard', '05/01/2017');    

8. haley from anthony to barker
update sls.team_personnel
set thru_date = '04/30/2017'
-- select * from sls.team_personnel
where employee_number = '163700'
  and thru_date > current_date;

insert into sls.team_personnel (team, employee_number, from_date)
values('team barker', '163700', '05/01/2017');    
  
9. guarantees are in place
select * from sls.payroll_guarantees

10. need to add senior indicator: warmack, croaker, pearson
alter table sls.personnel
add column is_senior boolean not null default FALSE;

update sls.personnel
set is_senior = true
where employee_number in ('1109815','1147250','128530');

alter table sls.folks
add column is_senior boolean not null default false;

-- here is the config for 201705
select a.team, a.last_name, a.first_name, a.payplan, b.amount as guarantee,
  case when is_senior then 'senior' else null::citext end as is_senior
from sls.folks a
left join sls.payroll_guarantees b on a.employee_number = b.employee_number
  and b.year_month = (select year_month from sls.open_month)
order by a.team, a.last_name

/*
5/23
from kc: 
Just nickoli jimmy moved him to regular beginning of month.  That's why he hasn't hit clock.
*/

select * from sls.folks where last_name = 'holland'

select * from sls.personnel_payplans where employee_number = '111232'

delete 
from sls.personnel_payplans
where employee_number = '111232'
  and payplan = 'hourly';

update sls.personnel_payplans
set thru_date = '12/31/9999'
where employee_number = '111232'  
  and payplan = 'standard';


--c: personnel assigned to teams in open month
      select b.team, a.*
      from sls.personnel a
      inner join sls.team_personnel b  on a.employee_number = b.employee_number
      where b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
        and thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')
      order by b.team, a.last_name

------------------------------------------------------------------------------------------------------------------------------------
-- June 2017 Team Configs ----------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
-- add guarantees for june
insert into sls.payroll_guarantees (employee_number,year_month, amount)
select employee_number, 201706, amount
from sls.payroll_guarantees
where year_month = 201705;

-- how teams are currently configured
select b.team, a.last_name, a.first_name, a.employee_number, a.is_senior, c.payplan, d.amount as guarantee
from sls.personnel a
inner join sls.team_personnel b  on a.employee_number = b.employee_number
  and b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and b.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')
inner join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and c.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open') 
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = (select year_month from sls.months where open_closed = 'open')  
order by b.team, a.last_name;


-- from KC: -------------------------------------------------------------
Team Leader	        Team Leader	        Team Leader	        Team Leader	        Team Leader	        Team Leader	        Team Leader
Anthony Michael	    Tom Abol	          Ben Knudson	        Jimmy Weber       	Rick Stout	        Corey Eden	        Josh Barker
Donavan Eastman	    Scott Pearson	      Nate Dockendorf	    Brett Hanson	      Sam Foster	        Alan Mulhurn	      AJ Preston
Arden Loven	        Jim Warmack	        Tyler Bedney	      Josh Solie	        Bryn Seay	          Phil Yunker	        Jesse Chavez
Austin Janzen	      Craig Crocker	      Justin Bjarnason	  Nikolai Holland	    Chris Garceau	      Dylan Haley	        Nick Bellmore
Steve Flaat	        Jeff Tarr	          Larry Stadstad	    Tanner Trosen	      John Olderback	    Ross Belgarde	      Damien Brooks
                                                            Brian Schneider									
Team Corey and Team Barker will split down the middle figured as one team leader of eight divided by 2.  Thanks Jon						
-------------------------------------------------------------------------
team ben:
  remove:
    carlson
    carter
  add:
    bedney
    bjarnason 
    
team weber:
  add:
    schneider
    
team eden
  add: 
    mulhern from barker
    yunker from langenstein
    haley from barker
    belgarde
    
team barker:  
  remove:
    mulhern
    haley
    
  add:
    chavez from langenstein
    bellmore from langenstein
    brooks from langenstein


New Hourly: 
  Tyler Bedney, 
  Justin Bjarnason
  Brian Schneider
  Ross Belgarde

Changes:
    Solie: hourly -> standard

1. new consultants
select * from sls.personnel order by last_name
new personnel: Tyler Bedney: 112589, 6/5/17, TBE, TBE
               Justin Bjarnason: 147741, 6/5/17, JBJ, JBJ
               Brian Schneider: 1566882, 6/5/17,  BSC, BSC
               Ross Belgarde: 12242, 6/19/17, RBE, RBE
               
-- add new consultants to sls.personnel
insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,ry1_id,
  ry2_id,fi_id,is_senior,anniversary_date) values
('RY1', '112589', 'Bedney', 'Tyler', '06/05/2017', 'TBE', 'TBE', 'none', false, '06/05/2017'),                 
('RY1', '1447741', 'Bjarnason', 'Justin', '06/05/2017', 'JBJ', 'JBJ', 'none', false, '06/05/2017'),
('RY1', '1566882', 'Schneider', 'Brian', '06/05/2017', 'BSC', 'BSC', 'none', false, '06/05/2017'),
('RY1', '12242', 'Belgarde', 'Ross', '06/19/2017', 'RBE', 'RBE', 'none', false, '06/05/2017');

-- add new consultants to sls.team_personnel, sls.personnel_roles, sls.personnel_payplans
-- bedney 112589 - team ben - consultant - hourly
insert into sls.team_personnel (team, employee_number, from_date)
values('team ben', '112589', '06/01/2017');    
insert into sls.personnel_roles (employee_number, team_role, from_date)
values('112589', 'consultant', '06/01/2017'); 
insert into sls.personnel_payplans (employee_number, payplan, from_date)
values('112589', 'hourly', '06/01/2017');

-- bjarnason 1447741 - team ben - consultant - hourly
insert into sls.team_personnel (team, employee_number, from_date)
values('team ben', '1447741', '06/01/2017');    
insert into sls.personnel_roles (employee_number, team_role, from_date)
values('1447741', 'consultant', '06/01/2017'); 
insert into sls.personnel_payplans (employee_number, payplan, from_date)
values('1447741', 'hourly', '06/01/2017');

-- schneider 1566882 - team weber - consultant - hourly
insert into sls.team_personnel (team, employee_number, from_date)
values('team weber', '1566882', '06/01/2017');    
insert into sls.personnel_roles (employee_number, team_role, from_date)
values('1566882', 'consultant', '06/01/2017'); 
insert into sls.personnel_payplans (employee_number, payplan, from_date)
values('1566882', 'hourly', '06/01/2017');

-- belgarde 12242 - team eden - consultant - hourly
insert into sls.team_personnel (team, employee_number, from_date)
values('team eden', '12242', '06/01/2017');    
insert into sls.personnel_roles (employee_number, team_role, from_date)
values('12242', 'consultant', '06/01/2017'); 
insert into sls.personnel_payplans (employee_number, payplan, from_date)
values('12242', 'hourly', '06/01/2017');


2. terms: chris carlson 123425
          logan carter 123600
   no longer in teams: jared langenstein 184625
   
update sls.team_personnel
set thru_date = '05/31/2017'
where employee_number = '184625'
  and thru_date > current_date;
  
update sls.personnel_roles
set thru_date = '05/31/2017'
where employee_number = '184625'
  and thru_date > current_date;
  
update sls.personnel_payplans
set thru_date = '05/31/2017'
where employee_number = '184625'
  and thru_date > current_date;


3. team restructuring

-- mulhern 151702 from team barker to team eden
update sls.team_personnel
set thru_date = '05/31/2017'
where employee_number = '151702'
  and thru_date > current_date;
insert into sls.team_personnel (team, employee_number, from_date)
values('team eden', '151702', '06/01/2017');  

-- yunker 1151450 from team langenstein to team eden
update sls.team_personnel
set thru_date = '05/31/2017'
where employee_number = '1151450'
  and thru_date > current_date;
insert into sls.team_personnel (team, employee_number, from_date)
values('team eden', '1151450', '06/01/2017');  

-- haley 163700 from barker to eden
update sls.team_personnel
set thru_date = '05/31/2017'
where employee_number = '163700'
  and thru_date > current_date;
insert into sls.team_personnel (team, employee_number, from_date)
values('team eden', '163700', '06/01/2017');  

-- chavez 124120 from langestein to team barker
update sls.team_personnel
set thru_date = '05/31/2017'
where employee_number = '124120'
  and thru_date > current_date;
insert into sls.team_personnel (team, employee_number, from_date)
values('team barker', '124120', '06/01/2017');  

-- bellmore 111352 from langestein to team barker
update sls.team_personnel
set thru_date = '05/31/2017'
where employee_number = '111352'
  and thru_date > current_date;
insert into sls.team_personnel (team, employee_number, from_date)
values('team barker', '111352', '06/01/2017'); 

-- brooks 117205 from langestein to team barker
update sls.team_personnel
set thru_date = '05/31/2017'
where employee_number = '117205'
  and thru_date > current_date;
insert into sls.team_personnel (team, employee_number, from_date)
values('team barker', '117205', '06/01/2017'); 

4. payplan change: solie 1130101 from hourly to standard

update sls.personnel_payplans
set thru_date = '05/31/2017'
where employee_number = '1130101'
  and thru_date > current_date;
insert into sls.personnel_payplans (employee_number,payplan,from_date)
values('1130101','standard','06/01/2017');  
-- standard has 3000 guarantee
insert into sls.payroll_guarantees (employee_number, year_month, amount)
values('1130101', 201706, 3000);

/*
6/28
realized that i fat fingered bjarnason's employeenumber
alter the foreign key constraints so i can update the employeenumber in 
personnel and have the foreign keys update
*/
first
do not need table sls.folks in sales_consultant_payroll.py class ClockHoursByMonth
alter table sls.folks
rename to z_unused_folks;

alter table sls.team_personnel
drop constraint team_personnel_employee_number_fkey;

alter table sls.team_personnel
add constraint team_personnel_employee_number_fkey foreign key (employee_number) 
  references sls.personnel (employee_number) on update cascade;

alter table sls.personnel_roles
drop constraint personnel_roles_employee_number_fkey;

alter table sls.personnel_roles
add constraint personnel_roles_employee_number_fkey foreign key (employee_number) 
  references sls.personnel (employee_number) on update cascade;

alter table sls.personnel_payplans
drop constraint personnel_payplans_employee_number_fkey;

alter table sls.personnel_payplans
add constraint personnel_payplans_employee_number_fkey foreign key (employee_number) 
  references sls.personnel (employee_number) on update cascade;


-- and now, fix bjanrason employee_number
update sls.personnel  
set employee_number = '147741'
where employee_number = '1447741';
------------------------------------------------------------------------------------------------------------------------------------
-- June 2017 Team Configs ----------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------        

-- new consultant justin brunk JBR

insert into sls.personnel(store_code,employee_number,last_name,first_name,start_date,ry1_id,
  ry2_id,fi_id,is_senior,anniversary_date, first_full_month_emp) values
('RY1', '158763', 'Brunk', 'Justin', '02/26/2018', 'JBR', 'JBR', 'none', false, '02/26/2018', 201803);

insert into sls.team_personnel (team, employee_number, from_date)
values('team foster', '158763', '02/26/2018');    

insert into sls.personnel_roles (employee_number, team_role, from_date)
values('158763', 'consultant', '02/26/2018'); 

insert into sls.personnel_payplans (employee_number, payplan, from_date)
values('158763', 'standard', '02/26/2018');


select * from sls.personnel


