﻿/* team leader draw paid in 2017


select last_name,
  sum(case when x.generate_series = 201701 then draw else 0 end) as jan,
  sum(case when x.generate_series = 201702 then draw else 0 end) as feb,
  sum(case when x.generate_series = 201703 then draw else 0 end) as mar,
  sum(case when x.generate_series = 201704 then draw else 0 end) as apr,
  sum(case when x.generate_series = 201705 then draw else 0 end) as may
from (  
  select c.*, coalesce(d.draw, 0)::integer as draw
  from (
    select *
    from (select last_name, employee_number from sls.tmp_teams) a,
    (select * from generate_series(201701, 201705)) b) c
  left join sls.paid_by_month d on c.employee_number = d.employee_number
    and c.generate_series = d.year_month) x
group by last_name 
   
*/


-- select * from sls.months where open_closed = 'open'

update sls.months set open_closed = 'closed';
update sls.months set open_closed = 'open' where year_month = 201705;

-- drop table if exists teams;
-- create temp table teams as
-- drop table if exists sls.tmp_teams;
-- create table sls.tmp_teams(
--   year_month integer not null,
--   last_name citext not null,
--   first_name citext not null,
--   employee_number citext primary key,
--   team citext not null);
truncate sls.tmp_teams;
insert into sls.tmp_teams  
select (select year_month from sls.months where open_closed = 'open') as year_month, 
  a.last_name, a.first_name, a.employee_number, c.team
from sls.personnel a
inner join sls.personnel_roles b on a.employee_number = b.employee_number
  and b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and b.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')
  and b.team_role = 'team_leader'
inner join sls.team_personnel c on a.employee_number = c.employee_number  
  and c.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and c.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open');


-- drop table if exists team_census;
-- create table team_census as
-- drop table if exists sls.tmp_team_census;
-- create table sls.tmp_team_census (
--   year_month integer not null,
--   team citext not null primary key,
--   census integer not null);
truncate sls.tmp_team_census;
insert into sls.tmp_team_census  
select (select year_month from sls.months where open_closed = 'open') as year_month, 
  c.team, count(*) as census
from sls.personnel a
inner join sls.personnel_roles b on a.employee_number = b.employee_number
  and b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and b.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')
  and b.team_role = 'consultant'
inner join sls.team_personnel c on a.employee_number = c.employee_number  
  and c.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and c.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')
group by c.team;

-- drop table if exists sls.tmp_team_consultants;
-- create table sls.tmp_team_consultants (
--   year_month integer not null,
--   team citext not null,
--   first_name citext not null,
--   last_name citext not null,
--   employee_number citext primary key);
truncate sls.tmp_team_consultants;
insert into sls.tmp_team_consultants  
select (select year_month from sls.months where open_closed = 'open') as year_month,
  c.team, a.first_name, a.last_name, a.employee_number
from sls.personnel a
inner join sls.personnel_roles b on a.employee_number = b.employee_number
  and b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and b.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')
  and b.team_role = 'consultant'
inner join sls.team_personnel c on a.employee_number = c.employee_number  
  and c.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and c.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open');
      
drop table if exists sls.tmp_team_agg;
create table sls.tmp_team_agg (
  year_month integer not null,
  team citext primary key,
  team_volume numeric(3,1) not null,
  team_new_volume numeric(3,1) not null,
  team_front_gross numeric(8,2) not null,
  team_sfe numeric(8,2) not null);
truncate sls.tmp_team_agg;
insert into sls.tmp_team_agg  
select (select year_month from sls.months where open_closed = 'open') as year_month,
  cc.team, sum(team_volume) as team_volume, sum(team_new_volume) as team_new_volume,
  sum(team_front_gross) as team_front_gross, sum(team_sfe) as team_sfe
from ( -- c
  select bb.team, -- psc: based on join on psc
    sum(case when aa.ssc_first_name = 'none' then unit_count else 0.5 * unit_count end) as team_volume ,
    coalesce(
      sum(
        case when aa.vehicle_type = 'new' then
          case 
            when aa.ssc_first_name = 'none' then unit_count 
            else 0.5 * unit_count end
        end), 0) as team_new_volume,
    sum(case when aa.ssc_first_name = 'none' then aaa.front_gross else 0.5 * front_gross end) as team_front_gross,
    coalesce(sum(case when aa.ssc_first_name = 'none' then c.amount else 0.5 * c.amount end), 0) as team_sfe
  from sls.deals_by_month aa
  inner join sls.deals_gross_by_month aaa on aa.year_month = aaa.year_month
    and aa.stock_number = aaa.control  
  inner join sls.tmp_team_consultants bb on aa.psc_employee_number = bb.employee_number 
  left join sls.sfe c on aa.vin = c.vin
    and aa.year_month = c.year_month
  where aa.year_month = (select year_month from sls.months where open_closed = 'open')  
    and aa.sale_type <> 'wholesale'
  group by bb.team
  union all
  select bb.team, -- ssc: based on join on ssc
    sum(case when aa.ssc_first_name = 'none' then unit_count else 0.5 * unit_count end) as team_volume,
    coalesce(
      sum(
        case when aa.vehicle_type = 'new' then
          case 
            when aa.ssc_first_name = 'none' then unit_count 
            else 0.5 * unit_count end
        end), 0) as team_new_volume,
    sum(case when aa.ssc_first_name = 'none' then aaa.front_gross else 0.5 * front_gross end) as team_front_gross,
    coalesce(sum(case when aa.ssc_first_name = 'none' then c.amount else 0.5 * c.amount end), 0) as team_sfe
  from sls.deals_by_month aa
  inner join sls.deals_gross_by_month aaa on aa.year_month = aaa.year_month
    and aa.stock_number = aaa.control  
  inner join sls.tmp_team_consultants bb on aa.ssc_employee_number = bb.employee_number 
  left join sls.sfe c on aa.vin = c.vin
    and aa.year_month = c.year_month  
  where aa.year_month = (select year_month from sls.months where open_closed = 'open')  
    and aa.sale_type <> 'wholesale'
  group by bb.team) cc
group by cc.team;

-- drop table if exists team_leader_stats;
-- create temp table team_leader_stats as
-- create table sls.team_leader_stats (
--   year_month integer not null,
--   last_name citext not null,
--   first_name citext not null,
--   employee_number citext not null,
--   team citext not null,
--   census integer not null,
--   team_volume numeric(4,1) not null,
--   avg_per_cons numeric(4,2) not null,
--   team_new_volume numeric(4,1) not null,
--   fi_gross numeric(8,2) not null,
--   team_front_gross numeric(8,2) not null,
--   team_sfe numeric(8,2) not null,
--   store_front_gross numeric(8,2) not null,
--   store_sfe numeric(8,2) not null,
--   store_volume integer not null,
--   last_updated timestamptz not null,
--   constraint team_leader_stats_pkey primary key(year_month,employee_number));

delete 
from sls.team_leader_stats 
where year_month = (select year_month from sls.months where open_closed = 'open'); 
insert into sls.team_leader_stats
select a.year_month, a.last_name, a.first_name, a.employee_number, 
  a.team, coalesce(b.census, 0) as census, coalesce(c.team_volume, 0) as team_volume, 
  coalesce(
      case 
      when coalesce(b.census, 0) <> 0 then round(c.team_volume/b.census, 2) 
      else 0
    end, 0) as avg_per_cons,
  coalesce(c.team_new_volume, 0) as team_new_volume,
  coalesce(d.fi_gross, 0) as fi_gross,
  coalesce(c.team_front_gross, 0) as team_front_gross, 
  coalesce(c.team_sfe, 0) as team_sfe,
  e.store_front_gross, f.store_sfe, f.store_volume,
  now()  
from sls.tmp_teams a
left join sls.tmp_team_census b on a.team = b.team
left join sls.tmp_team_agg c on a.team = c.team
left join (  -- individual fi_gross
  select fi_employee_number, sum(fi_gross) as fi_gross
  from sls.deals_by_month a
  inner join sls.deals_gross_by_month b on a.year_month = b.year_month
    and a.stock_number = b.control
  where a.year_month = (select year_month from sls.months where open_closed = 'open') 
    and a.sale_type <> 'wholesale'
  group by fi_employee_number) d on a.employee_number = d.fi_employee_number
left join (  -- store front gross
  select sum(-a.amount) as store_front_gross
  from sls.deals_Accounting_detail a
  where year_month = (select year_month from sls.months where open_closed = 'open')
    and gross_category = 'front'
    and store_code = 'ry1') e on 1 = 1
left join ( -- store volume, store sfe
  select sum(a.unit_count) as store_volume, coalesce(sum(b.amount), 0) as store_sfe
  from sls.deals_by_month a
  left join sls.sfe b on a.vin = b.vin
    and a.year_month = b.year_month
  where a.year_month = (select year_month from sls.months where open_closed = 'open')
    and a.store_code = 'ry1'
    and a.sale_type <> 'wholesale') f on 1 = 1;

-- 
-- 
-- create table sls.team_leader_payroll (
--   year_month integer not null,
--   last_name citext not null,
--   first_name citext not null,
--   employee_number citext not null,
--   team citext not null,
--   census integer not null,
--   team_volume numeric(4,1) not null,
--   avg_per_cons numeric(4,2) not null,
--   team_new_volume numeric(4,1) not null,
--   fi_gross numeric(8,2) not null,
--   team_front_gross numeric(8,2) not null,
--   team_sfe numeric(8,2) not null,
--   store_front_gross numeric(8,2) not null,
--   store_sfe numeric(8,2) not null,
--   store_volume integer not null,
--   fi_pay numeric(8,2) not null,
--   team_front_gross_pay numeric(8,2) not null,
--   store_front_gross_pay numeric(8,2) not null,
--   team_volume_bonus numeric(8,2) not null,
--   team_new_volume_bonus numeric(8,2) not null,
--   store_volume_bonus numeric(8,2) not null,
--   pto_pay numeric(8,2) not null,
--   total_earned numeric(8,2) not null,
--   guarantee numeric(8,2) not null,
--   draw numeric(8,2) not null,
--   due_at_month_end numeric(8,2) not null, 
--   last_updated timestamptz not null,
--   constraint team_leader_payroll_pkey primary key(year_month,employee_number));
delete
from sls.team_leader_payroll
where year_month = (select year_month from sls.months where open_closed = 'open');  
insert into sls.team_leader_payroll
select z.*,
  round(
    case 
      when total_earned > guarantee then total_earned - draw
      else guarantee - draw
    end, 2) as due_at_month_end, now()  
from (
  select x.year_month, x.last_name, x.first_name, x.employee_number, x.team, census, 
    x.team_volume, x.avg_per_cons, x.team_new_volume, x.fi_gross, x.team_front_gross,
    x.team_sfe, x.store_front_gross, x.store_sfe, x.store_volume,
    x.fi_pay, x.team_front_gross_pay, x.store_front_gross_pay, x.team_volume_bonus,
    x.team_new_volume_bonus, x.store_volume_bonus, x.pto_pay,
    coalesce(fi_pay, 0) + coalesce(team_front_gross_pay, 0) + coalesce(store_front_gross_pay, 0) +
    coalesce(team_volume_bonus, 0) + coalesce(team_new_volume_bonus, 0) +
    coalesce(store_volume_bonus, 0) + coalesce(pto_pay, 0) as total_earned,
    x.guarantee, y.draw
  from (
    select a.*,
      case 
        when avg_per_cons >= 12 then round(.08 * a.fi_gross, 2)
        when avg_per_cons < 12 then round(.07 * a.fi_gross, 2)
      end as fi_pay,
      case 
        when a.avg_per_cons >= 12 then round(.0275 * (a.team_front_gross + a.team_sfe), 2)
        when a.avg_per_cons < 12 then round(.0225 * (a.team_front_gross + a.team_sfe), 2)
      end as team_front_gross_pay,  
      round(.0025 * (a.store_front_gross + a.store_sfe), 2) as store_front_gross_pay,
      case
        when a.avg_per_cons < 12 then 25 * a.team_volume
        when a.avg_per_cons between 12 and 13 then 35 * a.team_volume
        when a.avg_per_cons > 13 then 50 * a.team_volume
      end as team_volume_bonus,
      30 * a.team_new_volume as team_new_volume_bonus,  
      case
        when a.store_volume < 351 then 0
        when a.store_volume between 351 and 375 then 500
        when a.store_volume between 376 and 400 then 750
        when a.store_volume > 400 then 1250
      end as store_volume_bonus,
      coalesce(b.pto_pay, 0) as pto_pay, 
      coalesce(z.amount, 0) as guarantee 
    from sls.team_leader_stats a
    left join (
      select a.year_month, a.employee_number, round(a.pto_hours * b.pto_rate, 2) as pto_pay
      from sls.clock_hours_by_month a
      left join sls.pto_intervals b on a.employee_number = b.employee_number
        and a.year_month = b.year_month
      where a.year_month = (select year_month from sls.months where open_closed = 'open')
        and a.pto_hours <> 0) b on a.employee_number = b.employee_number 
          and a.year_month = b.year_month
    left join sls.payroll_guarantees z on a.employee_number = z.employee_number
      and a.year_month = z.year_month
    where a.year_month = (select year_month from sls.months where open_closed = 'open')) x
  left join sls.paid_by_month y on x.year_month = y.year_month
    and x.employee_number = y.employee_number) z;


select * from sls.team_leader_payroll order by team, year_month;
-- 
-- select * from sls.team_leader_stats;
-- 
-- select * from sls.tmp_team_agg;
-- 
-- select * from sls.tmp_team_consultants;    
-- 
-- select * from sls.tmp_teams;
-- 
-- delete from sls.team_leader_stats;
-- select * from sls.team_leader_payroll;
    
select * from sls.team_leader_payroll where year_month = 201707

