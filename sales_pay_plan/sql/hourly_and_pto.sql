﻿requires ext_ads: ext_Ads_dds_edwClockHoursFact.py
                  ext_ads_dds_edwEmployeeDim.py

select * from sls.folks where payplan = 'hourly'


select d.the_date, a.*, c.*
from sls.folks a
inner join ads.ext_dds_edwEmployeeDim b on a.employee_number = b.employeenumber                  
inner join ads.ext_dds_edwClockHoursFact c on b.employeekey = c.employeekey
inner join dds.dim_date d on c.datekey = d.date_key
where d.year_month = 201704
  and c.clockhours + vacationhours + ptohours + holidayhours <> 0

select * from scpp.pto_intervals where employee_number in ('1147250','123425')
carlson: 36.20
warmack: 40.37


select a.team, a.employee_number, a.last_name, a.first_name, a.payplan, 
  sum(c.clockhours) as clock_hours, sum(c.vacationhours) as vacation_hours
from sls.folks a
inner join ads.ext_dds_edwEmployeeDim b on a.employee_number = b.employeenumber                  
inner join ads.ext_dds_edwClockHoursFact c on b.employeekey = c.employeekey
inner join dds.dim_date d on c.datekey = d.date_key
where d.year_month = 20170
  and c.clockhours + vacationhours + ptohours + holidayhours <> 0
group by a.team, a.employee_number, a.last_name, a.first_name, a.payplan 

-- whoops, need overtime

select a.team, a.employee_number, a.last_name, a.first_name, a.payplan, 
  sum(c.regularhours) as regular_hours, sum(overtimehours) as overtime, sum(c.vacationhours) as vacation_hours
from sls.folks a
inner join ads.ext_dds_edwEmployeeDim b on a.employee_number = b.employeenumber                  
inner join ads.ext_dds_edwClockHoursFact c on b.employeekey = c.employeekey
inner join dds.dim_date d on c.datekey = d.date_key
where d.year_month = 201705
  and c.clockhours + vacationhours + ptohours + holidayhours <> 0
group by a.team, a.employee_number, a.last_name, a.first_name, a.payplan 


drop table if exists sls.xfm_fact_clock_hours;
CREATE TABLE sls.xfm_fact_clock_hours
(
  employee_number citext NOT NULL,
  the_date date NOT NULL,
  clock_hours numeric(12,4),
  regular_hours numeric(12,4) NOT NULL,
  overtime_hours numeric(12,4) NOT NULL,
  vacation_hours numeric(12,4) NOT NULL,
  pto_hours numeric(12,4) NOT NULL,
  holiday_hours numeric(12,4),
  CONSTRAINT xfm_fact_clock_hours_pkey PRIMARY KEY (employee_number, the_date));
  

drop table if exists sls.clock_hours_by_month;
create table sls.clock_hours_by_month (
  employee_number citext not null,
  year_month integer not null,
  clock_hours numeric(12,4) not null default 0,
  regular_hours numeric(12,4) NOT NULL default 0,
  overtime_hours numeric(12,4) NOT NULL default 0,
  pto_hours numeric(12,4) NOT NULL default 0,
  holiday_hours numeric(12,4) default 0,
  constraint clock_hours_by_month_pkey primary key (employee_number,year_month));

insert into sls.clock_hours_by_month
select a.employee_number, c.year_month, sum(clock_hours) as clock_hours,
  sum(regular_hours) as regular_hours, sum(b.overtime_hours) as overtime_hours, 
  sum(b.vacation_hours + b.pto_hours) as pto_hours,
  sum(b.holiday_hours) as holiday_hours
from sls.folks a
inner join sls.xfm_fact_clock_hours b on a.employee_number = b.employee_number
inner join dds.dim_date c on b.the_date = c.the_date
where c.year_month > 201702
  and b.clock_hours + b.vacation_hours + b.pto_hours + b.holiday_hours <> 0
group by c.year_month, a.employee_number;


-- and for luigi:
delete
from sls.clock_hours_by_month
where year_month = (select year_month from sls.open_month);

insert into sls.clock_hours_by_month
select a.employee_number, c.year_month, sum(clock_hours) as clock_hours,
  sum(regular_hours) as regular_hours, sum(b.overtime_hours) as overtime_hours, 
  sum(b.vacation_hours + b.pto_hours) as pto_hours,
  sum(b.holiday_hours) as holiday_hours
from sls.folks a
inner join sls.xfm_fact_clock_hours b on a.employee_number = b.employee_number
inner join dds.dim_date c on b.the_date = c.the_date
where c.year_month = (select year_month from sls.open_month)
  and b.clock_hours + b.vacation_hours + b.pto_hours + b.holiday_hours <> 0
group by c.year_month, a.employee_number;
