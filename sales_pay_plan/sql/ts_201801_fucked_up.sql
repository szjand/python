﻿1. bryn says fi too low
2. austin says he was on executive pay plan

at the end of the month, arkona report servers were fucked up
not sure about 201801 data

-- this is from fin_data_mart/sql/fact_fs/fact_fs_monthly_update_including_page_2.sql
-- generates a list of all deals for the month
drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201801 --------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201801 -------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;


-- bryn's deals
drop table if exists vehicles;
create temp table vehicles as 
select a.control, case when ssc_first_name = 'none' then b.unit_count else .5 * b.unit_count end as unit_count
from step_1 a
left join sls.deals_by_month b on a.control = b.stock_number
where b.psc_first_name = 'bryn'
  or b.ssc_first_name = 'bryn';

-- fi accounts
drop table if exists accounts;
create temp table accounts as
select distinct d.gl_account  
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201801
  and b.page = 17
  and b.line in (1,2,6,7,11,12,16,17)
inner join fin.dim_fs_account d on a.fs_Account_key = d.fs_account_key;

-- 2 deals are fucked up, 31229RB, 32063A, his fi gross was 4607.86 short
select a.control, a.unit_count, coalesce(-b.amount, 0) as amount, c.*, .16 * sum(-b.amount) over (), .16 * sum(-c.amount) over()
from vehicles a
left join (
  select a.control, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts d on c.account = d.gl_account
  where a.post_status = 'Y'
    and b.year_month = 201801
  group by a.control) b on a.control = b.control
left join (
  select control, sum(amount) as amount
  from sls.deals_accounting_detail
  where gross_category = 'fi'
  group by control) c on a.control = c.control
  
select * from sls.xfm_deals limit 10

select * from sls.tmp_fi_gross limit 100

select * from sls.tmp_consultant_stats_1 limit 100

select * from sls.deals_gross_by_month where year_month = 201801

select * from sls.deals where stock_number in ('32063a','31229rb')

select * from sls.deals_accounting_Detail limit 10

select * from sls.consultant_payroll where year_month = 201801

select * from sls.tmp_consultants

regenerate payroll for 201801

1. are all deals accounted for
yep, looks ok
select *
from step_1 a
left join sls.deals_by_month b on a.control = b.stock_number
where store = 'ry1'
  and case when a.page = 16 then a.line not in (8,10) else 1 = 1 end
  and b.stock_number is null

select * 
from sls.deals_by_month a
left join step_1 b on a.stock_number = b.control
where a.year_month = 201801  
  and b.control is null

2.
-- sls.deals_Accounting_detail
drop table if exists deals_accounting_detail;
create temp table deals_accounting_detail as
select c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
  e.description as trans_description,
  max(case
    when page = 17 then 'fi'
    else 'front'
  end) as gross_category,
  sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row = true
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join sls.deals_accounts_routes d on b.account = d.gl_account
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and c.year_month = 201801
group by b.description, c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, e.description;

--  all step 1 deals are included
-- select *
-- from step_1 a
-- left join deals_accounting_detail b on a.control = b.control
-- where store = 'ry1'
--   and case when a.page = 16 then a.line not in (8,10) else 1 = 1 end
-- and b.control is null  

3.
drop table if exists deals_gross_by_month;
create temp table deals_gross_by_month as
select year_month, control,
  sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income')
    then -amount else 0 end) as front_sales,
  sum(case when gross_category = 'front' and account_type = 'cogs'
    then amount else 0 end) as front_cogs,
  sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
  sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income')
    then -amount else 0 end) as fi_sales,
  sum(case when gross_category = 'fi' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
  sum(case when gross_category = 'fi' then -amount else 0 end) as fi_gross
from deals_accounting_detail
where year_month = 201801
group by year_month, control;

select * from deals_gross_by_month


4. consultants / payplans
drop table if exists tmp_consultants;
create temp table tmp_consultants as
select * 
from sls.tmp_consultants;
update tmp_consultants
set year_month = 201801;
update tmp_consultants
set payplan = 'executive'
where last_name = 'janzen';

5. 
drop table if exists tmp_fi_gross;
create temp table tmp_fi_gross as
select year_month, employee_number, coalesce(sum(fi_gross), 0) as fi_gross
from (
  select a.year_month, a.psc_employee_number as employee_number,
    sum(
      case
        when ssc_first_name = 'none' then b.fi_gross * a.unit_count
        else 0.5 * b.fi_gross * a.unit_count
      end) as fi_gross
  from sls.deals_by_month a
  left join deals_gross_by_month b on a.year_month = b.year_month
    and a.stock_number = b.control
  where a.year_month = 201801
  group by a.year_month, a.psc_employee_number
  union -- deals by ssc
  select a.year_month, a.ssc_employee_number,
    sum(0.5 * b.fi_gross * a.unit_count)
  from sls.deals_by_month a
  left join deals_gross_by_month b on a.year_month = b.year_month
    and a.stock_number = b.control
  where a.year_month = 201801
    and a.ssc_first_name <> 'none'
  group by a.year_month, a.ssc_employee_number) c
group by year_month, employee_number;

select * from tmp_fi_gross

6.
-- save this as a spreadsheet, whole bunch of folks need an fi adjustment
select *, 
  case
    when payplan = 'executive' then round(.16 * fi_gross, 2)
    else round(.035 * fi_gross, 2)
  end as fi_pay
from tmp_consultants a
left join tmp_fi_gross b on a.employee_number = b.employee_number           
order by last_name   


bryn fi_pay: 4316.69 - 3508.61 = 808.08

austen 
  unit pay 11*250 + 1*280 = 3030
  fi pay   2902.37
  gap etc  315
  total = select 3030 + 2902.37 + 315  = 6247.37
  paid 4309.89
  owed select 6247.37 - 4309.89 = 1937.48     

-- 2/6 which tables persist data beyond the current month and need to be updated

-- select *  from sls.deals_accounting_detail
-- insert into sls.deals_accounting_detail
drop table if exists deals_accounting_detail;
create temp table deals_accounting_detail as
select c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
  e.description as trans_description,
  max(case
    when page = 17 then 'fi'
    else 'front'
  end) as gross_category,
  sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row = true
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join sls.deals_accounts_routes d on b.account = d.gl_account
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and c.year_month = 201801
group by b.description, c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, e.description;

select * from sls.deals_Accounting_detail where control = '32453'

select *
from (
select * from sls.deals_Accounting_detail  where year_month = 201801) a
full outer join deals_accounting_detail b on a.year_month = b.year_month and a.control = b.control and a.gl_account = b.gl_Account
  and a.journal_code = b.journal_code and a.trans_description = b.trans_description
order by b.control



                      
-- select * from sls.deals_gross_by_month
-- insert into sls.deals_gross_by_month
drop table if exists deals_gross_by_month;
create temp table deals_gross_by_month as
select year_month, control,
  sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income')
    then -amount else 0 end) as front_sales,
  sum(case when gross_category = 'front' and account_type = 'cogs'
    then amount else 0 end) as front_cogs,
  sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
  sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income')
    then -amount else 0 end) as fi_sales,
  sum(case when gross_category = 'fi' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
  sum(case when gross_category = 'fi' then -amount else 0 end) as fi_gross
from deals_accounting_detail
where year_month = 201801
group by year_month, control;

select * 
from (
select * from sls.deals_gross_by_month where year_month = 201801 and control in (select control from step_1)) a
full outer join deals_gross_by_month b on a.control = b.control
where b.control in (select control from step_1)
  and (
    coalesce(a.front_gross, 0) <> coalesce(b.front_gross, 0)
    or
    coalesce(a.fi_gross, 0) <> coalesce(b.fi_gross, 0))


ok, deals_Gross_by_month is used to generate the scorecard, lets fix it
delete from sls.deals_gross_by_month where year_month = 201801;
insert into sls.deals_gross_by_month 
select * from deals_gross_by_month;


select * from sls.get_sales_scorecard_ry2(201801) order by team,last_name;


                    
select * from sls.deals_by_month

select * from sls.paid_by_month

select * from sls.clock_hours_by_month where year_month = 201801

select * from sls.consultant_payroll where year_month = 201801