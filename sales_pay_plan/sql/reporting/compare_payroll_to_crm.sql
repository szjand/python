﻿-- detail 1 row per consultant/deal
select sum(unit_count) over (partition by psc_last_name) as total, x.*
from (
select a.psc_last_name, a.stock_number, 
  case
    when ssc_last_name = 'none' then unit_count
    else 0.5 * unit_count
  end as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
  or a.ssc_employee_number = b.employee_number
where a.year_month = 201712
union
select a.ssc_last_name, a.stock_number, 
  0.5 * unit_count as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
where a.year_month = 201712
  and a.ssc_last_name <> 'none') x
order by psc_last_name, stock_number


select psc_last_name, 
  sum(unit_count) filter (where year_month = 201705) as may,
  sum(unit_count) filter (where year_month = 201706) as jun,
  sum(unit_count) filter (where year_month = 201707) as jul,
  sum(unit_count) filter (where year_month = 201708) as aug,
  sum(unit_count) filter (where year_month = 201709) as sep,
  sum(unit_count) filter (where year_month = 201710) as oct,
  sum(unit_count) filter (where year_month = 201711) as nov
from (
  select a.year_month, a.psc_last_name, a.stock_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
    or a.ssc_employee_number = b.employee_number
  where a.year_month > 201704
  union
  select a.year_month, a.ssc_last_name, a.stock_number, 
    0.5 * unit_count as unit_count
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
  where a.year_month > 201704
    and a.ssc_last_name <> 'none') x
group by psc_last_name    
order by psc_last_name

-- 12/14/17
-- for comparing to spreadsheet sent by tayor
select *,
  sum(unit_count) over (partition by year_month, psc_last_name)
from (
  select a.year_month, a.psc_last_name, a.stock_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
    or a.ssc_employee_number = b.employee_number
  where a.year_month > 201706
  union
  select a.year_month, a.ssc_last_name, a.stock_number, 
    0.5 * unit_count as unit_count
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
  where a.year_month > 201706
    and a.ssc_last_name <> 'none') x
where (
  (year_month = 201707 and psc_last_name in ('belgarde','chavez','eastman','mulhern','tarr','warmack'))
  or
  (year_month = 201708 and psc_last_name in ('belgarde','brooks','eastman'))
  or
  (year_month = 201709 and psc_last_name in ('brooks','chavez','croaker','mulhern'))
  or
  (year_month = 201710 and psc_last_name in ('croaker','flaat','foster','loven','mulhern')) 
     or
  (year_month = 201711 and psc_last_name in ('eastman','flaat','foster','schneider')))
order by psc_last_name, year_month,stock_number

  