﻿select last_name, first_name, employee_number, unit_count
-- select *
from sls.consultant_payroll
where year_month = 201705



select min(year_month)
from sls.deals
limit 10


select *
from sls.deals_by_month
limit 10

select a.last_name, a.first_name, a.employee_number 
from sls.personnel a
left join (
  select  psc_last_name, psc_first_name, psc_employee_number, sum(unit_count) as February
  from sls.deals_by_month
  where year_month = 201702
    and ssc_last_name = 'none'
  group by psc_last_name, psc_first_name, psc_employee_number) b on a.employee_number = b.psc_employee_number  


select psc_last_name, psc_first_name, 
  sum(feb) as feb, sum(mar) as mar, sum(apr) as apr, sum(may) as may,
  sum(jun) as jun, sum(jul) as jul, sum(aug) as aug, sum(sep) as sep, 
  sum(oct) as oct, sum(nov) as nov, sum(dec) as dec
from (  
  select  psc_last_name, psc_first_name, psc_employee_number, 
    coalesce(sum(unit_count) filter (where year_month = 201702), 0) as feb,
    coalesce(sum(unit_count) filter (where year_month = 201703), 0) as mar,
    coalesce(sum(unit_count) filter (where year_month = 201704), 0) as apr,
    coalesce(sum(unit_count) filter (where year_month = 201705), 0) as may,
    coalesce(sum(unit_count) filter (where year_month = 201706), 0) as jun,
    coalesce(sum(unit_count) filter (where year_month = 201707), 0) as jul,
    coalesce(sum(unit_count) filter (where year_month = 201708), 0) as aug,
    coalesce(sum(unit_count) filter (where year_month = 201709), 0) as sep,
    coalesce(sum(unit_count) filter (where year_month = 201710), 0) as oct,
    coalesce(sum(unit_count) filter (where year_month = 201711), 0) as nov,
    coalesce(sum(unit_count) filter (where year_month = 201712), 0) as dec
  from sls.deals_by_month
  where ssc_last_name = 'none'
    and store_code = 'RY1'
  group by psc_last_name, psc_first_name, psc_employee_number  
  union all
  select  psc_last_name, psc_first_name, psc_employee_number, 
    coalesce(sum(unit_count*.5) filter (where year_month = 201702), 0) as feb,
    coalesce(sum(unit_count*.5) filter (where year_month = 201703), 0) as mar,
    coalesce(sum(unit_count*.5) filter (where year_month = 201704), 0) as apr,
    coalesce(sum(unit_count*.5) filter (where year_month = 201705), 0) as may,
    coalesce(sum(unit_count*.5) filter (where year_month = 201706), 0) as jun,
    coalesce(sum(unit_count*.5) filter (where year_month = 201707), 0) as jul,
    coalesce(sum(unit_count*.5) filter (where year_month = 201708), 0) as aug,
    coalesce(sum(unit_count*.5) filter (where year_month = 201709), 0) as sep,
    coalesce(sum(unit_count*.5) filter (where year_month = 201710), 0) as oct,
    coalesce(sum(unit_count*.5) filter (where year_month = 201711), 0) as nov,
    coalesce(sum(unit_count*.5) filter (where year_month = 201712), 0) as dec
  from sls.deals_by_month
  where ssc_last_name <> 'none'
    and store_code = 'RY1'
  group by psc_last_name, psc_first_name, psc_employee_number  
  union all
  select  ssc_last_name, ssc_first_name, ssc_employee_number, 
    coalesce(sum(unit_count*.5) filter (where year_month = 201702), 0) as feb,
    coalesce(sum(unit_count*.5) filter (where year_month = 201703), 0) as mar,
    coalesce(sum(unit_count*.5) filter (where year_month = 201704), 0) as apr,
    coalesce(sum(unit_count*.5) filter (where year_month = 201705), 0) as may,
    coalesce(sum(unit_count*.5) filter (where year_month = 201706), 0) as jun,
    coalesce(sum(unit_count*.5) filter (where year_month = 201707), 0) as jul,
    coalesce(sum(unit_count*.5) filter (where year_month = 201708), 0) as aug,
    coalesce(sum(unit_count*.5) filter (where year_month = 201709), 0) as sep,
    coalesce(sum(unit_count*.5) filter (where year_month = 201710), 0) as oct,
    coalesce(sum(unit_count*.5) filter (where year_month = 201711), 0) as nov,
    coalesce(sum(unit_count*.5) filter (where year_month = 201712), 0) as dec
  from sls.deals_by_month
  where ssc_last_name <> 'none'
    and store_code = 'RY1'
  group by ssc_last_name, ssc_first_name, ssc_employee_number) x
where psc_employee_number in (
'110052',
'112589',
'12242',
'147741',
'124120',
'128530',
'15552',
'1212688',
'156884',
'145840',
'148080',
'163700',
'111232',
'171150',
'189100',
'151702',
'1566882',
'1124625',
'1130690',
'1135518',
'1147250',
'1147810')
group by psc_last_name, psc_first_name, psc_employee_number 
order by psc_last_name, psc_first_name