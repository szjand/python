﻿05/01/2018
sfe spreadsheet has a separate chev & silverado tab
the same vehice shows up on both
per Ben C, that is legit
so, need to change PK, add make

alter table sls.sfe
drop constraint sfe_pkey;

alter table sls.sfe
add column make citext;

-- this won't work because of null values in make
alter table sls.sfe
add primary key (year_month,vin,prog,make);

or, simply add the make to the sfe column
eg sfe chev, sfe silverado
yes, i like that better

alter table sls.sfe
add primary key (year_month,vin,prog);


-- as of 201805 this is the correct query, though it only includes sfe and business elinte

-- dups from deals, just distinct it
-- 201805 added journal, eliminated recon accounts, matched spreadsheet totals exactly
drop table if exists sfe_new;
create temp table sfe_new as
select distinct a.amount, a.vin, b.stock_number, c.account, prog
from sls.sfe a
left join sls.deals b on a.vin = b.vin
  and b.year_month = 201805
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and c.journal_code = 'VSN'
    and department_code in ('UC', 'NC')) c on b.stock_number = c.control  
where a.year_month = 201805;
-- check totals
select prog, sum(amount), count(*)
from sfe_new
group by prog;






-- fuck pinnacle missing a bunch of stk #s they are not all march deals
-- lets try by separate programs
--sfe
drop table if exists sfe_new;
create temp table sfe_new as
select distinct a.amount, a.vin, b.stock_number, c.account, prog
from sls.sfe a
left join sls.deals b on a.vin = b.vin
  and b.year_month = 201803
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and department_code in ('NC')) c on b.stock_number = c.control  
where a.year_month = 201803
  and prog = 'sfe'
--   and b.bopmast_id <> 44117 -- (back on)
order by a.vin

select stock_number
from (
select distinct amount, vin, stock_number, account
from sfe_new) a
group by stock_number 
having count(*) > 1

select amount, vin, stock_number, account
from sfe_new

--bus_elite
drop table if exists sfe_new;
create temp table sfe_new as
select distinct a.amount, a.vin, b.stock_number, c.account, prog
from sls.sfe a
left join sls.deals b on a.vin = b.vin
  and b.year_month = 201803
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and department_code in ('NC')) c on b.stock_number = c.control  
where a.year_month = 201803
  and prog = 'business elite'

select stock_number
from (
select distinct amount, vin, stock_number, account
from sfe_new) a
group by stock_number 
having count(*) > 1

select sum(amount) from sfe_new

select amount, vin, stock_number, account
from sfe_new

-- sfe.amount is integer, should not be
alter table sls.sfe
alter column amount TYPE numeric(8,2)

-- cpo-
drop table if exists sfe_new;
create temp table sfe_new as
select distinct a.amount, a.vin, b.stock_number, c.account, prog
from sls.sfe a
left join sls.deals b on a.vin = b.vin
  and b.year_month = 201803
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and b.description not like '%RECON%'
    and department_code in ('UC')) c on b.stock_number = c.control  
where a.year_month = 201803
  and prog = 'cpo'

select amount, vin, stock_number, account
from sfe_new 



--pinnacle
drop table if exists sfe_new;
create temp table sfe_new as
select distinct a.amount, a.vin, b.stock_number, c.account, prog
from sls.sfe a
left join sls.deals b on a.vin = b.vin
--   and b.year_month = 201803
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and department_code in ('NC')) c on b.stock_number = c.control  
where a.year_month = 201803
  and prog = 'pinnacle'

delete from sfe_new where stock_number in ('29515','29393')

select amount, vin, stock_number, account
from sfe_new 
order by vin


select vin
from (
select distinct amount, vin, stock_number, account
from sfe_new) a
group by vin 
having count(*) > 1




























-- 201802
couple of new programs added that for which jeri requires the same report: stock number & cogs account
business elite
certified pre owned
so what i am thinking is, modify the sfe table, add a program attribute

drop table if exists sfe;
create temp table sfe as
select year_month, vin, amount
from sls.sfe
group by year_month, vin, amount;
truncate sls.sfe;
insert into sls.sfe
select * from sfe;
alter table sls.sfe
add column prog citext;
update sls.sfe
set prog = 'sfe';
alter table sls.sfe
alter column prog set not null;
-- alter table sls.sfe add primary key (year_month, vin);
-- alter table sls.sfe drop constraint sfe_pkey;
alter table sls.sfe add primary key (year_month, vin, prog);


-- 201802
-- dups from deals, just distinct it
drop table if exists sfe_new;
create temp table sfe_new as
select distinct a.amount, a.vin, b.stock_number, c.account, prog
from sls.sfe a
left join sls.deals b on a.vin = b.vin
  and b.year_month = 201802
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and department_code = 'NC') c on b.stock_number = c.control  
where a.year_month = 201802
  and prog <> 'cpo'
--   and b.bopmast_id <> 44117 -- (back on)
order by a.vin

select * from sfe_new

select prog, count(*) from sfe_new group by prog

drop table if exists cpo;
create temp table cpo as
select distinct a.amount, a.vin, b.stock_number, 
  case
    when b.stock_number = '32742XX' then '164600'
    else c.account
  end as account, prog
from sls.sfe a
left join sls.deals b on a.vin = b.vin
  and b.vehicle_type_code = 'U'
--   and b.year_month = 201802 -- includes deals from first of the year
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and b.account in ('165000','164600')
    and department_code = 'UC') c on b.stock_number = c.control  
where a.year_month = 201802
  and prog = 'cpo'
--   and b.bopmast_id <> 44117 -- (back on)
order by a.vin

select * from cpo

select * from cpo where vin in (select vin from cpo group by vin having count(*) > 1)

select * from sls.deals where vin = '1G1BE5SMXH7228168'

select * from sls.sfe where vin = '1G1BE5SMXH7228168'

select * from fin.fact_gl where control = '32742XX'

select prog, count(*)
from sls.sfe
where year_month = 201802
group by prog

select distinct account from cpo

select b.the_date, c.account, a.* 
from fin.fact_gl a 
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
where a.post_status = 'Y'
  and account in (select distinct account from cpo)
  and control = '32742xx'
  





-- 201712
-- dups from deals, just distinct it
drop table if exists sfe;
create temp table sfe as
select distinct a.amount, a.vin, b.stock_number, c.account
from sls.sfe a
left join sls.deals b on a.vin = b.vin
  and b.year_month = 201712
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and department_code = 'NC') c on b.stock_number = c.control  
where a.year_month = 201712
--   and b.bopmast_id <> 44117 -- (back on)
order by a.vin

select * from sfe where vin in (
select vin from sfe group by vin having count(*) > 1)

-- 201710
select a.amount, a.vin, b.stock_number, c.account
from sls.sfe a
left join sls.deals b on a.vin = b.vin
  and b.year_month = 201710
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and department_code = 'NC') c on b.stock_number = c.control  
where a.year_month = 201710
--   and b.bopmast_id <> 44117 -- (back on)
order by a.vin


-- 201709
select a.amount, a.vin, b.stock_number, c.account
from sls.sfe a
left join sls.deals b on a.vin = b.vin
  and b.year_month = 201709
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and department_code = 'NC') c on b.stock_number = c.control  
where a.year_month = 201709
  and b.bopmast_id <> 44117 -- (back on)
order by a.vin


-- buick sfe with stocknumber, cogs account
select a.bopmast_id, a.stock_number, a.vin, a.buyer_bopname_id, b.account
from sls.deals a
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and department_code = 'NC') b on a.stock_number = b.control
where vin in
  ('KL4CJGSBXHB134008',
  'LRBFXDSA4HD239718',
  '5GAKVBKD7HJ280724',
  'KL4CJGSB6HB028901',
  '1G4ZR5SS3HU197187',
  'LRBFXDSA7HD241639',
  'KL4CJGSB4HB029691',
  'LRBFXDSA4HD241291',
  '2G4GL5EX6H9160583',
  '2G4GL5EX4H9130823')
    and stock_number <> '29840'
order by vin




select a.vin, a.stock_number, b.account
-- select *
from sls.deals a
left join (
  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and department_code = 'NC') b on a.stock_number = b.control
where vin in
(
'3G1BE6SM3HS551596',
'2GNAXSEV2J6172949',
'3GCUKSEC5HG505548',
'1GCVKREC9HZ286624',
'3GCUKREC3HG507324',
'1GNEVGKW9JJ109643',
'3GCUKREC1HG500095',
'1GCVKREC5HZ402921',
'1G1JD5SH8H4167941',
'1GC1KWEG9HF247677',
'1GC1KVEY6HF170723',
'3GCUKSECXHG511202',
'1GCVKREC9HZ397979',
'3GCUKSEC3HG502924',
'2GNAXJEV0J6170286',
'3GCUKSEC4HG311562',
'1GC1KWEG8HF165956',
'1GNEVGKW9JJ112297',
'3GCUKSEJ6HG509140',
'3GCUKSEC4HG423701',
'1GC1KWEY4HF241567',
'1GC1KWEY6HF193750',
'1GCVKREC3HZ357185',
'3GCUKSEC6HG465643',
'3GCUKREC3HG498639',
'1G1ZE5ST7HF240452',
'2GNAXSEV7J6169965',
'3GCUKREC5HG510550',
'1GC1K0EG2HF209320',
'1GCVKREC5HZ356846',
'1GCVKREC2HZ316076',
'2GNAXTEX9J6177525',
'3GCUKREC1HG297189',
'1G1BE5SM2H7188815',
'1G1BE5SM4H7177086',
'1G1ZE5ST5HF290623',
'3GCUKSEJ5HG459461',
'3GCUKSEC5HG365789',
'1GCZGHFG6H1112748',
'3GCUKREC6HG506670',
'1GNEVJKW8JJ110828',
'3GNCJPSB4HL277970',
'1G1ZE5ST2HF186087',
'3GNAXSEVXJL108847',
'1G1ZE5ST6HF291389',
'1GNSKJKC5HR239431',
'1GCGTDEN7H1198793',
'3GCUKREC7HG496621',
'1GCVKREC7HZ356427',
'1GNEVGKW1JJ110561',
'1GCGTDEN5H1185766',
'1G1ZE5ST4HF293853',
'3GCUKREC5HG243216',
'1G1ZE5STXHF230224',
'1GCVKREC5HZ392343',
'1G1ZE5ST9HF211440',
'2GNAXHEV2J6171450',
'3GCUKSEC0HG464472',
'1GNEVGKW7JJ106272',
'1G1ZE5ST4HF292993',
'1GC1KWEY2HF205411',
'3GCUKREC4HG504741',
'1GC4K0CY0HF230742',
'2GNAXSEV7J6172753',
'1GNKVHKD7HJ255072',
'3GCUKSEC5HG511558',
'1GCVKREC0HZ374381',
'2GNAXSEVXJ6163383',
'3GCUKSEC1HG497397',
'1G1BE5SM2H7252433',
'1G1ZD5ST8JF101438',
'1GCVKREC7HZ391369',
'1GNKVGKD1HJ333551',
'1GNSKCKC1HR365829',
'3GCUKREC8HG469170',
'2GNAXSEV4J6173603',
'3GNCJPSB1HL253044',
'1GCVKREC3HZ396116',
'3GNAXSEV2JL105666',
'1GCGTEEN4H1318264',
'3GNCJPSB3HL268497',
'1GC4K0CY5HF221261',
'1GNEVGKW4JJ104558',
'3GCUKSEC7HG305416',
'1GCVKREC6HZ401616',
'3GCUKSEC4HG306118',
'1GCVKREC6HZ399527',
'1GCGTDENXH1286754',
'1GNEVGKW3JJ101781',
'3GCUKSEJ8HG455744',
'3GCUKREC4HG504433',
'1G1ZE5ST8HF229198',
'1GNEVHKW0JJ102053',
'3GCUKTEC1HG482047',
'2GNAXJEV8J6164929',
'1G1BE5SM7H7169404',
'1GCVKREC0HZ402292',
'1GC1KWEY6HF239531',
'1G1ZE5ST8HF125567',
'2GNAXSEV3J6151060',
'1G1FH1R70H0134665',
'1GNEVGKW1JJ101763')
and year_month = 201708
and bopmast_id <> 43608
and seq = (
  select max(seq)
  from sls.deals
  where bopmast_id = a.bopmast_id)
order by vin 



select *
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where account_type = 'cogs'
  and department_code = 'NC'
  and control = '31321'

  select distinct control, account
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  where account_type = 'cogs'
    and post_status = 'Y'
    and department_code = 'NC'
    and control = '31321'