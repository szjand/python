﻿-- detail 1 row per consultant/deal

-----------------
-----------------
uses sls.tmp_consultants which is not being updated 
-----------------
-----------------
select sum(unit_count) over (partition by psc_last_name) as total, x.*
from (
select a.psc_last_name, a.stock_number, 
  case
    when ssc_last_name = 'none' then unit_count
    else 0.5 * unit_count
  end as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.model
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
  or a.ssc_employee_number = b.employee_number
where a.year_month = 201803
union
select a.ssc_last_name, a.stock_number, 
  0.5 * unit_count as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.model
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
where a.year_month = 201803
  and a.ssc_last_name <> 'none') x
order by psc_last_name, stock_number

select * from sls.deals_by_month where stock_number = '33046'

-- from the above query, just last name and count
select psc_last_name, sum(unit_count)
  from (
  -- detail 1 row per consultant/deal
  select sum(unit_count) over (partition by psc_last_name) as total, x.*
  from (
  select a.psc_last_name, a.stock_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count, 
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
    or a.ssc_employee_number = b.employee_number
  where a.year_month = 201711
  union
  select a.ssc_last_name, a.stock_number, 
    0.5 * unit_count as unit_count, 
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
  where a.year_month = 201711
    and a.ssc_last_name <> 'none') x) z
group by psc_last_name 
order by psc_last_name


-- backons for the month
select *
from (
select sum(unit_count) over (partition by psc_last_name) as total, x.*
  from (
  select a.psc_last_name, a.stock_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count, 
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
    or a.ssc_employee_number = b.employee_number
  where a.year_month = 201711
  union
  select a.ssc_last_name, a.stock_number, 
    0.5 * unit_count as unit_count, 
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
  where a.year_month = 201711
    and a.ssc_last_name <> 'none') x
  order by psc_last_name, stock_number) z
where unit_count = -1


-- detail 1 row per consultant/deal
-- sub total by consultant
select sum(unit_count) over (partition by psc_last_name), x.*
from (
  select a.psc_last_name, a.stock_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count, 
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
    or a.ssc_employee_number = b.employee_number
  where a.year_month = 201711
  union
  select a.ssc_last_name, a.stock_number, 
    0.5 * unit_count as unit_count, 
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
  where a.year_month = 201711
    and a.ssc_last_name <> 'none') x
order by psc_last_name, stock_number

-- summary, consultant & count
select psc_last_name, sum(unit_count)
from (
select a.psc_last_name, a.stock_number, 
  case
    when ssc_last_name = 'none' then unit_count
    else 0.5 * unit_count
  end as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
  or a.ssc_employee_number = b.employee_number
where a.year_month = 201707
union
select a.ssc_last_name, a.stock_number, 
  0.5 * unit_count as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
where a.year_month = 201707
  and a.ssc_last_name <> 'none') x
group by psc_last_name
order by psc_last_name


-- includes front/fi gross
  select a.psc_last_name, a.stock_number, 
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count, 
    case
      when ssc_last_name = 'none' then front_gross
      else 0.5 * front_gross
    end as front_gross,   
    case
      when ssc_last_name = 'none' then fi_gross
      else 0.5 * fi_gross
    end as fi_gross,   
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
    or a.ssc_employee_number = b.employee_number
  left join sls.deals_gross_by_month c on a.stock_number = c.control 
    and c.year_month = 201706
  where a.year_month = 201706
  union
  select a.ssc_last_name, a.stock_number, 
    0.5 * unit_count as unit_count, 
    case
      when ssc_last_name = 'none' then front_gross
      else 0.5 * front_gross
    end as front_gross,   
    case
      when ssc_last_name = 'none' then fi_gross
      else 0.5 * fi_gross
    end as fi_gross,   
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
  left join sls.deals_gross_by_month c on a.stock_number = c.control
    and c.year_month = 201706
  where a.year_month = 201706
    and a.ssc_last_name <> 'none'   
order by psc_last_name, stock_number



-- sfe for june
insert into sls.sfe
select 201706, a.vin, 900
from sls.deals a
left join dds.ext_inpmast b on a.vin = b.inpmast_vin
where a.year_month = 201706
  and vehicle_type_code = 'N'
  and store_code = 'ry1'
  and (model like '%SILVERADO 1%' or model like '%EQUINOX%' or model like '%MALIBU%')
  and sale_type_code <> 'W'
  and unit_count = 1;

-- 3 month avg
select psc_last_name, 
  sum(case when year_month = 201704 then unit_count else 0 end) as "201704",
  sum(case when year_month = 201705 then unit_count else 0 end) as "201705",
  sum(case when year_month = 201706 then unit_count else 0 end) as "201706",
  sum(unit_count) as total,
  round(sum(unit_count)/3, 1) as average
from (
  select a.psc_last_name, a.stock_number, a.year_month,
    case
      when ssc_last_name = 'none' then unit_count
      else 0.5 * unit_count
    end as unit_count, 
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
    or a.ssc_employee_number = b.employee_number
  where a.year_month between 201704 and 201706
  union
  select a.ssc_last_name, a.stock_number, a.year_month,
    0.5 * unit_count as unit_count, 
    a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
  from sls.deals_by_month a
  inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
  where a.year_month = 201706
    and a.ssc_last_name <> 'none') x
where psc_last_name not in ('carlson','pearson')
group by psc_last_name
order by psc_last_name

select *
from sls.consultant_payroll
where year_month = 201709
order by last_name