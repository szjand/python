﻿-- xmf_deals with more than one row
select * 
from scpp.xfm_deals a
where exists (
  select 1
  from scpp.xfm_deals
  where stock_number = a.stock_number
    and seq > 1)
order by stock_number
limit 500    

-- xfm_deals with count = -1
select * 
from scpp.xfm_deals a
where exists (
  select 1
  from scpp.xfm_deals
  where stock_number = a.stock_number
    and gl_count = -1)
order by stock_number, seq
limit 500    

-- deals with unit count = .5
select *
from scpp.deals a
where exists (
  select 1
  from scpp.deals
  where stock_number = a.stock_number
    and unit_count = .5)
order by stock_number, seq   
limit 100 

select *
from scpp.xfm_glptrns

-- this one looks wrong, deal_date should be december not february, it think
select * 
from scpp.xfm_deals a
where gl_date_change <> false

select * 
from scpp.xfm_deals a
where stock_number = '25690'

select * 
from scpp.deals a
where stock_number = '25690'

-- uh oh, H9589G, gl_count = 2 ?!?!?
select * 
from scpp.xfm_deals a
where stock_number = 'H9589G'
select * 
from scpp.deals a
where stock_number = 'H9589G'