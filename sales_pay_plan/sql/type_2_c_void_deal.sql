﻿/*

31093 sold to spicer by hanson on 6/30, unwound on 7/1 and sold to raymond by schneider
system handled it ok as type_2_c
BUT
instead of reversing the deal, the office voided it
therefore there was no reversing entry in the sale account representing the unwind
and the type_2_c row was generated with an unit_count of 1 instead of -1

*/
select *
from sls.deals
where stock_number = '31093'
order by bopmast_id, seq

select *
from sls.xfm_deals
where stock_number = '31093'
order by bopmast_id, seq

select *
from sls.deals a
where exists (
  select 1
  from sls.deals
  where bopmast_id = a.bopmast_id
    and notes = 'type_2_c')
order by bopmast_id, seq    

select *
from sls.deals
where notes = 'type_2_c'
  and unit_count = 1

  

select * 
from sls.xfm_deals
where bopmast_id in (42338,42889,42921)
order by bopmast_id, seq



select * 
from sls.ext_deals limit 10
where bopmast_id in (42338,42889,42921)
order by bopmast_id, seq




select * 
from sls.ext_accounting_deals 
where control = '31093'


-- fact_gl : new cars
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department, post_status
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
-- where a.post_status = 'Y'
where control = '31093'
  and journal_code = 'VSN'
  and account_type = 'sale'
  and department = 'new vehicle' 

update sls.  


select *
from sls.deals_by_month
where stock_number = '31093'

-- the fix

update sls.deals_by_month
set unit_count = -1
-- select * from sls.deals_by_month
where bopmast_id = 42921
  and seq = 2;


update sls.xfm_deals
set gl_count = -1
-- select * from sls.xfm_deals
where bopmast_id = 42921
  and seq = 2;

update sls.deals
set unit_count = -1
-- select * from sls.deals
where bopmast_id = 42921
  and seq = 2;  

