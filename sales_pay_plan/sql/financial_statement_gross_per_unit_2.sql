﻿/**/
--7/6/17 ----------------------------------------------------------------------------------
7/9: include page 7
-- reporting
-- fs count - sales/cogs/gross for those units || sales/cogs/gross
-- what about unwinds?
-- start with used cars retail certified P6L1
-- defined by the accounts routed to the line
-- The accounts and totals for each account: -- probably don't need totals at this stage
-- just line/accounts and account attributes
drop table if exists accounts_lines cascade;
create temp table accounts_lines as
select year_month, page, line, col, line_label, gm_account, gl_account, 
  d.store, area, d.department, sub_Department, account_type, 
  cc.description as account_description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_account cc on c.gl_account = cc.account
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
--       and d.store = 'ry1'
where ((b.page = 16 and b.line between 1 and 14)
  or (b.page between 5 and 15)
  or (page = 17 and line between 1 and 20))
  and b.year_month > 201700
order by d.store,  b.line, b.col;   
create unique index on accounts_lines(year_month, gl_account);    

-- this will be all relevant (account, date) transactions
drop table if exists test_1 cascade;
create temp table test_1 as
select b.year_month, b.the_date, a.control, a.doc, a.trans, a.seq, 
  c.account, c.description as account_description, cc.account_type, cc.gm_account,
  d.journal_code, e.description as trans_description, 
  cc.store, cc.area, cc.department, cc.sub_department, cc.page, cc.line, cc.col,
  a.amount,
  case 
    when cc.account_type = 'sale' and d.journal_code in ('VSN','VSU') then
      case
        when a.amount < 0 then 1
        else
          case
            when a.amount > 0 then -1
            else 0
          end
      end
    else 0
  end as unit_count
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_lines cc on b.year_month = cc.year_month
  and c.account = cc.gl_account
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and b.year_month > 201700
  and cc.store <> 'none'
order by store, year_month, page, line, col;

create unique index on test_1 (trans, seq);
create index on test_1(control);
create index on test_1(store);
create index on test_1(page);
create index on test_1(line);
*/

select * from test_1
-- page gross looks good
select store, year_month, page, sum(amount)
from test_1
group by store, year_month, page
order by store, year_month, page;


-- unit count looks good
-- except for page 17, don't know what is being counted
select store, year_month, page, sum(unit_count)
from test_1
group by store, year_month, page
order by store, year_month, page

-- page 17 by line, some lines show counts some don't
select store, year_month, page, line, sum(unit_count)
from test_1
where year_month = 201706
  and store = 'ry2'
  and page = 17
group by store, year_month, line, page
order by store, year_month, line, page


select * 
from accounts_lines
where page = 17
  and year_month = 201706
  and store = 'ry1'

select *
from (
    select *
    from test_1
    where page = 17
      and year_month = 201706
      and store = 'ry1'
      and account_type = 'other income'
      and journal_code = 'VSN') x
left join (
  select distinct stock_number, year_month
  from sls.deals z
  where year_month = 201706
  group by stock_number, year_month
    ) xx on x.control = xx.stock_number
  
    order by control


select x.control, x.unit_count, x.line, xx.*
from (
  select *
  from test_1
  where page = 5
    and year_month = 201706
    and store = 'ry1'
    and line between 1 and 20
    and unit_count <> 0) x
left join (
select *
from test_1
where page = 17
  and year_month = 201706
  and store = 'ry1') xx on x.control = xx.control    
order by x.control, x.line


select line, sum(unit_count)
from test_1
where year_month = 201701
  and store = 'ry1'
  and page = 16  
group by line
order by line  


select line, sum(amount)
from test_1
where year_month = 201701
  and store = 'ry1'
  and page = 16
group by line
order by line


select *
from test_1
where year_month = 201702
  and store = 'ry1'
  and page = 16
  and line = 1
order by control


select account, sum(amount)
from test_1
where year_month = 201702
  and store = 'ry1'
  and page = 16
  and line = 1
group by account
order by account

select control, sum(amount)
from test_1
where year_month = 201702
  and store = 'ry1'
  and page = 16
  and line = 1
  and account = '164700'
group by control
order by control


select *
from test_1
where year_month = 201702
  and control = '29743'


select account_type, sum(amount)
from test_1
where year_month = 201702
  and control = '29743'
group by account_type


select account_type, sum(amount) from (
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '29743' order by account_type, account
--   and journal_code = 'VSN'
--   and account_type = 'sale'
--   and department = 'new vehicle' 
) x group by account_type


select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
-- select sum(amount)
select account_type,
  sum(case when the_Date <= '02/17/2017' then amount else 0 end) as at_sale,
  sum(case when the_Date > '02/17/2017' then amount else 0 end) as post_sale
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '29757' 
  and account_type in ('cogs','expense','sale','other income')
group by account_type  


-- this matches the gross on the fs
select line, sum(amount)
from test_1
where year_month = 201701
and page = 9
group by line


select *
-- select sum(amount) -- this matches fs gross and recap house gross to the penny
from test_1
where year_month = 201701
and page = 9
and control = '30259'

-- this matches house gross to the penny too
select *
-- select sum(amount) -- this matches fs gross and recap house gross to the penny
from test_1
where year_month = 201702
-- and page = 9
and control = '29757'


select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
-- select sum(amount)
-- select account_type,
--   sum(case when the_Date <= '02/17/2017' then amount else 0 end) as at_sale,
--   sum(case when the_Date > '02/17/2017' then amount else 0 end) as post_sale
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '30259' 
  and account_type in ('cogs','expense','sale','other income')
-- group by account_type  