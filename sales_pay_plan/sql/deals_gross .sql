﻿select aa.store_code, aa.bopmast_id, aa.seq, bb.*
from sls.deals aa
left join (
  select control, sum(-1 * a.amount) as front_gross
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- new car accounts
    select distinct d.gl_account
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201703
      and (
        (b.page between 5 and 15) or -- new cars
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
  where a.post_status = 'Y'
    and b.year_month > 201703
--     and c.store_code = 'RY1'
  group by control) bb on aa.stock_number = bb.control


-- unwind on a previous month deal with a negative gross yields an increase in gross: 29951R
-- leave out the seq
-- there was $825 service done on 28128XXZ in may, why is it not showing up

-- ok, so i am mind fucking on the fact that accounting info is only available at the stocknumber level,
-- no way to associate with the actual deal (deal 456 on 123A, it uwninds, sold on deal 518)
-- specifically the issue with this query is  well illustrated by 28128XXZ, deal in april, accounting
-- from april , but then the accounting in may with no corresponding row from deals in may
-- so if i back up and remove the year_month from the join conditions, what happens
-- 1. 1209 rows ( as opposed to 999)
-- 5/15 this is close to where i left off on gross, pick up from here below
drop table if exists wtf;
create temp table wtf as
select aa.store_code, aa.bopmast_id, aa.stock_number, aa.unit_count, aa.year_month as deal_year_month,
  bb.year_month as acctg_year_month, bb.control, bb.sales as sales, bb.cogs as cogs, 
  bb.front_gross as front_gross
from (
  select store_code, bopmast_id, stock_number, year_month, sum(unit_count) as unit_count
  from sls.deals
  group by store_code, bopmast_id, stock_number, year_month) aa
left outer join ( -- bb
  select a.control, b.year_month,  
    sum(case when account_type in ('other income','sale') then -1 * amount else 0 end) as sales,
    sum(case when account_type = 'cogs' then amount else 0 end) as cogs, 
    sum(-1 * a.amount) as front_gross
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- f, variable accounts
    select distinct d.gl_account
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
--       and b.year_month = _last_month 
      and (
        (b.page between 5 and 15) or -- new cars
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
  where a.post_status = 'Y'
    and b.year_month > 201702
  group by a.control, b.year_month) bb on aa.stock_number = bb.control -- and aa.year_month = bb.year_month
where bb.front_gross <> 0
--   and aa.unit_count <> 0
order by stock_number;


selecT * from wtf where stock_number = '28128XXZ' or control = '28128XXZ'

selecT * from wtf where year_month = 201705 order by stock_number

selecT * from wtf where year_month = 201704 and bb_year_month > year_month

select * from wtf where deal_year_month < acctg_year_month order by bopmast_id

-- 5/12 my problem: 30908 in wtf

select * from wtf where stock_number = '30508'

select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '30508' order by the_date
  and journal_code = 'VSU'
  and account_type = 'sale'
  and department = 'used vehicle'  


   select a.control, b.year_month,  
    sum(case when account_type in ('other income','sale') then -1 * amount else 0 end) as sales,
    sum(case when account_type = 'cogs' then amount else 0 end) as cogs, 
    sum(-1 * a.amount) as front_gross
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- f, variable accounts
    select distinct d.gl_account
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
--       and b.year_month = _last_month 
      and (
        (b.page between 5 and 15) or -- new cars
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
  where a.post_status = 'Y'
    and b.year_month > 201702
    and a.control = '28128XXZ'
  group by a.control, b.year_month







-- compare accounts derived from dim_account(dept,account type) vs fs (page, line)
-- tend to prefer going with the statement, but if there is no activity in a month, 
-- then that account will not show up, eg, MV1 sales/cogs
select *
from (
  select distinct account
  from fin.dim_account 
  where department_code in ('nc','uc')
    and account_type in ('sale','cogs','other income')) a
full outer join (
  select distinct d.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201704
    and (
      (b.page between 5 and 15) or -- new cars
      (b.page = 16 and b.line between 1 and 14)) -- used cars
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) b on a.account = b.gl_account
where a.account is null
  or b.gl_account is null  
 


-- 5/15 ------------------------------------------------------------------------------------------------------------------------------
ok fucker, what are you going to use for the relevant accounts
i think this is the way to go

-- well, 244300 blows this, 
-- 244300 was routed to page 17 line 5 in 2011 only, subsequently routed to line 6
-- so this approach included both, which is wrong
any account ever routed to a relevant fs line
rather than trying to pin down  which accounts in which month
    select distinct d.gl_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
    and (
      (b.page between 5 and 15) or -- new cars
      (b.page = 16 and b.line between 1 and 14) or)-- used cars
      (b.page = 17 and b.line between 1 and 20)) -- fi
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key

how to break out of this being circular
can not know what accounts are used in the current month statement because the statement is not done yet
use previous month statement to determine which acccounts to use but that may or may not be accurate
the big exception that keeps popping up is MV1

ok, go back to sypffxmst (page/line/gm_account) & ffpxrefdta (gl_account)

select distinct b.g_l_acct_number
select case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge, a.fxmlne::integer, b.g_l_acct_number
from arkona_fdw.ext_Eisglobal_sypffxmst a 
inner join arkona_fdw.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2017
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    (a.fxmpge = 17 and a.fxmlne between 1 and 20))


-- 5/15 this is close to where i left off on gross, pick up from here below
-- from wtf above
-- include journal

-- rather than categorizing accounting transactions by department (fin.dim_account)
-- categorize by financial statement page/line

select a.control, b.year_month,  
  sum(case when account_type in ('other income','sale') then -1 * amount else 0 end) as sales,
  sum(case when account_type = 'cogs' then amount else 0 end) as cogs, 
  sum(-1 * a.amount) as front_gross
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- f, variable accounts
  select distinct d.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
--       and b.year_month = _last_month 
    and (
      (b.page between 5 and 15) or -- new cars
      (b.page = 16 and b.line between 1 and 14) or)-- used cars
      (b.page = 17 and b.line between 1 and 20)) -- fi
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
where a.post_status = 'Y'
  and b.year_month > 201702
group by a.control, b.year_month

-- ok, back up, ungroup
-- fs info 
drop table if exists fs_info;
-- 1 row per account/page/line
create temp table fs_info as -- 1013 rows
-- select f.store, b.page, b.line, d.gl_account
-- from fin.fact_fs a
-- inner join fin.dim_fs b on a.fs_key = b.fs_key
--     and (
--       (b.page between 5 and 15) or -- new cars
--       (b.page = 16 and b.line between 1 and 14) or -- used cars
--       (b.page = 17 and b.line between 1 and 20)) -- fi
-- inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- inner join fin.dim_account e on d.gl_account = e.account
-- inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
-- group by f.store, b.page, b.line, d.gl_account;

select case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number as gl_account
from arkona_fdw.ext_Eisglobal_sypffxmst a 
inner join arkona_fdw.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2017
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    (a.fxmpge = 17 and a.fxmlne in (1,2,6,7,11,12,16,17)));

select page, line, gl_account from fs_info group by page, line, gl_account having count(*) > 1


drop table if exists bunch_o_data;
create temp table bunch_o_data as
select b.description as account_description, c.year_month, a.control,
  a.amount, aa.journal_code, b.account_type, b.department,
  d.*
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row = true
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join fs_info d on b.account = d.gl_account
-- inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and c.year_month > 201702;

select * from fin.dim_Account where account = '180601'

-- from fs by line/account matches statement
select c.store, b.year_month, line, d.gl_account, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201704
  and b.page = 17 and b.line between 1 and 20
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
group by c.store, b.year_month, line, d.gl_account
having sum(amount) <> 0
order by c.store, line

but bunch_o_data does not

select store_code, page, line, gl_account, sum(amount)
from bunch_o_data
where year_month = 201704
  and page = 17
group by store_code, page, line, gl_account
order by store_code, page, line, gl_account

select * from fs_info where page = 17 order by gl_account

select * from bunch_o_data where year_month = 201704 and page = 17 order by store_Code, line, gl_account

select * from fin.dim_Account where account = '180601'

select * from fin.dim_fs_account where gl_account = '180601'

select *
from fin.fact_gl a
inner join fin.dim_Account b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where b.account = '180601'
  and c.year_month = 201704


-- 5/15 ------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
-- 5/16 ------------------------------------------------------------------------------------------------------------------------------

so, where was i, struggling with why 180601 (page 17 line 1) does not show up in gross query
is it an issue with on this local machine/db processing fin.dim_Account as truely type 2, whereas in production i have 
been manually doing type 1 updates
does the bunch_o_data query need to not just where current_row = true but trans date between fromdate and thrudate?
yep, that worked

now, check the rest
ry1 looks good until lines 16/17, on the statement there is no value for line 16, but the statement line 17 = the quer 16 + 17
ry2 looks ok

select store_code, page, line, sum(amount)
from bunch_o_data
where year_month = 201704
  and page = 17
group by store_code, page, line
order by store_code, page, line

-- check fs by line: shows the same thing, so, at this point in the game, i declare no issue
select c.store, b.year_month, line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201704
  and b.page = 17 and b.line between 1 and 20
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
group by c.store, b.year_month, line
having sum(amount) <> 0
order by c.store, line

-- check out page 6, good
select store_code, page, line, sum(amount)
from bunch_o_data
where year_month = 201704
  and page = 16
group by store_code, page, line
order by store_code, page, line

-- and new cars (just page) good
select store_code, page, sum(amount)
from bunch_o_data
where year_month = 201704
  and page not in (16,17)
group by store_code, page
order by store_code, page


detail table for gross
exclude f/i comp and chargeback (line in (1,2,6,7,11,12,16,17))



this table is used for generating unit_count and processing deal changes (back ons, etc)
select * from sls.ext_accounting_deals limit 100

what i need now is sales/cogs/gross from accounting/fs for payroll by deal
or rather in the case of accounting, by control

select * from bunch_o_data limit 100

select * from sls.deals_by_month limit 100

-- add category (front/fs)
select year_month, store_code, control, journal_code, gl_account, 
  account_type, 
  max(case 
    when page = 17 then 'fi_gross'
    else 'front_gross'
  end) as category
from bunch_o_data
group by year_month, store_code, control, journal_code, gl_account, account_type

select a.stock_number, primary_sc, secondary_sc, fi_manager, b.*
from sls.deals_by_month a
left join (
  select year_month, store_code, control, journal_code, gl_account, 
    account_type, 
    max(case 
      when page = 17 then 'fi_gross'
      else 'front_gross'
    end) as category, sum(amount) as amount
  from bunch_o_data
  group by year_month, store_code, control, journal_code, gl_account, account_type) b on a.year_month = b.year_month
  and a.stock_number = b.control
where a.year_month = 201704 and stock_number = '30454a' order by category, amount
order by gl_account


select stock_number, category, 
select a.stock_number, primary_sc, secondary_sc, fi_manager, b.*
from sls.deals_by_month a
left join (
  select year_month, store_code, control, journal_code, gl_account, 
    account_type, 
    max(case 
      when page = 17 then 'fi_gross'
      else 'front_gross'
    end) as category, sum(amount)
  from bunch_o_data
  group by year_month, store_code, control, journal_code, gl_account, account_type) b on a.year_month = b.year_month
  and a.stock_number = b.control
where a.year_month = 201704
order by a.stock_number

-- 5/16 ------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
-- 5/17 ------------------------------------------------------------------------------------------------------------------------------
drop table if exists fs_info;
-- 1 row per account/page/line
create temp table fs_info as -- 1013 rows
select case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number as gl_account
from arkona_fdw.ext_Eisglobal_sypffxmst a 
inner join arkona_fdw.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2017
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    -- exclude fin comp & chargebacks
    (a.fxmpge = 17 and a.fxmlne in (1,2,6,7,11,12,16,17)));

drop table if exists bunch_o_data;
create temp table bunch_o_data as
select b.description as account_description, c.year_month, a.control,
  a.amount, aa.journal_code, b.account_type, b.department,
  d.*, e.description as trans_description
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row = true
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join fs_info d on b.account = d.gl_account
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and c.year_month > 201702;

select * from bunch_o_data limit 100


select a.stock_number, primary_sc, secondary_sc, fi_manager, b.*
from sls.deals_by_month a
left join (
  select year_month, store_code, control, journal_code, gl_account, 
    account_type, 
    max(case 
      when page = 17 then 'fi_gross'
      else 'front_gross'
    end) as category, sum(amount)
  from bunch_o_data
  group by year_month, store_code, control, journal_code, gl_account, account_type) b on a.stock_number = b.control
where a.year_month = 201704
order by a.stock_number    


what i want to see is accounting entries in the month prior to the sale month

select *
from sls.deals_by_month
where stock_number in  (
select stock_number
from sls.deals_by_month
group by stock_number
having count(*) > 1)
order by stock_number

-- these are the stock numbers with multiple deals
select stock_number
from (
select stock_number, bopmast_id
from sls.deals_by_month
group by stock_number, bopmast_id) x
group by stock_number
having count(*) > 1

select * 
from sls.deals_by_month
where stock_number in ('30157A','29951R','30994A')
order by stock_number

-- yikes, this is what i think i am afraid of, cartesian, no way to relate an accounting transaction to  a 
-- deal, just to a stock_number
-- need to look at and possibly consider the gl_description and buyer name for those rare (?) occassions where
-- a stocknumber is sold/unound/sold in the same month
-- while this is interesting, feels like i am going down a rabbit hole
select a.year_month as deal_year_month, a.stock_number, a.bopmast_id, primary_sc, secondary_sc, fi_manager, buyer,
  b.*
from sls.deals_by_month a
left join (
  select year_month, store_code, control, journal_code, gl_account, 
    account_type, trans_description,
    max(case 
      when page = 17 then 'fi_gross'
      else 'front_gross'
    end) as category, sum(amount)
  from bunch_o_data
  group by year_month, store_code, control, journal_code, gl_account, account_type, trans_description) b on a.stock_number = b.control and a.buyer = b.trans_description
where stock_number in ('30157A','29951R','30994A')
order by a.stock_number, a.year_month, a.bopmast_id

ok, already have the buyer in deals_by_month
select * from sls.deals_by_month  limit 10

select * from bunch_o_data limit 10


-- back the fuck up, what am i trying to accomplish
generally, i am trying to create the daily payroll snapshot
and this part is just the gross



select a.stock_number, primary_sc, secondary_sc, fi_manager, b.*
from sls.deals_by_month a
left join (
  select year_month, store_code, control, journal_code, gl_account, 
    account_type, 
    max(case 
      when page = 17 then 'fi_gross'
      else 'front_gross'
    end) as category, sum(amount)
  from bunch_o_data
  group by year_month, store_code, control, journal_code, gl_account, account_type) b on a.stock_number = b.control
where a.year_month = 201704
order by a.stock_number    



select a.year_month as deal_month, a.stock_number, primary_sc, secondary_sc, fi_manager, a.buyer, b.year_month as acct_month,
  b.store_code, 
  sum(case when category = 'front_gross' and account_type in ('sale', 'Other Income') then -amount else 0 end) as front_sales,
  sum(case when category = 'front_gross' and account_type = 'cogs' then amount else 0 end) as front_cogs,
  sum(case when category = 'front_gross' then -amount else 0 end) as front_gross,
  sum(case when category = 'fi_gross' and account_type in ('sale', 'Other Income') then -amount else 0 end) as fi_sales,
  sum(case when category = 'fi_gross' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
  sum(case when category = 'fi_gross' then -amount else 0 end) as fi_gross,  
  sum(-b.amount) as total_gross
from sls.deals_by_month a
left join (
  select year_month, store_code, control, journal_code, gl_account, 
    account_type, 
    max(case 
      when page = 17 then 'fi_gross'
      else 'front_gross'
    end) as category, sum(amount) as amount
  from bunch_o_data
  group by year_month, store_code, control, journal_code, gl_account, account_type) b on a.stock_number = b.control
where a.year_month = 201704
group by a.year_month, a.stock_number, primary_sc, secondary_sc, fi_manager, b.year_month, b.store_code, a.buyer
order by a.stock_number    



29162a looks goofy

why does 29162a show the recapping on 5/8 in sls.deals in either production or local
oh shit, 
-- compare xfm to deals month/count
select b.*, c.*, (select distinct stock_number from sls.deals where bopmast_id = c.bopmast_id)
from (
  select bopmast_id, year_month, sum(gl_count) as unit_count
  from (
    select distinct bopmast_id, (extract(year from gl_Date) * 100) + extract(month from gl_date) as year_month, gl_count
    from sls.xfm_deals) a
  group by bopmast_id, year_month) b
full outer join (
  select bopmast_id, year_month, sum(unit_count)::integer as unit_count
  from sls.deals
  group by bopmast_id, year_month) c on b.bopmast_id = c.bopmast_id and b.year_month = c.year_month
where b.unit_count <> c.unit_count

-- ok, why the diffs
select 'deals' as source,run_date, bopmast_id, seq, deal_status_code, stock_number, unit_count, gl_date, notes
from sls.deals
where stock_number = '30342XX'
union all
select 'xfm',run_date, bopmast_id, seq, deal_status, stock_number, gl_count, gl_date, null::citext
from sls.xfm_deals
where stock_number = '30342XX'
order by source, seq

select 'deals' as source,run_date, bopmast_id, seq, deal_status_code, stock_number, unit_count, gl_date, notes
from sls.deals
where bopmast_id = '40885'
union all
select 'xfm',run_date, bopmast_id, seq, deal_status, stock_number, gl_count, gl_date, null::citext
from sls.xfm_deals
where bopmast_id = '40885'
order by source, seq

some were leftovers from when i initially did not update year month with gl_date change, fixed them (local & production)
h9734: reset to accepted on 4/10, still in accepted

select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, c.department, c.account_type
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '29951R'
  and journal_code = 'VSN'
  and account_type = 'sale'
  and department = 'new vehicle' 

29951R
  accounting:
    4/29 sold - ramos
    5/2  unwound - ramos
    5/10 sold - lemar
shows as a 0 in xfm because there is a row in xfm for lemar on 5/10 with deal status A and unit-count = -1
do not know how i could have prevented this or even if i should, this is an example of an accounting row with
only a resolution of stocknumber being applied to a different deal

-- 5/17 ------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
-- 5/18 ------------------------------------------------------------------------------------------------------------------------------
the last one to figure out is 29162a
ahh, did not get picked up for update because the script generating sls.changed_deals excludes rows from sls.deals
where deal_status = deleted, so, this is strange, the data seems to imply that on run date 5/3 the deal was deleted,
but then on 5/8 was reinstituted, which seems weird, bopmast_id does not change, 

-- any other eveidence of a deleted deal resuscitating?
-- to the possible likely extent that this query would show any resuscitated deleted deals, 29162A is the only one (41887)
select *
from (
  select bopmast_id, max(seq) as max_seq
  from sls.xfm_deals
  group by bopmast_id) a
inner join (
  select bopmast_id, seq
  from sls.xfm_deals
  where deal_status = 'deleted') b on a.bopmast_id = b.bopmast_id and a.max_seq <> b.seq


so i am going to assume that it is just a weird anomaly (test for it in the future) or somehow it was processed
incorrectly and should have been a type_2_a or _b change.
i will edit the record  

-- and this takes care of it, after running these 2 updates, sls.changed_deals flagged it as a type_2_f
update sls.xfm_deals
set deal_status = 'A'
where bopmast_id = 41887 
  and seq = 2

update sls.deals
set deal_status_code = 'A',
    deal_status = 'accepted',
    notes = 'type_2_b'
where bopmast_id = 41887 
  and seq = 2


-- great, now where was i
-- i think this looks pretty good, what all do i need
-- do i need account description? had to leave that out of grouping
-- i find myself wanting more information on accounting entries that are not in the same month as the deal
-- 30509 sold in 201704, in 201705 front cogs 225, for what? should it be included in 201705 payroll calc ?
select a.year_month as deal_month, a.stock_number, primary_sc, secondary_sc, fi_manager, a.buyer, b.year_month as acct_month,
  b.store_code, 
  sum(case when category = 'front_gross' and account_type in ('sale', 'Other Income') then -amount else 0 end) as front_sales,
  sum(case when category = 'front_gross' and account_type = 'cogs' then amount else 0 end) as front_cogs,
  sum(case when category = 'front_gross' then -amount else 0 end) as front_gross,
  sum(case when category = 'fi_gross' and account_type in ('sale', 'Other Income') then -amount else 0 end) as fi_sales,
  sum(case when category = 'fi_gross' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
  sum(case when category = 'fi_gross' then -amount else 0 end) as fi_gross,  
  sum(-b.amount) as total_gross
from sls.deals_by_month a
left join (
  select year_month, store_code, control, journal_code, gl_account, 
    account_type, 
    max(case 
      when page = 17 then 'fi_gross'
      else 'front_gross'
    end) as category, sum(amount) as amount
  from bunch_o_data
  group by year_month, store_code, control, journal_code, gl_account, account_type) b on a.stock_number = b.control
where a.year_month = 201704
group by a.year_month, a.stock_number, primary_sc, secondary_sc, fi_manager, b.year_month, b.store_code, a.buyer
order by a.stock_number    


select * from bunch_o_data where control = '30509'

ok, maybe some premature combining going on here, yes, will be combining deals and gross, but first, need to create
populate and maintain  the damned gross table

select year_month, store_code, control, journal_code, gl_account, 
  account_type, 
  max(case 
    when page = 17 then 'fi'
    else 'front'
  end) as gross_category, sum(amount) as amount
from bunch_o_data
group by year_month, store_code, control, journal_code, gl_account, account_type


drop table if exists fs_info;
-- 1 row per account/page/line
-- this gives me all(?) the relevant accounts and the page/line
create temp table fs_info as -- 1013 rows
select case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number as gl_account
from arkona_fdw.ext_Eisglobal_sypffxmst a 
inner join arkona_fdw.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2017
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    -- exclude fin comp & chargebacks
    (a.fxmpge = 17 and a.fxmlne in (1,2,6,7,11,12,16,17)));

select page,line,gl_account from fs_info group by page,line,gl_account having count(*) > 1

drop table if exists bunch_o_data;
create temp table bunch_o_data as
select b.description as account_description, c.year_month, a.control,
  a.amount, aa.journal_code, b.account_type, b.department,
  d.*, e.description as trans_description
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row = true
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join fs_info d on b.account = d.gl_account
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and c.year_month > 201702;

select year_month, control, gl_account, journal_code, sum(amount) from bunch_o_data group by year_month, control, gl_account, journal_code having count(*) > 1

select * from bunch_o_data where year_month = 201704 and control = '29622C' and gl_account = '164601' and journal_code = 'vsu'

-- combine fs_info & bunch_o_data
-- monthly account detail
-- 1 row per year_month/control/account/journal/trans_description
drop table if exists  sls.deals_accounting_detail;
create table sls.deals_accounting_detail (
  year_month integer not null,
  control citext not null,
  journal_code citext not null,
  account_type citext not null,
  department citext not null,
  store_code citext not null,
  page integer not null,
  line integer not null,
  gl_account citext not null,
  account_description citext not null,
  trans_description citext not null,
  gross_category citext not null,
  amount numeric(12,2) not null,
  constraint deals_accounting_detail_pkey primary key(year_month,control,gl_account,journal_code,trans_description));

drop table if exists  sls.deals_accounting_detail_tmp;
create table sls.deals_accounting_detail_tmp (
  year_month integer not null,
  control citext not null,
  journal_code citext not null,
  account_type citext not null,
  department citext not null,
  store_code citext not null,
  page integer not null,
  line integer not null,
  gl_account citext not null,
  account_description citext not null,
  trans_description citext not null,
  gross_category citext not null,
  amount numeric(12,2) not null,
  constraint deals_accounting_detail_tmp_pkey primary key(year_month,control,gl_account,journal_code,trans_description));  
  
-- drop table if exists wtf;
-- create temp table wtf as
insert into sls.deals_accounting_detail_tmp
select c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, b.description as account_description, 
  e.description as trans_description, 
  max(case 
    when page = 17 then 'fi'
    else 'front'
  end) as gross_category,
  sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row = true
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join ( -- 1 row per account/page/line, this gives me all(?) the relevant accounts and the page/line
  select case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
    a.fxmpge as page, a.fxmlne::integer as line, b.g_l_acct_number as gl_account
  from arkona_fdw.ext_Eisglobal_sypffxmst a 
  inner join arkona_fdw.ext_ffpxrefdta b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
  where a.fxmcyy = 2017
    and (
      (a.fxmpge between 5 and 15) or
      (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
      -- exclude fin comp & chargebacks
      (a.fxmpge = 17 and a.fxmlne in (1,2,6,7,11,12,16,17)))) d on b.account = d.gl_account
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and c.year_month > 201702
group by b.description, c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, e.description;

drop table if exists sls.deals_gross_by_month;
create table sls.deals_gross_by_month (
  year_month integer not null,
  control citext not null,
  front_sales numeric (8,2) not null,
  front_cogs numeric (8,2) not null,
  front_gross numeric (8,2) not null,
  fi_sales numeric (8,2) not null,
  fi_cogs numeric (8,2) not null,
  fi_gross numeric (8,2) not null,
  constraint deals_gross_by_month_pkey primary key (year_month,control));
  
insert into sls.deals_gross_by_month
select year_month, control,
  sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income') then -amount else 0 end) as front_sales,
  sum(case when gross_category = 'front' and account_type = 'cogs' then amount else 0 end) as front_cogs,
  sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
  sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income') then -amount else 0 end) as fi_sales,
  sum(case when gross_category = 'fi' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
  sum(case when gross_category = 'fi' then -amount else 0 end) as fi_gross
from sls.deals_accounting_detail
group by year_month, control;

select a.year_month, a.control, a.front_Gross, a.fi_gross 
from sls.deals_gross_by_month a
inner join (
  select control
  from sls.deals_gross_by_month
  group by control
  having count(*) > 2) b on a.control = b.control
order by a.control, a.year_month  

select *
from sls.deals_accounting_detail
where control = '30956'
order by year_month

-- 5/18 ------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
-- 5/21 ------------------------------------------------------------------------------------------------------------------------------
-- time to get daily updates into luigi.  just like deals_by_month, this needs to be updated daily 
-- use the _tmp table  pattern

-- need the accounts/routes info from arkona
create table sls.deals_accounts_routes (
  store_code citext not null,
  page integer not null,
  line integer not null,
  gl_account citext not null,
  constraint deals_accounts_routes_pkey primary key (page,line,gl_account));

create index on sls.deals_accounts_routes(gl_account);  

insert into sls.deals_accounts_routes
select case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
  a.fxmpge as the_page, a.fxmlne::integer as line, trim(b.g_l_acct_number) as gl_account
from arkona_fdw.ext_eisglobal_sypffxmst a
inner join arkona_fdw.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy = 2017
  and trim(a.fxmcde) = 'GM'
  and b. consolidation_grp <> '3'
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
  and b.company_number = 'RY1'
  and b.g_l_acct_number <> ''
  and (
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    -- exclude fin comp & chargebacks
    (a.fxmpge = 17 and a.fxmlne in (1,2,6,7,11,12,16,17)));

-- drop table sls.deals_accounting_detail_tmp cascade;
-- truncate sls.deals_accounting_detail_tmp;
-- insert into sls.deals_accounting_detail_tmp
-- select c.year_month, a.control,
--   aa.journal_code, b.account_type, b.department,
--   d.store_code, d.page, d.line, d.gl_Account, b.description as account_description, 
--   e.description as trans_description, 
--   max(case 
--     when page = 17 then 'fi'
--     else 'front'
--   end) as gross_category,
--   sum(a.amount) as amount
-- from fin.fact_gl a
-- inner join fin.dim_journal aa on a.journal_key = aa.journal_key
-- inner join dds.dim_date c on a.date_key = c.date_key
-- inner join fin.dim_account b on a.account_key = b.account_key
-- --   and b.current_row = true
--   and c.the_date between b.row_from_Date and b.row_thru_date
-- inner join sls.deals_accounts_routes d on b.account = d.gl_account
-- inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
-- where post_status = 'Y'
--   and c.year_month > 201702
-- group by b.description, c.year_month, a.control,
--   aa.journal_code, b.account_type, b.department,
--   d.store_code, d.page, d.line, d.gl_Account, e.description;


-- changed rows
 ------------------
??????????????????????????????????????????????????????????????????
 uh oh what about voids

 maybe, just replace all rows for the current open month rather than trying
 to update

 there is no good way to update the aggregate table (deals_accounting_detail)
 based on the post_status of individual transactions short of regenerating
 the entire table on a nightly basis
 i do not think that is necessary

 i am liking the notion delete and replace open month only, that guarantees
 accuracy for as long as the month is open
 do not forget: the mission is payroll, that can be generated daily and that does
 not change once the payroll has been issued

 so
 nightly
 truncate and populate _tmp with open month data
 do not even need tmp

-- this is it 
delete 
from sls.deals_accounting_detail
where year_month = (select year_month from sls.open_month);

insert into sls.deals_accounting_detail
select c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, b.description as account_description, 
  e.description as trans_description, 
  max(case 
    when page = 17 then 'fi'
    else 'front'
  end) as gross_category,
  sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row = true
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join sls.deals_accounts_routes d on b.account = d.gl_account
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and c.year_month = (select year_month from sls.open_month)
group by b.description, c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, e.description;


-- and deals_gross_by_month
delete 
from sls.deals_gross_by_month 
where year_month = (select year_month from sls.open_month)


insert into sls.deals_gross_by_month
select year_month, control,
  sum(case when gross_category = 'front' and account_type in ('sale', 'Other Income') then -amount else 0 end) as front_sales,
  sum(case when gross_category = 'front' and account_type = 'cogs' then amount else 0 end) as front_cogs,
  sum(case when gross_category = 'front' then -amount else 0 end) as front_gross,
  sum(case when gross_category = 'fi' and account_type in ('sale', 'Other Income') then -amount else 0 end) as fi_sales,
  sum(case when gross_category = 'fi' and account_type = 'cogs' then amount else 0 end) as fi_cogs,
  sum(case when gross_category = 'fi' then -amount else 0 end) as fi_gross
from sls.deals_accounting_detail
where year_month = (select year_month from sls.open_month)
group by year_month, control;

-- 5/21 ------------------------------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------------------------------
-- issues
--------------------------------------------------------------------------------------------------------------------------------------
-- what about 29490, february deal, account 1629012 in feb $700 GJE: GMC VGB FEBRUARY, in april $771.43 GJE: SIERRA VOLUME BONUS FEB
-- my feeling at this time is that there is no sane way to accomodate for entries made in the current month on deals 
-- from previous months, whether additional gross or a hit


-- 30575
-- april payroll front_gross: -1100.29  sfe: 600
-- accounting: on 4/30 additional 911.11 COGS, acct 1634001, journal GJE, comment: GMC VOLUME BONUS APR
-- so today front gross is 189.18
-- why is the amount different: 600 vs 911.11
-- why is the comment GMC on a fucking equinox

-- paid sfe (from ben) vs accounted sfe
select distinct control, c.description, -a.amount, e.description, f.amount as sfe
-- select sum(a.amount), sum(f.amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
inner join (
    select a.stock_number, b.amount
    from sls.deals a
    inner join  sls.sfe b on a.vin = b.vin) f on a.control = f.stock_number
where a.post_status = 'Y'
  and journal_code in ('GJE')
  and account_type = 'cogs'
  and department = 'new vehicle' 
order by control  


-- 28128XXZ
--     sold in april, front gross: 1283.32
--     payroll generated on sunday, midday 4/30
--     on 4/30 @ 7 pm, michelle made correcting entries (C/E) to the vehicle amounting to 261.59 additional cogs
--     so if we look at front gross on that vehicle now (from accounting) we see 1021.73
--------------------------------------------------------------------------------------------------------------------------------------
-- issues
--------------------------------------------------------------------------------------------------------------------------------------