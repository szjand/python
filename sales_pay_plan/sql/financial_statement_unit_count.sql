﻿drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from ( -- h
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201611
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201611
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
    where a.post_status = 'Y' order by control) h
group by store, page, line, line_label, control
order by store, page, line;

select *
from step_1;


-- ok, count is still good
select store, page, line, line_label, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page, line, line_label
order by store, page, line, line_label

-- add page total
select x.*, sum(unit_count) over (partition by store, page)
from (
  select store, page, line, line_label, sum(unit_count) as unit_count
  from (
    select *
    from step_1) a
  group by store, page, line, line_label) x
order by store, page, line


-- by store, page
select store, page, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page
order by store, page

201603 ry1 used count  fs: 276, fact: 275
-- line 10 is the culprit fs: 25  fact: 24
select store, line, sum(unit_count)
from (
  select *
  from step_1
  where page = 16) a
group by store, line
order by store, line

-- missing 27451x
select * 
from step_1
where page = 16 and line = 10
  and left(trim(control), 1) <> 'H'
order by length(control), control


select store,
  sum(case when year_month = 201601 then unit_count else 0 end) as "201601",
  sum(case when year_month = 201602 then unit_count else 0 end) as "201602",
  sum(case when year_month = 201603 then unit_count else 0 end) as "201603",
  sum(case when year_month = 201604 then unit_count else 0 end) as "201604",
  sum(case when year_month = 201605 then unit_count else 0 end) as "201605",
  sum(case when year_month = 201606 then unit_count else 0 end) as "201606",
  sum(case when year_month = 201607 then unit_count else 0 end) as "201607",
  sum(case when year_month = 201608 then unit_count else 0 end) as "201608",
  sum(case when year_month = 201609 then unit_count else 0 end) as "201609",
  sum(case when year_month = 201610 then unit_count else 0 end) as "201610",
  sum(case when year_month = 201611 then unit_count else 0 end) as "201611",
  sum(case when year_month = 201612 then unit_count else 0 end) as "201612",
  sum(case when year_month = 201701 then unit_count else 0 end) as "201701",
  sum(case when year_month = 201702 then unit_count else 0 end) as "201702",
  sum(case when year_month = 201703 then unit_count else 0 end) as "201703"
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount, b.year_month,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201601 and 201703
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201707 -- only need one year_month here to generate page/line/label/accounts
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'  ) h
group by store
order by store


201603 ry1 used count  fs: 276, fact: 275
-- line 10 is the culprit fs: 25  fact: 24
select store, line, sum(unit_count)
from (
  select *
  from step_1
  where page = 16) a
group by store, line
order by store, line

-- missing 27451x
select * 
from step_1
where page = 16 and line = 10
  and left(trim(control), 1) <> 'H'
order by length(control), control

-- this was an auction purchase in january 2016
-- ws to honda in march 2016
-- 145200: SLS USED TRK WHSL, all the sales transactions were done again PVU instead of NVU
-- if i remove the join to dim_journal, it shows up
select d.the_date, b.account, b.description, c.journal_code, c.journal, a.*
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
where control = '27451x'
order by the_date, account

-- a correctly entered wholesale to honda
select d.the_date, b.account, c.journal_code, c.journal, a.*
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
where control = '27876b'
order by the_date, account 



201607 ry2 used count  fs: 144, fact: 143
-- page 14 is the culprit fs: 25  fact: 24
select store, page, sum(unit_count)
from (
  select *
  from step_1
  where store = 'ry2') a
group by store, page
order by store, page

-- problem is line 15 (versa) fs: 0  fact: -1
select store, page, line, sum(unit_count)
from (
  select *
  from step_1
  where store = 'ry2'
    and page = 14) a
group by store, page, line
order by store, page, line

-- h9140
-- sold in june 2016, unwound and resold in july 2016
-- which would normally be processed correctly, it is not in this case
-- because jeri made an adusting entry to account 242100 in july which shows
-- up as a -1 count
-- i don't believe there is anything i can do about this
select *
from step_1
where store = 'ry2'
  and page = 14
  and line = 15

select * from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount, b.year_month,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201601 and 201703
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701 -- only need one year_month here to generate page/line/label/accounts
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'  
) x where store = 'ry2' and page = 14 and line = 15 and year_month = 201607






select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
  a.control, a.amount, b.year_month,
  case when a.amount < 0 then 1 else -1 end as unit_count
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  and aa.journal_code in ('VSN','VSU')
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201702
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- d: fs gm_account page/line/acct description
  select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201702 -- only need one year_month here to generate page/line/label/accounts
    and b.page between 5 and 15 and b.line between 1 and 45
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code = '4'
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
where a.post_status = 'Y'  
  and d.store = 'ry1'
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- 201703 diff with sls.deals

create temp table fs_deals as -- 531 rows
select d.store, d.gl_account, 
  a.control,
  case when a.amount < 0 then 1 else -1 end as unit_count
from fin.fact_gl a   
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201706
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- d: fs gm_account page/line/acct description
  select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201706
    and (
      (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
      or
      (b.page = 16 and b.line between 1 and 14)) -- used cars
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code = '4'
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
  and aa.journal_code in ('VSN','VSU')   
  where a.post_status = 'Y' 

-- 14 rows from fs not in sls.deals    
-- fuck, they are all intra market wholesales, except h9777a which does not come up in tool
select *
from fs_deals a
left join  (
  select store_code, bopmast_id, stock_number, primary_sc, secondary_sc, 
    fi_manager, gap, service_contract, total_care, unit_count, delivery_date
  from sls.deals b
  where seq = (
    select max(seq)
    from sls.deals
    where store_code = b.store_code
      and bopmast_id = b.bopmast_id)
  group by store_code, bopmast_id, stock_number, primary_sc, secondary_sc, 
    fi_manager, gap, service_contract, total_care, unit_count,delivery_date) c on a.control = c.stock_number   
where c.store_code is null    
order by control       

-- wow, nothing here
select *
from  sls.deals a
where year_month = 201706
  and not exists (
    select 1
    from fs_deals
    where control = a.stock_number)
order by bopmast_id


    
  
    