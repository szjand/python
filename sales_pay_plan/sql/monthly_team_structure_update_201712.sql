﻿-- current team config:
-- drop table if exists team_config;
-- create temp table team_config as
select a.last_name, a.first_name, a.employee_number, b.team, bb.team_role,
  c.payplan, 
  case when is_senior then 'Yes' else null end as is_senior, coalesce(d.amount, 0)::integer as guarantee
-- select *  
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
inner join sls.personnel_roles bb on a.employee_number = bb.employee_number
  and bb.thru_date > current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201712
order by team, team_role, last_name  
---------------------------------------------------------------------
-- 12/29  
/*
no more team leader pay and an entirely new structure: 

Hey Jon, 

	Here is the team schedule going for december.  Also I no Payroll goes in two sections this December, It needs to be done the first time on December 27th for and than an adjustment that goes on 2018 pay for end of december pay.  Not sure what is easiest for you as far as running the report.  What do you think?
Team Eden
Bryn S
Josh B
Jimmy W
Austin J
Sam F

Team Anthony 
Dylan H
Nik H
Tyler B
Frank D
Jesse C

Team Stout
Justin B
Ross B
Kyle E
Donovan E

Team Abul
Brian S
Craig C

Team Foster
Jeff T
Alan M
Steve F


Team Knudson
Arden L
Jim W
Larry S

yunker & brooks no longer on pay plan
Phil is in an admin role now not sales and Damian is in a sales coordinator role not sales either.  Both are part of team eden
*/



-- 1. 2 new teams: Foster, Knudson
insert into sls.teams (team) values
('team foster'),
('team knudson');

-- 2. no one is assigned to team a-z
update sls.team_personnel
set thru_date = '11/30/2017'
-- select * from sls.team_personnel
where team = 'team a-z'
  and thru_date > current_date;

-- 3. jared langenstein is no longer a team leader
--    ben foster is now a team leader
-- current team leaders
-- select *
-- from sls.personnel a
-- inner join sls.personnel_roles b on a.employee_number = b.employee_number
--   and b.thru_date > current_date
-- inner join sls.team_roles c on b.team_role = c.team_role
--   and c.team_role = 'team_leader'  

update sls.personnel_roles
set thru_date = '11/30/2017'
where employee_number = (
  select employee_number  
  from sls.personnel
  where last_name = 'langenstein'
    and first_name = 'jared')
  and thru_date > current_date;

insert into sls.personnel values (
  'RY1','148050','Foster','Benjamin','10/21/2013','none','none','BFO',false,'10/21/2013');

insert into sls.personnel_roles values (
  '148050','team_leader','12/01/2017','12/31/9999');  

-- 4. assign/reassign consultants 
-- team eden
-- -- seay
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  from sls.personnel 
  where last_name = 'seay');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team eden', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'seay';
-- -- barker
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'barker');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team eden', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'barker';  
-- -- weber
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'weber');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team eden', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'weber';  
-- -- janzen
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'janzen');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team eden', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'janzen';  
-- -- foster
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'foster' and first_name = 'samuel');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team eden', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'foster' and first_name = 'samuel';
-- team anthony
-- -- haley
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'haley');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team anthony', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'haley';  
-- -- holland
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'holland');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team anthony', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'holland'; 
-- -- bedney
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'bedney');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team anthony', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'bedney'; 
-- -- decouteau
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'decouteau');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team anthony', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'decouteau'; 
-- team stout
-- -- eastman
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'eastman');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team stout', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'eastman'; 
-- team aubol
-- -- croaker
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'croaker');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team aubol', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'croaker'; 
-- team foster
-- -- tarr
-- -- ben foster
insert into sls.team_personnel (team,employee_number,from_date)
select 'team foster', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'foster' and first_name = 'benjamin'; 
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'tarr');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team foster', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'tarr'; 
-- -- mulhern
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'mulhern');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team foster', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'mulhern'; 
-- -- flaat
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'flaat');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team foster', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'flaat'; 
-- team knudson
-- -- ben knudson
insert into sls.team_personnel (team,employee_number,from_date)
select 'team knudson', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'knudson'; 
-- -- loven
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'loven');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team knudson', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'loven'; 
-- -- warmack
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'warmack');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team knudson', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'warmack'; 
-- -- Stadstad
update sls.team_personnel
set thru_date = '11/30/2017'
where thru_date > current_date
and employee_number = (
  select employee_number
  -- select *
  from sls.personnel 
  where last_name = 'Stadstad');
insert into sls.team_personnel (team,employee_number,from_date)
select 'team knudson', employee_number, '12/01/2017'
from sls.personnel
where last_name = 'Stadstad'; 

-- 5. no one is being paid on the team leader pay plan any more
update sls.personnel_payplans
set thru_date = '11/30/2017'  
where employee_number in (
  select employee_number
  from sls.personnel_payplans
  where payplan = 'team_leader'  
  and thru_date > current_date);

-- 6. people currently identified as team leaders have no guarantee
delete 
from sls.payroll_guarantees 
where year_month = 201712
  and employee_number in (
    select a.employee_number
    from sls.payroll_guarantees a
    inner join sls.personnel_roles b on a.employee_number = b.employee_number
      and b.team_role = 'team_leader'
      and b.thru_date > current_date
    where year_month = 201712) ; 

-- 7. yunker & brooks no longer on paypla
update sls.team_personnel
set thru_date = '11/30/2017'
-- select * from sls.team_personnel
where thru_date > current_date
  and employee_number in (
    select employee_number 
    from sls.personnel 
    where last_name in ('brooks', 'yunker'));
update sls.personnel_roles
set thru_date = '11/30/2017'
-- select * from sls.personnel_roles
where thru_date > current_date
  and employee_number in (
    select employee_number 
    from sls.personnel 
    where last_name in ('brooks', 'yunker'));    
update sls.personnel_roles
set thru_date = '11/30/2017'
-- select * from sls.personnel_roles
where thru_date > current_date
  and employee_number in (
    select employee_number 
    from sls.personnel 
    where last_name in ('brooks', 'yunker'));     
update sls.personnel_payplans
set thru_date = '11/30/2017'
-- select * from sls.personnel_payplans
where thru_date > current_date
  and employee_number in (
    select employee_number 
    from sls.personnel 
    where last_name in ('brooks', 'yunker'));        




  