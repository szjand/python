﻿-- select * from sls.months where open_closed = 'open'
/*
update sls.months set open_closed = 'closed';
update sls.months set open_closed = 'open' where year_month = 201706;
*/

drop table if exists sls.tmp_consultants;
create table sls.tmp_consultants (
  year_month integer not null,
  store_code citext not null,
  employee_number citext primary key, 
  last_name citext not null,
  first_name citext not null,
  is_senior boolean not null,
  team citext not null,
  payplan citext not null);
truncate sls.tmp_consultants; 
insert into sls.tmp_consultants
select (select year_month from sls.months where open_closed = 'open') as year_month, 
  a.store_code, a.employee_number, a.last_name, a.first_name, a.is_senior, 
  c.team, d.payplan
from sls.personnel a
inner join sls.personnel_roles b on a.employee_number = b.employee_number
  and b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and b.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')
  and b.team_role = 'consultant'
inner join sls.team_personnel c on a.employee_number = c.employee_number  
  and c.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and c.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')
inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
  and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and d.thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open');    

drop table if exists sls.tmp_fi_gross;
create table sls.tmp_fi_gross (
  year_month integer not null,
  employee_number citext primary key,
  fi_gross numeric(8,2) not null);
truncate sls.tmp_fi_gross;  
insert into sls.tmp_fi_gross
select year_month, employee_number, sum(fi_gross) as fi_gross
from (
  select a.year_month, a.psc_employee_number as employee_number,
    sum(
      case 
        when ssc_first_name = 'none' then b.fi_gross * a.unit_count
        else 0.5 * b.fi_gross * a.unit_count
      end) as fi_gross
  from sls.deals_by_month a
  left join sls.deals_gross_by_month b on a.year_month = b.year_month
    and a.stock_number = b.control
  where a.year_month = (select year_month from sls.months where open_closed = 'open')
  group by a.year_month, a.psc_employee_number     
  union -- deals by ssc
  select a.year_month, a.ssc_employee_number, 
    sum(0.5 * b.fi_gross * a.unit_count)
  from sls.deals_by_month a
  left join sls.deals_gross_by_month b on a.year_month = b.year_month
    and a.stock_number = b.control
  where a.year_month = (select year_month from sls.months where open_closed = 'open')
    and a.ssc_first_name <> 'none'
  group by a.year_month, a.ssc_employee_number) c
group by year_month, employee_number;


-- drop table if exists sls.tmp_consultant_stats_1;
-- create table sls.tmp_consultant_stats_1 (
--   year_month integer not null,
--   employee_number citext primary key,
--   guarantee numeric(8,2) not null,
--   fi_gross numeric(8,2) not null,
--   regular_hours numeric(5,2) not null,
--   overtime_hours numeric(5,2) not null,
--   pto_hours numeric(5,2) not null,
--   pto_rate numeric(8,2) not null,
--   draw numeric(8,2) not null);
truncate sls.tmp_consultant_stats_1;
insert into sls.tmp_consultant_stats_1  
select a.year_month, a.employee_number, 
  coalesce(c.amount, 0) as guarantee, coalesce(b.fi_gross, 0), 
  case when a.payplan = 'hourly' then f.regular_hours else 0 end as regular_hours,
  case when a.payplan = 'hourly' then f.overtime_hours else 0 end as overtime_hours,
  coalesce(f.pto_hours, 0) as pto_hours, e.pto_rate,
  coalesce(d.draw, 0)
from sls.tmp_consultants a
left join sls.tmp_fi_gross b on a.year_month = b.year_month
  and a.employee_number = b.employee_number
left join sls.payroll_guarantees c on a.employee_number = c.employee_number  
  and a.year_month = c.year_month
left join sls.paid_by_month d on a.employee_number = d.employee_number
  and a.year_month = d.year_month
left join sls.pto_intervals e on a.employee_number = e.employee_number
  and a.year_month = e.year_month
left join sls.clock_hours_by_month f on a.employee_number = f.employee_number
  and a.year_month = f.year_month; 

-- drop table if exists sls.tmp_consultant_stats_2;
-- create table sls.tmp_consultant_stats_2 (
--   employee_number citext primary key,
--   unit_count numeric(4,1) not null,
--   total_care numeric(4,1) not null,
--   service_contract numeric(4,1) not null,
--   gap numeric(4,1) not null,
--   lease numeric(4,1) not null,
--   new_cadillac numeric(4,1) not null,
--   qual_count numeric(4,1) not null,
--   non_qual_count numeric(4,1) not null);
truncate sls.tmp_consultant_stats_2;
insert into sls.tmp_consultant_stats_2
select a.employee_number,
  coalesce(sum(case when b.ssc_first_name = 'none' 
    then b.unit_count else 0.5 * b.unit_count end), 0) as unit_count, 
  coalesce(
    sum(
      case 
        when b.ssc_first_name = 'none' then b.total_care * b.unit_count
        else 0.5 * b.total_care * b.unit_count
      end), 0) as total_care,
  coalesce(
    sum(
      case 
        when b.ssc_first_name = 'none' then b.service_contract * b.unit_count
        else 0.5 * b.service_contract * b.unit_count
      end), 0) as service_contract,
  coalesce(
    sum(
      case 
        when b.ssc_first_name = 'none' then b.gap * b.unit_count 
        else 0.5 * b.gap * b.unit_count
      end), 0) as gap,   
  coalesce(
    sum(
      case 
        when b.sale_type = 'lease' and b.ssc_first_name = 'none' then unit_count  
        when b.sale_type = 'lease' and b.ssc_first_name <> 'none' then 0.5 * unit_count
        else 0
      end), 0) as lease,
  coalesce(
    sum(
      case
        when sale_group in ('new cad car','new cad truck') and b.ssc_first_name = 'none' then unit_count
        when sale_group in ('new cad car','new cad truck') and b.ssc_first_name <> 'none' then 0.5 * unit_count
        else 0
      end), 0) as new_cadillac,
  coalesce(
    case
      when a.payplan = 'executive' then
        sum( 
          case when executive_qualified = 1 then 
            case 
              when b.ssc_first_name = 'none' then b.unit_count 
              else unit_count * .5
            end
          end)
      else 0
    end, 0) as qual_count,
  coalesce(
    case
      when a.payplan = 'executive' then
        sum( 
          case when executive_qualified = 0 then 
            case 
              when b.ssc_first_name = 'none' then b.unit_count 
              else unit_count * .5
            end
          end)
      else 0
    end, 0) as non_qual_count  
from sls.tmp_consultants a
left join sls.deals_by_month b on a.year_month = b.year_month
  and (b.psc_employee_number = a.employee_number or b.ssc_employee_number = a.employee_number)
group by a.employee_number, a.payplan;

-- drop table if exists sls.consultant_payroll;
-- create table sls.consultant_payroll (
--   year_month integer not null,
--   team citext not null,
--   last_name citext not null,
--   first_name citext not null,
--   employee_number citext not null,
--   payplan citext not null,
--   unit_count numeric(3,1) not null,
--   unit_pay numeric(8,2) not null,
--   fi_pay numeric(8,2) not null,
--   total_care numeric(8,2) not null,
--   service_contract numeric(8,2) not null,
--   gap numeric(8,2) not null,
--   lease numeric(8,2) not null,
--   new_cadillac numeric(8,2) not null,
--   hourly_pay numeric(8,2) not null,
--   overtime_pay numeric(8,2) not null,
--   pto_pay numeric(8,2) not null,
--   total_earned numeric(8,2) not null,
--   draw numeric(8,2) not null,
--   guarantee numeric(8,2) not null,
--   due_at_month_end numeric(8,2) not null,
--   last_updated timestamptz,
--   constraint consultant_payroll_pkey primary key(year_month,employee_number));
delete 
from sls.consultant_payroll 
where year_month = (select year_month from sls.months where open_closed = 'open');
insert into sls.consultant_payroll 
select year_month, team, last_name, first_name, employee_number, payplan,
  unit_count, unit_pay, fi_pay, total_care, service_contract,
  gap, leases, cadillac,
  hourly_pay, overtime_pay, pto_pay, total_earned,
  draw, guarantee,
  round(case 
    when total_earned > guarantee then total_earned - draw
    else guarantee - draw
  end, 2) as due_at_month_end,
  now() as last_updated
from ( 
  select year_month, team, last_name, first_name, employee_number, payplan,
    unit_count, unit_pay, fi_pay, total_care, service_contract, gap, leases, cadillac,   
    hourly_pay, overtime_pay, pto_pay, 
    case
      when payplan = 'hourly' then hourly_pay+overtime_pay+pto_pay
      else unit_pay+fi_pay+total_care+service_contract+gap+leases+cadillac+pto_pay 
    end as total_earned,
    draw, guarantee
  from (  
    select (select year_month from sls.months where open_closed = 'open') as year_month, 
      now() as last_updated,
      team, last_name, first_name, employee_number, payplan, unit_count,
      case 
        when payplan = 'executive' then (200 * qual_count) + (280 * non_qual_count)
        when payplan <> 'executive' and is_senior = true then 330 * unit_count
        when payplan <> 'executive' then 280 * unit_count 
      end as unit_pay,
      case
        when payplan = 'executive' then round(.16 * fi_gross, 2)
        else round(.035 * fi_gross, 2)
      end as fi_pay,
      25 * total_care as total_care, 25 * service_contract as service_contract,
      15 * gap as gap, 25 * lease as leases, 25 * new_cadillac as cadillac,
      regular_hours * 16 as hourly_pay, overtime_hours * 24 as overtime_pay, 
      pto_hours * pto_rate as pto_pay,
      guarantee, draw
    from (
      select a.*, 
        b.guarantee, b.fi_gross, b.regular_hours, b.overtime_hours, b.pto_hours, b.pto_rate, b.draw,
        c. unit_count, c.total_care, c.service_contract, c.gap, c.lease, c.new_cadillac, c.qual_count, c.non_qual_count
      from sls.tmp_consultants a
      left join sls.tmp_consultant_stats_1 b on a.employee_number = b.employee_number
      left join sls.tmp_consultant_stats_2 c on a.employee_number = c.employee_number) m) n) x;
  

/*
-- april teams
select * 
from sls.personnel a
inner join sls.team_personnel b  on a.employee_number = b.employee_number
where b.from_Date < '05/01/2017'
  and thru_date > '03/31/2017'
order by team, last_name  

-- may teams
select * 
from sls.personnel a
inner join sls.team_personnel b  on a.employee_number = b.employee_number
where b.thru_date > '04/30/2017' 
  and b.from_date < '06/01/2017'
order by team, last_name  

-- april pay plans
select * 
from sls.personnel a
inner join sls.personnel_payplans b  on a.employee_number = b.employee_number
where b.from_Date < '05/01/2017'
  and thru_date > '03/31/2017'
order by last_name

-- may pay plans
select * 
from sls.personnel a
inner join sls.personnel_payplans b  on a.employee_number = b.employee_number
where b.thru_date > '04/30/2017' 
  and b.from_date < '06/01/2017'
order by last_name
*/


select *
from sls.consultant_payroll
where year_month = 201707
order by last_name









                    