﻿-- select * from sls.consultant_pay
-- 
-- select e.last_name, e.first_name, f.arkona_total_gross, arkona_comm_draw, cat
-- from sls.folks e
-- left join (
--   select m.*, n.arkona_comm_draw, n.cat
--   from ( -- pyhshdta
--     select a.employee_, check_month, 201600 + check_month as year_month, sum(a.total_gross_pay) as arkona_total_gross
--     from dds.ext_pyhshdta a
--     where trim(a.distrib_code) in ('TEAM','SALE')
--       and a.payroll_cen_year = 117
--       and a.payroll_ending_month = 4
--       and a.company_number = 'RY1'
--     group by a.employee_, check_month) m
--   left join ( -- pyhscdta
--     select employee_, check_month, 
--       sum(case when q.code_id in ('79','78') then amount end) as arkona_comm_draw,
--       string_agg(q.description || ':' || q.amount::citext, ' | ' order by q.description) as cat
--     from ( 
--       select a.employee_name, a.employee_, a.check_month, b.description, sum(b.amount) as amount, b.code_id
--       --select distinct c.description
--       from dds.ext_pyhshdta a
--       inner join dds.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
--         and a.company_number = b.company_number
--         and a.employee_ = b.employee_number
--         and b.code_type in ('0','1') -- 0,1: income, 2: deduction
--       where trim(a.distrib_code) in ('TEAM','SALE')
--         and a.payroll_cen_year = 117
--         and a.payroll_ending_month = 4
--         and a.company_number = 'RY1'
--       group by a.employee_name, a.employee_, a.check_month, b.description, b.code_id) q
--     group by  employee_, check_month) n on m.employee_ = n.employee_ and m.check_month = n.check_month) f on e.employee_number = f.employee_
-- 

-- 6/5
-- change the payroll_ending_month in pyhshdta & pyhscdta
-- not using sls.folks anymore
-- select year_month, last_name, first_name, employee_number, payplan, pto_pay, draw, guarantee, due_at_month_end
-- -- select *
-- from sls.consultant_payroll
-- where year_month = 201705
-- union
-- select year_month, last_name, first_name, employee_number, 'team_leader', pto_pay, draw, guarantee, due_at_month_end
-- -- select *
-- from sls.team_leader_payroll
-- where year_month = 201705

-- select e.last_name, e.first_name, coalesce(ee.total_pay, eee.total_pay) as vision, f.arkona_total_gross, arkona_comm_draw, commission, draw, overtime, pto_pay_out, lease, vacation, spiffs
-- from sls.folks e
-- left join sls.consultantpay ee on e.employee_number = ee.employee_number
-- left join sls.team_leader_pay eee on e.employee_number = eee.employee_number

looks like there is still confusion about pto
anthony michael, does total_earned include pto?

-- 9/5/17  looking at this: system: month end + draw should equal arkona: arkona-total_gross - lease & spiffs
select e.*, f.arkona_total_gross, arkona_comm_draw, commission, f.draw, overtime, pto_pay_out, lease, vacation, spiffs
-- select last_name, total_earned, arkona_total_gross
from (
  select year_month, last_name, first_name, employee_number, payplan, pto_pay, total_earned, draw, guarantee, due_at_month_end
  -- select *
  from sls.consultant_payroll
  where year_month = 201711 -------------------------------------------------------------------------------
  union
  select year_month, last_name, first_name, employee_number, 'team_leader', pto_pay, total_earned, draw, guarantee, due_at_month_end
  -- select *
  from sls.team_leader_payroll
  where year_month = 201711) e ----------------------------------------------------------------------------
left join (
  select m.*, n.arkona_comm_draw, n.commission, draw, overtime, pto_pay_out, lease, vacation, spiffs
  from ( -- pyhshdta
    select a.employee_, check_month, 201600 + check_month as year_month, sum(a.total_gross_pay) as arkona_total_gross
    from arkona.ext_pyhshdta a
    where trim(a.distrib_code) in ('TEAM','SALE')
      and a.payroll_cen_year = 117
      and a.payroll_ending_month = 10 -----------------------------------------------------------------------
      and a.company_number = 'RY1'
    group by a.employee_, check_month) m
  left join ( -- pyhscdta
    select employee_, check_month, 
      sum(case when q.code_id in ('79','78') then amount end) as arkona_comm_draw,
      sum(case when q.description = 'commissions' then amount else 0 end) as commission,
      sum(case when q.description = 'draws' then amount else 0 end) as draw,
      sum(case when q.description = 'overtime pay' then amount else 0 end) as overtime,
      sum(case when q.description = 'pay out pto' then amount else 0 end) as pto_pay_out,
      sum(case when q.description = 'lease program' then amount else 0 end) as lease,
      sum(case when q.description = 'sales vacation' then amount else 0 end) as vacation,
      sum(case when q.description = 'spiff pay outs' then amount else 0 end) as spiffs
--       string_agg(q.description || ':' || q.amount::citext, ' | ' order by q.description) as cat
    from ( 
      select a.employee_name, a.employee_, a.check_month, b.description, sum(b.amount) as amount, b.code_id
      -- select distinct b.description
      from arkona.ext_pyhshdta a
      inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
        and a.company_number = b.company_number
        and a.employee_ = b.employee_number
        and b.code_type in ('0','1') -- 0,1: income, 2: deduction
      where trim(a.distrib_code) in ('TEAM','SALE')
        and a.payroll_cen_year = 117
        and a.payroll_ending_month = 10 ------------------------------------------------------------------------
        and a.company_number = 'RY1'
      group by a.employee_name, a.employee_, a.check_month, b.description, b.code_id) q
    group by  employee_, check_month) n on m.employee_ = n.employee_ and m.check_month = n.check_month) f on e.employee_number = f.employee_
order by last_name    