﻿create schema if not exists sls;
comment on schema sls is 'Sales Data Mart: all things having to do with vehicle sales: initially sales pay plans';
drop table if exists sls.ext_deals cascade;
create table sls.ext_deals (
  run_date date not null,
  store_code citext not null,
  bopmast_id integer not null,
  deal_status citext not null,
  deal_type citext not null,
  sale_type citext not null,
  sale_group citext not null,
  vehicle_type citext not null,
  stock_number citext not null,
  vin citext not null,
  odometer_at_sale integer not null,
  buyer_bopname_id integer not null,
  cobuyer_bopname_id integer not null,
  primary_sc citext not null,
  secondary_sc citext not null,
  fi_manager citext not null,
  origination_date date not null,
  approved_date date not null,
  capped_date date not null,
  delivery_date date not null,
  gap numeric(8,2) not null,
  service_contract numeric(8,2) not null,
  total_care numeric(8,2) not null,
  hash citext,
  constraint ext_deals_pkey primary key (store_code, bopmast_id));
COMMENT ON COLUMN sls.ext_deals.store_code IS 'source: bopmast.bopmast_company_number';
COMMENT ON COLUMN sls.ext_deals.bopmast_id IS 'source: bopmast.record_key';
COMMENT ON COLUMN sls.ext_deals.deal_status IS 'source: bopmast.record_status';
COMMENT ON COLUMN sls.ext_deals.deal_type IS 'source: bopmast.record_type';
COMMENT ON COLUMN sls.ext_deals.sale_type IS 'source: bopmast.sale_type';
COMMENT ON COLUMN sls.ext_deals.sale_group IS 'source: bopmast.franchise_code';
COMMENT ON COLUMN sls.ext_deals.vehicle_type IS 'source: bopmast.vehicle_type';
COMMENT ON COLUMN sls.ext_deals.stock_number IS 'source: bopmast.bopmast_stock_number';
COMMENT ON COLUMN sls.ext_deals.vin IS 'source: bopmast.bopmast_vin';
COMMENT ON COLUMN sls.ext_deals.odometer_at_sale IS 'source: bopmast.odometer_at_sale';
COMMENT ON COLUMN sls.ext_deals.buyer_bopname_id IS 'source: bopmast.buyer_number';
COMMENT ON COLUMN sls.ext_deals.cobuyer_bopname_id IS 'source: bopmast.co_buyer_number';
COMMENT ON COLUMN sls.ext_deals.primary_sc IS 'source: bopmast.primary_salespers';
COMMENT ON COLUMN sls.ext_deals.secondary_sc IS 'source: bopmast.secondary_slspers2';
COMMENT ON COLUMN sls.ext_deals.fi_manager IS 'source: bopmast.pd_policy_number';
COMMENT ON COLUMN sls.ext_deals.origination_date IS 'source: bopmast.origination_date';
COMMENT ON COLUMN sls.ext_deals.approved_date IS 'source: bopmast.date_approved';
COMMENT ON COLUMN sls.ext_deals.capped_date IS 'source: bopmast.date_capped';
COMMENT ON COLUMN sls.ext_deals.delivery_date IS 'source: opmast.delivery_date';
COMMENT ON COLUMN sls.ext_deals.gap IS 'source: bopmast.gap_premium';
COMMENT ON COLUMN sls.ext_deals.service_contract IS 'source: bopmast.serv_cont_amount';
COMMENT ON COLUMN sls.ext_deals.total_care IS 'source: bopmast.amo_total';
COMMENT ON COLUMN sls.ext_deals.hash IS 'md5 of entire row';
create index on sls.ext_deals(hash);

drop table if exists sls.ext_bopmast_partial;
create table sls.ext_bopmast_partial (
  store citext,
  bopmast_id integer,
  deal_status citext,
  deal_type citext,
  sale_type citext,
  sale_group citext,
  vehicle_type citext,
  stock_number citext,
  vin citext,
  odometer_at_sale integer,
  buyer_bopname_id integer,
  cobuyer_bopname_id integer,
  primary_sc citext,
  secondary_sc citext,
  fi_manager citext,
  origination_date integer,
  approved_date date,
  capped_date date,
  delivery_date date,
  gap numeric(8,2),
  service_contract numeric(8,2),
  total_care numeric(8,2),
  constraint ext_bopmast_partial_pkey primary key (store, bopmast_id));

-- select max(capped_date) from sls.xfm_bopmast
-- 
-- select max(date_capped) from arkona.ext_bopmast_tmp
-- 
-- and compare the scrape from 4/1
-- 
-- select 'update', d.*
-- from sls.xfm_bopmast d
-- inner join (
--   select  bopmast_company_number, record_key, coalesce(record_status,'none'),record_type, sale_type,
--     coalesce(franchise_code,'none'), vehicle_type, bopmast_stock_number, bopmast_vin, odometer_at_sale,
--     buyer_number, coalesce(co_buyer_number, -1),coalesce(primary_salespers,'none'), coalesce(secondary_slspers2, 'none'), 
--     coalesce(f_i_manager,'none'),origination_date, date_approved, date_capped, delivery_date, gap_premium,
--     serv_cont_amount, amo_total, md5(a::text) as hash
--   from arkona.ext_bopmast_tmp a
--   where bopmast_stock_number is not null) e on d.store = e.bopmast_company_number
--   and d.bopmast_id = e.record_key
--   and d.hash <> e.hash
-- 
-- union
-- 
-- select 
-- from (
--   select  bopmast_company_number, record_key, coalesce(record_status,'none'),record_type, sale_type,
--     coalesce(franchise_code,'none'), vehicle_type, bopmast_stock_number, bopmast_vin, odometer_at_sale,
--     buyer_number, coalesce(co_buyer_number, -1),coalesce(primary_salespers,'none'), coalesce(secondary_slspers2, 'none'), 
--     coalesce(f_i_manager,'none'),origination_date, date_approved, date_capped, delivery_date, gap_premium,
--     serv_cont_amount, amo_total, md5(a::text) as hash
--   from arkona.ext_bopmast_tmp a
--   where bopmast_stock_number is not null) d
-- left join sls.xfm_bopmast e on d.bopmast_company_number = e.store
--   and d.record_key = e.bopmast_id
-- where e.store is null    

-- 4/2
rethinking, the basic process from scpp is pretty good, just need to clean up the testing to pick up
the missed deals
so, this becomes ext_deals, need to include the exclusions

select stock_number
from sls.xfm_bopmast
group by stock_number
having count(*) > 1
-- -- 4/23 
-- starting over (again)
-- no need to care about dup stock numbers or, for that matter, null stock numbers
-- stock number is just another attribute
-- until it comes to joining to accounting
truncate sls.ext_bopmast_partial;
insert into sls.ext_bopmast_partial
select TRIM(BOPMAST_COMPANY_NUMBER) as bopmast_company_number ,RECORD_KEY,
  TRIM(RECORD_STATUS) as RECORD_STATUS, TRIM(RECORD_TYPE) as RECORD_TYPE,
  TRIM(SALE_TYPE) as SALE_TYPE ,TRIM(FRANCHISE_CODE) as FRANCHISE_CODE,
  TRIM(VEHICLE_TYPE) VEHICLE_TYPE, TRIM(BOPMAST_STOCK_NUMBER) as BOPMAST_STOCK_NUMBER,
  TRIM(BOPMAST_VIN) as BOPMAST_VIN, ODOMETER_AT_SALE,BUYER_NUMBER,CO_BUYER_NUMBER,
  TRIM(PRIMARY_SALESPERS) as PRIMARY_SALESPERS,
  TRIM(SECONDARY_SLSPERS2) as SECONDARY_SLSPERS2, 
  TRIM(PD_POLICY_NUMBER) as PD_POLICY_NUMBER,
  ORIGINATION_DATE,DATE_APPROVED,DATE_CAPPED,DELIVERY_DATE,
  GAP_PREMIUM,SERV_CONT_AMOUNT,AMO_TOTAL
from from_ubuntu_test.ext_bopmast_0228
where origination_date > 20170000;

alter table sls.ext_deals
alter column stock_number drop not null,
alter column vin drop not null;  
-- ok, this is where i need to do filtering from scpp.ext_deals
-- a little tricky, occasionally, the stocknumber disappears from bopmast on a legitimate deal
-- so i can't just exclude null stocknumbers, need to pull valid deals that have null stocknumbers
-- no fi managers ?!?!?! wtf, it's stored in pd_policy_number
-- looking like i need to explicitly exclude null stocknumbers and then add in the ones i want
-- script for initial load: bopmast where origination date > 20170000
-- ubuntu::test.ext_bopmast_0228 (foreign server)
truncate sls.ext_deals;
insert into sls.ext_deals (run_date,store_code,bopmast_id,deal_status,deal_type,sale_type,
  sale_group,vehicle_type,stock_number,vin,odometer_at_sale,buyer_bopname_id,
  cobuyer_bopname_id,primary_sc,secondary_sc,fi_manager,origination_date,approved_date,
  capped_date,delivery_date,gap,service_contract,total_care)
select '02/28/2017'::date,a.BOPMAST_COMPANY_NUMBER,a.RECORD_KEY,coalesce(a.RECORD_STATUS, 'none'),a.RECORD_TYPE,a.sale_type,
  coalesce(a.FRANCHISE_CODE,'none'),a.vehicle_type,a.BOPMAST_STOCK_NUMBER,a.BOPMAST_VIN,a.odometer_at_sale,
  a.BUYER_NUMBER, coalesce(a.CO_BUYER_NUMBER, -1), 
  case
    when coalesce(a.PRIMARY_SALESPERS, 'none') = 'AMA' then 'HAN'
    when coalesce(a.PRIMARY_SALESPERS, 'none') = 'HAL' and a.BOPMAST_COMPANY_NUMBER = 'RY1' then 'JHA'
    when coalesce(a.PRIMARY_SALESPERS, 'none') = 'ENT' and a.BOPMAST_COMPANY_NUMBER = 'RY1' then 'AJO'
    when coalesce(a.PRIMARY_SALESPERS, 'none') = 'WWW' and a.BOPMAST_COMPANY_NUMBER = 'RY2' then 'WHE'
    else coalesce(a.PRIMARY_SALESPERS, 'none')
  end,
  case
    when coalesce(a.SECONDARY_SLSPERS2, 'none') = 'AMA' then 'HAN'
    when coalesce(a.SECONDARY_SLSPERS2, 'none') = 'HAL' and a.BOPMAST_COMPANY_NUMBER = 'RY1' then 'JHA'
    when coalesce(a.SECONDARY_SLSPERS2, 'none') = 'ENT' and a.BOPMAST_COMPANY_NUMBER = 'RY1' then 'AJO'
    when coalesce(a.SECONDARY_SLSPERS2, 'none') = 'WWW' and a.BOPMAST_COMPANY_NUMBER = 'RY2' then 'WHE'
    else coalesce(a.SECONDARY_SLSPERS2, 'none')
  end,
  coalesce(a.PD_POLICY_NUMBER,'none'),
  (select dds.db2_integer_to_date(a.origination_date)),a.DATE_APPROVED,
  a.DATE_CAPPED,a.delivery_date,a.GAP_PREMIUM,a.SERV_CONT_AMOUNT,a.AMO_TOTAL
from ext_bopomast_partial a
-- where a.BOPMAST_VIN is not null
--   and BOPMAST_STOCK_NUMBER is not null
--   and BOPMAST_STOCK_NUMBER in ( -- excludes dup stock numbers
--     select BOPMAST_STOCK_NUMBER
--     from ext_bopomast_partial
--     group by BOPMAST_STOCK_NUMBER
--     having count(*) = 1)
where (select dds.db2_integer_to_date(a.origination_date)) > '12/31/2014';

-- not needed for intial load
-- union -- null stock numbers with non null vin & deal status 
-- select *
-- from (
--   select '02/28/2017'::date, a.BOPMAST_COMPANY_NUMBER,a.RECORD_KEY,a.RECORD_STATUS,a.RECORD_TYPE,a.sale_type,
--     coalesce(a.FRANCHISE_CODE,'none'),a.vehicle_type,
--     coalesce(a.BOPMAST_STOCK_NUMBER,'none'),a.BOPMAST_VIN,a.odometer_at_sale,
--     a.BUYER_NUMBER,coalesce(a.CO_BUYER_NUMBER, -1), 
--     case
--       when coalesce(a.PRIMARY_SALESPERS, 'none') = 'AMA' then 'HAN'
--       when coalesce(a.PRIMARY_SALESPERS, 'none') = 'HAL' and a.BOPMAST_COMPANY_NUMBER = 'RY1' then 'JHA'
--       when coalesce(a.PRIMARY_SALESPERS, 'none') = 'ENT' and a.BOPMAST_COMPANY_NUMBER = 'RY1' then 'AJO'
--       when coalesce(a.PRIMARY_SALESPERS, 'none') = 'WWW' and a.BOPMAST_COMPANY_NUMBER = 'RY2' then 'WHE'
--       else coalesce(a.PRIMARY_SALESPERS, 'none')
--     end,
--     case
--       when coalesce(a.SECONDARY_SLSPERS2, 'none') = 'AMA' then 'HAN'
--       when coalesce(a.SECONDARY_SLSPERS2, 'none') = 'HAL' and a.BOPMAST_COMPANY_NUMBER = 'RY1' then 'JHA'
--       when coalesce(a.SECONDARY_SLSPERS2, 'none') = 'ENT' and a.BOPMAST_COMPANY_NUMBER = 'RY1' then 'AJO'
--       when coalesce(a.SECONDARY_SLSPERS2, 'none') = 'WWW' and a.BOPMAST_COMPANY_NUMBER = 'RY2' then 'WHE'
--       else coalesce(a.SECONDARY_SLSPERS2, 'none')
--     end,
--     coalesce(a.PD_POLICY_NUMBER,'none'), 
--     (select dds.db2_integer_to_date(a.origination_date)),a.DATE_APPROVED,
--     a.DATE_CAPPED,a.delivery_date,a.GAP_PREMIUM,a.SERV_CONT_AMOUNT,a.AMO_TOTAL
-- from ext_bopomast_partial a
-- where (select dds.db2_integer_to_date(origination_date)) > '12/31/2014'
--   and BOPMAST_STOCK_NUMBER is null
--   and BOPMAST_VIN is not null
--   and RECORD_STATUS is not null) x


select *
from sls.ext_deals a
LEFT JOIN scpp.ext_deals b on a.stock_number = b.stock_number
where b.stock_number is null 
  and a.sale_type <> 'W'


delete from sls.ext_deals;
delete from sls.ext_bopmast_partial;

select * from sls.ext_deals  

select * from sls.ext_bopmast_partial

select distinct run_date from sls.ext_deals


-- 4/20 -----------------------------------------------------------------------------------
-- ok, let's do the daily  
actually, nothing really to do here, script ok, smartdraw ok, move on to xfm_deals
select *
from sls.ext_deals

-- 4/21 -----------------------------------------------------------------------------------
remove has
alter table sls.ext_deals
drop column hash;

-- 4/22 -----------------------------------------------------------------------------------
started over (again)
changed the intial load process
done: 3/1

-- 4/23 -----------------------------------------------------------------------------------
doubting the dup stocknumber bit
should it be dup  bopmast_id