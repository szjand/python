﻿
-- 4/24 
any change except for deal status is type 1, straight update
deal status requires a new row in sls.deals, because the unit count changes
identify the "type 2", process, then update the rest
a type2 insert will include any and all incidental type changes, inserting from current xfm row
what is the deal deate when a deal is unwound
also, when gl_date changes, does the deal date change?  seems like if it is the same deal (vehicle & customer) it does not
so, if gl_date changes, but the customer does not, the deal date does not change

3/6 do it all manual (here in the sql script)
3/6     what changed
28855A  app_date  cap_date
29655   app_date  cap_date
30635   app_date  cap_date  deal_type   gl_date   gl_count !!!!
30245A  app_date  cap_date
28970R  app_date  cap_date
30745X  orig_date deal_status !!!!

3/8
type_1: 28970R 
type_2_d: 29910XXA, 30342XX
type_2_e: 30635
type_2_f: 30745X

select * 
from sls.deals
where stock_number in ('28970R','29910XXA','30342XX','30635','30745X')
order by stock_number, seq



-- 4/25
-- changed deals
drop table if exists sls.changed_deals;
create table sls.changed_deals (
  xfm_hash citext not null,
  deal_hash citext not null,
  change_type citext not null,
  constraint changed_deals_pkey primary key (xfm_hash,deal_hash));
create unique index on sls.changed_deals(xfm_hash);
create unique index on sls.changed_deals(deal_hash);

/*
5/8/17
given up on the notion of a deal date, 
the available dates are:  
  bopmast: origination_date
           approved_date
           capped_date
           delivery_date
  glpmast: gl_date

the closest thing to a deal date is probably the delivery date
but fuck it, it is a nebulous term that means something different to everybody,
asked greg: the date the contract was signed

year_month: the month to which the unit_count applies, should, i think, roughly track gl_date

do i really want to refactor sls.deals.deal_date to sls.deals.gl_date, i think so
this is going to break a bunch of stuff for a while
oh well

alter table sls.deals
rename column deal_date to gl_date;

changed all instances in this script
5/9
occassionally the buyer changes on a deal (2/1440)

select *
from sls.xfm_deals
where bopmast_id in ( -- bopmast_id with more than 1 buyer
  select bopmast_id
  from (
    select bopmast_id, buyer_bopname_id
    from sls.xfm_deals
    group by bopmast_id, buyer_bopname_id) a
  group by bopmast_id
  having count(*) > 1)
order by bopmast_id, seq

i am defining a deal as a bopmast_id, not a stock_number
a bopmast_id will always be for a single stock_number,
but a stock_number can be for more than one bopmast_id (unwinds)

-- general rule: if the unit count changes, year_month gets updated
*/


2_a: unit_count(deals) is positive, gl_count(xfm_deals) is negative 
     deals.gl_date <> gl_date(xfm_deals)     
     deal has been unwound in accounting
2_b: deal_status(xfm_deals) = none or A
     deal_status_code(deals) = U
     deal has been uncapped in bopmast
2_c: deal_status(xfm_deals) = deleted
     deal_status_code = U
     gl_date(xfm_deals) <> deals.gl_date
     deal has been deleted from bopmast
2_d: notes(deals) = type_2_a
     deal_status(xfm_deals) = deleted   
     a deal that was previously unwound in accounting has been deleted from bopmast
     update row only
2_e: deal.notes = type_2_a (30635)
     deal.unit_count = -1
     xfm_deals.gl_count = 1
     no change in buyer
     a deal that has been previously unwound in accounting has been reposted
     new row
2_f: deals.notes = type_2_b
     deals.unit_count < 0
     xfm_deals.gl_count > 0
     xfm_deals.deal_status = U
     a deal previously un-capped in bopmast has been re-capped
     new row
2_g: xfm_deals.deal_status = deleted
     no other changes
 deals.notes = type_2_b
     deals.deal_status_code = A
     xfm_deals.deal_status = A
     a deal previously un-capped in bopmast has been re-accepted: DO NOTHING
-- need to exclude deals where deal_status = deleted and deal_status_code = deleted     
-- i don't believe a deleted deal ever comes back to life
-- 5/19 29164B sold/capped 5/11/17, 5/18 accounting voided reposted, believe this is a type 1, the only
-- thing changing is the gl_date, so the danger criteria involving gl_date has to change
-- 6/4 Danger Will Robinson 31176XA, the only thing that has changed is the gl_date
--    deal originally posted on 5/31, that post subsequently voided and reposted on 6/1
--    don't know how i am going to deal with voids yet, manually fix this one (change gl_date and year_month)
--    did not show up in may payroll
truncate sls.changed_deals;
insert into sls.changed_deals
select c.hash as xfm_hash, d.hash as deal_hash,
  case 
    when c.gl_date <> d.gl_date 
      and c.gl_count < 0 and d.unit_count > 0
      and c.buyer_bopname_id = d.buyer_bopname_id 
      and c.deal_status = d.deal_status_code 
      and d.notes <> 'type_2_a' then 'type_2_a'  --30365 3/6
    when c.deal_status in ('none', 'A') and d.deal_status_code = 'U' then 'type_2_b' -- 30745x 3/6
    when c.deal_status = 'deleted' and d.deal_status_code = 'U' 
      and c.gl_date <> d.gl_date and d.notes <> 'type_2_a' then 'type_2_c' -- 30157A
    when d.notes = 'type_2_a' and c.deal_status = 'deleted' then 'type_2_d'
    when d.notes = 'type_2_a' and c.gl_count > 0 and c.bopmast_id = d.bopmast_id then 'type_2_e'
    when d.notes = 'type_2_b' and c.gl_count > 0 and c.deal_status = 'U' then 'type_2_f'
    -- type_2_g: H10234G 5/23/17 the only thing changed was deal status: U -> deleted not sure why no accounting changes
    -- except, this is an intramarket ws that was retailed at honda, that deal was deleted
    -- see below, H10172A, the accounting entry to sale account has been voided
    when c.deal_status = 'deleted' and d.deal_status_code = 'U' then 'type_2_g'
    -- previously uncapped deal (type_2b) has been accepted
    when d.notes = 'type_2_b' and d.deal_status_code = 'none' and c.deal_status = 'A' then 'do nothing'
    when c.deal_status <> d.deal_status_code 
      or c.gl_count <> d.unit_count
      or (c.gl_date <> d.gl_date 
        and -- 29164B: same year_month and no change in status and no change in count = type 1
            (extract(year from c.gl_date) * 100) + extract(month from c.gl_date) <>  (extract(year from d.gl_date) * 100) + extract(month from d.gl_date) 
            OR c.deal_status <> d.deal_status_code 
            or c.gl_count <> d.unit_count)
         then 'DANGER WILL ROBINSON'      
    else 'type_1'
  end as change_type
from ( -- c: most recent xfm rows 1 row per store/bopmast_id
  select *
  from sls.xfm_deals a
  where a.seq = (
    select max(seq)
    from sls.xfm_deals
    where store_code = a.store_code
      and bopmast_id = a.bopmast_id)) c
inner join (-- d: most recent deals rows 1 row per store/bopmast_id
  select *
  from sls.deals a
  where a.deal_status <> 'deleted'
    and a.seq = (
      select max(seq)
      from sls.deals
      where store_code = a.store_code
        and bopmast_id = a.bopmast_id)) d on c.store_code = d.store_code
    and c.bopmast_id = d.bopmast_id  
  and c.hash <> d.hash; 
        
/*
5/10/17
buyer_bopname_id changed on the deal, nothing else
it is just a type 1

-- this didn't work, the hash also has to be updated
-- will have to come up with an algorithm for buyer changing

update sls.deals
set buyer_bopname_id = 1040823,
    notes = 'type_1'
where store_code = 'ry1'
  and bopmast_id = 41864;

update sls.deals
set hash = '2fba8da0b4d4cae03146492fa9c3c9ce'
where store_code = 'ry1'
  and bopmast_id = 41864;
  
*/

-- and here are the details
-- manually going throught the different possibilities and then building the queries to handle 
-- each type_2 situation 
-- 4/26 to handle 29523R, had to add xfm_deals max(run_date), 2 rows with same hash
select 'xfm' as source, b.change_type, run_date, store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  gl_date, gl_count, '' as notes, a.hash
-- select *  
from sls.xfm_deals a
inner join sls.changed_deals b on a.hash = b.xfm_hash
where a.run_date = (
  select max(run_date)
  from sls.xfm_deals
  where store_code = a.store_code
    and bopmast_id = a.bopmast_id)
--   and b.change_type = 'type_2_a'
union
select 'deal', b.change_type, run_date, store_code, bopmast_id, deal_status_code,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  gl_date, unit_count, notes, a.hash
from sls.deals a
inner join sls.changed_deals b on a.hash = b.deal_hash
where a.seq = (
  select max(seq)
  from sls.deals
  where store_code = a.store_code
    and bopmast_id = a.bopmast_id)  
order by stock_number, bopmast_id, source 

-- -- -- type_1 3/6
-- type_1: there are differences between xfm_deals and deals but not in any type_2 trigger attribute 
-- 28855A, 29655, 30245A, 28970R
-- select a.*
-- delete won't work, try the update route
-- all values from xfm_deals
-- except (from deals: b): seq, year_month, gl_date, deal_status 
-- ! it worked
update sls.deals w
set run_date = x.run_date, 
    store_code = x.store_code, 
    bopmast_id = x.bopmast_id, 
    deal_status_code = x.deal_status_code,
    deal_type_code = x.deal_type,
    sale_type_code = x.sale_type,
    sale_group_code = x.sale_group,
    vehicle_type_code = x.vehicle_type,
    stock_number = x.stock_number, 
    vin = x.vin,
    odometer_at_sale = x.odometer_at_sale, 
    buyer_bopname_id = x.buyer_bopname_id, 
    cobuyer_bopname_id = x.cobuyer_bopname_id,
    primary_sc = x.primary_sc, 
    secondary_sc = x.secondary_sc, 
    fi_manager = x.fi_manager, 
    origination_date = x.origination_date,
    approved_date = x.approved_date, 
    capped_date = x.capped_date, 
    delivery_date = x.delivery_date, 
    gap = x.gap, 
    service_contract = x.service_contract,
    total_care = x.total_care, 
    seq = x.seq, 
    year_month = x.year_month, 
    unit_count = x.gl_count,
    gl_date = x.gl_date, 
    deal_status = x.deal_status, 
    notes = x.notes, 
    hash = x.hash
from (    
  select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
    a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
    a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
    a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
    a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
    a.total_care, 
    b.seq, 
    -- 29164B: gl_date from xfm
    b.year_month, a.gl_count, a.gl_date, b.deal_status, 'type_1' as notes, a.hash
  from sls.changed_deals z
  inner join  sls.xfm_deals a on z.xfm_hash = a.hash
    and z.change_type = 'type_1'
  inner join sls.deals b on z.deal_hash = b.hash
    and z.change_type = 'type_1') x
where w.store_code = x.store_code
  and w.bopmast_id = x.bopmast_id
  and w.seq = x.seq;

-- 4/25
-- type_2_a 3/6
-- 30635: xfm_deals.gl_date <> deals.gl_date and xfm_deals.gl_count <> deals.unit_count and xfm_deals.buyer_bopname_id = deals.buyer_bopname_id
-- type_2_a
-- 5/9 new understanding, ie, if unit_count changes, then gl_date and year_month change
-- new row: all values from xfm_deals except generate new seq
insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
  sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date,
  approved_date, capped_date, delivery_date, gap, service_contract,
  total_care, seq, year_month, unit_count,
  gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care, 
  (select max(seq) + 1 from sls.deals where store_code = b.store_code and bopmast_id = b.bopmast_id) as seq,
  (select year_month from dds.dim_date where the_date = a.gl_date) as year_month, 
  a.gl_count, 
  a.gl_date, a.deal_status, 'type_2_a' as notes, 
  a.hash
from sls.changed_deals z
inner join  sls.xfm_deals a on z.xfm_hash = a.hash
  and z.change_type = 'type_2_a'
inner join sls.deals b on z.deal_hash = b.hash
  and z.change_type = 'type_2_a';


-- 4/25
-- type_2_b 3/6
-- 30745X: xfm_deals.deal_status <> deals.deal_status_code (deal_status)
-- type_2_b: xfm_deals.deal_status changed from U to none
--    un-capped in bopmast but not in accounting
-- new row: all values from xfm_deals except (from deals: b): generate new seq, year_month (doesn't exist in xfm), 
--    set unit count to -1 (uncapped)

insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
  sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date,
  approved_date, capped_date, delivery_date, gap, service_contract,
  total_care, seq, year_month, unit_count,
  gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care, 
  (select max(seq) + 1 from sls.deals where store_code = b.store_code and bopmast_id = b.bopmast_id) as seq,
  b.year_month, 
  -1 as unit_count, 
  a.gl_date, 
  case
    when a.deal_status = 'U' then 'capped'
    when a.deal_status = 'A' then 'accepted'
    else 'none'
  end as deal_status, 'type_2_b' as notes, 
  a.hash
from sls.changed_deals z
inner join  sls.xfm_deals a on z.xfm_hash = a.hash
  and z.change_type = 'type_2_b'
inner join sls.deals b on z.deal_hash = b.hash
  and z.change_type = 'type_2_b';


-- 4/25
-- type_2_c 3/6
-- 30157A
-- deal status changed from U to deleted
-- c.deal_status = 'deleted' and d.deal_status_code = 'U' 
--       and c.gl_date <> d.gl_date
-- new row: all values from xfm_Deals except (from deals: b): generate new seq, gl_date (new gl_date), 
--    generate new year_month based on new gl_date, 
--    this is an unwind, deal has been deleted from bopmast, count has to come from xfm (a)
insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
  sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date,
  approved_date, capped_date, delivery_date, gap, service_contract,
  total_care, seq, year_month, unit_count,
  gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care, 
  (select max(seq) + 1 from sls.deals where store_code = b.store_code and bopmast_id = b.bopmast_id) as seq,
  (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer as year_month,
  a.gl_count, 
  a.gl_date, 'deleted' as deal_status, 'type_2_c' as notes, 
  a.hash
from sls.changed_deals z
inner join  sls.xfm_deals a on z.xfm_hash = a.hash
  and z.change_type = 'type_2_c'
inner join sls.deals b on z.deal_hash = b.hash
  and z.change_type = 'type_2_c';

--4/26
-- type_2_d
-- previous state was 2_a (unwound in accounting) as indicated by deals.notes
-- current state is deleted, xfm_deals.deal_status = deleted
-- unit count is alreay negative
-- all that's needed is to update run_date, deal_status_code & deal_status to deleted, and notes to type_2_d

update sls.deals a
set run_date = x.run_date,
    deal_status_code = 'deleted',
    deal_status = 'deleted',
    notes = 'type_2_d'
-- select * from sls.deals where hash in (    
from (    
  select n.run_date, m.deal_hash
  from sls.changed_deals m
  inner join sls.xfm_deals n on m.xfm_hash = n.hash
  where m.change_type = 'type_2_d') x
where a.hash = x.deal_hash;

-- 4/26
-- type_2_e 3/8 30635
-- deals.notes = type_2_a, deals.unit_count = -1, xfm_deals.gl_count = 1
-- a deal that has been previously unwound in accounting has been reposted
-- need a new row with a positive unit count
-- new row: all values from xfm_Deals except generate new seq
insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
  sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date,
  approved_date, capped_date, delivery_date, gap, service_contract,
  total_care, seq, year_month, unit_count,
  gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care, 
  (select max(seq) + 1 from sls.deals where store_code = b.store_code and bopmast_id = b.bopmast_id) as seq,
  (select year_month from dds.dim_date where the_date = a.gl_date) as year_month,
  a.gl_count, 
  a.gl_date, 
  case
    when a.deal_status = 'U' then 'capped'
    when a.deal_status = 'A' then 'accepted'
    else 'none'
  end as deal_status, 
  'type_2_e' as notes, 
  a.hash
from sls.changed_deals z
inner join  sls.xfm_deals a on z.xfm_hash = a.hash
  and z.change_type = 'type_2_e'
inner join sls.deals b on z.deal_hash = b.hash
  and z.change_type = 'type_2_e';

-- 4/26
-- type_2_f 3/8 30745X
-- deals.notes = type_2_b, deals.unit_count = -1, xfm_deals.gl_count = 1
-- a deal previously un-capped in bopmast has been re-capped
-- need a new row with a positive unit count
-- 5/9 unit count changes, so gl_date & year_month change
-- new row: all values from xfm_Deals except generate new seq, 
-- 29523R 2 rows with same hash, limit xfm_deals to most recent run_date
insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
  sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date,
  approved_date, capped_date, delivery_date, gap, service_contract,
  total_care, seq, year_month, unit_count,
  gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care, 
  (select max(seq) + 1 from sls.deals where store_code = b.store_code and bopmast_id = b.bopmast_id) as seq,
  (select year_month from dds.dim_date where the_date = a.gl_date) as year_month,
  a.gl_count, 
  a.gl_date, 
  case
    when a.deal_status = 'U' then 'capped'
    when a.deal_status = 'A' then 'accepted'
    else 'none'
  end as deal_status,
  'type_2_f' as notes, 
  a.hash
from sls.changed_deals z
inner join  sls.xfm_deals a on z.xfm_hash = a.hash
  and z.change_type = 'type_2_f'
  and a.run_date = (
    select max(run_date)
    from sls.xfm_deals
    where store_code = a.store_code
      and bopmast_id = a.bopmast_id)
inner join sls.deals b on z.deal_hash = b.hash
  and z.change_type = 'type_2_f';

-- 5/23/17
-- type_2_g h10234G
-- deal_status changes from U to deleted, NO OTHER CHANGES
-- force unit count to -1
-- 6/2/17 H10172A, sold 5/23, deleted 6/1, only sale acct entry voided on 6/2, therefore the gl_date does not change
-- as currently configured, that would generate a year_month of 201705, the problem with that is that the sale is
-- credited in 201705, and retroactively this would cancel that attribution.
-- the unwind should be in 201706, which means that this script needs to set year_month based on run_date
insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
  sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date,
  approved_date, capped_date, delivery_date, gap, service_contract,
  total_care, seq, year_month, unit_count,
  gl_date, deal_status, notes, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care, 
  (select max(seq) + 1 from sls.deals where store_code = b.store_code and bopmast_id = b.bopmast_id) as seq,
  (select year_month from dds.dim_date where the_date = a.run_date),
  -1 as unit_count, 
  a.gl_date, 'deleted' as deal_status,
  'type_2_g' as notes, 
  a.hash
from sls.changed_deals z
inner join  sls.xfm_deals a on z.xfm_hash = a.hash
  and z.change_type = 'type_2_g'
  and a.run_date = (
    select max(run_date)
    from sls.xfm_deals
    where store_code = a.store_code
      and bopmast_id = a.bopmast_id)
inner join sls.deals b on z.deal_hash = b.hash
  and z.change_type = 'type_2_g';


