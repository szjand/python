﻿/*

-- 6/22/17 --------------------------------------------------
looks like i did not finish this, probably fixed h10165a but did not change the code
30062B
seq 1: 5-22 capped
seq 2: 6-13 type_2_b, status -> A from U, unit count -1, BUT year_month is still 201705
seq 3: 6-15 type_2_f, recapped, now year_month is 201706

so what i am proposing is that type_2_b should generate a year_month from the gl date
the struggle is where to document/maintain this
    1. this file
    2. e/python projects/sales_pay_plan/sql/deals_p2.sql (the development script)
    3.~luigi/project/luigi/car_deals.py (in git)
    4. smartdraw: sales_pay_plans.sdr

even though the code is mostly sqL, leaning toward #3 because it is in source control     

-------------------------------------------------------------------
*/


this came up with h10165a
capped on 4/27/17
on 5/4 unwound in bopmast (to accepted) and in accounting
currently type_2_b changes the deal_date to 5/4, but does not change the year_month,
the whole issue of what is the fucking deal date
this should be a negative deal count for may

do i try to establish an original deal deate?
don''t know what to do 
maybe change deal_date to gl_date, year_month to unit_count_month

hmm, type_2_b definitin:
2_b: deal_status(xfm_deals) = none or A
     deal_status_code(deals) = U
     deal has been uncapped in bopmast

H10165a was also reversed in accounting, is that always the case?     
nope, 29470 was not reversed in accounting
30625a not reversed in accounting
h9734 not reversed in accounting

type_2_b deals  reversed in accounting
H9734             No
H8019a            No
H10165A           Yes
30745x            No
29501a            No
29523R            no
30154             no
30349xxa          no
30508             yes
30625a            no
29470             no
*/
-- so, let's look at the eventual status of all type_2_b deals
-- wtf, H9734 capped on 3/16 uncapped on 4/10, still in accpeted status, should definitely been a negative unit count in april
select bopmast_id, stock_number, seq, deal_status_code, buyer_bopname_id, year_month, unit_count, deal_date, deal_status, notes
from sls.deals a
where exists (
  select a 
  from sls.deals 
  where notes = 'type_2_b'
    and store_code = a.store_code
    and bopmast_id = a.bopmast_id)
order by bopmast_id, seq   
-- new cars
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '29470'
  and journal_code = 'VSN'
  and account_type = 'sale'
  and department = 'new vehicle' 
-- used cars
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '30625a'
  and journal_code = 'VSU'
  and account_type = 'sale'
  and department = 'used vehicle'   


select * from sls.deals where delivery_date <> deal_date


-- of the type_2_b deals, which ones have year_month different that deal_date
select *
from (
  select bopmast_id, stock_number, seq, deal_status_code, buyer_bopname_id, year_month, 
    unit_count, deal_date, deal_status, notes,
    (select year_month from dds.dim_date where the_date = a.deal_date) as deal_date_year_month
  from sls.deals a
  where exists (
    select a 
    from sls.deals 
    where notes = 'type_2_b'
      and store_code = a.store_code
      and bopmast_id = a.bopmast_id)) x
where year_month <> deal_date_year_month      
order by bopmast_id, seq 

select * 
from sls.deals
where stock_number = '30508'


select *
from (
  select bopmast_id, stock_number, seq, deal_status_code, buyer_bopname_id, year_month, 
    unit_count, deal_date, deal_status, notes,
    (select year_month from dds.dim_date where the_date = a.deal_date) as deal_date_year_month
  from sls.deals a) x
where year_month <> deal_date_year_month  


-- ok, as i consider automating all this, looking thru all the different types, thinking they need a more
-- concise definition of each one
-- i am also suspicious of the case statement actually covering all possibilities, should do a permutations drawing
-- am uneasy of else pointing to type_1, would like some sort of safety, if one of the critical elements changes
-- and it is not picked up in the case statement, then flag it for inspection
-- then else to type_1

select bopmast_id, stock_number, seq, deal_status_code, buyer_bopname_id, year_month, unit_count, deal_date, deal_status, notes
from sls.deals a
where exists (
  select a 
  from sls.deals 
  where notes = 'type_2_a'
    and store_code = a.store_code
    and bopmast_id = a.bopmast_id)
order by bopmast_id, seq   

select *
from sls.xfm_deals a
where stock_number = '30491'


-- does delivery date change?
select store_code, bopmast_id
from (
  select store_code, bopmast_id, delivery_date
  from sls.xfm_deals
  group by store_code, bopmast_id, delivery_date) a
group by store_code, bopmast_id 
having count(*) > 1

-- only 2 (of 1432), and in each case it is a change when the deal status goes from none to U or A
select *
from sls.xfm_deals
where bopmast_id in (41586,41059)
order by bopmast_id, seq

-- deals where delivery date <> deal date
select stock_number, seq, deal_status_code, capped_date, delivery_date, year_month, deal_date, deal_status, notes
from sls.deals a
where exists (
  select 1
  from sls.deals
  where delivery_date <> deal_date
    and store_code = a.store_code
    and bopmast_id = a.bopmast_id)
  order by stock_number, seq  


select * from sls.xfm_deals where stock_number = '28844A'

select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '28662a'
  and journal_code = 'VSU'
  and account_type = 'sale'
  and department = 'used vehicle'   



