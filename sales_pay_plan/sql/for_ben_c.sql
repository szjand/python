﻿-- include make, buyer, co-buyerm year_month
-- exclude buyer_bopname_id
-- include but don't display bopmast_id, seq
-- is this the query to join to bopname, inpmast ?
select 
  (select team from sls.folks where employee_number = x.primary_sc) 
  || 
    case
      when x.secondary_sc is null then ''
      else ' / ' || (select team from sls.folks where employee_number = x.secondary_sc) end as team,
  x.store_code, x.bopmast_id, x.seq, x.unit_count, x.sale_type, x.sale_group, x.new_used, 
  x.stock_number, x.vin, x.model, x.odometer_at_sale, x.buyer_bopname_id, 
  case 
    when x.store_code = 'ry1' then (select last_name from sls.personnel where employee_number = x.primary_sc)
    when x.store_code = 'ry2' then (select last_name from sls.personnel where employee_number = x.primary_sc)
  end as primary_sc,
  case 
    when x.store_code = 'ry1' and x.secondary_sc <> 'none' then (select last_name from sls.personnel where employee_number = x.secondary_sc and store_code = x.store_code)
    when x.store_code = 'ry2' and x.secondary_sc <> 'none' then (select last_name from sls.personnel where employee_number = x.secondary_sc and store_code = x.store_code)
  end as secondary_sc,  
  coalesce((select last_name from sls.personnel where employee_number = x.fi_manager and store_code = x.store_code), 'none') as fi_manager,  
  x.gap, x.service_contract, x.total_care,
  coalesce(x.fi_gross, 0) as fi_gross,
  coalesce(x.front_gross, 0) as front_gross,  
  coalesce(y.amount, 0) as sfe
-- select *  
from sls.payroll_deals x --order by stock_number
left join sls.sfe y on x.vin = y.vin
order by team, primary_sc, bopmast_id, seq

