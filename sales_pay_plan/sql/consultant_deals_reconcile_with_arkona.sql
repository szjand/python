﻿/*
8/28/17 discrepancies with salesperson analysis detail
30952 garceau capped 8/27, no sale entry in account 1432012, 1432012 posted with control 30952a
31694 haley july deal
31595 hanson 1429301 posted with control 31595a
31493xx tarr july deal

need a test for new car sale account posted with used car stocknumber
*/

select *
from sls.ext_deals 
where stock_number = '31595'

select *
from sls.xfm_deals 
where stock_number = '31595'

select *
from sls.deals 
where stock_number = '31493xx'

select *
from sls.ext_accounting_deals
where control = '31595'




select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
-- select sum(amount)  
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '31621a'
order by account



-- need a test for new car sale account posted with used car stocknumber
select a.control, c.account, c.description, sum(amount) 
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join sls.deals_accounts_routes f on c.account = f.gl_account
  and f.page between 5 and 15
where a.post_status = 'Y'
  and b.year_month = (select year_month from sls.months where open_closed = 'open')
  and right(control,1) not in ('1','2','3','4','5','6','7','8','9', '0','R')
group by a.control, c.account, c.description
having sum(amount) <> 0  
order by a.control 
