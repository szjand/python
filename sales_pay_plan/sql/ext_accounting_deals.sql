﻿
1. instead of using cdc extract of glptrns, use fact_gl
-- 1st cut
drop table if exists wtf;
create temp table wtf as
select d.the_date, b.account, b.description, c.journal_code, b.department_code, a.control, a.amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.current_row = true
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
where b.account_type = 'sale'
  and b.department_code in ('nc','uc') 
  and c.journal_code in ('vsn','vsu')
  and a.post_status = 'Y'
  and d.the_date between '01/01/2017' and '02/28/2017';

-- probably don't need the amount
-- actually account and description will be handy in analysis
-- add the transaction description, could be helpful with intra market wholesale
drop table if exists sls.ext_accounting_deals cascade;
create table sls.ext_accounting_deals (
  run_date date not null,
  gl_date date not null,
  control citext not null, 
  amount numeric(12,2),
  unit_count integer not null,
  account citext not null, 
  account_description citext not null,
  gl_description citext not null,
  constraint ext_accounting_deals_pkey primary key (gl_date,control));

-- going with the grouping
-- can't go with gl_description as part of grouping, fucks up the offsetting entries
-- but for the purpose of intra market ws, max(description) should be good enuf
-- initial load
insert into sls.ext_accounting_deals
select '02/28/2017', coalesce(the_date, '12/31/9999')::date as the_date, 
  control, amount,
  coalesce(case when amount < 0 then 1 else -1 end, 0) as the_count,
  account, account_description, gl_description
from (
  select d.the_date, a.control, sum(a.amount) as amount,
    b.account, b.description as account_description, max(e.description) as gl_description 
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key 
    and b.current_row = true
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.dim_date d on a.date_key = d.date_key
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where b.account_type = 'sale'
    and b.department_code in ('nc','uc') 
    and c.journal_code in ('vsn','vsu')
    and a.post_status = 'Y'
    and d.the_date between '01/01/2017' and '02/28/2017'
  group by d.the_date, a.control, b.account, b.description
  having sum(amount) <> 0) x 
where not exists (
    select 1
    from sls.ext_accounting_deals
    where control = x.control
      and gl_date = x.the_date);

select * from sls.ext_accounting_deals
/* 
-- this is the equivalent of scpp.xfm_glptrns
create temp table pseudo_xfm_glptrns as
select a.control, a.the_date,
  sum(case when a.amount < 0 then 1 else -1 end) as unit_count
from wtf a
left join (-- exclude self cancelling multiple entries on a day
  select control, the_date
  from ( -- assign unit count to rows w/multiple rows per date
    select a.control, a.the_date,
      case when a.amount < 0 then 1 else -1 end as the_count
    from wtf a
    inner join ( --multiple rows per date
      select control, the_date
      from wtf
      group by control, the_date
      having count(*) > 1) b on a.control = b.control and a.the_date = b.the_date) x
  group by control, the_date
  having sum(the_count) = 0) b on a.control = b.control and a.the_date = b.the_date
where b.control is null -- exclude cancelling multiple entries on a day
group by a.control, a.the_date
order by control

-- hell, do a group to eliminate the self cancelling, try it at least
-- should be ok as long the the date is in the grouping
select 'detail', the_date, control, account, amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where control = '29160'
  and account = '1411001'

union all

select 'group', the_date, control, account, sum(amount)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where control = '29160'
  and account = '1411001'
group by the_date, control, account   

select the_date, control,
  case when amount < 0 then 1 else -1 end as the_count,
  amount
from (
  select d.the_date, b.account, a.control, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.current_row = true
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.dim_date d on a.date_key = d.date_key
  where b.account_type = 'sale'
    and b.department_code in ('nc','uc') 
    and c.journal_code in ('vsn','vsu')
    and a.post_status = 'Y'
    and d.the_date between '01/01/2017' and '02/28/2017'
  group by d.the_date, b.account, a.control
  having sum(amount) <> 0
) x order by control, the_date


struggling a bit with getting started
in this initial cut, there , deals with multiple rows
do i start with just the intial row, then process the subsequent rows as updates:
select * 
from pseudo_xfm_glptrns a
inner join (
  select control
  from pseudo_xfm_glptrns
  group by control
  having count(*) > 1) b on a.control = b.control
order by a.control, a.the_date  
*/

-- 4/20 -------------------------------------------------------------------------------------
start the dailys

only change is making it a separate script (sql & py)

select *
from sls.ext_accounting_deals a
-- -- 4/23 starting over again, just february transactions this time
truncate sls.ext_accounting_deals;
insert into sls.ext_accounting_deals
select '02/28/2017', the_date, control, amount,
  case when amount < 0 then 1 else -1 end as the_count,
  account, account_description, gl_description
from (
  select d.the_date, a.control, sum(a.amount) as amount,
    b.account, b.description as account_description, max(e.description) as gl_description
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.current_row = true
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.dim_date d on a.date_key = d.date_key
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where b.account_type = 'sale'
    and b.department_code in ('nc','uc')
    and c.journal_code in ('vsn','vsu')
    and a.post_status = 'Y'
    and d.the_date between '02/01/2017' and '02/28/2017'
  group by d.the_date, a.control, b.account, b.description
  having sum(amount) <> 0) x
where not exists (
    select 1
    from sls.ext_accounting_deals
    where control = x.control
      and the_date = x.the_date)
  order by control, the_date;

 -- 4/22 -----------------------------------------------------------------------------------
 starting over (again)
 
3/1 
select * from sls.ext_accounting_deals order by run_date desc 
11 new rows