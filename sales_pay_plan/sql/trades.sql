﻿drop table if exists deals;
-- proving to be somewhat tricky to get the count correct
create temp table deals as
select a.store_code, a.bopmast_id, seq, unit_count,
  case sale_type_code
    when 'F' then 'fleet'
    when 'I' then 'intra_market_ws'
    when 'L' then 'lease'
    when 'R' then 'retail'
    when 'W' then 'wholesale'
  end as sale_type,
  b.sale_group, a.vehicle_type_code as new_used, 
  a.stock_number, a.vin, e.model, a.odometer_at_sale, a.buyer_bopname_id, 
  case 
    when a.store_code = 'ry1' then (select employee_number from sls.personnel where ry1_id = a.primary_sc)
    when a.store_code = 'ry2' then (select employee_number from sls.personnel where ry2_id = a.primary_sc)
  end as primary_sc,
  case 
    when a.store_code = 'ry1' and a.secondary_sc <> 'none' then (select employee_number from sls.personnel where ry1_id = a.secondary_sc and store_code = a.store_code)
    when a.store_code = 'ry2' and a.secondary_sc <> 'none' then (select employee_number from sls.personnel where ry2_id = a.secondary_sc and store_code = a.store_code)
  end as secondary_sc,  
  coalesce((select employee_number from sls.personnel where fi_id = a.fi_manager and store_code = a.store_code), 'none') as fi_manager
-- select *  
from sls.deals a
left join sls.sale_groups b on a.store_code = b.store_code and a.sale_group_code = b.code
left join (-- fi
  select * 
  from (
    select store, control, 
      sum(case when line in (1,2,6,7,11,12,16,17) then -1 * amount end) as fi_gross
    from (
      select d.*, b.description as account_description, 
        c.the_date, a.control, a.doc, a.ref, a.amount, aa.journal_code, e.description
      from fin.fact_gl a
      inner join fin.dim_journal aa on a.journal_key = aa.journal_key
      inner join fin.dim_account b on a.account_key = b.account_key
      inner join dds.dim_date c  on a.date_key = c.date_key
        and c.year_month = 201704
      inner join (
        select store, b.page, b.line, b.line_label, col, d.gl_account
        from fin.fact_fs a
        inner join fin.dim_fs b on a.fs_key = b.fs_key
          and b.year_month = 201703
          and b.page = 17
          and b.line between 1 and 20
        inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
        inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
        group by store, b.page, b.line, b.line_label, col, d.gl_account
        having sum(amount) <> 0) d on b.account = d.gl_account
      inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
      where post_status = 'Y') a
    group by store, control) x
  where fi_gross is not null) c on a.store_code = c.store and a.stock_number = c.control
left join ( -- front gross
  select control, sum(-1 * a.amount) as front_gross
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- new car accounts
    select distinct d.gl_account
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201703
      and (
        (b.page between 5 and 15) or -- new cars
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
  where a.post_status = 'Y'
    and b.year_month = 201704
--     and c.store_code = 'RY1'
  group by control) d on a.stock_number = d.control  
-- add model for ben
left join dds.ext_inpmast e on a.stock_number = e.inpmast_stock_number  
where a.year_month = 201704
--   and seq = (select max(seq) from sls.deals where store_code = a.store_code and bopmast_id = a.bopmast_id)
  and a.primary_sc <> 'HSE'  


select primary_sc, secondary_sc, stock_number, new_used, sale_type, string_agg(distinct trade, ',')
from (
  select 
    (select last_name from sls.personnel where employee_number = a.primary_sc) as primary_sc,
    (select last_name from sls.personnel where employee_number = a.secondary_sc) as secondary_sc,
    a.stock_number, a.new_used, a.sale_type, b.stock_ as trade
  from deals a
  left join arkona.ext_boptrad b on a.store_code = b.company_number
    and a.bopmast_id = b.key
  where left(primary_sc, 1) = '1') x
group by primary_sc, secondary_sc, stock_number, new_used, sale_type
order by primary_sc, length(stock_number), stock_number

-- this is close, does not accomodate split deals
-- so Haley is 14 instead of 13, Warmack & Croaker don't show halves
select primary_sc, count(distinct stock_number),
  sum(case when trade is not null then 1 else 0 end) as trades
from (  
  select primary_sc, secondary_sc, stock_number, new_used, sale_type, string_agg(distinct trade, ',') as trade
  from (
    select 
      (select last_name from sls.personnel where employee_number = a.primary_sc) as primary_sc,
      (select last_name from sls.personnel where employee_number = a.secondary_sc) as secondary_sc,
      a.stock_number, a.new_used, a.sale_type, b.stock_ as trade
    from deals a
    left join arkona.ext_boptrad b on a.store_code = b.company_number
      and a.bopmast_id = b.key
    where left(primary_sc, 1) = '1') x
  group by primary_sc, secondary_sc, stock_number, new_used, sale_type) x
group by primary_sc
order by primary_sc