﻿/*
30491
2/28: sold and capped
3/31: unwound and capped
3/31: accounting: sales: 58381
                  cogs:  58450
      which results in a balance of 69 which generates a -1 unit count
in the old script, excluded multiple entries on a day that cancel each other out
by polarity
remember, that was the flaw (some year(s) ago) in how we were trying to generate
a unit count from glptrns, assuming that count depended on zeroing the sum of daily entries

so in this case went with the grouping to try to eliminate offsetting entries, which proved to 
be close, buy as 30491 shows, if the offsetting entries are not equal, we have a problem

feeling now that that is too coarse,  EXCEPT, all i am dealing with here is unit count
not the total gross
so, is that good enuf????
*/
select * from sls.ext_deals where stock_number = '30491'

select * from sls.xfm_deals where stock_number = '30491'

select * from sls.deals where stock_number = '30491'

select * from sls.ext_accounting_deals where control = '30491'

delete from sls.ext_accounting_deals where control = '30491' and gl_date = '03/13/2017'

select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '30491'
  and journal_code = 'VSN'
  and account_type = 'sale'
  and department = 'new vehicle' 

-- this is the query (inner detail) from ext_accounting_deals
select d.the_date, a.control, amount,
  b.account, b.description as account_description, e.description
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.current_row = true
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where b.account_type = 'sale'
  and control = '30491'
  and b.department_code in ('nc','uc')
  and c.journal_code in ('vsn','vsu')
  and a.post_status = 'Y'
  and d.the_date between '02/01/2017' and current_date
group by d.the_date, a.control, b.account, b.description
having sum(amount) <> 0  
order by control

-- let's try to generate some history of multiple entries per day
-- here is a year's worth, not that many, 130 rows
select * 
from (
  select d.the_date, a.control, amount,
    b.account, b.description as account_description, e.description
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.current_row = true
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.dim_date d on a.date_key = d.date_key
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where b.account_type = 'sale'
    and b.department_code in ('nc','uc')
    and c.journal_code in ('vsn','vsu')
    and a.post_status = 'Y') x
inner join (
  select d.the_date, a.control
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
    and b.current_row = true
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.dim_date d on a.date_key = d.date_key
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where b.account_type = 'sale'
    and b.department_code in ('nc','uc')
    and c.journal_code in ('vsn','vsu')
    and a.post_status = 'Y'
    and d.the_date between '01/01/2016' and current_date
  group by d.the_date, a.control 
  having count(*) > 1) y on x.the_date = y.the_date and x.control = y.control

-- i think the exclusion approach willl work, at what level do i inject the exclusion
-- may need to abandon the grouping having sum(amount) <> 0

-- shit to exclude: 

select x.control, x.the_date
from (
  select aa.control, aa.the_date,
    case when aa.amount < 0 then 1 else -1 end as the_count
  from (  
    select d.the_date, a.control, amount,
      b.account, b.description as account_description, e.description
    from fin.fact_gl a
    inner join fin.dim_account b on a.account_key = b.account_key
      and b.current_row = true
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.dim_date d on a.date_key = d.date_key
    inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
    where b.account_type = 'sale'
      and b.department_code in ('nc','uc')
      and c.journal_code in ('vsn','vsu')
      and a.post_status = 'Y'
      and d.the_date between '01/01/2016' and current_date) aa
  inner join ( -- stock numbers with multiple entries per day (only 58 since 1/1/16)
    select d.the_date, a.control
    from fin.fact_gl a
    inner join fin.dim_account b on a.account_key = b.account_key
      and b.current_row = true
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.dim_date d on a.date_key = d.date_key
    inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
    where b.account_type = 'sale'
      and b.department_code in ('nc','uc')
      and c.journal_code in ('vsn','vsu')
      and a.post_status = 'Y'
      and d.the_date between '01/01/2016' and current_date
    group by d.the_date, a.control
    having count(*) > 1) bb on aa.control = bb.control and aa.the_date = bb.the_date) x
group by control, the_date
having sum(the_count) = 0    

-- looking good
-- let's build a base table
drop table if exists base;
create temp table base as
select d.the_date, a.control, amount,
  b.account, b.description as account_description, e.description
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.current_row = true
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where b.account_type = 'sale'
  and b.department_code in ('nc','uc')
  and c.journal_code in ('vsn','vsu')
  and a.post_status = 'Y'
  and d.the_date between '02/01/2017' and current_date;

-- build exclusions from base table  
select x.control, x.the_date
from (
  select aa.control, aa.the_date,
    case when aa.amount < 0 then 1 else -1 end as the_count
  from base aa  
  inner join ( -- multiple rows per date
    select control, the_date
    from base
    group by control, the_date
    having count(*) > 1) bb on aa.control = bb.control and aa.the_date = bb.the_date) x
group by x.control, x.the_date
having sum(the_count) = 0


select *
from base aa
left join (
  select x.control, x.the_date
  from (
    select a.control, a.the_date,
      case when a.amount < 0 then 1 else -1 end as the_count
    from base a  
    inner join ( -- multiple rows per date
      select control, the_date
      from base
      group by control, the_date
      having count(*) > 1) b on a.control = b.control and a.the_date = b.the_date) x
  group by x.control, x.the_date
  having sum(the_count) = 0) bb on aa.the_date = bb.the_date and aa.control = bb.control
where bb.control is null   
order by aa.control

-- this looks like a go
-- implement in linux as part of luigi
-- change nothing in the daily in body shop pay
-- compare
select current_date, the_date, control, amount, unit_count,
  account, account_description, gl_description
from (  -- ccc all rows
  select aa.the_date, aa.control, sum(aa.amount) as amount, aa.account, 
    aa.account_description, max(aa.description) as gl_description,
    sum(case when aa.amount < 0 then 1 else -1 end) as unit_count
  from base aa
  left join (-- multiple rows per date to exclude
    select x.control, x.the_date
    from ( -- assign unit count to rows w/multiple rows per date
      select a.control, a.the_date,
        case when a.amount < 0 then 1 else -1 end as the_count
      from base a  
      inner join ( -- multiple rows per date
        select control, the_date
        from base
        group by control, the_date
        having count(*) > 1) b on a.control = b.control and a.the_date = b.the_date) x
    group by x.control, x.the_date
    having sum(the_count) = 0) bb on aa.the_date = bb.the_date and aa.control = bb.control
  where bb.control is null -- this is the line that excludes the multiple entry vehicles   
  group by aa.the_date, aa.control, aa.account, aa.account_description) ccc
where not exists (
  select 1
  from sls.ext_accounting_deals
  where control = ccc.control
    and gl_date = ccc.the_date)  


create table sls.ext_accounting_base (
  the_date date not null,
  control citext not null,
  amount numeric(8,2) not null,
  account citext not null,
  account_description citext not null,
  description citext not null);
create index on sls.ext_accounting_base(the_date);  
create index on sls.ext_accounting_base(control);


