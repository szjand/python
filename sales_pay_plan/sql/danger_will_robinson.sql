﻿
hash exposed by car_deals.py DealsChangedRows.run query:
/*
                    select c.hash as xfm_hash, d.hash as deal_hash,
                      case
                        -- 9/18/17: when deal status is none/none ignore
--                         when d.deal_status_code = 'none' and c.deal_status = 'none' then 'ignore'
                        -- 8/8/17: exceptional anomalies to ignore, 31112A, see danger_will_robinson.sql
                        when c.bopmast_id in (43283,13634,43827) then 'ignore'
                        when c.gl_date <> d.gl_date
                          and c.gl_count < 0 and d.unit_count > 0
                          and c.buyer_bopname_id = d.buyer_bopname_id
                          and c.deal_status = d.deal_status_code
                          and d.notes <> 'type_2_a' then 'type_2_a'  --30365 3/6
                        when c.deal_status in ('none', 'A') and d.deal_status_code = 'U' then 'type_2_b' -- 30745x 3/6
                        when (
                              c.deal_status = 'deleted'
                                and d.deal_status_code = 'U'
                                and c.gl_date <> d.gl_date
                                and d.notes <> 'type_2_a')
                          or ( -- curious possible anomaly 31135C, 31588XA, no change in gl_date
                              c.deal_status = 'deleted'
                                and d.deal_status_code in ('none','A')
                                and d.notes = 'type_2_b')
                          then 'type_2_c' -- 30157A
                        when d.notes = 'type_2_a' and c.deal_status = 'deleted' then 'type_2_d'
                        when d.notes = 'type_2_a' and c.gl_count > 0 and c.bopmast_id = d.bopmast_id then 'type_2_e'
                        when (d.notes = 'type_2_b' or d.notes = 'type_2_h') and c.gl_count > 0 and c.deal_status = 'U'
                          then 'type_2_f'
                        -- type_2_g: H10234G 5/23/17 the only thing changed was deal status: U -> deleted not sure why no accounting changes
                        -- except, this is an intramarket ws that was retailed at honda, that deal was deleted
                        when c.deal_status = 'deleted' and d.deal_status_code = 'U' then 'type_2_g'
                        -- 7/11/17 30838A: type_2_b with type_1 changes while still just accepted
                        when c.gl_date = d.gl_date and c.deal_status = 'A' and d.deal_status_code = 'A'
                          and d.notes = 'type_2_b' and c.gl_count = 1 and d.unit_count = -1 then 'type_2_h'
                        when d.notes = 'type_2_b' and d.deal_status_code = 'none' and c.deal_status = 'A' then 'do nothing'
                        -- temp handling of unwind anomalies, awaiting response from jeri: danger_will_robinson.sql
                        when c.deal_status <> d.deal_status_code and c.stock_number in ('31126A','31694','31493XX') then
                          'type_2_i'
                        when c.deal_status <> d.deal_status_code
                          or c.gl_count <> d.unit_count
                          or (c.gl_date <> d.gl_date
                            and -- 29164B: same year_month and no change in status and no change in count = type 1
                                (extract(year from c.gl_date) * 100) + extract(month from c.gl_date) <> (extract(year from d.gl_date) * 100) + extract(month from d.gl_date)
                                OR c.deal_status <> d.deal_status_code
                                or c.gl_count <> d.unit_count)
                             then 'DANGER WILL ROBINSON'
                        else 'type_1'
                      end as change_type
                    from ( -- c: most recent xfm rows 1 row per store/bopmast_id
                      select *
                      from sls.xfm_deals a
                      where a.seq = (
                        select max(seq)
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id)) c
                    inner join (-- d: most recent deals rows 1 row per store/bopmast_id
                      select *
                      from sls.deals a
                      where a.deal_status <> 'deleted'
                        and a.seq = (
                          select max(seq)
                          from sls.deals
                          where store_code = a.store_code
                            and bopmast_id = a.bopmast_id)) d on c.store_code = d.store_code
                        and c.bopmast_id = d.bopmast_id
                      and c.hash <> d.hash;
*/                      

select 'xfm' as source, 0 as year_month, run_date, store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  gl_date, gl_count, '' as notes, hash
from sls.xfm_deals 
-- where bopmast_id = 43389
where hash = '6e1804c8c80d5fb61ce83fcc45e94161'  
union
select 'deal', year_month, run_date, store_code, bopmast_id, deal_status_code,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  gl_date, unit_count, notes, a.hash
from sls.deals a
-- where bopmast_id = 43389
where hash = '5553d685148cf637847782ffa108dbde'
order by source, run_date

-- 4/7/18
select * from sls.xfm_deals where bopmast_id = 46610
lots of accounting in/out
only approved_date & capped_date change & gl_date have changed
no substantive changes
hell, it has been a long time since i had one of these, not sure what to do
i am going to cheat since this is the only time this has come up, add it to ignore cloase

select * from sls.deals where bopmast_id = 46610

-- 10/14/17

30865AA
  sold & capped 8/31/17
  unwound 9/6/17
  sold & capped 10/11/17
  uncapped, recapped & added ssc 10/12
all of which resulted in this weird config:
select run_date, deal_Status, primary_sc, secondary_sc, gl_count, row_type, gl_date
from sls.xfm_deals where stock_number = '30865AA'


2017-09-01;U;       TAR;none; 1;new;    2017-08-31
2017-09-07;deleted; TAR;none;-1;update; 2017-09-06
2017-10-12;U;       EAS;none;-1;new;    2017-09-06
2017-10-14;U;       EAS;JAN;  1;update; 2017-10-11

do not know why the 10/12 row got a -1 count

anyway, delete the 10/12 row in xfm, and deals and rerun

delete from sls.xfm_deals where bopmast_id = 44497 and run_date = '10/12/2017';
delete from sls.deals where bopmast_id = 44497;

that did not work 
delete all 44497 rows, including sls.ext_deals

delete from sls.xfm_deals where bopmast_id = 44497;
delete from sls.deals where bopmast_id = 44497;
delete from sls.ext_deals where bopmast_id = 44497;

except, now nothing for 44497, think i need to delete some more predecessor files, so it all gets processed
yep, that did it, except for deals by month, nothing there for october

yep, done



-- 9/19/17
---------------------------------------------------------------------------------------------------------------

31049 from deal status none to deal status none : added ignore if none/none

31666P ???
30528A ???
---------------------------------------------------------------------------------------------------------------
-- 9/14/17

31619 bpomast_id: 43827
select *
from sls.xfm_deals
where stock_number = '31619'
looking at it the morning of 9/14
5250:
unwind 9/13 @ 9:38 AM
accpt  9/13 @ 12:20 PM
cap    9/14 @ 1:11 AM
system sees a capped deal with changing dates becoming a capped deal
maybe getting lazy
add this to individual stock_number ignore
---------------------------------------------------------------------------------------------------------------
31493XX
  unwind 5250: 8/2 anthony
  glpdtim 3774360: 8/2 anthony
  recap 5250: 8/4 heather
  glpdtim 3775486 8/4 heather but gtdate is 8/2
  
  seems goofy, in accounting uncapped and recapped on the same day, 8/2, but i believe the
  recap date is priori dated, in 5250 shows recapped on 8/4

31694 
    unwind 5250: 8/3 heather
    glpdtim 3775025: 8/3, gtdate 8/3
    recap 5250: 8/4 heather tandeski
    glpdtim 3775820: 8/4 BUT gtdate is 8/3
  the recapping entry on 8/3 does not show up in sls.ext_accounting_deals because it is excluded by the
  multiple entry vehicle exclusion clause

31126A  
  unwind 5250: 8/3 heather
  glpdtim 3775011: 8/3
  recap 5250: 8/4 heather
  glpdtim: 3775928: 8/3 gtdate 8/3


lets look at an earlier "no problem" instance of a type_2_b being recapped - no help  

-- fact_gl : control/account/store
select trans, seq, b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, c.account_type
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '31493xx'
  and d.journal_code = 'VSU'
  and c.store_code = 'RY1'
  and c.account_type = 'sale'
order by the_date  

select * from arkona.ext_glpdtim limit 100

select * from sls.deals where notes = 'type_2_b'  

select * from sls.xfm_deals where stock_number in (select stock_number from sls.deals where notes = 'type_2_b')
order by stock_number, run_date

select stock_number, deal_status, unit_count, gl_Date, notes from sls.deals a where exists (select 1 from sls.deals where notes = 'type_2_b' and store_code = 'ry1' and bopmast_id =a.bopmast_id) and a.vehicle_type_code = 'N' order by stock_number, seq

select * from sls.ext_accounting_deals where control = '31694'


                        select x.control, x.the_date
                        from ( -- assign unit count to rows w/multiple rows per date
                          select a.control, a.the_date,
                            case when a.amount < 0 then 1 else -1 end as the_count
                          from sls.ext_accounting_base a
                          inner join ( -- multiple rows per date
                            select control, the_date
                            from sls.ext_accounting_base
                            group by control, the_date
                            having count(*) > 1) b on a.control = b.control and a.the_date = b.the_date) x
                        group by x.control, x.the_date
                        having sum(the_count) = 0
                        order by control

select * from sls.ext_accounting_base where control = '31694'            

select the_date, control, amount, count(*)
from sls.ext_accounting_base
group by the_date, control, amount
having count(*) > 1
order by control


select * from sls.deals where stock_number in ('31126A','31694','31493XX') order by stock_number, seq

update sls.deals
set deal_status_code = 'U'
where stock_number in ('31126A','31694','31493XX') 
  and seq = 3;

-----------------------------------------------------------------------------------------------------------
8/8/17
31112A

this is a goofy one

8/7/17 16:19 unwound
8/8/17 00:24 capped
xfm_deals does not show the 8/7 unwind transaction
because in class XfmDealsChangedRows, only the most recent transaction from sls.ext_accounting_deals is 
joined to sls.ext_deals
it is rare to have multiple accounting transactions of different dates in the same scrape
so, what i am going to try is to manually insert the 8/7 row in sls.xfm_deals, 
nah, that gets messy, in this one case, i am going to forego recording all detailed transactions,
in reality, this unwind/recap business is meaningless in the the larger context of car deals

this seems a bit heavy handed, but reliable, in class DealsChangedRows i will add yet add a case of manually entered 
  vehicles to ignore

select trans, seq, b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, c.account_type
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '31112a'
  and d.journal_code = 'VSU'
  and c.store_code = 'RY1'
  and c.account_type = 'sale'
order by the_date  

select * from sls.xfm_deals where stock_number = '31112a'

select * from sls.ext_accounting_base where control = '31112a'


select * from sls.ext_accounting_deals where control = '31112a'

-----------------------------------------------------------------------------------------------------------

8/21/17
31135C, 31588XA, added conditional branch to type_2_c to accomodate these 2 


-----------------------------------------------------------------------------------------------------------

9/11/17
H10374G (13634): add to ignore clause

select *
from sls.Deals 
where bopmast_id = 13634

select *
from sls.Deals 
where vin = '5FNRL5H60DB056774'

select *
from sls.xfm_Deals 
where bopmast_id = 13634

select *
from sls.ext_Deals 
where bopmast_id = 13634

select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where control = 'H10374G'
  and journal_code = 'VSU'
  and account_type = 'sale'
  and department = 'used vehicle'  


select *
from arkona.xfm_inpmast
where inpmast_vin = '5FNRL5H60DB056774'




