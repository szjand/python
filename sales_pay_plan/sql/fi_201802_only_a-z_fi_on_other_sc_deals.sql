﻿
-- this shows both consultant and fi, so, for a-z guys doing fi, are any of the deals for a consultant
-- other than themself?
-- looks like only 2 deals for bryn
select a.store_code, stock_number, sale_group, unit_count, psc_last_name, 
  (select team_role from sls.personnel_roles where employee_number = a.psc_employee_number and thru_date > current_date), 
  ssc_last_name, fi_last_name || ', ' || fi_first_name, b.*
-- select *
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month = 201802
  and left(psc_employee_number, 1) = '1'
--   and (psc_last_name = 'seay' or ssc_last_name = 'seay' or fi_last_name = 'seay')
order by fi_last_name


select a.store_code, stock_number, sale_group, unit_count, psc_last_name, (select team_role from sls.personnel_roles where employee_number = a.psc_employee_number and thru_date > current_date), ssc_last_name, fi_last_name, b.*
-- select *
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month = 201802
  and left(psc_employee_number, 1) = '1'
  and (psc_last_name = 'seay' or ssc_last_name = 'seay' or fi_last_name = 'seay')
order by sale_group  


select sale_Group, count(*), sum(fi_gross) as fi, sum(front_gross) as front
-- select *
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month = 201802
  and left(psc_employee_number, 1) = '1'
  and (psc_last_name = 'seay' or ssc_last_name = 'weber' or fi_last_name = 'weber')
group by sale_group  
order by sale_group  


select stock_number, sum(front_gross) as front, sum(fi_gross) as fi
-- select *
from sls.deals_by_month a
left join sls.deals_gross_by_month b on a.stock_number = b.control
where a.year_month = 201802
  and left(psc_employee_number, 1) = '1'
  and (psc_last_name = 'seay' or ssc_last_name = 'seay' or fi_last_name = 'seay')
group by stock_number
order by stock_number  



select *
from sls.deals_by_month
where year_month = 201802
  and stock_number = '33124xx'


select * 
from sls.personnel
where last_name = 'weber'

update sls.personnel 
set fi_id = 'JWE'
where last_name = 'weber'


select *
from ads.ext_dim_salesperson
where lastname = 'weber'
  

