﻿select employee_number, last_name, first_name, per_unit, count(*) as units, count(*) * per_unit as total
from (
  select stock_number, last_name, first_name, b.employee_number,
    case 
      when payplan <> 'executive' then 280
      when payplan = 'executive' and odometer_at_sale < 100000 then 250
      when payplan = 'executive' and odometer_at_sale >= 100000 then 280
    end as per_unit
  from sls.deals a
  inner join sls.personnel b on a.primary_sc = b.ry2_id
  inner join sls.personnel_payplans c on b.employee_number = c.employee_number
    and thru_date > current_Date
  where a.store_Code = 'RY2'
    and a.year_month = 201708) c
group by employee_number, last_name, first_name, per_unit  
order by last_name

--08/01/17
not good, stadstadt show 7, should be 5
mulher shows 4 should be 2


select * from sls.personnel_payplans


  select stock_number, last_name, first_name, b.employee_number,
    case 
      when payplan <> 'executive' then 280
      when payplan = 'executive' and odometer_at_sale < 100000 then 200
      when payplan = 'executive' and odometer_at_sale >= 100000 then 280
    end as per_unit
  from sls.deals a
  inner join sls.personnel b on a.primary_sc = b.ry2_id
  inner join sls.personnel_payplans c on b.employee_number = c.employee_number
    and thru_date > current_Date
  where a.store_Code = 'RY2'
    and a.year_month = 201707
order by last_name, stock_number   


select * from sls.deals_by_month where year_month = 201707 and psc_last_name = 'stadstad' and store_code = 'ry2'


-- 08/01/17
-- used sales_pay_plan/sql/consultant_deals_by_month as base
-- this seems to work ok
-- 1/1/18, added is_senior to rate case
select employee_number, last_name, first_name, per_unit, sum(unit_count) as units,
  per_unit * sum(unit_count) as total
from (
select e.employee_number, last_name, first_name, unit_count,
      case 
        when is_senior then 330
        when payplan <> 'executive' then 280
        when payplan = 'executive' and odometer_at_sale < 100000 then 250
        when payplan = 'executive' and odometer_at_sale >= 100000 then 280
      end as per_unit
  from (
    select a.store_code, a.psc_last_name as last_name, a.psc_first_name as first_name, 
      b.employee_number, a.stock_number, 
      case
        when ssc_last_name = 'none' then unit_count
        else 0.5 * unit_count
      end as unit_count, a.odometer_at_sale, b.is_senior
    from sls.deals_by_month a
    inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
      or a.ssc_employee_number = b.employee_number
    where a.year_month = 201802 ---------------------------------------------------------------
    union
    select a.store_code, a.ssc_last_name, a.ssc_first_name, b.employee_number, a.stock_number, 
      0.5 * unit_count as unit_count, a.odometer_at_sale, b.is_senior
    from sls.deals_by_month a
    inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
    where a.year_month = 201802----------------------------------------------------------------
      and a.ssc_last_name <> 'none') e
  left join sls.personnel_payplans f on e.employee_number = f.employee_number
    and f.thru_date > current_date
  where store_code = 'ry2') g
group by employee_number, last_name, first_name, per_unit


-- 12/1/17: larry stadstadt is no longer on teams but sells honda vehicles

select a.stock_number, a.unit_count, b.*
from sls.deals a
left join sls.personnel b on a.primary_sc = b.ry2_id
where a.year_month = 201802
  and a.store_code = 'ry2'
  and left(b.employee_number, 1) = '1'
order by last_name  


select * from sls.tmp_consultants
