﻿--------------------------------------------------------------------------------------------------------------------
-- 9/1/17
payroll submitted
this feels like the best time and place to set the new open month, copy over guarantees

update sls.months
set open_closed = 'closed'
where open_closed = 'open';

update sls.months
set open_closed = 'open'
where year_month = '201804';


-- insert into sls.payroll_guarantees (employee_number,year_month,amount)
-- select employee_number, 201802, amount
-- from sls.payroll_guarantees
-- where year_month = 201801;

select * from sls.months where open_closed = 'open'


--------------------------------------------------------------------------------------------------------------------

/*

201707 
from KC:

I am attaching the team structures for july.
Bryn and Sam F are no longer on a guarantee.
Corey eden is on the normal plan without any guarantee as well.
Scott pearson isn’t on a guarantee this month as well he has alredy been paid out.
Tyler B, Justin B,Ross B and Brian S are on the standard plan now as well.  
Let me know if you have any questions Jon.  Thanks


Team Leader	      Team Leader	    Team Leader	      Team Leader	    Team Leader	      Team Leader	  Team Leader	  Trave Evals
Anthony Michael	  Tom Abol	      Rick Stout	      Jimmy Weber	    Jared Langenstein	Corey Eden	  Josh Barker	  Ben K
Donavan Eastman	 	                Nate Dockendorf	  Brett Hanson	  Sam Foster	      Alan Mulhurn	AJ Preston	
Arden Loven	      Jim Warmack	    Tyler Bedney	    Josh Solie	    Bryn Seay	Phil    Yunker	      Jesse Chavez	
Austin Janzen	    Craig Crocker	  Justin Bjarnason  Nikolai Holland	Chris Garceau	    Dylan Haley	  Nick Bellmore	
Steve Flaat	      Jeff Tarr	      Larry Stadstad	  Tanner Trosen	  John Olderback	  Ross Belgarde	Damien Brooks	
                                                    Brian Schneider		
                                                    								
Team Corey and Team Barker will split down the middle figured as one team leader of eight divided by 2.  
Thanks Jon							
							
Kyle	will be on team anthony						


Jon to KC:
1.	Do Tyler, Justin, Ross & Brian have a 3000 guarantee like all other consultants on the standard payplan?
2.	is Kyle Entzel hourly?
3.	So, Jared is back to being a team leader and Ben Knudson is no longer a team leader, correct?

KC: 
Jared is digital director but these 4 guys report directly to him as well as josh and Corey. Those guys are the A to Z  
Everyone on standard payplan is 3k 
Kyle is hourly

Jon:
Is jared to be paid on the team leader pay plan?
KC:
no

KC:
I made a mistake on Barkers and Edens team
Phil Yunker is on Josh
AJ preston is on Corey e team.  

*/

select a.last_name, a.first_name, b.team, c.payplan, coalesce(d.amount, 0)::integer as guarantee
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201708
order by b.team, c.payplan, a.last_name  



1. new consultant
insert into sls.personnel values('RY1','156884','Entzel','Kyle','07/10/2017','ENT','ENT','none',false,'07/10/2017');
insert into sls.team_personnel values('team anthony','156884','07/01/2017', '12/31/9999'); -- from_date the first of the month
insert into sls.personnel_roles values('156884','consultant','07/01/2017','12/31/9999');
insert into sls.personnel_payplans values('156884','hourly','07/01/2017','12/31/9999');

2. convert the previously hourly to standard
select a.*, b.first_name || ' ' || b.last_name
from sls.personnel_payplans a
inner join sls.personnel b on a.employee_number = b.employee_number
where a.payplan = 'hourly'
  and a.employee_number <> '156884'
  and a.thru_date > current_date

update sls.personnel_payplans
set thru_date = '06/30/2017'
-- select * from sls.personnel_payplans
where employee_number in (
  select a.employee_number
  from sls.personnel_payplans a
  where a.payplan = 'hourly'
    and a.from_date < '07/01/2017'
    and a.thru_date > current_date);

insert into sls.personnel_payplans (employee_number, payplan, from_date)
select employee_number, 'standard', '07/01/2017'
from sls.personnel_payplans
where payplan = 'hourly'
  and thru_date = '06/30/2017';   

3. update team assignments

                from        to
bedney          team ben    team stout
bjarnason       team ben    team stout
dockendorf      team ben    team stout
knudson         team ben    none
stadstad        team ben    team stout

foster          team stout  team langenstein
seay            team stout  team langenstein
garceau         team stout  team langenstein
olderbak        team stout  team langenstein

update sls.team_personnel
set thru_date = '06/30/2017'
-- select * from sls.team_personnel 
where employee_number in (
  select employee_number
  from sls.personnel
  where last_name in ('bedney','bjarnason','dockendorf','knudson','stadstad','foster','seay','garceau','olderbak'));

insert into sls.team_personnel (team, employee_number, from_date) values
('team stout', (select employee_number from sls.personnel where last_name = 'bedney'), '07/01/2017'),
('team stout', (select employee_number from sls.personnel where last_name = 'bjarnason'), '07/01/2017'),
('team stout', (select employee_number from sls.personnel where last_name = 'dockendorf'), '07/01/2017'),
('team stout', (select employee_number from sls.personnel where last_name = 'stadstad'), '07/01/2017'),
('team langenstein', (select employee_number from sls.personnel where last_name = 'foster' and first_name = 'samuel'), '07/01/2017'),
('team langenstein', (select employee_number from sls.personnel where last_name = 'seay'), '07/01/2017'),
('team langenstein', (select employee_number from sls.personnel where last_name = 'garceau'), '07/01/2017'),
('team langenstein', (select employee_number from sls.personnel where last_name = 'olderbak'), '07/01/2017');


4. update guarantees
-- Bryn and Sam F are no longer on a guarantee.
-- Corey eden is on the normal plan without any guarantee as well.
-- Scott pearson isn’t on a guarantee this month as well he has alredy been paid out.
-- olderbak & garceau no guarantee

select a.*, b.first_name || ' ' || b.last_name
from sls.payroll_guarantees a
inner join sls.personnel b on a.employee_number = b.employee_number
  and b.last_name not in ('pearson','eden','foster','seay', 'garceau','olderbak')
inner join sls.team_personnel c on a.employee_number = c.employee_number
  and c.thru_date > current_Date  
where a.year_month = '201706'
order by b.last_name

copy over from 201706 excluding those that no longer have a guarantee

insert into sls.payroll_guarantees
select a.employee_number, 201707, a.amount
from sls.payroll_guarantees a
inner join sls.personnel b on a.employee_number = b.employee_number -- exclude 
  and b.last_name not in ('pearson','eden','foster','seay', 'garceau','olderbak')
inner join sls.team_personnel c on a.employee_number = c.employee_number -- currently
  and c.thru_date > current_Date  
where a.year_month = '201706';

-- stadstad update to 5000
update sls.payroll_guarantees
set amount = 5000
where year_month = 201707
  and employee_number = (select employee_number from sls.personnel where last_name = 'stadstad');

-- add stout for 8000
insert into sls.payroll_guarantees
select employee_number, 201707, 8000
from sls.personnel
where last_name= 'stout';


-- 201707 should be good to go

select a.last_name, a.first_name, b.team, c.payplan, coalesce(d.amount, 0)::integer as guarantee
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
inner join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201707 
order by b.team, 
  case 
    when payplan = 'team_leader' then '1'
    else a.last_name 
  end

-- missing guarantee for belgarde, bedney, bjarnason, schneider
-- which makes sense, last month they had no guarantee
-- add them

insert into sls.payroll_guarantees
select employee_number, 201707, 3000
from sls.personnel
where last_name in ('Schneider','Bjarnason','Bedney','Belgarde');

select * 
from sls.months
where open_closed = 'open'

-- 7/31 fix the preston - yunker fkup

select b.last_name, a.* 
from sls.team_personnel a
inner join sls.personnel b on a.employee_number = b.employee_number
where thru_date > current_Date
  and b.last_name in ('yunker','preston')

update sls.team_personnel
set team = 'team eden'
where employee_number = '1112410'
  and thru_date > current_date;

update sls.team_personnel
set team = 'team barker'
where employee_number = '1151450'
  and thru_date > current_date;  


--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
-- 8/20/17

Is this all (personnel, team assignemnts, payplans & guarantees) correct for August payroll?
 
thanks

Everything but Kyle which is on ricks team.  I will send you an email on the last day of the month with any pay differences I know of.  

select a.last_name, a.first_name, a.employee_number, b.team, 
  c.payplan, coalesce(d.amount, 0)::integer as guarantee
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201708
order by b.team, c.payplan, a.last_name 

move Kyle Entzel from team anthony to team stout
emp#: 156884

update sls.team_personnel
set thru_date = '07/31/2017'
where employee_number = '156884';

insert into sls.team_personnel(team, employee_number, from_date)
values ('team stout','156884','08/01/2017');

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
-- 9/25/17 lots and lots of changes see /sales_pay_plan/docs/201709_teams.pdf

team                team leader         consultant          digital
-------------------------------------------------------------------------------
team anthony        anthony michael     craig croaker       damian brooks
                                        austin janzen       jesse chavez
                                        steve flaat
                                        donovan eastman
                                        
team a-z            jared langenstein   chris garceau        bryn seay
                                                             james weber
                                                             samuel foster
                                                             joshua barker
                                                                                                                          
team stout          rick stout          justin bjarnason     allen preston
                                        kyle entzel          ross belgarde
                                        tyler bedney
                                        brett hanson 

team eden           corey eden          arden loven          dylanger haley
                                        joshua solie         george yunker
                                        nikolai holland
                                        dominic ********                       ***** no employee named dominic ???, dominic did not make it, probably frank decouteau

team aubol          tom aubol           james warmack        alan mulhern
                                        jeff tarr            nicholas bellmore
                                        tanner trosen
                                        brian schneider                                                                                                                                            

1. fix team name
need to fix FK constraints in team_personnel first
-- first an old unused table
alter table sls. z_unused_folks
drop constraint folks_team_fkey;

alter table sls.team_personnel
drop constraint team_personnel_team_fkey,
add constraint team_personnel_team_fkey foreign Key (team)
  references sls.teams (team) on update cascade on delete cascade;

update sls.teams set team = 'team aubol' where team = 'team tom'

2. new team
insert into sls.teams values('team a-z');

3. new value for team_roles: digital

do
$$
declare 
   _employee_number citext := '110052';
   _thru_date date := '08/31/2017';
   _from_date date := '09/01/2017';
   _role citext := 'digital';
begin
  update sls.personnel_roles
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
  insert into sls.personnel_roles (employee_number,team_role,from_date)
  values(_employee_number,_role,_from_date);
end
$$;


4. no more team barker, team weber reassign those folks
-brooks/chavez: barker -> anthony
-bellmore: barker -> aubol
-yunker: barker -> eden
-barker: barker -> a-z
-trosen/schneider: weber -> aubol
-hanson: weber -> stout
-holland/solie: weber -> eden
weber: weber -> a-z

do
$$
declare 
   _employee_number citext := '1147810';
   _thru_date date := '08/31/2017';
   _from_date date := '09/01/2017';
   _old_team citext  := 'team weber';
   _new_team citext := 'team a-z';
begin
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
  insert into sls.team_personnel (team, employee_number,from_date)
  values(_new_team,_employee_number,_from_date);
end
$$;

5. no more team langenstein, it is now team a-z
update sls.team_personnel set team = 'team a-z' where team = 'team langenstein';

6. payplan for josh and jimmy from team leader to executive

do
$$
declare 
   _employee_number citext := '1147810';
   _thru_date date := '08/31/2017';
   _from_date date := '09/01/2017';
   _new_payplan citext := 'executive';
begin
  update sls.personnel_payplans
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
  insert into sls.personnel_payplans (employee_number,payplan, from_date)
  values(_employee_number,_new_payplan,_from_date);
end
$$;

7. remove olderbak, dockendorf, stadstadt from teams
update sls.team_personnel
set thru_date = '08/31/2017'
where employee_number = '1130690'
  and thru_date > current_date;

8. other team changes
croaker: aubol -> anthony
loven: anthony -> eden
mulhern: eden -> aubol
preston: eden -> stout
belgarde: eden -> stout
do
$$
declare 
   _employee_number citext := '12242';
   _thru_date date := '08/31/2017';
   _from_date date := '09/01/2017';
   _old_team citext  := 'team eden';
   _new_team citext := 'team stout';
begin
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
  insert into sls.team_personnel (team, employee_number,from_date)
  values(_new_team,_employee_number,_from_date);
end
$$;

9. add and configure frank decouteau

insert into sls.personnel
values ('RY1','15552','Frank','Decouteau','09/25/2017','FDE','FDE','none',false,'09/25/2017');
insert into sls.team_personnel (team,employee_number,from_date)
values('team eden','15552','09/01/2017');
insert into sls.personnel_roles(employee_number, team_role, from_date)
values('15552','consultant','09/01/2017');
insert into sls.personnel_payplans(employee_number, payplan, from_date)
values('15552','hourly','09/01/2017');

-- add team_role to this query
select a.last_name, a.first_name, a.employee_number, b.team, bb.team_role,
  c.payplan, coalesce(d.amount, 0)::integer as guarantee
-- select *  
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
inner join sls.personnel_roles bb on a.employee_number = bb.employee_number
  and bb.thru_date > current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201709
order by b.team, c.payplan, a.last_name   

-- 9/29, new employee Frank Decouteau goes to team a-z, not team eden
update sls.team_personnel
set team = 'team a-z'
where employee_number = '15552'

update sls.personnel_roles
set team_role = 'digital'
where employee_number = '15552'

-------------------------------------------------------------------------------------------------------------
-- 10/29/17
-------------------------------------------------------------------------------------------------------------
add dealcount to guess at who is no longer in the team structure

-- current team config:
drop table if exists team_config;
create temp table team_config as
select a.last_name, a.first_name, a.employee_number, b.team, bb.team_role,
  c.payplan, coalesce(d.amount, 0)::integer as guarantee
-- select *  
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
inner join sls.personnel_roles bb on a.employee_number = bb.employee_number
  and bb.thru_date > current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201710;


-- deal count
drop table if exists deal_count;
create temp table deal_count as
select distinct sum(unit_count) over (partition by psc_last_name) as total, x.psc_last_name
from (
select a.psc_last_name, a.stock_number, 
  case
    when ssc_last_name = 'none' then unit_count
    else 0.5 * unit_count
  end as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
  or a.ssc_employee_number = b.employee_number
where a.year_month = 201710
union
select a.ssc_last_name, a.stock_number, 
  0.5 * unit_count as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
where a.year_month = 201710
  and a.ssc_last_name <> 'none') x;

-- so what this tells me is that chris garceau and aj preston are not in october payroll
select *
from team_config a
left join deal_count b on a.last_name = b.psc_last_name  

so go ahead and remove them
-- garceau: 150040
-- preston: 1112410
do
$$
declare 
   _employee_number citext := '1112410';
   _thru_date date := '09/30/2017';

begin
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
  update sls.personnel_roles
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
  update sls.personnel_payplans
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
end
$$;  


-- 11/2/17
-- bellmore termed in oct
-- josh solie termed 11/1
do
$$
declare 
   _employee_number citext := '1130101';
   _thru_date date := '10/31/2017';

begin
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
  update sls.personnel_roles
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
  update sls.personnel_payplans
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
end
$$;  


-------------------------------------------------------------------------------------------------------------
-- 11/28/17
-------------------------------------------------------------------------------------------------------------
-- current team config:
drop table if exists team_config;
create temp table team_config as
select a.last_name, a.first_name, a.employee_number, b.team, bb.team_role,
  c.payplan, coalesce(d.amount, 0)::integer as guarantee
-- select *  
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
inner join sls.personnel_roles bb on a.employee_number = bb.employee_number
  and bb.thru_date > current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201711;

-- deal count
drop table if exists deal_count;
create temp table deal_count as
select distinct sum(unit_count) over (partition by psc_last_name) as total, x.psc_last_name
from (
select a.psc_last_name, a.stock_number, 
  case
    when ssc_last_name = 'none' then unit_count
    else 0.5 * unit_count
  end as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
  or a.ssc_employee_number = b.employee_number
where a.year_month = 201711
union
select a.ssc_last_name, a.stock_number, 
  0.5 * unit_count as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
where a.year_month = 201711
  and a.ssc_last_name <> 'none') x;


select *
from team_config a
left join deal_count b on a.last_name = b.psc_last_name  
order by team, team_role, last_name

brett hanson termed 11/6, has 3 deals for november, leave him in but remove the guarantee

update sls.payroll_guarantees 
set amount = 0
where employee_number = '161325'
  and year_month = 201711

-- 11/30: changes from KC -------------------------------
team leaders get a guarantee
anthony: 10,000
corey: 8,000
decouteau: hourly -> executive
entzel: hourly -> standard

select * 
from sls.payroll_guarantees

insert into sls.payroll_guarantees (employee_number,year_month,amount) values
('195460',201711, 10000),
('137220',201711, 8000);

select * from sls.team_leader_payroll where year_month = 201711

select * from sls.personnel_payplans where employee_number in ('15552','156884')

update sls.personnel_payplans
set thru_date = '10/31/2017'
where employee_number in ('15552','156884');
insert into sls.personnel_payplans(employee_number,payplan,from_date) values
('15552','executive','11/01/2017'),
('156884','standard','11/01/2017');

-- assuming entzel gets $3000 guarantee
insert into sls.payroll_guarantees (employee_number,year_month,amount) values
('156884',201711, 3000);


select * from sls.consultant_payroll where year_month = 201711 order by team, last_name

-------------------------------------------------------------------------------------------------------------
-- December 2017
-------------------------------------------------------------------------------------------------------------
-- 12/2/17 brett hanson gone
update sls.team_personnel
set thru_date = '11/30/2017'
where employee_number = '161325'
  and thru_date > current_date;

update sls.personnel_roles
set thru_date = '11/30/2017'
where employee_number = '161325'
  and thru_date > current_date;
  

update sls.personnel_payplans
set thru_date = '11/30/2017'
where employee_number = '161325'
  and thru_date > current_date;

-- tanner trosen moved to honda
-- 12/2/17 brett hanson gone

do
$$
  declare 
    _employee_number TEXT := (
      select employee_number
      from sls.personnel
      where last_name = 'trosen');
begin
  update sls.team_personnel
  set thru_date = '11/30/2017'
  where employee_number = _employee_number
    and thru_date > current_date;

  update sls.personnel_roles
  set thru_date = '11/30/2017'
  where employee_number = _employee_number
    and thru_date > current_date;
    

  update sls.personnel_payplans
  set thru_date = '11/30/2017'
  where employee_number = _employee_number
    and thru_date > current_date;
end
$$;
--------------------------------------------------------------------------------------------
-- 1/29/18 LOTS O CHANGES
--------------------------------------------------------------------------------------------
1. kyle entzel gone

do
$$
  declare 
    _employee_number TEXT := (
      select employee_number
      from sls.personnel
      where last_name = 'entzel');
begin
  update sls.team_personnel
  set thru_date = '12/31//2017'
  where employee_number = _employee_number
    and thru_date > current_date;

  update sls.personnel_roles
  set thru_date = '12/31/2017'
  where employee_number = _employee_number
    and thru_date > current_date;
    

  update sls.personnel_payplans
  set thru_date = '12/31/2017'
  where employee_number = _employee_number
    and thru_date > current_date;
end
$$;

4 new consultants vanyo, longoria, annan, hill-defender
already added to sls.personnel
select * from sls.personnel order by start_date desc 
need to add to team_personnel, personnel_payplans, personnel_roles(?), payroll_guarantees(?)

select * from sls.teams
select * from sls.team_personnel
select * from sls.personnel_roles
select * from sls.personnel_payplans
do
$$
  declare 
    _employee_number TEXT := (
      select employee_number
      from sls.personnel
      where last_name = 'hill defender');
    _team text := 'team knudson';  
    _team_role text := 'consultant';   
    _from_date date := '01/01/2018';
    _payplan text := 'hourly';
    
begin
  insert into sls.team_personnel (team,employee_number,from_date)
  select _team, _employee_number, _from_date;
  insert into sls.personnel_roles (employee_number,team_role,from_date)
  select _employee_number, _team_role, _from_date;
  insert into sls.personnel_payplans (employee_number,payplan,from_date)
  select _employee_number, _payplan, _from_date;
end  
$$;

per Ben C, 
Updated the consultant info.  No hourly, everyone at a $3k minimum.  
New rules go in to effect with the $3k but that starts in February.

delete from sls.payroll_guarantees where year_month = 201801 and employee_number = '161325'
select * from sls.payroll_guarantees where year_month = 201801

insert into sls.payroll_guarantees (employee_number,year_month,amount) values
('1124625',201801,3000),
('148080',201801,3000),
('110052',201801,3000),
('1147810',201801,3000),
('15552',201801,3000),
('187594',201801,3000),
('178543',201801,3000),
('12654',201801,3000),
('186593',201801,3000)

select b.team, a.last_name, a.first_name, a.employee_number, bb.team_role,
  c.payplan, coalesce(d.amount, 0)::integer as guarantee
-- select *  
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
inner join sls.personnel_roles bb on a.employee_number = bb.employee_number
  and bb.thru_date > current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201801
where team_role <> 'team_leader'

               

-- current team config:
drop table if exists team_config;
create temp table team_config as
select b.team, a.last_name, a.first_name, a.employee_number, bb.team_role,
  c.payplan, coalesce(d.amount, 0)::integer as guarantee
-- select *  
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
inner join sls.personnel_roles bb on a.employee_number = bb.employee_number
  and bb.thru_date > current_date 
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201801
where team_role <> 'team_leader'  
order by team, last_name  


select a.*, b.clock_hours
from team_config a
left join (
  select employee_number, round(coalesce(clock_hours, 0), 2) as clock_hours
  from sls.clock_hours_by_month 
  where year_month = 201801
  and employee_number in ('186593','187594','178543','12654')) b on a.employee_number = b.employee_number

-- deal count
drop table if exists deal_count;
create temp table deal_count as
select distinct sum(unit_count) over (partition by psc_last_name) as total, x.psc_last_name
from (
select a.psc_last_name, a.stock_number, 
  case
    when ssc_last_name = 'none' then unit_count
    else 0.5 * unit_count
  end as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.psc_employee_number = b.employee_number
  or a.ssc_employee_number = b.employee_number
where a.year_month = 201801
union
select a.ssc_last_name, a.stock_number, 
  0.5 * unit_count as unit_count, 
  a.sale_type, a.vehicle_type, a.make, a.odometer_at_sale, a.total_care, a.service_contract, a.gap
from sls.deals_by_month a
inner join sls.tmp_consultants b on a.ssc_employee_number = b.employee_number
where a.year_month = 201801
  and a.ssc_last_name <> 'none') x;


select * from sls.deals where stock_number = '32336pa'

select * from sls.deals_by_month where stock_number = '32336pa'

select * from sls.tmp_consultants


--------------------------------------------------------------------------------------------
-- 02/05/18 
--------------------------------------------------------------------------------------------
select a.last_name, a.first_name, b.team, c.payplan, coalesce(d.amount, 0)::integer as guarantee
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201802
order by b.team, c.payplan, a.last_name  

1. austin janzen gone

do
$$
  declare 
    _employee_number TEXT := (
      select employee_number
      from sls.personnel
      where last_name = 'janzen');
begin
  update sls.team_personnel
  set thru_date = '12/31//2017'
  where employee_number = _employee_number
    and thru_date > current_date;

  update sls.personnel_roles
  set thru_date = '12/31/2017'
  where employee_number = _employee_number
    and thru_date > current_date;
    
  update sls.personnel_payplans
  set thru_date = '12/31/2017'
  where employee_number = _employee_number
    and thru_date > current_date;

  delete 
  from sls.payroll_guarantees
  where employee_number = _employee_number
    and year_month = 201802;
end
$$;


select * from sls.payroll_guarantees where employee_number = '171150'


--------------------------------------------------------------------------------------------
-- 02/15/18 
--------------------------------------------------------------------------------------------
select a.last_name, a.first_name, b.team, c.payplan, coalesce(d.amount, 0)::integer as guarantee
from sls.personnel a
inner join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.payroll_guarantees d on a.employee_number = d.employee_number
  and d.year_month = 201802
order by b.team, c.payplan, a.last_name  

1. david vanyo from team foster to team eden

do
$$
  declare 
    _employee_number citext := (
      select employee_number
      from sls.personnel
      where last_name = 'vanyo');
    _from_date date := '02/01/2018';
    _thru_date date := '01/31/2018';
    _to_team citext := 'team eden';
    
begin
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
    
  insert into sls.team_personnel (team,employee_number,from_date)
  values(_to_team, _employee_number, _from_date);   
end
$$;


--------------------------------------------------------------------------------------------
-- 03/02/18 
--------------------------------------------------------------------------------------------
-- from afton
    select a.*, b.first_name, b.last_name, c.team
    from sls.team_personnel_new a
    inner join sls.personnel_new b on a.employee_number = b.employee_number 
    inner join sls.teams_new c on a.team_id = c.team_id
    order by last_name

    select * from sls.personnel_new order by last_name
    
select *
from ( -- afton
  select team, first_name, last_name
  from (
    select a.*, b.first_name, b.last_name, c.team
    from sls.team_personnel_new a
    inner join sls.personnel_new b on a.employee_number = b.employee_number 
    inner join sls.teams_new c on a.team_id = c.team_id
    order by a.team_id) aa) a
full outer join (
  select team, first_name, last_name
  from ( -- jon
    select a.last_name, a.first_name, b.team, c.payplan, coalesce(d.amount, 0)::integer as guarantee
    from sls.personnel a
    inner join sls.team_personnel b on a.employee_number = b.employee_number
      and b.thru_date > current_date
    left join sls.personnel_payplans c on a.employee_number = c.employee_number
      and c.thru_date > current_date  
    left join sls.payroll_guarantees d on a.employee_number = d.employee_number
      and d.year_month = 201802
    order by b.team, c.payplan, a.last_name) c) b on a.team = b.team and a.first_name = b.first_name and a.last_name = b.last_name

-- afton problems: vancamp backwards & firstname is kevin not on team, michael menard backwards

chavez: anthony -> stout
stadstadt: knudson -> aubol
bjarnason: stout -> outlet
eastman: stout -> anthony

add honda teams and outlet team

-- jon missing a couple of honda consultants
-- clean the superfluous rows in jon (sorum, andrews, etc)
select * 
from sls.personnel_new a
full outer join sls.personnel b on a.employee_number = b.employee_number

select * 
from sls.personnel_new a
left join sls.team_personnel_new b on a.employee_number = b.employee_number

select *
from sls.personnel a
left join sls.team_personnel b on a.employee_number = b.employee_number
where b.employee_number is null



select storecode, lastname, firstname, hiredate
from ads.ext_edw_employee_dim
where distcode = 'SALE'
  and active = 'active'
  and currentrow = true
order by storecode, lastname

-- add the honda & outlet teams
insert into sls.teams values 
('team lizakowski'),
('team calcaterra'),
('team outlet');


select * 
from sls.personnel 
order by last_name

the reassignments:
chavez: anthony -> stout
stadstadt: knudson -> aubol
bjarnason: stout -> outlet
eastman: stout -> anthony  
do
$$
  declare 
    _employee_number citext := (
      select employee_number
      from sls.personnel
      where last_name = 'eastman');
    _from_date date := '03/01/2018';
    _thru_date date := '02/28/2018';
    _to_team citext := 'team anthony';
    
begin
  update sls.team_personnel
  set thru_date = _thru_date
  where employee_number = _employee_number
    and thru_date > current_date;
    
  insert into sls.team_personnel (team,employee_number,from_date)
  values(_to_team, _employee_number, _from_date);   
end
$$;

-- outlet guys
insert into sls.team_personnel (team,employee_number,from_date)values
('team outlet', (select employee_number from sls.personnel where last_name = 'dockendorf'), '01/01/2018'),
('team outlet', (select employee_number from sls.personnel where last_name = 'olderbak'), '01/01/2018');

-- honda teams
insert into sls.team_personnel (team,employee_number,from_date)values
('team lizakowski', (select employee_number from sls.personnel where last_name = 'trosen'), '01/01/2018'),
('team lizakowski', (select employee_number from sls.personnel where last_name = 'gabrielson'), '01/01/2018'),
('team lizakowski', (select employee_number from sls.personnel where last_name = 'spencer'), '01/01/2018'),
('team lizakowski', (select employee_number from sls.personnel where last_name = 'wilde'), '01/01/2018'),
('team lizakowski', (select employee_number from sls.personnel where last_name = 'syverson'), '01/01/2018'),
('team calcaterra', (select employee_number from sls.personnel where last_name = 'bjerk'), '01/01/2018'),
('team calcaterra', (select employee_number from sls.personnel where last_name = 'monreal'), '01/01/2018'),
('team calcaterra', (select employee_number from sls.personnel where last_name = 'hughes'), '01/01/2018'),
('team calcaterra', (select employee_number from sls.personnel where last_name = 'rea'), '01/01/2018'),
('team calcaterra', (select employee_number from sls.personnel where last_name = 'ames'), '01/01/2018');


select a.team, b.*
from sls.team_personnel a
left join sls.personnel b on a.employee_number = b.employee_number
where a.thru_date > current_date
order by team, last_name


select *
from sls.personnel a
left join ( -- arkona
  select sales_person_name, max(employee_number),
    max(case when company_number = 'RY1' and sales_person_type = 'S' then sales_person_id end) as RY1_SC,
    max(case when company_number = 'RY2' and sales_person_type = 'S' then sales_person_id end) as RY2_SC,
    max(case when sales_person_type = 'F' then sales_person_id end) as FIN
  from arkona.ext_bopslsp
  where active is null
    and company_number in ('ry1','ry2')
    and sales_person_type in ('S','F')
  group by sales_person_name) b on b.sales_person_name like '%'||a.last_name||'%'
order by a.last_name

select sales_person_type, count(*) from arkona.ext_bopslsp group by sales_person_type

S: Sales Representative
D: Desk Manager
C: Closer
F: F&I Manager
M: Sales Manager
B: BDC Representative

select * from ads.ext_edw_employee_dim where lastname = 'rea'

update sls.personnel
set ry2_id = 'JW'
where employee_number = '1147810'

update sls.personnel
set fi_id = 'JOB'
where employee_number = '110052'

-- 3/3/18 thought i finished cleaning up yesterday but i did not
-- shit is still going to be messy
-- and the fucking notion of afton and i maintaining separate structures for the same data is nuts
-- as usual, i will be driven by doing just enuf to satisfy  app functionality ignoring mainenance

select a.*, b.team, c.termdate
from sls.personnel a
left join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
left join ads.ext_edw_employee_dim c on a.employee_number = c.employeenumber
  and c.currentrow = true
order by store_code, team, last_name


-- removed terms and non sales consultants
delete from sls.personnel_payplans where employee_number in ('112589');
delete from sls.personnel_roles where employee_number in ('112589');
delete from sls.team_personnel where employee_number in ('112589');
delete from sls.personnel where employee_number in ('');

add the honda team leaders

insert into sls.personnel_roles (employee_number,team_role,from_date)
select a.employee_number, 'team_leader', '01/01/2018'
from sls.personnel a
where last_name in ('lizakowski','calcaterra');

insert into sls.team_personnel (team,employee_number,from_date)
select 'team lizakowski', a.employee_number, '01/01/2018'
from sls.personnel a
where last_name = 'lizakowski';

insert into sls.team_personnel (team,employee_number,from_date)
select 'team calcaterra', a.employee_number, '01/01/2018'
from sls.personnel a
where last_name = 'calcaterra';

select *
from sls.personnel a
left join sls.personnel_roles b on a.employee_number = b.employee_number
  and b.thru_date > current_date
order by store_code, a.last_name

select * from sls.teams

insert into sls.personnel values('RY1','165983','Menard','Michael','02/12/2018','MME','MME','none',false,'02/12/2018');
do
$$
  declare 
    _employee_number TEXT := (
      select employee_number
      from sls.personnel
      where last_name = 'menard');
    _team text := 'team stout';  
    _team_role text := 'consultant';   
    _from_date date := '02/12/2018';
    _payplan text := 'standard';
    
begin
  insert into sls.team_personnel (team,employee_number,from_date)
  select _team, _employee_number, _from_date;
  insert into sls.personnel_roles (employee_number,team_role,from_date)
  select _employee_number, _team_role, _from_date;
  insert into sls.personnel_payplans (employee_number,payplan,from_date)
  select _employee_number, _payplan, _from_date;
end  
$$;

select * from sls.personnel where last_name = 'vancamp'
update sls.team_personnel set team = 'team knudson' where employee_number = '175642'

select b.team, a.last_name, a.first_name -- c.payplan
from sls.personnel a
left join sls.team_personnel b on a.employee_number = b.employee_number
  and b.thru_date > current_date
left join sls.personnel_payplans c on a.employee_number = c.employee_number
  and c.thru_date > current_date  
left join sls.personnel_roles d on a.employee_number = d.employee_number
   and d.thru_date > current_date  
where a.store_code = 'RY1'
  and b.team is not null
  and d.team_role <> 'team_leader'
order by b.team asc, d.team_role desc, a.last_name asc
