﻿deals by consultant


select primary_sc, sum(the_count)
from (
  select primary_sc,
    sum(case when secondary_sc <> 'none' then .5 * unit_count else unit_count end) as the_count
  from sls.deals_by_month
  where year_month = 201704
  group by primary_sc
  union all
  select secondary_sc,
    sum(.5 * unit_count) as the_count
  from sls.deals_by_month
  where year_month = 201704
    and secondary_sc <> 'none'
  group by secondary_sc) a
group by primary_sc
order by primary_sc

