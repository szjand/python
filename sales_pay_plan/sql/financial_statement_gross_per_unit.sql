﻿drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from ( -- h
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a   
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201706
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201706
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
    and aa.journal_code in ('VSN','VSU')   
    where a.post_status = 'Y' order by control) h
group by store, page, line, line_label, control
order by store, page, line;


select * from step_1 where unit_count <> 1

select x.*, sum(amount) over w, count(control) over w
from (
  select a.store_code, a.page, a.line, c.control, sum(c.amount) as amount
  from sls.deals_accounts_routes a
  inner join fin.dim_account b on a.gl_account = b.account
  inner join fin.fact_gl c on b.account_key = c.account_key
  inner join dds.dim_date d on c.date_key = d.date_key
    and d.year_month = 201706
  group by a.store_code, a.page, a.line, c.control) x
window w as (partition by store_code, page, line order by page, line)
order by store_code, page, line, control


select a.store, a.page, a.line, a.line_label, a.control, a.unit_count, b.amount
from step_1 a
inner join fin.fact_gl b on a.control = b.control
  and b.post_status = 'Y'
inner join fin.dim_journal c on b.journal_key = c.journal_key
  and c.journal_code in ('VSN','VSU')


select a.store, a.page, a.line, a.line_label, a.control, a.unit_count, 
  b.*, c.journal_code, d.account, account_type, description
select sum(amount)  
from step_1 a
inner join fin.fact_gl b on a.control = b.control
  and b.post_status = 'Y'
inner join fin.dim_journal c on b.journal_key = c.journal_key
  and c.journal_code in ('VSN','VSU')
inner join fin.dim_account d on b.account_key = d.account_key
where a.control = '29466r'  
-- group by a.store, a.page, a.line, a.line_label, a.control, a.unit_count   


-- from DealsAccountingDetail
select c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
  e.description as trans_description,
  max(case
    when page = 17 then 'fi'
    else 'front'
  end) as gross_category,
  sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
--   and b.current_row = true
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join sls.deals_accounts_routes d on b.account = d.gl_account
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where post_status = 'Y'
  and c.year_month = 201706
group by b.description, c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, e.description
order by control                      
  
create temp table gross as
select control, 
  sum(case when gross_category = 'front' then amount else 0 end) as front,
  sum(case when gross_category = 'fi' then amount else 0 end) as fi
from  ( -- from DealsAccountingDetail
  select c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
    e.description as trans_description,
    max(case
      when page = 17 then 'fi'
      else 'front'
    end) as gross_category,
    sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  inner join dds.dim_date c on a.date_key = c.date_key
  inner join fin.dim_account b on a.account_key = b.account_key
  --   and b.current_row = true
    and c.the_date between b.row_from_Date and b.row_thru_date
  inner join sls.deals_accounts_routes d on b.account = d.gl_account
  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
  where post_status = 'Y'
    and c.year_month = 201706
  group by b.description, c.year_month, a.control,
    aa.journal_code, b.account_type, b.department,
    d.store_code, d.page, d.line, d.gl_Account, e.description) x
group by control  
order by control;






select *
from step_1 a
left join gross b on a.control = b.control
order by store, page, line


select *, sum(front) over w, count(a.control) over w
from step_1 a
left join gross b on a.control = b.control
window w as (partition by store, page, line order by  store,page, line)
order by store, page, line


-- above is not good enough

fs: 246 used
    165 new
    411 total
    
select *
from step_1

-- by store, page, count is good
select store, page, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page
order by store, page

select *
from step_1
where unit_count <> 1


-- step_1 573 rows
select control -- 580 rows
from step_1
group by control 
having sum(unit_count) > 0


select * -- 9 rows
-- 3 with unit_count = 0
from step_1
where control in (
  select control
  from step_1
  group by control
  having sum(unit_count) <> 1)
order by control


29466R sls.deals_gross_by_month does not agree with statement

-- sale amount    sale - cogs = 649.45
select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
  a.control, a.amount,
  case when a.amount < 0 then 1 else -1 end as unit_count
from fin.fact_gl a   
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201706
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- d: fs gm_account page/line/acct description
  select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201706
    and (
      (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
      or
      (b.page = 16 and b.line between 1 and 14)) -- used cars
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code = '4'
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
  and aa.journal_code in ('VSN','VSU')   
where a.post_status = 'Y' 
  and control = '29466R'

-- cogs amount
select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
  a.control, a.amount,
  case when a.amount < 0 then 1 else -1 end as unit_count
from fin.fact_gl a   
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201706
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- d: fs gm_account page/line/acct description
  select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201706
    and (
      (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
      or
      (b.page = 16 and b.line between 1 and 14)) -- used cars
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code = '5'
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
inner join fin.dim_journal aa on a.journal_key = aa.journal_key 
  and aa.journal_code in ('VSN','VSU')   
where a.post_status = 'Y' 
  and control = '29466R'  


select * -- front_gross = 250.55
from sls.deals_gross_by_month
where control = '29466r'    


-- 649.45
select b.the_date, c.account, c.description, d.journal_code, a.amount, e.description, 
  account_type_code, account_type, department
-- select sum(amount)  
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and control = '29466r'
  and year_month = 201706
  and journal_code = 'VSN'
  and account_type in ('sale', 'cogs')
  and department = 'new vehicle' 


select * 
from sls.deals_accounting_detail
where control = '29466r'

welcome to mindfuckville
what is the one right way to determine gross per vehicle
requirements: 
  must match the fin statement, bu tof the course, the statement does not breakout gross per vehicle


ok, work from the statement P5L5 Malibus: 7 units. sales: 174062 cogs: 175942, gross -1880
-- the correct sales and cogs totals
select year_month, page, line, col, line_label, gm_account, gl_account, store, area, department, sub_Department, amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where year_month = 201706
  and store = 'ry1'
  and page = 5 and line = 5


-- select c.account, sum(a.amount)  -- totals are correct
-- select sum(a.amount) -- correct total gross
-- why if i group on account i get the correct totals
-- select control, sum(amount) -- yikes this is where i get 18 rows, but correct total gross
-- select control, sum(amount) -- add journal VSN, VSU: 10 rows and wrong total gross
-- don't want to jump too far ahead, but i believe i now need to think about unit count
-- of the 10 rows, units not in step_1: 29859/0, 29862/-465.26, 30758/531.26
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201706
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_journal cc on a.journal_key = cc.journal_key
  and cc.journal_code in ('VSN','VSU')
inner join ( -- the above fs query to get the accounts
  select gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where year_month = 201706
    and store = 'ry1'
    and page = 5 and line = 5) d on c.account = d.gl_account
group by control


select * from step_1

sls.deals_accounts_routes looks ok
select *
from (
select page, line, gl_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where year_month = 201706
  and store = 'ry1'
  and page between 5 and 15) x   
full outer join (
select page, line, gl_account
from sls.deals_accounts_routes    
where store_code = 'ry1'
  and page between 5 and 15) xx on x.page = xx.page and x.line = xx.line and x.gl_account = xx.gl_account
order by x.page, x.line, xx.page, xx.line

-- matches the statement perfectly
select page, line, gross_category, sum(amount)
from sls.deals_Accounting_detail
where year_month = 201706
  and store_code = 'ry1'
group by page, line, gross_category
order by page, line


-- god damnit of course, the statement, there is a count and there is gross, the
-- gross is not limited to the count
select a.*, b.delivery_date, b.year_month, b.unit_count, bopmast_id
from sls.deals_Accounting_detail a
left join sls.deals b on a.control = b.stock_number
where a.year_month = 201706
  and a.store_code = 'ry1'
  and a.page = 5 and a.line = 5
order by b.year_month, control



select a.control, b.bopmast_id, b.delivery_date, b.year_month, b.unit_count,
  sum(case when a.account_type = 'sale' then amount else 0 end) as sales,
  sum(case when a.account_type = 'cogs' then amount else 0 end) as cogs
from sls.deals_Accounting_detail a
left join sls.deals b on a.control = b.stock_number
where a.year_month = 201706
  and a.store_code = 'ry1'
  and a.page = 5 and a.line = 5
group by a.control, b.bopmast_id, b.delivery_date, b.year_month, b.unit_count
order by b.year_month, a.control



-- used cars retail certified
select *
from (
select a.control, b.bopmast_id, b.delivery_date, b.year_month, b.unit_count,
  sum(case when a.account_type = 'sale' then amount else 0 end) as sales,
  sum(case when a.account_type = 'cogs' then amount else 0 end) as cogs
from sls.deals_Accounting_detail a
left join sls.deals b on a.control = b.stock_number
where a.year_month = 201706
  and a.store_code = 'ry1'
  and a.page = 16 and a.line = 1
group by a.control, b.bopmast_id, b.delivery_date, b.year_month, b.unit_count) x
left join step_1 xx on x.control = xx.control 
order by x.year_month, x.control

select *
from sls.xfm_deals where stock_number = '31130xx'

select *
from step_1
where page = 16 and line = 1 and store = 'ry1'


select store,page,line,line_label, sum(unit_count)
from step_1
group by store,page,line,line_label
order by store,page,line,line_label


--7/6/17 ----------------------------------------------------------------------------------
reporting
fs count - sales/cogs/gross for those units || sales/cogs/gross
what about unwinds?
start with used cars retail certified P6L1
defined by the accounts routed to the line
The accounts and totals for each account: -- probably don't need totals at this stage
-- just line/accounts and account attributes
    drop table if exists accounts_lines cascade;
    create temp table accounts_lines as
    select year_month, page, line, col, line_label, gm_account, gl_account, 
      d.store, area, d.department, sub_Department, account_type, 
      cc.description as account_description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
    inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
    inner join fin.dim_account cc on c.gl_account = cc.account
    inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
--       and d.store = 'ry1'
    where ((b.page = 16 and b.line between 1 and 14)
      or (b.page between 5 and 15))
      and b.year_month > 201700
    order by d.store,  b.line, b.col;   
    create unique index on accounts_lines(year_month, gl_account);
    
select * from accounts_lines order by store, page, line

-- it is looking like the count really is not the count, it is the count minus unwinds
-- actually sold 18 cert cars & unwound 1 resulting in 17 on the statement
-- figure count by account_type = sale and journal = VSU, 
-- if amount < 0 unit_count = 1, if amount > 0 unit_count = -1

-- 30169a: one stock number two deals

drop table if exists  unit_counts;
create temp table unit_counts as
-- count by control/transaction
select b.year_month, b.the_date, cc.store, cc.page, cc.line, cc.col, cc.gm_account, cc.gl_account, cc.account_type,
--   b.account_type, b.description, b.control, b.amount
   c.description as account_description, d.journal_code, a.control, trans, seq, a.amount,
   case 
     when a.amount < 0 then 1
     else
       case
         when a.amount > 0 then -1
         else 0
       end
  end as unit_count, e.description,
  a.doc, a.ref
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_lines cc on c.account = cc.gl_account
  and cc.year_month = b.year_month
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and b.year_month = 201706
  and journal_code in ('VSU','VSN')
  and c.account_Type = 'sale'
--   and control = '30169a'
order by control;

create unique index on unit_counts(control, the_date, unit_count);

-- count by line
-- matches statement counts
select x.*, sum(line_count) over (partition by store,page) as page_count
from (   
  select store, page, line, sum(unit_count) as line_count
  from unit_counts
  group by store, page, line) x
order by store, page, line

select year_month, store, page, sum(unit_count)
from unit_counts
group by year_month, store, page
order by store, year_month, page

select a.*
from unit_counts a
where control = '27912R'

select a.*
from unit_counts a
where control = '29160'

order by page, line


-- control exists on more than one line
select *
from unit_counts
where control in (
  select control
  from (
  select store, page, line, control
  from unit_counts
  group by store, page, line, control
  ) x group by control having count(*) > 1)
order by control, page, line  

-- count by control/line
-- doesn't work for 31242x, this is where i have to be able to distinguish
-- between multiple deals in the same month on the same stock_number
-- hmm, transaction description will be different
-- 31137A will be a good test as well, that posting was a mistake, should have been 31137
select store, page, line, control, sum(unit_count) as unit_count
from unit_counts
group by store, page, line, control order by s8
order by store, page, line, control

select *
from unit_counts
where control = '31242x'

-- hmm, transaction description will be different
select store, page, line, control, description, sum(unit_count) as unit_count
from unit_counts
where control = '30169a'
group by store, page, line, control, description
order by store, page, line, control



-- the real problem now is how to correlate accounting to these 2 entries
select store, page, line, control, sum(unit_count)
from unit_counts
where control = '30169a'
group by store, page, line, control
order by store, page, line, control

-- maybe by account ?
-- except unit_counts only has sale accounts
select a.*
from unit_counts a
where control = '30169a'

-- want some kind of display that verifies the statement
-- but also shows the reality that the statement does not show
-- eg unwinds
-- but also just the actual deals for the statement month and the accounting for those deals
-- separate out accounting transactions for something other than car deals for the statement month
-- the trick really is clearly defining what it is i want to see

-- discerning the difference between the number of units sold (not affected by unwinds) vs unit count total (decreased by unwinds)
select * from fin.dim_Account limit 100

    select b.the_date, a.control, a.doc, a.ref, c.account, cc.store, cc.page, cc.line, d.journal_code, 
      c.description as acct_description, e.description as trans_description, a.amount
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join accounts_lines cc on c.account = cc.gl_account
    inner join fin.dim_journal d on a.journal_key = d.journal_key
    inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
    where a.post_status = 'Y'
      and b.year_month = 201706
      and a.control = '30169a'

select a.*
from unit_counts a
inner join (
  select control
  from unit_counts
  group by control
  having count(*) > 1) b on a.control = b.control
order by control  

select control, trans, unit_count
from unit_counts a
where control in ('26501','29818RA','30062B','30169A','31137A','31242X') 
order by control, trans


select store, unit_count, count(*) from (
select store, control, sum(unit_count) as unit_count
from unit_counts
group by store, control
) x group by store, unit_count


select *
from unit_counts a
where exists (
  select 1
  from unit_counts
  where control = a.control
    and unit_count <> 1)
order by control

select stock_number, bopmast_id, unit_count, seq, vin, year_month, gl_date, deal_Status, notes, b.fullname
from sls.deals a
left join ads.ext_dim_customer b on a.buyer_bopname_id = b.bnkey
where stock_number in ('26501','29818RA','30062B','30169A','31137A','31242X') 
order by stock_number, bopmast_id, seq

select control,
  sum(case when c.description = 'CST U/C RETAIL CERTIFIED' then a.amount else 0 end) as unit_cogs,
  sum(case when c.description = 'CST U/C RTL RECON CERT' then a.amount else 0 end) as unit_recon_cogs,
  sum(case when c.description = 'SLS U/C RETAIL CERTIFIED' then a.amount else 0 end) as unit_sales,
  sum(a.amount) as unit_gross
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_lines cc on c.account = cc.gl_account
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and b.year_month = 201706
group by control   
order by control


select y.*, sum(gross) over (partition by store, page, line) line_gross
from (
  select x.store, x.page, x.line, xx.control,
    sum(case when x.account_type = 'cogs' then xx.amount else 0 end) as cogs,
    sum(case when x.account_type = 'sale' then xx.amount else 0 end) as sales,
    sum(xx.amount) as gross
  from accounts_lines x 
  left join (
    select b.the_date, a.control, a.doc, a.ref, c.account, cc.store, cc.page, cc.line, d.journal_code, 
      c.description as acct_description, e.description as trans_description, a.amount
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join accounts_lines cc on c.account = cc.gl_account
    inner join fin.dim_journal d on a.journal_key = d.journal_key
    inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
    where a.post_status = 'Y'
      and b.year_month = 201706
      and cc.store <> 'none') xx on x.gl_account = xx.account --and x.page = xx.page and x.line = xx.line
  where x.store <> 'none'    
  group by x.store, x.page, x.line, xx.control) y 
where control = '30169a'  
order by y.store, y.page, y.line, y.control


select *
from accounts_lines
where store = 'ry1'
  and page = 7


select b.the_date, a.control, a.doc, a.ref, c.account, cc.store, cc.page, cc.line, d.journal_code, 
  c.description as acct_description, e.description as trans_description, a.amount
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_lines cc on c.account = cc.gl_account
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and b.year_month = 201706
  and c.account in ('1402103','1402003','1602103','1602003')
  and cc.store = 'ry1' order by cc.store,page,line
  and page = 7


        


  




-- 7/8  ok, what the fuck am i doing, what is the next step

what i want to see is $$ for each line

accounts for lines: 1 row per account & month
unit_counts: 1 row per control(stock_number), trans & amount

maybe i am getting confused because i did unit_counts first, do gl transactions by year_month, account first

select * from accounts_lines limit 10

-- this will be all relevant (account, date) transactions
drop table if exists test_1 cascade;
create temp table test_1 as
select b.year_month, b.the_date, a.control, a.doc, a.trans, a.seq, 
  c.account, c.description as account_description, cc.account_type, cc.gm_account,
  d.journal_code, e.description as trans_description, 
  cc.store, cc.area, cc.department, cc.sub_department, cc.page, cc.line, cc.col,
  a.amount,
  case 
    when cc.account_type = 'sale' and d.journal_code in ('VSN','VSU') then
      case
        when a.amount < 0 then 1
        else
          case
            when a.amount > 0 then -1
            else 0
          end
      end
    else 0
  end as unit_count
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts_lines cc on b.year_month = cc.year_month
  and c.account = cc.gl_account
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and b.year_month > 201700
  and cc.store <> 'none'
order by store, year_month, page, line, col;

create unique index on test_1 (trans, seq);
create index on test_1(control);
create index on test_1(store);
create index on test_1(page);
create index on test_1(line);

select * from test_1 limit 10
-- page gross looks good
select store, year_month, page, sum(amount)
from test_1
group by store, year_month, page
order by store, year_month, page

-- unit count looks good
select store, year_month, page, sum(unit_count)
from test_1
where year_month = 201706
group by store, year_month, page
order by store, year_month, page

select *
from test_1
where control = '30169a'

do i represent the transaction for the 30169a sale and unwind as part of the overall 30169a transactions for june
or are they part of the separate non-actual-deal transactions that contribute to the total?


select a.* ,
  sum(unit_count) over (partition by control) as real_deal
from test_1 a
where year_month = 201706
  and store = 'ry1'
  and page = 5 and line = 5
order by sum(unit_count) over (partition by control)

  
select , sum(real_deal) over (partition by page, line) 
from (
select a.* ,
  sum(unit_count) over (partition by control) as real_deal
from test_1 a
where year_month = 201706
  and store = 'ry1') x
order by page, line

select *
from test_1
where year_month = 201706
  and store = 'ry1'
  and page = 5 
--   and line = 5
  and unit_count <> 0  

can there be more than one june_deal value for a stock_number
select a.*, sum(unit_count) over (partition by control, page, line) as june_del
from test_1 a
where year_month = 201706
--   and unit_count <> 0  
--   and control in ('26501','29818RA','30062B','30169A','31137A','31242X') 
order by control, the_date


can there be more than one june_deal value for a stock_number
yep
select control
from (
  select control, june_del
  from (
    select a.*, sum(unit_count) over (partition by control, page, line) as june_del
    from test_1 a
    where year_month = 201706) x
  group by control, june_del) xx
group by control
having count(*) > 1 


select * from test_1
where control in ('30151a','30607','31293x')
order by control, the_date  

-- hmm are correction consistently identifiable by control, account, page, line summing to 0
-- 291054RA: car wash $39 appled to 165101 instead of 165100
select * 
from test_1
where control in (
  select control
  from (
    select control, line
    from test_1
    where amount = 39
    group by control, line) x
  group by control having count(*) > 1) 
order by control, the_date


-- don't know about including year_month in group
-- so, yes, some, but not all of these are corrections, don't know how exhaustive it is
select a.*
from test_1 a
inner join (
select year_month, control, account, page, line
from test_1
group by year_month, control, account, page, line
having sum(amount) = 0) b on a.year_month = b.year_month
  and a.control = b.control and a.account = b.account
  and a.page = b.page and a.line = b.line
order by a.control

-- ok, these have been interesting diversions, but, what do i want from test_1
-- first, what is the PK, think i am going to have to go with trans/seq, essentially a surrogate
-- should be ok, can generate unique subsets of data, i believe, to satisfy different needs

ok, can get counts and amounts at the page/line level

-- page gross looks good
select store, year_month, page, sum(amount)
from test_1
group by store, year_month, page
order by store, year_month, page

-- unit count looks good
-- 201706 chev new cars
-- fs shows 17, 17 delivered
select store, year_month, page, sum(unit_count)
from test_1
where year_month = 201706
group by store, year_month, page
order by store, year_month, page

-- what were the real deliveries
select *
from test_1
where year_month = 201706
  and store = 'ry1'
  and page = 5
  and line between 1 and 20
  and unit_count <> 0 
order by control  

-- these are the 17 deliveries
select control
from test_1
where year_month = 201706
  and store = 'ry1'
  and page = 5
  and line between 1 and 20
group by control
having sum(unit_count) <> 0


-- fs shows 17, 18 delivered (also, 31130XX unwound, 18 - 1 = 17)
-- what were the real deliveries
select *
-- select sum(amount)
from test_1
where year_month = 201706
  and store = 'ry1'
  and page = 16
  and line = 1
--   and unit_count > 0 -- actual deliveries
  and unit_count <> 0 -- fs count
order by control  


-- deliveries from sls.deals
select b.fullname, a.stock_number, a.* 
from sls.deals a
inner join ads.ext_dim_customer b on a.buyer_bopname_id = b.bnkey
where store_code = 'ry1'
  and year_month = 201706
  and sale_group_code = 'ucc'
  and sale_type_code <> 'W'
  and exists (
    select 1
    from sls.deals
    where bopmast_id = a.bopmast_id
    group by bopmast_id
      having sum(unit_count) > 0)
order by stock_number  

-- gross on transactions for other than del vehicles
select *
-- select sum(amount)  -- 645.06
from test_1 a
where year_month = 201706
  and store = 'ry1'
  and page = 16
  and line = 1
  and exists (
    select 1
    from test_1
    where control = a.control
    group by control
    having sum(unit_count) < 1)
order by control  

-- gross on del vehicles
select *
-- select sum(amount)  -- 17165.5
from test_1 a
where year_month = 201706
  and store = 'ry1'
  and page = 16
  and line = 1
  and exists (
    select 1
    from test_1
    where control = a.control
    group by control
    having sum(unit_count) = 1)
order by control  


select x.*, sum(amount) over (partition by line)
from ( 
select line, control, sum(amount) as amount
from test_1
where year_month = 201706
  and store = 'ry1'
  and page = 16
group by line, control) x
order by line, control  


select page, line, sum(amount) as total_gross,
  sum(
    case 
      when exists (
        select 1 
        from test_1
        where control = a.control
        group by control
        having sum(unit_count) = 1) then amount else 0 end) as del_gross,
  sum(
    case 
      when exists (
        select 1 
        from test_1
        where control = a.control
        group by control
        having sum(unit_count) < 1) then amount else 0 end) as non_del_gross      
from test_1 a
where year_month = 201706
  and store = 'ry1'
  and page between 5 and 16
group by page, line

