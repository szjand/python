# coding=utf-8
import db_cnx
import ops
import string
import csv
# import datetime
# 2/16/16 tried cursor to cursor, too fucking slow
# todo adjust dates in where clause
# todo db2 connection to "ubuntu server" to get daily scrapes, hmmm, db2 data already in postgres ...
# todo return to extracting from arkona
# todo separate out glptrns from deals and add appropriate dependencies

task = 'ext_deals'
pg_con = None
# db2_con = None
ubuntu_con = None
run_id = None
# TODO fucking hard coded year in the date generation
month_day = '0402'
the_date = month_day[0:2] + '-' + month_day[2:4] + '-2017'
# the_date = '2016' + '-' + month_day[0:2] + '-' + month_day[2:4]
# the_date = datetime.datetime.strptime('2016' + '-' + month_day[0:2] + '-' + month_day[2:4], '%Y-%m-%d').date()
# the a & b appended to the table name are aliases where these tables are used in joins
bopmast_table = 'test.ext_bopmast_' + month_day + ' a'
glptrns_table = 'test.ext_glptrns_' + month_day + ' a'
glpmast_table = 'test.ext_glpmast_' + month_day + ' b'
bopmast_file = 'files/ext_deals.csv'
glptrns_file = 'files/ext_glptrns_for_deals.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.postgres_ubuntu() as ubuntu_con:
        with ubuntu_con.cursor() as ubuntu_cur:
            sql = ("""
                --select '01/31/2016'::date as run_date, bopmast_company_number,
                select '%s', bopmast_company_number,
                  bopmast_stock_number,
                  bopmast_vin, bopmast_search_name,
                  case
                    when coalesce(primary_salespers, 'None') = 'AMA' then 'HAN'
                    when coalesce(primary_salespers, 'None') = 'HAL' and bopmast_company_number = 'RY1' then 'JHA'
                    when coalesce(primary_salespers, 'None') = 'ENT' and bopmast_company_number = 'RY1' then 'AJO'
                    when coalesce(primary_salespers, 'None') = 'WWW' and bopmast_company_number = 'RY2' then 'WHE'
                    else coalesce(primary_salespers, 'None')
                  end,
                  case
                    when coalesce(secondary_slspers2, 'None') = 'AMA' then 'HAN'
                    when coalesce(secondary_slspers2, 'None') = 'HAL' and bopmast_company_number = 'RY1' then 'JHA'
                    when coalesce(secondary_slspers2, 'None') = 'ENT' and bopmast_company_number = 'RY1' then 'AJO'
                    when coalesce(secondary_slspers2, 'None') = 'WWW' and bopmast_company_number = 'RY2' then 'WHE'
                    else coalesce(secondary_slspers2, 'None')
                  end,
                  -- coalesce(primary_salespers, 'None'),
                  -- coalesce(secondary_slspers2, 'None'),
                  coalesce(record_status, 'None'), date_approved, date_capped, origination_date,
                  coalesce(b.year::citext, 'UNK'), coalesce(b.make, 'UNK'),
                  coalesce(b.model, 'UNK'),
                  a.record_key
                from %s
                left join dds.ext_inpmast b on a.bopmast_stock_number = b.inpmast_stock_number
                  and inpmast_vin <> '1MEBM50U1HG626400'
                where origination_date > 20150000
                  and sale_type <> 'W'
                  and bopmast_vin is not null
                  and bopmast_stock_number <> '2'
                    --  1/16/17 wtf, 2 bopmast rows for H9313B
                    and bopmast_stock_number <> 'H9313B'
                  and bopmast_stock_number in (
                    select bopmast_stock_number
                    --from test.ext_bopmast_0131
                    from %s
                    group by bopmast_stock_number
                    having count(*) = 1)
                union -- sanitized subset of dups excluded from the above query
                select *
                from (
                  select '%s', bopmast_company_number,
                    bopmast_stock_number,
                    bopmast_vin, bopmast_search_name,
                    case
                      when coalesce(primary_salespers, 'None') = 'AMA' then 'HAN'
                      when coalesce(primary_salespers, 'None') = 'HAL' and bopmast_company_number = 'RY1' then 'JHA'
                      when coalesce(primary_salespers, 'None') = 'ENT' and bopmast_company_number = 'RY1' then 'AJO'
                      when coalesce(primary_salespers, 'None') = 'WWW' and bopmast_company_number = 'RY2' then 'WHE'
                      else coalesce(primary_salespers, 'None')
                    end,
                    case
                      when coalesce(secondary_slspers2, 'None') = 'AMA' then 'HAN'
                      when coalesce(secondary_slspers2, 'None') = 'HAL' and bopmast_company_number = 'RY1' then 'JHA'
                      when coalesce(secondary_slspers2, 'None') = 'ENT' and bopmast_company_number = 'RY1' then 'AJO'
                      when coalesce(secondary_slspers2, 'None') = 'WWW' and bopmast_company_number = 'RY2' then 'WHE'
                      else coalesce(secondary_slspers2, 'None')
                    end,
                    -- coalesce(primary_salespers, 'None'),
                    -- coalesce(secondary_slspers2, 'None'),
                    coalesce(record_status, 'None') as record_status, date_approved, date_capped, origination_date,
                    coalesce(b.year::citext, 'UNK'), coalesce(b.make, 'UNK'),
                    coalesce(b.model, 'UNK'),
                    a.record_key
                  --from test.ext_bopmast_0310 a
                  from %s
                  left join dds.ext_inpmast b on a.bopmast_stock_number = b.inpmast_stock_number
                    and inpmast_vin <> '1MEBM50U1HG626400'
                  where origination_date > 20150000
                    and sale_type <> 'W'
                    and bopmast_vin is not null
                    and bopmast_stock_number <> '2'
                    --  1/16/17 wtf, 2 bopmast rows for H9313B
                    and bopmast_stock_number <> 'H9313B'
                    and bopmast_stock_number in (
                      select bopmast_stock_number
                      --from test.ext_bopmast_0310
                      from %s
                      group by bopmast_stock_number
                      having count(*) > 1)) z
                -- this does the sanitizing: eliminate those rows with no record_status
                where record_status <> 'none'
            """) % (the_date, bopmast_table, bopmast_table, the_date, bopmast_table, bopmast_table)
            ubuntu_cur.execute(sql)
            with open(bopmast_file, 'wb') as f:
                csv.writer(f).writerows(ubuntu_cur)
        with ubuntu_con.cursor() as ubuntu_cur:
            # TODO i can't fucking believ e i hard coded the glpmast year
            sql = ("""
                select
                  gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtrsts, gtadjust, gtpsel,
                  gtjrnl, gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl_), trim(gtdoc_),
                  trim(gtrdoc_), gtrdtyp, trim(gtodoc_), trim(gtref_), trim(gtvnd_),
                  trim(gtdesc), gttamt, gtcost, gtctlo,gtoco_, 1
                FROM  %s
                inner join %s on trim(a.gtacct) = trim(b.account_number)
                -- WHERE b.year = 2016
                where year = 2017
                    and b.account_type = '4'
                    and b.department in ('NC','UC')
                    and b.account_desc not like '%%WH%%'
                    and b.account_sub_type in ('A','B')
                    -- and a.gtpost <> 'V'
                    -- and a.gttamt < 0
                    and a.gtjrnl in ('VSN','VSU')
            """) % (glptrns_table, glpmast_table)
            ubuntu_cur.execute(sql)
            with open(glptrns_file, 'wb') as f:
                csv.writer(f).writerows(ubuntu_cur)
    # with db_cnx.pg() as pg_con:
    with db_cnx.pg_jon_localhost() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate scpp.ext_deals")
            with open(bopmast_file, 'r') as io:
                pg_cur.copy_expert("""copy scpp.ext_deals from stdin with csv encoding 'latin-1'""", io)
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate scpp.ext_glptrns_for_deals")
            with open(glptrns_file, 'r') as io:
                pg_cur.copy_expert("""copy scpp.ext_glptrns_for_deals from stdin with csv encoding 'latin-1'""", io)
        with pg_con.cursor() as pg_cur:
            sql = ("""
                insert into scpp.xfm_glptrns (row_type,stock_number,gl_date,gl_count,run_date)
                select 'New',a.gtctl_, a.gtdate,
                  sum(case when a.gttamt < 0 then 1 else -1 end) as unit_count,
                  '%s'
                from scpp.ext_glptrns_for_deals a
                left join (
                -- ok, these are the vehicles with multiple entries on a day that cancel each other out (by polarity)
                  select gtctl_, gtdate
                  from ( -- assign unit count to rows w/multiple rows per date
                    select a.gtctl_, a.gtdate,
                      case when a.gttamt < 0 then 1 ELSE - 1 end as the_count
                    from scpp.ext_glptrns_for_deals a
                    inner join ( -- multiple rows per date
                      select gtctl_, gtdate
                      from scpp.ext_glptrns_for_deals
                      where gtpost = 'Y'
                      group by gtctl_, gtdate
                      having count(*) > 1) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
                    where a.gtpost = 'Y') x
                  group by gtctl_, gtdate
                  having sum(the_count) = 0) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
                where gtpost = 'Y'
                  AND not exists (
                    select 1
                    from scpp.xfm_glptrns
                    where stock_number = a.gtctl_
                      and gl_date = a.gtdate)
                  and b.gtctl_ is null  -- this is the line that excludes the multiple entry vehicles
                group by a.gtctl_, a.gtdate
        """) % the_date
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    # if db2_con:
    #     db2_con.close()
    if ubuntu_con:
        ubuntu_con.close()
    if pg_con:
        pg_con.close()
