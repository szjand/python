# coding=utf-8
import db_cnx
import ops
import string

task = 'xfm_deals_for_update'
pg_con = None
run_id = None

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            # new rows
            sql = "truncate scpp.xfm_deals_for_update"
            pg_cur.execute(sql)
            # candidate rows for update, xfm_deals (x) joined with deals (y)
            sql = """
                insert into scpp.xfm_deals_for_update
                select  x.year_month_date_capped, x.year_month_run_date, x.employee_number, x.stock_number,
                  x.unit_count, x.date_capped, x.gl_date, x.customer_name, x.model_year, x.make, x.model,
                  x.seq, x.record_status, x.run_date, x.status_change,
                  y.year_month, y.employee_number,
                  y.stock_number, y.unit_count,
                  y.deal_date,
                  y.customer_name, y.model_year,
                  y.make, y.model, y.seq,
                  y.notes, y.deal_status, y.run_date, x.gl_count, x.gl_year_month
                from ( -- x:
                  select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer
                        as year_month_date_capped,
                    (100* extract(year from a.run_date) + extract(month from a.run_date)):: integer
                        as year_month_run_date,
                    coalesce(b.employee_number, 'House') as employee_number, a.stock_number as stock_number,
                    case
                      when secondary_sc = 'None' then 1
                      else .5
                    end as unit_count,
                    a.date_capped, a.customer_name as customer_name,
                    a.model_year as model_year, a.make as make, a.model as model, 'Dealertrack' as deal_source,
                    (select max(d.seq) + 1 from scpp.deals d where d.stock_number = a.stock_number) as seq,
                    case a.record_status
                      when 'U' then 'Capped'
                      when 'A' then 'Accepted'
                      when 'None' then 'None'
                    end as record_status, a.run_date as run_date, status_change as status_change,
                    a.gl_date, a.gl_count,
                    case
                      when a.gl_date is not null then
                        (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
                      else 0
                    end as gl_year_month
                  from scpp.xfm_deals a
                  -- inner join, RY1 only: FK constraint ref s_c_config
                  inner join scpp.sales_consultants b on
                    case
                      when a.store_code = 'RY1' then a.primary_sc = b.ry1_id
                      when a.store_code = 'RY2' then a.primary_sc = b.ry2_id
                    end
                    and b.employee_number not in ('150040','1106225')
                  where a.run_date = (select max(xd.run_date) from scpp.xfm_deals xd)
                    and a.row_type = 'update'
                    and a.seq = (
                      select max(e.seq)
                      from scpp.xfm_deals e
                      where e.stock_number = a.stock_number)
                  union -- secondary sc
                  select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer
                        as year_month_date_capped,
                    (100* extract(year from a.run_date) + extract(month from a.run_date)):: integer
                        as year_month_run_date,
                    coalesce(b.employee_number, 'House'), a.stock_number,
                    0.5 as unit_count,
                    a.date_capped, a.customer_name,
                    a.model_year, a.make, a.model, 'Dealertrack',
                    (select max(h.seq) + 1 from scpp.deals h where h.stock_number = a.stock_number) as seq,
                    case a.record_status
                      when 'U' then 'Capped'
                      when 'A' then 'Accepted'
                      when 'None' then 'None'
                    end, a.run_date, a.status_change,  a.gl_date, a.gl_count,
                    case
                      when a.gl_date is not null then
                        (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
                      else 0
                    end as gl_year_month
                  from scpp.xfm_deals a
                  inner join scpp.sales_consultants b on
                    case
                      when a.store_code = 'RY1' then a.secondary_sc = b.ry1_id
                      when a.store_code = 'RY2' then a.secondary_sc = b.ry2_id
                    end
                    and b.employee_number not in ('150040','1106225')
                  where a.secondary_sc <> 'None'
                    and a.run_date = (select max(i.run_date) from scpp.xfm_deals i)
                    and a.row_type = 'update'
                    and a.seq = (
                      select max(f.seq)
                      from scpp.xfm_deals f
                      where f.stock_number = a.stock_number)) x

              inner join scpp.deals y on x.stock_number = y.stock_number
                -- modified to include deleted deals
                -- modified to be only status_changes and deleted deals
                and (x.record_status <> y.deal_status or x.status_change = 'deleted')
                and y.seq = (
                  select max(g.seq)
                  from scpp.deals g
                  where g.stock_number = y.stock_number)
              where not ((x.record_status = 'Accepted' or x.record_status = 'None') and y.deal_status = 'Deleted')
                -- already busted down to status None, no need to process Delete in this case
                and not (x.status_change = 'Deleted' and y.deal_status = 'None')
                and x.status_change not in ('No Change', 'None to Accepted', 'Accepted to None')
                -- EX1
                and not (x.status_change = 'Accepted to Capped'
                    and y.deal_status = 'Capped' and y.deal_date = x.gl_date)
                -- EX2
                and not (x.status_change = 'Capped to Accepted' and y.deal_status = 'Deleted')
                -- split deal unwind, had to handle manually
                and x.stock_number <> '29910xxa'
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
