# coding=utf-8
'''
4/23/17
    limit to gl_date >= 2/1/17
    since sls.ext_accounting_deals pk is control & date, i feel safe in limiting to the latest date
      since earlier rows will have been processed earlier
'''
import db_cnx
# TODO 30635 issue, capped and uncapped on same day (3/8/17) resulting in 2 rows in xfm_glptrs
# task = 'xfm_deals'
pg_con = None
# the_date = str(datetime.datetime.now() - datetime.timedelta(days=1))[:10]
# month_day = ((datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%m") +
#             (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%d"))
# print the_date
month_day = '0602'
the_date = '2017-06-02'
try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            ###########################
            # new rows
            ###########################
            sql = """
                insert into sls.xfm_deals (run_date, store_code, bopmast_id, deal_status,
                  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                  primary_sc, secondary_sc, fi_manager, origination_Date, approved_date,
                  capped_date, delivery_date, gap, service_contract, total_care,
                  row_type, seq,
                  gl_date, gl_count, hash)
                select a.run_date, a.store_code, a.bopmast_id, a.deal_status,
                  a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
                  a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
                  'new', 1,
                  coalesce(b.gl_date, '12/31/9999'), coalesce(b.unit_count, 0),
                  ( -- generate the hash
                    select md5(z::text) as hash
                    from (
                      select store_code, bopmast_id, deal_status,
                        deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                        odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                        primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                        capped_date, delivery_date, gap, service_contract,total_care,
                        coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
                      from sls.ext_deals x
                      left join sls.ext_accounting_deals y on x.stock_number = y.control
                        and y.gl_date = (
                          select max(gl_date)
                          from sls.ext_accounting_deals
                          where control = y.control)
                      where x.store_code = a.store_code
                        and x.bopmast_id = a.bopmast_id) z)
                from sls.ext_deals a
                left join sls.ext_accounting_deals b on a.stock_number = b.control
                  and b.gl_date = (
                    select max(gl_date)
                    from sls.ext_accounting_deals
                    where control = b.control)
                where gl_date >= '02/01/2017'
                  and not exists (
                    select *
                    from sls.xfm_deals
                    where store_code = a.store_code
                      and bopmast_id = a.bopmast_id) order by stock_number;
            """
            pg_cur.execute(sql)
            ###########################
            # changed rows
            # disappearing stk/vin 3/8: coalesce(e., f.)
            # 3/21 xfm_deals.hash can not be unique: 29523R: capped 3/13, uncapped 3/14, capped 3/21: no changes
            #           hash same as 3/13 row, legitimately
            ###########################
            sql = """
                insert into sls.xfm_deals
                select {0}, e.store_code, e.bopmast_id, e.deal_status, e.deal_type,
                  e.sale_type, e.sale_group, e.vehicle_type,
                  -- e.stock_number, e.vin,
                  coalesce(e.stock_number, f.stock_number), coalesce(e.vin, f.vin),
                  e.odometer_at_sale, e.buyer_bopname_id, e.cobuyer_bopname_id,
                  e.primary_sc, e.secondary_sc, e.fi_manager, e.origination_date,
                  e.approved_date, e.capped_date, e.delivery_date, e.gap,
                  e.service_contract, e.total_care,
                  'update'::citext,
                  (
                    select max(seq) + 1
                    from sls.xfm_deals
                    where store_code = e.store_code
                      and bopmast_id = e.bopmast_id) as seq,
                   e.gl_date, e.gl_count, e.hash
                from (
                  select z.*, md5(z::text) as hash
                  from (
                    select store_code, bopmast_id, deal_status,
                      deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                      capped_date, delivery_date, gap, service_contract,total_care,
                      coalesce(gl_date, '12/31/9999') as gl_date, coalesce(unit_count, 0) as gl_count
                    from sls.ext_deals x
                    left join sls.ext_accounting_deals y on x.stock_number = y.control
                      and y.gl_date = (
                        select max(gl_date)
                        from sls.ext_accounting_deals
                        where control = y.control)) z) e
                inner join sls.xfm_deals f on e.store_code = f.store_code
                  and f.seq = (
                    select max(seq)
                    from sls.xfm_deals
                    where store_code = f.store_code
                      and bopmast_id = f.bopmast_id)
                  and e.bopmast_id = f.bopmast_id
                  and e.hash <> f.hash;
            """.format("'" + the_date + "'")
            pg_cur.execute(sql)
            ###########################
            # deleted rows
            # 3/8 exclude rows that are already deleted
            #     in hash generator, substitued 'deleted' for deal_status
            ###########################
            sql = """
                insert into sls.xfm_deals (run_date, store_code, bopmast_id, deal_status,
                  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                  capped_date, delivery_date, gap, service_contract, total_care,
                  row_type, seq,
                  gl_date, gl_count, hash)
                select {0}, a.store_code, a.bopmast_id, 'deleted'::citext as deal_status,
                  a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
                  a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
                  'update'::citext as row_type,
                  (select max(seq) + 1 from sls.xfm_deals where store_code = a.store_code and bopmast_id = a.bopmast_id) as seq,
                  c.gl_date, c.unit_count,
                  ( -- generate a new hash
                    select md5(z::text) as hash
                    from (
                      select store_code, bopmast_id, 'deleted',
                        deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                        odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                        primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                        capped_date, delivery_date, gap, service_contract,total_care,
                        gl_date, gl_count
                      from sls.xfm_deals k
                      where store_code = a.store_code
                        and bopmast_id = a.bopmast_id
                        and seq = (
                          select max(seq)
                          from sls.xfm_deals
                          where store_code = k.store_code
                            and bopmast_id = k.bopmast_id)) z)
                from sls.xfm_deals a
                left join sls.ext_deals b on a.store_code = b.store_code
                  and a.bopmast_id = b.bopmast_id
                left join sls.ext_accounting_deals c on a.stock_number = c.control
                  and c.gl_date = (
                    select max(gl_date)
                    from sls.ext_accounting_deals
                    where control = c.control)
                where b.store_code is null -- no longer in extract
                  and a.seq = (
                    select max(seq)
                    from sls.xfm_deals
                    where store_code = a.store_code
                      and bopmast_id = a.bopmast_id)
                  and a.deal_status <> 'deleted';
            """.format("'" + the_date + "'")
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
