# coding=utf-8
import db_cnx

pg_con = None

try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            ###########################
            # new rows
            # -- this will actually be new deals as well as deals that previously existed
            # -- in xfm_ but were not capped (ie not in deals)
            # -- any deal not already in deals, therefor seq = 1
            # need to add hash and notes - explicit insert
            ###########################
            sql = """
                insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                  sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                  primary_sc, secondary_sc, fi_manager, origination_date,
                  approved_date, capped_date, delivery_date, gap, service_contract,
                  total_care, seq, year_month, unit_count,
                  gl_date, deal_status, notes, hash)
                select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
                  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                  a.total_care,
                  1 as seq,
                  case
                    when a.gl_date is null then
                      (100* extract(year from a.capped_date) + extract(month from a.capped_date)):: integer
                    else
                      (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
                  end as year_month,
                  a.gl_count,
                  case -- delivery date may figure into this, eventually
                    when a.gl_date is null then a.capped_date
                    else a.gl_date
                  end as deal_date,
                  case
                    when a.deal_status = 'U' then 'capped'
                    when a.deal_status = 'A' then 'accepted'
                    else 'none'
                  end as deal_status, 'new row', a.hash
                from sls.xfm_deals a
                left join sls.deals b on a.store_code = b.store_code
                  and a.bopmast_id = b.bopmast_id
                where a.deal_status = 'U'
                  and coalesce(a.gl_date, a.capped_date) > '01/31/2017' -- only feb and later deals
                  and b.store_code is null;
            """
            pg_cur.execute(sql)
            ###########################
            # changed rows
            ###########################
            pg_cur.execute('truncate sls.changed_deals')
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
