# coding=utf-8
import db_cnx
import ops
import string


task = 'load_deals'
pg_con = None
run_id = None
_record_status_change = None
_deal_status = None
_stock_number = None

# TODO: remove exclusion of RY2 AND garceau/olderbak (pay plan = finance)

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            # new rows
            # -- this will actually be new deals as well as deals that previously existed
            # -- in xfm_ but were not capped (ie not in deals)
            # -- any deal not already in deals, therefor seq = 1
            sql = """
                insert into scpp.deals (year_month,employee_number,stock_number,unit_count,
                  deal_date,customer_name,model_year,make,model,deal_source,seq, deal_status,run_date)
                select
                  case
                    when a.gl_date is null then
                      (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer
                    else
                      (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
                  end as year_month,
                  coalesce(b.employee_number, 'House'), a.stock_number,
                  case
                    when secondary_sc = 'None' then 1
                    else .5
                  end as unit_count,
                  case
                    when a.gl_date is null then a.date_capped
                    else a.gl_date
                  end as deal_date, a.customer_name,
                  a.model_year, a.make, a.model, 'Dealertrack', 1, 'Capped', a.run_date
                from scpp.xfm_deals a
                -- inner join, RY1 only: FK constraint ref s_c_config
                inner join scpp.sales_consultants b on
                  case
                    when a.store_code = 'RY1' then a.primary_sc = b.ry1_id
                    when a.store_code = 'RY2' then a.primary_sc = b.ry2_id
                  end
                  and b.employee_number not in ('150040','1106225')
                where coalesce(a.gl_date, a.date_capped) > '12/31/2015' --i think
                  AND not exists (
                    select *
                    from scpp.deals
                    where stock_number = a.stock_number)
                  -- fuck me RY2 transfers, need to exclude there pre-RY1 deals
                  -- 2/6/17 all have been termed
                  and
                    case
                      when b.employee_number in ('1132420','164050','173489') then
                       case
                          when a.gl_date is null then a.date_capped
                          else a.gl_date
                        end > '04/30/2016'
                      else 1 = 1
                    end
                union
                select
                  case
                    when a.gl_date is null then
                      (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer
                    else
                      (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
                    end as year_month,
                  coalesce(b.employee_number, 'House'), a.stock_number,
                  0.5 as unit_count,
                  a.date_capped, a.customer_name,
                  a.model_year, a.make, a.model, 'Dealertrack', 1, 'Capped', a.run_date
                from scpp.xfm_deals a
                inner join scpp.sales_consultants b on
                  case
                    when a.store_code = 'RY1' then a.secondary_sc = b.ry1_id
                    when a.store_code = 'RY2' then a.secondary_sc = b.ry2_id
                  end
                  and b.employee_number not in ('150040','1106225')
                where a.secondary_sc <> 'None'
                  AND coalesce(a.gl_date, a.date_capped) > '12/31/2015' --i think
                  AND not exists (
                    select *
                    from scpp.deals
                    where stock_number = a.stock_number)

            """
            pg_cur.execute(sql)
            # update deals
            sql = "select * from scpp.xfm_deals_for_update"
            pg_cur.execute(sql)
            for row in pg_cur:
                _record_status_change = row[14]
                _deal_status = row[26]
                _stock_number = row[3]
                with pg_con.cursor() as pg_cur_2:
                    # matrix: 1
                    if _record_status_change == 'Deleted' and _deal_status == 'Capped':
                        sql = """
                            insert into scpp.deals (year_month,employee_number,stock_number,unit_count,
                              deal_Date,customer_name,model_year,make,model,deal_source,seq,
                              notes,deal_status,run_date)
                            select x_year_month_run_date, y_employee_number, y_stock_number, -1, x_run_date,
                              y_customer_name, y_model_year, y_make, y_model, 'Dealertrack',
                              (select max(seq) + 1 from scpp.deals where stock_number = a.y_stock_number),
                              'Deleted', 'Deleted', x_run_date
                            from scpp.xfm_deals_for_update a
                            where x_stock_number = '%s'
                        """ % _stock_number
                        pg_cur_2.execute(sql)
                    # matrix: 2
                    if _record_status_change == 'None to Capped':
                        sql = """
                            insert into scpp.deals (year_month,employee_number,stock_number,unit_count,
                              deal_Date,customer_name,model_year,make,model,deal_source,seq,
                              notes,deal_status,run_date)
                            select
                              case
                                when gl_year_month <> 0 then gl_year_month
                                else x_year_month_date_capped
                              end, x_employee_number, x_stock_number, 1,
                              case
                                when x_gl_date is not null then x_gl_date
                                else x_date_capped
                              end,
                              x_customer_name, x_model_year, x_make, x_model, 'Dealertrack',
                              (select max(seq) + 1 from scpp.deals where stock_number = a.x_stock_number),
                              'Capped', 'Capped', x_run_date
                            from scpp.xfm_deals_for_update a
                            where x_stock_number = '%s'
                        """ % _stock_number
                        pg_cur_2.execute(sql)
                    # matrix: 3
                    if _record_status_change == 'Accepted to Capped':
                        sql = """
                            insert into scpp.deals (year_month,employee_number,stock_number,unit_count,
                              deal_Date,customer_name,model_year,make,model,deal_source,seq,
                              notes,deal_status,run_date)
                            select
                              case
                                when gl_year_month <> 0 then gl_year_month
                                else x_year_month_date_capped
                              end, x_employee_number, x_stock_number, 1,
                              case
                                when x_gl_date is not null then x_gl_date
                                else x_date_capped
                              end,
                              x_customer_name, x_model_year, x_make, x_model, 'Dealertrack',
                              (select max(seq) + 1 from scpp.deals where stock_number = a.x_stock_number),
                              'Capped', 'Capped', x_run_date
                            from scpp.xfm_deals_for_update a
                            where x_stock_number = '%s'
                        """ % _stock_number
                        pg_cur_2.execute(sql)
                    # matrix: 4
                    if _record_status_change == 'Capped to None' and _deal_status == 'Capped':
                        sql = """
                            insert into scpp.deals (year_month,employee_number,stock_number,unit_count,
                              deal_Date,customer_name,model_year,make,model,deal_source,seq,
                              notes,deal_status,run_date)
                            select x_year_month_run_date, y_employee_number, y_stock_number, -1,
                              /*y_deal_date,*/ x_run_date,
                              y_customer_name, y_model_year, y_make, y_model, 'Dealertrack',
                              (select max(seq) + 1 from scpp.deals where stock_number = a.y_stock_number),
                              'Un-capped', x_deal_status, x_run_date
                            from scpp.xfm_deals_for_update a
                            where x_stock_number = '%s'
                        """ % _stock_number
                        pg_cur_2.execute(sql)
                    # matrix: 5
                    if _record_status_change == 'Capped to Accepted' and _deal_status == 'Capped':
                        sql = """
                            insert into scpp.deals (year_month,employee_number,stock_number,unit_count,
                              deal_Date,customer_name,model_year,make,model,deal_source,seq,
                              notes,deal_status,run_date)
                            select x_year_month_run_date, y_employee_number, y_stock_number, -1,
                              /*y_deal_date,*/ x_run_date,
                              y_customer_name, y_model_year, y_make, y_model, 'Dealertrack',
                              (select max(seq) + 1 from scpp.deals where stock_number = a.y_stock_number),
                              'Un-capped', x_deal_status, x_run_date
                            from scpp.xfm_deals_for_update a
                            where x_stock_number = '%s'
                        """ % _stock_number
                        pg_cur_2.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
