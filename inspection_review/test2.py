# encoding=utf-8
# triple quote the quoted variable:  stocknumber = """'25409XXR'"""
# define the query as a triple quoted string, doing the variable as: ... and b.stocknumber = %(xn)s"""
# modify the query so:  sql = query % dict(xn=stocknumber)
# and bingo,  master_cursor.execute(sql)   , the fucker works

import adsdb

ads_con = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\advantage\\dpsvseries\\dpsvseries.add',
                        userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
                        TrimTrailingSpaces='TRUE')
# stocknumber = """'25409XXR'"""
stocknumber = repr('25409XXR')
master_cursor = ads_con.cursor()
query = """
    SELECT b.stocknumber,
      trim(stocknumber) + ':  Inspected by ' + trim(c.fullname),
      trim(f.yearmodel) + ' ' + trim(f.make) + ' ' + trim(f.model) + ' '
          + coalesce(trim(f.TRIM), '') + ' ' + TRIM(coalesce(f.bodystyle)),
        'Engine: ' + trim(coalesce(f.engine))
    FROM vehicleinspections a
    INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    LEFT JOIN organizations bb on b.owninglocationid = bb.partyid
      AND bb.name = 'Rydells'
    LEFT JOIN people c on a.technicianid = c.partyid
    LEFT JOIN VehicleItems f on a.VehicleItemID = f.VehicleItemID
    LEFT JOIN VehicleWalks g on a.VehicleInventoryItemID = g.VehicleInventoryItemID
    WHERE b.stocknumber = %(xn)s"""
    # WHERE CAST(vehicleinspectionts AS sql_date) = curdate() - 1
print 'query: '
print query
sql = query % dict(xn=stocknumber)
print sql
# print query
master_cursor.execute(sql)
master_result = master_cursor.fetchall()
for t in master_result:
    print t[0] + '  ' + t[1]
    print 't: ' + str(type(t))
    print 't: ' + str(t)
print 'master_result:  ' + str(type(master_result))
print 'master_result: ' + str(master_result)
# print master_result[0][1]  + '\n' + master_result[0][2]  + '\n' + master_result[0][3]
