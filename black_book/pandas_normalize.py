# http://pandas.pydata.org/pandas-docs/stable/generated/pandas.io.json.json_normalize.html
# can't initially get it to work with my data
# data = [{'state': 'Florida',
#          'shortname': 'FL',
#          'info': {
#               'governor': 'Rick Scott'
#          },
#          'counties': [{'name': 'Dade', 'population': 12345},
#                      {'name': 'Broward', 'population': 40000},
#                      {'name': 'Palm Beach', 'population': 60000}]},
#         {'state': 'Ohio',
#          'shortname': 'OH',
#          'info': {
#               'governor': 'John Kasich'
#          },
#          'counties': [{'name': 'Summit', 'population': 1234},
#                       {'name': 'Cuyahoga', 'population': 1337}]}]
data = """
{
    "year_list": [
        {
            "name": "2017",
            "make_list": [
                {
                    "name": "Acura",
                    "model_list": [
                        {
                            "name": "ILX"
                        },
                        {
                            "name": "RDX"
                        }
                    ]
                }
            ]
        }
    ]
}
"""
from pandas.io.json import json_normalize
# result = json_normalize(data, 'counties', ['state', 'shortname',
#                                           ['info', 'governor']])
result = json_normalize(data, 'model_list', ['year_list' 'name',
                                          ['make_list', 'name']])
print(result)

#          name  population shortname info.governor    state
# 0        Dade       12345        FL    Rick Scott  Florida
# 1     Broward       40000        FL    Rick Scott  Florida
# 2  Palm Beach       60000        FL    Rick Scott  Florida
# 3      Summit        1234        OH   John Kasich     Ohio
# 4    Cuyahoga        1337        OH   John Kasich     Ohio