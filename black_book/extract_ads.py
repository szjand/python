# coding=utf-8

import adsdb
import psycopg2
import csv
pgCon = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
pgCursor = pgCon.cursor()
adsCon = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\advantage\\BlackBookVSeries\\BlackBookVSeries.add',
                       userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
                       TrimTrailingSpaces='TRUE')
schema = 'bb'
adsCursor = adsCon.cursor()
# start with a small table 19M
adsCursor.execute("select trim(name) from system.tables where name = 'blackbookpubdates'")
# adsCursor.execute("select trim(name) from system.tables where name = 'ucinv_tmp_avail_disposition'")
# adsCursor.execute("select trim(name) from system.tables where name = 'ucinv_tmp_sales_by_status'")
# adsCursor.execute("select trim(name) from system.tables where name = 'ucinv_tmp_sales_activity'")
# adsCursor.execute("select trim(name) from system.tables where name = 'ucinv_tmp_avail_4'")
# adsCursor.execute("select trim(name) from system.tables where name = 'ucinv_tmp_vehicles'")
numrows = adsCursor.rowcount
out_file = 'files/bb_table_data.csv'
# schema = 'dds'
for x in xrange(0, numrows):
    row = adsCursor.fetchone()
    tableName = row[0]
    tableCursor = adsCon.cursor()
    tableCursor.execute("select * from " + tableName)
    with open(out_file, 'wb') as f:
        csv.writer(f).writerows(tableCursor.fetchall())
    tableCursor.close()
    f.close()
    pgCursor = pgCon.cursor()
    pgCursor.execute("truncate " + schema + "." + tableName)
    pgCon.commit()
    io = open(out_file, 'r')
    pgCursor.copy_expert("""copy """ + schema + """.""" + tableName + """ from stdin with(format csv)""", io)
    pgCon.commit()
    io.close()
adsCursor.close()
adsCon.close()
pgCursor.close()
pgCon.close()
