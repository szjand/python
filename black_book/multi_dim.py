import json

#this one is possible linteresting

input_data = """
{
  "secret": "[Hidden]",
  "minutes": 20,
  "link": "http:\/\/www.1.com",
  "bookmark_collection": {
    "free_link": {
      "name": "#free_link#",
      "bookmarks": [
        {
          "name": "1",
          "link": "http:\/\/www.1.com"
        },
        {
          "name": "2",
          "link": "http:\/\/2.dk"
        },
        {
          "name": "3",
          "link": "http:\/\/www.3.in"
        }
      ]
    },
    "boarding_pass": {
      "name": "Boarding Pass",
      "bookmarks": [
        {
          "name": "1",
          "link": "http:\/\/www.1.com\/"
        },
        {
          "name": "2",
          "link": "http:\/\/www.2.com\/"
        },
        {
          "name": "3",
          "link": "http:\/\/www.3.hk"
        }
      ]
    },
    "sublinks": {
      "name": "sublinks",
      "link": [
        "http:\/\/www.1.com",
        "http:\/\/www.2.com",
        "http:\/\/www.3.com"
      ]
    }
  }
}
"""
# This converts from JSON to a python dict
parsed_input = json.loads(input_data)

# Now, all of your static variables are referenceable as keys:
secret = parsed_input['secret']
minutes = parsed_input['minutes']
link = parsed_input['link']

# Plus, you can get your bookmark collection as:
bookmark_collection = parsed_input['bookmark_collection']

# Print a list of names of the bookmark collections...
print bookmark_collection.keys() # Note this contains sublinks, so remove it if needed

# Get the name of the Boarding Pass bookmark:
print bookmark_collection['boarding_pass']['name']

# Print out a list of all bookmark links as:
#  Boarding Pass
#    * 1: http://www.1.com/
#    * 2: http://www.2.com/
#  ...
for bookmark_definition in bookmark_collection.values():
    # Skip sublinks...
    if bookmark_definition['name'] == 'sublinks':
        continue
    print bookmark_definition['name']
    for bookmark in bookmark_definition['bookmarks']:
        print "    * %(name)s: %(link)s" % bookmark

# Get the sublink definition:
sublinks = parsed_input['bookmark_collection']['sublinks']

# .. and print them
print sublinks['name']
for link in sublinks['link']:
    print '  *', link