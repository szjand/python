import json
from pandas.io.json import json_normalize
import pandas as pd

with open('files/minimum_bb.json') as f:
    data = json.load(f)

result = json_normalize(data)

print(result)