﻿drop Table if exists bb.raw_json;


CREATE TABLE bb.raw_json(
  jsonb_data jsonb,
  json_data json)

select * from bb.raw_json 

select jsonb_data->'year_list' from bb.raw_json

select jsonb_data->'model_list'
from bb.raw_json
where jsonb_data @> '{"name":"2017"}'
  and jsonb_data @> '{"name":"Acura"}'

select jsonb_pretty(jsonb_data) from bb.raw_json  


SELECT
    json_data.key,
    jsonb_typeof(json_data.value),
    count(*)
FROM bb.raw_json x, jsonb_each(x.jsonb_data) AS json_data
group by key, jsonb_typeof
order by key, jsonb_typeof;

SELECT
    json_data.key,
    pg_typeof(json_data.value),
    json_data.value
FROM bb.raw_json x, jsonb_each(x.jsonb_data) AS json_data;

select jsonb_array_elements(jsonb_data->'make_list') from bb.raw_json

SELECT '{"a1":{"b1":"y1","b2":["c1", {"c2":"z1","c3":"z3"}]},"a2":"v2"}'::jsonb #- '{a1}' AS level_1;


select jsonb_each_text(jsonb_data) from bb.raw_json
