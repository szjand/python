﻿drop table all_data;
create temp table all_data as
select 
  case
    when b.dip_store_id = 19 then 'Sioux Falls'
    when b.dip_store_id = 43 then 'Kalispell'
  end as market, 
  a.dealership_id, a.dealership_name, b .dip_store_id, 
  d.the_date, c.lead_source_type, c.measure_type, c.measure_value ,
  e.map, e.par, e.target
from dip.dealerships a 
left join dip.dip_stores b on a.dealership_id = b.dealership_id
left join dip.funnel_measures c on b.dip_store_id = c.dip_store_id
inner join dds.dim_date d on c.date_key = d.date_key
  and d.year_month in (201612, 201611)
left join dip.funnel_mpt e on c.funnel_measures_id = e.funnel_measures_id
where b.dip_store_id in (19, 43)
  and (e.funnel_measures_id is not null or (measure_type = 'Delivered' and lead_source_type = 'Chat'))
  and 
    case
      when c.lead_source_type = 'eLead' then c.measure_type in ('Appointment Percentage','Bad Lead Percentage')
      when c.lead_source_type = 'Phone' then c.measure_type in ('Shown Appointment Percentage','Closing Percentage')
      else 1 = 1
    end
order by b.dip_store_id, d.the_date, lead_source_type, measure_type;

select * from all_data where the_date = '2016-11-01' order by dealership_name, lead_source_type, measure_type

