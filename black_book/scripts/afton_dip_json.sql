﻿select *
from dip.dealerships

select *
from dip.measure_types

select *
from dip.funnel_measures

drop table _wtf;
create temp table _wtf as
select a.dip_store_id, b.dealership_name, d.the_date, c.measure_type, c.measure_value, e.map, e.par, e.target
from dip.dip_Stores a
left join dip.dealerships b on a.dealership_id = b.dealership_id
left join dip.funnel_measures c on a.dip_store_id = c.dip_store_id
inner join dip.lead_source_types cc on c.lead_source_type = cc.lead_source_type
  and cc.lead_source_type = 'phone'
inner join dds.dim_date d on c.date_key = d.date_key
  and d.year_month = 201612
  and d.first_day_of_month = true 
left join dip.funnel_mpt e on c.funnel_measures_id = e.funnel_measures_id  
  and e.status = 'active'
where e.funnel_measures_id is not null  
  and a.dip_store_id in (2,9)
order by a.dip_store_id, c.measure_type  

select * from _wtf

-- nope
select row_to_json(x) 
from (
select j.*, 
  (
    select coalesce(array_to_json(array_agg(row_to_json(z))), '[]')
    from (  
      select measure_type, measure_value, map, par, target
      from _wtf
      where dip_store_id = j.dip_store_id) z) as metrics
from (
  select distinct dip_store_id, '1' as months, dealership_name
  from _wtf) j) x





http://johnatten.com/2015/04/22/use-postgres-json-type-and-aggregate-functions-to-map-relational-data-to-json/
instead of artists and albumns, dealers and metrics

select row_to_json(art) as artists
from(
  select a.id, a.name, 
  (
    select json_agg(alb)
      from (
        select * 
        from albums w
        where artist_id = a.id) alb) as albums
from artists as a) art;

select * from _wtf

instead of artists and albumns, dealers and metrics

artists = dealers
albums = measures

-- returns json objects of id and name
-- each row is a json object
select row_to_json(dealers)
from (
  select distinct dip_store_id, dealership_name, measure_type
  from _wtf) as dealers

-- aggregate measures for a dealer into a json array
select json_agg(measures)
from (
  select measure_value as "value", map, par, target
  from _wtf
  where dip_store_id = 2) as measures

its 3 way  dealer -< measure_type -< measure_values  
  
include the measure type in the dealer query
select row_to_json(x)
from (
  select measure_value as "value", map, par, target
  from _wtf) x

-- its 3 way:  dealer -< measure_type -< measure_values   
-- object - nested object - nested array

-- before i go too far, can i do a nested object
-- actuall thinking now it is an object with a nested array with a nested array
select row_to_json (m_type)
from (
select dip_store_id, measure_type
from _wtf) as m_type

-- this is starting from the outside and, of course, works, because it is the same old simple masterdetail
-- hmmm measure type needs to be a name not a value
-- this is from the outside in 

select row_to_json(b)
from (
  select dealers.dealership_name, 
    (
      select json_agg (m_type)
      from (
        select measure_type
        from _wtf
        where dip_store_id = dealers.dip_store_id) as m_type) as metrics
  from (
      select distinct dip_store_id, dealership_name
      from _wtf) as dealers) b
      
-- try it with just measure_type / values first
select * from _wtf

select row_to_json(a)
from (
  select lev_2.*,
    (
      select json_agg(lev_3)
      from (
        select measure_value as "value", map, par, target
        from _wtf
        where dip_store_id = lev_2.dip_store_id
          and the_date = lev_2.the_date
          and measure_type = lev_2.measure_type) as lev_3) as meas
  from (
    select dip_store_id, measure_type, the_date 
    from _wtf a
    where dip_store_id = 2
      and the_date = '12/01/2016'
      and measure_type = 'Appointment Percentage') as lev_2) a

-- no array, both row_to_json
select to_json(a)
from (
  select lev_2.dealership_name,
    (
      select to_json(lev_3) as "Appointment Percentage"
      from (
        select measure_value as "value", map, par, target
        from _wtf
        where dip_store_id = lev_2.dip_store_id
          and the_date = lev_2.the_date
          and measure_type = lev_2.measure_type) as lev_3) as metrics
  from (
    select dealership_name, dip_store_id, measure_type, the_date 
    from _wtf a
    where dip_store_id = 2
      and the_date = '12/01/2016'
      and measure_type = 'Appointment Percentage') as lev_2) a

select array_to_json(array_agg(b))
from(-- B
  select numbers,array_to_json(array_agg(a)) as alias
  from ( -- A
        select numbers
        from (values (1),(2),(3)) s(numbers)
        )a
group by numbers)b

  
dealers
-------
PK dip_id
dealer_name

dealer_measure_types
------------------
PK measure_type

dealer_measures
---------------
PK FK dip_id
PK FK measure_type
PK the_date
measure_value
map
par
target


select * from dealers

create temp table dealers as
select distinct dip_store_id, dealership_name
from dip.dip_Stores a
left join dip.dealerships b on a.dealership_id = b.dealership_id

create table

select json_agg(x)
from (
  select measure_value as "value", map, par, target
  from _wtf) x  
