﻿drop schema bb cascade;
create schema bb;

create TABLE bb.ValueAdded(
    ValueID bigint,
    VINYear citext,
    GroupNumber citext,
    AddDeduct citext,
    AddDeductCode citext,
    Description citext,
    Value integer,
    BlackBookPubDate date)
 WITH (OIDS=FALSE);

create TABLE bb.BlackBookADT(
    BlackBookADTID bigint,
    Vin citext,
    VINYear citext,
    Year citext,
    Plants citext,
    GroupNumbers citext,
    ModelNumber citext,
    Manufacturer citext,
    Make citext,
    Model citext,
    Series citext,
    BodyStyle citext,
    WholesaleExtraClean integer,
    WholeSaleClean integer,
    WholesaleAverage integer,
    WholesaleRough integer,
    RetailExtraClean integer,
    RetailClean integer,
    RetailAverage integer,
    RetailRough integer,
    MSRP integer,
    LoanValue integer,
    EngineDescription citext,
    MakeNumber citext,
    UVC citext,
    RegionalAdjustment citext,
    BlackBookPubDate date)
 WITH (OIDS=FALSE);

create TABLE bb.GroupMil(
    GroupMilID bigint,
    VINYear citext,
    GroupNumber citext,
    MileageCategory citext,
    BlackBookPubDate date)
 WITH (OIDS=FALSE);

create TABLE bb.MilgClas(
    MilgClasID bigint,
    VINYear citext,
    BeginningMileageRange integer,
    EndingMileageRange integer,
    ExtraCleanAdjustment integer,
    CleanAdjustment integer,
    AverageAdjustment integer,
    RoughAdjustment integer,
    LoanValueAdjustment integer,
    MileageCategoryCode citext,
    BlackBookPubDate date)
 WITH (OIDS=FALSE);

create TABLE bb.PriceIncludes(
    DataID bigint,
    VINYear citext,
    GroupNumber citext,
    PriceIncludesData citext,
    BlackBookPubDate date)
 WITH (OIDS=FALSE);

create TABLE bb.VinYearLookup(
    VinYearLookup bigint,
    VinYear citext,
    Year citext)
 WITH (OIDS=FALSE);

create TABLE bb.BLACKBOOKPUBDATES(
    BlackBookPubDate date,
    FromDate date,
    ThruDate date)
 WITH (OIDS=FALSE);

create TABLE bb.VinYear2039(
    TenthVinChar citext NOT NULL,
    SeventhVinCharIsInteger boolean NOT NULL,
    theYear citext,
    PRIMARY KEY (TenthVinChar,SeventhVinCharIsInteger,theYear)) WITH (OIDS=FALSE);

