# http://stackoverflow.com/questions/18349562/convert-a-nested-json-mixpanel-api-raw-data-export-into-csv-ideally-in-pytho


import sys
import json

def getKeys(newDict):
    retv = []
    for key in newDict.keys():
        try:
            keyForEmbeddedDict = newDict[key].keys()
            retv.extend(getKeys(newDict[key]))
        except AttributeError:
            retv.append(key)
    return retv

def getValues(newDict):
    retv = []
    for key in newDict.keys():
        try:
            keyForEmbeddedDict = newDict[key].keys()
            retv.extend(getValues(newDict[key]))
        except AttributeError:
            retv.append(newDict[key])
    return retv

def main():
    t = {}

    filename = 'files/acura_pp.json' # Add your filename
    with open(filename) as f:
        t = json.load(f)
    keys = getKeys(t)
    result = getValues(t)

    print keys
    print result
    return

if __name__ == '__main__':
    main()
    sys.exit(0)