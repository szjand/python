import pandas as pd
from pandas.io.json import json_normalize, nested_to_record

# pd.read_json("Z:/E/python projects/black_book/file/blackbook_2017.json", dtype=object).dtypes

# json_normalize("Z:/E/python projects/black_book/file/acura.txt")

def flatten_json(y):
    out = {}

    def flatten(x, name=""):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + "_")
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + "_")
                i += 1
        else:
            out[str(name[:-1])] = str(x)

    flatten(y)
    return out

sample_object = {
  "class_code": "*",
  "year_list": [
    {
      "make_list": [
        {
          "model_list": [
            {
              "series_list": [
                {
                  "style_list": [
                    {
                      "uvc": "2017020104",
                      "name": "4D Sdn w/Prem&A-SPEC"
                    },
                    {
                      "uvc": "2017020106",
                      "name": "4D Sdn w/Tech&A-SPEC"
                    },
                    {
                      "uvc": "2017020100",
                      "name": "4D Sedan"
                    },
                    {
                      "uvc": "2017020101",
                      "name": "4D Sedan w/Prem Pkg"
                    },
                    {
                      "uvc": "2017020105",
                      "name": "4D Sedan w/Tech Pkg"
                    }
                  ],
                  "name": "FUCKYOU"
                }
              ],
              "name": "ILX"
            },
            {
              "series_list": [
                {
                  "style_list": [
                    {
                      "uvc": "2017020520",
                      "name": "4D SUV FWD"
                    }
                  ],
                  "name": "Advance"
                },
                {
                  "style_list": [
                    {
                      "uvc": "2017020084",
                      "name": "4D SUV AWD"
                    },
                    {
                      "uvc": "2017020085",
                      "name": "4D SUV AWD w/Tech"
                    },
                    {
                      "uvc": "2017020059",
                      "name": "4D SUV FWD"
                    },
                    {
                      "uvc": "2017020064",
                      "name": "4D SUV FWD w/Tech"
                    }
                  ],
                  "name": "Base"
                }
              ],
              "name": "RDX"
            }
          ],
          "name": "Acura"
        }
      ],
      "name": "2017"
    }
  ],
  "name": "All"
}

# flat = flatten_json(sample_object)
# print(flat)

# print(json_normalize(sample_object))
# print(nested_to_record(sample_object))

pd.read_json(sample_object)

















# # http://kailaspatil.blogspot.com/2013/07/python-script-to-convert-json-file-into.html
# # didn"t worki
# import fileinput
# import json
# import csv
# import sys
#
# l = []
# for line in fileinput.input("files/blackbook_2017.json"):
#     l.append(line)
# myjson = json.loads("".join(l))
# keys = {}
# for i in myjson:
#     for k in i.keys():
#         keys[k] = 1
# mycsv = csv.DictWriter(sys.stdout, fieldnames=keys.keys(),
#                        quoting=csv.QUOTE_MINIMAL)
# mycsv.writeheader()
# for row in myjson:
#     mycsv.writerow(row)