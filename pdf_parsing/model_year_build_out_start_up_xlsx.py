# encoding=utf-8
"""
xlsm from global connect
    order workbench
        reports & tools
           GMFleet.com Order Management Reports
                1st report in the list 4-12-2019 GMNA Weekly Dealer Constraints
very interesting
workbook consisting of multiple worksheets:
    Build Out-US
    SUMMARY04122019
    Buick
    Cadillac
    Chevrolet
    GMC
start out with the Build Out
see if we can get a clean scrape from the xlsm

1st shot
convert xlsm to csv:
    https://stackoverflow.com/questions/23554808/how-to-extract-sheet-from-xlsm-and-save-it-as-csv-in-python
and it works, nice looking csv
it also will loop through all the sheets and creates an separate csv file for each sheet, but, for now
    we are not using that additional data
6/11/19
    base this on the work done in natl_constraints_report_xlsm.py
    up until now, the global connect build out/start up report has been a pdf, now it is an xlsx


"""

import os
import csv
import xlrd

xlsx_path = 'xlsx/'
xlsx_file = os.listdir(xlsx_path)[0]
csv_path = 'csv_from_xlsx/'
print(xlsx_file)

workbook = xlrd.open_workbook(xlsx_path + xlsx_file)
for sheet in workbook.sheets():
    print(sheet.name)
    if sheet.name.strip() == '2019MY Build-Out Matrix':
        with open(csv_path + '{}.csv'.format(sheet.name), 'w') as f:
            writer = csv.writer(f)
            for row in range(sheet.nrows):
                out = []
                for cell in sheet.row_values(row):
                    out.append(cell)
                writer.writerow(out)
    if sheet.name.strip() == '2020MY Start-Up Matrix':
        with open(csv_path + '{}.csv'.format(sheet.name), 'w') as f:
            writer = csv.writer(f)
            for row in range(sheet.nrows):
                out = []
                for cell in sheet.row_values(row):
                    out.append(cell)
                writer.writerow(out)
