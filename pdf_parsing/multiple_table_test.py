# encoding=utf-8
"""
serious problem with parse
4 (At least today) tables/files created from the pdf
os.scandir does not necessarily read the files in numerical order
so get a mismatch between the table which is indicated by the i loop which is in numeric order
could just reverse it, start i at 3 instead of 0, but i have no way to insure
that the files are consistently processed in any given order
this is an attempt to remedy that situation
code based on model_year_build_out_start_up.py

"""
import camelot
import db_cnx
import os
import csv
# location for csv files generated from pdfs
csv_path = 'new_csv_path/'
pdf_path = 'pdfs/'
changes = {'\n': ' '}  # a dictionary of changes to make, find 'key' substitue with 'value'
# copy_text['v'] fills in the division value for all rows
tables = camelot.read_pdf('pdfs/model_year_build_out_start_up.pdf', pages='1-end', copy_text=['v'])
# print(tables.n)  # this gives me the number of tables/pages, in this case 4
i = tables.n
# while i > 0:
while i == 4:
    for filename in os.listdir(csv_path):
        os.unlink(csv_path + filename)
    # hmm the strip_text get rids of the newlines, this is good, don't have to rewrite the file without newlines
    # flavor='stream' gets rid of the bogus extra column, but won't work with copy_text
    # tables = camelot.read_pdf('pdfs/2019 B-O  2020 S-U - V3.1 US.pdf', pages=str(i), flavor='stream', strip_text='\n')
    # besides divisions, flavor = 'stream' screws up other rows as well, this should have benn one row:
    #       "", "", "", "1/2/19", "", "", "10/9/18"
    #       "", "LCF6500 XD Diesel", "LCFDSL", "", "", "", ""
    # these 2 lines 38 & 39 give me a clean csv that with 2 problems
    # 1. Buick in the Chevrolet Truck/Crossover division: handled in nc.xfm_model_year_build_out_start_up()
    # 2. a superfluous extra empty column

    # tables = camelot.read_pdf('pdfs/2019 B-O  2020 S-U - V3.1 US.pdf', pages=str(i), copy_text=['v'], strip_text='\n')
    # tables.export('new_csv_path/build_out.csv', f='csv')

    tables = camelot.read_pdf('pdfs/2019 B-O  2020 S-U - V3.1 US.pdf', pages=str(i), copy_text=['v'], strip_text='\n')
    cols = [0, 1, 2, 3, 4, 5, 6]
    # tables[0].to_csv('new_csv_path/build_out.csv', mode='w', columns=cols, index=False)
    tables[0].to_csv('new_csv_path/build_out.csv', columns=cols)
    # tables[0].to_csv('new_csv_path/build_out.csv')  # to_json, to_excel, to_html
    # print(tables[0].df)
    # tables[0].shape[1]  # gives the number of columns
    # print(tables[0].df)
    # cols = [0, 1, 2, 3, 4, 5, 6]
    # new_df = tables[0].df
    # print(new_df.columns[cols])
    # print(tables[0].df.iloc[7])

    # with os.scandir(csv_path) as files:
    #     # i = 0
    #     for file in files:
    #         new_rows = []  # a temp holder for modified rows
    #         with open(file, 'r') as f:
    #             reader = csv.reader(f)
    #             for row in reader:
    #                 new_row = row
    #                 for key, value in changes.items():
    #                     new_row = [x.replace(key, value) for x in new_row]
    #                 new_rows.append(new_row)
    #         with open(file, 'w') as f:
    #             # overwrite file with new_rows, where line feed has been removed from headers
    #             writer = csv.writer(f)
    #             writer.writerows(new_rows)
    #         if len(tables[0].cols) == 11:  # there will always be just 1 table, ie, table[0]
    #             with db_cnx.pg() as pg_con:
    #                 with pg_con.cursor() as pg_cur:
    #                     with open(file, 'r') as io:
    #                         pg_cur.copy_expert("""copy nc.ext_build_out_2019 from stdin
    #                                                 with csv encoding 'latin-1 '""", io)
    #         if len(tables[0].cols) == 7:
    #             with db_cnx.pg() as pg_con:
    #                 with pg_con.cursor() as pg_cur:
    #                     with open(file, 'r') as io:
    #                         pg_cur.copy_expert("""copy nc.ext_start_up_2020 from stdin
    #                                                 with csv encoding 'latin-1 '""", io)
    i = i - 1
# with db_cnx.pg() as pg_con:
#     with pg_con.cursor() as pg_cur:
#         pg_cur.execute('select nc.xfm_model_year_build_out_start_up()')
