# encoding=utf-8
"""
pdf from global connect
    order workbench
        reports & tools
            National Constraints Report: generates a pdf names showconstraintsreport.do
                consists of 2 sections:
                    1. 2019 MY Anticipated Build-Out Constraints
                        build out dates for specific options
                    2. 2019/2020 Constraints
it became necessary to do this in 2 groups, if there are more than 9 pages, scandir returns the files
like: 1, 10, 11, 12, 2, 3 ,4
which conflicts with i which is simply in numeric order
and since the table structures at the begining and the end of the pdf are different everything broke

populates 2 tables:
    nc.xfm_build_out_constraints: option level  build outs
    nc.xfm_national_constraints: probably not used in deference to data from dealer constraints distribution report
"""
import camelot
import db_cnx
import os
import csv
# location for csv files generated from pdfs
csv_path = 'csv_from_pdf/'
pdf_path = 'pdfs/'
# (a dictionary of changes to make, find 'key' substitue with 'value', removing line feeds from csv files
changes = {'\n': ''}
# start with an empty direcory
for filename in os.listdir(csv_path):
    os.unlink(csv_path + filename)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute('truncate nc.ext_build_out_constraints')
        pg_cur.execute('truncate nc.ext_national_constraints')
# copy_text['v'] fills in the division value for all rows
tables = camelot.read_pdf('pdfs/national_constraints_report.pdf', pages='1-9', copy_text=['v'])
tables.export('csv_from_pdf/national_constraints_report.csv', f='csv')
with os.scandir(csv_path) as files:
    i = 0
    for file in files:
        new_rows = []  # a temp holder for modified rows
        # replace line feeds in the files
        with open(file, 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                new_row = row
                for key, value in changes.items():
                    new_row = [x.replace(key, value) for x in new_row]
                new_rows.append(new_row)
        with open(file, 'w') as f:
            # overwrite file with new_rows, where line feed has been removed
            writer = csv.writer(f)
            writer.writerows(new_rows)
        if len(tables[i].cols) == 9:
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_national_constraints from stdin
                                                with csv encoding 'latin-1 '""", io)
        if len(tables[i].cols) == 5:
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_build_out_constraints
                                                from stdin with csv encoding 'latin-1 '""", io)
        if len(tables[i].cols) == 4:
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_build_out_constraints(model,description,last_dosp,
                                                last_production_week) from stdin with csv encoding 'latin-1 '""", io)
        i += 1
# now do pages 10 - end
# start with an empty direcory
for filename in os.listdir(csv_path):
    os.unlink(csv_path + filename)
tables = camelot.read_pdf('pdfs/national_constraints_report.pdf', pages='10-end', copy_text=['v'])
tables.export('csv_from_pdf/national_constraints_report.csv', f='csv')
with os.scandir(csv_path) as files:
    i = 0
    for file in files:
        new_rows = []  # a temp holder for modified rows
        # replace line feeds in the files
        with open(file, 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                new_row = row
                for key, value in changes.items():
                    new_row = [x.replace(key, value) for x in new_row]
                new_rows.append(new_row)
        with open(file, 'w') as f:
            # overwrite file with new_rows, where line feed has been removed
            writer = csv.writer(f)
            writer.writerows(new_rows)
        if len(tables[i].cols) == 9:
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_national_constraints from stdin
                                                with csv encoding 'latin-1 '""", io)
        if len(tables[i].cols) == 5:
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_build_out_constraints
                                                from stdin with csv encoding 'latin-1 '""", io)
        if len(tables[i].cols) == 4:
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_build_out_constraints(model,description,last_dosp,
                                                last_production_week) from stdin with csv encoding 'latin-1 '""", io)
        i += 1
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute('select nc.xfm_national_constraints()')
