import camelot
import sqlite3
# tables = camelot.read_pdf('test_2.pdf', pages='1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18')
# tables = camelot.read_pdf('test_2.pdf', pages='1,2,3,4') # generates 4 pages
# print(tables)  # with 3 pages, this show: <TableList n=3>
# print(tables[0].df)
# tables.export('test_3', f='sqlite')
# tables.export('test_2.xls', f='excel') # missing xlwt
# all pages without knowing how many there are
# tables = camelot.read_pdf('test_2.pdf', pages='1', copy_text=['v','h'])
# tables.export('span_text.json', f='json')
# financial statement
tables = camelot.read_pdf('test_1.pdf', pages='1')
tables.export('fs_page_1.csv', f='csv')
