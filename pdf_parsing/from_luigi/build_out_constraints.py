# encoding=utf-8
"""
going with the build out constraints only, not the model year constraints
instead of nulls, this generates empty strings, going with that
4/28/19
    this is currently using the pdf download from

    BUT
    today, realized this report is available from the Fleet page
    Order Workbench
        -> Reports & Tools
            -> GMFleet.com Order Management Reports
                -> 4-12-2019 GMNA Weekly Dealer Constraints
    as a fucking xlsm file
    looks neat and tidy compared to all the fucking tweaking i have to do with the pdf
Alas, that source does not appear to be current
"""
import utilities
import luigi
import datetime
from selenium import webdriver
import time
import os
import camelot

pipeline = 'nc_inventory'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
username = 'cartiva01'
password = ''
pdf_dir = 'buildout_constraints_pdf/'
csv_dir = 'buildout_constraints_csv/'
csv_file = 'buildout_constraints.csv'


class BuildoutConstraintsDownload(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def setup_driver():
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"plugins.plugins_list": [{"enabled": False, "name": "Chrome PDF Viewer"}],
                   # Disable Chrome's PDF Viewer
                   "download.default_directory": pdf_dir, "download.extensions_to_open": "applications/pdf"}
        options.add_experimental_option("prefs", profile)
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(driver):
        # get username and password fields
        boxes = driver.find_elements_by_class_name('form-textbox')
        username_box = boxes[0]
        password_box = boxes[1]
        login_btn = driver.find_element_by_class_name('loginMobile')
        username_box.send_keys(username)
        password_box.send_keys(password)
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")
        # Give it 5 seconds to download
        time.sleep(5)

    def run(self):

        for filename in os.listdir(pdf_dir):
            os.unlink(pdf_dir + filename)

        driver = self.setup_driver()

        # URL goes straight to National Constraints Report
        driver.get('https://www.autopartners.net/apps/naowb/naowb/reportstools/showconstraintsreport.do?'
                   'action=showConstraintsPDFReport')

        self.login(driver)

        driver.quit()


class BuildoutConstraintsParse(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield BuildoutConstraintsDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # empty the csv_dir
        for filename in os.listdir(csv_dir):
            os.unlink(csv_dir + filename)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate nc.xfm_build_out_constraints_1')
        pdf_file = os.listdir(pdf_dir)[0]  # only ever 1 file in directory
        # copy_text['v'] fills in the division value for all rows
        table_count = camelot.read_pdf(pdf_dir + pdf_file, pages='1-end')
        i = table_count.n  # the number of tables in the pdf
        while i > 0:
            for filename in os.listdir(csv_dir):  # only ever to be one file in directory
                os.unlink(csv_dir + filename)
            tables = camelot.read_pdf(pdf_dir + pdf_file, pages=str(i), copy_text=['v'], strip_text='\n')
            tables[0].to_csv(csv_dir + csv_file)
            file = csv_dir + csv_file
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute('truncate nc.ext_build_out_constraints')
                    with open(file, 'r') as io:
                        if len(tables[0].cols) == 5:
                            pg_cur.copy_expert("""copy nc.ext_build_out_constraints from stdin
                                                with csv encoding 'latin-1 '""", io)
                        if len(tables[0].cols) == 4:
                            pg_cur.copy_expert("""copy nc.ext_build_out_constraints(model,description,last_dosp,
                                                    last_production_week) from stdin with csv encoding 'latin-1 '""",
                                               io)
                    sql = "select nc.xfm_build_out_constraints_1(" + str(i) + ")"
                    pg_cur.execute(sql)
            i = i - 1
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select nc.xfm_build_out_constraints()')
