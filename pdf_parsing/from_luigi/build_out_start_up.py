# encoding=utf-8
"""
PDF from:
    order workbench
        reports & tools
            GMFleet.com Order Management Reports
                2nd report in list: 2019 B-O  2020 S-U - V3.1 US.pdf


problems with parse, these are all on V3.1
1.
    4 (At least today) tables/files created from the pdf
    os.scandir does not necessarily read the files in numerical order
    so get a mismatch between the table which is indicated by the i loop which is in numeric order
    could just reverse it, start i at 3 instead of 0, but i have no way to insure
    that the files are consistently processed in any given order
    solution: 1st read the pdf to generate the number of tables, use that as the loop control, process one table
      completely, one at a time
2.
    division column has invisible text of Buick in Chevrolet Truck/Crossovers division, visible to the parser
    handle in the stored proc
3.
    somehow, table 4 (second table of start up) is generating 8 columns, which breaks the insert
    the 8th column is empty in each row, so, in the generation of the csv file, specify the columns
4.
    leaving empty fields as empty strings, csv/code generates this rather than nulls, fooled around with
    converting to nulls, nah, not worth it

 pdf from global connect
    order workbench
        reports & tools
            GMFleet.com Order Management Reports
                Order Management Reports
                    2nd report in list
 filename: 2019 B-O  2020 S-U - V3.1 US.pdf
"""


import utilities
import luigi
import datetime
# import csv
from selenium import webdriver
import time
import os
import camelot

pipeline = 'nc_inventory'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
username = 'cartiva01'
password = 'cd ~'
pdf_dir = 'buildout_pdf/'  # needs to be an absolute path
csv_dir = 'buildout_csv/'
csv_file = 'build_out.csv'


class BuildOutDownload(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def setup_driver():
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"plugins.plugins_list": [{"enabled": False, "name": "Chrome PDF Viewer"}],
                   # Disable Chrome's PDF Viewer
                   "download.default_directory": pdf_dir, "download.extensions_to_open": "applications/pdf"}
        options.add_experimental_option("prefs", profile)
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(driver):
        # get username and password fields
        boxes = driver.find_elements_by_class_name('form-textbox')
        username_box = boxes[0]
        password_box = boxes[1]
        login_btn = driver.find_element_by_class_name('loginMobile')
        username_box.send_keys(username)
        password_box.send_keys(password)
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    @staticmethod
    def download_pdf(driver):
        # Since we have successfully logged in go to fleet link
        driver.get('https://dealer.autopartners.net/sites/usfleet/Order%20Management%20Reports/Forms/AllItems.aspx')
        # Get all a tags with class name
        links = driver.find_elements_by_class_name('ms-listlink')
        for link in links:
            download_url = link.get_attribute('href')
            # Get the tag that has B-O and S-U
            if 'B-O' in download_url and 'S-U' in download_url:
                link.click()
                # Give it 5 seconds to download
                time.sleep(5)
                break

    def run(self):

        for filename in os.listdir(pdf_dir):
            os.unlink(pdf_dir + filename)

        driver = self.setup_driver()

        driver.get('https://dealer.autopartners.net/portal/US/Order/Pages/default.aspx?appdeptid=4')

        self.login(driver)

        self.download_pdf(driver)

        driver.quit()


class BuildOutParse(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield BuildOutDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # empty the csv_dir
        for filename in os.listdir(csv_dir):
            os.unlink(csv_dir + filename)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate nc.ext_build_out_2019')
                pg_cur.execute('truncate nc.ext_start_up_2020')
        # get the name of the pdf, it changes version in the filename over time
        pdf_file = os.listdir(pdf_dir)[0]  # only ever 1 file in directory
        # copy_text['v'] fills in the division value for all rows
        table_count = camelot.read_pdf(pdf_dir + pdf_file, pages='1-end')
        i = table_count.n  # the number of tables in the pdf
        while i > 0:
            for filename in os.listdir(csv_dir):  # only ever to be one file in directory
                os.unlink(csv_dir + filename)
            tables = camelot.read_pdf(pdf_dir + pdf_file, pages=str(i), copy_text=['v'], strip_text='\n')
            tables[0].to_csv(csv_dir + csv_file)
            file = csv_dir + csv_file
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        if len(tables[0].cols) == 11:
                            pg_cur.copy_expert("""copy nc.ext_build_out_2019 from stdin
                                                    with csv encoding 'latin-1 '""", io)
                        if len(tables[0].cols) == 7:
                            pg_cur.copy_expert("""copy nc.ext_start_up_2020 from stdin
                                                    with csv encoding 'latin-1 '""", io)
                    if len(tables[0].cols) == 8:
                        cols = [0, 1, 2, 3, 4, 5, 6]
                        tables[0].to_csv(csv_dir + csv_file, columns=cols)
                        file = csv_dir + os.listdir(csv_dir)[0]
                        with open(file, 'r') as io:
                            pg_cur.copy_expert("""copy nc.ext_start_up_2020 from stdin
                                                    with csv encoding 'latin-1 '""", io)
            i = i - 1
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select nc.xfm_model_year_build_out_start_up()')
