# encoding=utf-8
"""
get started with constraints
want to see if in luigi i can do afton's python style
start with selenium/dealer_constraint_distribution_report
had to install selenium in this venv
though the file being downloaded has an .xls extention, it is actually an html file
"""


import utilities
import luigi
import datetime
import csv
from selenium import webdriver
import time
import os
from bs4 import BeautifulSoup

pipeline = 'nc_inventory'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
username = 'cartiva01'
password = ''
download_dir = 'dealer_constraints_xls/'  # needs to be an absolute path


class DealerConstraintsDownload(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def setup_driver():
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": download_dir}
        options.add_experimental_option("prefs", profile)
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(driver):
        # get username and password fields
        boxes = driver.find_elements_by_class_name('form-textbox')
        username_box = boxes[0]
        password_box = boxes[1]
        login_btn = driver.find_element_by_class_name('loginMobile')
        username_box.send_keys(username)
        password_box.send_keys(password)
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    @staticmethod
    def download_excel(driver):
        division_option_elem = driver.find_element_by_id('ConsOrg')
        division_options = division_option_elem.find_elements_by_tag_name('option')
        # Get the first option after "Select Division"
        all_divisions = division_options[1]
        all_divisions.click()
        date_options_elem = driver.find_element_by_id('RptDate')
        date_options = date_options_elem.find_elements_by_tag_name('option')
        # Get the first option after "Select Report Date"
        most_recent_date = date_options[1]
        most_recent_date.click()
        # Click Excel Button
        export_to_excel = driver.find_element_by_id('btnSGoExp')
        export_to_excel.click()
        # Wait for sheet to download
        time.sleep(5)

    def run(self):
        for filename in os.listdir(download_dir):
            os.unlink(download_dir + filename)

        driver = self.setup_driver()

        driver.get('https://www.autopartners.net/apps/naowb/naowb/reportstools/rt_02.do?modName=rt&ForceReport=Cstrnt'
                   'Dstrbn&USER_ACTION=rt_02_body&METHOD=doJspInit')
        self.login(driver)
        self.download_excel(driver)

        driver.quit()


class DealerConstraintsParse(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield DealerConstraintsDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # file = download_dir + 'VOM_PdfAction.xls'
        file = download_dir + os.listdir(download_dir)[0]
        soup = BeautifulSoup(open(file), 'lxml')
        rows = []
        for row in soup.find_all('tr'):
            rows.append([val.text.strip() for val in row.find_all('td')])
            # this eliminates the rows with no info
        with open(file, 'w') as f:
            writer = csv.writer(f)
            writer.writerows(row for row in rows if len(row) > 1)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate nc.ext_dealer_constraints_1;")
                with open(file, 'r') as io:
                    pg_cur.copy_expert("""copy nc.ext_dealer_constraints_1 from stdin encoding 'latin-1 '""", io)
                sql = "select nc.update_dealer_constraints()"
                pg_cur.execute(sql)
