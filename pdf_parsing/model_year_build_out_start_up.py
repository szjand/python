# encoding=utf-8
"""
    don't know where this report comes from, email from afton named it: 2019 b-o & 2020 s-u - v1 - 12419 (1).pdf
    4/8/19 found it, V2 dated 3/8/19
    Order Workbench -> Reports & Tools -> View Reports: GMFleet.com Order Management Reports ->
        2019 B-O  2020 S-U - V2 3-8-19 US.PDF
    Thinking this is a single version, ie, overwrite daily, only data of interest is the most
    recent version

    populates 2 tables: nc.xfm_build_out_2019, nc.xfm_start_up_2020
        no history required, simply the most recent data

"""
import camelot
import db_cnx
import os
import csv
# location for csv files generated from pdfs
csv_path = 'csv_from_pdf/'
pdf_path = 'pdfs/'
changes = {'\n': ' '}  # a dictionary of changes to make, find 'key' substitue with 'value'
# start with an empty direcory
for filename in os.listdir(csv_path):
    os.unlink(csv_path + filename)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute('truncate nc.ext_build_out_2019')
        pg_cur.execute('truncate nc.ext_start_up_2020')
# copy_text['v'] fills in the division value for all rows
tables = camelot.read_pdf('pdfs/model_year_build_out_start_up.pdf', pages='1-end', copy_text=['v'])
tables.export('csv_from_pdf/build_out.csv', f='csv')
with os.scandir(csv_path) as files:
    i = 0
    for file in files:
        new_rows = []  # a temp holder for modified rows
        with open(file, 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                new_row = row
                for key, value in changes.items():
                    new_row = [x.replace(key, value) for x in new_row]
                new_rows.append(new_row)
        with open(file, 'w') as f:
            # overwrite file with new_rows, where line feed has been removed from headers
            writer = csv.writer(f)
            writer.writerows(new_rows)
        if len(tables[i].cols) == 11:
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_build_out_2019 from stdin
                                                with csv encoding 'latin-1 '""", io)
        if len(tables[i].cols) == 7:
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_start_up_2020 from stdin
                                                with csv encoding 'latin-1 '""", io)
        i += 1
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute('select nc.xfm_model_year_build_out_start_up()')
