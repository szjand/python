# encoding=utf-8
"""
an attempt to extract the "values" table from ctp program pdfs
Hi Jon,

Below is a list of the CTP Programs referred to in the CTP Spec that Mike put together and where to find them. We’re wondering if it is possible to scrape the cash amounts found in the tables (typically on the second or third pages of the programs) because they are PDF documents.  Would you mind taking a peak at one of the programs to determine if that is a possibility for deriving that data for the page?

Applicable to vehicles put into the fleet May 4th, 2021 or after:
-	GM Courtesy Transportation In-Service Program 21-40XL-001
-	GM Courtesy Transportation Out-of-Service Program 21-40-XM-001
-	Cadillac CTA Short Term In Service Program 21-40XA-003
-	Cadillac CTA Short Term Out of Service Program 21-40-XB-003


Applicable to vehicles put into the fleet prior to May 4th, 2021:
-	EXPIRED GM Courtesy Transportation Program 21-40X-004
-	GM Courtesy Transportation Short Term Emergency Program 21-40XX-004
-	Cadillac CTA Short Term In Service Program 21-40XA-003
-	Cadillac CTA Short Term Out of Service Program 21-40-XB-003


How to find these: Global Connect > Incentive Management App > Search Programs > Select “Do a detailed search” >
    -	Either:
        o	Change the date range from 5/1/2021 to today > enter “ctp” in the “key word” box > Click “submit”
            button in bottom right corner
        OR
        o	Enter each program number in the “Program Number” box to search each one individually
    -	For the expired program,  search the program number and change the status from “active” to “expired”


Feel free to give me a call with any questions! Thank you!

Taylor Monson

06/05/221
    updated camelot to 0.8.2
    installed pandas

20-40AAA: 21 Sierra 1500  p2
20-40AY: 21 Silverado 1500  p2
20-40AZ: 21 Camaro  p2
21-40X-5: 22, 21, 20 All p3-6
21-40XA-3: 21 Cad p3
21-40XB-3: 21 Cad p3
21-40XC-4: 21 Cad p2
21-40XD-4: 21 Cad p2
21-40XL-1: 21-40X-5: 22, 21, 20 All p3-5
21-40XM-1: 22, 21, 20 All p3-5
21-40XX-4: 21, 20, All p2-4
"""
import camelot
import db_cnx
import csv
import os
import pandas as pd


pdf_path = 'ctp/pdf/'
csv_path = 'ctp/csv/'
pdf_file = '20-40AAA-0.pdf'
csv_file = '20-40AAA-0.csv'

# # 12 tables
# tables = camelot.read_pdf(pdf_path + file_name, pages='1-end', copy_text=['v'])
# print(tables.n)

# # looks like values are in the first 3 tables
# tables = camelot.read_pdf(pdf_path + pdf_file, pages='2', line_scale=50)
# tables.export(csv_path + csv_file, f='csv')
# with os.scandir(csv_path) as files:
#     i = 0
#     for file in files:
#         print(tables[i])
#         print(len(tables[i].cols))
#         i += 1

tables = camelot.read_pdf(pdf_path + pdf_file, pages='2', line_scale=50)
print(tables[0].df)