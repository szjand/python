# encoding=utf-8
"""
    ha, it works, 14 column csv
    create csv from xls:
    https://stackoverflow.com/questions/10802417/how-to-save-an-excel-worksheet-as-csv
    openpyxl fails on xls
    try xlrd

    the problem is that it is not an xls, but an html readable by excel
"""

import db_cnx
# import xlrd
#
# from openpyxl.workbook import Workbook as openpyxlWorkbook
# import pyexcel as p
# import pandas as pd
# import codecs
from bs4 import BeautifulSoup
import csv


# file_name = 'csv_from_pdf/constraint.csv'
file_name = 'csv_from_pdf/finally.csv'
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy nc.ext_dealer_constraints_1 from stdin encoding 'latin-1 '""", io)

# # file_name = 'csv_from_pdf/constraint.csv'
# file_name = 'csv_from_pdf/finally.csv'
# with db_cnx.pg() as pg_con:
#     with pg_con.cursor() as pg_cur:
#         with open(file_name, 'r') as io:
#             pg_cur.copy_expert("""copy nc.ext_dealer_constraints_1 from stdin
#                                     with csv encoding 'latin-1 '""", io)

# https://stackoverflow.com/questions/41566882/python-beautifulsoup4-parsing-multiple-tables
# this is the most promising yet
# now lets get it to write a csv: https://stackoverflow.com/questions/14167352/beautifulsoup-html-csv
# this looks real close
file = open('pdfs/VOM_PdfAction (8).xls')
soup = BeautifulSoup(file, 'lxml')
rows = []
for row in soup.find_all('tr'):
    # rows.append([val.text.encode('utf8').strip() for val in row.find_all('td')])
    rows.append([val.text.strip() for val in row.find_all('td')])
    # i am guessing that ^ is a list comprehension,
    # try to break it down, didn't work
    # for val in row.find_all('td'):
    #     rows.append(val.text.strip())

# for row in rows:
#     print(len(row))

# # original working
# with open('csv_from_pdf/finally.csv', 'w') as f:
#      writer = csv.writer(f)
#      writer.writerows(row for row in rows if row)

# this eliminates the rows with no info
with open('csv_from_pdf/finally.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerows(row for row in rows if len(row) > 1)

# # this didn't work for shit
# with open('csv_from_pdf/finally.csv', 'w') as f:
#     for row in rows:
#         if len(row) > 2:
#             writer = csv.writer(f)
#             writer.writerows(row)


# # same error: xlrd.biffh.XLRDError: Unsupported format, or corrupt file: Expected BOF record; found b'<html>\n\t'
# pd.read_excel('pdfs/VOM_PdfAction (8).xls').to_csv('csv_from_pdf/panda_test_2.csv', index=False)


# # https://stackoverflow.com/questions/41566882/python-beautifulsoup4-parsing-multiple-tables
# # this is the most promising yet
# # file = open('csv_from_pdf/soup_portion.html')
# file = open('pdfs/VOM_PdfAction (8).xls')
# soup = BeautifulSoup(file, 'lxml')
# rows = []
# for tr in soup.find_all('tr'):
#     td = [td for td in tr.stripped_strings]
#     print(td)

# # https://stackoverflow.com/questions/41566882/python-beautifulsoup4-parsing-multiple-tables
# # this is the most promising yet
# # file = open('csv_from_pdf/soup_portion.html')
# file = open('pdfs/VOM_PdfAction (8).xls')
# soup = BeautifulSoup(file, 'lxml')
# rows = []
# for tr in soup.find_all('tr'):
#     td = [td for td in tr.stripped_strings]
#     with open(fin)



# # print soup to file
# f = codecs.open('pdfs/VOM_PdfAction (8).xls', 'r')
# soup = BeautifulSoup(f.read(), 'lxml')
# with open('csv_from_pdf/soup.html','w') as file:
#     file.write(soup.prettify())

# # print soup to file
# f = codecs.open('pdfs/VOM_PdfAction (8).xls', 'r')
# soup = BeautifulSoup(f.read(), 'lxml')
# print(soup.get_text())


# # https://kite.com/python/examples/4420/beautifulsoup-parse-an-html-table-and-write-to-a-csv
# # after minor modifications, doesn't fail, but doesn't put any data in the file
# html = open('pdfs/VOM_PdfAction (8).xls')
# soup = BeautifulSoup(html.read(), 'lxml')
# table = soup.find("table")
#
# output_rows = []
# for table_row in table.findAll('tr'):
#     columns = table_row.findAll('td')
#     output_row = []
#     for column in columns:
#         output_row.append(column.text)
#     output_rows.append(output_row)
#     print(output_row)
# with open('csv_from_pdf/output.csv', 'w') as csvfile:
#     writer = csv.writer(csvfile)
#     writer.writerows(output_rows)

# # output looks ok
# from bs4.diagnose import diagnose
# with open('pdfs/VOM_PdfAction (8).xls') as fp:
#     data = fp.read()
# diagnose(data)

# # creates an empty file
# f = codecs.open('pdfs/VOM_PdfAction (9).txt', 'r')
# soup = BeautifulSoup(f.read(), 'lxml')
# table = soup.select_one("table")
# headers = [span.text.encode("utf-8").strip() for span in table.select("tr span")]
# with open('csv_from_pdf/bs_test.csv', 'w') as f:
#     wr = csv.writer(f)
#     print(headers)
#     wr.writerow(headers)
#     wr.writerows([[div.text.encode("utf-8").strip() for div in row.find_all("td")] for
#                   row in table.select("tr + tr")])


# # creates an empty file
# # tried modifying aftons sample but it didn't worrk
# f = codecs.open('pdfs/VOM_PdfAction (8).xls', 'r')
# soup = BeautifulSoup(f.read(), 'lxml')
# # table = soup.select_one("table")
# for tables in soup.select('table'):
#     headers = [span.text.encode("utf-8").strip() for span in tables.select("tr span")]
#     with open('csv_from_pdf/bs_test.csv', 'w') as f:
#         wr = csv.writer(f)
#         print(headers)
#         wr.writerow(headers)
#         wr.writerows([[div.text.encode("utf-8").strip() for div in row.find_all("td")] for
#                       row in tables.select("tr + tr")])

# # https://github.com/python-excel/xlrd/issues/188
# # fails with lxml.etree.XMLSyntaxError: Entity 'nbsp' not defined, line 9, column 63
# from lxml import etree, objectify
#
# metadata = 'pdfs/VOM_PdfAction (8).xls'
# parser = etree.XMLParser(remove_blank_text=True)
# tree = etree.parse(metadata, parser)
# root = tree.getroot()
#
# tree.write('pdfs/test.xls',
# pretty_print=True, xml_declaration=True, encoding='UTF-8')

# # required lxml
# # https://stackoverflow.com/questions/46143726/pandas-data-frame-saving-into-csv-file
# https://stackoverflow.com/questions/9623029/python-xlrd-unsupported-format-or-corrupt-file
# data = pd.read_html('pdfs/VOM_PdfAction (8).xls')
# print(data)
# df = pd.DataFrame(data)
# df.to_csv('csv_from_pdf/panda_test.csv')

# # https://stackoverflow.com/questions/9918646/how-to-convert-xls-to-xlsx
# #same stupid error xlrd.biffh.XLRDError: Unsupported format, or corrupt file: Expected BOF record; found b'<html>\n\t'
# p.save_book_as(file_name='pdfs/VOM_PdfAction (8).xls',
#                dest_file_name='csv_from_pdf/dealer_constraints.xlsx')




# wb = openpyxl.load_workbook('VOM_PdfAction(8).xls')
# sh = wb.get_active_sheet()
# with open('test.csv', 'w', newline="")  as f:  # open('test.csv', 'w', newline="") for python 3
#     c = csv.writer(f)
#     for r in sh.rows:
#         c.writerow([cell.value for cell in r])

# # fails because the fucking file is html
# with xlrd.open_workbook('pdfs/VOM_PdfAction (8).xls') as wb:
#     sh = wb.sheet_by_index(0)  # or wb.sheet_by_name('name_of_the_sheet_here')
#     with open('csv_from_pdf/a_file.csv', 'w', newline="") as f:   # open('a_file.csv', 'w', newline="") for python 3
#         c = csv.writer(f)
#         for r in range(sh.nrows):
#             c.writerow(sh.row_values(r))

# # content is a string containing the file. For example the result of an http.request(url).
# # You can also use a filepath by calling "xlrd.open_workbook(filepath)".
#
# # xlsBook = xlrd.open_workbook(file_contents=content)
#
# filepath = 'pdfs/VOM_PdfAction (8).xls'
# xlsBook = xlrd.open_workbook(filepath)
# workbook = openpyxlWorkbook()
#
# for i in range(0, xlsBook.nsheets):
#     xlsSheet = xlsBook.sheet_by_index(i)
#     sheet = workbook.active if i == 0 else workbook.create_sheet()
#     sheet.title = xlsSheet.name
#
#     for row in range(0, xlsSheet.nrows):
#         for col in range(0, xlsSheet.ncols):
#             sheet.cell(row=row, column=col).value = xlsSheet.cell_value(row, col)
#
# # The new xlsx file is in "workbook", without iterators (iter_rows).
# # For iteration, use "for row in worksheet.rows:".
# # For range iteration, use "for row in worksheet.range("{}:{}".format(startCell, endCell)):"
