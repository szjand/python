# encoding=utf-8
"""
pdf from global connect
    order workbench
        reports & tools
            Dealer Constraints Distribution Report
            save as excel: actually, it is an html file saved with an xls extension
"""
import db_cnx
import csv
from bs4 import BeautifulSoup

# file = open('pdfs/VOM_PdfAction (8).xls')
file = 'pdfs/VOM_PdfAction (8).xls'
soup = BeautifulSoup(open(file), 'lxml')
rows = []
for row in soup.find_all('tr'):
    rows.append([val.text.strip() for val in row.find_all('td')])
# this eliminates the rows with no info
with open('csv_from_pdf/finally.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerows(row for row in rows if len(row) > 1)

with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate nc.ext_dealer_constraints_1;")
        with open('csv_from_pdf/finally.csv', 'r') as io:
            pg_cur.copy_expert("""copy nc.ext_dealer_constraints_1 from stdin encoding 'latin-1 '""", io)