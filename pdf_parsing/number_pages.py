# encoding=utf-8
"""
working with national constraints report (showconstraintsreport.pdf
the build out constraints are what we need
but
we have tables that extend across pages that creattes incomplete records
so, lets try to number pages and rows as we go
"""
import camelot
import db_cnx
import os
import utilities
# import csv
# location for csv files generated from pdfs

csv_dir = 'csv_from_pdf/'
pdf_dir = 'pdfs/'
pdf_file = 'showconstraintsreport.pdf'
csv_file = 'buildout_constraints.csv'
db2_server = 'report'
pg_server = '173'

with utilities.pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute('truncate nc.xfm_build_out_constraints_1')

table_count = camelot.read_pdf(pdf_dir + pdf_file, pages='1-end')
i = table_count.n  # the number of tables in the pdf
while i > 0:
    for filename in os.listdir(csv_dir):  # only ever to be one file in directory
        os.unlink(csv_dir + filename)
    tables = camelot.read_pdf(pdf_dir + pdf_file, pages=str(i), copy_text=['v'], strip_text='\n')
    tables[0].to_csv(csv_dir + csv_file)
    file = csv_dir + csv_file
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate nc.ext_build_out_constraints')
            with open(file, 'r') as io:
                if len(tables[0].cols) == 5:
                    pg_cur.copy_expert("""copy nc.ext_build_out_constraints from stdin
                                        with csv encoding 'latin-1 '""", io)
                if len(tables[0].cols) == 4:
                    pg_cur.copy_expert("""copy nc.ext_build_out_constraints(model,description,last_dosp,
                                            last_production_week) from stdin with csv encoding 'latin-1 '""", io)
            sql = "select nc.xfm_build_out_constraints_1(" + str(i) + ")"
            pg_cur.execute(sql)
    i = i - 1
# with utilities.pg(pg_server) as pg_con:
#     with pg_con.cursor() as pg_cur:
#         pg_cur.execute('select nc.xfm_build_out_constraints()')
