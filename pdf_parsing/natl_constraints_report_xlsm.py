# encoding=utf-8
"""
xlsm from global connect
    order workbench
        reports & tools
           GMFleet.com Order Management Reports
                1st report in the list 4-12-2019 GMNA Weekly Dealer Constraints
very interesting
workbook consisting of multiple worksheets:
    Build Out-US
    SUMMARY04122019
    Buick
    Cadillac
    Chevrolet
    GMC
start out with the Build Out
see if we can get a clean scrape from the xlsm

1st shot
convert xlsm to csv:
    https://stackoverflow.com/questions/23554808/how-to-extract-sheet-from-xlsm-and-save-it-as-csv-in-python
and it works, nice looking csv
it also will loop through all the sheets and creates an separate csv file for each sheet, but, for now
    we are not using that additional data

issues:
    pdf is dated 4/24, xlsm is dated 4/10, ie, not current
end of story, damnit
"""
import camelot
import db_cnx
import os
import csv
import xlrd

xlsm_path = 'xlsm/'
xlsm_file = os.listdir(xlsm_path)[0]
print(xlsm_file)

workbook = xlrd.open_workbook(xlsm_path + xlsm_file)
for sheet in workbook.sheets():
    # if sheet.name =='Build Out-US':
    with open('{}.csv'.format(sheet.name), 'w') as f:
        writer = csv.writer(f)
        for row in range(sheet.nrows):
            out = []
            for cell in sheet.row_values(row):
                out.append(cell)
            writer.writerow(out)

# csv_path = 'csv_from_pdf/'
# pdf_path = 'pdfs/'
# # (a dictionary of changes to make, find 'key' substitue with 'value', removing line feeds from csv files
# changes = {'\n': ''}
# # start with an empty direcory
# for filename in os.listdir(csv_path):
#     os.unlink(csv_path + filename)
# with db_cnx.pg() as pg_con:
#     with pg_con.cursor() as pg_cur:
#         pg_cur.execute('truncate nc.ext_build_out_constraints')
#         pg_cur.execute('truncate nc.ext_national_constraints')