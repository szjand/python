import camelot
import os
import string
import fileinput
import csv
import db_cnx

# # remove line feed from within fields
# # the double\\ in the replace was the solution
# f = open('csv_from_pdf/build_out-page-4-table-1.csv')
# csv_f = csv.reader(f)
# for row in csv_f:
#     print(row)
#     print(str(row).replace('\\n',' '))
#
#
# # # list tables and number of columns
# csv_path = 'csv_from_pdf/'
# pdf_pat = 'pdfs/'
# # start with an empty direcory
# for filename in os.listdir(csv_path):
#     os.unlink(csv_path + filename)
# tables = camelot.read_pdf('pdfs/model_year_build_out_start_up.pdf', pages='1-end')
# tables.export('csv_from_pdf/build_out.csv', f='csv')
# with os.scandir(csv_path) as files:
#     i = 0
#     for file in files:
#         print(tables[i])
#         print(len(tables[i].cols))
#         i += 1

# # this works much better, gives me a valid csv file that copies into pg
# new_rows = []
# changes = {'\n' : ' '}
# with open('csv_from_pdf/build_out-page-1-table-1.csv', 'r') as f:
#     reader = csv.reader(f)
#     for row in reader:
#         new_row = row
#         for key, value in changes.items():
#             new_row = [x.replace(key, value) for x in new_row]
#         new_rows.append(new_row)
# with open('csv_from_pdf/build_out-page-1-table-101.csv', 'w') as f:
#     writer = csv.writer(f)
#     writer.writerows(new_rows)
# with db_cnx.pg() as pg_con:
#     with pg_con.cursor() as pg_cur:
#         with open('csv_from_pdf/build_out-page-1-table-101.csv', 'r') as io:
#             pg_cur.copy_expert("""copy nc.ext_build_out_2019 from stdin
#                                     with csv encoding 'latin-1 '""", io)

# this works much better, gives me a valid csv file that copies into pg
# same thing, but overwriting the original file, and it works
new_rows = []
changes = {'\n' : ' '}
with open('csv_from_pdf/build_out-page-1-table-1.csv', 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        new_row = row
        for key, value in changes.items():
            new_row = [x.replace(key, value) for x in new_row]
        new_rows.append(new_row)
with open('csv_from_pdf/build_out-page-1-table-1.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerows(new_rows)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        with open('csv_from_pdf/build_out-page-1-table-1.csv', 'r') as io:
            pg_cur.copy_expert("""copy nc.ext_build_out_2019 from stdin
                                    with csv encoding 'latin-1 '""", io)