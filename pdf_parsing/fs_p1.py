import camelot
import db_cnx
file_name = 'csv_from_pdf/fs_p2-page-2-table-1.csv'
tables = camelot.read_pdf('pdfs/fs_p1.pdf',pages='2')
tables.export('csv_from_pdf/fs_p2.csv', f='csv')
print(tables[0])
print(len(tables[0].cols))
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy nc.fs_p2 from stdin with csv encoding 'latin-1 '""", io)
