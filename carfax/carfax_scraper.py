# encoding=utf-8
"""
04/22/21
    appears adding --kiosk-printing in options is good enough to get it printing, still going to downloads rather
    than pdf_dir, maybe it is the shared drive
"""

from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException
# import os
import requests
import json

pdf_dir = '/mnt/hgfs/E/python_projects/carfax/pdf_dir/'
user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None


def setup_driver():
    # # Chromedriver path
    # settings = {
    #     "appState": {
    #         "recentDestinations": [{
    #             "id": "Save as PDF",
    #             "origin": "local"
    #         }],
    #         "selectedDestinationId": "Save as PDF",
    #         "version": 2
    #     }
    # }
    chromedriver_path = '/usr/bin/chromedriver'
    options = webdriver.ChromeOptions()
    profile = {"plugins.plugins_list": [{"enabled": False, "name": "Chrome PDF Viewer"}],
               "download.prompt_for_download": False,  # To auto download the file
               "download.directory_upgrade": True,
               "download.default_directory": pdf_dir}
               # "printing.print_preview_sticky_settings": json.dumps(settings)}
    options.add_experimental_option("prefs", profile)
    options.add_argument('--kiosk-printing')
    options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                         '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
    driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
    return driver


def get_secret(category, platform):
    modified_platform = platform.replace(' ', '+')
    url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
           (modified_platform, category))
    response = requests.get(url, auth=(user, passcode))
    return response.json()


def login(driver):
    credentials = get_secret('Cartiva', 'Carfax')
    resp_dict = credentials
    username_box = driver.find_element_by_id('username')
    password_box = driver.find_element_by_id('password')
    login_btn = driver.find_element_by_id('login_button')
    username_box.send_keys(resp_dict.get('username'))
    password_box.send_keys(resp_dict.get('secret'))
    login_btn.click()
    WebDriverWait(driver, 20).until(ec.presence_of_element_located(('id', 'vin')))


def main(vin):
    driver = setup_driver()
    try:
        driver.get('https://www.carfaxonline.com/login')
        # sign in
        login(driver)
        # enter a vin
        vin_box = driver.find_element_by_id('vin')
        vin_box.send_keys(vin)
        # click get carfax report
        driver.find_element_by_id('header_run_vhr_button').click()
        time.sleep(2)
        driver.switch_to.window(driver.window_handles[1])
        try:  # verify the print button is on the page, signifying a valid vin response
            WebDriverWait(driver, 10).until(ec.presence_of_element_located(('id', 'cipPrintBtn')))
        except TimeoutException:
            print('Bad VIN?')
            exit()
        print('we made it to the vehicle history report')
        # click the big print button in the upper left of the page
        driver.find_element_by_id('cipPrintBtn').click()
        time.sleep(2)  # required
        # click the print button on the Printing Options modal
        driver.find_element_by_xpath('/html/body/div[4]/div[2]/div/form/div[5]/input[1]').click()
        time.sleep(10)
        # now this takes me to to print preview page, can't see dev tools, don't know what to do
        # nope, it is not a new window  driver.switch_to.window(driver.window_handles[2])
        # this doesn't work either, driver.find_element_by_xpath('//*[@id="button-strip"]/paper-button[2]').click()
        # driver.close()
    finally:
        driver.quit()


if __name__ == '__main__':
    main('1GCER14K7HJ165644')
