# encoding=utf-8
"""
04/22/21
    just parsing snippets
"""

import json
from bs4 import BeautifulSoup as bs


# pdf_dir = '/mnt/hgfs/E/python_projects/carfax/pdf_dir/'
html_dir = '/home/jon/carfax/'


def add_indent(text, n):
    sp = " "*n
    lsep = chr(10) if text.find(chr(13)) == -1 else chr(13)+chr(10)
    lines = text.split(lsep)
    for i in range(len(lines)):
        spacediff = len(lines[i]) - len(lines[i].lstrip())
        if spacediff:
            lines[i] = sp*spacediff + lines[i]
        return lsep.join(lines)


def owner_history(vin):
    file = html_dir + vin + '.html'
    with open(file, 'r') as f:
        soup = bs(f, 'lxml')

    # -----------------------------------------------------------------------------
    # < ownership history
    # -----------------------------------------------------------------------------
        ownership_history = soup.find(id="summaryOwnershipHistoryTable")
        # # this is what i did to generate just the table for afton
        # the_table = ownership_history.prettify()
        # the_table = add_indent(the_table,1)
        # print(the_table)
        for tag in ownership_history('img'):
            tag.decompose()
        for tag in ownership_history('th', attrs={'class': 'backToTopHeading'}):
            tag.decompose()
        # and this is afton's magic
        ownership_dict = dict()
        # Find table rows that contain a class name of summary (odd and even are real names)
        summary_table_rows = ownership_history.select('tr[class*="summary"]')
        for row in summary_table_rows:
            # table_cells = row.select('td') # turns out this is not used
            # Find cells with no classes --  data values
            no_class = row.find_all(attrs={'class': None})
            # Find cells with class of tcCopy -- title values
            title_class = row.find(attrs={'class': 'tcCopy'})
            # get the inner string and strip all the extra spacing
            title = title_class.string.strip()
            # Add the title to the dict with an empty list (array)
            ownership_dict[title] = []

            for idx, val in enumerate(no_class):
                # Start a new dict for the data values
                val_dict = dict()
                # Index + 1 is the owner (otherwise it starts at 0)
                val_dict[idx + 1] = val.string.strip()
                # Append the dict to the list
                ownership_dict[title].append(val_dict)

        print(ownership_dict)
        database_json = json.dumps(ownership_dict)
    # -----------------------------------------------------------------------------
    # /> ownership history
    # -----------------------------------------------------------------------------

    # -----------------------------------------------------------------------------
    # < vehicle vin decode
    # -----------------------------------------------------------------------------


def vehicle_vin_decode(vin):
    """
    thinking the eventual table could be built with multiple jsonb fields
    """
    file = html_dir + vin + '.html'
    with open(file, 'r') as f:
        soup = bs(f, 'lxml')
        the_vehicle = {}
        vin_decode = {}
        vin_decode['vin'] = vin
        vin_decode['year_make_model'] = soup.find('div', id='headerMakeModelYear').get_text(strip='True')
        vin_decode['body_type'] = soup.find('div', id='headerBodyType').get_text(strip='True')
        vin_decode['engine'] = soup.find('div', id='headerEngineInfo').get_text(strip='True')
        vin_decode['fuel'] = soup.find('div', id='headerFuel').get_text(strip='True')
        vin_decode['drivetrain'] = soup.find('div', id='headerDriveline').get_text(strip='True')
        vin_decode['original_window_sticker'] = soup.find('div', id='originalWindowSticker').get_text(strip='True')
        the_vehicle['vehicle'] = vin_decode
        print(the_vehicle)

    # -----------------------------------------------------------------------------
    # /> vehicle vin decode
    # -----------------------------------------------------------------------------

    # -----------------------------------------------------------------------------
    # < history summary
    # -----------------------------------------------------------------------------


def vehicle_history_summary(vin):
    file = html_dir + vin + '.html'
    with open(file, 'r') as f:
        soup = bs(f, 'lxml')
        history_summary = {}
        summary = {}
        summary ['accidents'] = soup.find('div',id='vhrHeaderRow0').get_text()
        summary['owners'] = soup.find('div', id='vhrHeaderRow1').get_text()
        summary['service'] = soup.find('div', id='vhrHeaderRow2').get_text()
        summary['owner_type'] = soup.find('div', id='vhrHeaderRow3').get_text()
        summary['reg_state'] = soup.find('div', id='vhrHeaderRow4').get_text()
        summary['odometer'] = soup.find('div', id='vhrHeaderRow5').get_text()
        history_summary['history_summary'] = summary
        print(history_summary)
    # -----------------------------------------------------------------------------
    # /> history summary
    # -----------------------------------------------------------------------------

    # -----------------------------------------------------------------------------
    # < detailed history
    # https://www.geeksforgeeks.org/how-to-use-xpath-with-beautifulsoup/
    # -----------------------------------------------------------------------------


def detailed_history(vin):
    file = html_dir + vin + '.html'
    with open(file, 'r') as f:
        soup = bs(f, 'lxml')
        # sll this compose helps cleanup the history detail section
        for tag in soup('script'):
            tag.decompose()
        for tag in soup('img'):
            tag.decompose()
        for tag in soup('svg'):
            tag.decompose()
        for tag in soup('style'):
            tag.decompose()
        classes = ['favorites', 'histogram-popup', 'owner-bar', 'details-row details-row-header']
        # classes = ['favorites', 'histogram-popup']
        for i in classes:
            for tag in soup.find_all("div", {'class': i}):
                tag.decompose()


        # # < test 1 works for all 3 owners
        # lvl_1 = soup.find('div', id='owner3LinkTarget')
        # dom = etree.HTML(str(lvl_1))
        # print(dom.xpath('//*[@id="owner3LinkTarget"]/div[3]/div[1]')[0].text.strip())
        # # /> test 1 works for all 3 owners

        # # < test 2 works for all 3 owners first row of data
        if soup.find('div', id='vhrHeaderRow1').get_text().split(' ', 1)[0] == 'CARFAX':
            owners = 1
        else:
            owners = int(soup.find('div', id='vhrHeaderRow1').get_text().split(' ', 1)[0])
        # for idx in range(1, owners + 1): # range is (inclusive, exclusive), eg owners = 3, range = 1,2,3
        #     level_id = 'owner'+str(idx)+'LinkTarget'
        #     level = soup.find('div', id=level_id)
        #     dom = etree.HTML(str(level))
        #     the_xpath = '//*[@id='+'"'+level_id+'"'']/div[3]/div[1]'
        #     print(dom.xpath(the_xpath)[0].text.strip())
        # -----------------------------------------------------------------------------
        # /> test 2 works for all 3 owners first row of data
        # -----------------------------------------------------------------------------

        # # this gives me the owner_link_target interspersed with long lists of <class 'bs4.element.ResultSet'>
        # # from the docs: find_all() returns a _list_ of tags and strings–a ResultSet object
        # for idx in range(1, owners + 1):
        #     owner_link_target = 'owner' + str(idx) + 'LinkTarget'
        #     print(owner_link_target)
        #     for top_div in soup.find(id=owner_link_target):
        #         first_row = soup.find_all('div', attrs={'class': 'details-row evenrow'})
        #         print(type(first_row))

        # # this is interesting, it is so long, had to limit it to just the first owner
        # # all the text from all the even rows, with lots of blank lines
        # # don't yet know how to remove the blank lines
        # for idx in range(1, 2):
        #     owner_link_target = 'owner' + str(idx) + 'LinkTarget'
        #     print(owner_link_target)
        #     for top_div in soup.find(id=owner_link_target):
        #         for x in  soup.find_all('div', attrs={'class': 'details-row evenrow'}):
        #             if len(x.get_text(strip='True')) == 0:
        #                 x.extract()
        #             print(x.text)

        # for idx in range(1, 2):
        #         owner_link_target = 'owner' + str(idx) + 'LinkTarget'
        #         print(owner_link_target)
        #         for top_div in soup.find(id=owner_link_target):
        #             for x in  soup.find_all('div', attrs={'class': 'details-row evenrow'}):
        #                 print(x.text)
        # need to figure out how to iterate through the bs4.element.ResultSet' like a list of elements

        # # this is looking better, but i think there are way more objects than i expect
        # # fuck, 38 instances of mileage 151,631 !!!
        # for idx in range(1, 2):
        #     owner_link_target = 'owner' + str(idx) + 'LinkTarget'
        #     print(owner_link_target)
        #     for top_div in soup.find(id=owner_link_target):
        #         first_row = soup.find_all('div', attrs={'class': 'details-row evenrow'})
        #         for item in range(0, len(first_row) - 1):
        #             print(first_row[item].prettify())

#         for idx in range(1, 2):  # limit to just the first owner
#             owner_link_target = 'owner' + str(idx) + 'LinkTarget'
#             for owner_link in soup.find('div', id=owner_link_target):
#                 # # for the 22 rows for owner 1, this gives me 28 pairs  of bs4.element.navigableString/bs4.element.tag
#                 # print(type(owner_link))
#                 print(owner_link)
#
# # this is where i keep getting fucking stuck, understnading the type of what is being returned and whether to
# # iterate it or not
# #
# #                 for detail_row in  owner_link.select('div', {'class': ['details-row evenrow', 'details-row oddrow']}):
# #                     print(detail_row.string())

        # # ok, ctfd, try to iterate, a single item at a time
        # # decomposed some more up top
        # # this appears to be getting the correct block, only 3 instances of 01/22/2007
        # # except, i don't get this, why the idx but nothing else for 0 - 3
        # # 05/02/21 ok, it makes sense, i have decomposed the contents of the first 4 divs
        # """
        #     0
        #
        #     1
        #
        #     2
        #
        #     3
        #
        #     4 <div class="details-row evenrow">
        # """
        # for idx in range(1, 2):  # limit to just the first owner
        #     owner_link_target = 'owner' + str(idx) + 'LinkTarget'
        #     for idx, owner_link in enumerate(soup.find('div', id=owner_link_target)):
        #         print(idx, owner_link)



        for idx in range(1, 2):  # limit to just the first owner
            owner_link_target = 'owner' + str(idx) + 'LinkTarget'
            # owner_link = soup.select('div', id=owner_link_target)
            # # print(type(owner_link))  # bs4.element.ResultSet, len = 859
            owner_link = soup.find('div', id=owner_link_target)
            print(type(owner_link))  # bs4.element.Tag, len = 52, 52 what ????
            # owner_link = soup.find_all('div', id=owner_link_target)
            # print(type(owner_link))  # bs4.element.ResultSet, len = 1
            # print(len(owner_link))
            # print(owner_link)
            # printing find vs find_all: results are identical, the only difference is that find_all text isodate
            #     surrounded in [], eg a big list

            # **********************************************************************************************************
            # # finally, this works for miles
            # # the only glitch is, for owner 3, the last 2 rows have no mileage, this only list one of those rows
            # miles = owner_link.find_all('div',  attrs={'class': 'mileage'})
            # for idx, mileage in enumerate(miles):
            #     print(idx, mileage.get_text().strip())

            # miles = owner_link.find_all('div',  attrs={'class': 'mileage'})
            # for idx, the_date in enumerate(miles):
            #     print(idx, repr(the_date.previous_siblings))

            # # the payoff from working thru the docs
            # for idx, string in enumerate(owner_link.stripped_strings):
            #     print(idx, repr(string))

            # # this gives me something, but ...
            # no_class = owner_link.find_all('div',  attrs={'class': None})
            # for idx, the_div in enumerate(no_class):
            #     print(idx, the_div.get_text().strip())

            # find all the divs with class details-row evenrow or details-row oddrow and fiddle with sibblings
            top_div = owner_link.find_all('div', {'class': ['details-row evenrow', 'details-row oddrow']}, limit=1)
            print(type(top_div)) # why does this return 2 results: bs4.element.Tag & element.ResultSet
            print(len(top_div))

            top_div = owner_link.find('div', {'class': ['details-row evenrow', 'details-row oddrow']})
            print(type(top_div)) # why does this return 2 results: bs4.element.Tag & element.ResultSet
            print(len(top_div))


            # print(top_div[0].prettify())
            # for some_div in top_div:
            #     # print(some_div.prettify()) # this is giving me the entire div




def main(vin):
    # owner_history(vin)
    # vehicle_vin_decode(vin)
    # vehicle_history_summary(vin)
    detailed_history(vin)


if __name__ == '__main__':
    # main('3GNAXUEU2JS552362')  # 18 equinox
    main('JHLRE487X7C041457')  # 07 CR-V 3 owners
    # main('1GYKPFRS3LZ165417')  # 20 cad xt6
