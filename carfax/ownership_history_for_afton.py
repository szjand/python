# encoding=utf-8

from bs4 import BeautifulSoup as bs
import json

"""
04/28/21
she did it, a little different, but looks good
and here is a query she sent as well

select data_title, b.key as the_owner, b.value as the_value
from (
  select "key" as data_title, "value" as data_values_json
  from jsonb_each_text('{"Year purchased": [{"1": "2007"}, {"2": "2013"}, {"3": "2019"}], "Type of owner": [{"1": "Personal"}, {"2": "Personal"}, {"3": "Personal"}], "Estimated length of ownership": [{"1": "6 yrs. 4 mo."}, {"2": "5 yrs. 11 mo."}, {"3": "1 yr. 9 mo."}]}
  '::jsonb)
) A
left join jsonb_array_elements(data_values_json::jsonb) as data_values on 1 = 1
left join jsonb_each_text(data_values) b on 1 = 1


vin = JHLRE487X7C041457

trying to create this data structure
the actual table has 3 more columns, but if you can get these 3 to work, the others will be just the same
i also removed those 3 columns from the HTML

trying to create this data structure:
    {ownership history: {
        Owner 1: {'year purchased': '2007', 'type of owner': 'personal', 'est length of ownership': '6 yrs 4 mos'},
        Owner 2: {'year purchased': '2013', 'type of owner': 'personal', 'est length of ownership': '5 yrs 11 mos'},
        Owner 3: {'year purchased': '2019',' type of owner': 'personal', 'est length of ownership': '1 yr 9 mos'}}}
"""
html = """
    <table border="0" cellpadding="0" cellspacing="0" id="summaryOwnershipHistoryTable" width="100%">
      <tbody>
        <tr class="secHdrRow">
          <th height="30px">
            <a id="OwnershipHistorySection">
              <div class="section-header-container">
                <div class="section-header-title">
                  <img alt="CARFAX" class="section-header-logo" src="https://static.carfax.com/ReportDeliveryTeam/VehicleHistoryReport/images/sitespeed/CarfaxLogo.svg"/>
                  <span class="section-header-text">
                    Ownership History
                  </span>
                </div>
                <div class="section-description">
                  The number of owners is estimated
                </div>
              </div>
            </a>
          </th>
          <th class="backToTopHeading">
            <div class="backToTop">
              <a class="section-header-link back-to-top-link" href="#backToTopBody">
                <i class="material-icons caret">
                  expand_less
                </i>
                <p>
                  Back To Top
                </p>
              </a>
            </div>
          </th>
          <th class="statCol ownerColumnTitle">
            <img align="absmiddle" alt="" border="0" height="15px" src="https://media.carfax.com/img/vhr/ownertick.png" width="11px"/>
            Owner 1
          </th>
          <th class="statCol ownerColumnTitle">
            <img align="absmiddle" alt="" border="0" height="15px" src="https://media.carfax.com/img/vhr/ownertick.png" width="11px"/>
            Owner 2
          </th>
          <th class="statCol ownerColumnTitle">
            <img align="absmiddle" alt="" border="0" height="15px" src="https://media.carfax.com/img/vhr/ownertick.png" width="11px"/>
            Owner 3
          </th>
        </tr>
        <tr class="summaryOdd">
          <td class="eventCol" colspan="2">
            <div class="tcCopy">
              Year purchased
            </div>
          </td>
          <td class="statCol">
            <div>
              2007
            </div>
          </td>
          <td class="statCol">
            <div>
              2013
            </div>
          </td>
          <td class="statCol">
            <div>
              2019
            </div>
          </td>
        </tr>
        <tr class="summaryEven">
          <td class="eventCol" colspan="2">
            <div class="tcCopy">
              Type of owner
            </div>
          </td>
          <td class="statCol">
            <div>
              Personal
            </div>
          </td>
          <td class="statCol">
            <div>
              Personal
            </div>
          </td>
          <td class="statCol">
            <div>
              Personal
            </div>
          </td>
        </tr>
        <tr class="summaryOdd">
          <td class="eventCol" colspan="2">
            <div class="tcCopy">
              Estimated length of ownership
            </div>
          </td>
          <td class="statCol">
            <div>
              6 yrs. 4 mo.
            </div>
          </td>
          <td class="statCol">
            <div>
              5 yrs. 11 mo.
            </div>
          </td>
          <td class="statCol">
            <div>
              1 yr. 9 mo.
            </div>
          </td>
        </tr>
      </tbody>
    </table>
"""
soup = bs(html, 'lxml')
# remove img tags & a superfluous th from the first row
for tag in soup('img'):
    tag.decompose()
for tag in soup('th', attrs={'class': 'backToTopHeading'}) :
    tag.decompose()
# print(soup.prettify())

# ***** above is what i sent to afton, below is what afton sent to me *****

ownership_dict = dict()
# Find table rows that contain a class name of summary (odd and even are real names)
summary_table_rows = soup.select('tr[class*="summary"]')
for row in summary_table_rows:
    # table_cells = row.select('td') # turns out this is not necessary
    # Find cells with no classes --  data values
    no_class = row.find_all(attrs={'class': None})
    # Find cells with class of tcCopy -- title values
    title_class = row.find(attrs={'class': 'tcCopy'})
    # get the inner string and strip all the extra spacing
    title = title_class.string.strip()
    # Add the title to the dict with an empty list (array)
    ownership_dict[title] = []

    for idx, val in enumerate(no_class):
        # Start a new dict for the data values
        val_dict = dict()
        # Index + 1 is the owner (otherwise it starts at 0)
        val_dict[idx + 1] = val.string.strip()
        # Append the dict to the list
        ownership_dict[title].append(val_dict)

print(ownership_dict)
database_json = json.dumps(ownership_dict)