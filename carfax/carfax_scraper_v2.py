# encoding=utf-8
"""
04/22/21
    appears adding --kiosk-printing in options is good enough to get it printing, still going to downloads rather
    than pdf_dir, maybe it is the shared drive
"""

from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
import requests
from bs4 import BeautifulSoup as bs
import csv


# pdf_dir = '/mnt/hgfs/E/python_projects/carfax/pdf_dir/'
html_dir = '/home/jon/carfax/'
user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None


def setup_driver():
    chromedriver_path = '/usr/bin/chromedriver'
    options = webdriver.ChromeOptions()
    profile = {}
    options.add_experimental_option("prefs", profile)
    options.add_argument("--headless")
    options.add_argument("--window-size=1920x1080")
    driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
    return driver


def get_secret(category, platform):
    modified_platform = platform.replace(' ', '+')
    url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
           (modified_platform, category))
    response = requests.get(url, auth=(user, passcode))
    return response.json()


def login(driver):
    credentials = get_secret('Cartiva', 'Carfax')
    resp_dict = credentials
    username_box = driver.find_element_by_id('username')
    password_box = driver.find_element_by_id('password')
    login_btn = driver.find_element_by_id('login_button')
    username_box.send_keys(resp_dict.get('username'))
    password_box.send_keys(resp_dict.get('secret'))
    login_btn.click()
    WebDriverWait(driver, 20).until(ec.presence_of_element_located(('id', 'vin')))


def owner_history(vin):
    file = html_dir + vin + '.html'
    with open(file, 'r') as f:
        soup = bs(f, 'lxml')

    # -----------------------------------------------------------------------------
    # < ownership history
    # these are my failed attempts before reaching out to Afton
    # -----------------------------------------------------------------------------
    """
    trying to create this data structure (actually 6 "columns")
        {ownership history: {
            Owner 1: {year purchased : 2007, type of owner : personal, est length of ownership: 6 yrs 4 mos},
            Owner 2: {year purchased : 2013, type of owner : personal, est length of ownership: 5 yrs 11 mos},
            Owner 3: {year purchased : 2019, type of owner : personal, est length of ownership: 1 yr 9 mos}}}      
    """
    ownership_history = soup.find(id="summaryOwnershipHistoryTable")
    # ownership_rows = ownership_history.find_all("tr")
    the_table = ownership_history.prettify()
    the_table = add_indent(the_table,1)
    print(the_table)

    # # list of all td values: ['Year purchased', '2007', '2013', '2019']
    # for tr in ownership_rows:
    #     td = tr.find_all('td')
    #     # print(td)
    #     row = [i.text.strip() for i in td]
    #     print(row)
    #
    # # not what i am looking for, htis returns each td value as a separate list
    # for index, row in enumerate(ownership_rows):
    #     if index > 0:
    #         # td = row.find_all('div', attrs={'class': 'tcCopy'}).get_text(strip=True)
    #         td = row.find_all('div', attrs={'class': 'tcCopy'})
    #         # print(td)
    #         wtf = [i.text.strip() for i in td]
    #         print(wtf)
    #
    # # this returns 6 dictionaries: {'field_name': 'Year purchased'}, {'field_name': 'Type of owner'},etc
    # ownership_info = {}
    # for index, row in enumerate(ownership_rows):
    #     if index > 0:
    #         ownership_info['field_name'] = row.find('div', attrs={'class': 'tcCopy'}).get_text(strip=True)
    #         print(index, ownership_info)

    # all text in ownership history table
    # the_test = [text for text in ownership_history.stripped_strings]
    # print(the_test)
    # # this might be something worth trying to refactor
    # table = soup.find('table')
    # headers = [header.text for header in table.find_all('th')]
    # results = [{headers[i]: cell for i, cell in enumerate(row.find_all('td'))}
    #            for row in table.find_all('tr')]

    # # another SO example worth playing with
    # https://www.baseball-reference.com/leagues/MLB/2013-finalyear.shtml
    # (The table has an id, it makes it more simple to target )
    # batting = doc.find(id='misc_batting')
    # careers = []
    # for row in batting.find_all('tr')[1:]:
    #     dictionary = {}
    #     dictionary['names'] = row.find(attrs = {"data-stat": "player"}).text.strip()
    #     dictionary['experience'] = row.find(attrs={"data-stat": "experience"}).text.strip()
    #     careers.append(dictionary)

    # list comprehension with enumerate
    # """
    #     import requests
    #     import bs4
    #
    #     page=requests.get("https://covid-19.hackanons.com/test.html")
    #
    #     soup= bs4.BeautifulSoup(page.text,'lxml')
    #
    #     table=soup.find('table',id="main_table_countries_today")
    #
    #     headers=[ heading.text.replace(",Other","") for heading in table.find_all('th')]
    #
    #     table_rows=[ row for row in table.find_all('tr')]
    #
    #     results=[{headers[index]:cell.text for index,cell in enumerate(row.find_all("td")) }for row in table_rows]
    #
    #     for i in results:
    #         if "Country" in i:
    #             if i["Country"]=="India":
    #                 print(i)
    # """
    # https://towardsdatascience.com/using-beautifulsoup-on-wikipedia-dd0c620d5861
    # -----------------------------------------------------------------------------
    # /> ownership history
    # -----------------------------------------------------------------------------

    # -----------------------------------------------------------------------------
    # < vehicle vin decode
    # -----------------------------------------------------------------------------


def main(vin):
    driver = setup_driver()
    try:
        driver.get('https://www.carfaxonline.com/login')
        # sign in
        login(driver)
        # enter a vin
        vin_box = driver.find_element_by_id('vin')
        vin_box.send_keys(vin)
        # click get carfax report
        driver.find_element_by_id('header_run_vhr_button').click()
        time.sleep(2)
        driver.switch_to.window(driver.window_handles[1])
        with open(html_dir + vin + '.html', 'w', newline='')as f:
            f.write(driver.page_source)
    finally:
        driver.quit()
    # parse_file(vin)


if __name__ == '__main__':
    # main('3GNAXUEU2JS552362')
    # main('JHLRE487X7C041457')
    main('1GYKPFRS3LZ165417')  # 20 cad xt6
