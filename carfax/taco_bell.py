# encoding=utf-8
"""
https://www.reddit.com/r/learnpython/comments/aktqqs/need_help_looping_through_beautifulsoup_result_set/
https://boards.greenhouse.io/tacobell
"""
from selenium import webdriver
from bs4 import BeautifulSoup as bs

url = 'https://boards.greenhouse.io/tacobell'
html_dir = '/home/jon/carfax/'


def setup_driver():
    chromedriver_path = '/usr/bin/chromedriver'
    options = webdriver.ChromeOptions()
    profile = {}
    options.add_experimental_option("prefs", profile)
    options.add_argument("--headless")
    options.add_argument("--window-size=1920x1080")
    driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
    return driver


def get_the_html():
    driver = setup_driver()
    try:
        driver.get(url)
        with open(html_dir + 'taco_bell.html', 'w', newline='')as f:
            f.write(driver.page_source)
    finally:
        driver.quit()


def parse_the_html():
    file = html_dir + 'taco_bell.html'
    with open(file, 'r') as f:
        soup = bs(f, 'lxml')

        # sections = soup.find_all('section', {'class': 'level-0'})
        # print(sections)
        # print(type(sections))

        # found_jobs = []
        # for job_groups in sections: # fixed some types in this section
        #     for job_posts in job_groups.find_all('div', {'class' : 'opening'}):
        #         location = job_posts.find('span',{'class': 'location'})
        #         loc = location.getText()
        #         if loc == 'Irvine':
        #             found_jobs.append(job_posts.getText)
        #
        # print('Found ' + str(len(found_jobs)) + ' job posting(s)...')
        #
        # for job in found_jobs:
        #         print(job)

        # levels = soup.find_all('section', {'class' : 'level-0'})
        # print('levels: ' + str(type(levels))) # ResultSet
        # for level in levels:
        #     print('level: ' + str(type(level))) # Tag
        #     divs = level.find_all('div', {'class' : 'opening'})
        #     for div in divs: # Tag
        #         print('div: ' + str(type(div)))
        #         # print(div.text)


        Irvine_jobs = []
        levels = soup.find_all('section', {'class' : 'level-0'})
        for level in levels:
            temp = []
            temp.append(level.find("h3").text)
            divs = level.find_all('div', {'class' : 'opening'})
            for div in divs:
                if div.find("span").text == "Irvine":
                    temp.append(div.find("a").text)
            Irvine_jobs.append(temp)
        print(Irvine_jobs)


def main():
    # get_the_html()
    parse_the_html()


if __name__ == '__main__':
    main()
