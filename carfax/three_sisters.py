# encoding=utf-8
"""
"""
from bs4 import BeautifulSoup

html_doc = """
<html>
    <head>
        <title>The Dormouse's story</title>
    </head>
<body>
    <p class="title">
        <b>The Dormouse's story</b>
    </p>

<p class="story">Once upon a time there were three little sisters; and their names were
    <a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
    <a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
    <a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
    and they lived at the bottom of a well.</p>

<p class="story">...</p>
"""
"""
head_tag = soup.head
title_tag = head_tag.contents[0]
title_tag = soup.title
html_tag = soup.html
"""
# A tag’s children are available in a list called .contents

# soup.find_all('title', limit=1) vs soup.find('title')
#   find_all() returns a list containing the single result, and find() just returns the result

soup = BeautifulSoup(html_doc, 'lxml')

# for child in soup.html.descendants:
#     print(child)

result = soup.find('a')

print(type(result))

print(result)
print(result.next_sibling.next_sibling)

