#!/usr/bin/python
from selenium import webdriver
from selenium.webdriver.chrome.options import Options



def setup_driver():
    DIRNAME = 'C:'
    chromedriver_path = DIRNAME + '/chromedriver'
    chrome_options = Options()
    chrome_options.add_argument("--headless")

    driver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
    return driver

def setup_driver_not_headless():
    DIRNAME = 'C:'
    chromedriver_path = DIRNAME + '/chromedriver'
    chrome_options = Options()
    # chrome_options.add_argument("--headless")

    driver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
    return driver