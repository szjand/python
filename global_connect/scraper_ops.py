# encoding=utf-8

import json
import psycopg2
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import os


def pg():
    return psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")


def send_email_error(_run_id, _error, _line_number, _position,  _error_emails,_line_script):
    body = """
    <html>
    <body>
   <ul>
        <li> Error: %s </li>
        <li> Run Id: %s </li>
        <li> Line Number: %s </li>
        <li> Line Script : %s </li>
        <li> Position: %s </li>
    </ul>
    </body>
    </html>
    """ % ( _error, _run_id, _line_number, _line_script,_position)
    message = MIMEText(body, 'html')
    msg = MIMEMultipart()
    # message['Subject'] = 'Failed Task: ' + _task + ' Run_ID: '  + str( _run_id)
    message['Subject'] = 'Failed Scraper: '' Run_ID: '+_run_id
    smtp_server = 'mail.cartiva.com'
    e = smtplib.SMTP(smtp_server)
    addr_to = _error_emails
    addr_from = 'abruggeman@cartiva.com'
    msg['To'] = ', '.join(addr_to)
    msg['From'] = addr_from
    # receivers = [_error_emails]
    e.sendmail(addr_from, addr_to, message.as_string())
    e.quit()


def log_start(_task, task_command, task_location):
    os.popen(task_command)
    with pg() as con:
        with con.cursor() as cur:
            cur.execute("truncate tsk.win_task_imports")
            with open(task_location, 'r') as io:
                cur.copy_expert \
                    ("""copy tsk.win_task_imports from stdin with csv header encoding 'latin-1' """, io)
            sql = """
            select tsk.task_start('%s')
            """ % (_task)
            # sql_test = """
            # select tsk.task_start_new('%s')
            # """ % (_task)
            cur.execute(sql)
            # cur.execute(sql_test)
            return cur.fetchone()[0]


def log_error(_run_id, _message, _position, _number, _params, _line_script):
    with pg() as con:
        with con.cursor() as cur:
            sql = """
               select tsk.task_error('%s','%s','%s','%s', '%s', '%s')
            """ % (_message.replace("'",''), _run_id, _position, _number, _params, _line_script.replace("'",''))
            cur.execute(sql)


def log_pass(_run_id):
    with pg() as con:
        with con.cursor() as cur:
            sql = """
                select tsk.task_complete('%s')
            """ % _run_id
            cur.execute(sql)


def add_vin(_run_id, _vin):
    with pg() as con:
        with con.cursor() as cur:
            sql = """
            select ops.scraper_vin('%s','%s')
            """ % (_run_id, _vin)
            cur.execute(sql)
            return cur.fetchone()[0]
