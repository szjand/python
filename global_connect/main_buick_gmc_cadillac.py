import db_cnx
import scraper_v2 as scraper
import setup
import ops
import scraper_ops
import linecache
import time
import sys
task = 'global_connect_scraper_buick_gmc_cad_vehicle_incentives'
task_location = 'C:\\scrapers\\global_connect\\inventives\\' + task + '.csv'
task_command = 'SCHTASKS /Query /TN \\tasks\\global_connect\\' + task + ' /V /FO CSV >' + task_location

counter = 0
while counter <= 3:
    counter += 1
    with db_cnx.pg() as con:
        with con.cursor() as cur:
            sql = """
            select * from gmgl.vin_incentive_scraper_vins()
            """
            cur.execute(sql)
            query = """
                select gmgl.assert_vin_incentive()
            """
            def main():
                run_id = ops.scraper_start(task, task_command, task_location)
                driver = setup.setup_driver_not_headless()
                vins = cur.fetchall()
                if scraper.login(driver, run_id) == 'pass':
                    for index, curcur in enumerate(vins):
                        print ('beginning of loop')
                        vin = curcur[0]
                        is_ctp = curcur[1]
                        print(vin)
                        if vin:
                            if is_ctp:
                                print(' IS CTP')
                                if scraper.ctp_enter_vin(driver, vin, run_id) == 'pass':
                                    print ('entered vin')
                                    if scraper.table_content(driver, run_id, vin) == 'pass':
                                        scraper.new_search(driver, run_id)
                                        print ('table content done and new search started (CTP)')
                                    else:
                                        print ('no pass but just keep going (CTP)')
                                        continue
                                elif scraper.enter_vin(driver, vin, run_id) == 'recall':
                                    print('recall')
                                    continue
                                else:
                                    print ('no recall but just keep going (CTP)')
                                    driver.quit()
                                    driver = setup.setup_driver_not_headless()
                                    scraper.login(driver, run_id)
                                    continue
                            else:
                                if scraper.enter_vin(driver, vin, run_id) == 'pass':
                                    print ('entered vin')
                                    if scraper.table_content(driver, run_id, vin) == 'pass':
                                        scraper.new_search(driver, run_id)
                                        print ('table content done and new search started')
                                    else:
                                        print ('no pass but just keep going')
                                        # scraper.new_search(driver, run_id)
                                        continue
                                elif scraper.enter_vin(driver, vin, run_id) == 'recall':
                                    print('recall')
                                    continue
                                else:
                                    print ('no recall but just keep going')
                                    driver.quit()
                                    driver = setup.setup_driver_not_headless()
                                    scraper.login(driver, run_id)
                                    continue
                    driver.quit()
                if scraper.is_complete(query,run_id) == 'pass':
                        ops.scraper_end(run_id)

                        driver.quit()
            counter += 4


            if __name__ == '__main__':
                try:
                    main()
                finally:
                    print('done')