#!/usr/bin/python
import db_cnx
import json
import scraper_ops
import linecache
import time
import sys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

zip_code = 58201


def exceptions(run_id, position,params, error):
    # print error
    # find line error info
    exc_type, exc_obj, exc_tb = sys.exc_info()
    # line number
    line_number = exc_tb.tb_lineno
    # find line error script
    f = exc_tb.tb_frame
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, line_number, f.f_globals)
    scraper_ops.log_error(run_id, str(error), position, line_number, params, line)


def login(driver, run_id):
    # Logging in on the vin lookup page so that there is no navigation
    try:
        username = 'cartiva01'
        password = ''

        driver.get('https://www.autopartners.net/apps/ioms/vinlookup/vinLookup.do?linkMode=0')

        username_box = driver.find_element_by_id('IDToken1')
        password_box = driver.find_element_by_id('IDToken2')
        login_btn = driver.find_element_by_name('Login.Submit')

        username_box.send_keys(username)
        password_box.send_keys(password)
        login_btn.click()

        # Verify Page
        time.sleep(5)
        driver.find_element_by_name('vin')
        return 'pass'
    except Exception as error:
        position = 'Logging In'
        params = 'N/A'
        exceptions(run_id, position, params, error)
        return 'fail'


def ctp_enter_vin(driver, vin, run_id):
    try:
        # Verify Page
        print(vin)
        # driver.implicitly_wait(3)

        # wait for vin input to be clickable
        vin_input = WebDriverWait(driver, 10).until(
            ec.element_to_be_clickable((By.NAME, 'vin'))
        )
        vin_input.send_keys(vin)

        # dropdown = driver.find_element_by_id('ext-gen60')
        dropdown = WebDriverWait(driver, 10).until(
            ec.element_to_be_clickable((By.ID, 'ext-gen60'))
        )
        dropdown.click()
        ctp_unit = driver.find_element_by_xpath("//*[text()[contains(., 'GMDRAC/CTA Short Term (w/<7500 miles)')]]")
        ctp_unit.click()
        # enter zip information
        zip_input = driver.find_element_by_name('zipCode')
        zip_input.send_keys(zip_code)
        # print('enter zip')
        # print('sleeping')

        # click the submit button on the bottom of the page
        submit_button = driver.find_element_by_xpath("//span[.='Submit']")
        submit_button.click()

        # print ('first submit')

        # Find the submit pop up
        submit_popup = WebDriverWait(driver, 10).until(
            ec.visibility_of_element_located((By.CSS_SELECTOR, "div.x-window"))
        )
        # print submit_popup.text

        # Select the second submit button
        submit_again = submit_popup.find_element_by_xpath("//button[text()='Submit']")
        submit_again.click()
        print ('Second submit')

        return 'pass'

    except TimeoutException:
        # If the submit again pop up does not appear then there is an error message
        # This checks  to see if the error message is about a recall
        print ('timeout Exception')
        error_div = driver.find_element_by_css_selector('div.errorPanel')
        error_li = error_div.find_elements_by_id('em-vinLookup')
        if len(error_li) > 0:
            for li in error_li:
                if li.text.encode('utf-8') != '':
                    print(li.text)
                    # look for the text "recall"
                    if "recall" in li.text:
                        # print('found recall')
                        # enter recall into db
                        with db_cnx.pg() as con:
                            with con.cursor() as cur:
                                sql = """
                                           select gmgl.vin_recalls('%s','%s')
                                           """ % (vin, li.text)
                                cur.execute(sql)
                                # reload the page
                                driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')
                                # print('reload url')
                                print ('recall')
                                return 'recall'
                    elif "unable to process your request" in li.text:
                        print ('HERE NOW FOUND MATCHING')
                        driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')
                        return 'recall'
                    elif "No vehicle could be found for the supplied VIN" in li.text:
                        with db_cnx.pg() as con:
                            with con.cursor() as cur:
                                sql = """
                                           select gmgl.vin_incentive_not_found('%s')
                                           """ % vin
                                cur.execute(sql)
                                # reload the page
                                driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')
                                # print('reload url')
                                print ('recall')
                                return 'recall'
                    else:
                        driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')
                        return 'recall'

    except Exception as error:
        params = 'VIN: %s' % vin

        # print error
        position = 'Entering Vin'
        # scraper_ops.log_error(run_id, str(error), position, line_number, params, line)
        return 'fail'


def enter_vin(driver, vin, run_id):
        try:
            print('at the beginning')
            # driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')

            vin_input = WebDriverWait(driver, 10).until(
                ec.element_to_be_clickable((By.NAME, 'vin'))
            )
            vin_input.send_keys(vin)
            try:
                dropdown = WebDriverWait(driver, 10).until(
                    ec.element_to_be_clickable((By.ID, 'ext-gen60'))
                )
                dropdown.click()
                new_unit = driver.find_element_by_xpath("//*[text()[contains(., 'New')]]")
                new_unit.click()
            except Exception as error:
                print ('ok')

            zip_input = driver.find_element_by_name('zipCode')
            zip_input.send_keys(zip_code)

            # click the submit button on the bottom of the page
            submit_button = driver.find_element_by_xpath("//span[.='Submit']")
            submit_button.click()
            print('first submit')
            # Find the submit pop up
            submit_popup = WebDriverWait(driver, 10).until(
                ec.visibility_of_element_located((By.CSS_SELECTOR, "div.x-window"))
            )
            # Select the second submit button
            submit_again = submit_popup.find_element_by_xpath("//button[text()='Submit']")
            submit_again.click()
            print('after second submit')

            return 'pass'

        except TimeoutException:
            # If the submit again pop up does not appear then there is an error message
            # This checks  to see if the error message is about a recall
            # print ('timeout Exception')
            error_div = driver.find_element_by_css_selector('div.errorPanel')
            error_li = error_div.find_elements_by_id('em-vinLookup')
            if len(error_li) > 0:
                for li in error_li:
                    if li.text.encode('utf-8') != '':
                        print(li.text)
                        print ('HERE?!')
                        # look for the text "recall"
                        if "recall" in li.text:
                            # print('found recall')
                            # enter recall into db
                            with db_cnx.pg() as con:
                                with con.cursor() as cur:

                                    sql = """
                                    select gmgl.vin_recalls('%s','%s')
                                    """ % (vin, li.text)
                                    cur.execute(sql)
                                    # reload the page
                                    driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')
                                    return 'recall'

                        elif "unable to process your request" in li.text:
                            print ('HERE NOW FOUND MATCHING')
                            driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')
                            return 'recall'
                        elif "No vehicle could be found for the supplied VIN" in li.text:
                            with db_cnx.pg() as con:
                                with con.cursor() as cur:
                                    sql = """
                                           select gmgl.vin_incentive_not_found('%s')
                                               """ % vin
                                    cur.execute(sql)
                                    # reload the page
                                    driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')
                                    # print('reload url')
                                    print ('recall')
                                    return 'recall'
                        else:
                            driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')
                            return 'recall'
                                    # print('reload url')
                        # This fails because we cannot do the next steps.
        except Exception as error:
            params = 'VIN: %s' % vin
            # print error
            position = 'Entering Vin'
            exceptions(run_id, position, params, error)
            # scraper_ops.log_error(run_id, str(error), position, line_number, params, line)
            return 'fail'


def table_content(driver, run_id, vin):
    i = 1
    while i < 3:
        try:
            print ('in table content')
            WebDriverWait(driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, "span[class^=x-grid3-row-state]"))
            )
            print ('execute script')
            num_of_major = int(driver.execute_script("return result.totalRecords"))
            counter = 0
            with db_cnx.pg() as con:
                with con.cursor() as cur:
                    while counter < num_of_major:
                        print(counter)
                        major = driver.execute_script("return result.records[%s].json" % counter)
                        stackables = driver.execute_script("return result.records[%s].json.incentives.items" % counter)
                        print(major)
                        print(stackables)
                        sql = """
                                  select gmgl.xfm_vin_incentives('%s','%s','%s')
                                   """ % (vin, json.dumps(major), json.dumps(stackables))
                        cur.execute(sql)
                        counter += 1
            return 'pass'
        except TimeoutException:

            if i >= 3:
                print('Time out error')
                position = 'TIMEOUT EXCEPTION'
                params = vin
                exceptions(run_id, position, params, position)
                return 'fail'
            else:
                print ('Let us try again')
                i += 1
                driver.get('https://www.autopartners.net/apps/ioms/vinlookup/')
                enter_vin(driver,vin, run_id)
                continue



def new_search(driver, run_id):
    try:

        new_search_button = driver.find_element_by_xpath("//button[text()='New Search']")
        new_search_button.click()
        # print ('new search ')

    except Exception as error:
        position = 'New Search'
        # find line error info
        params = 'N/A'

        exceptions(run_id, position, params, error)

        return 'fail'


def is_complete(query, run_id):
    with db_cnx.pg() as con:
        with con.cursor() as cur:
            cur.execute(query)
            if cur.fetchone()[0] == 'TRUE':
                return 'pass'
            else:
                return 'fail'
                # position = 'ASSERT FAILURE. Not all vins are in the database. Something went wrong..'
                # # find line error info
                # params = 'N/A'
                # scraper_ops.log_error(run_id, position, position, 164, params, 'Query')


