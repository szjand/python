import psycopg2
import pyodbc

# 3/25/16
# this is WAY minimal for now
# the immediate problem was this:
#     i need to develop and test scpp against local host
#     but to do that i had to change the connection string for pg in all files
#     for every connection.  pain in the ass, and messy
#     this way, i change the pg connection string in one place, always refer
#     to pg connection as db_cnx.pg() and it's all good

# pyodbc seems like the best way for connection to ads, adsdb is a bit buggy, pyodbc supports python context manager
# http://stackoverflow.com/questions/33532699/how-can-i-connect-to-local-advantage-database-using-pyodbc-in-python
# http://mkleehammer.github.io/pyodbc/api.html


# production ####################################################
def pg():
    return  psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")


def sandbox_pg():
    return  psycopg2.connect("host='10.130.196.174' dbname='cartiva' user='rydell' password='cartiva'")

# localhost #####################################################
# def pg():
#     return psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")

# Win7 Apache PHP Postgres VM local host
def win_local_pg():
    return psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")

def ads_sco():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\67.135.158.12:6363\\advantage\\dpsVSeries\\dpsVSeries.add;'
                          'UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')

def ads_dps():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\67.135.158.12:6363\\advantage\\scotest\\sco.add;UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')

def ads_dds():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\67.135.158.12:6363\\advantage\\dds\\dds.add;UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')

def arkona_report():
    return pyodbc.connect('DRIVER={iSeries Access ODBC Driver};system=REPORT2.ARKONA.COM;uid=rydejon;pwd=fuckyou5')


def postgres_ubuntu():
    return  psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'")

def ads_dpsvseries():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\67.135.158.12:6363\\advantage\\dpsvseries\\dpsvseries.add;UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')

def mysql_shoretel_config():
    return pyodbc.connect("Provider=MSDASQL; DRIVER={MySQL ODBC 5.2a Driver};SERVER=192.168.100.10; "
                          "Port=4308;DATABASE=shoreware;USER=st_configread;PASSWORD=passwordconfigread;OPTION=3;")

def mysql_shoretel():
    return pyodbc.connect('SERVER=192.168.100.10;'
                          'DATABASE=shorewarecdr;'
                          'USER=st_cdrreport;'
                          'PASSWORD=passwordcdrreport;')

def drive_centric():
    return pyodbc.connect('Driver={SQL Server};'
                            'Server=52.22.117.38;'
                            'Database=leadcrumb;'
                            'uid=store-access;pwd=h%Mf4Ze5L#yQ9n*')
def drive_user():
    return pyodbc.connect('Driver={SQL Server};'
                            'Server=52.22.117.38;'
                            'Database=leadcrumb_daily_copy;'
                            'uid=store-access;pwd=h%Mf4Ze5L#yQ9n*')

def afton_local():
    return psycopg2.connect("host='localhost' dbname='Testing' user='postgres' password='cartiva'")

def ads_shoretel():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\67.135.158.12:6363\\advantage\\shoretel\\shoretel.add;UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')