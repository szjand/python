import socket
import scraper_ops


def scraper_start(task, task_command,task_location):
    ip = socket.gethostbyname(socket.gethostname())
    run_id = scraper_ops.log_start(task, task_command, task_location)
    return run_id


def scraper_end(run_id):
    scraper_ops.log_pass(run_id)


def scraper_vin(vin, run_id):
    run_id = scraper_ops.add_vin(run_id, vin)
    return run_id
