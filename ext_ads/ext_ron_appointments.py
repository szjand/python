# coding=utf-8
import csv
import db_cnx
import ops
import time

"""

"""
table_name = 'ads.ext_ron_appointments'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_ron_appointments.csv'

try:
    with db_cnx.ads_service_scheduler() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                from appointments
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
except Exception, error:
    ops.email_error('move_list update: ext_ads.ext_move_list.py', time.strftime("%m/%d/%Y"), error)
finally:
    pass
