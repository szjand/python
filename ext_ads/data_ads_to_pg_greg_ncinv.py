# coding=utf-8
import adsdb
import psycopg2
import psycopg2.extras
import csv
pgCon = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
# pgCon = psycopg2.connect("host='localhost' dbname='Test1' user='postgres' password='cartiva'")
pgCursor = pgCon.cursor()
# dds connection
adsCon = adsdb.connect(DataSource='\\\\172.17.196.12:6363\\advantage\\dpsvseries\\dpsvseries.add',
                       userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
                       TrimTrailingSpaces='TRUE')
schema = 'dps'

adsCursor = adsCon.cursor()
# adsCursor.execute("select trim(name) from system.tables where name "
#                   "in('day','dimcardealinfo','dimcustomer','dimsalesperson','dimvehicle','ncinv_vehicle_sale')")
adsCursor.execute("select trim(name) from system.tables where name in ('makemodelclassifications')")
numrows = adsCursor.rowcount
out_file = 'files/table_data.csv'
for x in xrange(0, numrows):
    row = adsCursor.fetchone()
    tableName = row[0]
    # print tableName
    tableCursor= adsCon.cursor()
    tableCursor.execute("select * from " + tableName)
    with open(out_file, 'wb') as f:
        csv.writer(f).writerows(tableCursor.fetchall())
    tableCursor.close()
    f.close()
    pgCursor = pgCon.cursor()
    pgCursor.execute("truncate " + schema + "." + tableName)
    pgCon.commit()
    io = open(out_file, 'r')
# opcodekey 3042 has an empty string for description which causes failure with not null constraint
#     pgCursor.copy_expert("""copy """ + schema + """.""" + tableName + """ from stdin with(format csv)""", io)
# fails with psycopg2.DataError: invalid byte sequence for encoding "UTF8": 0xa0  CONTEXT:  COPY dimopcode, line 8518
#     pgCursor.copy_expert("""copy """ + schema + """.""" + tableName +
#                          """ from stdin with(format csv, FORCE_NOT_NULL(description))""", io)
# http://stackoverflow.com/questions/4867272/invalid-byte-sequence-for-encoding-utf8
    if tableName == 'dimOpCode':
        pgCursor.copy_expert("""copy """ + schema + """.""" + tableName +
                             """ from stdin with(format csv, FORCE_NOT_NULL(description),
                             encoding 'windows-1251')""", io)
    else:
        pgCursor.copy_expert("""copy """ + schema + """.""" + tableName + """ from stdin with(format csv)""", io)
    pgCon.commit()
    io.close()
adsCursor.close()
adsCon.close()
pgCursor.close()
pgCon.close()
