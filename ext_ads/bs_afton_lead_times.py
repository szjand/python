# coding=utf-8
"""
    advantage query from E:\sql\postgresql\lisa_toc\ron_body_shop\afton_job_lead_times.sql
"""
import db_cnx
import csv

pg_con = None
ads_con = None
run_id = None
file_name = 'files/bs_afton_lead_times.csv'

try:
    with db_cnx.ads_bs() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
SELECT a.jobid, a.ronumber, b.fromts AS arrived, c.fromts AS released, d.fromts AS completed, e.fromts AS delivered
FROM (
		 SELECT bsj.jobid, bsj.customer, bsj.ronumber, bsv.vehicleyear, bsv.vehiclemake, bsv.vehiclemodel,bsv.vin
 		 FROM BSJobs bsj
 		 INNER JOIN BSVehicles bsv ON bsv.JobID = bsj.JobID
 		 WHERE bsj.jobid IN (
            SELECT jobid
			FROM bsjobs aa
			JOIN (
		 	    SELECT ro 	 				 									 
		 		FROM DDS.factrepairorder a
		 		JOIN dds.day b on a.closedatekey = b.datekey
		   		    AND b.thedate BETWEEN '01/01/2022' AND curdate()
				WHERE left(ro,2) = '18'
		 		GROUP BY ro)as bb on aa.ronumber = CAST(bb.ro as sql_integer))) a 
left JOIN ( -- arrived
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'RepairVehicleArrived_RepairVehicleArrived') AS b on a.jobid = b.jobid
left JOIN ( -- released earliest BETWEEN blueprint AND repair
		 SELECT jobid, MIN(fromts) AS fromts
		 FROM JobProgressStatuses
		 WHERE status in ('Release_Blueprint', 'Release_Repair')
		 GROUP BY jobid) AS c on a.jobid = c.jobid
left JOIN ( -- completed
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'JobStatus_JobCompleted') AS d on a.jobid = d.jobid
left JOIN ( -- delivered
		 SELECT * 
		 FROM JobProgressStatuses
		 WHERE status = 'Delivered_Delivered') AS e on a.jobid = e.jobid	
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate toc.bs_lead_times_1")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy toc.bs_lead_times_1 from stdin with csv encoding 'latin-1 '""", io)
        print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    print(error)
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()