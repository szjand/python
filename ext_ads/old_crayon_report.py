import db_cnx
import ops
import datetime
# 12/19/2017: replaces the delphi version of the same thing
#             needed to add the updating of dds.stgArkonaGLPTRNS as that has been superceded by fin.fact_gl
#             32 days worth of data
db2_con = None
ads_con = None
task = None


def result_iter(cursor, arraysize=1000):
    """An iterator that uses fetchmany to keep memory usage down"""
    while True:
        results = cursor.fetchmany(arraysize)
        if not results:
            break
        for result in results:
            yield result


def days_back_range(start, end, step):
    while start >= end:
        yield start
        start += step

# print ('start:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))
try:
    task = 'stgArkonaGLPTRNS'
    for no_of_days in days_back_range(32, 0, -1):
    # for no_of_days in days_back_range(2, 0, -1):
        with db_cnx.dealertrack_report2() as db2_con:
            with db2_con.cursor() as db2_cur:
                # same extract as luigi/fact_gl.XfmGlptrns
                sql = """
                    select
                      gtco#, gttrn#, gtseq#, gtdtyp, gttype, trim(gtpost), gtrsts, gtadjust, gtpsel,
                      trim(gtjrnl), gtdate, gtrdate, gtsdate, trim(gtacct), trim(REPLACE(gtctl#,'''','')), trim(gtdoc#),
                      trim(gtrdoc#), gtrdtyp, trim(REPLACE(gtodoc#,'''','')), trim(gtref#), trim(REPLACE(gtvnd#,'''','')),
                      -- replace(trim(upper(gtdesc)), ',', ' '),
                      'test', gttamt, gtcost, gtctlo,gtoco#
                    from rydedata.glptrns
                    where gtdate = current_date - {} days
                      and gtpost = 'Y'
                      and gtadjust <> 'Y'
                      and (gttrn# <> 4046331 and gtseq# <> 90)                 
                """ .format(no_of_days)
                db2_cur.execute(sql)
                with db_cnx.ads_dds() as ads_con:
                    with ads_con.cursor() as ads_cur:
                        sql = """
                            delete
                            from stgArkonaGLPTRNS
                            where gtdate = curdate() - {}
                        """.format(no_of_days)
                        ads_cur.execute(sql)
                        ads_con.commit()
                        for row in result_iter(db2_cur):
                            sql = """
                                insert into stgArkonaGLPTRNS
                                values ('%s',%s, %s,'%s','%s', '%s','%s','%s', '%s','%s','%s', '%s',
                                    '%s','%s', '%s','%s','%s', '%s','%s','%s', '%s','%s',%s, %s,'%s', '%s')
                            """ % (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9],
                                   row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18],
                                   row[19], row[20], row[21], row[22], row[23], row[24], row[25])
                            ads_cur.execute(sql)
    print ('finish stgArkonaGLPTRNS:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))

    task = 'updateGlDetailsTable'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """EXECUTE PROCEDURE udpateGlDetailsTable()"""
            ads_cur.execute(sql)
    # print ('finish updateGlDetailsTable:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))

    task = 'updateVehicleCostTable'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """EXECUTE PROCEDURE updateVehicleCostTable()"""
            ads_cur.execute(sql)
    # print ('finish updateVehicleCostTable:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))

    task = 'createGrossLogTable'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """EXECUTE PROCEDURE createGrossLogTable()"""
            ads_cur.execute(sql)
    # print ('finish createGrossLogTable:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))

except Exception, error:
    ops.email_error(task, 'old_crayon_report', error)
    print ('failed ' + task + ':' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))
finally:
    if db2_con:
        db2_con.close()
    if ads_con:
        ads_con.close()
