# coding=utf-8
import db_cnx
import csv

"""
this is a big one, 130M
see clean_file.py
"""
table_name = 'ads.ext_dim_ccc'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/dim_ccc.csv'

try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT ccckey,storecode,ro,line,complaint,cause,correction
                from dimccc
                where ccckey > 1081889
                  and not (complaint = 'N/A' AND correction = 'N/A')
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
