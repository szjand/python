import db_cnx
import datetime
# 12/18 this works with rydedata.glptrns -> dds.stg_Arkona_GLPDEPT
# try it with glptrns, took some fucking around, but works
db2_con = None
ads_con = None
table_name = 'stg_Arkona_GLPTRNS'


def resultIter(cursor, arraysize=1000):
    """An iterator that uses fetchmany to keep memory usage down"""
    while True:
        results = cursor.fetchmany(arraysize)
        if not results:
            break
        for result in results:
            yield result


def date_range(start, end, step):
    while start >= end:
        yield start
        start += step


print ('start:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))
try:
    for no_of_days in date_range(32, 0, -1):
        with db_cnx.dealertrack_report2() as db2_con:
            with db2_con.cursor() as db2_cur:
                # same extract as luigi/fact_gl.XfmGlptrns
                sql = """
                    select
                      gtco#, gttrn#, gtseq#, gtdtyp, gttype, trim(gtpost), gtrsts, gtadjust, gtpsel,
                      trim(gtjrnl), gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl#), trim(gtdoc#),
                      trim(gtrdoc#), gtrdtyp, trim(gtodoc#), trim(gtref#), trim(gtvnd#),
                      -- replace(trim(upper(gtdesc)), ',', ' '),
                      'test', gttamt, gtcost, gtctlo,gtoco#
                    from rydedata.glptrns
                    where gtdate = current_date - {} days
                      and gtpost = 'Y'
                      and gtadjust <> 'Y'
                """ .format(no_of_days)
                db2_cur.execute(sql)
                with db_cnx.ads_dds() as ads_con:
                    with ads_con.cursor() as ads_cur:
                        sql = """
                            delete
                            from stgArkonaGLPTRNS
                            where gtdate = curdate() - {}
                        """.format(no_of_days)
                        ads_cur.execute(sql)
                        ads_con.commit()
                        for row in resultIter(db2_cur):
                            sql = """
                                insert into stgArkonaGLPTRNS
                                values ('%s',%s, %s,'%s','%s', '%s','%s','%s', '%s','%s','%s', '%s',
                                    '%s','%s', '%s','%s','%s', '%s','%s','%s', '%s','%s',%s, %s,'%s', '%s')
                            """ % (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9],
                                   row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18],
                                   row[19], row[20], row[21], row[22], row[23], row[24], row[25])

                            ads_cur.execute(sql)
except Exception, error:
    print error
finally:
    if db2_con:
        db2_con.close()
    if ads_con:
        ads_con.close()
print ('finish:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))
