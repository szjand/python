﻿/*
-- ext_vehicle_items ----------------------------------------------------
DROP TABLE if exists ads.ext_vehicle_items;

CREATE TABLE ads.ext_vehicle_items
(
  vehicleitemid citext primary key,
  vin citext,
  bodystyle citext,
  "trim" citext,
  interiorcolor citext,
  exteriorcolor citext,
  engine citext,
  transmission citext,
  make citext,
  model citext,
  yearmodel citext,
  vinresolved boolean
);
create unique index on ads.ext_vehicle_items(vin);

alter table ads.ext_vehicle_items
add column hash text;

create unique index on ads.ext_vehicle_items(hash);

update ads.ext_vehicle_items x
set hash = y.hash
from (
  select vehicleitemid, md5(a::text) as hash
  from (
    select vehicleitemid, vin, bodystyle, trim, interiorcolor, 
      exteriorcolor, engine, transmission, make, model, yearmodel, 
      vinresolved
    from ads.ext_vehicle_items) a) y
where x.vehicleitemid = y.vehicleitemid;

-- ext_vehicle_items ----------------------------------------------------

-- tmp_ext_vehicle_items ----------------------------------------------------
DROP TABLE if exists ads.tmp_ext_vehicle_items;

CREATE TABLE ads.tmp_ext_vehicle_items
(
  vehicleitemid citext primary key,
  vin citext,
  bodystyle citext,
  "trim" citext,
  interiorcolor citext,
  exteriorcolor citext,
  engine citext,
  transmission citext,
  make citext,
  model citext,
  yearmodel citext,
  vinresolved boolean,
  hash text
);
create unique index on ads.tmp_ext_vehicle_items(vin);
create unique index on ads.tmp_ext_vehicle_items(hash);
-- tmp_ext_vehicle_items ----------------------------------------------------
  
delete from ops.ads_extract where task = 'tmp_ext_vehicle_items'

select * from ops.ads_extract where task = 'tmp_ext_vehicle_items'

select exteriorcolor, count(*) from ads.ext_vehicle_items group by exteriorcolor
-- set up an upsert test

select *
from ads.tmp_ext_vehicle_items
where vin like '%12311%'
union
select *
from ads.tmp_ext_vehicle_items
where exteriorcolor = 'siler'

select * from ads.ext_vehicle_items
where vin in ('KNADC123116060886','1D7HU18267S123115','1GKEV33797J134497','1G2WK52J52F123112','2GTEK19T621231138','1GCRKSE31BZ321446','2A4RR5DG1BR799338','1GNFK26319R202685')

-- "new" rows
delete from ads.ext_vehicle_items where vin like '%12311%';

-- changed rows to update
update ads.tmp_ext_vehicle_items
set exteriorcolor = 'silver'
where exteriorcolor = 'siler';


SELECT string_agg(column_name, ',')
FROM information_schema.columns
WHERE table_schema = 'ads'
  AND table_name   = 'ext_vehicle_items'
group by table_name  


insert into ads.ext_vehicle_items
-- changed rows
select a.vehicleitemid,a.vin,a.bodystyle,a.trim,a.interiorcolor,a.exteriorcolor,a.engine,
  a.transmission,a.make,a.model,a.yearmodel,a.vinresolved,a.hash
from ads.tmp_ext_vehicle_items a
inner join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
where a.hash <> b.hash
union
-- new rows
select a.vehicleitemid,a.vin,a.bodystyle,a.trim,a.interiorcolor,a.exteriorcolor,a.engine,
  a.transmission,a.make,a.model,a.yearmodel,a.vinresolved,a.hash
from ads.tmp_ext_vehicle_items a
left join ads.ext_vehicle_items b on a.vehicleitemid = b.vehicleitemid
where b.vehicleitemid is null 
on conflict (vehicleitemid)
do update 
set (vin,bodystyle,trim,interiorcolor,exteriorcolor,
  engine,transmission,make,model,yearmodel,vinresolved,hash) 
= (excluded.vin,excluded.bodystyle,excluded.trim,excluded.interiorcolor,
  excluded.exteriorcolor,excluded.engine,excluded.transmission,excluded.make,
  excluded.model,excluded.yearmodel,excluded.vinresolved,excluded.hash);

 */ 

-- ext_vehicle_inventory_items ----------------------------------------------------
DROP TABLE ads.ext_vehicle_inventory_items;

CREATE TABLE ads.ext_vehicle_inventory_items
(
  vehicleinventoryitemid citext primary key,
  tablekey citext,
  basistable citext,
  stocknumber citext,
  fromts timestamp with time zone,
  thruts timestamp with time zone,
  locationid citext,
  vehicleitemid citext,
  bookerid citext,
  currentpriority integer,
  owninglocationid citext
);
create unique index on ads.ext_vehicle_inventory_items(stocknumber);


alter table ads.ext_vehicle_inventory_items
add column hash text;

create unique index on ads.ext_vehicle_inventory_items(hash);

update ads.ext_vehicle_inventory_items x
set hash = y.hash
from (
  select vehicleinventoryitemid, md5(a::text) as hash
  from (
    select vehicleinventoryitemid,tablekey,basistable,stocknumber,fromts,thruts,
      locationid,vehicleitemid,bookerid,currentpriority,owninglocationid
    from ads.ext_vehicle_inventory_items) a) y
where x.vehicleinventoryitemid = y.vehicleinventoryitemid;

 
-- ext_vehicle_inventory_items ----------------------------------------------------

-- tmp_ext_vehicle_inventory_items ----------------------------------------------------
DROP TABLE if exists ads.tmp_ext_vehicle_inventory_items;

CREATE TABLE ads.tmp_ext_vehicle_inventory_items
(
  vehicleinventoryitemid citext primary key,
  tablekey citext,
  basistable citext,
  stocknumber citext,
  fromts timestamp with time zone,
  thruts timestamp with time zone,
  locationid citext,
  vehicleitemid citext,
  bookerid citext,
  currentpriority integer,
  owninglocationid citext,
  hash text
);
create unique index on ads.ext_vehicle_inventory_items(stocknumber);
create unique index on ads.tmp_ext_vehicle_inventory_items(hash);

-- tmp_ext_vehicle_inventory_items ----------------------------------------------------

delete from ads.ext_vehicle_inventory_items where stocknumber like '%6666%'

select currentpriority, count(*) from ads.tmp_ext_vehicle_inventory_items group by currentpriority order by count(*)

select * from ads.ext_Vehicle_inventory_items where currentpriority = 2000

update ads.ext_vehicle_inventory_items set currentpriority = 2000 where currentpriority = 425

insert into ads.ext_vehicle_inventory_items
-- changed rows
select a.vehicleinventoryitemid,a.tablekey,a.basistable,a.stocknumber,a.fromts,
  a.thruts,a.locationid,a.vehicleitemid,a.bookerid,a.currentpriority,a.owninglocationid,a.hash
from ads.tmp_ext_vehicle_inventory_items a
inner join ads.ext_vehicle_inventory_items b on a.vehicleInventoryItemID = b.vehicleInventoryItemID
where a.hash <> b.hash
union
-- new rows
select a.vehicleinventoryitemid,a.tablekey,a.basistable,a.stocknumber,a.fromts,a.thruts,
  a.locationid,a.vehicleitemid,a.bookerid,a.currentpriority,a.owninglocationid,a.hash
from ads.tmp_ext_vehicle_inventory_items a
left join ads.ext_vehicle_inventory_items b on a.vehicleInventoryItemID = b.vehicleInventoryItemID
where b.vehicleInventoryItemID is null
on conflict (vehicleInventoryItemID)
do update
set (tablekey,basistable,stocknumber,fromts,thruts,
  locationid,vehicleitemid,bookerid,currentpriority,owninglocationid,hash)
= (excluded.tablekey,excluded.basistable,excluded.stocknumber,excluded.fromts,excluded.thruts,
  excluded.locationid,excluded.vehicleitemid,excluded.bookerid,excluded.currentpriority,
  excluded.owninglocationid,excluded.hash);

                  
SELECT string_agg('excluded.' || column_name, ',')
FROM information_schema.columns
WHERE table_schema = 'ads'
  AND table_name   = 'ext_vehicle_inventory_items'
group by table_name 

vehicleinventoryitemid,tablekey,basistable,stocknumber,fromts,thruts,locationid,vehicleitemid,bookerid,currentpriority,owninglocationid,hash
a.vehicleinventoryitemid,a.tablekey,a.basistable,a.stocknumber,a.fromts,a.thruts,a.locationid,a.vehicleitemid,a.bookerid,a.currentpriority,a.owninglocationid,a.hash
excluded.vehicleinventoryitemid,excluded.tablekey,excluded.basistable,excluded.stocknumber,excluded.fromts,excluded.thruts,excluded.locationid,excluded.vehicleitemid,excluded.bookerid,excluded.currentpriority,excluded.owninglocationid,excluded.hash
select count(*) from ads.ext_vehicle_inventory_items