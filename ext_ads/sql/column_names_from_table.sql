﻿SELECT string_agg('excluded.' || column_name, ',')
FROM information_schema.columns
WHERE table_schema = 'ads'
  AND table_name   = 'ext_vehicle_items'
group by table_name  