﻿create table ads.ext_keyper(
  stock_number citext,
  created_date date,
  cabinet citext,
  first_name citext,
  last_name citext,
  transaction_date timestamptz,
  message citext,
  asset_status citext,
  issue_reason citext);

create index on ads.ext_keyper(stock_number); 
create index on ads.ext_keyper(transaction_date);
create index on arkona.xfm_inpmast(status);


select a.stock_number, a.first_name, a.last_name, transaction_date, asset_status, issue_reason
from ads.ext_keyper a
inner join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.current_row = true
  and b.status = 'I'
where a.asset_Status = 'out'
and transaction_date = (
  select max(transaction_date)
  from ads.ext_keyper
  where stock_number = a.stock_number
  group by stock_number)

select *
from arkona.xfm_inpmast
where inpmast_stock_number = 'G33665A'


-- a few with multiple current rows
select inpmast_Stock_number, current_row
from arkona.xfm_inpmast
where current_row = true
group by inpmast_Stock_number, current_row 
having count(*) > 1

-- not multiple current rows, multiple vins per stocknumber, each of which has a current row
select inpmast_vin, inpmast_stock_number, inpmast_key, row_from_date, row_thru_date, current_row
from arkona.xfm_inpmast
where inpmast_stock_number in (
  select inpmast_Stock_number
  from arkona.xfm_inpmast
  where current_row = true
  group by inpmast_Stock_number, current_row 
  having count(*) > 1)
order by inpmast_stock_number, inpmast_key












  .