import db_cnx
import csv

keyper_con = None
pg_con = None
file_name = 'files/keyper.csv'
try:
    with db_cnx.keyper() as keyper_con:
        with keyper_con.cursor() as keyper_cur:
            sql = """
                Select upper(a.name) as stock_number, convert(nvarchar(MAX), a.created_date, 101) as created_date,
                  coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') as cabinet, 
                  d.first_name, d.last_name, 
                  e.transaction_date, e.message, e.asset_status, f.name as issue_reason
                from dbo.asset a
                left join dbo.cabinet b on a.cabinet_id = b.cabinet_id
                left join dbo.system c on b.system_id = c.system_id
                left join asset_transactions e on a.asset_id = e.asset_id
                left join dbo.[user] d on e.user_id = d.user_id
                left join dbo.issue_reason f on e.issue_reason_id = f.issue_reason_id
                -- where a.name in ('G33832X','G33896A') 
                where a.deleted = 0
                  and e.transaction_Date > dateadd(DD, -365, getdate())
            """
            keyper_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(keyper_cur.fetchall())
        with db_cnx.pg() as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate ads.repair_order")
                with open(file_name, 'r') as io:
                    pg_cur.copy_expert("""copy ads.ext_keyper from stdin with csv encoding 'latin-1'""", io)
except Exception, error:
    print error
finally:
    pass
