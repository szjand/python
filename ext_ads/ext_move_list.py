# encoding=utf-8
"""

"""
import csv
import db_cnx
import ops
import time
file_name = 'files/ext_move_list.csv'
try:
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
              SELECT 
              vii.StockNumber,
              Coalesce(TimestampDiff(SQL_TSI_HOUR, vm.FromTS, Current_Timestamp() ),0) AS HoursSinceRequest,
              vii.CurrentPriority,  
              (SELECT LastName 
                FROM People
                WHERE PartyID = vm.SchedulerID) AS RequestedBy,	  
              coalesce(a.keystatus, 'Unknown') AS KeyStatus,   		
              (SELECT
                  CASE 
                    WHEN l.DetailRequired = TRUE  
                      THEN trim(o.Name) + ': ' + trim(l.LocationShortName) + ' - ' + trim(v.LocationDetail)
                    ELSE trim(o.Name) + ': ' + trim(l.LocationShortName) + 
                      CASE 
                        WHEN l.LocationDescription IS NOT NULL THEN  ' - ' + trim(l.LocationDescription) 
                        ELSE '' 
                      END +
                      CASE 
                        WHEN (v.LocationDetail IS NOT NULL AND v.LocationDetail <> '') 
                          THEN ' - ' + TRIM(v.LocationDetail) 
                        ELSE '' 
                      END 
                  END AS CurrentLocation
                FROM VehicleItemPhysicalLocations v
                LEFT JOIN LocationPhysicalVehicleLocations l ON l.PhysicalVehicleLocationID = v.PhysicalVehicleLocationID
                LEFT JOIN Organizations o ON o.PartyID = l.LocationPartyID
                WHERE v.ThruTS IS NULL  
                      AND v.VehicleInventoryItemID = vm.VehicleInventoryItemID) AS CurrentLocation, 	
                CASE 
                  WHEN l.DetailRequired = TRUE  
                    THEN trim(o.Name) + ': ' + trim(l.LocationShortName) + ' - ' + trim(vm.MoveToDetails)
                  ELSE trim(o.Name) + ': ' + trim(l.LocationShortName) + 
                    CASE 
                      WHEN l.LocationDescription IS NOT NULL 
                        THEN  ' - ' + trim(l.LocationDescription) 
                      ELSE ''
                    END +
                    CASE 
                      WHEN (vm.MovetoDetails IS NOT NULL AND vm.MoveToDetails <> '') 
                        THEN ' - ' + TRIM(vm.MoveToDetails) 
                      ELSE '' 
                    END 
                  END AS MoveToLocation,
              vi.ExteriorColor AS color,
              Coalesce(Trim(vi.YearModel), '') + ' ' + Coalesce(Trim(vi.Make), '') + ' ' + 
                Coalesce(Trim(vi.Model), '') + ' ' + 
                Coalesce(Trim(vi.Trim),'')  AS VehicleDescription            
            FROM  VehiclesToMove vm
            INNER join VehicleInventoryItems vii
             on vm.VehicleInventoryItemID = vii.VehicleInventoryItemID
            INNER JOIN VehicleItems vi
              ON vi.VehicleItemID = vii.VehicleItemID
            INNER JOIN MakeModelClassifications mmc
              on mmc.Make = vi.Make
              AND mmc.Model = vi.Model
              AND mmc.ThruTS IS NULL
            LEFT JOIN LocationPhysicalVehicleLocations l ON l.PhysicalVehicleLocationID = vm.MoveToLocation
            LEFT JOIN Organizations o ON o.PartyID = l.LocationPartyID
            LEFT JOIN keyperkeystatus a ON vii.stocknumber = a.stocknumber
            WHERE vm.ThruTS IS NULL
            ORDER BY vii.CurrentPriority DESC;            
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_move_list")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_move_list from stdin with csv encoding 'latin-1'""", io)
except Exception, error:
    ops.email_error('move_list update: ext_ads.ext_move_list.py', time.strftime("%m/%d/%Y"), error)
finally:
    pass