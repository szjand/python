# encoding=utf-8
"""
10/18/22  time to replace walk feedback emails, they are too intermittent coming out of delphi
"""
import csv
import db_cnx
import ops
import time
file_name = 'files/ext_vehicle_walk_feedback.csv'
try:
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                INSERT INTO vehicle_walk_feedback (walk_date,walker,evaluator,stock_number,vin,model_year,make,
                    model,trim_level,body_style,miles,notes,sent,copied_to_pg,prior_owner,evaluator_notes)
                SELECT cast(a.VehicleWalkTS AS sql_date) AS walk_date, c.fullname AS walker, d.fullname AS evaluator,
                    e.stocknumber, f.vin, f.yearmodel, f.make, f.model, f.TRIM, f.bodystyle, k.value AS miles, 
                    g.notes AS walk_feedback,
                    false, false,
                    l.prior_owner,
                    m.notes AS evaluator_notes
                FROM VehicleWalks a
                JOIN VehicleEvaluations b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
                JOIN people c on a.vehiclewalkerid = c.partyid
                JOIN people d on b.vehicleevaluatorid = d.partyid
                JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
                JOIN VehicleItems f on e.VehicleItemID = f.VehicleItemID
                JOIN VehicleInventoryItemNotes g on a.VehicleWalkid = g.tablekey
                  AND g.subcategory = 'VehicleWalk_EvaluatorFeedback'
                LEFT JOIN VehicleItemMileages k on b.VehicleEvaluationID = k.tablekey
                LEFT JOIN (
                    select VehicleItemID, trim(trim(firstname) + ' ' + trim(lastname)) AS prior_owner
                    FROM VehicleItemOwnerInfo aa 
                    where fromts = (
                      SELECT MAX(fromts)
                  	  FROM VehicleItemOwnerInfo
                  	  WHERE VehicleItemID = aa.VehicleItemID)) l on f.VehicleItemID = l.VehicleItemID		
                LEFT JOIN VehicleInventoryItemNotes m on e.VehicleInventoryItemID = m.VehicleInventoryItemID 
				    AND m.category = 'VehicleEvaluation'	
				    AND m.subcategory = 'VehicleEvaluation_ReconNote'													
                WHERE CAST(a.VehicleWalkTS AS sql_date) = curdate()
                    AND NOT EXISTS (
                        SELECT 1
                        FROM vehicle_walk_feedback
                        WHERE stock_number = e.stocknumber);
            """
            ads_cur.execute(sql)
            """
            the issue is i only want to copy the walks into pg once, but i am going to scrape the data multiple times 
            a day, could handle it with a timestamp
            so, what i did, add a field copied_to_pg that is set to false when the record is inserted into
            vehicle_walk_feedback, but then set to true after writing the output of vehicle_walk_feedback to file
            for copying to pg
            """
            sql = """
                select * 
                from vehicle_walk_feedback
                where walk_date = curdate()
                  and copied_to_pg = false;
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur)
            sql = """
                update vehicle_walk_feedback
                set copied_to_pg = true
                where walk_date = curdate()
                  and copied_to_pg = false;
            """
            ads_cur.execute(sql)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            # pg_cur.execute("truncate ads.ext_vehicle_walk_feedback")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_vehicle_walk_feedback from stdin with csv encoding 'latin-1'""", io)
except Exception, error:
    print(error)
finally:
    pass
