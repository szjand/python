# coding=utf-8
import adsdb
import psycopg2
import psycopg2.extras
import csv
pg_con = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
# pg_con = psycopg2.connect("host='localhost' dbname='Test1' user='postgres' password='cartiva'")
pg_cursor = pg_con.cursor()
# dds connection
ads_con = adsdb.connect(DataSource='\\\\172.17.196.12:6363\\advantage\\dds\\dds.add',
                        userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
                        TrimTrailingSpaces='TRUE')

ads_cursor = ads_con.cursor()
# adsCursor.execute("select trim(name) from system.tables where name "
#                   "in('day','dimcardealinfo','dimcustomer','dimsalesperson','dimvehicle','ncinv_vehicle_sale')")
# numrows = adsCursor.rowcount
out_file = 'files/table_data.csv'

ads_cursor.execute("select * from dimopcode")
with open(out_file, 'wb') as f:
    csv.writer(f).writerows(ads_cursor.fetchall())

pg_cursor.execute("truncate dds.dimopcode")
pg_con.commit()
io = open(out_file, 'r')
# opcodekey 3042 has an empty string for description which causes failure with not null constraint
#     pgCursor.copy_expert("""copy """ + schema + """.""" + tableName + """ from stdin with(format csv)""", io)
# fails with psycopg2.DataError: invalid byte sequence for encoding "UTF8": 0xa0  CONTEXT:  COPY dimopcode, line 8518
#     pgCursor.copy_expert("""copy """ + schema + """.""" + tableName +
#                          """ from stdin with(format csv, FORCE_NOT_NULL(description))""", io)
# http://stackoverflow.com/questions/4867272/invalid-byte-sequence-for-encoding-utf8
pg_cursor.copy_expert("""copy dds.dimopcode from stdin with(format csv, FORCE_NOT_NULL(description),
                     encoding 'windows-1251')""", io)

pg_con.commit()
io.close()

ads_cursor.execute("select * from factrepairorder where cast(rocreatedts as sql_date) > curdate() - 8")
with open(out_file, 'wb') as f:
    csv.writer(f).writerows(ads_cursor.fetchall())
ads_cursor.close()

pg_cursor.execute("delete from dds.factrepairorder where rocreatedts::date > current_date - 8")
pg_con.commit()

io = open(out_file, 'r')
pg_cursor.copy_expert("""copy dds.factrepairorder from stdin with(format csv)""", io)

pg_con.commit()
io.close()

ads_cursor.close()
ads_con.close()
pg_cursor.close()
pg_con.close()
