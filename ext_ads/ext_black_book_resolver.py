# encoding=utf-8
"""

"""
import csv
import db_cnx
file_name = 'files/ext_black_book_resolver.csv'
try:
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                select vehicleitemid,groupnumber,vin,vinyear,uvc,expiredts
                from blackbookresolver;       
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_black_book_resolver")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_black_book_resolver from stdin with csv encoding 'latin-1'""", io)
except Exception, error:
    print(error)
finally:
    pass