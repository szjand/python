# coding=utf-8
import db_cnx
import csv

pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_tp_techs.csv'

try:
    with db_cnx.ads_sco() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                from tptechs
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_tp_techs")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_tp_techs from stdin with csv encoding 'latin-1 '""", io)
        print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    print(error)
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()