# coding=utf-8
import csv
import db_cnx
import ops
import time

"""

"""
# !!!!!!!!!!!!!!!!!!!!!!!!!!!! moved to ads_for_luigi uc_inventory.py
table_name = 'ads.ext_vehicle_walks'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_vehicle_walks.csv'

try:
    with db_cnx.ads_dpsvseries() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                from vehiclewalks
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
except Exception, error:
    ops.email_error('ext_vehicle_walks', time.strftime("%m/%d/%Y"), error)
    print(error)
finally:
    print ('passssssssssssssssssssssssssss')