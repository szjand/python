# coding=utf-8
"""
3/2/17:
    feed back from weekly open ro list pointed me to an ro that should not have been on the list
    so i proceeded to update ALL THE FUCKING RO close and finalclose dates
    looks like panic stop limited the damage to ros from 2012 and 2013
    this is the fix
    download from sdprhdr, a year at a time into dds.zjon_ro_fuck_up
    update factrepairorder
    the uploading to the ads table in the script took too fucking long
    extract the arkona data to a csv file, use arc to import the data
"""
import db_cnx
import csv

ads_con = None
db2_con = None
file_name = 'files/ro_fuck_up.csv'
try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                Select ro_number,
                  case
                    when close_date = 0 then date('9999-12-31')
                    else
                      cast(left(digits(close_date), 4) || '-' || substr(digits(close_date), 5, 2)
                        || '-' || substr(digits(close_date), 7, 2) as date)
                    end as close_date,
                  case
                    when final_close_date = 0 then date('9999-12-31')
                    else
                    cast(left(digits(final_close_date), 4) || '-' || substr(digits(final_close_date), 5, 2)
                      || '-' || substr(digits(final_close_date), 7, 2) as date)
                    end as final_close_date
                from RYDEDATA.SDPRHDR
                where open_date between 20130000 and 20131231
                """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur.fetchall())
            # csv_data = csv.reader(file(file_name))
            # for row in csv_data:
            #     sql = """
            #         insert into zjon_ro_fuck_up
            #         values ('%s','%s','%s')
            #     """ % (row[0], row[1], row[2])
            #     with db_cnx.ads_dds() as ads_con:
            #         with ads_con.cursor() as ads_cur:
            #             ads_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()
    if db2_con:
        db2_con.close()
