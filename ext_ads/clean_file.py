import csv
import string
import unicodedata

# trying to save dimCCC in postgres thru errors on lines with non printable charaters

# got to move on to something else for now, but, downloaded the problem record (ccckey 810643) into a small
# file and it fucking inserted into the table just fine

# experiment with a small file
# https://stackoverflow.com/questions/15129567/csv-writer-writing-each-character-of-word-in-separate-column-cell/27065792#27065792
# seems goofy, but this finally worked: http://stanford.edu/~mgorkove/cgi-bin/rpython_tutorials/Writing_Data_to_a_CSV_With_Python.php
file_name = 'files/dim_ccc.csv'
new_file_name = 'files/new_dim_ccc.csv'
with open (file_name, 'r') as f:
    for line in f:
        with open (new_file_name, 'a') as csv:
            # csv.writer(j).writerow("".join(i for i in line if ord(i)<128))
            # csv.writer(j).writerows(''.join(x if x in string.printable else '' for x in line))
            # new_line = ''.join(filter(lambda x: x in string.printable, line))
            new_line = filter(lambda x: x in string.printable, line)
            # print(str([new_line]))
            csv.write(new_line)
