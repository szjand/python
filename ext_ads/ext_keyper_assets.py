# coding=utf-8
import db_cnx
import csv

"""

"""
table_name = 'ads.keyper_creation_dates'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/keyper_ssets.csv'

try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                FROM (
                  SELECT stocknumber, created_date
                  FROM ext_keyper_assets
                  WHERE created_date > '01/01/2016'
                  GROUP BY stocknumber, created_date) a
                WHERE stocknumber NOT IN (
                  SELECT stocknumber
                  FROM (
                    SELECT stocknumber, created_date
                    FROM ext_keyper_Assets
                    WHERE created_date > '01/01/2016'
                    GROUP BY stocknumber, created_date) a
                  GROUP BY stocknumber HAVING COUNT(*) > 1)
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
