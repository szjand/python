﻿alter table pdq.writers
add column store citext;

update pdq.writers
set store = case when left(employee_number, 1) = '1' then 'RY1' else 'RY2' end;

-- version 3, base join on writers, so at the beginning of the month 
-- all writers display even if they havve 0 stats

CREATE OR REPLACE FUNCTION pdq.get_writer_stats()
  RETURNS TABLE(store citext, writer_number citext, writer citext, employee_number citext, lof bigint, rotate numeric, "air filters" numeric, "cabin filters" numeric, batteries numeric, bulbs numeric, wipers numeric, "lof pacing" bigint) AS
$BODY$  
/*
this is run after midnight for the previous day, so "current date" = current_date - 1
add totals, penetration and lof pacing per andrew
select * from pdq.get_writer_stats();
*/
declare
  _from_date date := (
    select biweekly_pay_period_start_date
    from dds.dim_date
    where the_Date = current_date - 1);  
  _thru_date date := (
    select biweekly_pay_period_end_date
    from dds.dim_date
    where the_Date = current_date - 1);    
begin
  return query  
select b.*, b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period as "lof pacing"
from (
  select a.store, a.writer_number, (a.last_name || ', ' || a.first_name)::citext as writer, a.employee_number,
    count(*) filter (where b.category = 'LOF') as LOF,
    count(*) filter (where b.category = 'Rotate') as rotate,
    count(*) filter (where b.category = 'Air Filters') as "air filters",
    count(*) filter (where b.category = 'Cabin Filters') as "cabin filters",
    count(*) filter (where b.category = 'Batteries') as batteries,
    count(*) filter (where b.category = 'Bulbs') as bulbs,
    count(*) filter (where b.category = 'Wipers') as wipers
  from pdq.writers a  
  left join pdq.ros b on a.employee_number = b.employee_number
    and b.the_date between '09/29/2019' and '10/12/2019'
  group by a.store, a.writer_number, a.last_name || ', ' || a.first_name, a.employee_number) b 
join dds.working_days c on c.department = 'pdq'
  and c.the_date = current_date - 1  
union
select b.store, b.writer_number, b.writer, b.employee_number,
  b.lof, 
  case when b.lof = 0 then 0 else round(100.0 * b.rotate/b.lof, 1) end,
  case when b.lof = 0 then 0 else round(100.0 * b."air filters"/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b."cabin filters"/b.lof, 1) end,
  case when b.lof = 0 then 0 else round(100.0 * b.batteries/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b.bulbs/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b.wipers/b.lof, 1) end,
  b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period as "lof pacing"
  from (
    select a.store, 'Total LOF & Penetration'::citext as writer_number,
      null::citext as writer ,null::citext as employee_number,
      count(*) filter (where a.category = 'LOF') as LOF,
      count(*) filter (where a.category = 'Rotate') as rotate,
      count(*) filter (where a.category = 'Air Filters') as "air filters",
      count(*) filter (where a.category = 'Cabin Filters') as "cabin filters",
      count(*) filter (where a.category = 'Batteries') as batteries,
      count(*) filter (where a.category = 'Bulbs') as bulbs,
      count(*) filter (where a.category = 'Wipers') as wipers
    from pdq.ros a
    where a.the_date between '09/29/2019' and '10/12/2019'
    group by a.store) b
  join dds.working_days c on c.department = 'pdq'
    and c.the_date = current_date - 1
order by store, writer nulls last;
end;
$BODY$
  LANGUAGE plpgsql;

    