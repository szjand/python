﻿--< new writer -------------------------------------------------------------

the whole pdq process needs to be refactored to remove the dependency on advantage,
repair order fact now in pg

-------------------------------------
--< 10/26/20
-------------------------------------
Would you be able to add Tim Durand # 533 to this list please. Honda Store

Ryan Shroyer

-- using dds.service_writer & arkona.ext_pymast
insert into pdq.writers (first_name,last_name,employee_number,writer_number,writer_key,store,active)
select b.employee_first_name, b.employee_last_name, b.pymast_employee_number, 
  a.writer_number, a.service_writer_key,
  case left(b.pymast_employee_number, 1)
    when '1' then 'RY1'
    when '2' then 'RY2'
  end, true
from dds.dim_service_writer a
join arkona.ext_pymast b on b.pymast_employee_number = a.employee_number
  and b.active_code <> 'T'
where a.writer_number in ('533')
  and a.current_row;
  
-------------------------------------
--/> 10/26/20
-------------------------------------



-------------------------------------
--< 09/22/20
-------------------------------------
Can I get Harrison Green and Austin Knutson added to the writer stats please?

Thank you,
Dayton Marek 


harrison already exists in pdq.writers
select * from pdq.writers where writer_number = '475'
just need to activate him
update pdq.writers
set active = true
where writer_number = '475';



insert into pdq.writers (first_name,last_name,employee_number,writer_number,writer_key,store,active)
select b.employee_first_name, b.employee_last_name, b.pymast_employee_number, 
  a.writernumber, a.servicewriterkey,
  case left(b.pymast_employee_number, 1)
    when '1' then 'RY1'
    when '2' then 'RY2'
  end, true
from ads.ext_dim_service_writer a
join arkona.xfm_pymast b on b.pymast_employee_number = a.employeenumber
  and b.current_row
  and b.active_code <> 'T'
where a.writernumber in ('509')
  and a.currentrow;

select * from pdq.writers
-------------------------------------
--/> 09/22/20
-------------------------------------



-------------------------------------
--< 08/11/20
-------------------------------------
Please add Anthony Erickson to this report…  advisor 534

Thanks Jon!

Joel T. Dangerfield

select * from dds.dim_service_writer where writer_name like 'erickson%'

insert into pdq.writers (first_name,last_name,employee_number,writer_number,writer_key,store,active)
select b.employee_first_name, b.employee_last_name, b.pymast_employee_number, 
  a.writernumber, a.servicewriterkey,
  case left(b.pymast_employee_number, 1)
    when '1' then 'RY1'
    when '2' then 'RY2'
  end, true
from ads.ext_dim_service_writer a
join arkona.xfm_pymast b on b.pymast_employee_number = a.employeenumber
  and b.current_row
  and b.active_code <> 'T'
where a.writernumber in ('534')
  and a.currentrow;
-------------------------------------
--/> 08/11/20
-------------------------------------
-------------------------------------
--< 08/07/20
-------------------------------------
Good morning Jon,

Could I ever have Liz Howard added to this list please? 

Thank you,
Dayton Marek 

select * from dds.dim_service_writer where writer_name like 'howard%'

insert into pdq.writers (first_name,last_name,employee_number,writer_number,writer_key,store,active)
select b.employee_first_name, b.employee_last_name, b.pymast_employee_number, 
  a.writernumber, a.servicewriterkey,
  case left(b.pymast_employee_number, 1)
    when '1' then 'RY1'
    when '2' then 'RY2'
  end, true
from ads.ext_dim_service_writer a
join arkona.xfm_pymast b on b.pymast_employee_number = a.employeenumber
  and b.current_row
  and b.active_code <> 'T'
where a.writernumber in ('745')
  and a.currentrow;

-------------------------------------
--/> 08/07/20
-------------------------------------


-------------------------------------
--< 07/21/20
-------------------------------------
Good morning Jon,

Would we ever be able to remove Harris green and Stacy Piche off the list? And then add Kyle Murdoch? That would be awesome!

Thank you,
Dayton Marek 

update pdq.writers
set active = false
-- select * from pdq.writers
where last_name in ('piche','green');

select * from dds.dim_service_writer where writer_name like 'murdoch%'

insert into pdq.writers (first_name,last_name,employee_number,writer_number,writer_key,store,active)
select b.employee_first_name, b.employee_last_name, b.pymast_employee_number, 
  a.writernumber, a.servicewriterkey,
  case left(b.pymast_employee_number, 1)
    when '1' then 'RY1'
    when '2' then 'RY2'
  end, true
from ads.ext_dim_service_writer a
join arkona.xfm_pymast b on b.pymast_employee_number = a.employeenumber
  and b.current_row
  and b.active_code <> 'T'
where a.writernumber in ('605')
  and a.currentrow;
-------------------------------------
--/> 07/21/20
-------------------------------------
-------------------------------------
--< 05/26/20
-------------------------------------
Good morning Jon!

I was wondering if you would be able to add Vander Dean and Kelsey Kozel to the writers list? They are new writers and will be up and running by themselves very soon!

Thank you,
Dayton Marek 

insert into pdq.writers (first_name,last_name,employee_number,writer_number,writer_key,store,active)
select b.employee_first_name, b.employee_last_name, b.pymast_employee_number, 
  a.writernumber, a.servicewriterkey,
  case left(b.pymast_employee_number, 1)
    when '1' then 'RY1'
    when '2' then 'RY2'
  end, true
from ads.ext_dim_service_writer a
join arkona.xfm_pymast b on b.pymast_employee_number = a.employeenumber
  and b.current_row
  and b.active_code <> 'T'
where a.writernumber in ('503','504')
  and a.currentrow;

-------------------------------------
--/> 05/26/20
-------------------------------------


02/20/20
Can you remove Roger Stewart from the reporting and add Paul Behm writer number #502 please?
Andrew Neumann

select * from ads.ext_dim_service_writer where name like 'behm%'

insert into pdq.writers (first_name,last_name,employee_number,writer_number,writer_key,store,active)
select b.employee_first_name, b.employee_last_name, b.pymast_employee_number, 
  a.writernumber, a.servicewriterkey,
  case left(b.pymast_employee_number, 1)
    when '1' then 'RY1'
    when '2' then 'RY2'
  end, true
from ads.ext_dim_service_writer a
join arkona.xfm_pymast b on b.pymast_employee_number = a.employeenumber
  and b.current_row
--   and b.distrib_code = 'PDQW'
  and b.active_code <> 'T'
  and b.pymast_employee_number not in ('197560')
where a.writernumber in ('502')
  and a.currentrow;

02/18/2020
Please add the following to Nissan/honda ESR list

Ryan Shroyer                     advisor # 488
Frances Johnson               advisor # 532

Thank you,

Joel

-- they both had RY1 employeenumbers in ads.ext_dim_service_writer
select * from ads.ext_dim_service_writer where writernumber in ('488','532')

update ads.ext_dim_service_writer
set employeenumber = '2126055'
where servicewriterkey = 910;

update ads.ext_dim_service_writer
set employeenumber = '259756'
where servicewriterkey = 965; 

-- 1/21/20 from andrew
Jon, 
Can you please add Spencer Morgan to the list of ESR’s (Service Writers) for PDQ please? We moved him and would like his numbers to start reflecting on the daily report. TIA
select *
from jeri.service_writers
where writer_name like 'morgan%'


1/2/20 from ryan shroyer
Can I remove Jared (607)   from this list.  Add Elliot Beauvais   530      Add Reyes Lara  531 


10/1/19
Wyatt Olson <wolson@rydellcars.com>
Could you please add writer 437  Roger Stewart he is a new writer that just started. Thank you

10/10/19
Can we add Tasha Lindman #500 and Shiv Adhikari #409 to the list please thank you


alter table pdq.writers  alter store set not null;
-- select * from pdq.writers order by  left(employee_number, 1), last_name

insert into pdq.writers (first_name,last_name,employee_number,writer_number,writer_key,store,active)
select b.employee_first_name, b.employee_last_name, b.pymast_employee_number, 
  a.writernumber, a.servicewriterkey,
  case left(b.pymast_employee_number, 1)
    when '1' then 'RY1'
    when '2' then 'RY2'
  end, true
from ads.ext_dim_service_writer a
join arkona.xfm_pymast b on b.pymast_employee_number = a.employeenumber
  and b.current_row
--   and b.distrib_code = 'PDQW'
  and b.active_code <> 'T'
  and b.pymast_employee_number not in ('197560')
where a.writernumber in ('488','532')
  and a.currentrow;



--/> new writer -------------------------------------------------------------


--< remove writer -------------------------------------------------------------

02/20/20
Can you remove Roger Stewart from the reporting and add Paul Behm writer number #502 please?
Andrew Neumann

update pdq.writers
set active = false
-- select * from pdq.writers
where last_name = 'stewart';


-- 1/7/20 -----------------------------------
Scott noticed that Roger Stewart was here yesterday 1/6/19 all day and no oil changes are showing up under his name/number. Is there something you can do to fix that issue please?
Also, Shiv Adhikari can be removed as he no longer works here.
Andrew

select * 
from pdq.writers
where writer_number = '409'

update pdq.writers
set active = false
where writer_number = '409';

-- 1/2/20 from ryan shroyer -----------------------------------
Can I remove Jared (607)   from this list.  Add Elliot Beauvais   530      Add Reyes Lara  531 

delete
-- select * 
from pdq.writers
where writer_number = '607';

-- cant delete him due to fk in pdq.ros;
-- so, lets alter the table
-- add a field: active boolean
-- deleting becomes changing active to false.
-- then need to update queries using this table.

alter table pdq.writers
add column active boolean;

update pdq.writers
set active = true;

alter table pdq.writers
alter column active set not null;

update pdq.writers
set active = false
where writer_number = '607';

select * from pdq.writers where active;

select * from pdq.writers where not active;

-- search the pdq schema
select 
	routine_catalog AS DatabaseName
	,routine_schema AS SchemaName
	,routine_name AS FunctionName
	,routine_type AS ObjectType
-- select routine_definition
from information_schema.routines 
where specific_schema = 'pdq'
  and routine_definition like '%pdq.writers%'

-- search all schemas
select 
	routine_catalog AS DatabaseName
	,routine_schema AS SchemaName
	,routine_name AS FunctionName
	,routine_type AS ObjectType
from information_schema.routines 
where routine_definition like '%pdq.writers%'

--/> remove writer -------------------------------------------------------------