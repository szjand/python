﻿pdq gross
from .../flightplan/sql/flightplan_data_2_include_honda.sql

pdq gross is at line 2484
prerequisites


11/24 - 12/07



-- line 2068
-- insert into fp.fixed_gross_details
drop table if exists gross;
create temp table gross as
select b.the_date, d.store, a.control, d.department,
  d.page, d.line, d.col, d.account, e.journal_code,
  sum(-a.amount) as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between '11/24/2019' and current_date
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account
  and d.page = 16 and d.line between 21 and 61
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code;   

select * from gross order by store, page, line

-- insert into fp.parts_split

  select 

    coalesce((sum(amount * .5) filter (where account in ('147800','167800')))::integer, 0) as pdq

  from gross
  where store = 'RY1'

    

select the_Date, sum(amount)::integer as amount
from gross
where store = 'RY1'
  and department = 'quick lane'
  and line between 21 and 33
group by the_date


select * from fp.gl_accounts

-- this is giving me labor only

select b.the_date, d.store, a.control, d.department,
  d.page, d.line, d.col, d.account, e.journal_code,
  sum(-a.amount) as amount
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between '11/24/2019' and current_date
join fin.dim_account c on a.account_key = c.account_key
join fp.gl_accounts d on c.account = d.account

  and d.store = 'RY1'
  and d.department = 'quick lane'
  and d.page = 16
  and d.line between 21 and 33
  and d.col = 1
  
--   and d.page = 16 and d.line between 21 and 61
join fin.dim_journal e on a.journal_key  = e.journal_key    
where a.post_status = 'Y'
group by b.the_date, a.control, d.store, d.department, d.page, d.line, d.col,
  d.account, e.journal_code
order by control  


select *
from fp.flightplan
where the_date = current_date - 1
order by seq

-- this matches the total of sales in RO Gross Profit (77) and parts & labor in RO Total(60)
select a.post_status, a.amount, b.*
-- select sum(a.amount) -- filter (where a.amount < 0)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'Sale'
where a.control = '19358403'
  and a.post_status = 'Y'
order by a.amount  
order by account

-- ok, need the relevant ros

select *
from (
select a.ro
from ads.ext_fact_repair_order a
join dds.dim_date b on a.closedatekey = b.date_key
  and b.the_date between '11/24/2019' and '12/07/2019'
join ads.ext_dim_Service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetypecode = 'QL'  
group by a.ro) aa
full outer join (
select ro
from pdq.ros
where the_date between '11/24/2019' and '12/07/2019'
group by ro) bb on aa.ro = bb.ro


select *
from ads.ext_fact_repair_order
where ro = '19358403'


ok, getting ros not written by pdq writer, multiple lines not all pdq
maybe refine sales with pdq accounts
eg 
16372976

select a.post_status, a.amount, b.*
-- select sum(a.amount) -- filter (where a.amount < 0)
from fin.fact_gl a
join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'Sale'
where a.control = '16372976'
  and a.post_status = 'Y'

select *
from fin.dim_account
where description like '%PDQ%'  
  and account_type = 'sale'

drop table if exists pdq.sale_accounts;
create table pdq.sale_accounts (
  account citext primary key,
  account_key integer not null,
  department_code citext not null,  
  store citext not null,
  description citext not null);
create index on pdq.sale_accounts(account);
create index on pdq.sale_acounts(account_key);
insert into pdq.sale_accounts  
select account, account_key,department_code,store,description
from fin.dim_account
-- trying to get the honda accounts for parts
where (description like '%PDQ%' or description like '%QL%' or (store_code = 'RY2' and description like '%OIL & GREASE%') ) 
  and account_type = 'sale'
  and row_thru_date > current_Date;
comment on table pdq.sale_accounts is 'subset of general ledger accounts where account type is Sale and description contains PDQ';  

drop table if exists pdq.sales_ros;
create table pdq.sales_ros (
    ro citext primary key,
    close_date date not null);
comment on table pdq.sales_ros is 'ro and close date for ros with any line where service type is QL, based on close date';    
insert into pdq.sales_ros
select a.ro, b.the_date
from ads.ext_fact_repair_order a
join dds.dim_date b on a.closedatekey = b.date_key
  and b.the_date between '11/24/2019' and '12/07/2019'
join ads.ext_dim_Service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetypecode = 'QL'  
group by a.ro, b.the_date
on conflict(ro)
do nothing ;

-- no parts for honda
-- looks "ok" now
select ro, close_date, 
  sum(coalesce(a.amount, 0)) filter (where department_code = 'QL') as labor,
  sum(coalesce(a.amount, 0)) filter (where department_code = 'PD') as parts
from fin.fact_gl a
join pdq.sale_accounts b on a.account_key = b.account_key
join pdq.sales_ros c on a.control = c.ro
group by ro, close_date
order by ro 


drop table if exists pdq.ro_sales cascade;
create table pdq.ro_sales (
  store_code citext not null,
  ro citext primary key,
  close_date date not null,
  labor numeric(8,2) not null default '0',
  parts numeric(8,2) not null default '0',
  total_sales numeric(8,2) not null default '0');
comment on table pdq.ro_sales is 'populated by function pdq.update_ro_sales(), run daily on luigi server @ 4:45 am (cron) in pdq_writer_stats.py a for populating the pdq stat email with pay period sales to date, based on ro close date';
  

select sum(labor), sum(parts), sum(total_sales) from pdq.ro_sales where store_code = 'RY1';