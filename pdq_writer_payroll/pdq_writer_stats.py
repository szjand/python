# encoding=utf-8
"""
workout the pdq writer stats before putting it into luigi
andrew decided this should go out daily and include pacing (payperiod) for lof's
12/4
    per andrew's request added sales totals
"""
import utilities
import csv
from openpyxl import load_workbook
import openpyxl
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from email.mime.text import MIMEText
import os

pg_server = '173'
# file_name = '../../extract_files/pyhshdta.csv'
file_name = 'writer_stats.csv'
spreadsheet = 'writer_stats.xlsx'


def check_ads_extracts():
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            # ads requirement
            pg_cur.execute("select ops.check_for_ads_extract('ads_ext_fact_repair_order')")
            if not pg_cur.fetchone()[0]:
                raise ValueError('The ads requirement: ads_ext_fact_repair_order is missing')
            pg_cur.execute("select ops.check_for_ads_extract('ads_ext_dim_opcode')")
            if not pg_cur.fetchone()[0]:
                raise ValueError('The ads requirement: ads_ext_dim_opcode is missing')


def get_ro_data():
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('select pdq.get_ros()')


def get_ro_sales_data():
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('select pdq.udpate_ro_sales()')


def create_spreadsheet():
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select 'Payperiod from '::text ||
                  to_char(biweekly_pay_period_start_date, 'MM/DD/YYYY') || ' thru '::text ||
                  to_char(biweekly_pay_period_end_date, 'MM/DD/YYYY') || '      ' ||
                  'Data thru '::text || to_char(a.the_date, 'MM/DD/YYYY')
                from dds.dim_date a
                join dds.working_days b on a.the_date = b.the_date
                  and b.department = 'pdq'
                where a.the_Date = current_date;                   
            """
            pg_cur.execute(sql)
            with open(file_name, 'w') as f:
                writer = csv.writer(f)
                writer.writerow([pg_cur.fetchone()[0]])
                writer.writerow([])
                # pg_cur.execute("select sum(total_sales) from pdq.ro_sales where store_code = 'RY1';")
                # writer.writerow(["RY1 Sales pay period to date: $" + str(pg_cur.fetchone()[0])])
                # pg_cur.execute("select sum(total_sales) from pdq.ro_sales where store_code = 'RY2';")
                # writer.writerow(["RY2 Sales pay period to date: $" + str(pg_cur.fetchone()[0])])

                pg_cur.execute("select pdq.get_ro_sales_for_pay_period('{}')".format('RY1'))
                writer.writerow(["RY1 Sales pay period to date: $" + str(pg_cur.fetchone()[0])])
                pg_cur.execute("select pdq.get_ro_sales_for_pay_period('{}')".format('RY2'))
                writer.writerow(["RY2 Sales pay period to date: $" + str(pg_cur.fetchone()[0])])

                writer.writerow([])
                writer.writerow(["store", "writer #", "writer", "emp #", "lof", "rotate", "air filters",
                                 "cabin filters", "batteries", "bulbs", "wipers", "lof pacing"])
                pg_cur.execute('select * from pdq.get_writer_stats();')
                writer.writerows(pg_cur)
            wb = openpyxl.Workbook()
            wb.save(spreadsheet)
            wb = load_workbook(spreadsheet)
            ws = wb.get_active_sheet()
            with open(file_name) as f:
                reader = csv.reader(f, delimiter=',')
                for row in reader:
                    ws.append(row)
            wb.save(spreadsheet)


def email_file():
    try:
        COMMASPACE = ', '
        sender = 'jandrews@cartiva.com'
        recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
        # recipients = ['test@cartiva.com', 'jandrews@cartiva.com', 'dwilkie@rydellcars.com']
        outer = MIMEMultipart()
        outer['Subject'] = 'PDQ Writer Stats'
        outer['To'] = COMMASPACE.join(recipients)
        outer['From'] = sender
        # outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
        attachments = [spreadsheet]
        for x_file in attachments:
            try:
                with open(x_file, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(x_file))
                outer.attach(msg)
            except Exception:
                raise
        composed = outer.as_string()
        e = smtplib.SMTP('mail.cartiva.com')
        try:
            e.sendmail(sender, recipients, composed)
        except smtplib.SMTPException:
            print("Error: unable to send email")
        e.quit()
    except Exception as error:
        print(error)


def main():
    check_ads_extracts()
    get_ro_data()
    get_ro_sales_data()
    create_spreadsheet()
    email_file()

if __name__ == '__main__':
    main()
