﻿/*
We have rolled out a new PDQ pay plan for the writers.  I would eventually like to automate this, but for now I’ll do a spreadsheet to calculate each pay period.

I am looking for a report that shows the following:

GM Store by writer:

Service type:  QL
Total for each category using the following op codes:

Rotate:  ROT, AROT, FREEROT, ROTEMP, WROT
Air Filters:  13A
Cabin Filters:  13C
Batteries:  N0110, N0110N
Bulbs:  BULB
Wipers:  WBN, WB, 3WB

HN Store by writer

Service type: QL
Total for each category using the following op codes:

Rotate:  ROT, AROT, FROT, ROTEMP
Air Filters:  RAF
Cabin Filters:  ICF, NCF
Batteries:  RBATT, RB
Bulbs:  BULB
Wipers:  RWI, RWB


If this works, will you do a test run for the previous pay period (8/18-8/31) so I can take a peek at the info?

9/25/19
Andrew came in and asked if this could be emailed out on a daily basis
and include a column for lof pacing

*/

select locationid, a.userid, a.firstname, a.lastname, a.supervisorname, a.title, b.department_code, distrib_code
from ads.ext_pto_compli_users a
left join arkona.xfm_pymast b on a.userid = b.pymast_employee_number
  and b.current_row
where a.status = '1'
  and b.distrib_code = 'PDQW'




drop table if exists pdq.opcodes cascade;
create table pdq.opcodes (
  store citext not null,
  category citext not null,
  opcode citext not null,
primary key(store,opcode));  
insert into pdq.opcodes values
('RY1','Rotate','ROT'),  
('RY1','Rotate','AROT'),  
('RY1','Rotate','FREEROT'),  
('RY1','Rotate','ROTEMP'),  
('RY1','Rotate','WROT'),  
('RY1','Air Filters','13A'),  
('RY1','Cabin Filters','13C'), 
('RY1','Batteries','N0110'), 
('RY1','Batteries','N0110N'), 
('RY1','Bulbs','BULB'), 
('RY1','Wipers','WBN'), 
('RY1','Wipers','WB'), 
('RY1','Wipers','3WB'), 
('RY2','Rotate','ROT'),  
('RY2','Rotate','AROT'),  
('RY2','Rotate','FROT'),  
('RY2','Rotate','ROTEMP'),  
('RY2','Air Filters','RAF'),
('RY2','Cabin Filters','ICF'), 
('RY2','Cabin Filters','NCF'), 
('RY2','Batteries','RBATT'), 
('RY2','Batteries','RB'), 
('RY2','Bulbs','BULB'), 
('RY2','Wipers','RWI'), 
('RY2','Wipers','RWB'),

('RY1','LOF','M1020'), 
('RY1','LOF','M1040'),
('RY1','LOF','M1520'),
('RY1','LOF','M1030'),
('RY1','LOF','M11030'),
-- ('RY1','LOF','DEX25'),
('RY1','LOF','NBO'),
('RY1','LOF','PDQDEL110'),
('RY1','LOF','PDQDEL112'),
('RY1','LOF','PDQDEL113'),
('RY1','LOF','PDQDEL115'),
('RY1','LOF','PDQDS10'),
('RY1','LOF','PDQDS12'),
('RY1','LOF','PDQDS13'),
('RY1','LOF','PDQDS15'),
('RY1','LOF','PDQD0204'),
('RY1','LOF','PDQD0205'),
('RY1','LOF','PDQD0206'),
('RY1','LOF','PDQD0207'),
('RY1','LOF','PDQD0208'),
('RY1','LOF','PDQD0209'),
('RY1','LOF','DEX25'),
('RY1','LOF','PDQM14'),
('RY1','LOF','PDQM15'),
('RY1','LOF','PDQM16'),
('RY1','LOF','PDQM17'),
('RY1','LOF','PDQM18'),
('RY1','LOF','PDQ104'),
('RY1','LOF','PDQ105'),
('RY1','LOF','PDQ106'),
('RY1','LOF','PDQ107'),
('RY1','LOF','PDQ108'),
('RY1','LOF','PDQD4'),
('RY1','LOF','PDQD5'),
('RY1','LOF','PDQD6'),
('RY1','LOF','PDQD7'),
('RY1','LOF','PDQD8'),
('RY1','LOF','PDQ4'),
('RY1','LOF','PDQ5'),
('RY1','LOF','PDQ6'),
('RY1','LOF','PDQ7'),
('RY1','LOF','PDQ8'),
('RY1','LOF','PDQ5204'),
('RY1','LOF','PDQ5205'),
('RY1','LOF','PDQ5206'),
('RY1','LOF','PDQ5207'),
('RY1','LOF','PDQ5208'),
('RY2','LOF','LOF'), 
('RY2','LOF','LOFDS'),
('RY2','LOF','LOFD'), 
('RY2','LOF','LOF0205'),
('RY2','LOF','M1S'),
('RY2','LOF','LOFE'),
('RY2','LOF','LOFE020'),
('RY2','LOF','EM1S'),
('RY2','LOF','110009'), 
('RY2','LOF','YW15AA'),  
('RY2','LOF','YW10AA'), 
('RY2','LOF','LOFC');




drop table if exists base;
create temp table base as
select a.storecode, b.the_date, a.ro, a.line, d.opcode, e.category, c.writer_number, 
  c.last_name, c.first_name, c.employee_number
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date between '08/18/2019' and '08/31/2019'
join pdq.writers c on a.servicewriterkey = c.writer_key
join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
join pdq.opcodes e on a.storecode = e.store
  and d.opcode = e.opcode;

-- sent to jeri on 9/6
select storecode, writernumber, 
  employee_last_name || ', ' || employee_first_name as writer, 
  pymast_employee_number, 
  count(*) filter (where category = 'LOF') as LOF,
  count(*) filter (where category = 'Rotate') as rotate,
  count(*) filter (where category = 'Air Filters') as "air filters",
  count(*) filter (where category = 'Cabin Filters') as "cabin filters",
  count(*) filter (where category = 'Batteries') as batteries,
  count(*) filter (where category = 'Bulbs') as bulbs,
  count(*) filter (where category = 'Wipers') as wipers
-- select *
from base  
group by storecode, writernumber, 
  employee_last_name || ', ' || employee_first_name, 
  pymast_employee_number
order by storecode, writer



select a.*,
  count(*) over (partition by pymast_employee_number, category)
from base a
order by storecode, employee_last_name, category




select * from pdq_writers order by  left(pymast_employee_number, 1), employee_last_name

select * from ads.ext_dim_service_writer where writernumber = '487'


/*
jon: Does Service Type matter:
If the writer is a pdq writer and the opcode is one specified, I’m thinking the service type does not matter.
?

jeri: Can you see if there are any instances in which another service type is used?

-- just pdq writers
select a.storecode, c.employee_last_name, c.employee_first_name, f.servicetype, f.servicetypecode, count(*), min(a.ro), max(a.ro)
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date between '06/01/2019' and '08/31/2019'
join pdq_writers c on a.servicewriterkey = c.servicewriterkey
join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
join jon.pdq_opcode_categories e on a.storecode = e.store
  and d.opcode = e.opcode
join ads.ext_dim_service_type f on a.servicetypekey = f.servicetypekey 
group by a.storecode, c.employee_last_name, c.employee_first_name, f.servicetype, f.servicetypecode


-- all writers
select c.name, f.servicetype, f.servicetypecode, count(*), min(a.ro), max(a.ro)
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date between '06/01/2019' and '08/31/2019'
join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
join jon.pdq_opcode_categories e on a.storecode = e.store
  and d.opcode = e.opcode
join ads.ext_dim_service_type f on a.servicetypekey = f.servicetypekey 
group by c.name, f.servicetype, f.servicetypecode
*/


9/12/19
jeri:
  Will you also add # of oil changes per writer to this report?  
  Is there any way to set it up so it automatically sends to the 
  managers on Sunday morning of payroll week (this Sunday)?

jon:
Yes, 
    1.  To whom (email addresses) should this information be sent?
    2.  Did anyone review these numbers?
    3.	I need verification that this list of writers is complete and correct
    4.	I need , for each store, a complete list of oil change op codes

jeri:
  1.	Please send to following emails:
    a.	aneumann@rydellcars.com
    b.	wolson@rydellcars.com
    c.	nneumann@rydellcars.com
    d.	jdangerfield@gfhonda.com
    e.	rshroyer@gfhonda.com
  2.	The numbers are close but not exact—however, the only comparison we have currently is manually entered by writers
  3.	List of writers is accurate 
  4.	Oil change op codes are attached.

'jdangerfield@gfhonda.com'; 'rshroyer@gfhonda.com';'aneumann@rydellcars.com'; 'wolson@rydellcars.com'; 'nneumann@rydellcars.com'

cron every other sunday looks challenging at best,
so in the function to generate the data/email,
start it with a test to determine if its the correct sunday based on:

select the_date, day_name
from dds.dim_date
where the_Date = biweekly_pay_period_start_date
  and the_year = 2019
order by the_date  


select a.the_date, biweekly_pay_period_start_date
from dds.dim_date a 
where a.the_year = 2019
  and a.day_name = 'sunday'


do
$$
declare _the_date date := '09/01/2019';
begin

if exists (
  select 1 
  from dds.dim_date
  where biweekly_pay_period_start_date = _the_date) then
    raise notice 'send email';
else
  raise notice 'nope exit';
end if;
  
end
$$;


-- 9/14/2019
-- ok, building this for real so that tomorrow it works
-- decided to use a new schema

create schema pdq;
comment on schema pdq is 'all the elements necessary to generate an email of pdq writer statistics for payroll generation';

drop function if exists pdq.is_payperiod_start();
create function pdq.is_payperiod_start()
  returns boolean as
$BODY$
/*
select pdq.is_payperiod_start();
*/
  select exists (
    select 1
    from dds.dim_Date
--         where biweekly_pay_period_start_date = current_date);
    where biweekly_pay_period_start_date = '09/15/2019');
$BODY$
language sql;


drop table if exists base;
create temp table base as
select a.storecode, b.the_date, a.ro, a.line, d.opcode, e.category, c.writer_number, 
  c.last_name, c.first_name, c.employee_number
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date between '08/18/2019' and '08/31/2019'
join pdq.writers c on a.servicewriterkey = c.writer_key
join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
join pdq.opcodes e on a.storecode = e.store
  and d.opcode = e.opcode;


create index on ads.ext_fact_repair_order(storecode);
create index on ads.ext_dim_opcode(opcodekey);
create index on ads.ext_dim_opcode(opcode);

select ro, line
from base
group by ro, line
having count(*) > 1

select * from base where ro = '16360610' and line = 3


select a.storecode, b.the_date, a.ro, a.line, d.opcode, e.category, c.writer_number, 
  c.last_name, c.first_name, c.employee_number
-- select *  
from ads.ext_fact_repair_order a
-- join dds.dim_date b on a.opendatekey = b.date_key
--   and b.the_date between '08/18/2019' and '08/31/2019'
-- join pdq.writers c on a.servicewriterkey = c.writer_key
-- join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
-- join pdq.opcodes e on a.storecode = e.store
--   and d.opcode = e.opcode
where a.ro = '19351633'  
  and line = 3


drop table if exists pdq.writers cascade;
create table pdq.writers (
  first_name citext not null,
  last_name citext not null,
  employee_number citext not null,
  writer_number citext not null,
  writer_key integer not null,
  primary key (employee_number));
create unique index on pdq.writers(writer_key);    
create unique index on pdq.writers(writer_number);
       
insert into pdq.writers  
select b.employee_first_name, b.employee_last_name, b.pymast_employee_number, a.writernumber, a.servicewriterkey
from ads.ext_dim_service_writer a
join arkona.xfm_pymast b on b.pymast_employee_number = a.employeenumber
  and b.current_row
  and b.distrib_code = 'PDQW'
  and b.active_code <> 'T'
  and b.pymast_employee_number not in ('197560')
where a.currentrow
  and a.active;
  
drop table if exists pdq.ros cascade;
create table pdq.ros (
  store citext not null,
  the_date date not null,
  ro citext not null, 
  line integer not null, 
  opcode citext not null,
  category citext not null,
  writer_number citext not null references pdq.writers(writer_number),
  employee_number citext not null references pdq.writers(employee_number),
  writer citext not null,
  primary key (ro, line));
create index on pdq.ros(opcode);
create index on pdq.ros(the_date);
alter table pdq.ros
add constraint opcode_fk foreign key(store,opcode)
references pdq.opcodes(store,opcode);

drop function if exists pdq.get_ros();
create function pdq.get_ros()
returns void as
$BODY$
insert into pdq.ros  
select a.storecode, b.the_date, a.ro, a.line, d.opcode, e.category, 
  c.writer_number, c.employee_number, c.last_name || ', ' || c.first_name
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_date between current_date - 14 and current_date -1
join pdq.writers c on a.servicewriterkey = c.writer_key
join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
join pdq.opcodes e on a.storecode = e.store
  and d.opcode = e.opcode
group by a.storecode, b.the_date, a.ro, a.line, d.opcode, e.category,
  c.last_name || ', ' || c.first_name, c.writer_number, c.employee_number
on conflict(ro,line) do nothing; 
$BODY$
language sql;

select the_date, count(*) from pdq.ros group by the_Date order by the_date

-- distinct
select ro, line
from pdq.ros
group by ro, line
having count(*) > 1

-- not distinct 
-- can have, for example, bulbs on multiple lines of same ro
-- front & rear wiper blades can be on separate lines
select ro, opcode
from pdq.ros
group by ro, opcode
having count(*) > 1

select * 
from pdq.ros

drop function if exists pdq.get_writer_stats();
create function pdq.get_writer_stats()
returns table(store citext,writer_number citext,writer citext,employee_number citext,
  lof bigint,rotate bigint,"air filters" bigint,"cabin filters" bigint, 
  batteries bigint,bulbs bigint,wipers bigint) as
$BODY$  
/*
select * from pdq.get_writer_stats();
*/
select store, writer_number, writer, 
  employee_number, 
  count(*) filter (where category = 'LOF') as LOF,
  count(*) filter (where category = 'Rotate') as rotate,
  count(*) filter (where category = 'Air Filters') as "air filters",
  count(*) filter (where category = 'Cabin Filters') as "cabin filters",
  count(*) filter (where category = 'Batteries') as batteries,
  count(*) filter (where category = 'Bulbs') as bulbs,
  count(*) filter (where category = 'Wipers') as wipers
from pdq.ros  
where the_date between current_date - 14 and current_date - 1
group by store, writer_number, writer, employee_number
order by store, writer;
$BODY$ 
language sql;


select * from pdq.ros 
where writer = 'robles, rudolph'
  and category = 'rotate'
order by the_date  

select current_date - 14