﻿do $$
--declare
--  _from_date date := (
--    select biweekly_pay_period_start_date
--    from dds.dim_date
--    where the_Date = current_date - 1);  
--  _thru_date date := (
--    select biweekly_pay_period_end_date
--    from dds.dim_date
--    where the_Date = current_date - 1);    
begin
drop table if exists wtf;
create temp table wtf as  
select b.*, -- b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period as "lof pacing"
  case
    when c.wd_of_biweekly_pay_period = 0 then 0 else
    b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period 
  end as "lof pacing"
from (
  select a.store, a.writer_number, (a.last_name || ', ' || a.first_name)::citext as writer, a.employee_number,
    count(*) filter (where b.category = 'LOF') as LOF,
    (count(*) filter (where b.category = 'Rotate'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Rotate')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as rotate,
    (count(*) filter (where b.category = 'Air Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Air Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "air filters",
    (count(*) filter (where b.category = 'Cabin Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Cabin Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "cabin filters",
    (count(*) filter (where b.category = 'Batteries'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Batteries')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as batteries,    
    (count(*) filter (where b.category = 'Bulbs'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Bulbs')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as bulbs,
    (count(*) filter (where b.category = 'Wipers'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Wipers')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as wipers  
  from pdq.writers a  
  left join pdq.ros b on a.employee_number = b.employee_number
    and b.the_date between '10/11/2020' and '10/13/2020'
  where a.active
  group by a.store, a.writer_number, a.last_name || ', ' || a.first_name, a.employee_number) b 
join dds.working_days c on c.department = 'pdq'
  and c.the_date = current_date - 1  
union
select b.store, b.writer_number, b.writer, b.employee_number,
  b.lof, 
  case when b.lof = 0 then '0' else (round(100.0 * b.rotate/b.lof, 1))::text end || '%',
  case when b.lof = 0 then '0' else round(100.0 * b."air filters"/b.lof, 1)::text end || '%', 
  case when b.lof = 0 then '0' else round(100.0 * b."cabin filters"/b.lof, 1)::text end || '%',
  case when b.lof = 0 then '0' else round(100.0 * b.batteries/b.lof, 1)::text end || '%', 
  case when b.lof = 0 then '0' else round(100.0 * b.bulbs/b.lof, 1)::text end || '%', 
  case when b.lof = 0 then '0' else round(100.0 * b.wipers/b.lof, 1)::text end || '%',
  case
    when c.wd_of_biweekly_pay_period = 0 then 0 
    else b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period 
  end as "lof pacing"
  from (
    select a.store, 'Total LOF & Penetration'::citext as writer_number,
      null::citext as writer ,null::citext as employee_number,
      count(*) filter (where a.category = 'LOF') as LOF,
      count(*) filter (where a.category = 'Rotate') as rotate,
      count(*) filter (where a.category = 'Air Filters') as "air filters",
      count(*) filter (where a.category = 'Cabin Filters') as "cabin filters",
      count(*) filter (where a.category = 'Batteries') as batteries,
      count(*) filter (where a.category = 'Bulbs') as bulbs,
      count(*) filter (where a.category = 'Wipers') as wipers
    from pdq.ros a
    where a.the_date between '10/11/2020' and '10/13/2020'
    group by a.store) b
  join dds.working_days c on c.department = 'pdq'
    and c.the_date = current_date - 1
order by store, writer nulls last;
end $$;

select * from wtf;
