﻿select count(distinct ro) 
from pdq.ros
where the_date between '06/05/2022' and '06/18/2022'


  select a.store_code, a.open_date, a.ro, a.line, d.opcode, e.category, f.writer_number, f.writer_name,
    count(*) over (partition by a.store_code)
  from dds.fact_repair_order a
  join dds.dim_service_type aa on a.service_type_key = aa.service_type_key
    and aa.service_type_code = 'QL'
  join dds.dim_opcode d on a.opcode_key = d.opcode_key
  join pdq.opcodes e on a.store_code = e.store
    and e.category = 'LOF'
    and d.opcode = e.opcode
  left join pdq.writers c on a.service_writer_key = c.writer_key
  left join dds.dim_service_writer f on a.service_writer_key = f.service_writer_key
  where a.open_date between '06/05/2022' and '06/18/2022'
    and c.writer_key is null
  group by a.store_code, a.open_date, a.ro, a.line, d.opcode, e.category, f.writer_number, f.writer_name
order by a.ro

select * from dds.dim_Service_type

select * from dds.dim_Service_writer limit 100