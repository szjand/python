﻿-- generate yesterdays report using dds as a verification

-- get_ros
  select a.storecode, b.the_date, a.ro, a.line, d.opcode, e.category, 
    c.writer_number, c.employee_number, c.last_name || ', ' || c.first_name
-- ry1: 161, ry2: 59    
select a.storecode, b.the_date, count(*)    
  from ads.ext_fact_repair_order a
  join dds.dim_date b on a.opendatekey = b.date_key
    and b.the_date between '10/11/2020' and current_date - 2
  join pdq.writers c on a.servicewriterkey = c.writer_key
    and c.active
  join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
  join pdq.opcodes e on a.storecode = e.store
    and d.opcode = e.opcode
group by a.storecode, b.the_date    
--   group by a.storecode, b.the_date, a.ro, a.line, d.opcode, e.category,
--     c.last_name || ', ' || c.first_name, c.writer_number, c.employee_number


-- compare to dds 161/58, good enough, the 1 diff aggrees with arkona
select a.store_code, b.the_date, count(*)    
  from dds.fact_repair_order a
  join dds.dim_date b on a.open_Date = b.the_date
    and b.the_date between '10/11/2020' and current_date - 2
  join pdq.writers c on a.service_writer_key = c.writer_key
    and c.active
  join dds.dim_opcode d on a.opcode_key = d.opcode_key
  join pdq.opcodes e on a.store_code = e.store
    and d.opcode = e.opcode
group by a.store_code, b.the_date  

-- pdq.get_ros() convert to dds
-- no need for join to dds.dim_date, the dates are in dds.fact_repair_order
drop table if exists ros;
create temp table ros as
  select a.store_code, a.open_date, a.ro, a.line, d.opcode, e.category, 
    c.writer_number, c.employee_number, c.last_name || ', ' || c.first_name
  from dds.fact_repair_order a
--   join dds.dim_date b on a.open_date = b.the_date
--     and b.the_date between '10/11/2020' and current_date - 2
  join pdq.writers c on a.service_writer_key = c.writer_key
    and c.active
  join dds.dim_opcode d on a.opcode_key = d.opcode_key
  join pdq.opcodes e on a.store_code = e.store
    and d.opcode = e.opcode
  where a.open_date between '10/11/2020' and current_date - 2
  group by a.store_code, a.open_date, a.ro, a.line, d.opcode, e.category,
    c.last_name || ', ' || c.first_name, c.writer_number, c.employee_number;


-- update_ro_sales
--   select storecode, ro, close_date, 
-- RY1;2020-10-12;4235.60;3123.30;7358.90
-- RY2;2020-10-12;1149.90;1165.14;2315.04
  select storecode, close_date,   
    coalesce(sum(-a.amount) filter (where department_code = 'QL'), 0) as labor,
    coalesce(sum(-a.amount) filter (where department_code = 'PD'), 0) as parts,
    coalesce(sum(-a.amount), 0) as total_sales
  from fin.fact_gl a
  join pdq.sale_accounts b on a.account_key = b.account_key
  join (
    select a.storecode, a.ro, b.the_date as close_date
    from ads.ext_fact_repair_order a
    join dds.dim_date b on a.closedatekey = b.date_key
      and b.the_date between '10/11/2020' and current_date - 2
    join ads.ext_dim_Service_type c on a.servicetypekey = c.servicetypekey
      and c.servicetypecode = 'QL'  
    group by a.storecode, a.ro, b.the_date) c on a.control = c.ro
--   group by storecode, ro, close_date
  group by storecode, close_date

-- compare to dds perfect match
-- update_ro_sales
  select store_code, close_date,   
    coalesce(sum(-a.amount) filter (where department_code = 'QL'), 0) as labor,
    coalesce(sum(-a.amount) filter (where department_code = 'PD'), 0) as parts,
    coalesce(sum(-a.amount), 0) as total_sales
  from fin.fact_gl a
  join pdq.sale_accounts b on a.account_key = b.account_key
  join (
    select a.store_code, a.ro, b.the_date as close_date
    from dds.fact_repair_order a
    join dds.dim_date b on a.close_date = b.the_date
      and b.the_date between '10/11/2020' and current_date - 2
    join dds.dim_Service_type c on a.service_type_key = c.service_type_key
      and c.service_type_code = 'QL'  
    group by a.store_code, a.ro, b.the_date) c on a.control = c.ro
--   group by storecode, ro, close_date
  group by store_code, close_date

-- pdq.update_ro_sales() convert to dds
-- likewise no need for join on dds.dim_date
drop table if exists ro_sales;
create temp table ro_sales as
  select store_code, ro, close_date, 
    coalesce(sum(-a.amount) filter (where department_code = 'QL'), 0) as labor,
    coalesce(sum(-a.amount) filter (where department_code = 'PD'), 0) as parts,
    coalesce(sum(-a.amount), 0) as total_sales
  from fin.fact_gl a
  join pdq.sale_accounts b on a.account_key = b.account_key
  join (
    select a.store_code, a.ro, a.close_date
    from dds.fact_repair_order a
--     join dds.dim_date b on a.closedatekey = b.date_key
--       and b.the_date between _from_date and _thru_date
    join dds.dim_Service_type c on a.service_type_key = c.service_type_key
      and c.service_type_code = 'QL'  
    where a.close_date between '10/11/2020' and current_date - 2
    group by a.store_code, a.ro, a.close_date) c on a.control = c.ro
  group by store_code, ro, close_date

  
-- get writer stats
-- this exactly matches yesterday and and todays email
select b.*, -- b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period as "lof pacing"
  case
    when c.wd_of_biweekly_pay_period = 0 then 0 else
    b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period 
  end as "lof pacing"
from (
  select a.store, a.writer_number, (a.last_name || ', ' || a.first_name)::citext as writer, a.employee_number,
    count(*) filter (where b.category = 'LOF') as LOF,
    count(*) filter (where b.category = 'Rotate') as rotate,
    count(*) filter (where b.category = 'Air Filters') as "air filters",
    count(*) filter (where b.category = 'Cabin Filters') as "cabin filters",
    count(*) filter (where b.category = 'Batteries') as batteries,
    count(*) filter (where b.category = 'Bulbs') as bulbs,
    count(*) filter (where b.category = 'Wipers') as wipers
  from pdq.writers a  
  left join pdq.ros b on a.employee_number = b.employee_number
    and b.the_date between '10/11/2020' and current_date - 2
  where a.active
  group by a.store, a.writer_number, a.last_name || ', ' || a.first_name, a.employee_number) b 
join dds.working_days c on c.department = 'pdq'
  and c.the_date = current_date - 1  
union
  select b.store, b.writer_number, b.writer, b.employee_number,
  b.lof, 
  case when b.lof = 0 then 0 else round(100.0 * b.rotate/b.lof, 1) end,
  case when b.lof = 0 then 0 else round(100.0 * b."air filters"/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b."cabin filters"/b.lof, 1) end,
  case when b.lof = 0 then 0 else round(100.0 * b.batteries/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b.bulbs/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b.wipers/b.lof, 1) end,
--   b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period as "lof pacing"
  case
    when c.wd_of_biweekly_pay_period = 0 then 0 else
    b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period 
  end as "lof pacing"
  from (
    select a.store, 'Total LOF & Penetration'::citext as writer_number,
      null::citext as writer ,null::citext as employee_number,
      count(*) filter (where a.category = 'LOF') as LOF,
      count(*) filter (where a.category = 'Rotate') as rotate,
      count(*) filter (where a.category = 'Air Filters') as "air filters",
      count(*) filter (where a.category = 'Cabin Filters') as "cabin filters",
      count(*) filter (where a.category = 'Batteries') as batteries,
      count(*) filter (where a.category = 'Bulbs') as bulbs,
      count(*) filter (where a.category = 'Wipers') as wipers
    from pdq.ros a
    where a.the_date between '10/11/2020' and current_date - 2
    group by a.store) b
  join dds.working_days c on c.department = 'pdq'
    and c.the_date = current_date - 1
order by store, writer nulls last;

-- pdq.get_writer_stats
-- substituting the dds temp tables
-- 1 ro switched between brooks and robles
-- dds agrees with arkona
-- this means we are golden, good to go
-- modify the functions
select b.*, -- b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period as "lof pacing"
  case
    when c.wd_of_biweekly_pay_period = 0 then 0 else
    b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period 
  end as "lof pacing"
from (
  select a.store, a.writer_number, (a.last_name || ', ' || a.first_name)::citext as writer, a.employee_number,
    count(*) filter (where b.category = 'LOF') as LOF,
    count(*) filter (where b.category = 'Rotate') as rotate,
    count(*) filter (where b.category = 'Air Filters') as "air filters",
    count(*) filter (where b.category = 'Cabin Filters') as "cabin filters",
    count(*) filter (where b.category = 'Batteries') as batteries,
    count(*) filter (where b.category = 'Bulbs') as bulbs,
    count(*) filter (where b.category = 'Wipers') as wipers
  from pdq.writers a  
  left join pdq.ros b on a.employee_number = b.employee_number
    and b.open_date between '10/11/2020' and current_date - 2
  where a.active
  group by a.store, a.writer_number, a.last_name || ', ' || a.first_name, a.employee_number) b 
join dds.working_days c on c.department = 'pdq'
  and c.the_date = current_date - 1  
union
  select b.store_code, b.writer_number, b.writer, b.employee_number,
  b.lof, 
  case when b.lof = 0 then 0 else round(100.0 * b.rotate/b.lof, 1) end,
  case when b.lof = 0 then 0 else round(100.0 * b."air filters"/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b."cabin filters"/b.lof, 1) end,
  case when b.lof = 0 then 0 else round(100.0 * b.batteries/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b.bulbs/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b.wipers/b.lof, 1) end,
--   b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period as "lof pacing"
  case
    when c.wd_of_biweekly_pay_period = 0 then 0 else
    b.lof * c.wd_in_biweekly_pay_period/c.wd_of_biweekly_pay_period 
  end as "lof pacing"
  from (
    select a.store, 'Total LOF & Penetration'::citext as writer_number,
      null::citext as writer ,null::citext as employee_number,
      count(*) filter (where a.category = 'LOF') as LOF,
      count(*) filter (where a.category = 'Rotate') as rotate,
      count(*) filter (where a.category = 'Air Filters') as "air filters",
      count(*) filter (where a.category = 'Cabin Filters') as "cabin filters",
      count(*) filter (where a.category = 'Batteries') as batteries,
      count(*) filter (where a.category = 'Bulbs') as bulbs,
      count(*) filter (where a.category = 'Wipers') as wipers
    from pdq.ros a
    where a.open_date between '10/11/2020' and current_date - 2
    group by a.store_code) b
  join dds.working_days c on c.department = 'pdq'
    and c.the_date = current_date - 1
order by store, writer nulls last;
