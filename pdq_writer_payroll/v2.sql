﻿/*
9/27

How much work would it be to put total columns on the bottom? So they are able to see penetrations %’s and total volume pace for all oil changes for the pay period?



Andrew
*/

--   select b.*, b.lof * c.wd_in_biweekly_pay_period/wd_of_biweekly_pay_period as "lof pacing"
--   from (
--     select a.store, a.writer_number, a.writer, 
--       a.employee_number, 
--       count(*) filter (where a.category = 'LOF') as LOF,
--       count(*) filter (where a.category = 'Rotate') as rotate,
--       count(*) filter (where a.category = 'Air Filters') as "air filters",
--       count(*) filter (where a.category = 'Cabin Filters') as "cabin filters",
--       count(*) filter (where a.category = 'Batteries') as batteries,
--       count(*) filter (where a.category = 'Bulbs') as bulbs,
--       count(*) filter (where a.category = 'Wipers') as wipers
--     from pdq.ros a
--     where a.the_date between '09/15/2019' and '09/28/2019'
--     group by a.store, a.writer_number, a.writer, a.employee_number) b
--   join dds.working_days c on c.department = 'pdq'
--     and c.the_date = current_date  
-- union    
-- 
--   select b.*, b.lof * c.wd_in_biweekly_pay_period/wd_of_biweekly_pay_period as "lof pacing"
--   from (
--     select a.store, 'Total'::citext, null::citext,null::citext,
--       count(*) filter (where a.category = 'LOF') as LOF,
--       count(*) filter (where a.category = 'Rotate') as rotate,
--       count(*) filter (where a.category = 'Air Filters') as "air filters",
--       count(*) filter (where a.category = 'Cabin Filters') as "cabin filters",
--       count(*) filter (where a.category = 'Batteries') as batteries,
--       count(*) filter (where a.category = 'Bulbs') as bulbs,
--       count(*) filter (where a.category = 'Wipers') as wipers
--     from pdq.ros a
--     where a.the_date between '09/15/2019' and '09/28/2019'
--     group by a.store) b
--   join dds.working_days c on c.department = 'pdq'
--     and c.the_date = current_date  
-- order by store, writer nulls last

select b.*, b.lof * c.wd_in_biweekly_pay_period/wd_of_biweekly_pay_period as "lof pacing"
from (
  select a.store, a.writer_number, a.writer, 
    a.employee_number, 
    count(*) filter (where a.category = 'LOF') as LOF,
    count(*) filter (where a.category = 'Rotate') as rotate,
    count(*) filter (where a.category = 'Air Filters') as "air filters",
    count(*) filter (where a.category = 'Cabin Filters') as "cabin filters",
    count(*) filter (where a.category = 'Batteries') as batteries,
    count(*) filter (where a.category = 'Bulbs') as bulbs,
    count(*) filter (where a.category = 'Wipers') as wipers
  from pdq.ros a
  where a.the_date between '09/15/2019' and '09/28/2019'
  group by a.store, a.writer_number, a.writer, a.employee_number) b
join dds.working_days c on c.department = 'pdq'
  and c.the_date = current_date  
union  -- totals, penetration & pacing
select b.store, b.writer_number, b.writer, b.employee_number,
  b.lof, 
  case when b.lof = 0 then 0 else round(100.0 * b.rotate/b.lof, 1) end,
  case when b.lof = 0 then 0 else round(100.0 * b."air filters"/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b."cabin filters"/b.lof, 1) end,
  case when b.lof = 0 then 0 else round(100.0 * b.batteries/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b.bulbs/b.lof, 1) end, 
  case when b.lof = 0 then 0 else round(100.0 * b.wipers/b.lof, 1) end,
  b.lof * c.wd_in_biweekly_pay_period/wd_of_biweekly_pay_period as "lof pacing"
from (
  select a.store, 'Total LOF & Penetration'::citext as writer_number,
    null::citext as writer ,null::citext as employee_number,
    count(*) filter (where a.category = 'LOF') as LOF,
    count(*) filter (where a.category = 'Rotate') as rotate,
    count(*) filter (where a.category = 'Air Filters') as "air filters",
    count(*) filter (where a.category = 'Cabin Filters') as "cabin filters",
    count(*) filter (where a.category = 'Batteries') as batteries,
    count(*) filter (where a.category = 'Bulbs') as bulbs,
    count(*) filter (where a.category = 'Wipers') as wipers
  from pdq.ros a
  where a.the_date between '09/15/2019' and '09/28/2019'
  group by a.store) b
join dds.working_days c on c.department = 'pdq'
  and c.the_date = current_date  
order by store, writer nulls last