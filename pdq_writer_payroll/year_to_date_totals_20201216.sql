﻿--  modified select clause from Function: pdq.get_writer_stats()
-- year to date total for dayton

  select 
    count(*) filter (where b.category = 'LOF') as LOF,
    (count(*) filter (where b.category = 'Rotate'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Rotate')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as rotate,
    (count(*) filter (where b.category = 'Air Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Air Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "air filters",
    (count(*) filter (where b.category = 'Cabin Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Cabin Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "cabin filters",
    (count(*) filter (where b.category = 'Batteries'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Batteries')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as batteries,    
    (count(*) filter (where b.category = 'Bulbs'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Bulbs')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as bulbs,
    (count(*) filter (where b.category = 'Wipers'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Wipers')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as wipers  
from pdq.ros b
where store = 'RY1'
  and the_date between '01/01/2020' and '12/31/2020'
group by store  

-- and for ry2

  select 
    count(*) filter (where b.category = 'LOF') as LOF,
    (count(*) filter (where b.category = 'Rotate'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Rotate')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as rotate,
    (count(*) filter (where b.category = 'Air Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Air Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "air filters",
    (count(*) filter (where b.category = 'Cabin Filters'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Cabin Filters')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as "cabin filters",
    (count(*) filter (where b.category = 'Batteries'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Batteries')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as batteries,    
    (count(*) filter (where b.category = 'Bulbs'))::text || ' / ' ||  
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Bulbs')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as bulbs,
    (count(*) filter (where b.category = 'Wipers'))::text || ' / ' || 
        case 
          when count(*) filter (where b.category = 'LOF') = 0 then '0' 
          else (round(100.0 * count(*) filter (where b.category = 'Wipers')/(count(*) filter (where b.category = 'LOF')), 1))::text 
        end ||'%' as wipers  
from pdq.ros b
where store = 'RY2'
  and the_date between '01/01/2020' and '12/31/2020'
group by store  