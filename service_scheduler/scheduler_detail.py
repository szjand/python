# encoding=utf-8

# import adsdb
import smtplib
from email.mime.text import MIMEText
import db_cnx


message = None
# rs_con = adsdb.connect(DataSource='\\\\10.130.196.83:6363\\advantage\\rydellservice\\rydellservice.add',
#                        userid='adssys', password='cartiva22', CommType='TCP_IP', ServerType='remote',
#                        TrimTrailingSpaces='TRUE')
rs_con = db_cnx.ads_service_scheduler()
rs_cur = rs_con.cursor()
# dds_con = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\Advantage\\DDS\\DDS.add',
#                         userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
#                         TrimTrailingSpaces='TRUE')
dds_con = db_cnx.ads_dds()
dds_cur = dds_con.cursor()
try:
    sql = """
        delete from schedulerdetailappointments;
    """
    dds_cur.execute(sql)
    dds_con.commit()
    # had to: cast timestamp as sql_char
    #         coalesce some nulls to 'None'
    #         see \DDS2\Executables\SchedulerDetail\scripts\appointment_lines.sql
    # 5/14/18 added addition appointmentid to exclude, ie pussied out
    # something in that record generated: Column not found: None
    # think there is an embedded CHAR(13) in appointments.comments in that row, fucked around with getting
    # rid of it, no luck
    # 6/21/18 added another one, found it by setting a breakpoint at the beginning of the except block
    sql = """
        SELECT a.appointmentid, cast(a.created AS sql_char),
          a.createdbyid, cast(a.starttime AS sql_char),
          cast(a.promisetime AS sql_char),
		  -- remove ' FROM customer
          a.status,a.ronumber,replace(a.customer, '''', '') AS customer,
          CASE
            WHEN a.notes IS NULL THEN 'None'
            ELSE replace(replace(trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),
                'Customer States:','')), '''', ''), '%', ' percent')
          END AS squawk,
          a.modelyear, a.make, a.model, a.modelcolor, a.vin,
          replace(replace(replace(coalesce(a.comments,'None'), '%',' percent'), CHAR(13)+char(10),''), '''', ''),
          coalesce(a.techcomments, 'None')
        FROM appointments a
        WHERE advisorid = 'Detail'
          AND CAST(starttime AS sql_date) >= curdate()
          and appointmentid not in ( '[908c17f1-02a9-3b45-8bb9-ea1455ecd500]','[cbd79c3e-eb86-f143-af43-76c76a2bddaf]',
            '[9ffde179-6bd4-1940-8915-58837d0c36f4]');
    """
    rs_cur.execute(sql)
    for row in rs_cur.fetchall():
        # print str(row)
        dds_cur.execute(""" insert into schedulerdetailappointments values""" + str(row))
        dds_con.commit()
    sql = """
        execute procedure xfm_detail_appointment_lines();
    """
    rs_cur.execute(sql)
    rs_con.commit()

    sql = """
        delete from scheduler_detail_appointment_lines;
    """
    dds_cur.execute(sql)
    dds_con.commit()
    # this gets rid of the embedded ' characters in the comments field: wasn't, don't, etc
    # 4/30/18 added coalesce
    sql = """
        SELECT appointment_id, appointment_line_id, coalesce(replace(comment, '''', ''), 'None')
        FROM xfm_appointment_lines
    """
    rs_cur.execute(sql)
    for row in rs_cur.fetchall():
        # print str(row)
        dds_cur.execute(""" insert into scheduler_detail_appointment_lines values""" + str(row))
        dds_con.commit()
    sql = """
        UPDATE schedulerdetailappointments
        SET jobdescription = x.comment
        FROM (
          SELECT *
          FROM scheduler_detail_appointment_lines) x
          WHERE schedulerdetailappointments.appointmentid = x.appointment_id;
    """
    dds_cur.execute(sql)
    dds_con.commit()
    print('passssssssssssssssssssssssssssssssssssss')
except Exception, error:
    body = str(error)
    message = MIMEText(body)
    message['Subject'] = 'Failed: Scheduler_Detail_Applointments'
    smtp_server = 'mail.cartiva.com'
    e = smtplib.SMTP(smtp_server)
    addr_to = 'jandrews@cartiva.com'
    addr_from = 'jandrews@cartiva.com'
    message['To'] = addr_to
    message['From'] = addr_from
    receivers = ['jandrews@cartiva.com']
    e.sendmail(addr_from, receivers, message.as_string())
    e.quit()
finally:
    rs_con.close()
    # rs_cur.close()
    dds_con.close()
    # dds_cur.close()
