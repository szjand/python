# encoding=utf-8

import smtplib
from email.mime.text import MIMEText
import db_cnx
import csv
"""
refactored to not use adsdb
"""
sched_con = None
dds_con = None
file_name = 'files/first_query.csv'
try:
    with db_cnx.ads_service_scheduler() as sched_con:
        with sched_con.cursor() as sched_cur:
            sql = """
                SELECT a.appointmentid, cast(a.created AS sql_char),
                  a.createdbyid, cast(a.starttime AS sql_char),
                  cast(a.promisetime AS sql_char),
                  -- remove ' FROM customer
                  a.status,a.ronumber,replace(a.customer, '''', '') AS customer,
                  CASE
                    WHEN a.notes IS NULL THEN 'None'
                    ELSE replace(replace(trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'')),
                        'Customer States:','')), '''', ''), '%', ' percent')
                  END AS squawk,
                  a.modelyear, a.make, a.model, a.modelcolor, a.vin,
                  replace(replace(replace(coalesce(a.comments,'None'), '%',' percent'), CHAR(13)+char(10),''), '''', ''),
                  coalesce(a.techcomments, 'None')
                FROM appointments a
                WHERE advisorid = 'Detail'
                  AND CAST(starttime AS sql_date) >= curdate()
                  and appointmentid <> '[908c17f1-02a9-3b45-8bb9-ea1455ecd500]';
            """
        sched_cur.execute(sql)
        # with open(file_name, 'wb') as f:
        #     csv.writer(f).writerows(sched_cur.fetchall())
        for row in sched_cur.fetchall():
            with db_cnx.ads_dds() as dds_con:
                with dds_con.cursor() as dds_cur:
                    dds_cur.execute(""" insert into schedulerdetailappointments values""" + str(row))


    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if sched_con:
        sched_con.close()
    if dds_con:
        dds_con.close()
