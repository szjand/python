﻿

select a.*, b.*, c.*
from test.ext_Arkona_sypffxmst a
left join test.ext_arkona_ffpxrefdta b on a.fxmact = b.factory_account
  and b.factory_financial_year = 2015
left join test.ext_arkona_glpmast c on b.g_l_acct_number = c.account_number  
  and c.year = 2015
where a.fxmcde = 'GM'
  and a.fxmcyy = 2015  
  and a.fxmpge = 16
  and c.active = 'Y'
order by a.fxmlne, a.fxmcol, b.g_l_acct_number  


-- throw in glptrns 
select coalesce(b.consolidation_grp, '1'), a.fxmact, a.fxmpge, a.fxmlne, c.account_number, c.account_type, c.account_desc, sum(d.gttamt) as gttamt
from test.ext_Arkona_sypffxmst a
left join test.ext_arkona_ffpxrefdta b on a.fxmact = b.factory_account
  and b.factory_financial_year = 2015
left join test.ext_arkona_glpmast c on b.g_l_acct_number = c.account_number  
  and c.year = 2015
left join test.ext_glptrns_1205 d on c.account_number = d.gtacct
  and d.gtdate between '11/01/2015' and '11/30/2015' 
  and d.gtpost <> 'V' -----------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
where a.fxmcde = 'GM'
  and a.fxmcyy = 2015  
  and a.fxmpge = 16
  and c.active = 'Y'
 group by coalesce(b.consolidation_grp, '1'), a.fxmact, a.fxmpge, a.fxmlne, c.account_number, c.account_type, c.account_desc 
 order by coalesce(b.consolidation_grp, '1'), a.fxmpge, a.fxmlne


 -- sales accounts only, group by store, page, line, column, start pos
select coalesce(b.consolidation_grp, '1'), a.fxmpge, a.fxmlne, fxmcol, fxmstr, sum(d.gttamt) as gttamt
from test.ext_Arkona_sypffxmst a
inner join test.ext_arkona_ffpxrefdta b on a.fxmact = b.factory_account
  and b.factory_financial_year = 2015
inner join test.ext_arkona_glpmast c on b.g_l_acct_number = c.account_number  
  and c.year = 2015
  and c.account_type = '4'
inner join test.ext_glptrns_1205 d on c.account_number = d.gtacct
  and d.gtdate between '11/01/2015' and '11/30/2015' 
  and d.gtpost <> 'V' -----------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
where a.fxmcde = 'GM'
  and a.fxmcyy = 2015  
  and a.fxmpge = 16
  and c.active = 'Y'
 group by coalesce(b.consolidation_grp, '1'), a.fxmpge, a.fxmlne, fxmcol, fxmstr
 order by coalesce(b.consolidation_grp, '1'), a.fxmpge, a.fxmlne


  -- all, group by store, gm_account, page, line, column, start pos
select coalesce(b.consolidation_grp, '1'), a.fxmact, a.fxmpge, a.fxmlne, fxmcol, fxmstr, sum(d.gttamt) as gttamt
from test.ext_Arkona_sypffxmst a
inner join test.ext_arkona_ffpxrefdta b on a.fxmact = b.factory_account
  and b.factory_financial_year = 2015
inner join test.ext_arkona_glpmast c on b.g_l_acct_number = c.account_number  
  and c.year = 2015
--  and c.account_type = '4'
inner join test.ext_glptrns_1205 d on c.account_number = d.gtacct
  and d.gtdate between '11/01/2015' and '11/30/2015' 
  and d.gtpost <> 'V' -----------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
where a.fxmcde = 'GM'
  and a.fxmcyy = 2015  
  --and a.fxmpge = 16
  and c.active = 'Y'
 group by coalesce(b.consolidation_grp, '1'), a.fxmact, a.fxmpge, a.fxmlne, fxmcol, fxmstr
 order by coalesce(b.consolidation_grp, '1'), a.fxmpge, a.fxmlne, a.fxmstr


 select *
 from test.ext_arkona_sypffxmst
 where fxmcyy = 2015
   and fxmcde = 'GM'
   and left(fxmact,1) between '0' and '9' order by fxmact


select *
from test.ext_arkona_ffpxrefdta   
where factory_financial_year = 2015
  and consolidation_grp is null 
order by factory_account


select a.fxmact, a.fxmpge, a.fxmlne, fxmcol, fxmstr, b.g_l_acc_number, b.factory_account
from (
   select *
   from test.ext_arkona_sypffxmst
   where fxmcyy = 2015
     and fxmcde = 'GM'
     and left(fxmact,1) between '0' and '9' order by fxmact) a
full outer join (
  select *
  from test.ext_arkona_ffpxrefdta   
  where factory_financial_year = 2015
    and consolidation_grp is null 
  order by factory_account) b on a.fxmact = b.factory_account

   