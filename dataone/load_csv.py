# encoding=utf-8
"""
the copy string for dao.LKP_VEH_OEM_OPTION (file has quoted empty string for null):
    copy dao.LKP_VEH_OEM_OPTION from  stdin with (FORMAT csv, header, FORCE_NULL(oem_option_msrp,oem_option_invoice))
the copy string for dao.DEF_OEM_OPTION (file has " as inches):
    copy dao.DEF_OEM_OPTION from stdin with (FORMAT csv, header, escape '\\')
"""
import db_cnx

pg_con = None
file_name = 'data/DEF_ENGINE.csv'

try:
    with open(file_name, 'r') as io:
        with db_cnx.pg() as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.copy_expert("""copy dao.DEF_ENGINE from stdin with csv header""", io)
except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()
