﻿

do
$$
declare
  vin citext := '1GCUYGED5KZ147475';
  vin_patt citext :=  left(vin, 8) || substring(vin, 10, 1) || substring(vin, 11, 1);
begin
drop table if exists wtf;
create temp table wtf as
select *
from dao.vin_reference
where vin_pattern = vin_patt;
end
$$;
select * from wtf;


select *
from dao.vin_reference
where year = 2018
and model = 'silverado 1500'