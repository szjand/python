﻿
drop table if exists opts;
create temp table opts as
select a.vehicle_id, a.vin_pattern, a.style, a.trim, a.body_subtype, a.engine_size, 
  aa.mfr_model_num, aa.mfr_package_code,
  c.oem_option_code, c.oem_option_name, d.equipment_category_group_name, d.equipment_category_name
from dao.vin_reference a
inner join dao.veh_trim_styles aa on a.vehicle_id = aa.vehicle_id
left join dao.lkp_veh_oem_option b on a.vehicle_id = b.vehicle_id
left join dao.def_oem_option c on b.oem_option_id = c.oem_option_id
left join dao.def_equipment_category d on c.equipment_category_id = d.equipment_category_id
where a.year = 2018
  and a.model = 'silverado 1500'

select vehicle_id, vin_pattern, count(*)
from opts  
group by vehicle_id, vin_pattern
order by count(*)
select *
from opts
order by vehicle_id, vin_pattern, equipment_category_group_name, equipment_category_name

select *
from opts
order by oem_option_name

select *
from opts
where oem_option_name like '%cajun red%'
  and equipment_category_group_name = 'exterior'
  and equipment_category_name = 'exterior features'

  


select *
from (
select concat(left(a.vin,8), substring(a.vin,10,2)) as vin_pattern, c.option_code
from gmgl.sold_vehicles a
inner join gmgl.models b on a.model = b.model_code
inner join gmgl.sold_vehicle_options c on a.vin = c.vin
where a.vin = '1GCVKREC1JZ235723' ) a
left join opts b on a.vin_pattern = b.vin_pattern and a.option_code = b.oem_option_code
where b.oem_option_code is not null


select *
from dao.vin_Reference
where year = 2018
  and model = 'silverado 1500'



SELECT * FROM pg_stat_activity WHERE wait_event IS NOT NULL;





  
select *
from dao.def_oem_option
where oem_option_code in ('K34','VJH','NE8','VRK','8X2','DL8','KG4','SLM','TDM','V76')
order by oem_option_code


select a.*, b.vin_pattern, b.year, b.make, b.model, b.style
from dao.lkp_veh_oem_option a
left join dao.vin_reference b on a.vehicle_id = b.vehicle_id
where oem_option_id in (
  select oem_option_id
  from dao.def_oem_option
  where oem_option_code = 'V76')


-- after talking to taylor, realized, a package is just an option code
-- and there are 16412 of them
select *
from dao.def_oem_option
where oem_option_name like '%packa%'

-- back to my silverados


select a.*, b.vin_pattern, b.year, b.make, b.model, b.style
from dao.lkp_veh_oem_option a
left join dao.vin_reference b on a.vehicle_id = b.vehicle_id
where oem_option_id in (
  select oem_option_id
  from dao.def_oem_option
  where oem_option_code = 'V76')



drop table if exists opts;
create temp table opts as
select a.vehicle_id, a.vin_pattern, a.style, a.trim, a.body_subtype, a.engine_size, 
  aa.mfr_model_num, aa.mfr_package_code,
  c.oem_option_code, c.oem_option_name, c.oem_option_description, 
  d.equipment_category_group_name, d.equipment_category_name
from dao.vin_reference a
inner join dao.veh_trim_styles aa on a.vehicle_id = aa.vehicle_id
inner join dao.lkp_veh_oem_option b on a.vehicle_id = b.vehicle_id
inner join dao.def_oem_option c on b.oem_option_id = c.oem_option_id
  and c.oem_option_name like '%packa%'
inner join dao.def_equipment_category d on c.equipment_category_id = d.equipment_category_id
where a.year = 2018
  and a.model = 'silverado 1500'

-- ok, here are all the packages for 2018 silverados
select distinct oem_option_code, oem_option_name, oem_option_description
from opts  
order by oem_option_name

select distinct oem_option_code, oem_option_name, oem_option_description
from opts  
where oem_option_description like '%wind%'
order by oem_option_name


select vin_pattern, trim, body_subtype, engine_size, mfr_model_num, oem_option_code, oem_option_name, oem_option_description
-- select *
from opts
group by vin_pattern, trim, body_subtype, engine_size, mfr_model_num, mfr_package_code, oem_option_code, oem_option_name, oem_option_description
order by oem_option_code, vin_pattern, body_subtype, engine_size, trim










