﻿select a.vehicle_id, a.trim, a.oem_body_style, a.mfr_model_num, a.mfr_package_code, 
  c.mfr_color_code, c.mfr_color_name, c.color_name
from dao.veh_trim_styles a
left join dao.lkp_veh_ext_color b on a.vehicle_id = b.vehicle_id
left join dao.def_ext_color c on b.ext_color_id = c.ext_color_id
where a.year = 2016
  and a.model = 'silverado 1500'
order by a.vehicle_id, c.mfr_color_code


select a.vehicle_id, a.trim, a.oem_body_style, a.mfr_model_num, a.mfr_package_code, 
  string_agg(c.mfr_color_code || ': ' ||c.mfr_color_name|| ' - ' || c.color_name, ' ')
from dao.veh_trim_styles a
left join dao.lkp_veh_ext_color b on a.vehicle_id = b.vehicle_id
left join dao.def_ext_color c on b.ext_color_id = c.ext_color_id
where a.year = 2018
  and a.model = 'silverado 1500'
group by a.vehicle_id, a.trim, a.oem_body_style, a.mfr_model_num, a.mfr_package_code

