﻿select vin, left(vin, 8) || substring(vin, 10, 1) || substring(vin, 11, 1)
-- select *
from ads.ext_vehicle_items
where vin = '1FTEW1EG3JFA44766'

-- returns 6 vehicle_ids
select *
from dao.vin_reference  
where vin_pattern = (
  select left(vin, 8) || substring(vin, 10, 1) || substring(vin, 11, 1)
  from ads.ext_vehicle_items
  where vin = '1FTEW1EG3JFA44766')

-- chrome returns 11 styles
select *
from chr.describe_vehicle 
where vin = '1GCUYGED5KZ147475'

-- nothing relevant in veh_trim_styles that doesn't exist in vin_reference
select a.year, a.make, a.model, a.trim, a.style, a.mfr_model_num, a.mfr_package_code, b.*
from dao.vin_reference  a
left join dao.veh_trim_styles b on a.vehicle_id = b.vehicle_id
where a.vin_pattern = (
  select left(vin, 8) || substring(vin, 10, 1) || substring(vin, 11, 1)
  from ads.ext_vehicle_items
  where vin = '1FTEW1EG3JFA44766')

-- hmmm, ok, trim matches to a single instance
select a.year, a.make, a.model, a.trim, a.style, a.mfr_model_num, a.mfr_package_code, b.*
from dao.vin_reference  a
inner join ads.ext_vehicle_items b on a.vin_pattern = left(b.vin, 8) || substring(b.vin, 10, 1) || substring(b.vin, 11, 1)
  and b.vin = '1FTEW1EG3JFA44766'


-- lets get a chevy
select *
from ads.ext_vehicle_items
where yearmodel = '2018'
  and model = 'silverado 1500'

-- yuck
select a.year, a.make, a.model, a.trim, a.style, a.mfr_model_num, a.mfr_package_code, b.*
from dao.vin_reference  a
inner join ads.ext_vehicle_items b on a.vin_pattern = left(b.vin, 8) || substring(b.vin, 10, 1) || substring(b.vin, 11, 1)
  and b.vin in('3GCUKSECXJG175296','1GCVKREC1JZ181792','3GCUKRECXJG317705')




  