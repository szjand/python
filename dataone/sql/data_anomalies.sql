﻿select *
from dao.def_evox_image
where evox_angle_id = 47

select *
from dao.def_evox_angle
where evox_angle_id = 47

select * -- 2 without records, so cardinality is 0 to many
from dao.def_evox_angle a
where not exists (
  select 1
  from dao.def_evox_image
  where evox_angle_id = a.evox_angle_id)


select *
from dao.lkp_veh_optional_specification
where specification_id = 229993

select *
from dao.def_specification
where specification_id = 229993


select specification_id, count(*) -- 80
from dao.lkp_veh_optional_specification a
where not exists (
  select 1
  from dao.def_specification
  where specification_id = a.specification_id)
group by specification_id  

select count(*) from dao.lkp_veh_optional_specification -- 128094

select evox_angle_id, count(*) --64025
from dao.def_evox_image a
where not exists (
  select 1
  from dao.def_evox_angle
  where evox_angle_id = a.evox_angle_id)
group by evox_angle_id  

select count(*) from dao.def_evox_image --1552848