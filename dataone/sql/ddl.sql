﻿

drop table if exists dao.veh_price cascade;
CREATE TABLE  dao.veh_price  (
   veh_price_id  integer primary key,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   msrp  integer NOT NULL,
   invoice  integer NOT NULL,
   dest  integer NOT NULL,
   gas_guzzler_tax  integer NOT NULL);
create unique index on dao.veh_price(vehicle_id);   

drop table if exists dao.VEH_TRIM_STYLES cascade;
CREATE TABLE  dao.VEH_TRIM_STYLES  (
   vehicle_id  integer primary key,
   style_complete  boolean NOT NULL,
   fleet  citext NOT NULL,
   year  integer   NOT NULL,
   make  citext NOT NULL,
   model  citext NOT NULL,
   trim  citext NOT NULL,
   drive_type  citext NOT NULL,
   style  citext NOT NULL,
   vehicle_type  citext NOT NULL,
   body_type  citext NOT NULL,
   body_subtype  citext NOT NULL,
   oem_body_style  citext NOT NULL,
   doors  integer NOT NULL,
   oem_doors  integer NOT NULL,
   mfr_model_num  citext NOT NULL,
   mfr_package_code  citext NOT NULL);
create unique index on dao.veh_trim_styles(year,make,model,style);

drop table if exists dao.LKP_VEH_ENG cascade;
CREATE TABLE  dao.LKP_VEH_ENG  (
   veh_eng_id  integer primary key,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   engine_id  integer NOT NULL references dao.def_engine(engine_id),
   marketing_name  citext NOT NULL,
   engine_code  citext NOT NULL,
   invoice_price  integer NOT NULL,
   msrp_price  integer NOT NULL,
   max_payload  citext NOT NULL,
   standard  boolean NOT NULL);
create unique index on dao.lkp_veh_eng(vehicle_id,engine_id);

drop table if exists dao.DEF_ENGINE cascade;
CREATE TABLE  dao.DEF_ENGINE  (
   engine_id  integer primary key,
   engine_name  citext NOT NULL,
   brand_name  citext NOT NULL,
   engine_type  citext NOT NULL,
   fuel_type  citext NOT NULL,
   fuel_quality  citext NOT NULL,
   oil_capacity  numeric(5,1) NOT NULL,
   electric_motor_configuration  citext NOT NULL,
   generator_description  citext NOT NULL,
   generator_max_hp  integer NOT NULL,
   ice_max_hp  integer NOT NULL,
   ice_max_hp_at  integer NOT NULL,
   ice_max_torque  integer NOT NULL,
   ice_max_torque_at  integer NOT NULL,
   electric_max_hp  integer NOT NULL,
   electric_max_kw  integer NOT NULL,
   electric_max_torque  integer NOT NULL,
   total_max_hp  integer NOT NULL,
   total_max_hp_at  integer NOT NULL,
   total_max_torque  integer NOT NULL,
   total_max_torque_at  integer NOT NULL,
   redline  integer NOT NULL,
   ice_displacement  numeric(5,1)   NOT NULL,
   ice_block_type  citext NOT NULL,
   ice_cylinders  integer NOT NULL,
   ice_valves  integer NOT NULL,
   ice_aspiration  citext NOT NULL,
   ice_fuel_induction  citext NOT NULL,
   ice_cam_type  citext NOT NULL,
   ice_bore  numeric(5,2) NOT NULL,
   ice_stroke  numeric(5,2) NOT NULL,
   ice_valve_timing  citext NOT NULL,
   ice_compression  numeric(5,1) NOT NULL);

drop table if exists dao.LKP_VEH_TRANS cascade;
CREATE TABLE  dao.LKP_VEH_TRANS  (
   veh_trans_id  integer primary key,
   vehicle_id  integer NOT NULL,
   trans_id  integer NOT NULL,
   standard  boolean NOT NULL);
create unique index on dao.lkp_veh_trans(vehicle_id,trans_id);  

drop table if exists dao.DEF_TRANSMISSION cascade;
CREATE TABLE  dao.DEF_TRANSMISSION  (
   trans_id  integer primary key,
   trans_name  citext NOT NULL,
   trans_brand  citext NOT NULL,
   trans_type  citext NOT NULL,
   trans_detail_type  citext NOT NULL,
   trans_gears  integer NOT NULL);

drop table if exists dao.LKP_VEH_EXT_COLOR cascade;
CREATE TABLE  dao.LKP_VEH_EXT_COLOR  (
   veh_ext_color_id  integer primary key,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   ext_color_id  integer NOT NULL references dao.def_ext_color(ext_color_id));
create unique index on dao.lkp_veh_ext_color(vehicle_id,ext_color_id);   

drop table if exists dao.DEF_EXT_COLOR cascade;
CREATE TABLE  dao.DEF_EXT_COLOR  (
   ext_color_id  integer primary key,
   color_name  citext NOT NULL,
   mfr_color_name  citext NOT NULL,
   mfr_color_code  citext NOT NULL,
   two_tone  boolean NOT NULL,
   r_code  integer NOT NULL,
   g_code  integer NOT NULL,
   b_code  integer NOT NULL,
   r_code_2  integer NOT NULL,
   g_code_2  integer NOT NULL,
   b_code_2  integer NOT NULL);

drop table if exists dao.LKP_VEH_INT_COLOR cascade;
CREATE TABLE  dao.LKP_VEH_INT_COLOR  (
   veh_int_color_id  integer primary key,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   int_color_id  integer NOT NULL references dao.def_int_color(int_color_id));
create unique index on dao.lkp_veh_int_color(vehicle_id,int_color_id);

drop table if exists dao.DEF_INT_COLOR cascade;
CREATE TABLE  dao.DEF_INT_COLOR  (
   int_color_id  integer primary key,
   color_name  citext NOT NULL,
   mfr_color_name  citext NOT NULL,
   mfr_color_code  citext NOT NULL,
   two_tone  boolean NOT NULL,
   fabric_type  citext NOT NULL,
   r_code  integer NOT NULL,
   g_code  integer NOT NULL,
   b_code  integer NOT NULL,
   r_code_2  integer NOT NULL,
   g_code_2  integer NOT NULL,
   b_code_2  integer NOT NULL);

drop table if exists dao.LKP_VEH_ROOF_COLOR cascade;
CREATE TABLE  dao.LKP_VEH_ROOF_COLOR  (
   veh_roof_color_id  integer primary key,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   roof_color_id  integer NOT NULL references dao.def_roof_color(roof_color_id));
create unique index on dao.lkp_veh_roof_color(vehicle_id,roof_color_id);

drop table if exists dao.DEF_ROOF_COLOR cascade;
CREATE TABLE  dao.DEF_ROOF_COLOR  (
   roof_color_id  integer primary key,
   color_name  citext NOT NULL,
   mfr_color_name  citext NOT NULL,
   mfr_color_code  citext NOT NULL,
   two_tone  boolean NOT NULL,
   fabric_type  citext NOT NULL,
   r_code  integer NOT NULL,
   g_code  integer NOT NULL,
   b_code  integer NOT NULL,
   r_code_2  integer NOT NULL,
   g_code_2  integer NOT NULL,
   b_code_2  integer NOT NULL);

drop table if exists dao.VIN_REFERENCE cascade;
CREATE TABLE  dao.VIN_REFERENCE  (
   vin_id  integer primary key,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   vin_pattern  citext NOT NULL,
   year  integer NOT NULL,
   make  citext NOT NULL,
   model  citext NOT NULL,
   trim  citext NOT NULL,
   style  citext NOT NULL,
   mfr_model_num  citext NOT NULL,
   mfr_package_code  citext NOT NULL,
   doors  integer NOT NULL,
   drive_type  citext NOT NULL,
   vehicle_type  citext NOT NULL,
   rear_axle  citext NOT NULL,
   body_type  citext NOT NULL,
   body_subtype  citext NOT NULL,
   bed_length  citext NOT NULL,
   engine_id  integer NOT NULL,
   engine_name  citext NOT NULL,
   engine_size  numeric(2,1) NOT NULL,
   engine_block  citext NOT NULL,
   engine_cylinders  integer NOT NULL,
   engine_valves  integer NOT NULL,
   engine_induction  citext NOT NULL,
   engine_aspiration  citext NOT NULL,
   engine_cam_type  citext NOT NULL,
   fuel_type  citext NOT NULL,
   trans_id  integer NOT NULL,
   trans_name  citext NOT NULL,
   trans_type  citext NOT NULL,
   trans_speeds  integer NOT NULL,
   wheelbase  float   NOT NULL,
   gross_vehicle_weight_range  citext NOT NULL,
   restraint_type  citext NOT NULL,
   brake_system  citext NOT NULL,
   country_of_mfr  citext NOT NULL,
   plant  citext NOT NULL);
create unique index on dao.vin_reference(vehicle_id,vin_pattern);   

drop table if exists dao.LKP_VEH_WARRANTY cascade;
CREATE TABLE  dao.LKP_VEH_WARRANTY  (
   veh_warranty_id  integer primary key,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   warranty_id  integer NOT NULL references dao.def_warranty(warranty_id));
create unique index on dao.lkp_veh_warranty(vehicle_id,warranty_id);

drop table if exists dao.DEF_WARRANTY cascade;
CREATE TABLE  dao.DEF_WARRANTY  (
   warranty_id  integer primary key,
   warranty_type  citext NOT NULL,
   warranty_name  citext NOT NULL,
   warranty_months  integer   NOT NULL,
   warranty_miles  integer   NOT NULL);

drop table if exists dao.LKP_VEH_MPG cascade;
CREATE TABLE  dao.LKP_VEH_MPG  (
   veh_mpg_id   integer primary key,
   vehicle_id   integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   engine_id   integer NOT NULL,
   transmission_id   integer NOT NULL,
   fuel_type  citext NOT NULL,
   fuel_grade  citext NOT NULL,
   city_old  integer NOT NULL,
   highway_old  integer NOT NULL,
   combined_old   integer NOT NULL,
   city  integer NOT NULL,
   highway  integer NOT NULL,
   combined  integer NOT NULL);
create unique index on dao.lkp_veh_mpg(veh_mpg_id,vehicle_id);   

drop table if exists dao.LKP_VEH_MODEL_NUMBER cascade;
CREATE TABLE  dao.LKP_VEH_MODEL_NUMBER  (
   veh_mfr_model_num_id  integer NOT NULL,
   vehicle_id   integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   mfr_model_num  citext NOT NULL,
   constraint lkp_vehi_model_number_pkey primary key (vehicle_id,mfr_model_num));

drop table if exists dao.LKP_VEH_EVOX_STUDIO640_SINGLE_IMAGE cascade;
CREATE TABLE  dao.LKP_VEH_EVOX_STUDIO640_SINGLE_IMAGE  (
   vehicle_id  integer primary key references dao.veh_trim_styles(vehicle_id),
   evox_image_id  integer NOT NULL);

drop table if exists dao.LKP_VEH_EVOX_COLORIZED640_SINGLE_IMAGE cascade;
CREATE TABLE  dao.LKP_VEH_EVOX_COLORIZED640_SINGLE_IMAGE  (
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   ext_color_id  integer NOT NULL,
   roof_color_id  integer NOT NULL,
   evox_image_id  integer NOT NULL,
   constraint LKP_VEH_EVOX_COLORIZED640_SINGLE_IMAGE_pkey primary key (vehicle_id , ext_color_id , roof_color_id , evox_image_id));

drop table if exists dao.DEF_EVOX_ANGLE cascade;
CREATE TABLE  dao.DEF_EVOX_ANGLE  (
   evox_angle_id  integer primary key,
   angle_description  citext NOT NULL);

drop table if exists dao.DEF_EVOX_IMAGE cascade;
CREATE TABLE  dao.DEF_EVOX_IMAGE  (
   evox_image_id  integer primary key,
   evox_vif  integer NOT NULL,
   evox_angle_id  integer NOT NULL,-- references dao.def_evox_angle(evox_angle_id),
   evox_collection  citext NOT NULL,
   file_format  citext NOT NULL,
   image_name  citext NOT NULL,
   image_path  citext NOT NULL);
create unique index on dao.def_evox_image(evox_image_id,evox_angle_id);   

drop table if exists dao.LKP_VEH_EVOX cascade;
CREATE TABLE  dao.LKP_VEH_EVOX  (
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   evox_vif  citext NOT NULL,
   match_year  boolean NOT NULL,
   match_model  boolean NOT NULL,
   match_trim  boolean NOT NULL,
   match_body_type  boolean NOT NULL,
   match_cab_type  boolean NOT NULL,
   match_doors  boolean NOT NULL,
   match_drive_type  boolean NOT NULL,
   constraint lkp_veh_evox_pkey primary key (vehicle_id,evox_vif));
comment on table dao.LKP_VEH_EVOX is 'Contains lookup records between vehicle_ids and eVox’s VIF number. 
  Allows those using both DataOne vehicle data and eVox imagery to directly link DataOne vehicles 
  to eVox images. Matches are generally based on model and vehicle type, and may not be trim specific.';

drop table if exists dao.DEF_SPECIFICATION cascade;  
CREATE TABLE  dao.DEF_SPECIFICATION  (
   specification_id  integer primary key,
   specification_category  citext NOT NULL,
   specification_name  citext NOT NULL,
   specification_value  citext NOT NULL,
   is_ancillary  boolean not null);

drop table if exists dao.LKP_VEH_STANDARD_SPECIFICATION cascade;
CREATE TABLE  dao.LKP_VEH_STANDARD_SPECIFICATION  (
   veh_specification_id  integer NOT NULL,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   specification_id  integer NOT NULL references dao.def_specification(specification_id),
   constraint LKP_VEH_STANDARD_SPECIFICATION_pkey primary key(vehicle_id,specification_id));

drop table if exists dao.LKP_VEH_OPTIONAL_SPECIFICATION cascade;
CREATE TABLE  dao.LKP_VEH_OPTIONAL_SPECIFICATION  (
   veh_specification_id  integer NOT NULL,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   specification_id  integer NOT NULL references dao.def_specification(specification_id),
   constraint LKP_VEH_OPTIONAL_SPECIFICATION_pkey primary key (vehicle_id,specification_id));

drop table if exists dao.DEF_EQUIPMENT_CATEGORY cascade;
CREATE TABLE  dao.DEF_EQUIPMENT_CATEGORY  (
   equipment_category_id  integer primary key,
   equipment_category_group_name  citext NOT NULL,
   equipment_category_name  citext NOT NULL);

drop table if exists dao.DEF_GENERIC_EQUIPMENT cascade;
CREATE TABLE  dao.DEF_GENERIC_EQUIPMENT  (
   generic_equipment_id  integer primary key,
   equipment_category_id  integer NOT NULL,
   generic_equipment_name  citext NOT NULL,
   generic_equipment_value  citext NOT NULL);

drop table if exists dao.LKP_VEH_STANDARD_GENERIC_EQUIPMENT cascade;
CREATE TABLE  dao.LKP_VEH_STANDARD_GENERIC_EQUIPMENT  (
   veh_standard_generic_equipment_id  integer NOT NULL,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   generic_equipment_id  integer NOT NULL references dao.def_generic_equipment(generic_equipment_id),
   constraint LKP_VEH_STANDARD_GENERIC_EQUIPMENT_pkey primary key(veh_standard_generic_equipment_id));

drop table if exists dao.LKP_VEH_OPTIONAL_GENERIC_EQUIPMENT cascade;
CREATE TABLE dao.LKP_VEH_OPTIONAL_GENERIC_EQUIPMENT  (
   veh_optional_generic_equipment_id  integer NOT NULL,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   generic_equipment_id  integer NOT NULL references dao.def_generic_equipment(generic_equipment_id),
   constraint LKP_VEH_OPTIONAL_GENERIC_EQUIPMENT_pkey primary key(vehicle_id , generic_equipment_id));   

drop table if exists dao.DEF_OEM_OPTION cascade;
CREATE TABLE  dao.DEF_OEM_OPTION  (
   oem_option_id  integer primary key,
   oem_option_code  citext NOT NULL,
   equipment_category_id  integer NOT NULL,
   oem_option_name  citext NOT NULL,
   oem_option_description  citext NOT NULL,
   oem_option_type  citext NOT NULL);

drop table if exists dao.LKP_VEH_OEM_OPTION cascade;
CREATE TABLE  dao.LKP_VEH_OEM_OPTION  (
   veh_oem_option_id  integer primary key,
   vehicle_id  integer NOT NULL references dao.veh_trim_styles(vehicle_id),
   oem_option_id  integer  NOT NULL references dao.def_oem_option(oem_option_id),
   oem_option_msrp   integer,
   oem_option_invoice   integer,
   oem_option_fleet  boolean NOT NULL);
create unique index on dao.lkp_veh_oem_option(oem_option_id,vehicle_id);   



