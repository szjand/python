﻿CREATE TABLE `VEH_PRICE` (
  `veh_price_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `msrp` int(10) unsigned NOT NULL DEFAULT '0',
  `invoice` int(10) unsigned NOT NULL DEFAULT '0',
  `dest` int(10) unsigned NOT NULL DEFAULT '0',
  `gas_guzzler_tax` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`veh_price_id`),
  KEY `vehicle_id` (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300170501 DEFAULT CHARSET=latin1;

CREATE TABLE `VEH_TRIM_STYLES` (
  `vehicle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `style_complete` enum('N','Y') NOT NULL DEFAULT 'N',
  `fleet` enum('Y','N','UK') NOT NULL DEFAULT 'N',
  `year` smallint(4) unsigned NOT NULL DEFAULT '0',
  `make` varchar(24) NOT NULL DEFAULT '',
  `model` varchar(32) NOT NULL DEFAULT '',
  `trim` varchar(32) NOT NULL DEFAULT '',
  `drive_type` varchar(10) NOT NULL DEFAULT '',
  `style` varchar(128) NOT NULL DEFAULT '',
  `vehicle_type` varchar(24) NOT NULL DEFAULT '',
  `body_type` varchar(32) NOT NULL DEFAULT '',
  `body_subtype` varchar(32) NOT NULL DEFAULT '',
  `oem_body_style` varchar(64) NOT NULL DEFAULT '',
  `doors` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `oem_doors` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mfr_model_num` varchar(32) NOT NULL DEFAULT '',
  `mfr_package_code` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=400903787 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_ENG` (
  `veh_eng_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) unsigned NOT NULL DEFAULT '0',
  `engine_id` int(11) unsigned NOT NULL DEFAULT '0',
  `marketing_name` varchar(128) NOT NULL,
  `engine_code` varchar(16) NOT NULL DEFAULT '',
  `invoice_price` mediumint(8) NOT NULL DEFAULT '0',
  `msrp_price` mediumint(8) NOT NULL DEFAULT '0',
  `max_payload` varchar(15) NOT NULL,
  `standard` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`veh_eng_id`),
  UNIQUE KEY `unique_id` (`vehicle_id`,`engine_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300067001 DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_ENGINE` (
  `engine_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `engine_name` varchar(128) NOT NULL DEFAULT '',
  `brand_name` varchar(64) NOT NULL DEFAULT '',
  `engine_type` varchar(48) NOT NULL DEFAULT '',
  `fuel_type` varchar(48) NOT NULL DEFAULT '',
  `fuel_quality` varchar(8) NOT NULL DEFAULT '',
  `oil_capacity` decimal(5,1) NOT NULL DEFAULT '0.0',
  `electric_motor_configuration` varchar(16) NOT NULL DEFAULT '',
  `generator_description` varchar(128) NOT NULL DEFAULT '',
  `generator_max_hp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ice_max_hp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ice_max_hp_at` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ice_max_torque` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ice_max_torque_at` smallint(5) unsigned NOT NULL DEFAULT '0',
  `electric_max_hp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `electric_max_kw` smallint(5) unsigned NOT NULL DEFAULT '0',
  `electric_max_torque` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_max_hp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_max_hp_at` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_max_torque` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_max_torque_at` smallint(5) unsigned NOT NULL DEFAULT '0',
  `redline` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ice_displacement` decimal(5,1) unsigned NOT NULL DEFAULT '0.0',
  `ice_block_type` char(1) NOT NULL DEFAULT '',
  `ice_cylinders` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ice_valves` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ice_aspiration` varchar(32) NOT NULL DEFAULT '',
  `ice_fuel_induction` varchar(64) NOT NULL DEFAULT '',
  `ice_cam_type` varchar(32) NOT NULL DEFAULT '',
  `ice_bore` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `ice_stroke` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `ice_valve_timing` varchar(8) NOT NULL DEFAULT '',
  `ice_compression` decimal(5,1) NOT NULL DEFAULT '0.0',
  PRIMARY KEY (`engine_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300002511 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_TRANS` (
  `veh_trans_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `trans_id` int(10) unsigned NOT NULL DEFAULT '0',
  `standard` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`veh_trans_id`),
  KEY `vehicle_id` (`vehicle_id`),
  KEY `vehicle_id_2` (`vehicle_id`,`trans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=98774 DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_TRANSMISSION` (
  `trans_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trans_name` varchar(64) NOT NULL DEFAULT '',
  `trans_brand` varchar(32) NOT NULL,
  `trans_type` char(3) NOT NULL,
  `trans_detail_type` varchar(32) NOT NULL,
  `trans_gears` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`trans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300000058 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_EXT_COLOR` (
  `veh_ext_color_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ext_color_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`veh_ext_color_id`),
  KEY `vehicle_id` (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300401050 DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_EXT_COLOR` (
  `ext_color_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color_name` varchar(12) NOT NULL DEFAULT '',
  `mfr_color_name` varchar(96) NOT NULL DEFAULT '',
  `mfr_color_code` varchar(24) NOT NULL DEFAULT '',
  `two_tone` enum('Y','N') NOT NULL DEFAULT 'N',
  `r_code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `g_code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `b_code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `r_code_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `g_code_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `b_code_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ext_color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300019995 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_INT_COLOR` (
  `veh_int_color_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `int_color_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`veh_int_color_id`),
  UNIQUE KEY `vehicle_id` (`vehicle_id`,`int_color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300138119 DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_INT_COLOR` (
  `int_color_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color_name` varchar(12) NOT NULL DEFAULT '',
  `mfr_color_name` varchar(96) NOT NULL DEFAULT '',
  `mfr_color_code` varchar(24) NOT NULL DEFAULT '',
  `two_tone` enum('Y','N') NOT NULL DEFAULT 'N',
  `fabric_type` varchar(64) NOT NULL DEFAULT '',
  `r_code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `g_code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `b_code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `r_code_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `g_code_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `b_code_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`int_color_id`),
  KEY `mfr_color_name` (`mfr_color_name`)
) ENGINE=InnoDB AUTO_INCREMENT=300020313 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_ROOF_COLOR` (
  `veh_roof_color_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `roof_color_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`veh_roof_color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300004993 DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_ROOF_COLOR` (
  `roof_color_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color_name` varchar(12) NOT NULL DEFAULT '',
  `mfr_color_name` varchar(96) NOT NULL DEFAULT '',
  `mfr_color_code` varchar(24) NOT NULL DEFAULT '',
  `two_tone` enum('Y','N') NOT NULL DEFAULT 'N',
  `fabric_type` varchar(24) NOT NULL DEFAULT '',
  `r_code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `g_code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `b_code` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `r_code_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `g_code_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `b_code_2` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`roof_color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300001012 DEFAULT CHARSET=latin1;

CREATE TABLE `VIN_REFERENCE` (
  `vin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) unsigned NOT NULL DEFAULT '0',
  `vin_pattern` varchar(10) NOT NULL DEFAULT '',
  `year` smallint(4) unsigned NOT NULL DEFAULT '0',
  `make` varchar(24) NOT NULL DEFAULT '',
  `model` varchar(32) NOT NULL DEFAULT '',
  `trim` varchar(48) NOT NULL DEFAULT '',
  `style` varchar(128) NOT NULL DEFAULT '',
  `mfr_model_num` varchar(12) NOT NULL DEFAULT '',
  `mfr_package_code` varchar(12) NOT NULL DEFAULT '',
  `doors` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `drive_type` varchar(3) NOT NULL DEFAULT '',
  `vehicle_type` varchar(24) NOT NULL DEFAULT '',
  `rear_axle` varchar(6) NOT NULL DEFAULT '',
  `body_type` varchar(32) NOT NULL DEFAULT '',
  `body_subtype` varchar(32) NOT NULL,
  `bed_length` varchar(8) NOT NULL DEFAULT '',
  `engine_id` int(11) unsigned NOT NULL DEFAULT '0',
  `engine_name` varchar(128) NOT NULL,
  `engine_size` float unsigned NOT NULL DEFAULT '0',
  `engine_block` char(1) NOT NULL,
  `engine_cylinders` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `engine_valves` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `engine_induction` varchar(32) NOT NULL,
  `engine_aspiration` varchar(32) NOT NULL,
  `engine_cam_type` varchar(8) NOT NULL,
  `fuel_type` varchar(12) NOT NULL DEFAULT '',
  `trans_id` int(11) unsigned NOT NULL DEFAULT '0',
  `trans_name` varchar(64) NOT NULL DEFAULT '',
  `trans_type` varchar(3) NOT NULL,
  `trans_speeds` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `wheelbase` float unsigned NOT NULL DEFAULT '0',
  `gross_vehicle_weight_range` varchar(20) NOT NULL,
  `restraint_type` varchar(255) NOT NULL DEFAULT '',
  `brake_system` varchar(18) NOT NULL DEFAULT '',
  `country_of_mfr` varchar(24) NOT NULL DEFAULT '',
  `plant` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`vin_id`),
  KEY `vehicle_id` (`vehicle_id`,`vin_pattern`,`year`,`make`,`model`,`trim`,`mfr_model_num`,`mfr_package_code`,`engine_id`,`trans_id`),
  KEY `vin_pattern` (`vin_pattern`)
) ENGINE=InnoDB AUTO_INCREMENT=300073086 DEFAULT CHARSET=utf8;

CREATE TABLE `LKP_VEH_WARRANTY` (
  `veh_warranty_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `warranty_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`veh_warranty_id`)
) ENGINE=InnoDB AUTO_INCREMENT=290171 DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_WARRANTY` (
  `warranty_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `warranty_type` varchar(32) NOT NULL DEFAULT '',
  `warranty_name` varchar(64) NOT NULL DEFAULT '',
  `warranty_months` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `warranty_miles` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`warranty_id`)
) ENGINE=InnoDB AUTO_INCREMENT=283 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_MPG` (
  `veh_mpg_id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `engine_id` int(11) NOT NULL,
  `transmission_id` int(11) NOT NULL,
  `fuel_type` varchar(64) NOT NULL,
  `fuel_grade` varchar(64) NOT NULL,
  `city_old` smallint(5) unsigned NOT NULL DEFAULT '0',
  `highway_old` smallint(5) unsigned NOT NULL DEFAULT '0',
  `combined_old` tinyint(4) NOT NULL,
  `city` smallint(5) unsigned NOT NULL DEFAULT '0',
  `highway` smallint(5) unsigned NOT NULL DEFAULT '0',
  `combined` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`veh_mpg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102519 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_MODEL_NUMBER` (
  `veh_mfr_model_num_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `mfr_model_num` varchar(32) NOT NULL,
  PRIMARY KEY (`vehicle_id`,`mfr_model_num`),
  KEY `veh_mfr_model_num_id` (`veh_mfr_model_num_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66439 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_EVOX_STUDIO640_SINGLE_IMAGE` (
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `evox_image_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_EVOX_COLORIZED640_SINGLE_IMAGE` (
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ext_color_id` int(10) unsigned NOT NULL DEFAULT '0',
  `roof_color_id` int(10) unsigned NOT NULL DEFAULT '0',
  `evox_image_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`vehicle_id`,`ext_color_id`,`roof_color_id`,`evox_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_EVOX_ANGLE` (
  `evox_angle_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `angle_description` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`evox_angle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_EVOX_IMAGE` (
  `evox_image_id` int(10) unsigned NOT NULL DEFAULT '0',
  `evox_vif` int(10) unsigned NOT NULL DEFAULT '0',
  `evox_angle_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `evox_collection` varchar(10) NOT NULL DEFAULT '',
  `file_format` varchar(4) NOT NULL DEFAULT '',
  `image_name` varchar(64) NOT NULL DEFAULT '',
  `image_path` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`evox_image_id`),
  KEY `evox_vif` (`evox_vif`,`evox_collection`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_EVOX` (
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `evox_vif` varchar(5) NOT NULL DEFAULT '0',
  `match_year` enum('Y','N') NOT NULL DEFAULT 'Y',
  `match_model` enum('Y','N') NOT NULL DEFAULT 'Y',
  `match_trim` enum('Y','N') NOT NULL DEFAULT 'Y',
  `match_body_type` enum('Y','N') NOT NULL DEFAULT 'Y',
  `match_cab_type` enum('Y','N') NOT NULL DEFAULT 'Y',
  `match_doors` enum('Y','N') NOT NULL DEFAULT 'Y',
  `match_drive_type` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`vehicle_id`,`evox_vif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_SPECIFICATION` (
  `specification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `specification_category` varchar(32) NOT NULL DEFAULT '',
  `specification_name` varchar(32) NOT NULL DEFAULT '',
  `specification_value` varchar(32) NOT NULL DEFAULT '',
  `is_ancillary` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`specification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_STANDARD_SPECIFICATION` (
  `veh_specification_id` int(10) unsigned NOT NULL,
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `specification_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`vehicle_id`,`specification_id`),
  KEY `specification_id` (`specification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_OPTIONAL_SPECIFICATION` (
  `veh_specification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `specification_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`vehicle_id`,`specification_id`),
  KEY `specification_id` (`specification_id`),
  KEY `veh_specification_id` (`veh_specification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=131071 DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_EQUIPMENT_CATEGORY` (
  `equipment_category_id` int(1) NOT NULL AUTO_INCREMENT,
  `equipment_category_group_name` varchar(48) NOT NULL,
  `equipment_category_name` varchar(64) NOT NULL,
  PRIMARY KEY (`equipment_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_GENERIC_EQUIPMENT` (
  `generic_equipment_id` int(10) unsigned NOT NULL,
  `equipment_category_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `generic_equipment_name` varchar(255) NOT NULL DEFAULT '',
  `generic_equipment_value` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`generic_equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_STANDARD_GENERIC_EQUIPMENT` (
  `veh_standard_generic_equipment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) unsigned NOT NULL DEFAULT '0',
  `generic_equipment_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`veh_standard_generic_equipment_id`),
  KEY `vehicle_id` (`vehicle_id`,`generic_equipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5675403 DEFAULT CHARSET=latin1;

CREATE TABLE `DEF_OEM_OPTION` (
  `oem_option_id` bigint(24) unsigned NOT NULL AUTO_INCREMENT,
  `oem_option_code` varchar(16) NOT NULL,
  `equipment_category_id` int(1) NOT NULL DEFAULT '0',
  `oem_option_name` varchar(255) NOT NULL,
  `oem_option_description` text NOT NULL,
  `oem_option_type` varchar(36) NOT NULL,
  PRIMARY KEY (`oem_option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300043287 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_OEM_OPTION` (
  `veh_oem_option_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) unsigned NOT NULL DEFAULT '0',
  `oem_option_id` bigint(24) unsigned NOT NULL DEFAULT '0',
  `oem_option_msrp` int(11) DEFAULT NULL,
  `oem_option_invoice` int(11) DEFAULT NULL,
  `oem_option_fleet` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`veh_oem_option_id`),
  KEY `vehicle_id` (`vehicle_id`,`oem_option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=301794092 DEFAULT CHARSET=latin1;

CREATE TABLE `LKP_VEH_OPTIONAL_GENERIC_EQUIPMENT` (
  `veh_optional_generic_equipment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(10) unsigned NOT NULL DEFAULT '0',
  `generic_equipment_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`vehicle_id`,`generic_equipment_id`),
  KEY `veh_optional_generic_equipment_id` (`veh_optional_generic_equipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1507306 DEFAULT CHARSET=latin1;

