﻿select * -- 168
from dao.vin_reference
where year = 2018
  and model = 'silverado 1500'
limit 100

select * -- 54
from dao.veh_trim_styles
where year = 2018
  and model = 'silverado 1500'


select * -- 54
from dao.veh_trim_styles a
left join dao.vin_Reference b on a.vehicle_id = b.vehicle_id
where a.year = 2018
  and a.model = 'silverado 1500'
order by a.vehicle_id  


-- NK
select vehicle_id, vin_pattern
from dao.vin_reference
group by vehicle_id, vin_pattern
having count(*) > 1

select *
-- select count(*) -- 54265
from dao.def_oem_option
limit 100



select a.vehicle_id, a.trim, a.oem_body_style, a.mfr_model_num, a.mfr_package_code, b.*, c.* -- 54/4663
-- select distinct c.oem_option_code, c.equipment_category_id, c.oem_option_name, c.oem_option_description, c.oem_option_type
from dao.veh_trim_styles a
left join dao.lkp_veh_oem_option b on a.vehicle_id = b.vehicle_id
left join dao.def_oem_option c on b.oem_option_id = c.oem_option_id
where a.year = 2018
  and a.model = 'silverado 1500' and equipment_category_id = 1

select *
from gmgl.vehicle_option_codes a
left join dao.
order by a.option_code

select *
from dao.def_Equipment_category


select vehicle_id
from dao.veh_price
group by vehicle_id 
having count(*) > 1

select *
from dao.veh_trim_styles
limit 100

select vehicle_type, body_type, count(*)
from dao.veh_trim_styles
group by vehicle_Type, body_type

--nk
select year, make, model, style
from dao.veh_trim_styles
group by year, make, model, style
having count(*) > 1

select *
from dao.lkp_veh_eng
limit 100
-- nk
select vehicle_id, engine_id
from dao.lkp_veh_eng
group by vehicle_id, engine_id
having count(*) > 1

select *
from dao.def_engine
limit 300

select engine_name
from dao.def_engine
group by engine_name
having count(*) > 1

select *
from dao.lkp_veh_trans
limit 100

select vehicle_id,trans_id
from dao.lkp_veh_trans
group by vehicle_id, trans_id
having count(*) > 1

select *
from dao.def_transmission

-- no natural key
select *
from dao.def_ext_color
where mfr_color_name = 'Pearl White/White Gold Clear Coat'
limit 500

select mfr_color_name, mfr_Color_code
from dao.def_ext_color
group by mfr_color_name, mfr_color_code
having count(*) > 1

select *
from dao.lkp_veh_ext_color
limit 500

-- nk
select vehicle_id, ext_color_id
from dao.lkp_veh_ext_color
group by vehicle_id, ext_color_id
having count(*) > 1

-- no nk
select *
from dao.DEF_INT_COLOR
where mfr_color_name = 'Charcoal' and mfr_color_code = 'K'
limit 500

-- nk
select vehicle_id, int_color_id
from dao.lkp_veh_int_color
group by vehicle_id, int_color_id
having count(*) > 1

-- nk
select vehicle_id, roof_color_id
from dao.lkp_veh_roof_color
group by vehicle_id, roof_color_id
having count(*) > 1

-- NK
select vehicle_id, vin_pattern
from dao.vin_reference
group by vehicle_id, vin_pattern
having count(*) > 1

select *
from dao.def_warranty
limit 500

-- nk
select vehicle_id, veh_warranty_id
from dao.lkp_veh_warranty
group by vehicle_id, veh_warranty_id
having count(*) > 1

-- nk
select vehicle_id, veh_mpg_id
from dao.lkp_veh_mpg
group by vehicle_id, veh_mpg_id
having count(*) > 1

select * 
from dao.lkp_veh_model_number
limit 500

--nk
select vehicle_id, veh_mfr_model_num_id
from dao.lkp_veh_model_number
group by vehicle_id, veh_mfr_model_num_id
having count(*) > 1

--nk
select evox_angle_id, evox_image_id
from dao.def_evox_image
group by evox_angle_id, evox_image_id
having count(*) > 1

select specification_category, specification_name, count(*)
from dao.def_specification
group by specification_category, specification_name

select vehicle_id, oem_option_id
from dao.lkp_veh_oem_option
group by vehicle_id, oem_option_id
having count(*) > 1



select *
from dao.veh_trim_styles a
left join dao.veh_price b on a.vehicle_id = b.vehicle_id
where b.vehicle_id is null 


select vehicle_id
from dao.veh_price
group by vehicle_id
having count(*) > 1

select *
from dao.vin_reference a
where not exists (
  select 1
  from dao.veh_trim_styles
  where a.vehicle_id

select *
from dao.veh_trim_styles a
where not exists (
  select 1
  from dao.vin_reference
  where vehicle_id = a.vehicle_id)

select vehicle_id
from dao.vin_reference
group by vehicle_id
having count(*) > 1
  
-----------------------------------------------------------------------------
-- load and truncate order
-----------------------------------------------------------------------------

create table dao.maint_load_order (
  table_name citext primary key,
  file_name citext,
  load_seq integer,
  truncate_seq integer);

SELECT 'insert into dao.maint_load_order (table_name, file_name) values(' || '''' || 'dao.' || table_name::text || '''' || ',''' || table_name::text || '.csv''' || ');'
  FROM information_schema.tables
 WHERE table_schema='dao'
   AND table_type='BASE TABLE'


insert into dao.maint_load_order (table_name, file_name) values('dao.veh_price','veh_price.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_eng','lkp_veh_eng.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_engine','def_engine.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_trans','lkp_veh_trans.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_transmission','def_transmission.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_ext_color','lkp_veh_ext_color.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_ext_color','def_ext_color.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_int_color','lkp_veh_int_color.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_int_color','def_int_color.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_roof_color','lkp_veh_roof_color.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_roof_color','def_roof_color.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.vin_reference','vin_reference.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_warranty','lkp_veh_warranty.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_warranty','def_warranty.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_mpg','lkp_veh_mpg.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_model_number','lkp_veh_model_number.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_evox_studio640_single_image','lkp_veh_evox_studio640_single_image.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_evox_colorized640_single_image','lkp_veh_evox_colorized640_single_image.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_evox_angle','def_evox_angle.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_evox_image','def_evox_image.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_evox','lkp_veh_evox.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_generic_equipment','def_generic_equipment.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_specification','def_specification.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_equipment_category','def_equipment_category.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_standard_generic_equipment','lkp_veh_standard_generic_equipment.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_optional_generic_equipment','lkp_veh_optional_generic_equipment.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.def_oem_option','def_oem_option.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_oem_option','lkp_veh_oem_option.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_standard_specification','lkp_veh_standard_specification.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.veh_trim_styles','veh_trim_styles.csv');
insert into dao.maint_load_order (table_name, file_name) values('dao.lkp_veh_optional_specification','lkp_veh_optional_specification.csv');



select * from dao.maint_load_order order by table_name


SELECT table_name
  FROM information_schema.tables
 WHERE table_schema='dao'
   AND table_type='BASE TABLE'

select *
from (
select 'def_engine' as table_name_name, count(*) from dao.def_engine
union
select 'lkp_veh_trans' as table_name, count(*) from dao.lkp_veh_trans
union
select 'lkp_veh_ext_color' as table_name, count(*) from dao.lkp_veh_ext_color
union
select 'lkp_veh_eng' as table_name, count(*) from dao.lkp_veh_eng
union
select 'veh_price' as table_name, count(*) from dao.veh_price
union
select 'def_transmission' as table_name, count(*) from dao.def_transmission
union
select 'lkp_veh_roof_color' as table_name, count(*) from dao.lkp_veh_roof_color
union
select 'lkp_veh_int_color' as table_name, count(*) from dao.lkp_veh_int_color
union
select 'vin_reference' as table_name, count(*) from dao.vin_reference
union
select 'def_roof_color' as table_name, count(*) from dao.def_roof_color
union
select 'def_int_color' as table_name, count(*) from dao.def_int_color
union
select 'def_ext_color' as table_name, count(*) from dao.def_ext_color
union
select 'lkp_veh_evox_studio640_single_image' as table_name, count(*) from dao.lkp_veh_evox_studio640_single_image
union
select 'lkp_veh_evox_colorized640_single_image' as table_name, count(*) from dao.lkp_veh_evox_colorized640_single_image
union
select 'lkp_veh_model_number' as table_name, count(*) from dao.lkp_veh_model_number
union
select 'def_warranty' as table_name, count(*) from dao.def_warranty
union
select 'def_generic_equipment' as table_name, count(*) from dao.def_generic_equipment
union
select 'lkp_veh_evox' as table_name, count(*) from dao.lkp_veh_evox
union
select 'def_specification' as table_name, count(*) from dao.def_specification
union
select 'def_evox_image' as table_name, count(*) from dao.def_evox_image
union
select 'def_evox_angle' as table_name, count(*) from dao.def_evox_angle
union
select 'def_equipment_category' as table_name, count(*) from dao.def_equipment_category
union
select 'maint_load_order' as table_name, count(*) from dao.maint_load_order
union
select 'lkp_veh_standard_specification' as table_name, count(*) from dao.lkp_veh_standard_specification
union
select 'lkp_veh_standard_generic_equipment' as table_name, count(*) from dao.lkp_veh_standard_generic_equipment
union
select 'lkp_veh_optional_specification' as table_name, count(*) from dao.lkp_veh_optional_specification
union
select 'lkp_veh_optional_generic_equipment' as table_name, count(*) from dao.lkp_veh_optional_generic_equipment
union
select 'lkp_veh_oem_option' as table_name, count(*) from dao.lkp_veh_oem_option
union
select 'def_oem_option' as table_name, count(*) from dao.def_oem_option
union
select 'veh_trim_styles' as table_name, count(*) from dao.veh_trim_styles) x
order by table_name_name
