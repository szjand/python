﻿-- starting with lkp_Veh_evox
-- first thing i notice is, the evox_vif does not always exist in either picture folder
select a.vehicle_id, a.trim, a.oem_body_style, a.mfr_model_num, a.mfr_package_code,
  b.evox_vif, c.*
-- select distinct b.evox_vif  
from dao.veh_trim_styles a
left join dao.lkp_veh_evox b on a.vehicle_id = b.vehicle_id
left join dao.def_evox_image c on b.evox_vif::integer = c.evox_vif
where a.year = 2018
  and a.model = 'silverado 1500'
order by a.vehicle_id


/*
ftp downloads
first, i am not crazy about the quality of the pics, we'll see what greg says
moved the 2 downloaded zip files into a folder named for the year
Evox_SingleColorizedPhotos_PNG640_2019_1.zip
  extracts into a folder named: color_0640_032_png
    which contains a folder for each evox_vif it contains which contains 1 picture for each color
Evox_AngularFrontStudioPhotos_PNG640_2019_1.zip
  extracts into a folder named: stills_0640_png
    which contains a folder for each evox_vif it contains which contains 1 picture

*/


lots of silverado evox_vif missing from stills_640
maybe problem is starting with lkp_veh_evox

-- looks the evox_vif is a diferent this way but still missing most for the silverados
-- these should be pointing to the \stills_640_png folder
select a.vehicle_id, a.trim, a.oem_body_style, a.mfr_model_num, a.mfr_package_code,
  c.*
from dao.veh_trim_styles a
left join dao.lkp_veh_evox_studio640_single_image b on a.vehicle_id = b.vehicle_id
left join dao.def_evox_image c on b.evox_image_id = c.evox_image_id
where a.year = 2018
  and a.model = 'silverado 1500'
order by a.vehicle_id

-- these are the pictures for each color in an evox_vif
-- and have pictures for all the silverados
-- point to the \color_640_032_png folder
select a.vehicle_id, a.trim, a.oem_body_style, a.mfr_model_num, a.mfr_package_code,
  c.*, d.*
from dao.veh_trim_styles a
left join dao.lkp_veh_evox_colorized640_single_image b on a.vehicle_id = b.vehicle_id
left join dao.def_evox_image c on b.evox_image_id = c.evox_image_id
left join dao.lkp_veh_evox d on a.vehicle_id = d.vehicle_id
where a.year = 2018
  and a.model = 'silverado 1500'
order by c.evox_vif--a.vehicle_id


----------------------------
  but pictures do not necessarily match trim as noted in the docs and exposed in table dal.lkp_veh_evox.match_trim attribute

select *
from dao.lkp_veh_evox
where evox_vif = '12347'