# coding=utf-8
import db_cnx
import ops
import string
import csv
# todo adjust date range for end of whatever current month is being populated, producion use curdate()
# 201602: 12/01/15 -> 02/29/16
# 201603: 01/01/2016 -> 03/31/2016
task = 'ext_email_capture'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_email_capture.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.ads_sco() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT a.transactiondate, c.employeenumber, a.transactionnumber,
                    hasvalidemail
                FROM cstfbemaildata a
                LEFT JOIN dds.factVehicleSale b on a.transactionnumber = b.stocknumber
                LEFT JOIN dds.dimSalesPerson c on b.consultantkey = c.salespersonkey
                WHERE a.transactiontype = 'sale'
                    AND a.transactionnumber not in ('','21008','21081a','23055a','23777c','29960',
                        '29959','29947','29986', '29884','29962', '29719','29932')
                --    AND a.transactiondate BETWEEN '11/02/2015' and '07/31/2016'
                    AND a.transactiondate BETWEEN curdate() - 90 and curdate()
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate scpp.ext_email_capture")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy scpp.ext_email_capture from stdin with (format csv)""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
