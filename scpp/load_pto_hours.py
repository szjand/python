# encoding=utf-8
import ops
import string
import db_cnx

# TODO what if an ops function fails
"""
1/11/17
    changed insert query to only return pto hours for the current month
"""
task = 'load_pto_hours'
pg_con = None
run_id = None

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                update scpp.pto_hours x
                set pto_hours = y.pto_hours
                from (
                  select a.employee_number, a.the_date, a.pto_hours
                  from scpp.ext_pto_hours a
                  inner join scpp.sales_consultants b on a.employee_number = b.employee_number) y
                where x.employee_number = y.employee_number
                  and x.the_date = y.the_date
            """
            pg_cur.execute(sql)
            sql = """
                insert into scpp.pto_hours (year_month,the_date,employee_number,pto_hours)
                with open_month as (
                  select *
                  from scpp.months
                  where open_closed = 'open')
                select a.year_month, a.the_date, a.employee_number, a.pto_hours
                from scpp.ext_pto_hours a
                inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
                  and b.year_month = (select year_month from open_month)
                where a.the_date between (select first_of_month from open_month) and
                    (select last_of_month from open_month)
                  AND not exists (
                    select 1
                    from scpp.pto_hours
                    where the_date = a.the_date
                      and employee_number = a.employee_number)
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
