# encoding=utf-8
import ops
import string
import db_cnx

# TODO what if an ops function fails

task = 'load_sales_consultants'
pg_con = None
run_id = None

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select count(*) as required
                from scpp.xfm_sales_consultants
            """
            pg_cur.execute(sql)
            if pg_cur.fetchone()[0] == 0:
                ops.log_pass(run_id, 'Not Required, nothing in xfm_sales_consultants')
                exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into scpp.sales_consultants (store_code, employee_number, last_name, first_name, full_name,
                  hire_date, term_date, user_name, ry1_id, ry2_id, first_full_month_employment)
                select store_code, employee_number, last_name, first_name, full_name,
                  hire_date, term_date, user_name, ry1_id, ry2_id, first_full_month_employment
                from scpp.xfm_sales_consultants
                where row_type = 'New'
            """
            pg_cur.execute(sql)
            sql = """
                insert into scpp.sales_consultant_configuration (employee_number, year_month, pay_plan_name,
                    draw, full_months_employment, years_service)
                select a.employee_number, b.year_month, 'Standard', 500,
                  case
                    when b.seq - c.seq < 0 then 0
                    else b.seq - c.seq - 1
                  end as full_months_employment,
                  (b.last_of_month - a.hire_date)/365 as years_service
                from scpp.xfm_sales_consultants a
                inner join scpp.months b on 1 = 1
                  and b.open_closed = 'open'
                -- inner join scpp.months c on a.hire_Date between c.first_of_month and c.last_of_month
                inner join scpp.months c on a.first_full_month_employment = c.year_month
                where a.row_type = 'New'
            """
            pg_cur.execute(sql)
            sql = """
                insert into scpp.sales_consultant_data (year_month,store_code,full_name,employee_number,
                  hire_date,pay_plan_name,draw,
                  full_months_employment,years_service,
                  store_name)
                select c.year_month, a.store_code, a.full_name, a.employee_number, a.hire_Date,
                b.pay_plan_name, b.draw, b.full_months_employment,
                b.years_service,
                case a.store_code
                  when 'RY1' then 'GM'
                  when 'RY2' then 'Honda Nissan'
                end as store_name
                from scpp.xfm_sales_consultants a
                inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
                inner join scpp.months c on b.year_month = c.year_month
                  and c.open_closed = 'open'
                where a.row_type = 'New'
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.sales_consultants x
                set first_name = z.first_name,
                    last_name = z.last_name,
                    full_name = z.full_name,
                    term_date = z.term_date,
                    user_name = z.user_name
                from (
                  select *
                  from scpp.xfm_sales_consultants
                  where row_type = 'Update') z
                where x.store_code = z.store_code
                  and x.ry1_id = z.ry1_id
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
