# encoding=utf-8
import ops
import string
import db_cnx

# the local variables referenced before assignment problem:
# http://stackoverflow.com/questions/31826483/pycharm-variable-assignment-not-recognized-inside-try-block
# https://www.toptal.com/python/top-10-mistakes-that-python-programmers-make
# https://docs.python.org/2/faq/programming.html#why-am-i-getting-an-unboundlocalerror-when-the-variable-has-a-value
# using with to create connection and cursor ensures they get closed
# requires string: switching to pyodbc from adsdb (too limited), error returns multipart exception
# with parts enclosed in single quotes, remove quotes to return error
# TODO what if an ops function fails
# todo *a* remove exclusion of RY1 and Finance paid consultants
# 1/5/17 *a*
#   exclude new hires for now, they don't go on the plan automatically
task = 'ext_sales_consultants'
# ads_con = None
pg_con = None
run_id = None
# file_name = 'files/ext_sales_consultants.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = "truncate scpp.ext_sales_consultants"
            pg_cur.execute(sql)
            sql = """
                insert into scpp.ext_sales_consultants
                select a.storecode, a.employeenumber, a.lastname, a.firstname, trim(a.firstname) || ' ' || a.lastname,
                  case
                    when (select dds.db2_integer_to_date(d.org_hire_date)) < a.hiredate
                      then (select dds.db2_integer_to_date(d.org_hire_date))
                    else a.hiredate
                  end,
                  a.termdate,
                  case
                    when a.employeenumber = '181665' then 'rkvasager@rydellcars.com'
                    else e.username
                  end,
                  b.sales_person_id as ry1_id, c.sales_person_id as ry2_id,
                  a.hiredate as last_hire_date,
                  case
                    -- latest hiredate <= first wkg day of the month then yearmonth of latest hiredate
                    when a.hiredate <= (-- aa.sc_first_working_day_of_month -- nope
                      select sc_first_working_day_of_month
                      from scpp.months
                      where year_month = extract(year from a.hiredate) * 100 + extract(month from a.hiredate))
                      then (
                        select year_month
                        from scpp.months
                        where a.hiredate between first_of_month and last_of_month)
                    else (
                    -- latest hiredate > first wkg day of the month then yearmonth of latest hiredate + 1
                      select year_month
                      from scpp.months
                      where seq = ( --aa.seq + 1)
                        select seq
                        from scpp.months
                        where year_month = extract(year from a.hiredate) * 100 + extract(month from a.hiredate)) + 1)
                  end,
                  (select dds.db2_integer_to_date(d.org_hire_date)) as orig_hire_date
                from ads.ext_dds_edwEmployeeDim a
                inner join scpp.months aa on 1 = 1
                  and aa.open_closed = 'open'
                left join dds.ext_bopslsp b on a.employeenumber = b.employee_number
                  and b.company_number = 'RY1'
                  and b.sales_person_type = 'S'
                left join dds.ext_bopslsp c on a.employeenumber = c.employee_number
                  and c.company_number = 'RY2'
                  and c.sales_person_type = 'S'
                left join dds.ext_pymast d on a.employeenumber  = d.pymast_employee_number
                left join ads.ext_sco_tpemployees e on a.employeenumber = e.employeenumber
                where a.currentrow = true
                  and a.termdate > aa.sc_first_working_day_of_month
                  and a.distcode = 'SALE'
                  and a.storecode = 'RY1'
                  and a.employeenumber not in (
                    '1130101', -- joshua solie new hire
                    '111232', -- nikolai holland new hire
                    '156845', -- brooke sabin new hire
                    '150040', -- chris garceau
                    '1106225', -- john olderbak
                    '161351', -- katie kuester (new digital)
                    '149507') -- tonya gable (new digital)
                    -- *a* new hires
            """
            pg_cur.execute(sql)
    #         with open(file_name, 'wb') as f:
    #             csv.writer(f).writerows(pg_cur.fetchall())
    # with db_cnx.pg() as pg_con:
    #     with pg_con.cursor() as pg_cur:
    #         pg_cur.execute("truncate scpp.ext_sales_consultants")
    #         with open(file_name, 'r') as io:
    #             pg_cur.copy_expert("""copy scpp.ext_sales_consultants from stdin with(format csv)""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
finally:
    # if ads_con:
    #     ads_con.close()
    if pg_con:
        pg_con.close()
