# encoding=utf-8
import ops
import string
import db_cnx
import datetime

# TODO what if an ops function fails

task = 'xfm_pto_intervals'
pg_con = None
run_id = None

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select count(*) as required
                from scpp.sales_consultant_configuration a
                inner join scpp.months b on a.year_month = b.year_month
                  and b.open_closed = 'open'
                where not exists (
                  select 1
                  from scpp.pto_intervals
                  where employee_number = a.employee_number
                    and year_month = b.year_month)
            """
            pg_cur.execute(sql)
            if pg_cur.fetchone()[0] == 0:
                # nothing to do, make sure xfm_pto_intervals is empty, so load_pto_intervals has nada to do
                sql = """ truncate scpp.xfm_pto_intervals """
                pg_cur.execute(sql)
                pg_con.commit()
                ops.log_pass(run_id, 'Not Required')
                exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                truncate scpp.xfm_pto_intervals
            """
            pg_cur.execute(sql)
            sql = """
                insert into scpp.xfm_pto_intervals
                with open_month as (
                  select *
                  from scpp.months
                  where open_closed = 'open')
                select f.employee_number,
                  (select year_month from open_month), f.most_recent_anniv, f.hire_date,
                  case
                    when hire_date = most_recent_anniv then hire_date
                  else
                    (select first_of_month
                      from scpp.months
                      WHERE seq = (
                        select seq - 12
                        from scpp.months
                        where f.most_recent_anniv between first_of_month and last_of_month))
                  end as pto_period_from,
                  case
                    when hire_date = most_recent_anniv then hire_date
                  else
                    (select last_of_month
                      from scpp.months
                      WHERE seq = (
                        select seq -1
                        from scpp.months
                        where f.most_recent_anniv between first_of_month and last_of_month))
                  END as pto_period_thru
                from (
                  select a.employee_number, x.most_recent_anniv, c.hire_date
                  from scpp.sales_consultant_configuration a
                  inner join scpp.sales_consultants c on a.employee_number = c.employee_number
                    and c.term_date > (select last_of_month from open_month)
                  left join scpp.months d on 1 = 1
                    and d.year_month = (select year_month from open_month)
                  left join scpp.months e on c.hire_Date between e.first_of_month and e.last_of_month
                  left join  lateral (
                    select max(thedate) as most_recent_anniv
                    from dds.day
                    where monthofyear = extract(month from c.hire_Date)
                      and dayofmonth = extract(day from c.hire_date)
                      and thedate <= (
                        select first_of_month
                        from scpp.months
                        where year_month = (
                            select year_month
                            from open_month))) x on 1 =1
                  where a.year_month = (select year_month from open_month)) f
                where not exists (
                  select 1
                  from scpp.pto_intervals
                  where employee_number = f.employee_number
                    and year_month = (select year_month from open_month))
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.xfm_pto_intervals w
                set pto_rate = 0
                where hire_date = most_recent_anniv
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.xfm_pto_intervals w
                set pto_rate = coalesce(x.pto_rate, 0)
                from (
                    select employee_number, year_month, total_gross,
                      -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
                      round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate
                    from  (
                      select a.employee_number, a.year_month, sum(b.total_gross_pay) as total_gross,
                        count(*),
                        case
                          when count(*) >= 24 then 24
                          else count(*)
                        end as adj_count
                      from scpp.xfm_pto_intervals a
                      left join dds.ext_pyhshdta b on a.employee_number = b.employee_
                        and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date
                          between a.pto_period_from and a.pto_period_thru
                      where a.most_recent_anniv <> a.hire_date
                      group by a.employee_number, a.year_month) e) x
                where w.employee_number = x.employee_number
                  and w.year_month = x.year_month
            """
            pg_cur.execute(sql)
            # see Intranet SQL Scripts\sales_cons_pay_plan\sql\postgres\roll_back_to prev_month_fix_july_clusterfux.sql
            # to rationalize this kluge
            if datetime.date.today() < datetime.date(2017, 2, 28):
                sql = """
                    update scpp.xfm_pto_intervals w
                    set pto_rate = coalesce(x.pto_rate, 0)
                    from (
                        select employee_number,
                          (select year_month from scpp.months where open_closed = 'open') as year_month,
                          -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
                          round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate
                        from (
                          select employee_ as employee_number, sum(total_gross_pay) as total_gross,
                            count(*),
                            case
                              when count(*) >= 24 then 24
                              else count(*)
                            end as adj_count
                          from (
                            select
                              case
                                when b.employee_ = '264050' then '164050'
                                when b.employee_ ='2132420' then '1132420'
                                else b.employee_
                              end as employee_,
                              b.total_gross_pay
                            from dds.ext_pyhshdta b
                            where (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date between '03/01/2015' and '02/29/2016'
                              and b.employee_ in ('164050','264050','1132420','2132420')) c
                          group by employee_) e) x
                    where w.employee_number = x.employee_number
                      and w.year_month = x.year_month
                """
                pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
