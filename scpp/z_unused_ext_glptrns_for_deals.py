# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_glptrns_for_deals'
run = ops.log_start(task)
pg_con = None
db2_con = None
file_name = 'files/ext_glptrns_for_deals.csv'

if ops.dependency_check(task) != 0:
    ops.log_dependency_failure(run)
    print 'Failed dependency check'
    exit()
try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  gtco#, gttrn#, gtseq#, gtdtyp, gttype, gtpost, gtrsts, gtadjust, gtpsel,
                  gtjrnl, gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl#), trim(gtdoc#),
                  trim(gtrdoc#), gtrdtyp, trim(gtodoc#), trim(gtref#), trim(gtvnd#),
                  trim(gtdesc), gttamt, gtcost, gtctlo,gtoco#, rrn(a)
                FROM  rydedata.glptrns a
                inner join rydedata.glpmast b on a.gtacct = b.account_number
                where a.gtdate between '01/01/2015' and current_date
                    and b.year = 2016
                    and b.account_type = '4'
                    and b.department in ('NC','UC')
                    and b.account_desc not like '%WH%'
                    and b.account_sub_type in ('A','B')
                    -- and a.gtpost <> 'V'
                    -- and a.gttamt < 0
                    and a.gtjrnl in ('VSN','VSU')
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate scpp.ext_glptrns_for_deals")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy scpp.ext_glptrns_for_deals from stdin with csv
                    encoding 'latin-1 '""", io)
    ops.log_pass(run)
except Exception, error:
    ops.log_error(str(run), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
