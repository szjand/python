# encoding=utf-8
import ops
import string
import db_cnx

# TODO what if an ops function fails
# added update to s_c_data :: guarantee_individual
task = 'update_sales_consultant_data'
pg_con = None
run_id = None

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                update scpp.sales_consultant_data a
                set pto_rate = b.pto_rate
                from (
                  select x.employee_number, x.year_month, x.pto_rate
                  from scpp.xfm_pto_intervals x
                  inner join scpp.months y on x.year_month = y.year_month
                    and y.open_closed = 'open') b
                where a.year_month = b.year_month
                  and a.employee_number = b.employee_number
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.sales_consultant_data a
                set pto_hours = b.pto_hours,
                    pto_pay = b.pto_hours * pto_rate
                from (
                  select x.employee_number, x.year_month, sum(x.pto_hours) as pto_hours
                  from scpp.pto_hours x
                  inner join scpp.months y on x.year_month = y.year_month
                    and y.open_closed = 'open'
                  group by x.employee_number, x.year_month) b
                where a.year_month = b.year_month
                  and a.employee_number = b.employee_number
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.sales_consultant_data d
                set email_score = e.email_score,
                    email_qualified = e.email_qualified
                from (
                  select a.*, b.year_month,
                    case when email_score >= c.metric_value then true else false end as email_qualified
                  from (
                    select employee_number,
                      round(100.0 * sum(case when has_valid_email then 1 else 0 end)/count(*), 2) as email_score
                    from scpp.ext_email_capture
                    where the_date between current_date - 90 and current_date
                    group by employee_number) a
                  inner join scpp.months b on 1 = 1
                    and b.open_closed = 'open'
                  inner join scpp.metrics c on b.year_month = c.year_month
                    and c.metric = 'email capture') e
                where d.employee_number = e.employee_number
                  and d.year_month = e.year_month
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.sales_consultant_data
                set metrics_qualified = csi_qualified and email_qualified and logged_qualified and auto_alert_qualified
                where year_month = (
                  select year_month
                  from scpp.months
                  where open_closed = 'open')
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.sales_consultant_data w
                set guarantee = x.guarantee
                from (
                  select a.employee_number, a.year_month,
                    case
                      when d.year_month is null then
                        case
                          when a.full_months_employment < 3 then c.guarantee_new_hire
                          when a.metrics_qualified = true then c.guarantee_with_qual
                          else c.guarantee
                        end
                      else d.guarantee
                    end as guarantee
                  from scpp.sales_consultant_data a
                  inner join scpp.months aa on a.year_month = aa.year_month
                    and aa.open_closed = 'open'
                  inner join scpp.pay_plans b on a.year_month = b.year_month
                    and a.pay_plan_name = b.pay_plan_name
                  inner join scpp.guarantee_matrix c on b.pay_plan_name = c.pay_plan_name
                    and b.year_month = c.year_month
                  left join scpp.guarantee_individual d on a.year_month = d.year_month
                    and a.employee_number = d.employee_number) x
                where w.year_month = x.year_month
                  and w.employee_number = x.employee_number
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.sales_consultant_data w
                set unit_count_sys = coalesce(x.the_count, 0),
                    unit_count = coalesce(x.the_count, 0) + w.unit_count_ovr
                from (
                  select a.year_month, a.employee_number, sum(a.unit_count) as the_count
                  from scpp.deals a
                  inner join scpp.months b on a.year_month = b.year_month
                    and b.open_closed = 'open'
                  group by a.year_month, a.employee_number) x
                where w.year_month = x.year_month
                  and w.employee_number = x.employee_number
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.sales_consultant_data w
                set per_unit =
                    case
                      when metrics_qualified then (
                        select tier_2
                        from scpp.per_unit_matrix a
                        where year_month = w.year_month
                          -- sometimes, early in the month, the net unit count will be less than 0
                          AND units @> floor(coalesce(abs(w.unit_count),0))::integer
                          and pay_plan_name = w.pay_plan_name)
                      else (
                        select tier_1
                        from scpp.per_unit_matrix a
                        where year_month = w.year_month
                          and units @> floor(coalesce(abs(w.unit_count),0))::integer
                          and pay_plan_name = w.pay_plan_name)
                      end
                where year_month = (
                  select year_month
                  from scpp.months
                  where open_closed = 'open')
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.sales_consultant_data
                set unit_count_x_per_unit = unit_count * per_unit
                where year_month = (
                  select year_month
                  from scpp.months
                  where open_closed = 'open')
            """
            pg_cur.execute(sql)
            sql = """
                update scpp.sales_consultant_data a
                set total_pay = b.total_pay
                from (select * from scpp.get_total_pay()) b
                where a.employee_number = b.employee_number
                  and a.year_month = b.year_month
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
finally:
    if pg_con:
        pg_con.close()
