# encoding=utf-8
import csv
import psycopg2
try:
    with psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'") as pgCon_1:
        pgCursor_1 = pgCon_1.cursor()
    with psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'") as pgCon_2:
        pgCursor_2 = pgCon_2.cursor()
    file_name = 'files/data.csv'
    sql = """
        select *
        from test.ext_glpmast_0323
    """
    pgCursor_1.execute(sql)
    with open(file_name, 'wb') as f:
        csv.writer(f).writerows(pgCursor_1)
    sql = """
        CREATE TABLE dds.ext_glpmast_test
        (
          company_number citext,
          fiscal_annual citext,
          year integer,
          account_number citext,
          account_type citext,
          account_desc citext,
          account_sub_type citext,
          account_ctl_type citext,
          department citext,
          typical_balance citext,
          debit_offset_acct citext,
          cred_offset_acct citext,
          offset_percent numeric(3,2),
          auto_clear_amount numeric(7,2),
          write_off_account citext,
          schedule_by citext,
          reconcile_by citext,
          count_units citext,
          control_desc1 citext,
          validate_stock_ citext,
          option_3 citext,
          option_4 citext,
          option_5 citext,
          beginning_balance numeric(11,2),
          jan_balance01 numeric(11,2),
          feb_balance02 numeric(11,2),
          mar_balance03 numeric(11,2),
          apr_balance04 numeric(11,2),
          may_balance05 numeric(11,2),
          jun_balance06 numeric(11,2),
          jul_balance07 numeric(11,2),
          aug_balance08 numeric(11,2),
          sep_balance09 numeric(11,2),
          oct_balance10 numeric(11,2),
          nov_balance11 numeric(11,2),
          dec_balance12 numeric(11,2),
          adj_balance13 numeric(11,2),
          units_beg_balance integer,
          jan_units01 integer,
          feb_units02 integer,
          mar_units03 integer,
          apr_units04 integer,
          may_units05 integer,
          jun_units06 integer,
          jul_units07 integer,
          aug_units08 integer,
          sep_units09 integer,
          oct_units10 integer,
          nov_units11 integer,
          dec_units12 integer,
          adj_units13 integer,
          active citext
        )
    """
    pgCursor_2.execute(sql)
    pgCon_2.commit()
    with open(file_name, 'r') as io:
        pgCursor_2.copy_expert("""copy dds.ext_glpmast_test from stdin with csv encoding 'latin-1) '""", io)
    pgCon_2.commit()
except Exception, error:
    print str(error)
