# encoding=utf-8
import ops
import string
import db_cnx

# TODO what if an ops function fails
# 4/8, this is not needed, all s_c_data updates done in separate task
task = 'load_email_capture'
run = ops.log_start(task)
pg_con = None

if ops.dependency_check(task) != 0:
    ops.log_dependency_failure(run)
    print 'Failed dependency check'
    exit()
try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:

            ops.log_pass(run)
    print 'Passssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
