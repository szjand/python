import db_cnx
import requests
from datetime import date
from requests.auth import HTTPBasicAuth
"""
5/27/20
    using KL7CJLSB6KB961734 a 2020 trax, attempting to get historical schema versions that should include sup_inc
"""
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        sql = "select 'KL7CJLSB6KB961734'"
        pg_cur.execute(sql)
        the_date = date.today()
        for row in pg_cur.fetchall():
            schema = '1.3'
            vin = row[0]
            zip = '58201'
            user = '306422'
            password = 'aa118f7cce54469d'
            url = 'https://incentives.chromedata.com/IncentivesWebService/newincentives/' + schema + '/' + zip + '/' + vin + '/incentives'
            data = requests.get(url, auth=HTTPBasicAuth(user, password))
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into chr.incentive_schema_versions_by_vin(the_date,schema_version,vin,response)
                    values ('{0}','{1}','{2}','{3}');
                """.format(the_date, schema, vin, resp)
                chr_cur.execute(sql)

