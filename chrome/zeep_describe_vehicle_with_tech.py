# encoding=utf-8
from datetime import datetime
import zeep
from zeep import Client
from zeep import helpers
import json
import db_cnx

with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        sql = "select '1FTPX28L8WKA85591'"
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            the_vin = row[0]
            the_date = datetime.today().strftime('%m/%d/%Y')
            # json_directory = '/mnt/hgfs/E/python_projects/chrome/json_zeep_build_data/'
            authentication = {"number": '265491',
                              "secret": '24a63940ed48890b',
                              "country": 'US',
                              "language": 'en'}
            client = Client("http://services.chromedata.com/Description/7b?wsdl")
            response = client.service.describeVehicle(accountInfo=authentication, vin=the_vin,
                                                      switch=['ShowExtendedTechnicalSpecifications',
                                                              'ShowAvailableEquipment'])
            my_dict = zeep.helpers.serialize_object(response, dict)
            the_response = json.dumps(my_dict, default=str)
            the_response = the_response.replace("'", "''")

            sql = """
                insert into chr2.describe_vehicle_by_vin_with_tech(vin,response)
                values ('{0}', '{1}');
            """.format(the_vin, the_response, the_date)
            pg_cur.execute(sql)

            sql = """
                update chr2.describe_vehicle_by_vin_with_tech y
                set style_count = x.style_count
                from (
                    select vin, jsonb_array_length(response->'style') as style_count
                    from chr2.describe_vehicle_by_vin_with_tech
                    where vin = '{0}') x
                where x.vin = y.vin;  
            """.format(the_vin)
            pg_cur.execute(sql)
