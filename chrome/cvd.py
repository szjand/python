import db_cnx
import requests
from datetime import datetime
"""

"""
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        # # single vin
        # sql = """ select '3C63R2HL7MG540368' as vin """

        # nc.stg_availability  ***********************************************
        sql = """
            select vin
            from nc.stg_availability a
            where not exists (
                select 1
                from cvd.nc_vin_descriptions
                where vin = a.vin);
        """

        # # current inventory from inpmast
        # sql = """
        #     select distinct inpmast_vin
        #     from arkona.ext_inpmast a
        #     where status = 'I'
        #       and type_n_u = 'U'
        #       and not exists (
        #         select 1
        #         from cvd.vin_descriptions
        #         where vin = a.inpmast_vin)
        #       and not exists (
        #         select 1
        #         from cvd.bad_vins
        #         where vin = a.inpmast_vin)
        #  """

        # #  month end vins from cu.used_3
        # sql = """
        #     select distinct a.vin --2303
        #     from cu.used_3  a
        #     where not exists (
        #       select 1
        #       from cvd.vin_descriptions
        #       where vin = a.vin)
        #     and not exists (
        #       select 1
        #       from cvd.bad_vins
        #       where vin = a.vin)
        #  """

        # # random vins
        # sql = """
        #     select distinct vin
        #     from cu.used_3 a
        #     where not exists (
        #       select 1
        #       from cvd.vin_descriptions
        #       where vin = a.vin)
        #     And not exists (
        #      select 1
        #      from cvd.bad_vins
        #      where vin = a.vin)
        #  """

        # # ford and toyota catch up
        # sql = """
        #     select distinct a.vin
        #     from cu.used_3 a
        #     join ads.ext_vehicle_inventory_items b on a.stock_number = b.stocknumber
        #     join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
        #       and c.make in ('ford','toyota')
        #     where not exists (
        #       select 1
        #       from cu.tmp_vehicles
        #       where vin = a.vin)
        #     and not exists (
        #       select 1
        #       from chr.cvd
        #       where vin = a.vin)
        # """
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin = row[0]
            the_date = datetime.today().strftime('%m/%d/%Y')
            print(vin)
            url = 'https://beta.rydellvision.com:8888/cvd/vin-descriptions/' + vin
            data = requests.get(url)
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into cvd.vin_descriptions(vin,response,the_date)
                    values ('{0}', '{1}', '{2}');
                """.format(vin, resp, the_date)
                chr_cur.execute(sql)

                sql = """
                    update cvd.vin_descriptions a
                    set source = b.source,
                        style_count = b.style_count
                    from (   
                        select x.vin, x.response->'result'->>'source' as source,
                            jsonb_array_length(x.response->'result'->'vehicles') as style_count
                        from cvd.vin_descriptions x
                        where vin = '{0}') b
                    where a.vin = b.vin;
                """.format(vin)
                chr_cur.execute(sql)
                pg_con.commit()
