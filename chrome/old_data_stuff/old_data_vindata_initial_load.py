import zipfile
import psycopg2
# initially loaded tables with download 10/20/15
# delete working files
# include exception handling

# try:
# initial load
zfile = zipfile.ZipFile('files\zip_downloads\VINDATA.zip')
zfile.extractall('files\working_files')

pg_con = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
pg_cur_1 = pg_con.cursor()
pg_cur_2 = pg_con.cursor()
pg_cur_1.execute("""
  select file_name, table_name
  from chr.maint_vindata
  order by load_sequence;
""")
schema = 'chr'
for x in pg_cur_1:
    print x[0]
    print x[1]
    file_name = 'files\working_files\\' + x[0]
    table_name = x[1]
    io = open(file_name, 'r')
    # will/can not generate null values for empty strings, would have to preprocess the file or update the table
    pg_cur_2.copy_expert("""copy """ + schema + """.""" + table_name + """ from stdin with csv header quote '~' """, io)
    pg_con.commit()
    io.close()
# # except:
# #     print('ruh roh')
pg_cur_1.close()
pg_cur_2.close()
pg_con.close()


