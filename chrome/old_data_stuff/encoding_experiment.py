
import psycopg2

pgCon = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
pgCursor = pgCon.cursor()
file_name = 'files/working_files/CategoryHeaders.txt'
pgCursor.execute('truncate chr.tmp_style_cats, chr.tmp_categories, chr.tmp_category_headers')
pgCon.commit()
io = open(file_name, 'r')
# pgCursor.copy_expert("""copy chr.tmp_category_headers from stdin with csv header quote '~'
#              encoding 'windows-1251' """, io)
#latin-1 doesn't bomb, and the data looks correct, eg, category_header_id 24, Decor with an accented e
pgCursor.copy_expert("""copy chr.tmp_category_headers from stdin with csv header quote '~'
             encoding 'latin-1' """, io)
pgCon.commit()
io.close()
pgCursor.execute("select category_header_id::text, category_header, category_header_sequence::Text from chr.tmp_category_headers")
for x in pgCursor:
    print x[0] + ' -- ' + x[1] + ' -- ' + x[2]
pgCursor.close()
pgCon.close()
