﻿select *
-- delete
from chr.chrome_log
where the_date = current_date 
order by load_start_ts desc

select *
from chr.version

delete from chr.chrome_log where log_id <> 'f8312bc1-2e41-42ba-84e0-9c66d7015647'

select *
from chr.version_vindata


delete from chr.chrome_log where log_id = '3adf9620-acca-4df7-9ba3-ef761915ec83'


select tmp_table_name
from chr.maint_nvd
where table_name <> 'version'
order by truncate_sequence

truncate chr.tmp_styles cascade

select * from chr.tmp_styles limit 10

select count(*) from chr.tmp_styles

select * 
from pg_locks a
inner join pg_class b on a.relation = b.oid and b.relkind = 'r'
where b.relname like '%styles'

select * from pg_stat_activity

SELECT relation::regclass, * FROM pg_locks WHERE NOT GRANTED;


select *
from chr.styles
where model_year = 2015
limit 100

delete
-- 

explain analyze
--select *
delete
from chr.styles
where model_year in (
select distinct model_year from chr.tmp_styles)


select distinct model_year
from chr.tmp_styles


