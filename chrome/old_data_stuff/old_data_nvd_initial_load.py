# coding=utf-8
import zipfile
import psycopg2
import glob
import os
# initially loaded tables with download 10/20/15
# process 1997 separately
# delete working files
# TODO include exception handling
pg_con = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
pg_cur_1 = pg_con.cursor()
pg_cur_2 = pg_con.cursor()
try:
    # initial load
    # loop thru the files in files\zip_downloads
    for filename in glob.iglob('files\zip_downloads\*.zip'):
        # files = glob.glob('files\working_files\*.txt')
        # for f in files:
        #     os.remove(f)
        for f in glob.glob('files\working_files\*.txt'):
            os.remove(f)
        # files = glob.glob('files\zip_downloads\*.zip')
        # for f in files:
        #     os.remove(f)
        print filename
        zipfile.ZipFile(filename).extractall('files\working_files')

        pg_cur_1.execute("""
          select file_name, table_name
          from chr.maint_nvd
          where table_name not in ('jpgs','version')
          order by load_sequence;
        """)
        schema = 'chr'
        for x in pg_cur_1:
            print x[0]
            print x[1]
            file_name = 'files\working_files\\' + x[0]
            table_name = x[1]
            io = open(file_name, 'r')
            # will/can not generate null values for empty strings, would have to preprocess the file or update the table
            # if table_name == 'category_headers' or table_name == 'tech_titles':
            # each new run seemed to expose yet another file with goofy data
            pg_cur_2.copy_expert("""copy """ + schema + """.""" + table_name + """ from stdin with csv header quote '~'
                         encoding 'latin-1' """, io)
            # else:
            #     pg_cur_2.copy_expert("""copy """ + schema + """.""" + table_name +
            #                          """ from stdin with csv header quote '~' """, io)
            pg_con.commit()
            io.close()
except:
    raise
pg_cur_1.close()
pg_cur_2.close()
pg_con.close()
