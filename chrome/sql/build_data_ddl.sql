﻿drop table if exists chr.build_data_describe_vehicle cascade;
create table chr.build_data_describe_vehicle (
	vin citext not null,
	response jsonb,
	style_count integer,
	source citext,
	primary key(vin));