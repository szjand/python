﻿select i->>'programNumber' as program_number, i->>'programDescription' as program_description,
select i->>'programNumber' as program_number, i->>'programDescription' as program_description,
  i->'market'->>'description' as "Dealer/Retail", i->'category'->>'description' as category,
  i->'programValues'->'valueVariationList'->0->'programValueList'->0 as program_value,
  i->'programRuleList'->0->>'type' as rule_type, i->'programRuleList'->0->>'description' as rule_description,
  i->'groupAffiliation'->>'description' as group_affiliation,
  i->'previousOwnership'->>'description' as prev_ownership,
  i->'vehicleStatusList',
  i->'signatureID' as sig_id, i->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
where vin = '3GNAXUEV5LS665508'
order by i->>'programNumber'


-- no group affil and no prev ownership
select i->>'programNumber' as program_number, i->>'programDescription' as program_description,
  i->'market'->>'description' as "Dealer/Retail", i->'category'->>'description' as category,
  i->'programValues'->'valueVariationList'->0->'programValueList'->0 as program_value,
  i->'programRuleList'->0->>'type' as rule_type, i->'programRuleList'->0->>'description' as rule_description,
  i->'groupAffiliation'->>'description' as group_affiliation,
  i->'previousOwnership'->>'description' as prev_ownership,
  i->'vehicleStatusList',
  i->'signatureID', i->>'signatureHistoryID'  
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
where vin = '3GNAXUEV5LS665508'
  and i->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and i->'previousOwnership'->>'description' = 'No Previous Ownership Requirement'

select s
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
join jsonb_array_elements(a.response->'stackabilityList') as s(stackables) on true
  and s->>'signatureID' = '15273'
  and s->>'relationType' = 'S'
where vin = '3GNAXUEV5LS665508'
  and i->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and i->'previousOwnership'->>'description' = 'No Previous Ownership Requirement' 


select s
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
join jsonb_array_elements(a.response->'stackabilityList') as s(stackables) on true
  and s->>'signatureID' = '15277'
--   and s->>'relationType' = 'S'
  and s->>'signatureHistoryID' = '55902'
where vin = '3GNAXUEV5LS665508'
  and i->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and i->'previousOwnership'->>'description' = 'No Previous Ownership Requirement'   

STACKING INCENTIVES p14
-- step 1 vehicle & zip (all table data is based on 58201)
select b
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
where vin = '3GNAXUEV5LS665508'

-- step 2 delivery type - not relevant
select b.*
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
where vin = '3GNAXUEV5LS665508'

-- step 3 wtf, same as step 2

-- step 4 group affiliation, only incentives with no group afiliation
-- Incentives that apply to all customers will have no group affiliation and no previous ownership requirements.
select b.*
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  and b->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and b->'previousOwnership'->>'description' = 'No Previous Ownership Requirement'  
where vin = '3GNAXUEV5LS665508'
  
-- step 5 vehicle status
--  not sure what this looks like if there are any vin specific incentives
-- the data in chr.incentives_by_style_id is all style_ids currently in GM inventory (88 rows)
-- in categories only 2 values for vehicleStatus: New & New - VIN Specific
select style_id
from chr.incentives_by_style_id a
-- the only vehicle status value in these is New
select distinct c
from chr.incentives_by_style_id a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
join jsonb_array_elements(b.incentives->'vehicleStatusList') as c(status) on true



-- all the programs in chr.incentives_by_style
select i->>'programNumber' as program_number, v->>'division', v->>'model', v->'styleIDs'->>0 as style_id, 
  i->'institutionList' as institutions, i->'market'->>'description' as market,
  i->'category'->>'group' as category_group, i->'category'->>'description' as category_desc, i->>'expiryDate'
from chr.incentives_by_style_id a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
join jsonb_array_elements(a.response->'vehicles') as v(vehicles) on true
where (i->>'expiryDate')::date > current_date
--   and v->'styleIDs'->>0 = '406427'
order by i->>'programNumber', v->>'division', v->>'model', v->'styleIDs'->>0



select count(*) from chr.incentives_by_style_id;-- 90
-- is all current inventory in chr.incentives_by_style
select distinct c.chrome_style_id
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
join nc.vehicle_configurations c on b.configuration_id = c.configuration_id
left join chr.incentives_by_style_id d on c.chrome_style_id = d.style_id
where a.thru_date > current_date
  and stock_number not like 'H%'
  and d.style_id is null


select b.chrome_style_id
from nc.vehicles a
join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
where a.vin = '3GNAXUEV5LS665508'

-- ok, there are 2 incentives without a program number, they are cat grp/desc: Lease/Residual * Lease/Lease Rate
-- described as "GM Consumer Non-Supported Lease Residual Values" & "Non-Supported Lease rates"
-- and are both GM Financial
select i->>'programNumber' as program_number,
  i->'signatureID' as sig_id, i->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_style_id a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
where style_id = '406427'
order by i->>'programNumber'

ok, lets go the path of for this vin 3GNAXUEV5LS665508, cash only (dealer or retail) incentives,
that apply to everyone

-- 37 total incentives
-- 11 apply to everyone (ownership/group affiliation)
-- 2 cash or dealer cash one of which is 20-40X-006 which is not in GM
select b->>'programNumber' as program_number, b.*
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  and b->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and b->'previousOwnership'->>'description' = 'No Previous Ownership Requirement'  
  and ((b->'category'->>'group' = 'Cash') or (b->'category'->>'group' = 'Dealer Cash'))
where vin = '3GNAXUEV5LS665508'


drop table if exists incentives;
create temp table incentives as
select coalesce(b->>'programNumber', 'none') as program_number, b->>'signatureID' as sig_id, b->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
where vin = '3GNAXUEV5LS665508';

drop table if exists stackables;
create temp table stackables as
select c->>'signatureID' as sig_id, c->>'signatureHistoryID' as sig_his_id, c->>'relationType' as stackable
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'stackabilityList') as c(stackable) on true
where vin = '3GNAXUEV5LS665508';



-- checking against global connect
-- missing 20-36CC, 20-40GP(non stackable)
-- extra: 20-06, 20-06QRD-004, 20-06Z-004, 20-09-004, 20-10A-003, 20-17-004, 20-40X-006
select * -- a.*, c.*
from incentives a
join stackables b on a.sig_id = b.sig_id
  and stackable = 'S'
join incentives c on b.sig_his_id = c.sig_his_id  
where a.program_number = '20-40ACA-007'
order by c.program_number

-- checking backward on 20-17-004 (sig for 20-17 sig_his for 20-40ACA), shows as stackable
select * from stackables where sig_id = '15349' and sig_his_id = '55902'

-- reverse it, make the primary join on sig_his_id
-- this generates identical response
select * -- a.*, c.*
from incentives a
join stackables b on a.sig_his_id = b.sig_his_id
  and stackable = 'S'
join incentives c on b.sig_id = c.sig_id  
where a.program_number = '20-40ACA-007'
order by c.program_number

-- match the vehicles list in rebate admin
-- perfect, exept rebate admin does not include the 2021 model(s)
select distinct coalesce(b->>'programNumber', 'none') as program_number,
  c->>'year' as model_year, c->>'division' as make, c->>'model' as model
from chr.incentives_by_style_id a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
join jsonb_array_elements(a.response->'vehicles') as c(vehicles) on true
where coalesce(b->>'programNumber', 'none') = '20-40ACA-007'
order by coalesce(b->>'programNumber', 'none'), c->>'year', c->>'division', c->>'model'




-- all institutions in chr.incentives_by_style (all gm inventory)
-- 1: no institutionList contains more than 1 element
select i->'institutionList'->0 as institutions, i->'institutionList'->1 as institutions
from chr.incentives_by_style_id a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
where i->'institutionList'->1 Is not null

-- 2: all the institutions
select distinct i->'institutionList'->0->>'description' as institutions
from chr.incentives_by_style_id a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true

-- -- this verifies my assumption about extracting number of elements from an array using deliveryTypeList
-- select i->>'programNumber' as program_number, v->>'division', v->>'model', v->'styleIDs'->>0 as style_id, 
--   i->'deliveryTypeList'->0 as del_type, i->'deliveryTypeList'->1 as del_type
-- from chr.incentives_by_style_id a
-- join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
-- join jsonb_array_elements(a.response->'vehicles') as v(vehicles) on true


----------------------------------------------------
--< existing incentives data model
----------------------------------------------------
select *
from gmgl.incentive_programs
where program_number = '20-03'
  and revision_number = '1'

select *
from gmgl.major_incentives
where program_key = 2014

select *
from gmgl.stackable_incentives
where program_key = 2014
  and lookup_date = current_date

-- these are all the current acknowledged major incentives
select distinct a.program_number
from gmgl.incentive_programs a
join gmgl.major_incentives b on a.program_key = b.program_key
  and b.lookup_date = current_date
where a.acknowledged
  and a.program_end_date > current_date

-- this should be all the stackables for 20-40ACA
-- more (2) than on an individual vin page
-- add the vin and matches gm page
select distinct a.program_number, d.program_number
from gmgl.incentive_programs a
join gmgl.major_incentives b on a.program_key = b.program_key
  and b.lookup_date = current_date
  and b.vin = '3GNAXUEV5LS665508'
join gmgl.stackable_incentives c on b.major_incentive_key = c.major_incentive_key  
join gmgl.incentive_programs d on c.program_key = d.program_key
where a.acknowledged
  and a.program_end_date > current_date
  and a.program_number = '20-40ACA'

----------------------------------------------------
--< existing incentives data model
----------------------------------------------------
  
-- need to do a cadillac with stackables only (or maybe it is 0 value majors)
select b.chrome_style_id
from nc.vehicles a
join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
where vin = '1GYKPDRS7LZ204188'

-- vin has 2 incentives not in style, style has 1 not in vin
select *
from chr.incentives_by_vin
where vin = '1GYKPDRS7LZ204188'

select *
from chr.incentives_by_style_id
where style_id = '405321'

drop table if exists incentives;
create temp table incentives as
select coalesce(b->>'programNumber', 'none') as program_number, b->>'signatureID' as sig_id, b->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
where vin = '1GYKPDRS7LZ204188';

drop table if exists stackables;
create temp table stackables as
select c->>'signatureID' as sig_id, c->>'signatureHistoryID' as sig_his_id, c->>'relationType' as stackable
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'stackabilityList') as c(stackable) on true
where vin = '1GYKPDRS7LZ204188';




-- lists no majors, 20-40XA not in GM
select b->>'programNumber' as program_number, b.*
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  and b->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and b->'previousOwnership'->>'description' = 'No Previous Ownership Requirement'  
  and ((b->'category'->>'group' = 'Cash') or (b->'category'->>'group' = 'Dealer Cash'))
where vin = '1GYKPDRS7LZ204188'
order by b->>'programNumber'


select * -- a.*, c.*
from incentives a
join stackables b on a.sig_id = b.sig_id
  and stackable = 'S'
join incentives c on b.sig_his_id = c.sig_his_id  
where a.program_number = '20-40AL-003'
order by c.program_number


select b->>'programNumber' as program_number, a.response->'vehicles'->0->>'styleIDs'
from chr.incentives_by_style_id a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  and b->>'programNumber' = '20-40ACC-003'

on the global connect page for the vin, 20-40ACC-003 is the first major listed
and on the rebate admin page it is included
BUT 
in the chrome incentives_by_style_id, the styleid for 1GYKPDRS7LZ204188, 405321 is not included

even so, there are no majors with a cash value, therefore no "trigger" incentive


-- encore gx


-- encore gx
select b.chrome_style_id
from nc.vehicles a
join nc.vehicle_configurations b on a.configuration_id = b.configuration_id
where vin = 'KL4MMCSLXLB116386'

select *
from chr.incentives_by_vin
where vin = 'KL4MMCSLXLB116386'

select *
from chr.incentives_by_style_id
where style_id = '409129'


select distinct i->'institutionList'->0->>'description' as institutions
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
where vin = 'KL4MMCSLXLB116386'

-- 2 fewer incentives in style vs vin
-- any hint of us bank? (possibly in one of the incentives without a program number)
-- nope
select *
from (
  select b->>'programNumber' as program_number, b->>'incentiveID' as incentive_id,  b->'institutionList'->0->>'description' as institutions
  from chr.incentives_by_vin a
  join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  where vin = 'KL4MMCSLXLB116386') a
left join (
  select b->>'programNumber' as program_number, b->>'incentiveID' as incentive_id,  b->'institutionList'->0->>'description' as institutions
  from chr.incentives_by_style_id a
  join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  where style_id = '409129') b on a.incentive_id = b.incentive_id
  order by a.program_number

-- incentives available to all
-- shows 3
-- 20-40ACA-007 : major
-- 20-35BA-006 : stackable under 20-40GFK
-- 20-40X-006 : nowhere on global incentive lookup page
select b->>'programNumber' as program_number, b.*
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  and b->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and b->'previousOwnership'->>'description' = 'No Previous Ownership Requirement'  
  and ((b->'category'->>'group' = 'Cash') or (b->'category'->>'group' = 'Dealer Cash'))
where vin = 'KL4MMCSLXLB116386'
order by b->>'programNumber'


-- stackables

drop table if exists incentives;
create temp table incentives as
select coalesce(b->>'programNumber', 'none') as program_number, b->>'signatureID' as sig_id, b->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
where vin = 'KL4MMCSLXLB116386';

drop table if exists stackables;
create temp table stackables as
select c->>'signatureID' as sig_id, c->>'signatureHistoryID' as sig_his_id, c->>'relationType' as stackable
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'stackabilityList') as c(stackable) on true
where vin = 'KL4MMCSLXLB116386';

select * -- a.*, c.*
from incentives a
join stackables b on a.sig_id = b.sig_id
  and stackable = 'S'
join incentives c on b.sig_his_id = c.sig_his_id  
where a.program_number = '20-40ACA-007'
order by c.program_number

in global not in chrome: 20-36CC, 20-40CAI
20-03-001
20-04-002
20-05-002
20-05A-003
20-05B-001
20-05C-001
20-05D-002
20-06-005 - not in glopbal
20-06EX-004
20-06QRD-004 - not in glopbal
20-06Z-004 - not in glopbal
20-07-001
20-09-004 - not in glopbal
20-10A-003 - not in glopbal
20-15-003
20-16-003
20-17-004
20-18-001
20-36CA-002
20-36CB-002
20-40CAG-000
20-40CI-001
20-40CT-002
20-40X-006 - not in glopbal 
20-40XH-000


-- source attribute in incentive object
-- all OEM
select distinct b->>'source'
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true