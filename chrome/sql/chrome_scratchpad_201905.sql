﻿

select a.style_count, b.*, c.peg, (select min(ground_date) from nc.vehicle_acquisitions where vin = b.vin)
from chr.describe_vehicle a
join nc.vehicles b on a.vin = b.vin
left join gmgl.vehicle_orders c on b.vin = c.vin
where a.style_count > 1

select * from chr.describe_vehicle where vin = '3GCUKREC5JG473182'

select * from chr.describe_vehicle where vin = '1GCGTDEN5K1281243'''

it appears that vehicles which had multiple styles at the time of being run, no longer have multiple styles
even as recent as vins run this past week

?!?!?!?

-- not sure yet what the PK should be, will depend on how this integrates with the rest of the model
-- the whole issue is, do i need the division_id ?
-- leaning away from  using the "id" values
-- but when i get to models, the issue is for 2018, 2019 & 2020, sierra & silverado 3500HD each have 2 model ids
-- because they each are a model for 2 subdivisions: pickups & chassis-cabs
-- but if i leave division_id out, and alfa romeo changes it's name to alfa-romeo
-- fuck it
-- for these base tables take what is offerred, if i need to restructure later i can do it off this foundation


----------------------------------------------------------------------------------
--< model_years
----------------------------------------------------------------------------------
drop table if exists chr.get_model_years;
create table if not exists chr.get_model_years (
  model_years jsonb);  
comment on table chr.get_model_years is 'repository for raw data retreived from call to
  https://beta.rydellvision.com:8888/chrome/model-years';
  
create table chr.tmp_model_years (
  model_year integer primary key);
comment on table chr.tmp_model_years is 'repository for extracted json from chr.get_model_years';

drop table if exists chr.model_years cascade; 
create table chr.model_years (
  model_year integer not null primary key);
comment on table chr.model_years is 'valid chrome model_years';
  
create or replace function chr.xfm_model_years()
returns void as
$BODY$

  truncate chr.tmp_model_years;
  insert into chr.tmp_model_years(model_year)  
  select jsonb_array_elements_text(model_years)::integer
  from chr.get_model_years;

  insert into chr.model_years(model_year) 
  select model_year
  from chr.tmp_model_years
  on conflict (model_year)
  do nothing;
  
$BODY$
language sql;

select * from chr.model_years;
----------------------------------------------------------------------------------
--/> model_years
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
--< divisions (makes)
----------------------------------------------------------------------------------
drop table if exists chr.get_divisions cascade;
create table if not exists chr.get_divisions (
  model_year integer not null primary key,
  divisions jsonb);  
comment on table chr.get_divisions is 'repository for raw data retreived from call to
  https://beta.rydellvision.com:8888/chrome/divisions/ + str(model_year)';
  
drop table if exists chr.tmp_divisions;
create table if not exists chr.tmp_divisions (
  model_year integer not null references chr.model_years(model_year),
  division_id integer not null,
  division citext not null,
  primary key (model_year,division_id)) ;
comment on table chr.tmp_divisions is 'repository for extracted json from chr.get_divisions';  

drop table if exists chr.divisions cascade;
create table if not exists chr.divisions (
  model_year integer references chr.model_years(model_year),
  division_id integer not null,
  division citext not null,
  primary key(model_year, division_id));
create unique index on chr.divisions(model_year, division);

create or replace function chr.xfm_divisions()
returns void as
$BODY$

  truncate chr.tmp_divisions;
  insert into chr.tmp_divisions(model_year,division_id,division)  
  select a.model_year, (attributes #>> '{id}')::integer as division_id, b."$value" as division
  from chr.get_divisions a,
    jsonb_to_recordset(a.divisions) as b("$value" citext, attributes jsonb);

  insert into chr.divisions(model_year, division_id, division)  
  select model_year, division_id, division
  from chr.tmp_divisions
  on conflict (model_year, division_id)
  do nothing;
  
$BODY$
language sql;  

-- ----------------------------------------------------------------------------------
-- --/> divisions (makes)
-- ----------------------------------------------------------------------------------
-- 
-- ----------------------------------------------------------------------------------
-- --< subdivisions 
-- ----------------------------------------------------------------------------------
-- -- drop table if exists chr.get_subdivisions;
-- create table if not exists chr.get_subdivisions (
--   model_year integer not null primary key,
--   subdivisions jsonb);  
-- comment on table chr.get_subdivisions is 'repository for raw data retreived from call to
--   https://beta.rydellvision.com:8888/chrome/subdivisions/ + str(model_year)';
-- 
-- drop table if exists chr.tmp_subdivisions cascade;
-- create table if not exists chr.tmp_subdivisions(
--   model_year integer references chr.model_years(model_year),
--   subdivision_id integer not null,
--   subdivision citext not null,
--   primary key(model_year, subdivision_id));
-- create unique index on chr.tmp_subdivisions(model_year, subdivision);
-- 
-- drop table if exists chr.subdivisions cascade;
-- create table if not exists chr.subdivisions(
--   model_year integer references chr.model_years(model_year),
--   subdivision_id integer not null,
--   subdivision citext not null,
--   primary key(model_year, subdivision_id));
-- create unique index on chr.tmp_subdivisions(model_year, subdivision);
-- 
-- create or replace function chr.xfm_subdivisions()
-- returns void as
-- $BODY$
-- 
--   truncate chr.tmp_subdivisions;
--   insert into chr.subdivisions (model_year,subdivision_id, subdivision)
--   select model_year, (attributes #>> '{id}')::integer as subdivision_id, b."$value" as subdivision
--   from chr.get_subdivisions,
--     jsonb_to_recordset(chr.get_subdivisions.subdivisions) as b("$value" citext, attributes jsonb);
-- 
--   insert into chr.subdivisions(model_year, subdivision_id, subdivision)  
--   select model_year, subdivision_id, subdivision
--   from chr.tmp_subdivisions
--   on conflict (model_year, subdivision_id)
--   do nothing;
--   
-- $BODY$
-- language sql;  
-- 
-- 
-- ----------------------------------------------------------------------------------
-- --/> subdivisions 
-- ----------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------
-- --< divisions_subdivisions  5/27 not doing this
-- ----------------------------------------------------------------------------------
-- drop table if exists chr.divisions_subdivisions;
-- create table if not exists chr.divisions_subdivisions (
--   model_year integer references chr.model_years(model_year),
--   division_id integer not null,
--   division citext not null,
--   subdivision_id integer not null,
--   subdivision citext not null,
--   foreign key (model_year,division_id) references chr.divisions(model_year,division_id),
--   foreign key (model_year,subdivision_id) references chr.subdivisions(model_year,subdivision_id),
--   primary key (division_id,subdivision_id));
--   
-- insert into chr.divisions_subdivisions(model_year,division_id,division,subdivision_id,subdivision)
-- select a.model_year, a.division_id, a.division, b.subdivision_id, b.subdivision
-- from chr.divisions a
-- full outer join chr.subdivisions b on a.model_year = b.model_year
--   and 
--     case
--       when a.division = 'chevrolet' then b.subdivision like 'chevy%'
--       else b.subdivision like '%' || a.division || '%'
--     end;
-- ----------------------------------------------------------------------------------
-- --/> divisions_subdivisions 
-- ----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
--< models 5/27 just doing models by division as models
----------------------------------------------------------------------------------


drop table if exists chr.get_models_by_division cascade;
create table if not exists chr.get_models_by_division (
  model_year integer not null,
  division_id integer not null,
  models jsonb);  
comment on table chr.get_models_by_division is 'repository for raw data retreived from call to
  https://beta.rydellvision.com:8888/chrome/models payload: {year:, divisionId: }
  limited to model_year > 2017 division in (chevrolet,buick,gmc,cadillac,honda,nissan)';

drop table if exists chr.tmp_models cascade;
create table if not exists chr.tmp_models(
  model_id integer primary key,
  model_year integer not null,
  division_id integer not null,
  model citext not null);
create unique index on chr.tmp_models(model_year, model_id);
comment on table chr.tmp_models is 'repository for extracted json from chr.get_models_by_division';  


create table if not exists chr.models(
  model_id integer primary key,
  model_year integer not null,
  division_id integer not null,
  model citext not null,
  foreign key (model_year,division_id) references chr.divisions(model_year,division_id));
create unique index on chr.models(model_year, model_id);
create index on chr.models(model_year,division_id);    
create index on chr.models(model);  
comment on table chr.models is 'the same model in the same model_year, eg, 2018 Silverado 3500, 
  can have multiple model_id''s';


create or replace function chr.xfm_models()
returns void as
$BODY$

  truncate chr.tmp_models;
  insert into chr.tmp_models (model_id,model_year,division_id,model)
  select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
  from chr.get_models_by_division,
    jsonb_to_recordset(chr.get_models_by_division.models) as b("$value" citext, attributes jsonb);

  insert into chr.models(model_id,model_year,division_id,model)
  select model_id, model_year, division_id, model
  from chr.tmp_models
  on conflict (model_id)
  do nothing;
  
$BODY$
language sql;  

----------------------------------------------------------------------------------
--/> models
----------------------------------------------------------------------------------

 ----------------------------------------------------------------------------------
--< styles
-----------------------------------------------------------------------------------

drop table if exists chr.get_styles cascade;
create table if not exists chr.get_styles (
  model_id integer not null,
  styles jsonb not null);
comment on table chr.get_models_by_division is 'repository for raw data retreived from call to
  https://beta.rydellvision.com:8888/chrome/styles/ + model_id, after an initial load,
  limited model_id''s that do not yet exist in chr.styles';

drop table if exists chr.tmp_styles cascade;
create table if not exists chr.tmp_styles(
  chrome_style_id citext primary key,
  model_id integer not null,
  style_name citext not null);
comment on table chr.tmp_styles is 'repository for extracted json from chr.get_styles';    

drop table if exists chr.styles cascade;
create table if not exists chr.styles (
  chrome_style_id citext primary key,
  model_id integer not null references chr.models(model_id),
  style_name citext not null);
create index on chr.styles(model_id);

create or replace function chr.xfm_styles()
returns void as
$BODY$

  truncate chr.tmp_styles;
  insert into chr.tmp_styles (chrome_style_id,model_id,style_name)
  select (attributes #>> '{id}') as chrome_style_id, model_id, b."$value" as style_name
  from chr.get_styles a,
    jsonb_to_recordset(a.styles) as b("$value" citext, attributes jsonb);

  insert into chr.styles (chrome_style_id,model_id,style_name)
  select chrome_style_id, model_id, style_name
  from chr.tmp_styles
  on conflict (chrome_style_id)
  do nothing;
  
$BODY$
language sql;  

----------------------------------------------------------------------------------
--/> styles
----------------------------------------------------------------------------------



-----------------------------------------------------------------------------------
--< describe vehicle by style id with ShowAvailableEquipment
-----------------------------------------------------------------------------------
5/28/19
there are currently 124 records in chr.get_describe_vehicle_by_style_id where the only data in response
is 
{
    "attributes": {
        "country": "US",
        "language": "en"
    },
    "responseStatus": {
        "status": [
            {
                "$value": "406768",
                "attributes": {
                    "code": "UnrecognizedStyleId"
                }
            }
        ],
        "attributes": {
            "description": "Unsuccessful",
            "responseCode": "Unsuccessful"
        }
    }
}

which interpret to mean that there are chrome style ids assigned before there is detail data about them available
but, those style ids exist in the table which means that they will not be retried, so, need to get rid of them 
each time this is run, i believe

select chrome_style_id 
from chr.styles a
where not exists (
  select 1
  from chr.get_describe_vehicle_by_style_id
  where chrome_style_id = a.chrome_style_id) 

drop table if exists chr.get_describe_vehicle_by_style_id;
create table if not exists chr.get_describe_vehicle_by_style_id (
  chrome_style_id citext not null,
  response jsonb not null);
comment on table chr.get_describe_vehicle_by_style_id is 'repository for raw data retreived from call to
  https://beta.rydellvision.com:8888/chrome/describe-vehicle-show-available/ + stlye_id,
  after an initial load,  limited to style_id''s that do not yet exist in chr.get_describe_vehicle_by_style_id';

create or replace function chr.remove_unsuccessful_requests()
returns void as
$BODY$

  delete 
  -- select *
  from chr.get_describe_vehicle_by_style_id
  where response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Unsuccessful';

  do 
  $$
  begin
    assert (
      select count(*)
      from chr.get_describe_vehicle_by_style_id  e 
      where coalesce(e.response -> 'responseStatus' -> 'attributes' ->>'responseCode', 'xxxx') <> 'Successful') = 0, 'non successful request(s)';
  end
  $$;
  
$BODY$
language sql;  
comment on function chr.remove_unsuccessful_requests() is 'remove unsuccessful requests from chr.get_describe_vehicle_by_style_id
  and verify no other non-successful requests in the table';

 
-- now, all responses should be successful
-- 5/28, after updattting now up to 2356 records
select e.response -> 'responseStatus' -> 'attributes' ->>'responseCode', count(*)
from chr.get_describe_vehicle_by_style_id  e 
group by e.response -> 'responseStatus' -> 'attributes' ->>'responseCode'

select chrome_style_id -- 44 fleet only
from chr.get_describe_vehicle_by_style_id  e 
left join jsonb_array_elements(e.response->'style') as f(style) on true 
where f.style -> 'attributes' ->> 'fleetOnly' = 'true'

















-- this looks like it for basic vehicle description
select e.chrome_style_id, 
--   f.*, -- returns the entire style object as a jsonb object
--   f.style, -- returns the entire style object as a jsonb object, these first 2 return the identical results
--   f.style -> 'bodyType', -- returns the body_type array from the style object
--   jsonb_array_length(f.style -> 'bodyType'), -- returns the number of elements in the bodyType array
--   f.style -> 'bodyType' -> 0, -- returns the first object of the bodyType array
--   f.style -> 'bodyType' -> 0 -> '$value', -- returns the jsonb value associated with the "$value" key in the object
  f.style -> 'subdivision' -> 'attributes' ->> 'id' as subdivision_id,
  f.style -> 'bodyType' -> 0 ->> '$value' as body_style_1, -- returns the text value associated with the "$value" key in the object
  f.style -> 'bodyType' -> 1 ->> '$value' as body_style_2, -- returns the text value associated with the "$value" key in the object
  f.style -> 'attributes' ->> 'name' as style_name, f.style -> 'attributes' ->> 'trim' as trim_level, 
  f.style -> 'attributes' ->> 'fleetOnly' as fleet_only, f.style -> 'attributes' ->> 'passDoors' as passenger_doors, 
  f.style -> 'attributes' ->> 'drivetrain' as drivetrain, f.style -> 'attributes' ->> 'nameWoTrim' as name_wo_trim, 
  f.style -> 'attributes' ->> 'altBodyType' as alt_body_type, f.style -> 'attributes' ->> 'alt_style_name' as alt_style_name, 
  f.style -> 'attributes' ->> 'mfrModelCode' as model_code, f.style -> 'marketClass' ->> '$value' as market_class
from chr.get_describe_vehicle_by_style_id  e 
left join jsonb_array_elements(e.response->'style') as f(style) on true 
where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful';

-- engines
select e.chrome_style_id, 
  f.engines -> 'fuelType' ->> '$value' as fuel,
  f.engines -> 'engineType' ->> '$value' as engine_type,
  f.engines ->> 'cylinders' as cylinders,
  f.engines -> 'displacement' -> 'value' -> 0->> '$value' as displacement,
  f.engines -> 'displacement' -> 'value' -> 0 -> 'attributes' ->> 'unit' as units
from chr.get_describe_vehicle_by_style_id  e 
left join jsonb_array_elements(e.response->'engine') as f(engines) on true 
where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful';

-- standard equipment
select e.chrome_style_id, 
  f.standard -> 'header' ->> '$value' as the_header,
  f.standard -> 'header' -> 'attributes' ->> 'id' as header_id,
  f.standard -> 'category' -> 0 -> 'attributes' ->> 'id' as category_id,
  f.standard ->> 'description' as description
  -- select count(*)  -- 197253
from chr.get_describe_vehicle_by_style_id  e 
left join jsonb_array_elements(e.response->'standard') as f(standard) on true 
where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
limit 1000

-- exterior color
select e.chrome_style_id, 
  f.ext_color -> 'attributes' ->> 'colorCode' as color_code,
  f.ext_color -> 'attributes' ->> 'colorName' as color
from chr.get_describe_vehicle_by_style_id  e 
left join jsonb_array_elements(e.response->'exteriorColor') as f(ext_color) on true 
where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful';

-- interior color
select e.chrome_style_id, 
  f.int_color -> 'attributes' ->> 'colorCode' as color_code,
  f.int_color -> 'attributes' ->> 'colorName' as color
from chr.get_describe_vehicle_by_style_id  e 
left join jsonb_array_elements(e.response->'interiorColor') as f(int_color) on true 
where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful';

-- factory options
select e.chrome_style_id, 
  f.factory_option -> 'header' -> 'attributes' ->> 'id' as header_id,
  f.factory_option -> 'header' ->> '$value' as the_header,
  f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
  f.factory_option -> 'description' ->> 0
from chr.get_describe_vehicle_by_style_id  e 
left join jsonb_array_elements(e.response->'factoryOption') as f(factory_option) on true 
where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
limit 100


select chrome_style_id -- 43 fleet only
from chr.get_describe_vehicle_by_style_id  e 
left join jsonb_array_elements(e.response->'style') as f(style) on true 
where f.style -> 'attributes' ->> 'fleetOnly' = 'true'


select a.model_year, b.division, c.model, e.*
from chr.model_years a
join chr.divisions b on a.model_year = b.model_year
  and b.division in ('chevrolet','buick','gmc','cadillac','honda','nissan')
join chr.models c on b.model_year = c.model_year
  and b.division_id = c.division_id  
join chr.styles d on c.model_id = d.model_id  
left join ( -- basic vehicle description from chrome
  select e.chrome_style_id, 
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'bodyType' -> 0 ->> '$value' as body_style_1, -- returns the text value associated with the "$value" key in the object
    f.style -> 'bodyType' -> 1 ->> '$value' as body_style_2, -- returns the text value associated with the "$value" key in the object
    f.style -> 'attributes' ->> 'name' as style_name, f.style -> 'attributes' ->> 'trim' as trim_level, 
    f.style -> 'attributes' ->> 'fleetOnly' as fleet_only, f.style -> 'attributes' ->> 'passDoors' as passenger_doors, 
    f.style -> 'attributes' ->> 'drivetrain' as drivetrain, f.style -> 'attributes' ->> 'nameWoTrim' as name_wo_trim, 
    f.style -> 'attributes' ->> 'altBodyType' as alt_body_type, f.style -> 'attributes' ->> 'mfrModelCode' as model_code, 
    f.style -> 'marketClass' ->> '$value' as market_class
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful') e on d.chrome_style_id = e.chrome_style_id
where a.model_year > 2017
order by division, model, model_year, c.model_id, style_name


drop table if exists base_vehicles cascade;
create temp table base_vehicles as
select e.chrome_style_id, a.model_year, b.division, e.subdivision, c.model, e.model_code, e.trim_level, e.passenger_doors, 
  e.drivetrain, e.alt_body_type, fleet_only
from chr.model_years a
join chr.divisions b on a.model_year = b.model_year
  and b.division in ('chevrolet','buick','gmc','cadillac','honda','nissan')
join chr.models c on b.model_year = c.model_year
  and b.division_id = c.division_id  
join chr.styles d on c.model_id = d.model_id  
-- only those with a describe-vehicle record
join ( -- basic vehicle description from chrome
  select e.chrome_style_id, 
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'bodyType' -> 0 ->> '$value' as body_style_1, -- returns the text value associated with the "$value" key in the object
    f.style -> 'bodyType' -> 1 ->> '$value' as body_style_2, -- returns the text value associated with the "$value" key in the object
    f.style -> 'attributes' ->> 'name' as style_name, f.style -> 'attributes' ->> 'trim' as trim_level, 
    f.style -> 'attributes' ->> 'fleetOnly' as fleet_only, f.style -> 'attributes' ->> 'passDoors' as passenger_doors, 
    f.style -> 'attributes' ->> 'drivetrain' as drivetrain, f.style -> 'attributes' ->> 'nameWoTrim' as name_wo_trim, 
    f.style -> 'attributes' ->> 'altBodyType' as alt_body_type, f.style -> 'attributes' ->> 'mfrModelCode' as model_code, 
    f.style -> 'marketClass' ->> '$value' as market_class
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful') e on d.chrome_style_id = e.chrome_style_id
where a.model_year > 2017
order by division, model, model_year, c.model_id, trim_level;
create unique index on base_vehicles(chrome_style_id);
create index on base_vehicles(model_year);
create index on base_vehicles(division);
create index on base_vehicles(model);

drop table if exists base_engines cascade;
create table base_engines as
select chrome_style_id, option_code, replace(description, 'ENGINE, ', '') as engine
-- select *
from (
  select e.chrome_style_id, 
    f.factory_option -> 'header' -> 'attributes' ->> 'id' as header_id,
    f.factory_option -> 'header' ->> '$value' as the_header,
    f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
    f.factory_option -> 'description' ->> 0 as description
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'factoryOption') as f(factory_option) on true) x
  where the_header in ('ENGINE');
create unique index on base_engines(chrome_style_id, option_code);  


drop table if exists base_pegs cascade;
create table base_pegs as
select chrome_style_id, option_code, replace(description, 'PREFERRED EQUIPMENT GROUP', 'PEG') as peg
from (
  select e.chrome_style_id, 
    f.factory_option -> 'header' -> 'attributes' ->> 'id' as header_id,
    f.factory_option -> 'header' ->> '$value' as the_header,
    f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
    f.factory_option -> 'description' ->> 0 as description
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'factoryOption') as f(factory_option) on true) x
  where the_header in ('PREFERRED EQUIPMENT GROUP');
create unique index on base_pegs(chrome_style_id);  


-----------------------------------------------------------------------------------
--/> describe vehicle by style id with ShowAvailableEquipment
-----------------------------------------------------------------------------------


 ----------------------------------------------------------------------------------
--< categoryDefinitions
-----------------------------------------------------------------------------------
drop table if exists chr.get_category_definitions;
create table if not exists chr.get_category_definitions (
  categories jsonb not null);

select *
from chr.get_category_definitions

drop table if exists chr.category_definitions;
create table chr.category_definitions (
  the_type citext not null,
  type_id integer not null,
  the_group citext not null,
  group_id integer not null,
  the_header citext not null, 
  header_id integer not null,
  category citext not null,
  category_id integer primary key);
create unique index on chr.category_definitions(type_id,group_id,header_id,category_id);
  
insert into chr.category_definitions
-- select type_id, group_id, header_id, category_id from (
-- select category_id from (
select coalesce("type" #>> '{$value}', 'none') as the_type, coalesce("type" #> '{attributes}' #>> '{id}', '0')::integer as type_id,
  "group" #>> '{$value}' as the_group, ("group" #> '{attributes}' #>> '{id}')::integer as group_id,
  "header" #>> '{$value}' as the_header, ("group" #> '{attributes}' #>> '{id}')::integer as header_id,
  category #>> '{$value}' as category, (category #> '{attributes}' #>> '{id}')::integer as category_id
from chr.get_category_definitions,
jsonb_to_recordset(chr.get_category_definitions.categories) as b("type" jsonb, "group" jsonb, "header" jsonb, "category" jsonb);
-- ) x group by category_id having count(*) > 1
-- ) x group by type_id, group_id, header_id, category_id  having count(*) > 1
-- order by ("type" #> '{attributes}' #>> '{id}')::integer, "group" #> '{attributes}' #>> '{id}', "group" #> '{attributes}' #>> '{id}'

select *
from chr.category_definitions
where category_id in (1001)

 ----------------------------------------------------------------------------------
--/> categoryDefinitions
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
--/> describe vehicle by style id with ShowAvailableEquipment
-----------------------------------------------------------------------------------


 ----------------------------------------------------------------------------------
--< TechnicalSpecificationsDefinitions
-----------------------------------------------------------------------------------
drop table if exists chr.get_technical_specifications_definitions;
create table if not exists chr.get_technical_specifications_definitions (
  definitions jsonb not null);

select jsonb_pretty(definitions)
from chr.get_technical_specifications_definitions

drop table if exists chr.technical_specifications_definitions;
create table chr.technical_specifications_definitions (
  the_group citext not null,
  group_id integer not null,
  title citext not null,
  title_id integer not null,
  the_header citext not null, 
  header_id integer not null,
  measurement_unit citext not null);

insert into chr.technical_specifications_definitions
-- select type_id, group_id, header_id, category_id from (
-- select category_id from (
select "group" #>> '{$value}' as the_group, ("group" #> '{attributes}' #>> '{id}')::integer as group_id,
  "title" #>> '{$value}' as the_type, ("title" #> '{attributes}' #>> '{id}')::integer as type_id,
  "header" #>> '{$value}' as the_header, ("group" #> '{attributes}' #>> '{id}')::integer as header_id,
  coalesce("attributes" #>> '{measurementUnit}', 'None')
from chr.get_technical_specifications_definitions a,
jsonb_to_recordset(a.definitions) as b("group" jsonb, "title" jsonb, "header" jsonb, "attributes" jsonb);
-- ) x group by category_id having count(*) > 1
-- ) x group by type_id, group_id, header_id, category_id  having count(*) > 1
-- order by ("type" #> '{attributes}' #>> '{id}')::integer, "group" #> '{attributes}' #>> '{id}', "group" #> '{attributes}' #>> '{id}'

select *
from chr.technical_specifications_definitions

 ----------------------------------------------------------------------------------
--/> TechnicalSpecificationsDefinitions
-----------------------------------------------------------------------------------

 ----------------------------------------------------------------------------------
--< VersionInformation
-----------------------------------------------------------------------------------
drop table if exists chr.get_version_info;
create table if not exists chr.get_version_info (
  response jsonb);

select jsonb_pretty(response) from chr.get_version_info  
  

 ----------------------------------------------------------------------------------
--/> VersionInformation
-----------------------------------------------------------------------------------

-- 5/19/19
-- along with everything else, need a presentation for ben/taylor to do catgegorization

-- add engine, alloc_group, build out, start up
drop table if exists nc.tmp_chrome cascade;
create table nc.tmp_chrome as
select e.chrome_style_id::citext, a.*, b.division::citext, /*d.subdivision::citext,*/ c.model::citext, 
  f.model_code::citext, f.trim_level::citext, 
  f.peg::citext, 
  case 
    when f.alt_body_type like '%Bed%' then 'Pickup'
    else f.alt_body_type
  end::citext as body_type,
  case
    when f.alt_body_type like '%Bed%' then trim(left(f.alt_body_type, position('Cab' in f.alt_body_type) -1))
    when f.alt_body_type like '%Chassis%' then trim(left(f.alt_body_type, position(' ' in f.alt_body_type) - 1))
    else 'n/a'
  end::citext as cab,
  case
    when f.alt_body_type like '%Bed%' then trim(replace(substring(f.alt_body_type, position('-' in f.alt_body_type) + 2, position('Bed' in f.alt_body_type) -2), 'Bed', ''))
    else 'n/a'
  end::citext as box,  
  f.passenger_doors::citext, 
  case f.drivetrain
    when 'Front Wheel Drive' then 'FWD'
    when 'All Wheel Drive' then 'AWD'
    when 'Rear Wheel Drive' then 'RWD'
    when 'Four Wheel Drive' then '4WD'
    else 'XXXXXX'
  end::citext as drive_train,
  case 
    when b.division in ('Chevrolet','Buick','GMC','Cadillac') then replace(f.gm_engine, 'ENGINE, ', '') 
    when b.division in ('Honda', 'Nissan') then f.hn_engine
  end::citext as engine,
  case 
    when trim(replace(f.gm_engine, 'ENGINE, ', '')) = 'NONE' then '0'
    when coalesce(replace(f.gm_engine, 'ENGINE, ', ''), hn_engine) is null then '0'
    when position('L' in coalesce(replace(f.gm_engine, 'ENGINE, ', ''), hn_engine)) = 0 then coalesce(replace(f.gm_engine, 'ENGINE, ', ''), hn_engine) 
    else replace(substring(replace(coalesce(replace(f.gm_engine, 'ENGINE, ', ''), hn_engine), 'DIESEL',''), position('L' in replace(coalesce(replace(f.gm_engine, 'ENGINE, ', ''), hn_engine), 'DIESEL','')) -3, 4), 'L', '')
  end as eng_disp, 
  case 
    when b.division in ('Chevrolet','Buick','GMC','Cadillac') then f.gm_eng_option_code
    when b.division in ('Honda', 'Nissan') then f.hn_eng_option_code
  end::citext as engine_option_code,    
  f.style_name::citext, f.market_class::citext
from chr.model_years a
join chr.divisions b on a.model_year = b.model_year
  and b.division in ('chevrolet','buick','gmc','cadillac','honda','nissan')
join chr.models c on b.model_year = c.model_year
  and b.division_id = c.division_id    
-- join chr.subdivisions d on c.subdivision_id = d.subdivision_id  
join chr.styles e on c.model_id = e.model_id  
join ( -- basic vehicle description from chrome
  select e.chrome_style_id, 
    f.style -> 'division' ->> '$value' as division,
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'bodyType' -> 0 ->> '$value' as body_style_1, -- returns the text value associated with the "$value" key in the object
    f.style -> 'bodyType' -> 1 ->> '$value' as body_style_2, -- returns the text value associated with the "$value" key in the object
    f.style -> 'attributes' ->> 'name' as style_name, f.style -> 'attributes' ->> 'trim' as trim_level, 
    f.style -> 'attributes' ->> 'fleetOnly' as fleet_only, f.style -> 'attributes' ->> 'passDoors' as passenger_doors, 
    f.style -> 'attributes' ->> 'drivetrain' as drivetrain, f.style -> 'attributes' ->> 'nameWoTrim' as name_wo_trim, 
    f.style -> 'attributes' ->> 'altBodyType' as alt_body_type, f.style -> 'attributes' ->> 'mfrModelCode' as model_code, 
    f.style -> 'marketClass' ->> '$value' as market_class,
    g.pegs -> 'attributes' ->> 'oemCode' as peg,
    h.gm_engines -> 'description' ->> 0 as gm_engine,
    h.gm_engines -> 'attributes' ->> 'oemCode' as gm_eng_option_code,
    i.hn_engines -> 'displacement' -> 'value' -> 0 ->> '$value' as hn_engine,
    i.hn_engines -> 'attributes' ->> 'oemCode' as hn_eng_option_code
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as g(pegs) on true
    and g.pegs -> 'header' ->> '$value' = 'PREFERRED EQUIPMENT GROUP'
  left join jsonb_array_elements(e.response->'factoryOption') as h(gm_engines) on true
    and h.gm_engines -> 'header' ->> '$value' = 'ENGINE'
    and f.style -> 'division' ->> '$value' in ('Chevrolet','Buick','GMC','Cadillac')
  left join jsonb_array_elements(e.response -> 'engine') as i(hn_engines) on true
    and f.style -> 'division' ->> '$value' in ('Honda','Nissan')
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful') f on e.chrome_style_id = f.chrome_style_id
where a.model_year > 2017 
  and f.fleet_only = 'false'
order by b.division, /*d.subdivision,*/ c.model, a.model_year, e.chrome_style_id;

-- there are no values for hn engine option codes
select * from nc.tmp_chrome where division in ('honda','nissan')

-- these are all gm vehicles that have multiple engine options
select * from nc.tmp_chrome where chrome_style_id in (
select chrome_style_id from nc.tmp_chrome group by chrome_Style_id having count(*) > 1)


drop table if exists chr.gm_base_config cascade;
create table if not exists chr.gm_base_config (
  chrome_style_id citext primary key,
  model_year integer not null,
  make citext not null,
  model citext not null,
  model_code citext not null,
  trim_level citext,
  cab citext not null,
  box citext not null,
  drive citext not null);

insert into chr.gm_base_config  
select a.chrome_style_id,
  (r.style -> 'attributes'->> 'modelYear')::integer,
  r.style -> 'division'->> '$value',
  r.style -> 'model'->> '$value',
  r.style -> 'attributes' ->> 'mfrModelCode',
  r.style -> 'attributes' ->> 'trim',
  case
    when r.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
      then trim(
        left(r.style -> 'attributes' ->> 'altBodyType', position('Cab' in r.style -> 'attributes' ->> 'altBodyType') -1))
    when r.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
      then trim(
        left(r.style -> 'attributes' ->> 'altBodyType', position(' ' in r.style -> 'attributes' ->> 'altBodyType') - 1))
    else 'n/a'
  end::citext as cab,
  case
    when r.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
      then trim(
        replace(
          substring(r.style -> 'attributes' ->> 'altBodyType', position('-' in r.style -> 'attributes' ->> 'altBodyType') + 2, 
            position('Bed' in r.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
    else 'n/a'
  end::citext as box,  
  case r.style -> 'attributes' ->> 'drivetrain'
    when 'Front Wheel Drive' then 'FWD'
    when 'All Wheel Drive' then 'AWD'
    when 'Rear Wheel Drive' then 'RWD'
    when 'Four Wheel Drive' then '4WD'
    else 'XXXXXX'
  end::citext as drive_train 
from chr.get_describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and r.style ->'attributes'->>'modelYear' in ('2019','2020')
  and r.style ->'division'->>'$value' in ('Buick','Chevrolet','GMC','Cadillac');

-- thinking model_year, model_code, engine_code
-- maybe just chrome_Style_id
-- need option_code AND displacement
drop table if exists chr.gm_engines cascade;
create table chr.gm_engines (
  chrome_style_id citext not null,
  option_code citext not null,
  description citext not null,
  displacement citext not null,
  primary key (chrome_style_id,option_code));
  
insert into chr.gm_engines  
select a.chrome_style_id, 
  f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
  replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '') as description,
  case
    when position('L' in replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '')) = 0
      then replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '')
    else replace(
      substring(
        replace(
          replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''), 'DIESEL',''),
      position('L' in replace(
        replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''), 'DIESEL','')) -3, 4), 'L', '')
  end as displacement      
from chr.get_describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and r.style ->'attributes'->>'modelYear' in ('2019','2020')
  and r.style ->'division'->>'$value' in ('Buick','Chevrolet','GMC','Cadillac')
join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true  
  and f.factory_option -> 'header' ->> '$value' = 'ENGINE';  

drop table if exists chr.gm_peg_to_trim cascade;
create table chr.gm_peg_to_trim (
  chrome_style_id citext primary key,
  trim_level citext,
  option_code citext not null);
insert into chr.gm_peg_to_trim  
select a.chrome_style_id,
r.style -> 'attributes' ->> 'trim',
  f.factory_option -> 'attributes' ->> 'oemCode' as option_code
from chr.get_describe_vehicle_by_style_id a 
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and r.style ->'attributes'->>'modelYear' in ('2019','2020')
  and r.style ->'division'->>'$value' in ('Buick','Chevrolet','GMC','Cadillac')
join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true
  and f.factory_option -> 'header' ->> '$value' = 'PREFERRED EQUIPMENT GROUP';


-- select chrome_Style_id, color_code from(
select chrome_style_id, model_year::integer, make, model, color_code, color
from (
  select a.chrome_style_id,
    r.style ->'attributes'->>'modelYear' as model_year,
    r.style ->'division'->>'$value' as make,
    r.style ->'model'->>'$value' as model,
    b.colors->'attributes'->>'colorCode' as color_code,
    b.colors->'attributes'->>'colorName' as color,
    b.ordinality
  from chr.get_describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'exteriorColor') with ordinality as b(colors) on true
  join jsonb_array_elements(a.response->'style') as r(style) on true
    and r.style ->'attributes'->>'fleetOnly' = 'false') x
where model_year in ('2018','2019','2020')
-- ) x group by chrome_Style_id, color_code having count(*) > 1
group by model_year, make, model, color_code, color;

-- interiors
-- gm only - join on gmgl.vehicle_option_codes
select a.*, c.interiors -> 'attributes' ->> 'oemCode', 
  c.interiors -> 'description' ->> 0,
  left(c.interiors -> 'description' ->> 0, position(',' in c.interiors -> 'description' ->> 0) -1),
  case 
    when c.interiors -> 'description' ->> 0 like '%LEATHER%' then 'Leather'
    else 'Cloth'
  end 
from nc.vehicle_acquisitions a
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'factoryOption') as c(interiors) on true
  and c.interiors -> 'header' ->> '$value' in ('SEAT TRIM')
join gmgl.vehicle_option_codes d on a.vin = d.vin
  and c.interiors -> 'attributes' ->> 'oemCode' = d.option_code
where a.thru_date > current_date


-- honda nissan
-- until we have invoices, this is available interiors
select a.*, aa.make, c.interiors -> 'attributes' ->> 'oemCode', 
  c.interiors -> 'description' ->> 0,
  left(c.interiors -> 'description' ->> 0, position(',' in c.interiors -> 'description' ->> 0) -1),
  case 
    when c.interiors -> 'description' ->> 0 like '%LEATHER%' then 'Leather'
    else 'Cloth'
  end 
from nc.vehicle_acquisitions a
join nc.vehicles aa on a.vin = aa.vin
    and aa.make in ('honda','nissan')
join chr.describe_vehicle b on a.vin = b.vin
join jsonb_array_elements(b.response->'factoryOption') as c(interiors) on true
  and c.interiors -> 'header' ->> '$value' in ('SEAT TRIM')
where a.thru_date > current_date



































-- available options for chrome_Style_id
select h.factoryoption -> 'header' ->> '$value' as header,
  h.factoryoption -> 'attributes' ->> 'oemCode' as option_code,
  h.factoryoption -> 'description' ->> 0 as description
from chr.get_describe_vehicle_by_style_id a
left join jsonb_array_elements(a.response->'factoryOption') as h(factoryOption) on true 
where a.chrome_Style_id = '405262'

-- these 62 options come from the global connect order page
-- this give 30 of the 62 options from vehicle_order_options from chrome factoryoptions
-- i don't know if the other oemcodes are available anywhere in chrome,
select a.order_number, a.option_code, a.*, bb.*
from gmgl.vehicle_order_options a
left join (
  select h.factoryoption -> 'header' ->> '$value' as header,
    h.factoryoption -> 'attributes' ->> 'oemCode' as option_code,
    h.factoryoption -> 'description' ->> 0 as description
  from chr.get_describe_vehicle_by_style_id a
  left join jsonb_array_elements(a.response->'factoryOption') as h(factoryOption) on true 
  where a.chrome_Style_id = '405262') bb on a.option_code = bb.option_code
where order_number = 'WXNPD7'
  and thru_ts > now()
order by a.option_code  



-- available options from chrome
select h.factoryoption -> 'header' ->> '$value' as header,
  h.factoryoption -> 'attributes' ->> 'oemCode' as option_code,
  h.factoryoption -> 'description' ->> 0 as description
from chr.get_describe_vehicle_by_style_id a
left join jsonb_array_elements(a.response->'factoryOption') as h(factoryOption) on true 
where a.chrome_Style_id = '405262'

-- looked thru this full response, didn't find and of the missing option codes
-- try it with additional switches?
select jsonb_pretty(response)
from chr.get_describe_vehicle_by_style_id a
where a.chrome_Style_id = '405262'

drop table if exists options;
create temp table options as
select a.vin, a.model_year, a.model_code, a.option_code, 
  b.description,
  e.header, e.description as chr_desc
from gmgl.vehicle_option_codes a
left join gmgl.option_codes b on a.model_year = b.model_year
  and a.model_code = b.model_code
  and a.option_code = b.option_code
  and a.vehicle_description = b.vehicle_description
left join (
  select a.vin, h.factoryoption -> 'header' ->> '$value' as header,
    h.factoryoption -> 'attributes' ->> 'oemCode' as option_code,
    h.factoryoption -> 'description' ->> 0 as description
  from chr.describe_vehicle a
  left join jsonb_array_elements(a.response->'factoryOption') as h(factoryOption) on true) e on a.vin = e.vin
    and a.option_code = e.option_code

-- current inventory
select b.stock_number, c.make, c.model, a.*
from options a
join nc.vehicle_acquisitions b on a.vin = b.vin
  and b.thru_Date > current_date 
join nc.vehicles c on b.vin = c.vin  
where a.model_year = '2019'  
order by b.stock_number  


select vin, option_code
from options
group by vin, option_code
having count(*) > 1


this one is interesting, 2018 cts, chrome includes everything from the invoice
of the other options, in chrome, some are included in standard
selecT *
from options
where vin = '1G6AX5SX5J0136510'
order by option_code

select *
from gmgl.vehicle_invoices
where vin = '1G6AX5SX5J0136510'

-- 2019 encore 
selecT *
from options
where vin = 'KL4CJESB5KB824504'
order by option_code

select jsonb_pretty(response) from chr.describe_vehicle where vin = 'KL4CJESB5KB824504'

select *
from gmgl.vehicle_invoices
where vin = 'KL4CJESB5KB824504'


  
    