﻿select distinct chrome_style_id
from nc.vehicle_configurations
where model_year in (2019,2020)
  and model = 'equinox'

select *
from gmgl.vin_incentive_cash
where sup_inc <> 0
  and emp_inc <> 0
order by from_ts desc   

select *
from gmgl.incentive_program_rules
where has_sup_inc
  and current_row
order by from_Ts desc  

select *
from gmgl.incentive_programs
where program_key = 1488

select *
from gmgl.major_incentives a
join nc.vehicle_acquisitions b on a.vin = b.vin
where program_key = 1488

select *
from (
  select a.program_number, a.program_name, b.vin, max(b.lookup_date) as lookup_date, a.program_Start_date, a.program_end_date
  from gmgl.incentive_program_rules aa
  join gmgl.incentive_programs a on aa.program_key = a.program_key
  join gmgl.major_incentives b on a.program_key = b.program_key
  where aa.has_sup_inc
    and aa.current_row
  group by a.program_number, a.program_name, b.vin, a.program_Start_date, a.program_end_date) x
join nc.vehicle_acquisitions y on x.vin = y.vin
  and y.thru_date > current_date  
join nc.vehicles z on y.vin = z.vin  

select *
from gmgl.vin_incentive_cash
where sup_inc <> 0
  and thru_ts::date > '01/01/2020'
limit 100


select *
from gmgl.vin_incentive_cash
where sup_inc <> 0
  and vin = 'KL7CJLSB6KB961734'
order by from_ts desc
limit 1

-- 2019 trax
-- lookup date 3/6/20
20-40ACA-003 
start: 3/3/20
end: 3/31/20
select c.*
from gmgl.major_incentives a
join nc.vehicles b on a.vin = b.vin
join gmgl.incentive_programs c on a.program_key = c.program_key
where a.vin = 'KL7CJLSB6KB961734'
  and major_incentive_key = 2254016

lets use this  


select *
from chr.incentives_by_vin
limit 1

drop table if exists chr.incentive_schema_versions_by_vin;
create table chr.incentive_schema_versions_by_vin (
  the_date date,
  schema_version numeric(3,1),
  vin citext, 
  response jsonb);