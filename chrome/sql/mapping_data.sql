﻿-- 07/10/23
-- renamed the tables with an _a suffix, want to see what new data looks like relative to existing data to figure out what
-- periodic updating of the data will look like
-- 
-- python file: python_projects/chrome/download_mapping_data.py
-- files from chrome are in E:\python_projects\chrome\black_book_mapping & E:\python_projects\chrome\kelley_blue_book_mapping
-- 
-- black book failed
-- psycopg2.errors.ForeignKeyViolation: insert or update on table "black_book_required_options_a" 
-- violates foreign key constraint "black_book_required_options_a_mapping_id_fkey"
-- DETAIL:  Key (mapping_id)=(28783) is not present in table "black_book_vehicle_map_a".
-- 
-- no option but to remove the FK reference, but what sense is it to have a mapping_id in chr.black_book_required_options
-- that does not exist in chr.black_book_vehicle_map

-- now this one makes no sense
-- psycopg2.errors.UniqueViolation: duplicate key value violates unique constraint "black_book_version_info_a_pkey"
-- DETAIL:  Key (chrome_data_release_id)=(387625) already exists.
-- CONTEXT:  COPY black_book_version_info_a, line 118
yep, in the file: so, have to change the PK 
~1.0~,~08/16/2020 00:00~,387625,~20230703~,~July-August 2023~
~1.0~,~08/15/2020 20:00~,387625,~20230703~,~July-August 2023~

select * from chr.black_book_version_info order by chrome_Data_release_id


drop table if exists chr.black_book_vehicle_map_a cascade;
create table chr.black_book_vehicle_map_a (
  mapping_id citext primary key,
  style_id citext,
  uvc citext);
create index on chr.black_book_vehicle_map_a(style_id);
create index on chr.black_book_vehicle_map_a(uvc);

--7/10/23 had to remove the FK reference, data has non-conforming values
-- ALTER TABLE chr.black_book_required_options DROP CONSTRAINT black_book_required_options_mapping_id_fkey;
drop table if exists chr.black_book_required_options_a cascade;
 create table chr.black_book_required_options_a(
   required_option_id citext primary key, 
--    mapping_id citext not null references chr.black_book_vehicle_map_a(mapping_id),
   mapping_id citext not null, 
   option_code citext,
   uoc citext,
   add_deduct citext);
create index on chr.black_book_required_options_a(mapping_id);
create index on chr.black_book_required_options_a(option_code);
create index on chr.black_book_required_options_a(uoc);

-- change PK from chrome_data_release_id to chrome_data_release_id,chrome_data_date
ALTER TABLE chr.black_book_version_info DROP CONSTRAINT black_book_version_info_pkey;
ALTER TABLE chr.black_book_version_info
  ADD CONSTRAINT black_book_version_info_pkey PRIMARY KEY(chrome_data_release_id,chrome_data_date);
  
ALTER TABLE chr.black_book_version_info_a DROP CONSTRAINT black_book_version_info_a_pkey;
ALTER TABLE chr.black_book_version_info_a
  ADD CONSTRAINT black_book_version_info_a_pkey PRIMARY KEY(chrome_data_release_id,chrome_data_date);
  
drop table if exists chr.black_book_version_info_a cascade;
create table chr.black_book_version_info_a (
  mapping_version numeric,
  chrome_data_date timestamp,
  chrome_data_release_id citext primary key,
  bb_period integer,
  display_period citext);
   

drop table if exists chr.kelley_blue_book_vehicle_map_a cascade;
create table chr.kelley_blue_book_vehicle_map_a (
  mapping_id citext primary key,
  style_id citext,
  vehicle_id citext);
create index on chr.kelley_blue_book_vehicle_map_a(style_id);
create index on chr.kelley_blue_book_vehicle_map_a(vehicle_id);

drop table if exists chr.kelley_blue_book_required_options_a cascade;
 create table chr.kelley_blue_book_required_options_a(
   required_option_id citext primary key, 
   mapping_id citext not null references chr.kelley_blue_book_vehicle_map_a(mapping_id),
   vehicle_option_id citext,
   option_code citext,
   add_deduct citext,
   option_display_name citext);
create index on chr.kelley_blue_book_required_options_a(mapping_id);
create index on chr.kelley_blue_book_required_options_a(option_code);
create index on chr.kelley_blue_book_required_options_a(vehicle_option_id);


drop table if exists chr.kelley_blue_book_version_info_a cascade;
create table chr.kelley_blue_book_version_info_a (
  mapping_version citext,
  chrome_data_date timestamp,
  chrome_data_release_id citext primary key,
  kbb_period citext,
  display_period citext,
  schema_version numeric);
   

no  other option for a unique index in the required_options tables 
select mapping_id, vehicle_option_id
from chr.kelley_blue_book_required_options
group by mapping_id, vehicle_option_id
having count(*) > 1

 
select required_option_id
from chr.black_book_required_options
group by required_option_id
having count(*) > 1

--------------------------------------------------------------------------------------------------------------------------------------------
-- 07/11/23
-- compare _a tables to 202210 tables
-- black book
select *
from chr.black_book_vehicle_map a
left join chr.black_book_required_options b on a.mapping_id = b.mapping_id

select a.* , b.*, c.vin, c.model_year, c.make, c.model
from chr.black_book_vehicle_map_a a
full outer join chr.black_book_vehicle_map b on a.mapping_id = b.mapping_id
left join cu.tmp_vehicles c on coalesce(a.style_id, b.style_id) = c.chrome_style_id
order by a.style_id::integer, b.style_id::integer

select a.* 
from chr.black_book_vehicle_map_a a
left join chr.black_book_vehicle_map b on a.mapping_id = b.mapping_id
where b.mapping_id is null 

-- kelley
select a.* , b.*, c.vin, c.model_year, c.make, c.model
from chr.kelley_blue_book_vehicle_map_a a
full outer join chr.kelley_blue_book_vehicle_map b on a.mapping_id = b.mapping_id
left join cu.tmp_vehicles c on coalesce(a.style_id, b.style_id) = c.chrome_style_id
order by a.style_id::integer, b.style_id::integer

--------------------------------------------------------------------------------------------------------------------------------------------
select * from chr.black_book_version_info_a
-- 07/12/23
-- do the fucking update
-- blackbook
-- completed 7/12/23
-- 08/05/23 unsure about this, but completed the below updates
--   leaning towards simply replacing the tables each month
insert into chr.black_book_vehicle_map
select a.* 
from chr.black_book_vehicle_map_a a
left join chr.black_book_vehicle_map b on a.mapping_id = b.mapping_id
where b.mapping_id is null; 

insert into chr.black_book_required_options
select a.* 
from chr.black_book_required_options_a a
left join chr.black_book_required_options b on a.required_option_id = b.required_option_id
where b.required_option_id is null; 

insert into chr.black_book_version_info
select a.* 
from chr.black_book_version_info_a a
left join chr.black_book_version_info b on a.chrome_data_release_id = b.chrome_data_release_id
  and a.chrome_data_date = b.chrome_data_date
where b.chrome_data_release_id is null; 


-- bluebook
-- completed 7/12/23
insert into chr.kelley_blue_book_vehicle_map
select a.* 
from chr.kelley_blue_book_vehicle_map_a a
left join chr.kelley_blue_book_vehicle_map b on a.mapping_id = b.mapping_id
where b.mapping_id is null; 

insert into chr.kelley_blue_book_required_options
select a.* 
from chr.kelley_blue_book_required_options_a a
left join chr.kelley_blue_book_required_options b on a.required_option_id = b.required_option_id
where b.required_option_id is null;

insert into chr.kelley_blue_book_version_info
select a.* 
from chr.kelley_blue_book_version_info_a a
left join chr.kelley_blue_book_version_info b on a.chrome_data_release_id = b.chrome_data_release_id
where b.chrome_data_release_id is null; 