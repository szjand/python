﻿
select *
from nc.vehicles a
join nc.vehicle_acquisitions b on a.vin = b.vin
  and b.thru_Date > current_date
where model = 'colorado'
order by model_year desc

2020 Colorado crew Z71 3.6 406959 :1GCGTDEN8L1238100

select * from chr.describe_Vehicle where vin = '1GCGTDEN8L1238100'

select * from nc.vehicles where vin = '1GCGTDEN8L1238100'

select distinct d->'header'->>'$value' as header, d->'styleId'->>0 as styleid, d->'installed'->'attributes'->>'cause'as installed, d->>'description' as description
from chr.describe_vehicle a
left join jsonb_array_elements(a.response->'style') as b(style) on true 
left join jsonb_array_elements(a.response->'factoryOption') as c(factory_options) on true
left join jsonb_array_elements(a.response->'standard') as d(standard) on true
where vin = '1GCGTDEN8L1238100'
-- group by d -> 'header' ->> '$value', d ->> 'description'
order by d->'header'->>'$value', d->>'description'

select distinct c->'header'->>'$value' as header, c->'attributes'->>'oemCode' as oem_code, c->'attributes'->>'standard' as standard, 
  c->'installed'->'attributes'->>'cause' as installed, c->'description'->>0 as description
from chr.describe_vehicle a
left join jsonb_array_elements(a.response->'style') as b(style) on true 
left join jsonb_array_elements(a.response->'factoryOption') as c(factory_options) on true
left join jsonb_array_elements(a.response->'standard') as d(standard) on true
where vin = '3GCUYDED1LG258119'
order by c->'attributes'->>'oemCode'
order by c -> 'header' ->> '$value', c->'description'->>0

-- looks like i didn't save any build data requests
select vin
from chr.describe_vehicle a
where response->'vinDescription'->'attributes'->>'builddata' = 'yes'

select * 
from gmgl.vehicle_option_codes 
where vin = '1GCGTDEN8L1238100'

select * 
from gmgl.option_codes
where model_year = '2020'
  and model_code = '12P43'


select distinct c->'header'->>'$value' as header, c->'attributes'->>'oemCode' as oem_code, 
  c->'attributes'->>'standard' as standard, c->'installed'->'attributes'->>'cause' as installed_cause, c->>'description', e.description
from chr.describe_vehicle a
left join jsonb_array_elements(a.response->'style') as b(style) on true 
left join jsonb_array_elements(a.response->'factoryOption') as c(factory_options) on true
left join jsonb_array_elements(a.response->'standard') as d(standard) on true
left join gmgl.option_codes e on c->'attributes'->>'oemCode' = e.option_code
  and e.model_year = '2020'
  and e.model_code = '12P43'
where vin = '1GCGTDEN8L1238100'
order by c -> 'header' ->> '$value'


select * 
from gmgl.vehicle_option_codes c
left join gmgl.option_codes e on c.option_code = e.option_code
  and e.model_year = '2020'
  and e.model_code = 'CK10543'
where vin = '3GCUYDED1LG258119' order by c.option_code

select vin from nc.vehicle_acquisitions where stock_number = 'g39375r'

select * from nc.vehicles where vin = '3GCUYDED1LG258119'

select * from gmgl.vehicle_invoices where vin = '3GCUYDED1LG258119'