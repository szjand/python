﻿select * 
from jon.describe_vehicle_by_Style_id
limit 10


select count(distinct chrome_style_id) -- 3381
from jon.describe_vehicle_by_Style_id


select chrome_Style_id, min(from_date) as the_first, max(from_date) as the_last, count(*) as how_many, 
  max(from_date) - min(from_date)
from jon.describe_vehicle_by_style_id
group by chrome_style_id
order by min(from_date)


select a.chrome_Style_id, min(a.from_date) as the_first, max(a.from_date) as the_last, count(*) as how_many, 
  max(a.from_date) - min(a.from_date) as days_from_first, f.style -> 'attributes' ->> 'modelYear' as model_year
from jon.describe_vehicle_by_style_id a
left join jsonb_array_elements(a.response->'style') as f(style) on true 
group by a.chrome_style_id, f.style -> 'attributes' ->> 'modelYear'
order by min(a.from_date)

select c.*, current_date - the_last as days_since_last_change
from (
  select a.chrome_Style_id, min(a.from_date) as the_first, max(a.from_date) as the_last, count(*) as how_many, 
    max(a.from_date) - min(a.from_date) as days_between_first_and_last, f.style -> 'attributes' ->> 'modelYear' as model_year
  from jon.describe_vehicle_by_style_id a
  left join jsonb_array_elements(a.response->'style') as f(style) on true 
  group by a.chrome_style_id, f.style -> 'attributes' ->> 'modelYear'
  order by min(a.from_date)) c
order by  current_date - the_last, the_first


select d.chrome_style_id,null,null,d.chrome_style_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
-- select *  -- 662
from jon.model_years a
join jon.divisions b on a.model_year = b.model_year
  and b.division in ('chevrolet','buick','gmc','cadillac','honda','nissan')
join jon.models c on a.model_year = c.model_year
  and b.division_id = c.division_id
  and c.model not like '4500%'
  and c.model not like '5500%'
  and c.model not like '6500%'
  and c.model not like '%LCF%'
join jon.styles d on c.model_id = d.model_id
where a.model_year = 2021
limit 2


select d.chrome_style_id,null,null,d.chrome_style_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
-- select *  -- 662
from jon.model_years a
join jon.divisions b on a.model_year = b.model_year
  and b.division in ('chevrolet','buick','gmc','cadillac','honda','nissan')
join jon.models c on a.model_year = c.model_year
  and b.division_id = c.division_id
  and c.model not like '4500%'
  and c.model not like '5500%'
  and c.model not like '6500%'
  and c.model not like '%LCF%'
join jon.styles d on c.model_id = d.model_id
where a.model_year between 2018 and 2020

                    
-- -- this doesnt make sense, 2 hours for only 555 styleids?
-- select from_date, count(*)
-- from jon.describe_vehicle_by_style_id
-- group by from_date
-- order by from_date
-- 
-- select *
-- from jon.describe_vehicle_by_style_id
-- where from_date = '11/22/2020'
-- 
-- 
-- select the_date,
--   max(case when task = 'styles' then thru_ts - from_ts end) as styles,
--   max(case when task = 'DescribeVehicleByStyleID' then thru_ts - from_ts end) as describe
-- --   DescribeVehicleByStyleID
-- from luigi.luigi_log
-- where pipeline = 'chrome'
--   and status = 'pass'
-- group by the_date  
-- order by the_date
-- 
-- -- why oh why did the process jump to 2 hours on 6/20/20
-- -- don't forget, this from date is just the first date the style_id shows up
-- select *
-- from (
--   select from_date, count(*)
--   from jon.describe_vehicle_by_style_id
--   group by from_date) aa
-- left join (  
--   select the_date,
--     max(case when task = 'styles' then thru_ts - from_ts end) as styles,
--     max(case when task = 'DescribeVehicleByStyleID' then thru_ts - from_ts end) as describe
--   --   DescribeVehicleByStyleID
--   from luigi.luigi_log
--   where pipeline = 'chrome'
--     and status = 'pass'
--   group by the_date) bb on aa.from_date = bb.the_date
-- order by aa.from_date  