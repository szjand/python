﻿-- 05/23/20  nissan

select * from nc.vehicles where vin = '1N4AA6DV5LC375854'
select * from nc.vehicle_configurations where configuration_id = 801
select * from jon.configurations where chrome_style_id = '405905'

select *
from chr.incentives_by_vin
where vin = '1N4AA6DV5LC375854'

in nissan 
  no idean what the 1st 7 programs are (20-1-0177v3 -> 20-1-1037)
  i->>'programDescription' = program name on the incentive lookup page
  do not initially see any correlation to may apply vs likely apply
    eg 20N2299RBR & 20N2299RBO show in nissan as likely apply
        while in chrome they show a residency rule
here is all the incentive data for 20N2299RBO:
    {
      "type": "Cash",
      "market": {
        "id": 1,
        "description": "Retail"
      },
      "region": {
        "id": 1696,
        "countryId": "US",
        "description": "Nissan US - MTN Region - DMA 724 (Fargo - 58103)"
      },
      "source": "OEM",
      "taxRule": {
        "id": 131,
        "description": "Unknown"
      },
      "category": {
        "id": 121,
        "group": "Cash",
        "description": "Stand Alone Cash"
      },
      "expiryDate": "2020-06-01",
      "incentiveID": "18296128",
      "programText": "Any customer Purchasing an eligible vehicle from any authorized Nissan Dealer during the program period is eligible to receive the Customer Cash.",
      "signatureID": 5737,
      "endRecipient": {
        "id": 61,
        "description": "Customer"
      },
      "effectiveDate": "2020-05-05",
      "programNumber": "20N2299RBO",
      "programValues": {
        "valueVariationList": [
          {
            "programValueList": [
              {
                "cash": 2500
              }
            ]
          }
        ]
      },
      "institutionList": [
        {
          "id": 109,
          "description": "OEM"
        }
      ],
      "programRuleList": [
        {
          "type": "Eligibility",
          "description": "Residents residing in qualifying regions of the United States listed in the BUSINESS CENTER BOUNDARIES section."
        }
      ],
      "deliveryTypeList": [
        {
          "id": 45,
          "description": "Non-subvented Finance"
        },
        {
          "id": 46,
          "description": "Non-subvented Lease"
        },
        {
          "id": 47,
          "description": "Cash Purchase"
        }
      ],
      "groupAffiliation": {
        "id": 0,
        "description": "No Specific Group Affiliation"
      },
      "previousOwnership": {
        "id": 0,
        "description": "No Previous Ownership Requirement"
      },
      "vehicleStatusList": [
        {
          "id": 48,
          "description": "New"
        }
      ],
      "programDescription": "Nissan Customer Cash",
      "signatureHistoryID": 49378
    }  

    
select i->>'programNumber' as program_number, i->>'programDescription' as program_description,
  i->'market'->>'description' as "Dealer/Retail", i->'category'->>'description' as category,
  i->'programValues'->'valueVariationList'->0->'programValueList'->0 as program_value,
  i->'programRuleList'->0->>'type' as rule_type, i->'programRuleList'->0->>'description' as rule_description,
  i->'groupAffiliation'->>'description' as group_affiliation,
  i->'previousOwnership'->>'description' as prev_ownership,
  i->'vehicleStatusList',
  i->'signatureID' as sig_id, i->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
where vin = '1N4AA6DV5LC375854'
order by i->>'programNumber'


