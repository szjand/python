﻿/*
version 372849 dated 1/21/2020 attempted to update on 1/22/2020
function jon.xfm_configurations fails :
psycopg2.errors.UniqueViolation: duplicate key value violates unique constraint "ext_configurations_model_year_model_code_peg_idx"
DETAIL:  Key (model_year, model_code, peg)=(2019, 32119, n/a) already exists.
the update includes 4 new chrome_style_ids 408189, 408190, 408191, 408192 for 2019 Nissan Frontier that are only unique with a minor BaseEquipment description
everything else is identical to existing style_ids
for the meantime, i am excluding those 4 style_ids in the body of the function

complete with a breaking change in the python file, so that i have to look at it again at the next update

same thing on 1/27/20
same thing on 2/10/20
*/

drop table if exists wtf;
create temp table wtf as
  select aa.chrome_style_id,aa.model_year::integer, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
    aa.drive, aa.model_code, aa.box, aa.trim_level, coalesce(aa.peg, 'n/a') as peg, aa.style_name, 
    aa.subdivision, aa.marketclass, aa.body_type
  from (
    select e.chrome_style_id,
      f.style -> 'attributes' ->> 'modelYear' as model_year,
      f.style -> 'division'  ->> '$value' as make,
      f.style -> 'model'  ->> '$value' as model,
      f.style -> 'attributes' ->> 'mfrModelCode' as model_code,
      f.style -> 'attributes' ->> 'trim' as trim_level,
     case f.style -> 'attributes' ->> 'drivetrain'
        when 'Front Wheel Drive' then 'FWD'
        when 'All Wheel Drive' then 'AWD'
        when 'Rear Wheel Drive' then 'RWD'
        when 'Four Wheel Drive' then '4WD'
        else 'XXXXXX'
      end::citext as drive,     
  -- changed extended to double
      case
        when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
          then 
            case
              when trim(
                left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1)) = 'Extended' then 'Double'
              else trim(left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
            end
        when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
          then trim(
            left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
        else 'n/a'
      end::citext as cab,
      case
        when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
          then trim(
            replace(
              substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
                position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
        else 'n/a'
      end::citext as box,        
      h.pegs -> 'attributes' ->> 'oemCode' as peg,
      f.style -> 'attributes' ->> 'name' as style_name,
      f.style -> 'subdivision' ->> '$value' as subdivision,
      f.style -> 'marketClass' ->> '$value' as marketClass,
      g.bt ->> '$value' as body_type
    from jon.describe_vehicle_by_style_id  e 
    left join jsonb_array_elements(e.response->'style') as f(style) on true 
    left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
      and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
    left join jsonb_array_elements(f.style -> 'bodyType') with ordinality as g(bt) on true
      and g.bt -> 'attributes' ->> 'primary' = 'true'    
    where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
      and f.style -> 'attributes' ->> 'fleetOnly' = 'false'
      and e.current_row) aa   
  left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
    and aa.model_code = bb.model_code
where aa.chrome_style_id not in ('408189','408190','408191','408192')    


select *
from wtf a
join (
  select model_year, model_code, peg
  from wtf
  group by model_year, model_code, peg
  having count(*) > 1) b on a.model_year = b.model_year and a.model_code = b.model_code and a.peg = b.peg
order by a.model_year, a.model_code,chrome_style_id
  
select * from nc.vehicles 
where chrome_Style_id in (
select chrome_style_id
from wtf a
join (
  select model_year, model_code, peg
  from wtf
  group by model_year, model_code, peg
  having count(*) > 1) b on a.model_year = b.model_year and a.model_code = b.model_code and a.peg = b.peg)


select *
from nc.vehicle_configurations
where chrome_Style_id in ('401161','401163')

select *
from jon.describe_Vehicle_by_style_id
where chrome_Style_id in ('401149','408190')


select * from jon.get_Version_info

select chrome_style_id, chrome_version, from_Date, thru_date, current_Row
from jon.describe_Vehicle_by_style_id
where chrome_Style_id in (
select chrome_style_id
from wtf a
join (
  select model_year, model_code, peg
  from wtf
  group by model_year, model_code, peg
  having count(*) > 1) b on a.model_year = b.model_year and a.model_code = b.model_code and a.peg = b.peg)
order by chrome_Style_id, from_date  


-- 1/27/90

select *
from wtf
where chrome_style_id in ('408189', '408190', '408191', '408192')


401149/408190 & 401155/401189 are identical except for the standard equipment detail in 2 places and different acodes

select *
from jon.describe_Vehicle_by_style_id
where chrome_Style_id in ('401163','408192')
  and current_row


-- here are the problems, them multiple chrome style ids for a model code
-- which violate the unique index in configurations: model_year, model_code, peg
-- there is no peg equivalent in honda/nissan
select *
from wtf
where model_year = 2019
  and model_code in ('32119','31319','32219','32419')
order by model_code, chrome_style_id

and there are a lot of non unique model_year/model_code combinations
but, throw in the peg, and it is only this fucking frontier
suspect chrome is fucked up, but dont know how to phrase the question
select *
from wtf a
join (
  select model_year, model_code
  from wtf
  group by model_year, model_code
  having count(*) > 1) b on a.model_year = b.model_year and a.model_code = b.model_code
where make in ('Honda','Nissan')  
order by a.model_code, a.chrome_style_id  



select *
from wtf
where model_code in ( '32119','31319','32219','32419') 
order by model_code, chrome_style_id

select *
from jon.configurations
where model_code in ( '32119','31319','32219','32419') 
order by model_code, chrome_style_id

select *
from wtf
where model_year = 2019
  and make = 'Nissan'
  and model = 'Frontier'


select *
from jon.describe_Vehicle_by_style_id
where chrome_Style_id in (
select chrome_style_id
from wtf
where model_code in ( '32119','31319','32219','32419'))
and current_row
order by chrome_style_id, chrome_version


  select model_year, model_code
  from wtf
  where make in ('honda','nissan')
  group by model_year, model_code
  having count(*) > 1
  