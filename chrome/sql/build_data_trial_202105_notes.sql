﻿select * from chr.build_data_trial where vin = '1FTEW1E51LFA95605'

select jsonb_pretty(response) from chr.build_data_trial where vin = '1FTEW1E51LFA95605'

select vin, pg_column_size(response) from chr.build_data_trial

why is the response truncated for the ford???
text and presumablly jsonb max size is 1G

2021 F150
      "installed": {
        "cause": "OptionCodeEngineered",
        "detail": 

Source: Indicates the source of the data; Sparse, Build Data, Engineered Data or Catalog. Sparse is the value if the VIN doesnt reduce to a style and therefore little data is returned.

05/16/21
Installation Causes p 21       
• Engine - item installed as part of or due to an engine associated with the VIN
• RelatedCategory – item installed because it is fundamentally the same thing (ex: Automatic Transmission and 4 Speed Automatic Transmission)
• RelatedColor – color installed because they are fundamentally the same color (ex: blue and royal blue)
• CategoryLogic – installed due to a rule associated with a category. (ex: V6 engine requires Automatic trans)
• OptionLogic – installed due to a rule associated with an option (ex: Option ABC requires option XYZ)
• OptionCodeBuild – the option code was found in OEM build data for this vehicle (specific license required)
• ExteriorColorBuild – the exterior color was found in OEM build data for this vehicle (specific license required)
• InteriorColorBuild – the interior color was found in OEM build data for this vehicle (specific license required)
• OptionCodeEngineered – the option code was found in engineered data for this vehicle (specific license required)
• ExteriorColorEngineered – the exterior color was found in engineered data for this vehicle (specific license required)
• InteriorColorEngineered – the interior color was found in engineered data for this vehicle (specific license required)
• EquipmentDescriptionInput – this equipment was specified in the request to the ADS system (user supplied)
• ExteriorColorInput – this exterior color was specified in the request to the ADS system (user supplied)
• InteriorColorInput – this interior color was specified in the request to the ADS system (user supplied)
• OptionCodeInput – this option was specified in the request to the ADS system (user supplied)
• BaseEquipment – this item is considered base equipment, or “standard” for this vehicle
• VIN – this item was specified by the VIN
• NonFactoryEquipmentInput – this item was passed in the NonFactoryEquipment field in the ADS request, and is simply installed as specified (user supplied)
• PrimaryOptionNameInput – this option(s) was an exact match of the Primary Option Description (user supplied).

20 F150 no engineered
under engine[] 5 options
	1 is installed:{cause: VIN}, all others are installed: null
under standard[]
	most are installed:null
	some are isntalled:{cause:BaseEquipment}
most everything is null


********** this is huge **********
looking at a 21 nissan, old license vs test license
the style object structure is different
new license:
	has 2 new values|: altModelName,altStyleName
	all attributes are at a single level under style
old license:
	many attributes are under style[{attributes{id,modelYear...}}]
dont know if this is another case of kinking test data or is a genuine change of structure
do a 7b request of the vin
and it comes out structured the "new" way