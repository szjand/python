﻿select a.vin, a.style_count, b.ground_date, c.*
from chr.describe_vehicle a
left join nc.vehicle_acquisitions b on a.vin = b.vin
left join nc.open_orders c on a.vin = c.vin
where a.style_count  > 1
order by b.ground_date desc 