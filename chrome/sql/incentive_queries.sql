﻿drop table if exists chr.incentives cascade;
create table chr.incentives (
  the_date date,
  vin citext,
  response jsonb,
  primary key (the_date,vin));
alter table chr.incentives
rename to incentives_by_vin;  

select * from chr.incentives_by_vin where vin = '1G1JD5SB6K4103819'


select *
from gmgl.vin_incentive_cash
where thru_ts > now()
limit 100


select distinct c.chrome_style_id
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
join nc.vehicle_configurations c on b.configuration_id = c.configuration_id
where a.thru_date > current_date
  and stock_number not like 'H%'

drop table if exists chr.incentives_by_style_id cascade;  
create table chr.incentives_by_style_id (
  the_date date,
  style_id citext,
  response jsonb,
  primary key (the_date,style_id));

select distinct c.chrome_style_id
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
join nc.vehicle_configurations c on b.configuration_id = c.configuration_id
where a.thru_date > current_date
  and stock_number not like 'H%'

select count(*)
from chr.incentives_by_style_id  

select * from jon.configurations limit 10

select c.chrome_style_id, b.make, b.model, b.model_code, d.peg, array_agg(a.stock_number)
from nc.vehicle_acquisitions a
join nc.vehicles b on a.vin = b.vin
join nc.vehicle_configurations c on b.configuration_id = c.configuration_id
join jon.configurations d on c.chrome_style_id = d.chrome_style_id
where a.thru_date > current_date
  and stock_number not like 'H%'
group by c.chrome_Style_id, b.make, b.model, b.model_code, d.peg
order by b.make, b.model, b.model_code,d.peg


select * from jon.configurations where chrome_style_id = '406418'
union
select * from jon.configurations where chrome_style_id = '406427'

select *
from chr.incentives_by_style_id
where style_id = '406427'


select *
from chr.incentives_by_vin
where vin = '1FTEW1EP0LFA03286'