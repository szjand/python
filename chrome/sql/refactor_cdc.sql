﻿if describe_vehicle_by_style_id is going to be used for configurations, ??changes to configs ?? i believe they will be type 1

REMEMBER describe_vehicle_by_style_id is not any single vehicle, it is a template/class
  configurations will contain non variable attributes, one value for each chrome style id
  
major mind fuck on foreign keys on option tables
  cant be describe_vehicle_by_style_id.chrome_style_id, not unique
  could be styles.chrome_style_id
  but the option_code comes from describe_vehicle_by_style_id, but they are not unique even
    with chrome style id because of type 2 history
  
12-8 
all data updated thru 12/7
working now on implementing in luigi using jon schema
will probably need to make some functions for the xfm stuff (new rows, changed rows) 
done:
version
model_years
divisions
models
12-10
the goal today (greg gone, afton out, payroll done) run chrome_v2.py in luigi today complete
which means i have to get done
styles - done - -TODO figure out how i want to handle chrome style ids that are removed
describe_vehicle_by_style_id done
22 minutes to run chrome in luigi up to this point
configurations
exterior_colors
interiors
engines
transmissions
packages
created xfm functions for all of these
the only question now is do i do them all in on luigi class or separate
going with all in one class, order doesnt matter, they all need to pass,
the class will require DescribeVehicleByStyleID
these are all lightweight functions and it hurts nothing to rerun as many as need be

select count(*) from jon.engines; --3976
select count(*) from jon.exterior_colors; --20309, 20690
select count(*) from jon.interiors; -- 6378
select count(*) from jon.transmissions; -- 3036
select count(*) from jon.configurations; --2622
select count(*) from jon.packages; -- 9350


select * from jon.get_version_info
delete from jon.get_version_info where the_date = current_date;

select * from jon.describe_vehicles_by_style_id_changed_rows;

select a.chrome_Style_id, chrome_version, from_Date, thru_date, current_row
from jon.describe_vehicle_by_Style_id a
join (
select chrome_Style_id
from jon.describe_vehicle_by_style_id
group by chrome_Style_id
having count(*) > 2) b on a.chrome_Style_id = b.chrome_style_id
order by chrome_style_id, from_date

select a.chrome_Style_id, chrome_version, from_Date, thru_date, current_row
from jon.describe_vehicle_by_Style_id a
where chrome_style_id = '369820'

shit i fucked up the query in xfm_describe_vehicle_by_style_id for current version
maybe
-- yep, was using chr.get_version instead of jon, duh
    select vers -> 'attributes' ->> 'build' as chrome_version, vers -> 'attributes' ->> 'date'
    from chr.get_version_info
    join jsonb_array_elements(response->'data') as a(vers) on true
    where vers -> 'attributes' ->> 'country' = 'US'
    order by vers -> 'attributes' ->> 'date' desc
    limit 1
-- yep, this is what i was expecting
    select vers -> 'attributes' ->> 'build' as chrome_version, vers -> 'attributes' ->> 'date'
    from jon.get_version_info
    join jsonb_array_elements(response->'data') as a(vers) on true
    where vers -> 'attributes' ->> 'country' = 'US'
    order by vers -> 'attributes' ->> 'date' desc
    limit 1    

select a.chrome_Style_id, chrome_version, from_Date, thru_date, current_row
from jon.describe_vehicle_by_Style_id a
where from_date = current_date

fix it

update jon.describe_vehicle_by_Style_id
set chrome_version = '369820'
where from_date = current_date

-------------------------------------------------------------
--< styles 12-10
-------------------------------------------------------------

  
-------------------------------------------------------------
--/> styles 12-10
-------------------------------------------------------------

-------------------------------------------------------------
--< configurations 12-7/8-19
-------------------------------------------------------------


-------------------------------------------------------------
--< configurations intial data 12-7-19
-------------------------------------------------------------
ok, cool, colors work, assume other options will work the same way
configuration, the big one

to the configurations table need to add body type, forecast_model, transmission

transmission is causing me some grief (see .../chrome/sql/body_types_transmissions), body types look ok
select * from chr.configurations
i think we are going to want a hash for this, might as well store one

drop table if exists jon.configurations;
CREATE TABLE jon.configurations
(
  chrome_style_id citext primary key,
  model_year integer NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  alloc_group citext NOT NULL,
  cab citext NOT NULL,
  drive citext NOT NULL,
  model_code citext NOT NULL,
  box citext NOT NULL,
  trim_level citext,
  peg citext NOT NULL,
  style_name citext NOT NULL,
  subdivision citext NOT NULL,
  market_class citext NOT NULL,
  body_type citext not null,
  hash text not null);
COMMENT ON TABLE jon.configurations
  IS 'chrome configurations at the level of chrome_style_id, does
  not include engines, interiors, colors, etc. Successful responses from chr.get_describe_vehicle_by_style_id,
  non fleet, 2018 and above, honda, nissan, chevrolet, buick, gmc, cadillac only.
  Need to figure out what to do about null trim_level';
CREATE UNIQUE INDEX ON jon.configurations(model_year, model_code, peg);
CREATE UNIQUE INDEX ON jon.configurations(hash);

drop table if exists jon.ext_configurations cascade;
CREATE TABLE jon.ext_configurations
(
  chrome_style_id citext primary key,
  model_year integer NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  alloc_group citext NOT NULL,
  cab citext NOT NULL,
  drive citext NOT NULL,
  model_code citext NOT NULL,
  box citext NOT NULL,
  trim_level citext,
  peg citext NOT NULL,
  style_name citext NOT NULL,
  subdivision citext NOT NULL,
  market_class citext NOT NULL,
  body_type citext not null);
COMMENT ON TABLE jon.ext_configurations
  IS 'temporary staging table for configurations, simiplifies adding a hash to the configurations table';
CREATE UNIQUE INDEX on jon.ext_configurations(model_year, model_code, peg);


-- initial feed
insert into jon.ext_configurations
select aa.chrome_style_id,aa.model_year::integer, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
  aa.drive, aa.model_code, aa.box, aa.trim_level, coalesce(aa.peg, 'n/a') as peg, aa.style_name, 
  aa.subdivision, aa.marketclass, aa.body_type
from (
  select e.chrome_style_id,
    f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'attributes' ->> 'mfrModelCode' as model_code,
    f.style -> 'attributes' ->> 'trim' as trim_level,
   case f.style -> 'attributes' ->> 'drivetrain'
      when 'Front Wheel Drive' then 'FWD'
      when 'All Wheel Drive' then 'AWD'
      when 'Rear Wheel Drive' then 'RWD'
      when 'Four Wheel Drive' then '4WD'
      else 'XXXXXX'
    end::citext as drive,     
-- changed extended to double
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then 
          case
            when trim(
              left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1)) = 'Extended' then 'Double'
            else trim(left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
          end
      when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
        then trim(
          left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
      else 'n/a'
    end::citext as cab,
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then trim(
          replace(
            substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
              position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
      else 'n/a'
    end::citext as box,        
    h.pegs -> 'attributes' ->> 'oemCode' as peg,
    f.style -> 'attributes' ->> 'name' as style_name,
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'marketClass' ->> '$value' as marketClass,
    g.bt ->> '$value' as body_type
  from jon.describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
    and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
  left join jsonb_array_elements(f.style -> 'bodyType') with ordinality as g(bt) on true
    and g.bt -> 'attributes' ->> 'primary' = 'true'    
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false'
    and e.from_date <> current_date) aa  -- leave out the data generated today, to facilitate doing the update scripting  
left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
  and aa.model_code = bb.model_code

/*
uh oh the above give me 2577
this gives me 2621
select count(*) from jon.describe_vehicle_by_style_id where from_date <> current_Date
my guess is the fleet only
-- yep
select fleet, count(*)
from (
  select e.chrome_Style_id, e.from_date, f.style -> 'attributes' ->> 'fleetOnly' as fleet
  from jon.describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true) x
where from_Date <> current_date  
group by fleet  
*/

------------------------------------------------------------------------
--< allocation groups uh oh, got to fix this
------------------------------------------------------------------------
select *
from (
  select distinct model_year, make, model, model_code, subdivision, alloc_group
  from jon.ext_configurations
  where make in ('chevrolet','buick','gmc','cadillac')
    and alloc_Group = 'n/a'
    and subdivision not in ('Chevy Medium Duty')
  order by model, model_code) a
left join (
  select distinct model_year, make::citext, model_code, alloc_group
  from gmgl.vehicle_orders
  where model_year = 2020) b on a.make = b.make and a.model_code = b.model_code
order by a.make, a.model, a.model_code  


select distinct model_year, make, model, model_code, subdivision, alloc_group
from jon.configurations
where make in ('chevrolet','buick','gmc','cadillac')
  and alloc_Group = 'n/a'
  and subdivision not in ('Chevy Medium Duty')
order by model, model_code


select *
from jon.configurations
where model_year = 2020
  and model = 'ct5'

update jon.configurations
set alloc_group = 'CT5'
-- select * from jon.configurations
where model_year = 2020
  and model = 'CT5'

  
update jon.configurations
set alloc_group = 'CORVET'
-- select * from jon.configurations
where model_year = 2020
  and model = 'corvette'
  
insert into nc.allocation_groups(model_year,make,model,model_code,alloc_group) values
(2020,'Chevrolet','Trailblazer','1TQ56','TRLBZR'),
(2020,'Chevrolet','Trailblazer','1TR56','TRLBZR'),
(2020,'Chevrolet','Trailblazer','1TS56','TRLBZR'),
(2020,'Chevrolet','Trailblazer','1TT56','TRLBZR'),
(2020,'Chevrolet','Trailblazer','1TU56','TRLBZR'),
(2020,'Chevrolet','Trailblazer','1TV56','TRLBZR'),
(2020,'Chevrolet','Trailblazer','1TW56','TRLBZR'),
(2020,'Chevrolet','Trailblazer','1TX56','TRLBZR'),
(2020,'Chevrolet','Trailblazer','1TY56','TRLBZR');
update jon.configurations
set alloc_group = 'TRLBZR'
-- select * from jon.configurations
where model_year = 2021
  and model = 'trailblazer'

(2020,'Chevrolet','Express Passenger','CG33706','EXPVAN'),
(2020,'Chevrolet','Corvette','1YC07','VET'),
(2020,'Buick','Encore GX','4TV06','ENCRGX'),
(2020,'Chevrolet','Bolt EV','1FB48','BOLTEV'),
(2020,'Chevrolet','Bolt EV','1FC48','BOLTEV'),
(2020,'Chevrolet','BLazer','1NH26','BLAZER'),
(2020,'Chevrolet','BLazer','1NK26','BLAZER'),
(2020,'Chevrolet','BLazer','1NL26','BLAZER'),
(2020,'Chevrolet','BLazer','1NM26','BLAZER'),
(2020,'Chevrolet','BLazer','1NR26','BLAZER'),
(2020,'Chevrolet','BLazer','1NS26','BLAZER'),
(2020,'Chevrolet','BLazer','1NT26','BLAZER'),
(2020,'Buick','Encore GX','4TR06','ENCRGX'),
(2020,'Buick','Encore GX','4TS06','ENCRGX'),
(2020,'Buick','Encore GX','4TT06','ENCRGX'),
(2020,'Buick','Encore GX','4V06','ENCRGX'),
(2020,'Buick','Encore GX','4TY06','ENCRGX'),
(2020,'Buick','Encore GX','4TZ06','ENCRGX'),
(2020,'Cadillac','CT4','6DB69','CT4'),
(2020,'Cadillac','CT4','6DC69','CT4'),
(2020,'Cadillac','CT4','6DD69','CT4'),
(2020,'Cadillac','CT4','6DE69','CT4'),
(2020,'Cadillac','CT5','6DB79','CT5'),
(2020,'Cadillac','CT5','6DC79','CT5'),
(2020,'Cadillac','CT5','6DD79','CT5'),
(2020,'Chevrolet','Express Cargo Van','CG23405','EXPVAN'),
(2020,'Chevrolet','Express Cargo Van','CG23705','EXPVAN'),
(2020,'Chevrolet','Express Cargo Van','CG33405','EXPVAN'),
(2020,'Chevrolet','Express Cargo Van','CG33705','EXPVAN'),
(2020,'Chevrolet','Express Commercial Cutaway','CG33503','EXPVAN'),
(2020,'Chevrolet','Express Commercial Cutaway','CG33803','EXPVAN'),
(2020,'Chevrolet','Express Commercial Cutaway','CG33903','EXPVAN'),
(2020,'Chevrolet','Express Passenger','CG23406','EXPVAN'),
(2020,'Chevrolet','Express Passenger','CG33406','EXPVAN'),
(2020,'Chevrolet','Express Passenger','CG23706','EXPVAN'),
(2020,'Chevrolet','Express Passenger','CG33706','EXPVAN'),
(2020,'GMC','Savana Cargo Van','TG23405','SAVVAN'),
(2020,'GMC','Savana Cargo Van','TG23705','SAVVAN'),
(2020,'GMC','Savana Cargo Van','TG33405','SAVVAN'),
(2020,'GMC','Savana Cargo Van','TG33705','SAVVAN'),
(2020,'GMC','Savana Commercial Cutaway','TG33503','SAVVAN'),
(2020,'GMC','Savana Commercial Cutaway','TG33803','SAVVAN'),
(2020,'GMC','Savana Commercial Cutaway','TG33903','SAVVAN'),
(2020,'GMC','Savana Passenger','TG23406','SAVVAN'),
(2020,'GMC','Savana Passenger','TG33406','SAVVAN'),
(2020,'GMC','Savana Passenger','TG33706','SAVVAN');

------------------------------------------------------------------------
--/> allocation groups uh oh, got to fix this
------------------------------------------------------------------------


-- initial install into the config table (after fixing nc.allocation_groups)
insert into jon.configurations(chrome_style_id,model_year,make,model,
  alloc_group,cab,drive,model_code,box,trim_level,peg,style_name,
  subdivision,market_class,body_type,hash)
select a.*,
  (
    select md5(z::text) as hash
    from (
      select chrome_style_id,model_year,make,model,alloc_group,cab,drive,model_code,
        box,trim_level,peg,style_name,subdivision,market_class,body_type
      from jon.ext_configurations
      where chrome_style_id = a.chrome_style_id) z)
from jon.ext_configurations a
where not exists (
  select 1
  from jon.configurations
  where chrome_style_id = a.chrome_style_id)

-------------------------------------------------------------
--/> configurations intial data 12-7-19
-------------------------------------------------------------

-------------------------------------------------------------
--< configurations new and changed rows 12-8-19
-------------------------------------------------------------

12/8 i am a little confused
jon.describe_vehicle_by_style_id has already been updated

-- 12/3: 2621
-- 12/7: 2566
select from_Date, count(*) 
from jon.describe_vehicle_by_style_id 
group by from_Date

-- false: 2520
-- true: 2667
select current_row, count(*) 
from jon.describe_vehicle_by_style_id 
group by current_row

-- 369298: 2621
-- 369446: 2566
select chrome_version, count(*) 
from jon.describe_vehicle_by_style_id 
group by chrome_version


think i need to sketch out the process
before i do anything with configurations, all the processing of jon.describe_vehicle_by_style_id is done
new rows have been added
changed rows have been closed and replaced with new version
jon.get_describe_vehicle_by_style_id  is just a scrape to be processed into describe_vehicle_by_style_id

so, how do i process configurations
-- new rows
-- first put the current data in ext_configurations
truncate jon.ext_configurations;
insert into jon.ext_configurations
select aa.chrome_style_id,aa.model_year::integer, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
  aa.drive, aa.model_code, aa.box, aa.trim_level, coalesce(aa.peg, 'n/a') as peg, aa.style_name, 
  aa.subdivision, aa.marketclass, aa.body_type
from (
  select e.chrome_style_id,
    f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'attributes' ->> 'mfrModelCode' as model_code,
    f.style -> 'attributes' ->> 'trim' as trim_level,
   case f.style -> 'attributes' ->> 'drivetrain'
      when 'Front Wheel Drive' then 'FWD'
      when 'All Wheel Drive' then 'AWD'
      when 'Rear Wheel Drive' then 'RWD'
      when 'Four Wheel Drive' then '4WD'
      else 'XXXXXX'
    end::citext as drive,     
-- changed extended to double
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then 
          case
            when trim(
              left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1)) = 'Extended' then 'Double'
            else trim(left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
          end
      when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
        then trim(
          left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
      else 'n/a'
    end::citext as cab,
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then trim(
          replace(
            substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
              position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
      else 'n/a'
    end::citext as box,        
    h.pegs -> 'attributes' ->> 'oemCode' as peg,
    f.style -> 'attributes' ->> 'name' as style_name,
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'marketClass' ->> '$value' as marketClass,
    g.bt ->> '$value' as body_type
  from jon.describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
    and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
  left join jsonb_array_elements(f.style -> 'bodyType') with ordinality as g(bt) on true
    and g.bt -> 'attributes' ->> 'primary' = 'true'    
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false'
    and e.current_row) aa  -- leave out the data generated today, to facilitate doing the update scripting  
left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
  and aa.model_code = bb.model_code

-- insert new rows into jon.configurations
insert into jon.configurations(chrome_style_id,model_year,make,model,
  alloc_group,cab,drive,model_code,box,trim_level,peg,style_name,
  subdivision,market_class,body_type,hash)
select a.*,
  (
    select md5(z::text) as hash
    from (
      select chrome_style_id,model_year,make,model,alloc_group,cab,drive,model_code,
        box,trim_level,peg,style_name,subdivision,market_class,body_type
      from jon.ext_configurations
      where chrome_style_id = a.chrome_style_id) z)
from jon.ext_configurations a
where not exists (
  select 1
  from jon.configurations
  where chrome_style_id = a.chrome_style_id);


-- changed rows  

/*
drop table if exists changed_rows;
create temp table changed_rows as
select b.chrome_style_id
from (
  select a.*,
    (
      select md5(z::text) as hash
      from (
        select chrome_style_id,model_year,make,model,alloc_group,cab,drive,model_code,
          box,trim_level,peg,style_name,subdivision,market_class,body_type
        from jon.ext_configurations
        where chrome_style_id = a.chrome_style_id) z)
  from jon.ext_configurations a) b
join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  and b.hash <> c.hash;

-- compare the changed rows with existing rows
-- almost all changes are non essential stuff: style_name & market_class, attributes that are not currently used
-- one nissan with a changed model code
select 'new' as source, b.*
from (
  select a.*,
    (
      select md5(z::text) as hash
      from (
        select chrome_style_id,model_year,make,model,alloc_group,cab,drive,model_code,
          box,trim_level,peg,style_name,subdivision,market_class,body_type
        from jon.ext_configurations
        where chrome_style_id = a.chrome_style_id) z)
  from jon.ext_configurations a) b
join jon.configurations c on b.chrome_style_id = c.chrome_style_id
  and b.hash <> c.hash
union
select 'old', a.*
from jon.configurations a
join changed_rows b on a.chrome_style_id = b.chrome_style_id  
order by chrome_style_id, source  
*/

-- type 1 simply update attributes in existing rows
update jon.configurations x
set model_year = y.model_year,
    make = y.make,
    model = y.model,
    alloc_group = y.alloc_group,
    cab = y.cab,
    drive = y.drive,
    model_code = y.model_code, 
    box = y.box,
    trim_level = y.trim_level,
    peg = y.peg,
    style_name = y.style_name,
    subdivision = y.subdivision,
    market_class = y.market_class,
    body_type = y.body_type,
    hash = y.hash
from (    
  select b.*
  from (
    select a.*,
      (
        select md5(z::text) as hash
        from (
          select chrome_style_id,model_year,make,model,alloc_group,cab,drive,model_code,
            box,trim_level,peg,style_name,subdivision,market_class,body_type
          from jon.ext_configurations
          where chrome_style_id = a.chrome_style_id) z)
    from jon.ext_configurations a) b
  join jon.configurations c on b.chrome_style_id = c.chrome_style_id
    and b.hash <> c.hash) y
where x.chrome_style_id = y.chrome_style_id;    

  
-------------------------------------------------------------
--/> configurations new and changed rows 12-8-19
-------------------------------------------------------------
  
-------------------------------------------------------------
--/> configurations 12-7/8-19
-------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
--< packages 12-8-19 
------------------------------------------------------------------------------------------------------------
drop table if exists jon.packages cascade;
create table jon.packages (
  chrome_style_id citext not null references jon.styles(chrome_style_id),
  option_code citext not null,
  description citext not null,
  primary key(chrome_style_id,option_code));

-- new rows
insert into jon.packages(chrome_style_id,option_code,description) 
select aa.chrome_style_id, option_code, description
from (
  select a.chrome_style_id, c.packages -> 'attributes' ->> 'oemCode' as option_code,
    c.packages -> 'description'  ->> 0 
  from jon.describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'factoryOption') as c(packages) on true  
  left join jsonb_array_elements(a.response->'style') as f(style) on true 
    and (
      c.packages -> 'header' ->> '$value' = 'ADDITIONAL EQUIPMENT'  
      or
      c.packages -> 'header' ->> '$value' = 'REQUIRED OPTION')
    and (
      c.packages -> 'description'  ->> 0 like '%PACKAGE%'
      or 
      c.packages -> 'description'  ->> 0 like '%EDITION%')
    and c.packages -> 'description'  ->> 0 not like '%SIRIUSXM%'
    and c.packages -> 'description'  ->> 0 not like '%STORAGE PACKAGE%'
    and c.packages -> 'description'  ->> 0 not like 'LPO%'
  where c.packages -> 'attributes' ->> 'oemCode' is NOT null 
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false'
    and a.current_row) aa
where not exists (
  select 1
  from jon.packages
  where chrome_style_id = aa.chrome_style_id
    and option_code = aa.option_code);    

-- changed rows type 1
update jon.packages x
set description = y.description
from (
  select a.chrome_style_id, c.packages -> 'attributes' ->> 'oemCode' as option_code,
    c.packages -> 'description'  ->> 0 as description
  from jon.describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'factoryOption') as c(packages) on true  
  left join jsonb_array_elements(a.response->'style') as f(style) on true 
    and (
      c.packages -> 'header' ->> '$value' = 'ADDITIONAL EQUIPMENT'  
      or
      c.packages -> 'header' ->> '$value' = 'REQUIRED OPTION')
    and (
      c.packages -> 'description'  ->> 0 like '%PACKAGE%'
      or 
      c.packages -> 'description'  ->> 0 like '%EDITION%')
    and c.packages -> 'description'  ->> 0 not like '%SIRIUSXM%'
    and c.packages -> 'description'  ->> 0 not like '%STORAGE PACKAGE%'
    and c.packages -> 'description'  ->> 0 not like 'LPO%'
  where c.packages -> 'attributes' ->> 'oemCode' is NOT null 
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false'
    and a.current_row) y
where x.chrome_style_id = y.chrome_style_id
  and x.option_code = y.option_code
  and x.description <> y.description;    
  
------------------------------------------------------------------------------------------------------------
--< packages 12-8-19
------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
--< jon.interiors 12-8-19
------------------------------------------------------------------------------------------------------------

drop table if exists jon.interiors cascade;
create table jon.interiors (
  chrome_style_id citext not null references jon.styles(chrome_style_id),
  option_code citext not null,
  color citext not null,
  material citext not null,
  primary key (chrome_style_id,option_code));
comment on table jon.engines is 'data derived from jon.describe_vehicle_by_style_id, 
  material and color derived from parsing the descripion in factoryOptions. at the
  time of creation only 2 chrome style ids with null values, guessed at these.';  

-- new rows
insert into jon.interiors(chrome_style_id,option_code,color,material)
select aa.chrome_style_id, aa.option_code, aa.color, aa.material
from (
  select a.chrome_style_id, 
    case
      when chrome_style_id in ('400631','400629') then 'G'
      else b.interiors -> 'attributes' ->> 'oemCode'
    end as option_code, 
    case
      when chrome_style_id in ('400631','400629') then 'Black'
      else left(b.interiors -> 'description' ->> 0, position(',' in b.interiors -> 'description' ->> 0) -1) 
    end as color,
    case 
      when b.interiors -> 'description' ->> 0 like '%LEATHER%' then 'Leather'
      when b.interiors -> 'description' ->> 0 like '%LTHR%' then 'Leather'
      when b.interiors -> 'description' ->> 0 like '%ROCK CREEK%' then 'Leather'
      when a.chrome_style_id in ('400631','400629') then 'Cloth'
      else 'Cloth'
    end as material  
  from jon.describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'style') as aa(style) on true
    and aa.style ->'attributes'->>'fleetOnly' = 'false'
    and (aa.style ->'attributes'->>'modelYear')::integer > 2017
  left join jsonb_array_elements(a.response -> 'factoryOption') as b(interiors) on true
    and b.interiors -> 'header' ->> '$value' = 'SEAT TRIM'
  where a.current_row) aa
where aa.option_code is not null -- couple of honda passport with null option code(411995, 411998)
  and not exists (
    select 1
    from jon.interiors
    where chrome_style_id = aa.chrome_style_id
      and option_code = aa.option_code);  

-- changed rows type 1 
update jon.interiors x
set color = y.color, 
    material = y.material
from (  
  select aa.chrome_style_id, aa.option_code, aa.color, aa.material
  from (
    select a.chrome_style_id, 
      case
        when chrome_style_id in ('400631','400629') then 'G'
        else b.interiors -> 'attributes' ->> 'oemCode'
      end as option_code, 
      case
        when chrome_style_id in ('400631','400629') then 'Black'
        else left(b.interiors -> 'description' ->> 0, position(',' in b.interiors -> 'description' ->> 0) -1) 
      end as color,
      case 
        when b.interiors -> 'description' ->> 0 like '%LEATHER%' then 'Leather'
        when b.interiors -> 'description' ->> 0 like '%LTHR%' then 'Leather'
        when b.interiors -> 'description' ->> 0 like '%ROCK CREEK%' then 'Leather'
        when a.chrome_style_id in ('400631','400629') then 'Cloth'
        else 'Cloth'
      end as material  
    from jon.describe_vehicle_by_style_id a
    join jsonb_array_elements(a.response->'style') as aa(style) on true
      and aa.style ->'attributes'->>'fleetOnly' = 'false'
      and (aa.style ->'attributes'->>'modelYear')::integer > 2017
    left join jsonb_array_elements(a.response -> 'factoryOption') as b(interiors) on true
      and b.interiors -> 'header' ->> '$value' = 'SEAT TRIM'
    where a.current_row) aa) y
where x.chrome_style_id = y.chrome_style_id
  and x.option_code = y.option_code
  and (x.color <> y.color or x.material <>  y.material);    

    
------------------------------------------------------------------------------------------------------------
--/> jon.interiors 12-8-19
------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------
--< transmissions 12-8-19: GM only, from .../ext_chrome/scripts/body_types_transmissions.sql
------------------------------------------------------------------------------------------------------------
drop table if exists jon.transmissions;
create table jon.transmissions (
  chrome_style_id citext not null references jon.styles(chrome_style_id),
  option_code citext not null,
  description citext not null,
  primary key (chrome_style_id, option_code));


-- new rows
insert into jon.transmissions(chrome_style_id,option_code,description) 
select aa.chrome_style_id, aa.option_code, aa.description
from (
  select a.chrome_style_id, 
    f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
    replace(f.factory_option -> 'description'  ->> 0, 'TRANSMISSION, ', '') as description
  from jon.describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'style') as r(style) on true
    and r.style ->'attributes'->>'fleetOnly' = 'false'
    and (r.style ->'attributes'->>'modelYear')::integer > 2017
  join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true  
    and f.factory_option -> 'header' ->> '$value' = 'TRANSMISSION'
  where a.current_row) aa
where not exists (
  select 1 
  from jon.transmissions
  where chrome_style_id = aa.chrome_style_id
    and option_code = aa.option_code);
    
-- changed rows
-- type 1, if the description has changed, overwrite it
update jon.transmissions x
set description = y.description
from (
  select aa.chrome_style_id, aa.option_code, aa.description
  from (
    select a.chrome_style_id, 
      f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
      replace(f.factory_option -> 'description'  ->> 0, 'TRANSMISSION, ', '') as description
    from jon.describe_vehicle_by_style_id a
    join jsonb_array_elements(a.response->'style') as r(style) on true
      and r.style ->'attributes'->>'fleetOnly' = 'false'
      and (r.style ->'attributes'->>'modelYear')::integer > 2017
    join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true  
      and f.factory_option -> 'header' ->> '$value' = 'TRANSMISSION'
    where a.current_row) aa) y  
WHERE x.chrome_style_id = y.chrome_style_id
  and x.option_code = y.option_code
  and x.description <> y.description;        
------------------------------------------------------------------------------------------------------------
--/> transmissions 12-8-19: GM only, from .../ext_chrome/scripts/body_types_transmissions.sql
------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------
--< engines 12-8-19
-------------------------------------------------------------
drop table if exists jon.engines cascade;
create table jon.engines (
  chrome_style_id citext not null references jon.styles(chrome_style_id),
  option_code citext not null,
  description citext not null,
  displacement citext not null,
  diesel boolean not null,
  primary key (chrome_style_id,option_code));
comment on table jon.engines is 'option_code only available on gm engines, for honda nissan, 
    engines are unique per chrome style id. data derived from jon.describe_vehicle_by_style_id';

drop table if exists jon.ext_engines cascade;
create table jon.ext_engines (
  chrome_style_id citext not null references jon.styles(chrome_style_id),
  option_code citext not null,
  description citext not null,
  displacement citext not null,
  diesel boolean not null,
  primary key (chrome_style_id,option_code));
comment on table jon.ext_engines is 'option_code only available on gm engines, for honda nissan, 
    engines are unique per chrome style id. data derived from jon.describe_vehicle_by_style_id 
    where current_row. simplifies  changed rows';

-- ext_engines
insert into jon.ext_engines
select aa.*
from (
  select a.chrome_style_id, 
    f.factory_option -> 'attributes' ->> 'oemCode' as option_code,
    replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '') as description,
    case
      when position('L' in replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '')) = 0
        then replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', '')
      else replace(
        substring(
          replace(
            replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''), 'DIESEL',''),
        position('L' in replace(
          replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''), 'DIESEL','')) -3, 4), 'L', '')
    end as displacement,
    case
      when position('DIESEL' in upper(replace(f.factory_option -> 'description' ->> 0, 'ENGINE, ', ''))) <> 0 then true
      else false
    end as diesel     
  from jon.describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'style') as r(style) on true
    and r.style ->'attributes'->>'fleetOnly' = 'false'
    and (r.style ->'attributes'->>'modelYear')::integer > 2017
    and r.style ->'division'->>'$value' in ('Buick','Chevrolet','GMC','Cadillac')
  join jsonb_array_elements(a.response->'factoryOption') as f(factory_option) on true  
    and f.factory_option -> 'header' ->> '$value' = 'ENGINE'
  where a.current_row  
  union -- honda/nissan: no option_code
  select a.chrome_Style_id, 'n/a',
    replace(j.eng_descr ->> 'description', 'Engine: ', ''),
    -- no displacement on electric
    coalesce(i.hn_engines -> 'displacement' -> 'value' -> 0 ->> '$value', 'n/a') as displacement,
      case
      when position('DIESEL' in upper(replace(j.eng_descr ->> 'description', 'Engine: ', ''))) <> 0 then true
      else false
    end as diesel 
  from jon.describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'style') as r(style) on true
    and r.style ->'attributes'->>'fleetOnly' = 'false'
    and r.style ->'attributes'->>'modelYear' in ('2018','2019','2020')
    and r.style ->'division'->>'$value' in ('Honda','Nissan')
  left join jsonb_array_elements(a.response -> 'engine') as i(hn_engines) on true  
  left join jsonb_array_elements(a.response -> 'standard') as j(eng_descr) on true
    and j.eng_descr ->> 'description' like '%Engine:%'
  where a.current_row) aa;   


   
-- new rows
insert into jon.engines
select a.*
from jon.ext_engines a
where not exists (
  select 1
  from jon.engines
  where chrome_style_id = a.chrome_style_id
    and option_code = a.option_code);

-- changed rows
update jon.engines x
set description = y.description,
    displacement = y.displacement, 
    diesel = y.diesel
from (    
  select a.*
  from jon.ext_engines a
  join jon.engines b on a.chrome_style_id = b.chrome_style_id
    and a.option_code = b.option_code
    and (a.description <> b.description or a.displacement <> b.displacement or a.diesel <> b.diesel)) y
where x.chrome_style_id = y.chrome_style_id
  and x.option_code = y.option_code;

  
-------------------------------------------------------------
--/> engines 12-8-19
-------------------------------------------------------------

-------------------------------------------------------------
--< exterior colors 12-7-19
-------------------------------------------------------------
first thing
  no need for the extra columns, like all the other option tables
  should consist of chrome_style_id, option_code, description 
  i believe i dont need any more history than what exists in describe_vehicle_by_style_id
-- 20676 rows
drop table if exists jon_colors;
create temp table jon_colors as
select a.chrome_style_id,
  (b.colors->'attributes'->>'colorCode')::citext as color_code,
  (b.colors->'attributes'->>'colorName')::citext as color,
  (r.style -> 'attributes' ->> 'modelYear')::integer as model_year, 
  r.style -> 'attributes' ->> 'mfrModelCode' as model_code  
from jon.describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'exteriorColor') as b(colors) on true
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and (r.style ->'attributes'->>'modelYear')::integer > 2017
where a.from_date <> current_date

select count(*) from chr.exterior_colors -- 20600   

select count(*)
from (
  select chrome_style_id, color_code, color
  from jon_colors
  group by chrome_style_id, color_code, color) a

select *
from jon_colors a
where not exists (
  select 1
  from chr.exterior_colors
  where chrome_Style_id = a.chrome_style_id)

select count(distinct chrome_style_id) from jon_colors


select chrome_style_id, color_code
from chr.exterior_colors
group by chrome_style_id, color_code
having count(*) > 1

drop table if exists jon.exterior_colors;
create table jon.exterior_colors (
  chrome_style_id citext not null references jon.styles(chrome_style_id),
  color_code citext not null,
  color citext not null,
  primary key(chrome_style_id,color_code));
create index on jon.exterior_colors(chrome_style_id);  
create index on jon.exterior_colors(color_code);
COMMENT ON TABLE jon.exterior_colors
  IS 'based on chrome ADS service with the ShowAvailableEquipment switch which returns
all available equipment, including colors. table is updated weekly';

-- initial feed
insert into jon.exterior_colors
select a.chrome_style_id,
  b.colors->'attributes'->>'colorCode',
  b.colors->'attributes'->>'colorName'
from jon.describe_vehicle_by_style_id a
join jsonb_array_elements(a.response->'exteriorColor') as b(colors) on true
join jsonb_array_elements(a.response->'style') as r(style) on true
  and r.style ->'attributes'->>'fleetOnly' = 'false'
  and (r.style ->'attributes'->>'modelYear')::integer > 2017
where a.from_date <> current_date; -- leave out the data generated today, to facilitate doing the update scripting

-- new rows
insert into jon.exterior_ccolors
select aa.chrome_style_id, aa.color_code, aa.color
from (
  select a.chrome_style_id,
    (b.colors->'attributes'->>'colorCode')::citext as color_code,
    (b.colors->'attributes'->>'colorName')::citext as color
  from jon.describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'exteriorColor') as b(colors) on true
  join jsonb_array_elements(a.response->'style') as r(style) on true
    and r.style ->'attributes'->>'fleetOnly' = 'false'
    and (r.style ->'attributes'->>'modelYear')::integer > 2017
  where a.current_row) aa
where not exists (
  select 1
  from jon.exterior_colors
  where chrome_style_id = aa.chrome_style_id
    and color_code = aa.color_code); 
     

-- changed rows
/*
-- wow, i am suprised to see 51 changed rows, all are 2020 models
-- verified on mfr websites, the new colors are correct
-- my guess is this is another case of my data has not been updated since first created
select aa.*, bb.color, cc.model_year, cc.make, cc.model, cc.model_code, peg
from (
  select a.chrome_style_id,
    (b.colors->'attributes'->>'colorCode')::citext as color_code,
    (b.colors->'attributes'->>'colorName')::citext as color
  from jon.describe_vehicle_by_style_id a
  join jsonb_array_elements(a.response->'exteriorColor') as b(colors) on true
  join jsonb_array_elements(a.response->'style') as r(style) on true
    and r.style ->'attributes'->>'fleetOnly' = 'false'
    and (r.style ->'attributes'->>'modelYear')::integer > 2017
  where a.current_row) aa  
join jon.exterior_colors bb on aa.chrome_style_id = bb.chrome_style_id
  and aa.color_code = bb.color_code
  and aa.color <> bb.color  
left join chr.configurations cc on aa.chrome_style_id = cc.chrome_style_id
order by cc.model_year, cc.make, cc.model
*/
-- type 1, if the color has changed, overwrite it
update jon.exterior_colors x
set color = y.color
from (
  select aa.chrome_style_id, aa.color_code, aa.color
  from (
    select a.chrome_style_id,
      (b.colors->'attributes'->>'colorCode')::citext as color_code,
      (b.colors->'attributes'->>'colorName')::citext as color
    from jon.describe_vehicle_by_style_id a
    join jsonb_array_elements(a.response->'exteriorColor') as b(colors) on true
    join jsonb_array_elements(a.response->'style') as r(style) on true
      and r.style ->'attributes'->>'fleetOnly' = 'false'
      and (r.style ->'attributes'->>'modelYear')::integer > 2017
    where a.current_row) aa  
  join jon.exterior_colors bb on aa.chrome_style_id = bb.chrome_style_id
    and aa.color_code = bb.color_code
    and aa.color <> bb.color) y  
WHERE x.chrome_style_id = y.chrome_style_id
  and x.color_code = y.color_code;    


-------------------------------------------------------------
--/> exterior colors 12-7-19
-------------------------------------------------------------



-------------------------------------------------------------
--< 12/6-7/19 
-------------------------------------------------------------

ok 
i think i have seen the light
describe_Vehicles_by_style_id is the source data for:
  configurations
  engines
  colors
  interiors
  transmissions
  packages

the source data does not get changed,
for each c_s_i with changed data, close the old row, add the new row, 
include from/thru dates & here is where version information can go 
date is an adequate granularity
version the raw data: just a c_s_i and the json response

so the processing path will be:
get_describe_vehicle_by_style_id -> describe_vehicle_by_style_id -> configs/engines/etc

start fresh 
assumptions: none of the data in tables leading up to describe_vehicle_by_style_id is relevant except for the
  final generation of the chrome_style_id

select * from jon.describe_vehicle_by_style_id limit 10

start with ddl (update on  chrome.sdr)

alter table jon.describe_vehicle_by_style_id
add column chrome_version citext, 
add column from_date date,
add column thru_date date default '12/31/9999';

update jon.describe_vehicle_by_style_id
set version = '369298', from_date = '12/03/2019' ;

alter table jon.describe_vehicle_by_style_id
add column current_row boolean,
add column hash text;

update jon.describe_vehicle_by_style_id
set current_row = true;

update jon.describe_vehicle_by_style_id aa
set hash = bb.hash
from (
  select chrome_style_id, md5(a::text) as hash
  from (
    select chrome_style_id, response
    from jon.describe_vehicle_by_style_id) a) bb
where aa.chrome_Style_id = bb.chrome_style_id

alter table jon.describe_Vehicle_by_style_id
alter column chrome_version set not null,
alter column from_date set not null,
alter column thru_Date set not null,
alter column current_row set not null,
alter column hash set not null;


select count(*) from jon.describe_vehicle_by_style_id




-- stop verion_info from running in luigi every night, just need to run it as part of entire chrome script
select *
from jon.get_version_info


select vers -> 'attributes' ->> 'build' as chrome_version 
from chr.get_version_info
join jsonb_array_elements(response->'data') as a(vers) on true
where vers -> 'attributes' ->> 'country' = 'US'
order by vers -> 'attributes' ->> 'date' desc
limit 1

  
picking up where i left off yesterday
basically, same pattern as on all the others

insert new rows
check for changed rows

the production table will be describe_vehicle_by_style_id
the process will scrape into get_describe_vehicle_by_style_id, then insert new rows into the production table
and check for changed rows in the scrape table against existing rows in the production table

so lets get started, with ddl


/*
** concern, is chrome going to be good enough for honda/nissan ?
12/7 deal with these concerns when i get to the configurations table

nc.vehicle_configurations: except for 3 2018 honda accord sedans(397456,397457,397454), chrome_style_id and engine are unique
select *
from chr.configurations
where chrome_style_id in ('397456','397457','397454')

select * 
from nc.vehicle_configurations
where chrome_style_id in ('397456','397457','397454')
*/

go ahead and run chrome_v2 including describe vehicle, including cleanup, but no new or changed row processing yet
/*
i am anticipating having to choose a subset of the entire describe_vehicle_by_Style_id dataset that is of
sufficient interest to maintain changes
we will see
 the pattern will be, keep describe_vehicle current, then from that, maintain all the child tables (config, eng, etc)
 */
 
CREATE OR REPLACE FUNCTION jon.xfm_describe_vehicle_by_style_id()
  RETURNS void AS
$BODY$
  -- new rows
--   insert into jon.describe_vehicle_by_style_id (chrome_style_id,response)
46 new rows, not inserted yet
-- new rows
insert into jon.describe_vehicle_by_style_id (chrome_style_id,response,chrome_version,from_date,current_row,hash)
select a.*, b.chrome_version, current_date, true,
  (
    select md5(z::text) as hash
    from (
      select chrome_style_id, response
      from jon.get_describe_vehicle_by_style_id
      where chrome_style_id = a.chrome_style_id) z) 
from jon.get_describe_vehicle_by_style_id a
join ( -- current chrome version
  select vers -> 'attributes' ->> 'build' as chrome_version 
  from chr.get_version_info
  join jsonb_array_elements(response->'data') as a(vers) on true
  where vers -> 'attributes' ->> 'country' = 'US'
  order by vers -> 'attributes' ->> 'date' desc
  limit 1) b on true
where not exists (
  select 1
  from jon.describe_vehicle_by_style_id
  where chrome_style_id = a.chrome_style_id);   

--  changed rows
drop table if exists jon.describe_vehicles_by_style_id_changed_rows;
create table jon.describe_vehicles_by_style_id_changed_rows (
  chrome_style_id citext primary key,
  response jsonb not null,
  chrome_version citext not null,
  hash text not null);

insert into jon.describe_vehicles_by_style_id_changed_rows(chrome_style_id,response,chrome_version,hash)
select c.chrome_style_id, d.response, e.chrome_version, d.hash
from jon.describe_vehicle_by_style_id c
join (
  select a.chrome_style_id, a.response, md5(a::text) as hash
  from jon.get_describe_vehicle_by_style_id a) d on c.chrome_style_id = d.chrome_style_id
    and c.hash <> d.hash
join ( -- current chrome version
  select vers -> 'attributes' ->> 'build' as chrome_version 
  from chr.get_version_info
  join jsonb_array_elements(response->'data') as a(vers) on true
  where vers -> 'attributes' ->> 'country' = 'US'
  order by vers -> 'attributes' ->> 'date' desc
  limit 1) e on true;

-- update current version of changed row 
update jon.describe_vehicle_by_style_id
set current_row = false,
    thru_date = current_date - 1
where current_row    
  and chrome_style_id in (
    select chrome_style_id   
    from jon.describe_vehicles_by_style_id_changed_rows);

    
-- add new row for each changed row
insert into jon.describe_vehicle_by_style_id (chrome_style_id,response,chrome_version,from_date,current_row,hash)
select chrome_style_id, response, chrome_version, current_date, true, hash
from jon.describe_vehicles_by_style_id_changed_rows;


    

/*
-- uh oh, chrome_style_id is no longer unique, multiple versions...make primary key chrome_style_id,current_row
-- that won't work either, multiple non current rows
-- ok, can only be one row with thru_date = 12/31/9999

drop table if exists wtf;
create temp table wtf as
select *
from jon.describe_vehicle_by_style_id;


DROP TABLE if exists jon.describe_vehicle_by_style_id;
CREATE TABLE jon.describe_vehicle_by_style_id
(
  chrome_style_id citext NOT NULL,
  response jsonb NOT NULL,
  chrome_version citext NOT NULL,
  from_date date NOT NULL,
  thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  current_row boolean NOT NULL,
  hash text NOT NULL,
  CONSTRAINT describe_vehicle_by_style_id_pkey PRIMARY KEY (chrome_style_id,thru_date));

insert into jon.describe_vehicle_by_style_id
select * from wtf;  


select count(*)
from jon.describe_vehicle_by_style_id 
where current_row
*/




  

  
-- fuck me, 2520 rows where hash <>
ok i am going to rationalize so many changes due to nothing having been updated since each row(csi) was first first inserted
we will see

select count(*) from jon.describe_vehicle_by_style_id


--which vehicles have changed data
select d.*, e.*
from jon.describe_vehicle_by_style_id c
join (
  select a.chrome_style_id, md5(a::text) as hash
  from jon.get_describe_vehicle_by_style_id a) d on c.chrome_style_id = d.chrome_style_id
    and c.hash <> d.hash
left join chr.configurations e on d.chrome_Style_id = e.chrome_style_id    

-- looked at the diff in this one, 2019 silverado 1500 LD
-- invoice diff, a few attributes added to factory option category (id 1381)
-- mostly stuff i really don't care about, 
-- BUT, need to update and keep current to catch those items tjat do matteh
select jsonb_pretty(response)
from jon.describe_vehicle_by_style_id
where chrome_style_id = '403472'

select jsonb_pretty(response)
from jon.get_describe_vehicle_by_style_id
where chrome_style_id = '403472'
-------------------------------------------------------------
--/> 12/6-7/19 
-------------------------------------------------------------
-------------------------------------------------------------
--< 12/5/19 
-------------------------------------------------------------
ran chrome.py
configurations.sql: 
  21 new rows, including 2021 chev trailblazer
  22 new engines
  9 new transmissions
  110 new colors
  33 new interiors
  35 new packages

ran chrome_v2.py

--< styles ------------------------------------------------------------------------
the question with styles is: will it be used for anything except the master list of chrome_style_ids?
select * from jon.styles
will the style_name be used for anything?


fails jon.xfm_styles: assert error changed style
same ones as last time
  select 'get_styles' as source, c.*
  from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb) order by chrome_style_id) c
  where chrome_style_id in ('406435','406436','406438','406439')       
  union
  select 'styles', a.*
  from jon.styles a
  where chrome_style_id in ('406435','406436','406438','406439')   
  order by chrome_style_id, source
  
only the style name changes  
go ahead and fix it, function jon.xfm_styles() has already inserted new rows, these are the only changed rows

update jon.styles a
set style_name = x.style_name
from (
  select *
  from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb) order by chrome_style_id) c
  where chrome_style_id in ('406433','406434','406435','406436','406438','406439','406437') ) x
where a.model_id = x.model_id  
  and a.chrome_style_id = x.chrome_style_id

-- fixed
select *
    from (
      select c.chrome_style_id, md5(c::text) as hash
      from (
        select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
        from jon.get_styles,
          jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb) order by chrome_style_id) c) d
    join (
      select chrome_style_id, md5(a::text) as hash
      from jon.styles a) e on d.chrome_style_id = e.chrome_style_id
        and d.hash <> e.hash



--< check for removed rows, are there any csi in styles that do not exist in get_styles  ---------------
ok, after giving it some thought, this is not a problem, just leave it in
just because they are no longer being produced, there may be some out there


select chrome_style_id
from jon.styles a
where not exists (
  select 1
  from (
      select (attributes #>> '{id}') as chrome_style_id, model_id, b."$value" as style_name
      from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) b
  where chrome_style_id = a.chrome_style_id)

oh shit, there is one
select * from jon.styles where chrome_style_id = '406149'

select *
from (
      select (attributes #>> '{id}') as chrome_style_id, model_id, b."$value" as style_name
      from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) a
where model_id = 32310      
      
its a 2020 malibu hybrid
select *
from chr.configurations
where chrome_style_id = '406149'

i dont fucking know what to do about this

there is no model code 1ZE69 in autobook for 2020 malibu either fleet or retail
aha from car & driver:
The hybrid model has been dropped from the Chevrolet Malibu lineup for 2020, as reported by Green Car Reports.

-- many chrome_style_ids per model
select model_id, count(*)
from jon.styles  
group by model_id
having count(*) > 1

select * 
from jon.styles
where model_id = 32780
--/> check for removed rows, are there any csi in styles that do not exist in get_styles  ---------------

--/> styles ------------------------------------------------------------------------


--< describe_vehicle ------------------------------------------------------------------------
now to get to the big one, describe_vehicle
this is going to be interesting and probably messy, time to review the stuff below
this would the the 3rd file

not sure if i should run describe vehicle by style id with ShowAvailableEquipment yet

basically, same pattern as on all the others

insert new rows
check for changed rows

the production table will be describe_vehicle_by_style_id
the process will scrape into get_describe_vehicle_by_style_id, then insert new rows into the production table
and check for changed rows in the scrape table against existing rows in the production table

select *
from jon.styles a
left join jon.get_describe_vehicle_by_style_id b on a.chrome_style_id = b.chrome_style_id
where b.chrome_style_id is null

-------------------------------------------------------------
--/> 12/5/19 
-------------------------------------------------------------  

11/26/19
calling it the refactor_cdc project
these are 3 query files from when i was getting started in october

current cdc is not working, these queries and .../chrome/chrome_v2.py are my attempt 
to clean it up
triggered by new colors not getting picked up 
and the belief that this data is going to be an integral part of building the ordering app

run both chrome.py and chrome_v2.py and see if i can sort out where i am in this process
chrome.py ~ 1 minute
run configurations.sql
chrome_v2 started at 8:55
failed with
    styles_cur.execute("select jon.xfm_styles()")
    psycopg2.errors.AssertFailure: changed style
but manually, function passes,
rerun styles section with out the xfm function    
and now the functions fails
this is the old thing about doing a commit on a connection when there are 
multiple cursor actions

added a pg.con_commit in styles, reran it
now it failed with the assert, but populated jon.get_styles, so i can see what the issues are

select 'get_styles' as source, c.*
from (
  select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
  from jon.get_styles,
    jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb) order by chrome_style_id) c
where chrome_style_id in ('406435','406436','406438','406439')       
union
select 'styles', a.*
from jon.styles a
where chrome_style_id in ('406435','406436','406438','406439')   
order by chrome_style_id, source

4 styles have the style_name changed, removing *Ltd Avail*

and, as i suspected, these are not new  models, so in chr.styles they did not get changed:
select * 
from chr.styles
where chrome_style_id in ('406435','406436','406438','406439')  


-- 1st file

-- Function: jon.xfm_models()

-- DROP FUNCTION jon.xfm_models();

CREATE OR REPLACE FUNCTION jon.xfm_models()
  RETURNS void AS
$BODY$

  -- new rows
  insert into jon.models(model_id,model_year,division_id,model)
  select * 
  from (
    select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
    from jon.get_models_by_division,
      jsonb_to_recordset(jon.get_models_by_division.models) as b("$value" citext, attributes jsonb)) c
  where not exists (
    select 1
    from jon.models
    where model_id = c.model_id);

  -- test for changed model
  do
  $$
  begin
  assert(
  select count(*)
  from (
    select c.model_id, md5(c::text) as hash
    from (
      select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
      from jon.get_models_by_division,
        jsonb_to_recordset(jon.get_models_by_division.models) as b("$value" citext, attributes jsonb)) c) d 
  join (
    select model_id, md5(a::text) as hash
    from jon.models a) e on d.model_id = e.model_id
      and d.hash <> e.hash) = 0, 'changed model';
  end
  $$;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION jon.xfm_models()
  OWNER TO rydell;


  -- ok, i think this is a one off, that CC is chassis cab and was included in the beginning
  -- for 2019 on, but my old way of processing did not pick it up because of the 
  -- on conflict do nothing
select 'models', a.*
from jon.models a
where model_id in (31262,31264,32213,32222)
union 
select 'get_models', a.*
from (
  select (attributes #>> '{id}')::integer as model_id, model_year, division_id, b."$value" as model
  from jon.get_models_by_division,
    jsonb_to_recordset(jon.get_models_by_division.models) as b("$value" citext, attributes jsonb)) a
where model_id in (31262,31264,32213,32222)    


select * from jon.get_models_by_division where models::text like '%31264%'

select * from jon.models where model_id = 31264

select * from chr.get_models_by_division where models::text like '%31264%'

select * from chr.get_models_by_division where models::text like '%CC%'

select * 
from chr.models
where model_id in (31262,31264,32213,32222)    

update jon.models
set model = model || ' CC'
where model_id in (31262,31264,32213,32222)    

select * from chr.models order by model


-- 2nd file

-- Function: jon.xfm_styles()

-- DROP FUNCTION jon.xfm_styles();

CREATE OR REPLACE FUNCTION jon.xfm_styles()
  RETURNS void AS
$BODY$
  -- new rows
  insert into jon.styles (chrome_style_id,model_id,style_name)
  select *
    from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb) order by chrome_style_id) c
  where not exists (
    select 1
    from jon.styles
    where chrome_style_id = c.chrome_style_id);   

  -- test for changed style
  do $$
  begin
  assert (
    select count(*)
    from (
      select c.chrome_style_id, md5(c::text) as hash
      from (
        select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
        from jon.get_styles,
          jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb) order by chrome_style_id) c) d
    join (
      select chrome_style_id, md5(a::text) as hash
      from jon.styles a) e on d.chrome_style_id = e.chrome_style_id
        and d.hash <> e.hash) = 0, 'changed style';
  end $$;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION jon.xfm_styles()
  OWNER TO rydell;

-- yikes 111 changes, of course non of which were previously detected
-- go ahead and fix them, not a big deal, not currently using style_name anywhere
create temp table changed_styles as
select d.chrome_style_id
from (
  select c.chrome_style_id, md5(c::text) as hash
  from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) c) d
join (
  select chrome_style_id, md5(a::text) as hash
  from jon.styles a) e on d.chrome_style_id = e.chrome_style_id
    and d.hash <> e.hash;

select * from changed_styles

select 'styles' as source, a.*
from jon.styles a
join changed_styles b on a.chrome_style_id = b.chrome_style_id
union
select 'get_styles', a.*
from (
  select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
  from jon.get_styles,
    jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) a
join changed_Styles b on a.chrome_style_id = b.chrome_style_id
order by chrome_style_id, model_id, source
    

select * 
from (
  select 'styles' as source, a.*
  from jon.styles a
  join changed_styles b on a.chrome_style_id = b.chrome_style_id
  union
  select 'get_styles', a.*
  from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) a
  join changed_Styles b on a.chrome_style_id = b.chrome_style_id
  order by chrome_style_id, model_id, source) c
left join chr.models d on c.model_id = d.model_id


-- just go ahead and update the whole enchilada
update chr.styles x
set style_name = y.style_name
from (
  select *
  from (
    select (attributes #>> '{id}') chrome_style_id, model_id, b."$value" as style_name
    from jon.get_styles,
      jsonb_to_recordset(jon.get_styles.styles) as b("$value" citext, attributes jsonb)) c) y
where x.chrome_style_id = y.chrome_style_id    

select * from jon.styles

-- 3rd file


/*
select chrome_style_id
-- select *
from jon.get_Describe_Vehicle_by_style_id
limit 10


create table jon.describe_vehicle_by_style_id (
  chrome_style_id citext primary key,
  response jsonb not null);

create table chr.describe_vehicle_by_style_id (
  chrome_style_id citext primary key,
  response jsonb not null);
  
insert into chr.describe_vehicle_by_style_id  
select *
from jon.get_Describe_Vehicle_by_style_id


select now()

select * from jon.styles
*/



CREATE OR REPLACE FUNCTION jon.xfm_describe_vehicle_by_style_id()
  RETURNS void AS
$BODY$
  -- new rows
  insert into jon.describe_vehicle_by_style_id (chrome_style_id,response)
  select *
  from jon.get_describe_vehicle_by_style_id a
  where not exists (
    select 1
    from jon.describe_vehicle_by_style_id
    where chrome_style_id = a.chrome_style_id);   

  -- test for changed style
--   do $$
--   begin
--   assert (
-- fuck me, 2485 rows where hash <>

select c.*, d.* 
from (
  select a.chrome_style_id, md5(a::text) as hash
  from jon.describe_vehicle_by_style_id a) c
join (
  select a.chrome_style_id, md5(a::text) as hash
  from jon.get_describe_vehicle_by_style_id a) d on c.chrome_style_id = d.chrome_style_id
    and c.hash <> d.hash
join chr.configurations e on c.chrome_Style_id = e.chrome_style_id    


select jsonb_pretty(response)
from jon.describe_vehicle_by_style_id
where chrome_style_id = '405296'

select jsonb_pretty(response)
from jon.get_describe_vehicle_by_style_id
where chrome_style_id = '405296'


-- well, at least this was down to 166, need to look at what the diffs are
-- exclude market_class, down to 122 rows
-- exlcude style_name, down to 87
-- exclude trim_level, down to 49|:  all but one are adding CC to 3500HD, one is nissan 370Z model_code change
drop table if exists changed_styles;
create temp table changed_styles as
select aa.chrome_style_id
--   select *
from (
  select chrome_style_id, md5(tmp_configurations::text) as hash
  from tmp_configurations) aa
join (
  select chrome_style_id, md5(a::text) as hash
  from tmp_jon_configurations a) bb on aa.chrome_style_id = bb.chrome_style_id 
    and aa.hash <> bb.hash;
    
select * from changed_Styles    

select 'chrome' as source, a.*
from tmp_configurations a
join changed_styles b on a.chrome_style_id = b.chrome_style_id
union
select 'changed', a.*
from tmp_jon_configurations a
join changed_styles b on a.chrome_style_id = b.chrome_style_id
order by chrome_style_id, source


/*
-- exclude market class
-- leave out style_name
-- exclude trim_level
drop table if exists tmp_configurations;
create temp table tmp_configurations as
select aa.chrome_style_id,aa.model_year::integer, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
  aa.drive, aa.model_code, aa.box, aa.trim_level, 
  coalesce(aa.peg, 'n/a') as peg, aa.style_name, 
  aa.subdivision-- , aa.marketclass
from (
  select e.chrome_style_id,
    f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'attributes' ->> 'mfrModelCode' as model_code,
    f.style -> 'attributes' ->> 'trim' as trim_level,
   case f.style -> 'attributes' ->> 'drivetrain'
      when 'Front Wheel Drive' then 'FWD'
      when 'All Wheel Drive' then 'AWD'
      when 'Rear Wheel Drive' then 'RWD'
      when 'Four Wheel Drive' then '4WD'
      else 'XXXXXX'
    end::citext as drive,     
-- changed extended to double
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then 
          case
            when trim(
              left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1)) = 'Extended' then 'Double'
            else trim(left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
          end
      when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
        then trim(
          left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
      else 'n/a'
    end::citext as cab,
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then trim(
          replace(
            substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
              position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
      else 'n/a'
    end::citext as box,        
    h.pegs -> 'attributes' ->> 'oemCode' as peg,
    f.style -> 'attributes' ->> 'name' as style_name,
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'marketClass' ->> '$value' as marketClass
  from chr.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
    and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false') aa
left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
  and aa.model_code = bb.model_code;


drop table if exists tmp_jon_configurations;
create temp table tmp_jon_configurations as
select aa.chrome_style_id,aa.model_year::integer, aa.make, aa.model, coalesce(bb.alloc_group, 'n/a') as alloc_group, aa.cab, 
  aa.drive, aa.model_code, aa.box, aa.trim_level, 
  coalesce(aa.peg, 'n/a') as peg, aa.style_name, 
  aa.subdivision-- , aa.marketclass
from (
  select e.chrome_style_id,
    f.style -> 'attributes' ->> 'modelYear' as model_year,
    f.style -> 'division'  ->> '$value' as make,
    f.style -> 'model'  ->> '$value' as model,
    f.style -> 'attributes' ->> 'mfrModelCode' as model_code,
    f.style -> 'attributes' ->> 'trim' as trim_level,
   case f.style -> 'attributes' ->> 'drivetrain'
      when 'Front Wheel Drive' then 'FWD'
      when 'All Wheel Drive' then 'AWD'
      when 'Rear Wheel Drive' then 'RWD'
      when 'Four Wheel Drive' then '4WD'
      else 'XXXXXX'
    end::citext as drive,     
-- changed extended to double
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then 
          case
            when trim(
              left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1)) = 'Extended' then 'Double'
            else trim(left(f.style -> 'attributes' ->> 'altBodyType', position('Cab' in f.style -> 'attributes' ->> 'altBodyType') -1))
          end
      when f.style -> 'attributes' ->> 'altBodyType' like '%Chassis%' 
        then trim(
          left(f.style -> 'attributes' ->> 'altBodyType', position(' ' in f.style -> 'attributes' ->> 'altBodyType') - 1))
      else 'n/a'
    end::citext as cab,
    case
      when f.style -> 'attributes' ->> 'altBodyType' like '%Bed%' 
        then trim(
          replace(
            substring(f.style -> 'attributes' ->> 'altBodyType', position('-' in f.style -> 'attributes' ->> 'altBodyType') + 2, 
              position('Bed' in f.style -> 'attributes' ->> 'altBodyType') -2), 'Bed', ''))
      else 'n/a'
    end::citext as box,        
    h.pegs -> 'attributes' ->> 'oemCode' as peg,
    f.style -> 'attributes' ->> 'name' as style_name,
    f.style -> 'subdivision' ->> '$value' as subdivision,
    f.style -> 'marketClass' ->> '$value' as marketClass
  from jon.get_describe_vehicle_by_style_id  e 
  left join jsonb_array_elements(e.response->'style') as f(style) on true 
  left join jsonb_array_elements(e.response->'factoryOption') as h(pegs) on true
    and h.pegs -> 'header' ->> '$value' in ('PREFERRED EQUIPMENT GROUP')
  where e.response -> 'responseStatus' -> 'attributes' ->>'responseCode' = 'Successful'
    and f.style -> 'attributes' ->> 'fleetOnly' = 'false') aa
left join nc.allocation_groups bb on aa.model_year::integer = bb.model_year
  and aa.model_code = bb.model_code;        
*/   