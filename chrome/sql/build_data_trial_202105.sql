﻿drop table if exists chr.build_data_trial cascade;
create table chr.build_data_trial (
	vin citext primary key,
	response jsonb,
	style_count integer);

select * from chr.build_data_trial	

select a.vin, a.style_count, ground_date
from chr.describe_vehicle a
join nc.vehicle_Acquisitions b on a.vin = b.vin
where style_count > 1
order by ground_date desc 

1GTU9EET7MZ240977

3GTU9DED2MG324160 orig run on 5/10 with 2 styles, today, 5/11 build data returns 1 style, but no mention of buiddata, but in 
	the vinDescription object, there is a filed source = Build

-- this query is from the chrome/sql/build_Data_queries file
select 'Factory Options' as category, f.factory_options->'header'->>'$value' as header,
  f.factory_options->>'oemCode' as option_code,
  f.factory_options->'installed'->>'cause',
  coalesce(o.oem_description->>'$value', replace(d.description::text, '"','')) as description
from chr.build_data_trial a
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
left join jsonb_array_elements(f.factory_options->'oem-description') as o(oem_description) on true
left join jsonb_array_elements(f.factory_options->'description') as d(description) on true
where a.vin = '3GTU9DED2MG324160'
  and f.factory_options->'installed'->>'cause' in ('OptionCodeBuild','Engine')
order by coalesce(o.oem_description->>'$value', replace(d.description::text, '"',''))
order by f.factory_options->>'oemCode' 

select * from gmgl.vehicle_invoices where vin = '3GTU9DED2MG324160'

so, looks like the deal is chrome lists the package, then lists the individual items in the
package with their individual oem codes



2021 F150  1FTFW1E51MFB49040

select 'Factory Options' as category, f.factory_options->'header'->>'$value' as header,
  f.factory_options->>'oemCode' as option_code,
  f.factory_options->'installed'->>'cause',
  coalesce(o.oem_description->>'$value', replace(d.description::text, '"','')) as description
from chr.build_data_trial a
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
left join jsonb_array_elements(f.factory_options->'oem-description') as o(oem_description) on true
left join jsonb_array_elements(f.factory_options->'description') as d(description) on true
where a.vin = '1FTFW1E51MFB49040'
  and f.factory_options->'installed'->>'cause' in ('OptionCodeEngineered')
order by coalesce(o.oem_description->>'$value', replace(d.description::text, '"',''))
order by f.factory_options->>'oemCode'


05/16/21 to the table add source, year, make, model, update style_count

shit, i was wrong about the engineered data, it actuallly returns 2 style_ids
                  select vin, count(*) as style_count
                  from (
                    select vin, jsonb_array_elements(response->'style')
                    from chr.build_data_trial
                    where style_count is null) a
                  group by vin

select * from chr.describe_vehicle where vin = 'JN1BJ1AW1MW426445'                  