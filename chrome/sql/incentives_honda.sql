﻿select *
from chr.incentives_by_vin
where vin = '5FNYF6H5XLB063181'

select i->>'programNumber' as program_number, i->>'programDescription' as program_description,
  i->'market'->>'description' as "Dealer/Retail", i->'category'->>'description' as category,
  i->'programValues'->'valueVariationList'->0->'programValueList'->0 as program_value,
  i->'programRuleList'->0->>'type' as rule_type, i->'programRuleList'->0->>'description' as rule_description,
  i->'groupAffiliation'->>'description' as group_affiliation,
  i->'previousOwnership'->>'description' as prev_ownership,
  i->'vehicleStatusList',
  i->'signatureID' as sig_id, i->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true
where vin = '5FNYF6H5XLB063181'
order by i->>'programNumber'


HP-X54:
    {
      "type": "Cash",
      "market": {
        "id": 2,
        "description": "Dealer"
      },
      "region": {
        "id": 128,
        "countryId": "US",
        "description": "Honda US National"
      },
      "source": "OEM",
      "taxRule": {
        "id": 131,
        "description": "Unknown"
      },
      "category": {
        "id": 130,
        "group": "Dealer Cash",
        "description": "Dealer Cash"
      },
      "expiryDate": "2020-06-01",
      "incentiveID": "18229501",
      "signatureID": 145,
      "endRecipient": {
        "id": 57,
        "description": "Dealer"
      },
      "effectiveDate": "2020-05-05",
      "programNumber": "HP-X54",
      "programValues": {
        "valueVariationList": [
          {
            "programValueList": [
              {
                "cash": 1750
              }
            ]
          }
        ]
      },
      "institutionList": [
        {
          "id": 109,
          "description": "OEM"
        }
      ],
      "programRuleList": [
        {
          "type": "Eligibility",
          "description": "Residents residing in qualifying regions of the United States."
        },
        {
          "type": "Qualification",
          "description": "Eligible new and unregistered vehicles."
        }
      ],
      "deliveryTypeList": [
        {
          "id": 43,
          "description": "Subvented Retail Finance"
        },
        {
          "id": 44,
          "description": "Subvented Lease"
        },
        {
          "id": 45,
          "description": "Non-subvented Finance"
        },
        {
          "id": 46,
          "description": "Non-subvented Lease"
        },
        {
          "id": 47,
          "description": "Cash Purchase"
        }
      ],
      "groupAffiliation": {
        "id": 0,
        "description": "No Specific Group Affiliation"
      },
      "previousOwnership": {
        "id": 0,
        "description": "No Previous Ownership Requirement"
      },
      "vehicleStatusList": [
        {
          "id": 48,
          "description": "New"
        }
      ],
      "programDescription": "Dealer/Lease/Finance Savings",
      "signatureHistoryID": 36648
    },