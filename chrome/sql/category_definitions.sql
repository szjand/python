﻿select 
  aa -> 'group' ->> '$value' as the_group, aa -> 'group' -> 'attributes' ->> 'id' as group_id,
  aa -> 'header' ->> '$value'as the_header, aa -> 'header' -> 'attributes' ->> 'id' as header_id,
  aa -> 'category' ->> '$value' as the_category, aa -> 'category' -> 'attributes' ->> 'id' as category_id,
  aa -> 'type' ->> '$value' as the_type, aa -> 'type' -> 'attributes' ->> 'id' as type_id
from chr.category_definitions a
left join jsonb_array_elements(a.response) as aa on true 
-- order by aa -> 'group' ->> '$value', aa -> 'type' ->> '$value', aa -> 'header' ->> '$value'  
order by aa -> 'category' -> 'attributes' ->> 'id'


select *
from jon.configurations a
left 
where model_year = 2020
limit 100