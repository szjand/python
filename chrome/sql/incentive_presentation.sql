﻿-- all the institutions for gm inventory
select distinct i->'institutionList'->0->>'description' as institutions
from chr.incentives_by_style_id a
join jsonb_array_elements(a.response->'incentives') as i(incentives) on true

-----------------------------------------------------
--< equinox
-----------------------------------------------------
-- cash, avail to all
select b->>'programNumber' as program_number, b->'programValues'->'valueVariationList'->0->'programValueList'->0->>'cash' as program_value
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  and b->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and b->'previousOwnership'->>'description' = 'No Previous Ownership Requirement'  
  and ((b->'category'->>'group' = 'Cash') or (b->'category'->>'group' = 'Dealer Cash'))
where vin = '3GNAXUEV5LS665508'

-- create stackable tables
drop table if exists incentives;
create temp table incentives as
select coalesce(b->>'programNumber', 'none') as program_number, b->>'signatureID' as sig_id, b->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
where vin = '3GNAXUEV5LS665508';

drop table if exists stackables;
create temp table stackables as
select c->>'signatureID' as sig_id, c->>'signatureHistoryID' as sig_his_id, c->>'relationType' as stackable
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'stackabilityList') as c(stackable) on true
where vin = '3GNAXUEV5LS665508';

-- checking against global connect
-- missing 20-36CC, 20-40GP
-- not in global: 20-06, 20-06QRD-004, 20-06Z-004, 20-09-004, 20-10A-003, 20-40GO-006, 20-40X-006
select a.program_number, c.program_number as stackable
from incentives a
join stackables b on a.sig_id = b.sig_id
  and stackable = 'S'
join incentives c on b.sig_his_id = c.sig_his_id  
where a.program_number = '20-40ACA-007'
order by c.program_number
-----------------------------------------------------
--/> equinox
-----------------------------------------------------

-----------------------------------------------------
--< xt6
-----------------------------------------------------
-- lists no majors, 20-40XA not in global connect
select b->>'programNumber' as program_number, b->'programValues'->'valueVariationList'->0->'programValueList'->0->>'cash' as program_value
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  and b->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and b->'previousOwnership'->>'description' = 'No Previous Ownership Requirement'  
  and ((b->'category'->>'group' = 'Cash') or (b->'category'->>'group' = 'Dealer Cash'))
where vin = '1GYKPDRS7LZ204188'
order by b->>'programNumber';

drop table if exists incentives;
create temp table incentives as
select coalesce(b->>'programNumber', 'none') as program_number, b->>'signatureID' as sig_id, b->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
where vin = '1GYKPDRS7LZ204188';

drop table if exists stackables;
create temp table stackables as
select c->>'signatureID' as sig_id, c->>'signatureHistoryID' as sig_his_id, c->>'relationType' as stackable
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'stackabilityList') as c(stackable) on true
where vin = '1GYKPDRS7LZ204188';


-- not in global: 20-06-005, 20-06QRD-004, 20-06Z-004, 20-09-004, 20-10A-003, 
--    20-40GO-006, 20-40X-006, 20-40XA-001, 20-40CU-001, 20-40XA-001
select a.program_number, c.program_number as stackable
from incentives a
join stackables b on a.sig_id = b.sig_id
  and stackable = 'S'
join incentives c on b.sig_his_id = c.sig_his_id  
where a.program_number = '20-40AL-003'
order by c.program_number
-----------------------------------------------------
--/> xt6
-----------------------------------------------------
-----------------------------------------------------
--< encore gx
-----------------------------------------------------

-- incentives available to all
-- shows 3
-- 20-40ACA-007 : major
-- 20-35BA-006 : stackable under 20-40GFK, category says cash, program values says tiers/termList
-- 20-40X-006 : nowhere on global incentive lookup page
select b->>'programNumber' as program_number, b->'programValues'->'valueVariationList'->0->'programValueList'->0->>'cash' as program_value
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
  and b->'groupAffiliation'->>'description' = 'No Specific Group Affiliation'
  and b->'previousOwnership'->>'description' = 'No Previous Ownership Requirement'  
  and ((b->'category'->>'group' = 'Cash') or (b->'category'->>'group' = 'Dealer Cash'))
where vin = 'KL4MMCSLXLB116386'
order by b->>'programNumber';

drop table if exists incentives;
create temp table incentives as
select coalesce(b->>'programNumber', 'none') as program_number, b->>'signatureID' as sig_id, b->>'signatureHistoryID' as sig_his_id
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'incentives') as b(incentives) on true
where vin = 'KL4MMCSLXLB116386';

drop table if exists stackables;
create temp table stackables as
select c->>'signatureID' as sig_id, c->>'signatureHistoryID' as sig_his_id, c->>'relationType' as stackable
from chr.incentives_by_vin a
join jsonb_array_elements(a.response->'stackabilityList') as c(stackable) on true
where vin = 'KL4MMCSLXLB116386';

-- not in global 20-06-005,20-06QRD-004,20-06Z-004,20-09-004,20-10A-003,20-40X-006, 20-40CAG-000
select a.program_number, c.program_number as stackable
from incentives a
join stackables b on a.sig_id = b.sig_id
  and stackable = 'S'
join incentives c on b.sig_his_id = c.sig_his_id  
where a.program_number = '20-40ACA-007'
order by c.program_number
-----------------------------------------------------
--/> encore gx
-----------------------------------------------------