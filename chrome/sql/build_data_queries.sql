﻿select vin, style_count
from chr.describe_vehicle
where style_count > 1
order by style_count desc 

select *
from chr.describe_vehicle
where style_count = 5

select *
from chr.describe_vehicle
where vin = '1GC4YUEY0LF102481'

select *
from nc.vehicles
where vin = '1GC4YUEY0LF102481'

select c.stock_number, c.ground_date, a.*, b.*
from jon.engines a
join nc.vehicles b on a.chrome_style_id = b.chrome_style_id
join nc.vehicle_acquisitions c on b.vin = c.vin
where diesel
  and model_year = 2020
  and c.ground_date > '01/01/2020'
order by c.stock_number  

select *
from jon.configurations
limit 10

select *
from jon.engines
where chrome_style_id = '406483'

select *  -- the diesel is the 3l
from nc.vehicle_configurations
where chrome_style_id = '406483'

select *
from nc.vehicles
where configuration_id = 743

3GCUYDETXLG211839

requires ShowExtendedDescriptions switch

-- trying to find a vin that still generates multiple styles
-- it seems that vins that initially generated multiple styles no longer do

select a.vin, a.style_count, b.ground_date
from chr.describe_vehicle a
join nc.vehicle_Acquisitions b on a.vin = b.vin
where a.style_count > 1
order by b.ground_date desc


select a.vin, (r.style ->'attributes'->>'id')::citext, a.style_count
from chr.describe_vehicle a
join jsonb_array_elements(a.response->'style') as r(style) on true
where a.vin = '1GTU9DEL7LZ278010'
order by a.style_count desc 

select * from chr.describe_vehicle where vin = '1GTU9DEL7LZ278010'

can not seem to find one
and whats weird is the structure of the response seems to have changed
nah,not the structure, just the order of the arrays


-- lets look at what makes used cars we see the most of

select * from arkona.xfm_inpmast limit 10

-- CHEVROLET;59462
-- HONDA;17954
-- FORD;17340
-- GMC;14136
-- PONTIAC;11086
-- NISSAN;9282
-- BUICK;8559
-- DODGE;7243
-- TOYOTA;5510
-- OLDSMOBILE;4698

select make, count(*)
from arkona.xfm_inpmast
where current_row
  and type_n_u = 'u'
group by make
order by count(*) desc  

select year, make, model, inpmast_vin
from arkona.ext_inpmast
where make = 'toyota'
  and length(inpmast_vin) = 17
order by year desc

select * from arkona.xfm_bopmast limit 10

-- -- sales, looks the same
-- CHEVROLET;13787
-- GMC;4265
-- HONDA;3219
-- FORD;2918
-- NISSAN;2003
-- BUICK;1818
-- PONTIAC;1529
-- DODGE;1432
-- CADILLAC;1184
-- TOYOTA;856

select b.make, count(*)
from arkona.xfm_bopmast a
join arkona.ext_inpmast b on a.bopmast_vin = b.inpmast_vin
where a.current_row
  and a.vehicle_type = 'U'
  and a.record_status = 'U'
group by b.make
order by count(*) desc  



------------------------------------------------------------------------------
select *
from nc.vehicles a
join nc.vehicle_configurations b on a.configuration_id =  b.configuration_id
join jon.configurations c on b.chrome_style_id = c.chrome_Style_id
where a.vin = '1GCUYGED7LZ177448'

select *
from jon.engines
where chrome_style_id = '406486'

select *
from jon.transmissions
where chrome_style_id = '406486'


select *
from gmgl.vehicle_option_codes a 
where a.vin = '1GCUYGED7LZ177448'



select distinct a.option_code, b.description
from gmgl.vehicle_option_codes a 
left join gmgl.option_codes b on a.option_code = b.option_code
  and a.model_year = b.model_year
  and a.vehicle_description = b.vehicle_description
where a.vin = '1GCUYGED7LZ177448'



select *
from jon.xfm_configurations
where chrome_style_id = '406486'  

create table chr.build_data (
  vin citext primary key,
  response jsonb);

-- with build data
-- factory options
select 'Factory Options' as category, f.factory_options->'header'->>'$value' as header,
  f.factory_options->'attributes'->>'oemCode' as option_code,
  coalesce(o.oem_description->>'$value', replace(d.description::text, '"','')) as description
from chr.build_data a
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
left join jsonb_array_elements(f.factory_options->'oem-description') as o(oem_description) on true
left join jsonb_array_elements(f.factory_options->'description') as d(description) on true
where a.vin = '1GCUYGED7LZ177448'
order by f.factory_options->'header'->>'$value', f.factory_options->'attributes'->>'oemCode'

-- standard equipment
select 'Standard Equipment', s.standard->'header'->>'$value','' as option_code,
  replace((s.standard->'description')::text,'"','') as description
--   s.standard->'category',
--   s.standard->'category'->0->'attributes'->>'id',
--   s.standard->'category'->1->'attributes'->>'id'
from chr.build_data a
join jsonb_array_elements(a.response->'standard') as s(standard) on true
left join jsonb_array_elements(a.response->'standard'->'category') as c(cat) on true
where a.vin = '1GCUYGED7LZ177448'


 -- without build data, requires VIS to identify options, no oem-description
select f.factory_options->'header'->>'$value',
  f.factory_options->'attributes'->>'oemCode',
  coalesce(o.oem_description->>'$value', replace(d.description::text, '"',''))
from chr.describe_vehicle a
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
left join jsonb_array_elements(f.factory_options->'oem-description') as o(oem_description) on true
left join jsonb_array_elements(f.factory_options->'description') as d(description) on true
join gmgl.vehicle_option_codes aa on a.vin = aa.vin 
  and aa.option_code = (f.factory_options->'attributes'->>'oemCode')::citext 
where a.vin = '1GCUYGED7LZ177448'
order by f.factory_options->'header'->>'$value', f.factory_options->'attributes'->>'oemCode'

select s.standard->'header'->>'$value',
  replace((s.standard->'description')::text,'"','') as description
--   s.standard->'category',
--   s.standard->'category'->0->'attributes'->>'id',
--   s.standard->'category'->1->'attributes'->>'id'
from chr.describe_vehicle a
join jsonb_array_elements(a.response->'standard') as s(standard) on true
left join jsonb_array_elements(a.response->'standard'->'category') as c(cat) on true
where a.vin = '1GCUYGED7LZ177448'





drop table if exists categories;
create temp table categories as
select c.categories->'group'->>'$value' as category_group,
  c.categories->'header'->>'$value' as category_header,
  c.categories->'category'->>'$value' as category,
  c.categories->'category'->'attributes'->>'id' as id
from chr.category_definitions a
join jsonb_array_elements(a.response) as c(categories) on true


-- not every option code in vis exists in chrome as factory options, either showavailable
select *
from (
select distinct a.option_code, b.description
from gmgl.vehicle_option_codes a 
left join gmgl.option_codes b on a.option_code = b.option_code
  and a.model_year = b.model_year
  and a.vehicle_description = b.vehicle_description
where a.vin = '1GCUYGED7LZ177448') aa
left join (
select f.factory_options->'header'->>'$value',
  f.factory_options->'attributes'->>'oemCode' as option_code,
  coalesce(o.oem_description->>'$value', replace(d.description::text, '"',''))
from chr.describe_vehicle a
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
left join jsonb_array_elements(f.factory_options->'oem-description') as o(oem_description) on true
left join jsonb_array_elements(f.factory_options->'description') as d(description) on true
where a.vin = '1GCUYGED7LZ177448') bb on aa.option_code = bb.option_code



-- not every option code in vis exists in chrome as factory options, either or build data
select *
from (
select distinct a.option_code, b.description
from gmgl.vehicle_option_codes a 
left join gmgl.option_codes b on a.option_code = b.option_code
  and a.model_year = b.model_year
  and a.vehicle_description = b.vehicle_description
where a.vin = '1GCUYGED7LZ177448') aa
left join (
select f.factory_options->'header'->>'$value',
  f.factory_options->'attributes'->>'oemCode' as option_code,
  coalesce(o.oem_description->>'$value', replace(d.description::text, '"',''))
from chr.build_data a
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
left join jsonb_array_elements(f.factory_options->'oem-description') as o(oem_description) on true
left join jsonb_array_elements(f.factory_options->'description') as d(description) on true
where a.vin = '1GCUYGED7LZ177448') bb on aa.option_code = bb.option_code


build a full out parsing of a vehicle for greg from build data

-- with build data
-- factory options
select f->'header'->>'$value' as header,
  f->'attributes'->>'oemCode' as option_code,
  coalesce(o->>'$value', replace(d.description::text, '"','')) as description
from chr.build_data a
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
left join jsonb_array_elements(f.factory_options->'oem-description') as o(oem_description) on true
left join jsonb_array_elements(f.factory_options->'description') as d(description) on true
where a.vin = '1GCUYGED7LZ177448'
order by f->'header'->>'$value', f->'attributes'->>'oemCode'

-- standard equipment
select s->'header'->>'$value' as header, 
  s->>'description' as description
from chr.build_data a
join jsonb_array_elements(a.response->'standard') as s(standard) on true
left join jsonb_array_elements(a.response->'standard'->'category') as c(cat) on true
where a.vin = '1GCUYGED7LZ177448';



select a.response->'vinDescription'->'attributes'->>'vin', 
  a.response->'vinDescription'->'attributes'->>'modelYear' as model_year,
  a.response->'vinDescription'->'attributes'->>'deivision' as make,
  a.response->'vinDescription'->'attributes'->>'mo'
from chr.build_data a
join jsonb_array_elements(a.response->'style') as s(style) on true
where a.vin = '1GCUYGED7LZ177448'

-- vehicle
select b.vin_desc->>'vin' as vin, 
  b.vin_desc->>'modelYear' as model_year,
  b.vin_desc->>'division' as make,
  s->'subdivision'->>'$value' as sub_division,
  a.response->'vinDescription'->'marketClass'->0->>'$value' as market_class,
  b.vin_desc->>'modelName' as model,
  b.vin_desc->>'styleName' as vin_style_name,
  s->'attributes'->>'name' as style_name,
  s->'attributes'->>'nameWoTrim' as style_name_wo_trim,
  s->'attributes'->>'altStyleName' as alt_style_name,
  b.vin_desc->>'bodyType' as body_type,
  s->'attributes'->>'altBodyType' as alt_body_type,
  b.vin_desc->>'drivingWheels' as drive_wheels,
  s->'attributes'->>'drivetrain' as drive_train,
  a.response->'vinDescription'->'marketClass'->0->>'$value' as market_class,
  s->'attributes'->>'mfrModelCode' as model_code,
  s->'attributes'->>'trim' as trim_level,
  f->'attributes'->>'oemCode' as peg,
  s->'bodyType'->0->>'$value' as bed,
  s->'bodyType'->1->>'$value' as cab
from chr.build_data a
join jsonb_array_elements(a.response->'style') as s(style) on true
join (
  select a.response->'vinDescription'->'attributes' as vin_desc
  from chr.build_data a
  where a.vin = '1GCUYGED7LZ177448') b on true
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
  and f.factory_options->'header'->>'$value' = 'PREFERRED EQUIPMENT GROUP'
where a.vin = '1GCUYGED7LZ177448'

-- engine
select  e->'attributes'->>'highOutput' as high_output,
  e->'engineType'->>'$value' as engine_type,
  e->'fuelType'->>'$value' as fuel,
  e->'horsepower'->'attributes'->>'value' as hp,
  e->'netTorque'->'attributes'->>'value' as torque,
  e->>'cylinders' as cylinders,
  (e->'displacement'->'value'->0->>'$value')::text || ' ' || (e->'displacement'->'value'->0->'attributes'->>'unit')::text as displacement_1,
  (e->'displacement'->'value'->1->>'$value')::text || ' ' || (e->'displacement'->'value'->1->'attributes'->>'unit')::text as displacement_2,
  e->'fuelEconomy'->'city'->'attributes'->>'high' as city_mpg,
  e->'fuelEconomy'->'hwy'->'attributes'->>'high' as hwy_mpg,
  e->'fuelCapacity'->'attributes'->>'high' as fuel_capacity,
  st->>'description' as description
from chr.build_data a
join jsonb_array_elements(a.response->'engine') as e(engine) on true 
join jsonb_array_elements(a.response->'standard') as st(standard) on true
  and st->'installed'->'attributes'->>'cause' = 'Engine'
where a.vin = '1GCUYGED7LZ177448'

-- transmission
select f->'description'->>0 as description,
  f->'oem-description'->0->>'$value' as oem_description
from chr.build_data a
join jsonb_array_elements(a.response->'style') as s(style) on true
join (
  select a.response->'vinDescription'->'attributes' as vin_desc
  from chr.build_data a
  where a.vin = '1GCUYGED7LZ177448') b on true
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
  and f.factory_options->'header'->>'$value' = 'TRANSMISSION'
where a.vin = '1GCUYGED7LZ177448'
