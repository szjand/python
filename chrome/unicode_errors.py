# encoding=utf-8
"""
https://stackoverflow.com/questions/21504319/python-3-csv-file-giving-unicodedecodeerror-utf-8-codec-cant-decode-byte-err
YES, it works:
opening the csv in pycharm, highlighted the bad character, found that character in notepad, turned out to be ³,
the error message: UnicodeDecodeError: 'utf-8' codec can't decode byte 0xb3 in position 728: invalid start byte
therefore in the script:
  text = b'\xb3'
  if msg == '³':

Decoding b'\xb3' with cp1252 is ³
Decoding b'\xb3' with cp1256 is ³
Decoding b'\xb3' with cp1255 is ³
Decoding b'\xb3' with iso8859_9 is ³
Decoding b'\xb3' with cp1258 is ³
Decoding b'\xb3' with iso8859_1 is ³
Decoding b'\xb3' with palmos is ³
Decoding b'\xb3' with iso8859_13 is ³
Decoding b'\xb3' with iso8859_3 is ³
Decoding b'\xb3' with iso8859_15 is ³
Decoding b'\xb3' with latin_1 is ³
Decoding b'\xb3' with raw_unicode_escape is ³
Decoding b'\xb3' with cp1254 is ³
Decoding b'\xb3' with iso8859_7 is ³
Decoding b'\xb3' with charmap is ³
Decoding b'\xb3' with unicode_escape is ³
Decoding b'\xb3' with cp1257 is ³
Decoding b'\xb3' with cp1253 is ³
Decoding b'\xb3' with iso8859_8 is ³

latin_1 seems to be the safest bet
"""
import pkgutil
import encodings
import os

def all_encodings():
    modnames = set([modname for importer, modname, ispkg in pkgutil.walk_packages(
        path=[os.path.dirname(encodings.__file__)], prefix='')])
    aliases = set(encodings.aliases.aliases.values())
    return modnames.union(aliases)

text = b'\xb3'
for enc in all_encodings():
    try:
        msg = text.decode(enc)
    except Exception:
        continue
    if msg == '³':
        print('Decoding {t} with {enc} is {m}'.format(t=text, enc=enc, m=msg))