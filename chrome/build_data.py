import db_cnx
import requests
"""

"""
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        # sql = """select vin from jon.vins;"""
        sql = "select 'JN8AY2NC0LX517620'"
        # sql = """
        #     select vin
        #     from nc.stg_availability a
        #     where not exists (
        #         select 1
        #         from chr.describe_vehicle
        #         where vin = a.vin);
        # """

        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin = row[0]
            url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-build-data/' + vin
            data = requests.get(url)
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into chr.build_data(vin,response)
                    values ('{0}','{1}');
                """.format(vin, resp)
                chr_cur.execute(sql)

