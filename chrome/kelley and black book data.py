import db_cnx
import requests
"""
url from Postman  -> VISION -> kbb-vs-bb
"""
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:

        sql = """
            select vin from pp.equinox_built_in_2018 ;
        """

        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin = row[0]
            print(vin)
            # vin = '3GNAXUEVXLS576033'
            url = 'https://beta.rydellvision.com:8888/vision/kbb-vs-bb/' + vin
            data = requests.get(url)
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into pp.kelley_black_book(vin,response)
                    values ('{0}','{1}');
                """.format(vin, resp)
                chr_cur.execute(sql)



