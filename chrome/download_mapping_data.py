# encoding=utf-8
"""
this copies the new data into tables with a suffix of _a on the name
the actual update sql: E:\python_projects\chrome\sql\mapping_data.sql
"""
from ftplib import FTP
import zipfile
import db_cnx


pg_server = 173
BlackBookPath = '/mnt/hgfs/E/python_projects/chrome/black_book_mapping/'
BlueBookPath = '/mnt/hgfs/E/python_projects/chrome/kelley_blue_book_mapping/'

ftp = FTP('ftp.chromedata.com')
ftp.login('u265491', 'car491')
ftp.cwd('/BlackBookMapping')
files = ftp.nlst()
for file in files:
    ftp.retrbinary("RETR " + file, open(BlackBookPath + file, 'wb').write)
ftp.cwd('/KBBMapping')
files = ftp.nlst()
for file in files:
    ftp.retrbinary("RETR " + file, open(BlueBookPath + file, 'wb').write)
ftp.close()

zipfile.ZipFile(BlackBookPath + 'Mapping-USBlackBook.zip').extractall(BlackBookPath)

with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:

        pg_cur.execute("truncate chr.black_book_required_options_a cascade")
        pg_cur.execute("truncate chr.black_book_vehicle_map_a cascade")
        pg_cur.execute("truncate chr.black_book_version_info_a cascade")

        with open(BlackBookPath + 'VehicleMap.txt') as aa:
            pg_cur.copy_expert("""copy chr.black_book_vehicle_map_a from stdin with csv
                                  header quote '~' encoding 'latin-1'""", aa)

        with open(BlackBookPath + 'RequiredOptions.txt') as bb:
            pg_cur.copy_expert("""copy chr.black_book_required_options_a from stdin with csv
                                  header quote '~' encoding 'latin-1'""", bb)

        with open(BlackBookPath + 'VersionInfo.txt') as cc:
            pg_cur.copy_expert("""copy chr.black_book_version_info_a from stdin with csv
                                  header quote '~' encoding 'latin-1'""", cc)

zipfile.ZipFile(BlueBookPath + 'Mapping-KBB.zip').extractall(BlueBookPath)

with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:

        pg_cur.execute("truncate chr.kelley_blue_book_required_options_a cascade")
        pg_cur.execute("truncate chr.kelley_blue_book_vehicle_map_a cascade")
        pg_cur.execute("truncate chr.kelley_blue_book_version_info_a cascade")

        with open(BlueBookPath + 'VehicleMap.txt') as aa:
            pg_cur.copy_expert("""copy chr.kelley_blue_book_vehicle_map_a from stdin with csv
                                  header quote '~' encoding 'latin-1'""", aa)

        with open(BlueBookPath + 'RequiredOptions.txt') as bb:
            pg_cur.copy_expert("""copy chr.kelley_blue_book_required_options_a from stdin with csv
                                  header quote '~' encoding 'latin-1'""", bb)

        with open(BlueBookPath + 'VersionInfo.txt') as cc:
            pg_cur.copy_expert("""copy chr.kelley_blue_book_version_info_a from stdin with csv
                                  header quote '~' encoding 'latin-1'""", cc)
