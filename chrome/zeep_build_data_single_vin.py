# encoding=utf-8
"""
06/01/21
    ALL Technical Documentation & Code Samples
        http://update.chromedata.com/
        User name: 265491
        Password:  car491
        Access to Data Products

    MAPPING TABLES (KBB & BLACK BOOK)
        ftp.chromedata.com
        User Name: u265491
        Password: car491

    Access to Chrome Web Services
    AUTOMOTIVE DESCRIPTION SERVICE – FULL (WITH BUILDDATA)
        Account Number: 306422
        Account Secret: aa118f7cce54469d
    BATCH PROCESSING: Automotive Description Service
    AUTOMOTIVE DESCRIPTION SERVICE- FULL (WITH BUILDDATA)
        ftp.chromedata.com
        User Name: rc306422_7
        Password: car422


Notes from build data trial:
    changed the end point to 7c
    end point 7c is returning a datetime not json serializable error
    nope, it's not the 7c it is the new credentials

    adding the default=str to this json.dumps(my_dict, default=str) got rid of the serializable error

06/15/21
    first test, 1GCRYDEK3MZ3596982021, model code CK10753, 2 styles
    and fucking build data request returns 2 styles with source = Catalog

07/23/21
    i am confused a 2020 sierra, 1GT49REY7LF185207, and this is returning catalog
    data, list of colors rather than the build color
09/07/21
    turned off saving response to file and added updating style_count, source
"""
import zeep
from zeep import Client
from zeep import helpers
import json
import db_cnx
from datetime import datetime


def get_chrome_by_vin(vehicle_vin):
    # json_directory = '/mnt/hgfs/E/python_projects/chrome/json_zeep_build_data/'

    authentication = {"number": '306422',
                      "secret": 'aa118f7cce54469d',
                      "country": 'US',
                      "language": 'en'}
    client = Client("http://services.chromedata.com/Description/7c?wsdl")
    response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
                                              switch=['ShowExtendedTechnicalSpecifications', 'ShowAvailableEquipment'])
    my_dict = zeep.helpers.serialize_object(response, dict)
    # json.dumps (dump string) convert a python object into a json string
    the_response = json.dumps(my_dict, default=str)

    # # writes the file
    # with open(json_directory + vehicle_vin + '.json', 'w', encoding='utf-8') as f:
    #     # json.dump used for writing to json file
    #     # 2021 nissan threw: TypeError: Object of type 'datetime' is not JSON serializable
    #     # added default=str
    #     json.dump(my_dict, f, default=str)

    the_response = the_response.replace("'", "''")
    the_date = datetime.today().strftime('%m/%d/%Y')
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into chr.build_data_describe_vehicle(vin,response, the_date)
                values ('{0}', '{1}', '{2}');
            """.format(vehicle_vin, the_response, the_date)
            pg_cur.execute(sql)
        with pg_con.cursor() as chr_cur:
            sql = """
                update chr.build_data_describe_vehicle y
                set style_count = x.style_count, source = x.source
                from (
                    select vin, jsonb_array_length(response->'style') as style_count,  
                        response->'vinDescription'->>'source' as source
                    from chr.build_data_describe_vehicle	
                    where vin = '{0}') x
                where x.vin = y.vin;  
            """.format(vehicle_vin)
            chr_cur.execute(sql)

if __name__ == '__main__':
    get_chrome_by_vin('3C63R2HL7MG540368')
