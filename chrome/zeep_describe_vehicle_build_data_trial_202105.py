# encoding=utf-8
"""
05/11/21
    received builddata credentials
    ALL Technical Documentation & Code Samples
        http://update.chromedata.com/
        User name: 265491
        Password:  car491
        Access to Chrome Web Services
    AUTOMOTIVE DESCRIPTION SERVICE – FULL (WITH BUILDDATA)
        Account Number: 306422
        Account Secret: aa118f7cce54469d
    BATCH PROCESSING: Automotive Description Service
    AUTOMOTIVE DESCRIPTION SERVICE- FULL (WITH BUILDDATA)
        ftp.chromedata.com
        User Name: rc306422_7
        Password: car422

changed the end point to 7c
end point 7c is returning a datetime not json serializable error
nope, it's not the 7c it is the new credentials

adding the default=str to this json.dumps(my_dict, default=str) got rid of the serializable error


"""
import zeep
from zeep import Client
from zeep import helpers
import json
import db_cnx


# https://www.ddataconsulting.com/blog/soap-in-python-using-zeep
# 12/19/18 good up to the db part, returns valid json


def get_chrome_by_vin(vehicle_vin):
    json_directory = '/mnt/hgfs/E/python_projects/chrome/json_zeep_build_data/'

    authentication = {"number": '306422',
                      "secret": 'aa118f7cce54469d',
                      "country": 'US',
                      "language": 'en'}
    client = Client("http://services.chromedata.com/Description/7c?wsdl")

    # # VIN No Switch
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin)

    # # VIN ShowAvailableEquipment
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch='ShowAvailableEquipment')
    # # VIN ShowExtendedTechnicalSpecifications
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch='ShowExtendedTechnicalSpecifications')
    #
    # # VIN ShowAvailableEquipment & ShowExtendedTechnicalSpecifications
    response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
                                              switch=['ShowExtendedTechnicalSpecifications','ShowAvailableEquipment'])

    # Style No Switch
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin)
    #
    # # Style ShowAvailableEquipment
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch='ShowAvailableEquipment')
    # # Style ShowExtendedTechnicalSpecifications
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch='ShowExtendedTechnicalSpecifications')

    # Style ShowAvailableEquipment & ShowExtendedTechnicalSpecifications
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch=['ShowExtendedTechnicalSpecifications','ShowAvailableEquipment'])

    my_dict = zeep.helpers.serialize_object(response, dict)
    # json.dumps (dump string) convert a python object into a json string
    the_response = json.dumps(my_dict, default=str)

    # writes the file
    with open(json_directory + vehicle_vin + '.json', 'w', encoding='utf-8') as f:
        # json.dump used for writing to json file
        # 2021 nissan threw: TypeError: Object of type 'datetime' is not JSON serializable
        # added default=str
        json.dump(my_dict, f, default=str)

    # the_response = the_response.replace("'", "''")
    # with db_cnx.pg() as pg_con:
    #     with pg_con.cursor() as pg_cur:
    #         sql = """
    #             insert into chr.build_data_trial(vin,response)
    #             values ('{0}', '{1}');
    #         """.format(vehicle_vin, the_response)
    #         pg_cur.execute(sql)



def get_chrome_by_style(style_id):
    authentication = {"number": 'rc306422_7',
                      "secret": '24a63940ed48890b',
                      "country": 'US',
                      "language": 'en'}
    client = Client("http://services.chromedata.com/Description/7b?wsdl")

    # # Style No Switch
    # response = client.service.describeVehicle(accountInfo=authentication, styleId=style_id)
    #
    # # Style ShowAvailableEquipment
    # response = client.service.describeVehicle(accountInfo=authentication, styleId=style_id,
    #                                           switch='ShowAvailableEquipment')
    # # Style ShowExtendedTechnicalSpecifications
    # response = client.service.describeVehicle(accountInfo=authentication, styleId=style_id,
    #                                           switch='ShowExtendedTechnicalSpecifications')

    # Style ShowAvailableEquipment & ShowExtendedTechnicalSpecifications
    response = client.service.describeVehicle(accountInfo=authentication, styleId=style_id,
                                              switch=['ShowExtendedTechnicalSpecifications','ShowAvailableEquipment'])

    my_dict = zeep.helpers.serialize_object(response)
    response = json.dumps(my_dict)
    return response

# 411618: trailblazer
# 406482: silverado 1500


if __name__ == '__main__':
    get_chrome_by_vin('1GCRYDEK3MZ359698')
    # print(get_chrome_by_style('406972'))

