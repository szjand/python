# encoding=utf-8
"""
start with batch/python/results/20201129_request_4
eventually this will loop thru a directory and process the files into tables

12/2/20
experiment with 20201129_request_5, big batch result
use it to test the structure of the chr2 tables i am building
"""
import csv
import db_cnx

file_path = 'batch_python/results/20201129_request_5/'
file_name = 'body_type.csv'
clean_file_name = 'clean_body_type.csv'
table_name = 'chr2.body_type_tmp'


with open(file_path + file_name, 'r',
          encoding='latin_1', newline='') as infile, open(
        file_path + clean_file_name, 'w') as outfile:
    reader = csv.reader(infile, delimiter=",", quotechar="~")
    writer = csv.writer(outfile)
    for row in reader:
        writer.writerow(row)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        with open(file_path + clean_file_name, 'r', encoding='latin_1', newline='') as the_file:
            pg_cur.copy_expert("copy " + table_name + " from stdin with csv header encoding 'latin-1'", the_file)
