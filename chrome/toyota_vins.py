import db_cnx
import requests
"""
toyota vins not yet in dds.dim_vehicle or chr.describe_vehicle
"""
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        sql = """
            select vin
            from jon.toyota_vins a
            where not exists (
              select 1
              from chr.describe_vehicle
              where vin = a.vin)
            limit 2000;
        """
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin = row[0]
            # print(vin)
            url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-options/' + vin
            data = requests.get(url)
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into chr.describe_vehicle(vin,response)
                    values ('{0}','{1}');
                """.format(vin, resp)
                chr_cur.execute(sql)
        with pg_con.cursor() as chr_cur:
            sql = """
                update chr.describe_vehicle y
                set style_count = x.style_count
                from (
                  select vin, count(*) as style_count
                  from (
                    select vin, jsonb_array_elements(response->'style')
                    from chr.describe_vehicle
                    where style_count is null) a
                  group by vin) x
                where x.vin = y.vin;  
            """
            chr_cur.execute(sql)
