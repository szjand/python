import db_cnx
import requests
"""
"""
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        # sql = """select vin from jon.vins;"""
        # sql = "select '5FNRL6H96RB017727'"
        # sql = """
        #     select '1N4BL4CW9PN325627'
        #     union select '5TDKDRBH4PS018416'
        # """
        # # canonical used car data
        # sql = """
        #     select distinct a.vin  -- 483 vins
        #     from cu.used_3 a
        #     join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
        #     left join chr.build_data_coverage c on b.make = c.division
        #       and b.year between c.from_year and c.thru_year
        #     where not exists (
        #       select 1
        #       from cu.tmp_vehicles
        #       where vin = a.vin)
        #     and c.division is null -- exclude candidates for build data
        #     and not exists (
        #       select 1
        #       from chr.describe_vehicle
        #       where vin = a.vin)
        #     and make not in ('CHEVORLET','KAWASAKI','HARLEY','HARLEY-DAVIDSON','YAMAHA')
        #     and a.vin not in ('1HD1KED12HB640756','1HD1KHM18FB688096','JS1GT76A252103098')
        # """
        # stg_availability
        sql = """
            select vin
            from nc.stg_availability a
            where not exists (
                select 1
                from chr.describe_vehicle
                where vin = a.vin)
            and not exists (
                select 1
                from chr.build_data_describe_vehicle
                where vin = a.vin)
        """
        # # open orders
        # sql = """
        #      select a.vin
        #      from nc.open_orders a
        #      where a.vin is not null
        #        and not exists (
        #          select 1
        #          from chr.describe_vehicle
        #          where vin = a.vin)
        #  """

        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin = row[0]
            print(vin)
            # url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle/' + vin
            url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-options/' + vin
            data = requests.get(url)
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into chr.describe_vehicle(vin,response)
                    values ('{0}','{1}');
                """.format(vin, resp)
                chr_cur.execute(sql)
        with pg_con.cursor() as chr_cur:
            sql = """
                update chr.describe_vehicle y
                set style_count = x.style_count
                from (
                  select vin, count(*) as style_count
                  from (
                    select vin, jsonb_array_elements(response->'style')
                    from chr.describe_vehicle
                    where style_count is null) a
                  group by vin) x
                where x.vin = y.vin;  
            """
            chr_cur.execute(sql)
