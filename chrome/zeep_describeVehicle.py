
import zeep
from zeep import Client
from zeep import helpers
import json

# https://www.ddataconsulting.com/blog/soap-in-python-using-zeep
# 12/19/18 good up to the db part, returns valid json


def get_chrome_by_vin(vehicle_vin):
    """

    """
    authentication = {"number": '265491',
                      "secret": '24a63940ed48890b',
                      "country": 'US',
                      "language": 'en'}
    client = Client("http://services.chromedata.com/Description/7b?wsdl")

    # # VIN No Switch
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin)

    # # VIN ShowAvailableEquipment
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch='ShowAvailableEquipment')
    # # VIN ShowExtendedTechnicalSpecifications
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch='ShowExtendedTechnicalSpecifications')
    #
    # # VIN ShowAvailableEquipment & ShowExtendedTechnicalSpecifications
    response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
                                              switch=['ShowExtendedTechnicalSpecifications','ShowAvailableEquipment'])

    # Style No Switch
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin)
    #
    # # Style ShowAvailableEquipment
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch='ShowAvailableEquipment')
    # # Style ShowExtendedTechnicalSpecifications
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch='ShowExtendedTechnicalSpecifications')

    # Style ShowAvailableEquipment & ShowExtendedTechnicalSpecifications
    # response = client.service.describeVehicle(accountInfo=authentication, vin=vehicle_vin,
    #                                           switch=['ShowExtendedTechnicalSpecifications','ShowAvailableEquipment'])

    my_dict = zeep.helpers.serialize_object(response)
    response = json.dumps(my_dict)
    return response


def get_chrome_by_style(style_id):
    authentication = {"number": '265491',
                      "secret": '24a63940ed48890b',
                      "country": 'US',
                      "language": 'en'}
    client = Client("http://services.chromedata.com/Description/7b?wsdl")

    # # Style No Switch
    # response = client.service.describeVehicle(accountInfo=authentication, styleId=style_id)
    #
    # # Style ShowAvailableEquipment
    # response = client.service.describeVehicle(accountInfo=authentication, styleId=style_id,
    #                                           switch='ShowAvailableEquipment')
    # # Style ShowExtendedTechnicalSpecifications
    # response = client.service.describeVehicle(accountInfo=authentication, styleId=style_id,
    #                                           switch='ShowExtendedTechnicalSpecifications')

    # Style ShowAvailableEquipment & ShowExtendedTechnicalSpecifications
    response = client.service.describeVehicle(accountInfo=authentication, styleId=style_id,
                                              switch=['ShowExtendedTechnicalSpecifications','ShowAvailableEquipment'])

    my_dict = zeep.helpers.serialize_object(response)
    response = json.dumps(my_dict)
    return response

# 411618: trailblazer
# 406482: silverado 1500


if __name__ == '__main__':
    print(get_chrome_by_vin('KL4CJGSM4JB657645'))
    # print(get_chrome_by_style('406972'))

