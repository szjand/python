# encoding=utf-8
"""
06/01/21
    ALL Technical Documentation & Code Samples
        http://update.chromedata.com/
        User name: 265491
        Password:  car491
        Access to Data Products

    MAPPING TABLES (KBB & BLACK BOOK)
        ftp.chromedata.com
        User Name: u265491
        Password: car491

    Access to Chrome Web Services
    AUTOMOTIVE DESCRIPTION SERVICE – FULL (WITH BUILDDATA)
        Account Number: 306422
        Account Secret: aa118f7cce54469d
    BATCH PROCESSING: Automotive Description Service
    AUTOMOTIVE DESCRIPTION SERVICE- FULL (WITH BUILDDATA)
        ftp.chromedata.com
        User Name: rc306422_7
        Password: car42
08/27/21
    finally, we have GM build data
09/07/21
    remove the writing to file
    add updating style_count & source
"""
from datetime import datetime
import zeep
from zeep import Client
from zeep import helpers
import json
import db_cnx
import time

with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        # sql = """
        #     select distinct b.vin
        #     from chr.build_data_coverage a
        #     join cra.five_years b on a.division = b.make
        #       and b.model_year::integer between a.from_year and a.thru_year
        #     where length(b.vin) = 17
        #       and (select cra.vin_check_sum(vin)) -- passes check digit
        #       and not exists (
        #         select 1
        #         from chr.build_data_describe_vehicle
        #         where vin = b.vin)
        #     limit 1000;
        # """
        sql = """
            select distinct vin 
            from cra.five_years a
            where chr_make is null 
              and not exists (
                select 1
                from chr.build_data_describe_vehicle
                where vin = a.vin);
        """
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            the_vin = row[0]
            the_date = datetime.today().strftime('%m/%d/%Y')
            json_directory = '/mnt/hgfs/E/python_projects/chrome/json_zeep_build_data/'
            authentication = {"number": '306422',
                              "secret": 'aa118f7cce54469d',
                              "country": 'US',
                              "language": 'en'}
            client = Client("http://services.chromedata.com/Description/7c?wsdl")
            response = client.service.describeVehicle(accountInfo=authentication, vin=the_vin,
                                                      switch=['ShowExtendedTechnicalSpecifications', 'ShowAvailableEquipment'])
            my_dict = zeep.helpers.serialize_object(response, dict)
            the_response = json.dumps(my_dict, default=str)
            the_response = the_response.replace("'", "''")
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    sql = """
                        insert into chr.build_data_describe_vehicle(vin,response,the_date)
                        values ('{0}', '{1}', '{2}');
                    """.format(the_vin, the_response, the_date)
                    pg_cur.execute(sql)
                    # time.sleep(1)
                    print(the_vin)
                    sql = """
                        update chr.build_data_describe_vehicle y
                        set style_count = x.style_count, source = x.source
                        from (
                            select vin, jsonb_array_length(response->'style') as style_count,  
                                response->'vinDescription'->>'source' as source
                            from chr.build_data_describe_vehicle	
                            where vin = '{0}') x
                        where x.vin = y.vin;  
                    """.format(the_vin)
                    pg_cur.execute(sql)



