﻿create table chr.kbb_vehicle_map (
  mapping_id integer primary key,
  style_id integer not null,
  vehicle_id citext not null);

create table chr.kbb_required_options (
	required_option_id integer primary key,
	mapping_id integer not null references chr.kbb_vehicle_map(mapping_id),
	vac citext,
	option_code citext,
	add_deduct citext,
	option_display_name citext);
	
create table chr.bb_vehicle_map (
	mapping_id integer primary key,
	style_id integer not null,
	uvc text not null);

create table chr.bb_required_options (
  required_option_id integer primary key,
  mapping_id integer not null references chr.bb_vehicle_map(mapping_id),
  option_code citext,
  uoc citext,
  add_deduct citext);


select * from chr.bb_required_options

select * 
from chr2.style_ids a
join chr.kbb_vehicle_map b on a.style_id::integer = b.style_id

select * from chr.describe_vehicle where vin = '1FTFW1E51MFB49040'

select make, count(*)
from arkona.ext_inpmast
where year = 2021
group by make

select make, model, inpmast_vin
from arkona.ext_inpmast
where year = 2021
and make = 'nissan'