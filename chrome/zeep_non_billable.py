
import zeep
from zeep import Client
from zeep import helpers
import json

# https://www.ddataconsulting.com/blog/soap-in-python-using-zeep
# 12/19/18 good up to the db part, returns valid json




def get_chrome_specs():
    authentication = {"number": '265491',
                      "secret": '24a63940ed48890b',
                      "country": 'US',
                      "language": 'en'}
    client = Client("http://services.chromedata.com/Description/7b?wsdl")

    # # Technical Specification Definitions
    response = client.service.getTechnicalSpecificationDefinitions(accountInfo=authentication)


    my_dict = zeep.helpers.serialize_object(response)
    response = json.dumps(my_dict)
    return response


def get_subdivisions():
    authentication = {"number": '265491',
                      "secret": '24a63940ed48890b',
                      "country": 'US',
                      "language": 'en'}
    client = Client("http://services.chromedata.com/Description/7b?wsdl")

    # # Subdivisions
    response = client.service.getSubdivisions(accountInfo=authentication, modelYear=2022)


    my_dict = zeep.helpers.serialize_object(response)
    response = json.dumps(my_dict)
    return response

if __name__ == '__main__':
    # print(get_chrome_by_vin('1GCPYCEF4LZ146777'))
    # print(get_chrome_specs())
    print(get_subdivisions())
