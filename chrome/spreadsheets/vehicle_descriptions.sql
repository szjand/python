﻿-- vehicle
select b.vin_desc->>'vin' as vin, 
  b.vin_desc->>'modelYear' as model_year,
  b.vin_desc->>'division' as make,
  s->'subdivision'->>'$value' as sub_division,
  a.response->'vinDescription'->'marketClass'->0->>'$value' as market_class,
  b.vin_desc->>'modelName' as model,
  b.vin_desc->>'styleName' as vin_style_name,
  s->'attributes'->>'name' as style_name,
  s->'attributes'->>'nameWoTrim' as style_name_wo_trim,
  s->'attributes'->>'altStyleName' as alt_style_name,
  b.vin_desc->>'bodyType' as body_type,
  s->'attributes'->>'altBodyType' as alt_body_type,
  b.vin_desc->>'drivingWheels' as drive_wheels,
  s->'attributes'->>'drivetrain' as drive_train,
  a.response->'vinDescription'->'marketClass'->0->>'$value' as market_class,
  s->'attributes'->>'mfrModelCode' as model_code,
  s->'attributes'->>'trim' as trim_level,
  f->'attributes'->>'oemCode' as peg,
  s->'bodyType'->0->>'$value' as bed,
  s->'bodyType'->1->>'$value' as cab
from chr.build_data a
join jsonb_array_elements(a.response->'style') as s(style) on true
join (
  select a.response->'vinDescription'->'attributes' as vin_desc
  from chr.build_data a
  where a.vin = '1GCUYGED7LZ177448') b on true
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
  and f.factory_options->'header'->>'$value' = 'PREFERRED EQUIPMENT GROUP'
where a.vin = '1GCUYGED7LZ177448';


-- engine
select  e->'attributes'->>'highOutput' as high_output,
  e->'engineType'->>'$value' as engine_type,
  e->'fuelType'->>'$value' as fuel,
  e->'horsepower'->'attributes'->>'value' as hp,
  e->'netTorque'->'attributes'->>'value' as torque,
  e->>'cylinders' as cylinders,
  (e->'displacement'->'value'->0->>'$value')::text || ' ' || (e->'displacement'->'value'->0->'attributes'->>'unit')::text as displacement_1,
  (e->'displacement'->'value'->1->>'$value')::text || ' ' || (e->'displacement'->'value'->1->'attributes'->>'unit')::text as displacement_2,
  e->'fuelEconomy'->'city'->'attributes'->>'high' as city_mpg,
  e->'fuelEconomy'->'hwy'->'attributes'->>'high' as hwy_mpg,
  e->'fuelCapacity'->'attributes'->>'high' as fuel_capacity,
  st->>'description' as description
from chr.build_data a
join jsonb_array_elements(a.response->'engine') as e(engine) on true 
join jsonb_array_elements(a.response->'standard') as st(standard) on true
  and st->'installed'->'attributes'->>'cause' = 'Engine'
where a.vin = '1GCUYGED7LZ177448';


-- transmission
select f->'description'->>0 as description,
  f->'oem-description'->0->>'$value' as oem_description
from chr.build_data a
join jsonb_array_elements(a.response->'style') as s(style) on true
join (
  select a.response->'vinDescription'->'attributes' as vin_desc
  from chr.build_data a
  where a.vin = '1GCUYGED7LZ177448') b on true
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
  and f.factory_options->'header'->>'$value' = 'TRANSMISSION'
where a.vin = '1GCUYGED7LZ177448';

-- standard equipment
select s->'header'->>'$value' as header, 
  s->>'description' as description
from chr.build_data a
join jsonb_array_elements(a.response->'standard') as s(standard) on true
left join jsonb_array_elements(a.response->'standard'->'category') as c(cat) on true
where a.vin = '1GCUYGED7LZ177448';


-- factory options
select f->'header'->>'$value' as header,
  f->'attributes'->>'oemCode' as option_code,
  coalesce(o->>'$value', replace(d.description::text, '"','')) as description
from chr.build_data a
join jsonb_array_elements(a.response->'factoryOption') as f(factory_options) on true
left join jsonb_array_elements(f.factory_options->'oem-description') as o(oem_description) on true
left join jsonb_array_elements(f.factory_options->'description') as d(description) on true
where a.vin = '1GCUYGED7LZ177448'
order by f->'header'->>'$value', f->'attributes'->>'oemCode';
