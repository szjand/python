﻿TABLE cu.tmp_vehicles
  vin citext NOT NULL,
  model_year citext NOT NULL,
  make citext NOT NULL,
--   subdivision citext,
  model citext NOT NULL,
  trim_level citext,
  drive citext,  -- Integration Guide p25, recommends using feature id 10750, but i prefer the shorter designation in vehicles
--   fleet_only boolean NOT NULL,
--   model_fleet boolean NOT NULL,
  model_code citext,
  cab citext,  -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?!
  box_size citext,
  engine_disp citext,
  cylinders citext,
  fuel citext,
  transmission citext,
  ext_color citext,
  int_color citext,
  pass_doors citext,
  msrp integer,
  market_class citext,
  body_type citext,  -- confusion with cab vs body type
--   alt_body_type citext,
  style_name citext,  -- styleDescription
--   alt_style_name citext,
--   name_wo_trim citext,
--   best_make_name citext,
--   best_model_name citext,
--   best_trim_name citext,
--   best_style_name citext,
--   pic_url citext,
  source citext,
  chrome_style_id citext,
  CONSTRAINT tmp_vehicles_pkey PRIMARY KEY (vin)

-- build data by year,make,model
drop table if exists the_list;
create temp table the_list as
select a.response->'result'->>'year' as model_year, a.response->'result'->>'make' as make, a.response->'result'->>'model' as model, count(*), min(vin), max(vin)
from chr.cvd a
where source = 'B'
  and style_count = 1
group by a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model' 
order by a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model' 

select * from the_list

-- 11/28/23 now that we have ford and toyota
-- ford build data starts at 2010
-- toyota starts at 1996
  2GCUDEED6P1102772: 23 silverado 1500

drop table if exists jon.tmp_cvd cascade;
create unlogged table jon.tmp_cvd as
-- 13969 rows of the 16589 in chr.cvd
select a.vin, a.response->'result'->>'year' as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  b->>'driveType' as drive,
--   e->>'name' as drive,
  b->>'mfrModelCode' as model_code,
  b->>'bodyType' as body_type, -- problem, doesn't break out cab type separate from body type, so, no body type of pickup !?!?!
--   b->>'boxStyle' as box_style, -- box style is not size
  f->>'name' as eng_disp,
  g->>'nameNoBrand' as cylinders,
  h->>'name' as fuel,
  i->>'name' as transmission,
  c.exterior as ext_color, -- c->>'genericDesc' as generic_ext_color, no longer have the generics, hmm maybe should have included them
  c.interior as int_color, -- d->>'genericDesc' as generic_int_color,
  b->>'doors' as doors, -- also features id 12175
  a.response->'result'->>'buildMSRP' as build_msrp,
  b->>'styleDescription' as style_description,
  a.response->'result'->>'source' as source,
  b->>'styleId' as style_id
--   a.response->'result'->>'source' as source, a.response->'result'->>'validVin' as valid_vin,
--   b->'doors' as doors,
--   b->'segment'->>0 as segment, b->>'styleId' as style_id, b->>'baseMSRP' as base_msrp, 
--    b->>'bodyType' as body_type, 
--   
--   b->>'driveType' as drive,
--   b->>'wheelbase' as wheelbase, b->>'styleDescription' as style_description,
--   c->>'description' as ext_color, d->>'description' as int_color,
--   a.response->'result'->>'vinProcessed' = a.response->'result'->>'vinSubmitted'
from chr.cvd a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
join chr.cvd_colors c on a.vin = c.vin
-- join jsonb_array_elements(a.response->'result'->'exteriorColors') c on true
-- join jsonb_array_elements(a.response->'result'->'interiorColors') d on true
-- left join jsonb_array_elements(a.response->'result'->'features') e on e->>'id' = '10750'
left join jsonb_array_elements(a.response->'result'->'techSpecs') f on f->>'id' = '10120'
-- left join jsonb_array_elements(a.response->'result'->'features') g on g->>'id' = '21690'
left join jsonb_array_elements(a.response->'result'->'techSpecs') g on g->>'description' = 'Engine Cylinders'
left join jsonb_array_elements(a.response->'result'->'features') h on h->>'id' = '10030'
left join jsonb_array_elements(a.response->'result'->'features') i on i->>'id' = '25190'
where source = 'B' 
  and style_count = 1
--   and (a.response->'result'->>'year')::integer > 2018
order by a.response->'result'->>'make', a.response->'result'->>'model', a.response->'result'->>'year'


-- oops lots (554) of dups
-- after fixing the colors, no dups !
select a.*
from jon.tmp_cvd a limit 10
join (
select vin from jon.tmp_cvd group by vin having count(*) > 1) b on a.vin = b.vin
order by a.vin

select a.*
from wtf a
order by make, model, model_year


-- -- list the dups issues out, 1 by 1, first issue ext_color
-- 2011 chrysler 200: mult ext_color
-- select * from chr.cvd where vin = '1C3BC2EB7BN532488'
-- 
-- it appears that all exteriorColors have a primary attribute, whether there are 1 or more primary colors
-- the colors are broken out into separate objects designated byt "type": 1, "type:" 2, etc
-- 
-- 11/29/23
-- decided to do a separate table of ext/int colors
-- E:\python_projects\chrome\CVD-demo\sql\cvd_colors.sql
-- populated a table, chr.cvd_colors, so no longer need to join on the arrays
-- -- this cleaned up some
-- join jsonb_array_elements(a.response->'result'->'exteriorColors') c on true
--   and c->>'primary' = 'true'
--   and c->>'type' = '1'
-- 
-- but there are some with array length over 10 ?!?!?
-- select vin, jsonb_array_length(a.response->'result'->'interiorColors')
-- from chr.cvd a
-- where source = 'B'
--   and jsonb_array_length(a.response->'result'->'interiorColors') > 1



select drive, count(*)
from wtf
group by drive



select * from wtf 
order by make, model, model_year

-- equinoxes missing cylinders, the techspec id is 21691 instead of feature id = 21690, use description 
select vin, model_year, make, cylinders from wtf where vin = '2GNFLGEK0F6170015'

select * from chr.cvd where vin = '3TMCZ5AN2LM364660'




select * from chr.cvd where vin = '1C6RR7LM7ES385189'

-- all box sides
select b->>'boxStyle', count(*)
from chr.cvd a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
group by b->>'boxStyle'

select b->>'name', b->>'nameNoBrand', count(*)
from chr.cvd a
left join jsonb_array_elements(a.response->'result'->'features') b on b->>'id' = '12210'
group by b->>'name', b->>'nameNoBrand'

-- chevy boxes
select model_year, make, box_size, min(vin), max(vin)
from cu.tmp_vehicles
where box_size <> 'n/a'
  and make = 'chevrolet'
group by model_year, make, box_size

-- all makes boxes
select model_year, make, box_size, min(vin), max(vin)
from cu.tmp_vehicles
where box_size not in ('n/a', 'standard')
--   and make = 'chevrolet'
group by model_year, make, box_size
order by model_year  desc

select b.*
from cu.tmp_vehicles a
left join gmgl.vehicle_invoices b on a.vin = b.vin
where a.model_year = '2023'
  and a.make = 'chevrolet'
  and a.box_size not in ('n/a', 'standard')

-- 2023 nissan pk, short bed
select * from chr.cvd where vin = '1N6AA1ED1PN101688'
in techSpecs id 18090 Cargo box length

select * from chr.build_Data_describe_vehicle where vin =  '1N6AA1ED1PN101688'

-- all occurences of techspec 18090
-- explain analyze
select a.vin, a.response->'result'->>'source', a.response->'result'->>'year' as model_year, a.response->'result'->>'make' as make, 
  a.response->'result'->>'model' as model, b->>'trim' as trim_level, 
  c->>'name', c->>'nameNoBrand', c->>'key'
from chr.cvd a
join jsonb_array_elements(a.response->'result'->'vehicles') b on true
Join jsonb_array_elements(a.response->'result'->'techSpecs') c on c->>'id' = '18090'
order by a.response->'result'->>'year', a.response->'result'->>'make', a.response->'result'->>'model'
limit 10


select * from chr.cvd where vin = '2GCUDEED4R1144067'

-- box size for nissan

select configuration_id, make, model, level_header from nc.vehicle_configurations
where config_type like '%b%'
  and make in ('honda','nissan','toyota')
  and (level_header like ('%long%') or level_header like ('%standard%'))
  
order by make, model

nissan long: 84, 629, 487,
			 standard: 1479, 2437, 1375, 383, 339, 448, 154, 

toyota: long: 1714, 2236, 2024, 2440
        standard: 2069, 2247, 1962, 2439, 2188 

select *
from nc.vehicles a
join chr.build_data_describe_vehicle b on a.vin = b.vin
where a.configuration_id in (   84, 629, 487, 1479, 2437, 1375, 383, 339, 448, 154, 1714, 2236, 2024, 2440, 2069, 2247, 1962, 2439, 2188 ) 