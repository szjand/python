# encoding=utf-8
"""
10/22/19
reworking chrome.py, to better detect changes and updates
copied tables to jon schema
1. find and replace chr. -> jon.

11/26/19
    don't fucking remember where i left off
    there are a number of artifacts:
        this script
        ~/scratchpad_chrome /2 /3 sql
        smartdraw chrome.sdr

    seems important because of reliance on chrome for generating configurations
    for ordering

    rearrange directory structure, move EVERYTHING here

    it appears that i had the sense to not edit original processing files
    but added new ones
12/5/19
working from ...chrome/sql/refactor_cdc.sql
implementing in the chrome project

"""
import db_cnx
import requests
from datetime import datetime

# VersionInformation
run_date = datetime.today().strftime('%m/%d/%Y')
url = 'https://beta.rydellvision.com:8888/chrome/version-info'
data = requests.get(url)
data = data.text.replace("'", "''")  # .encode("utf-8")
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as table_cur:
        sql = """
            insert into jon.get_version_info(the_date,response)
            values ('{0}','{1}');
        """.format(run_date,data)
        table_cur.execute(sql)


# model-years
url = 'https://beta.rydellvision.com:8888/chrome/model-years'
data = requests.get(url)
# insert the raw data into the get_ table
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as chr_cur:
        chr_cur.execute("truncate jon.get_model_years")
        sql = """
            insert into jon.get_model_years(model_years)
            values ('{}');
        """.format(data.text)
        chr_cur.execute(sql)
        chr_cur.execute("select jon.xfm_model_years()")

# # divisions
with db_cnx.pg() as pg_con:
    with  pg_con.cursor() as trun_cur:
        sql = "truncate jon.get_divisions"
        trun_cur.execute(sql)
    with pg_con.cursor() as model_year_cur:
        sql = """
            select model_year
            from jon.model_years
            where model_year > 2017;
        """
        model_year_cur.execute(sql)
        for model_year in model_year_cur:
            url = 'https://beta.rydellvision.com:8888/chrome/divisions/' + str(model_year[0])
            data = requests.get(url)
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into jon.get_divisions(model_year,divisions)
                    values ({0},'{1}');
                """.format(model_year[0], data.text)
                chr_cur.execute(sql)
                chr_cur.execute("select jon.xfm_divisions()")


# models by division
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as get_models_cur:
        get_models_cur.execute("truncate jon.get_models_by_division")
    with pg_con.cursor() as division_cur:
        sql = """
            select model_year, division_id
            from jon.divisions
            where division in('chevrolet','buick','gmc','cadillac','honda','nissan');
        """
        division_cur.execute(sql)
        for division in division_cur:
            payload = {"year": division[0], "divisionId": division[1]}
            url = 'https://beta.rydellvision.com:8888/chrome/models'
            json_data = requests.post(url, data=payload)
            with pg_con.cursor() as table_cur:
                sql = """
                    insert into jon.get_models_by_division(model_year,division_id,models)
                    values ({0},{1},'{2}');
                """.format(division[0], division[1], json_data.text)
                table_cur.execute(sql)
    with pg_con.cursor() as models_cur:
        models_cur.execute("select jon.xfm_models()")

# styles
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as get_styles_cur:
        get_styles_cur.execute("truncate jon.get_styles")
    with pg_con.cursor() as model_cur:
        model_sql = """
            select model_id
            from jon.models;
        """
        model_cur.execute(model_sql)
        for model_id in model_cur:
            url = 'https://beta.rydellvision.com:8888/chrome/styles/' + str(model_id[0])
            data = requests.get(url)
            with pg_con.cursor() as table_cur:
                sql = """
                    insert into jon.get_styles(model_id,styles)
                    values ({0},'{1}');
                """.format(model_id[0], data.text)
                table_cur.execute(sql)
        pg_con.commit()
    with pg_con.cursor() as styles_cur:
        styles_cur.execute("select jon.xfm_styles()")


# describe vehicle by style id with ShowAvailableEquipment
with db_cnx.pg() as pg_con:
    # select *
    # from arkona.xfm_inpmast a
    with pg_con.cursor() as style_cur:
        style_sql = """
            select chrome_style_id
            from jon.styles;
        """
        style_cur.execute(style_sql)
        for stlye_id in style_cur:
            url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-show-available/' + str(stlye_id[0])
            response = requests.get(url)
            response = response.text.replace("'", "''")
            with pg_con.cursor() as table_cur:
                sql = """
                    insert into jon.get_describe_vehicle_by_style_id(chrome_style_id,response)
                    values ('{0}','{1}');
                """.format(stlye_id[0], response)
                table_cur.execute(sql)
    with pg_con.cursor() as cleanup_cur:
        cleanup_cur.execute("select jon.remove_unsuccessful_requests()")


