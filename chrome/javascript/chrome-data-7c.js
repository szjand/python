'use strict';
const tags = ['api','api-vendors/chrome-data']
const wsdl = 'https://services.chromedata.com/Description/7c?wsdl';
const soap = require('soap');
const args = {
  "accountInfo": {
    "attributes": {
      "number": "265491",
      "secret": "24a63940ed48890b",
      "country": "US",
      "language": "en",
    }
  },
};

exports.plugin = {
  name: 'chrome-data',
  register: function(server, options) {

    server.route([
      {
        method: 'GET',
        path: '/model-years',
        options: {
          tags: tags,
          description: 'Get all model years',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/model-years
            */
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl,  function(err, client) {

                client.getModelYears(args, function(err, result) {
                  // reply(result.modelYear)
                  console.log(err)
                  if (result.modelYear) {
                    resolve(result.modelYear)
                  } else {
                    resolve(result)
                  }
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/category-definitions',
        options: {
          tags: tags,
          description: 'Get all category definitions',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/category-definitions
            */
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.getCategoryDefinitions(args, function(err, result) {

                  if (result.category) {

                    resolve(result.category)
                  } else {
                    resolve(result)
                  }
                  // reply(result.category)
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/styles/{model}',
        options: {
          tags: tags,
          description: 'Get styles based off of model ID',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/styles/30289
            */
            let model = request.params.model
            args.attributes = {
              "modelId": model
            }
            console.log(args)
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.getStyles(args, function(err, result) {
                  delete args.attributes
                  if (result.style) {

                    resolve(result.style)
                  } else {
                    resolve(result)
                  }
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'POST',
        path: '/models',
        options: {
          tags: tags,
          description: 'Get models based off of a year and a division id (see /divisions)',
          handler: function(request, h) {
            let model = request.payload
            /*
            ---------- Example Payload
            {
            year: 2018,
            divisionId: 8
            }
            */
            args.modelYear = model.year;
            args.divisionId = model.divisionId;
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.getModels(args, function(err, result) {

                  // reply(result.model)
                  delete args.modelYear
                  delete args.divisionId
                  if (result.model) {

                    resolve(result.model)
                  } else {
                    resolve(result)
                  }
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'POST',
        path: '/models-by-subdivision',
        options: {
          tags: tags,
          description: 'Get models based off of a year and a subdivision id',
          handler: function(request, h) {
            let model = request.payload
            /*
            ---------- Example Payload
            {
            year: 2018,
            subdivisionId: 9008
            }
            */
            args.modelYear = model.year;
            args.subdivisionId = model.subdivisionId;
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.getModels(args, function(err, result) {

                  // reply(result.model)
                  delete args.modelYear
                  delete args.subdivisionId
                  if (result.model) {

                    resolve(result.model)
                  } else {
                    resolve(result)
                  }
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/technical-specs',
        options: {
          tags: tags,
          description: 'Get all the technical specs',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/technical-specs
            */
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.getTechnicalSpecificationDefinitions(args, function(err, result) {
                  console.log(err)
                  if (result.definition) {

                    resolve(result.definition)
                  } else {
                    resolve(result)
                  }
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/subdivisions/{year}',
        options: {
          tags: tags,
          description: 'Get all subdivisions based off of year',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/subdivisions/2018
            */
            let year = request.params.year
            args.attributes = {
              "modelYear": year
            }
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.getSubdivisions(args, function(err, result) {
                  delete args.attributes
                  if (result.subdivision) {

                    resolve(result.subdivision)
                  } else {
                    resolve(result)
                  }
                  args.attributes = {
                    "showAvailableEquipment": true
                  }
                });
              });
            });
            return r
           }
        }
      },
      {
        method: 'GET',
        path: '/divisions/{year}',
        options: {
          tags: tags,
          description: 'Get a vin description.',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/divisions/2018
            */
            let year = request.params.year
            args.attributes = {
              "modelYear": year
            }
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.getDivisions(args, function(err, result) {

                  // reply(result.division)
                  // console.log(err)
                  delete args.attributes
                  if (result.division) {

                    resolve(result.division)
                  } else {
                    resolve(result)
                  }
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/describe-vehicle/{vin}',
        options: {
          tags: tags,
          description: 'Get all divisions based off of year',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/describe-vehicle/1GCVKREC3JZ128186
            */
            let vin = request.params.vin

            args.vin = vin
            // args.styleId = 384643
            // args.switch = ["ShowAvailableEquipment"]
            // args.OEMOptionCode = ["AAQ", "AL9", "ATG", "AT9", "A28", "BTV", "BWN", "BW5", "B30", "B34", "B35", "CTT", "C49", "C5J", "C68", "DD8", "DG6", "D75",
            //   "E63", "FE9", "GBA", "GU6", "G80", "HH1", "IO5", "KA1", "K05", "K34", "LGZ", "M5T", "NQ6", "NY7", "PPA", "QHE", "R28", "S1K", "T3C", "T3U", "UDD",
            //   "UE1", "UQA", "UTJ", "UVC", "U2K", "VJQ", "VK3", "VT5", "VV4", "W1Y", "ZJJ", "Z71", "Z82", "4Z7", "5GD", "9B7"
            // ]

            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.describeVehicle(args, function(err, result) {

                  console.log(err)
                  delete args.vin
                  delete args.switch
                  // delete args.OEMOptionCode
                  resolve(result)
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/describe-vehicle-equip-tech-specs/{vin}',
        options: {
          tags: tags,
          description: 'Describe vehicle with technical specs and available equip',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/describe-vehicle-equip-tech-specs/1GCVKREC3JZ128186
            */
            let vin = request.params.vin

            args.vin = vin
            args.switch = ["ShowExtendedTechnicalSpecifications", "ShowAvailableEquipment"]

            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.describeVehicle(args, function(err, result) {

                  console.log(err)
                  delete args.vin
                  delete args.switch
                  // delete args.OEMOptionCode
                  resolve(result)
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/describe-vehicle-options/{vin}',
        options: {
          tags: tags,
          description: 'Get a vin description and options',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/describe-vehicle/1GCVKREC3JZ128186
            */
            let vin = request.params.vin

            args.vin = vin
            // args.styleId = 384643
            args.switch = ["ShowAvailableEquipment"]
            // args.OEMOptionCode = ["AAQ", "AL9", "ATG", "AT9", "A28", "BTV", "BWN", "BW5", "B30", "B34", "B35", "CTT", "C49", "C5J", "C68", "DD8", "DG6", "D75",
            //   "E63", "FE9", "GBA", "GU6", "G80", "HH1", "IO5", "KA1", "K05", "K34", "LGZ", "M5T", "NQ6", "NY7", "PPA", "QHE", "R28", "S1K", "T3C", "T3U", "UDD",
            //   "UE1", "UQA", "UTJ", "UVC", "U2K", "VJQ", "VK3", "VT5", "VV4", "W1Y", "ZJJ", "Z71", "Z82", "4Z7", "5GD", "9B7"
            // ]


            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {

                client.describeVehicle(args, function(err, result) {

                  console.log(err)
                  delete args.vin
                  delete args.switch
                  // delete args.OEMOptionCode
                  resolve(result)
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/describe-vehicle-show-available/{styleId}',
        options: {
          tags: tags,
          description: 'Get a chrome style id description and options',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/describe-vehicle/406768
            */
            let styleId = request.params.styleId
            args.styleId = styleId
            args.switch = ["ShowAvailableEquipment"]

            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {
                client.describeVehicle(args, function(err, result) {
                  console.log(err)
                  delete args.styleId
                  delete args.switch
                  resolve(result)
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/describe-vehicle-build-data/{vin}',
        options: {
          tags: tags,
          description: 'Automotive Description Service - Full with Builddata',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/describe-vehicle-build-data/1GCVKREC3JZ128186
            */
            const buildArgs = {
              "accountInfo": {
                "attributes": {
                  "number": "306422",
                  "secret": "aa118f7cce54469d",
                  "country": "US",
                  "language": "en",
                }
              },
            };
            let vin = request.params.vin
            buildArgs.vin = vin
            buildArgs.switch = ["IncludeOnlyOEMBuildDataDecode"]
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {
                client.describeVehicle(buildArgs, function(err, result) {
                  if (err){
                    resolve (err)
                  }
                  console.log(err)
                  delete buildArgs.vin
                  delete buildArgs.switch
                  resolve(result)
                });
              });
            });
            return r
          }
        }
      },
      {
        method: 'GET',
        path: '/version-info',
        options: {
          tags: tags,
          description: 'Get the data version information',
          handler: function(request, h) {
            /*
            example: https://beta.rydellvision.com:8888/chrome/version-info
            */
            const r = new Promise(function(resolve, reject) {
              soap.createClient(wsdl, function(err, client) {
                client.getVersionInfo(args, function(err, result) {
                  console.log(err)
                  if (result.definition) {
                    resolve(result.definition)
                  } else {
                    resolve(result)
                  }
                });
              });
            });
            return r
          }
        }
      }
    ])
  }
}