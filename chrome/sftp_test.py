import pysftp
import datetime as dt
import shutil
import os

"""
https://appdividend.com/2022/01/30/python-sftp/

with some help on configuring the crontab command from https://stackoverflow.com/questions/3287038/cron-and-virtualenv
"""
myHostname = "sftp.cartiva.com"
myUsername = "aftontester"
myPassword = "PcbQp^cFU26P"

with pysftp.Connection(host=myHostname, username=myUsername, password=myPassword) as sftp:
    print("Connection succesfully stablished ... ")

    src_dir = '/mnt/hgfs/D/'
    dest_dir = '/mnt/hgfs/D/afton_files/'

    src_file = os.path.join(src_dir, 'jon_test.pdf')
    shutil.copy(src_file, dest_dir)  # copy the file to destination dir

    dest_file = os.path.join(dest_dir, 'jon_test.pdf')
    print('dest_file: ' + dest_file)
    # new_dest_file = os.path.join(dest_dir,'jon' + str(dt.datetime.now()).replace(" ", "") + '.pdf')
    new_dest_file = os.path.join(dest_dir, 'jon_test_' + str(dt.datetime.now().hour) + '.pdf')
    print('new_dest_file: ' +  new_dest_file)
    os.rename(dest_file, new_dest_file)  # rename

    # Define a file that you want to upload from your local directorty
    localFilePath = new_dest_file
    print(localFilePath)

    # Define the remote path where the file will be uploaded
    remoteFilePath = '/dip-books/uploads/' + 'jon_test_' + str(dt.datetime.now().hour) + '.pdf'

    # Use put method to upload a file
    sftp.put(localFilePath, remoteFilePath)

    print('*** Contratulations ***')

# connection closed automatically at the end of the with statement