#!/usr/bin/python
import sys
import csv
import pypyodbc
import psycopg2
import json

def main():
    file_name = 'ext_test.csv'
    # store = sys.argv[1]
    # unformattedInvoces = sys.argv[2]
    # invoices = sys.argv[1]
    # unformattedInvoces = '15489582,15489573,15489570,15489569,15489563'
    invoices = ('15489582')
    with pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver 64-bit};system=REPORT1.ARKONA.COM;uid=rydejon;pwd=fuckyou5') as cn:
        with cn.cursor() as cur:
            sql = """
        with
         total as (
           select sum(ptqty) as the_sum, trim(ptinv#) as ptinv#
           from rydedata.pdptdet
           where trim(ptinv#) in ('15489582')
        group by trim(ptinv#)),
         n(n) as (
           select 1 from sysibm.sysdummy1
           union all
           select n+1 from n where n < 1000),
         freight_out as (
           select trim(ptpart) as ptpart ,  trim(ptinv#) as ptinv#
           from rydedata.pdptdet
           where trim(ptinv#) in ('15489582')
             and ptline = 999
             and ptseq# = 0)
        select aa.invoice_number, aa.sort_name, aa.purchase_order_, aa.ptpart,
         row_number() over (partition by aa.invoice_number order by aa.bin_location, aa.ptpart) as item_number,
         (select the_sum from total gg where invoice_number = gg.ptinv#) as total_items,
         (select ptpart from freight_out gg where invoice_number = gg.ptinv#) -- , aa.bin_location
        from (
         select trim(a.invoice_number) as invoice_number, trim(a.sort_name) as sort_name, trim(a.purchase_order_) as purchase_order_,
        trim(ptseq#) as ptseq#, trim(b.ptpart) as ptpart, trim(ptqty) as ptqty, c.bin_location
         from rydedata.pdpthdr a
         inner join rydedata.pdptdet b on a.company_number = b.ptco#
           and trim(a.invoice_number) = trim(b.ptinv#)
         left join rydedata.pdpmast c on trim(b.ptpart) = trim(c.part_number)
           and a.company_number = c.company_number
         where trim(a.invoice_number) in ('15489582')) aa
        cross join n bb
        where bb.n <= aa.ptqty
        order by aa.bin_location, aa.ptpart
        """

            # """.format(invoices)
            cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(cur)
    with psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'") as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.xfm_invoices")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.xfm_invoices from stdin with csv encoding 'latin-1 '""", io)
            sql = "select * from  arkona.get_parts_labels('%s')" % '15489582'
            pg_cur.execute(sql)
            result = pg_cur.fetchall()
            print(json.dumps(result))


if __name__ == '__main__':
    main()