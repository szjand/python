# encoding=utf-8
"""

"""
import csv
import db_cnx

kbb_map_file = '/mnt/hgfs/E/python_projects/chrome/build_data_trial_202105/docs/Mapping_Table-KBB_Used/VehicleMap.txt'
kbb_map_table = 'chr.kbb_vehicle_map'
kbb_option_file = ('/mnt/hgfs/E/python_projects/chrome/build_data_trial_202105/docs/Mapping_Table-KBB_Used/'
                   'RequiredOptions.txt')
kbb_option_table = 'chr.kbb_required_options'
bb_map_file = ('/mnt/hgfs/E/python_projects/chrome/build_data_trial_202105/docs/Mapping_Table-US_Black_Book/'
               'vehiclemap.txt')
bb_map_table = 'chr.bb_vehicle_map'
bb_option_file = ('/mnt/hgfs/E/python_projects/chrome/build_data_trial_202105/docs/Mapping_Table-US_Black_Book/'
                  'requiredoptions.txt')
bb_option_table = 'chr.bb_required_options'


def parse_files(file_name, table_name):
    with open(file_name, 'r', encoding='latin_1', newline='') as infile, open(
            file_name + '_clean', 'w') as outfile:
        reader = csv.reader(infile, delimiter=",", quotechar="~")
        writer = csv.writer(outfile)
        for row in reader:
            writer.writerow(row)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            with open(file_name + '_clean', 'r', encoding='latin_1', newline='') as the_file:
                pg_cur.copy_expert("copy " + table_name + " from stdin with csv header encoding 'latin-1'", the_file)


if __name__ == '__main__':
    parse_files(bb_option_file, bb_option_table)
    # print(get_chrome_by_style('406972'))
