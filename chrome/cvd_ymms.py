import db_cnx
import requests
from datetime import datetime
"""

"""
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        # new cars
        sql = """
            select vin
            from (
                select *
                from nc.vehicle_acquisitions
                where stock_number like 'G%' 
                order by ground_date desc
                limit 20) a
            union (
                select vin
                from nc.vehicle_acquisitions
                where stock_number like 'H%'
                order by ground_date desc
                limit 20) 
            union (
                select vin
                from nc.vehicle_acquisitions
                where stock_number like 'T%'
                order by ground_date desc
                limit 20)   
         """
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin = row[0]
            the_date = datetime.today().strftime('%m/%d/%Y')
            print(vin)
            url = 'https://beta.rydellvision.com:8888/cvd/vin-descriptions/' + vin
            data = requests.get(url)
            resp = data.text
            resp = resp.replace("'", "''")
            with pg_con.cursor() as chr_cur:
                sql = """
                    insert into cvd.nc_vin_descriptions(vin,response,the_date)
                    values ('{0}', '{1}', '{2}');
                """.format(vin, resp, the_date)
                chr_cur.execute(sql)

                sql = """
                    update cvd.nc_vin_descriptions a
                    set source = b.source,
                        style_count = b.style_count
                    from (   
                        select x.vin, x.response->'result'->>'source' as source,
                            jsonb_array_length(x.response->'result'->'vehicles') as style_count
                        from cvd.nc_vin_descriptions x
                        where vin = '{0}') b
                    where a.vin = b.vin;
                """.format(vin)
                chr_cur.execute(sql)
                pg_con.commit()
