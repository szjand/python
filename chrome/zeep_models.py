
import zeep
from zeep import Client
from zeep import helpers
import json
import utilities

pg_server = '173'


def get_models():
    # authentication = {"number": '265491',
    #                   "secret": '24a63940ed48890b',
    #                   "country": 'US',
    #                   "language": 'en'}
    # client = Client("http://services.chromedata.com/Description/7b?wsdl")

    # # non build data
    # authentication = {"number": '265491',
    #                   "secret": '24a63940ed48890b',
    #                   "country": 'US',
    #                   "language": 'en'}
    # build data, works just the same
    authentication = {"number": '306422',
                      "secret": 'aa118f7cce54469d',
                      "country": 'US',
                      "language": 'en'}

    client = Client("http://services.chromedata.com/Description/7c?wsdl")

    # with utilities.pg(pg_server) as pg_con:
    #     with pg_con.cursor() as get_models_cur:
    #         get_models_cur.execute("truncate chr2.get_models")
    #     with pg_con.cursor() as model_cur:
    #         sql = """
    #             select model_year, division_id
    #             from chr2.divisions
    #             where division in('Hummer','Isuzu','Mercury','Pontiac','Saab','Saturn','Scion','Suzuki')
    #               and model_year between 2005 and 2022;
    #         """
    #         model_cur.execute(sql)
    #         for model in model_cur:
    # payload = {"year":  model[0], "model": model[1]}
    # print(payload)

    ## this worked
    # response = client.service.getModels(accountInfo=authentication, modelYear=2010,subdivisionId=6219)

    # this failed:
    # TypeError: {urn:description7c.services.chrome.com}ModelsRequest() got an unexpected keyword argument 'DivisionId'
    # error was due to case sensitivity, this now passes
    # try it with build data credentials
    response = client.service.getModels(accountInfo=authentication, modelYear=2023, divisionId=7)


    my_dict = zeep.helpers.serialize_object(response)
    response = json.dumps(my_dict)
    return response


if __name__ == '__main__':
    print(get_models())


