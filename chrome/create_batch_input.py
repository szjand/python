# encoding=utf-8
"""
20201129_request_1.csv
    try it first with one 2 style_ids and no tilde delimiter
    all header fields, no tilde, single styleid 415138, 415139, with styleid as passthru_id
    failed
    The batch process could not be completed.
    Result code:  6
    Message:  An error was found in the header line of the input file.
    Time stamp:  Sun Nov 29 12:56:07 PST 2020
    so lets put the tildes in
20201129_request_2.csv
    failed with header error again, shit, i did not do a line feed after the header, need to look at the file
    more closely before sending it up
20201129_request_3.csv
    looks good, styleid as passthru seems ok
20201129_request_4.csv
    662 rows, model year 2021 only, uploaded at 3:32, lets see how log the results take, had the results in 4 minutes !!
20201129_request_5.csv
    2515 rows, model years 2018 - 2020, uploaded 3:45, done at 3:49, 9,731,122 bytes of data
    1 error
"""
import csv
import db_cnx
import ftplib

upload_file_path = 'batch_python/requests/'
upload_file_name = '20201203_request_1.csv'


def generate_file():
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select d.chrome_style_id,null,null,d.chrome_style_id,null,null,null,null,null,null,null,null,
                    null,null,null,null,null,null,null,null
                from jon.model_years a
                join jon.divisions b on a.model_year = b.model_year
                  and b.division in ('chevrolet','buick','gmc','cadillac','honda','nissan')
                join jon.models c on a.model_year = c.model_year
                  and b.division_id = c.division_id
                  and c.model not like '4500%'
                  and c.model not like '5500%'
                  and c.model not like '6500%'
                  and c.model not like '%LCF%'
                join jon.styles d on c.model_id = d.model_id
                where a.model_year between 2018 and 2020 limit 3;          
            """
            pg_cur.execute(sql)
            # this works
            with open(upload_file_path + upload_file_name, 'w') as batch_request:
                # write a file header
                batch_request.write("passthru_id,vin,reducing_style_id,style_id,year,make,model,trim_name,"
                                    "manufacturer_model_code,wheelbase,exterior_color,interior_color,options,"
                                    "equipment,nonfactory_equipment,acode,reducing_acode,style_name,"
                                    "primary_option_names,include_technical_specification_title_id")
                batch_request.write("\n")
            # append the rows so as to not overwriter the header line
            with open(upload_file_path + upload_file_name, 'a') as batch_request:
                csv.writer(batch_request).writerows(pg_cur)

            # # and this works as well, is actually a little less fuzzy, no need to isert line feed
            # with open(upload_file_path + upload_file_name, 'w') as batch_request:
            #     csv.writer(batch_request).writerow(['passthru_id','vin','reducing_style_id','style_id',
            #                                         'year','make','model','trim_name','manufacturer_model_code',
            #                                         'wheelbase','exterior_color','interior_color','options','equipment',
            #                                         'nonfactory_equipment','acode','reducing_acode','style_name',
            #                                         'primary_option_names','include_technical_specification_title_id'])
            #     csv.writer(batch_request).writerows(pg_cur.fetchall())


def push_file():
    server = 'ftp.chromedata.com'
    username = 'rc265491_7'
    password = 'car491'
    ftp = ftplib.FTP(server)
    ftp.login(username, password)
    ftp.cwd('In')
    ftp.storbinary('STOR ' + upload_file_name, open(upload_file_path + upload_file_name, 'rb'))
    ftp.quit()


# def GetFile():
#     file_name = '20201129_request_1.csv'
#     server = 'ftp.chromedata.com'
#     username = 'rc265491_7'
#     password = 'car491'
#     ftp = ftplib.FTP(server)
#     ftp.login(username, password)
#     ftp.cwd('Out')


def main():
    generate_file()
    push_file()


if __name__ == '__main__':
    main()
