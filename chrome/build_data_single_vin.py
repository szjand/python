import requests
"""
"""
# vin = '3GTU9DEL4LG314302'  # 2020 sierra
# vin = '1GKFK16367J216066' # 2007 yukon
# vin = '5FNRL6H76LB008290'  # 2020 honda odyssey  BuildDataNotAvailable
# vin = '5N1AZ2BS6LN113071'  # 2020 nissan muran
# vin = '19XFC2F69LE010286'  # 2020 honda civic BuildDataNotAvailable
# vin = '2C4RDGCG2HR813371' # 2017 dodge caravan
# vin = '1FTFW1RG4LFA17674' # 2020 Ford F-150  BuildDataNotAvailable
# vin = '5YFEPRAE8LP005585' # 2020 toyota corolla  BuildDataNotAvailable
vin = '1GCUYGED7LZ177448'
url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-build-data/' + vin
data = requests.get(url)
resp = data.text
resp = resp.replace("'", "''")
print(resp)

