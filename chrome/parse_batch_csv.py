# encoding=utf-8
"""
clean up messy csv files from batch request with delimiter and quotechar
attribute chrome_code, styleid 406524, in the txt file is -PAINT, which makes no sense
    and of course excel sees it as =-PAINT which resolves as #NAME?, shouldn't be
    an issue parsing into db, but, be aware

technical_specificaiton_definition.csv fails with:
    UnicodeDecodeError: 'utf-8' codec can't decode byte 0xb3 in position 728: invalid start byte
    found the problem with unicode_errors.py
    try running it with latin_1 encoding, perfect!
"""
import csv

with open('batch_python/sandbox/tech_spec_value.csv', 'r',
          encoding='latin_1', newline='') as infile, open(
        'batch_python/sandbox/tech_spec_value_python.csv', 'w') as outfile:
    reader = csv.reader(infile, delimiter=",", quotechar="~")
    writer = csv.writer(outfile)
    for row in reader:
        writer.writerow(row)
