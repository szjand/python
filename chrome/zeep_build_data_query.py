# encoding=utf-8
"""
06/01/21
    ALL Technical Documentation & Code Samples
        http://update.chromedata.com/
        User name: 265491
        Password:  car491
        Access to Data Products

    MAPPING TABLES (KBB & BLACK BOOK)
        ftp.chromedata.com
        User Name: u265491
        Password: car491

    Access to Chrome Web Services
    AUTOMOTIVE DESCRIPTION SERVICE – FULL (WITH BUILDDATA)
        Account Number: 306422
        Account Secret: aa118f7cce54469d
    BATCH PROCESSING: Automotive Description Service
    AUTOMOTIVE DESCRIPTION SERVICE- FULL (WITH BUILDDATA)
        ftp.chromedata.com
        User Name: rc306422_7
        Password: car422
08/27/21
    finally, we have GM build data
09/07/21
    remove the writing to file
    add updating style_count & source
"""
from datetime import datetime
import zeep
from zeep import Client
from zeep import helpers
import json
import db_cnx

with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        # # equinoxen
        # sql = """
        #     select distinct vin
        #     from pp.equinox_mf_step_2 a
        #     where not exists (
        #       select 1
        #       from chr.build_data_describe_vehicle
        #       where vin = a.vin)
        # """
        # # competitor's pricing
        # sql = """
        #     select distinct a.vin
        #     from pp.uc_today a
        #     where not exists (
        #       select 1
        #       from chr.build_data_describe_vehicle
        #       where vin = a.vin)
        # """
        # # our uc inventory
        # sql = """
        #     select distinct b.vin
        #     from ads.ext_vehicle_inventory_items a
        #     join ads.ext_Vehicle_items b on a.vehicleitemid = b.vehicleitemid
        #     where a.thruts::date > current_date
        #         and not exists (
        #             select 1
        #             from chr.build_data_describe_Vehicle
        #             where vin = b.vin )
        # """
        # # interest credits
        # sql = """
        #     select vin
        #     from gmgl.wfpm_monthly_report a
        #     where not exists (
        #         select *
        #         from chr.build_data_describe_vehicle
        #         where vin = a.vin);
        # """
        # misc individual vin for testing
        # sql = "select '1GT49RE78RF270251'"


        # ## edw
        # sql = """
        #     select distinct vin
        #     from edw.more_build_vins a
        #     where not exists (
        #       select 1
        #       from chr.build_data_describe_vehicle
        #       where vin = a.vin)
        # """

        # # canonical uc data vins #########################################
        # sql = """
        #     select distinct a.vin  -- 131 vins
        #     from cu.used_3 a
        #     join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
        #     join chr.build_data_coverage c on b.make = c.division
        #       and b.year between c.from_year and c.thru_year
        #     where not exists (
        #       select 1
        #       from cu.tmp_vehicles
        #       where vin = a.vin)
        #     and not exists (
        #       select 1
        #       from chr.build_data_describe_vehicle
        #       where vin = a.vin)
        # """

        # nc.stg_availability  ***********************************************
        sql = """
            select vin
            from nc.stg_availability a
            where make not in ('HONDA','TOYOTA')
              and not exists (
                select 1
                from chr.build_data_describe_vehicle
                where vin = a.vin);
        """


        # ## edw 2 making up for all those that already exist in chr.describe_vehicle
        # sql = """
        #     select distinct a.vin -- 4301
        #     from edw.vehicle_describe_vehicle a
        #     join chr.build_data_coverage b on a.make = b.division
        #       and a.model_year between b.from_year and b.thru_year
        #     where not exists (
        #       select 1
        #       from chr.build_data_descrhunnibe_vehicle
        #       where vin = a.vin)
        # """


        # # competitor vins
        # sql = """
        #     select distinct vin
        #     from (
        #         select distinct bb.vin  --1384
        #         from (
        #             select a.vin, -- 346
        #                 (r.style ->>'id')::citext as chr_style_id,
        #                 (r.style->>'modelYear')::integer as model_year,
        #                 (r.style ->'division'->>'_value_1')::citext as make,
        #                 (r.style ->'model'->>'_value_1'::citext)::citext as model
        #             from pp.evals_1 a
        #             join chr.build_data_describe_vehicle b on  a.vin = b.vin
        #                 and b.source = 'build'
        #                 and b.style_count = 1
        #             join jsonb_array_elements(b.response->'style') as r(style) on true) aa
        #         join  scrapes.carsdotcom_daily bb on aa.model_year::citext = bb.model_year
        #             and aa.make = bb.make
        #             and aa.model = bb.model
        #         union
        #         select distinct bb.vin  -- 670
        #         from (
        #             select a.vin, -- 346
        #                 (r.style ->>'id')::citext as chr_style_id,
        #                 (r.style->>'modelYear')::integer as model_year,
        #                 (r.style ->'division'->>'_value_1')::citext as make,
        #                 (r.style ->'model'->>'_value_1'::citext)::citext as model
        #             from pp.evals_1 a
        #             join chr.build_data_describe_vehicle b on  a.vin = b.vin
        #                 and b.source = 'build'
        #                 and b.style_count = 1
        #             join jsonb_array_elements(b.response->'style') as r(style) on true) aa
        #         join  scrapes.cargurus_dealers_daily bb on aa.model_year::citext = bb.model_year
        #             and aa.make = bb.make
        #             and aa.model = bb.model
        #         union
        #         select distinct bb.vin  -- 180
        #         from (
        #             select a.vin, -- 346
        #                 (r.style ->>'id')::citext as chr_style_id,
        #                 (r.style->>'modelYear')::integer as model_year,
        #                 (r.style ->'division'->>'_value_1')::citext as make,
        #                 (r.style ->'model'->>'_value_1'::citext)::citext as model
        #             from pp.evals_1 a
        #             join chr.build_data_describe_vehicle b on  a.vin = b.vin
        #                 and b.source = 'build'
        #                 and b.style_count = 1
        #             join jsonb_array_elements(b.response->'style') as r(style) on true) aa
        #         join  scrapes.autotrader_daily bb on aa.model_year::citext = bb.model_year
        #             and aa.make = bb.make
        #             and aa.model = bb.model) x
        #     where not exists (
        #       select 1
        #       from chr.build_data_describe_vehicle
        #       where vin = x.vin)
        # """
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            the_vin = row[0]
            print(the_vin)
            the_date = datetime.today().strftime('%m/%d/%Y')
            json_directory = '/mnt/hgfs/E/python_projects/chrome/json_zeep_build_data/'
            authentication = {"number": '306422',
                              "secret": 'aa118f7cce54469d',
                              "country": 'US',
                              "language": 'en'}
            client = Client("http://services.chromedata.com/Description/7c?wsdl")
            response = client.service.describeVehicle(accountInfo=authentication, vin=the_vin,
                                                      switch=['ShowExtendedTechnicalSpecifications', 'ShowAvailableEquipment'])
            my_dict = zeep.helpers.serialize_object(response, dict)
            the_response = json.dumps(my_dict, default=str)
            # with open(json_directory + the_vin + '.json', 'w', encoding='utf-8') as f:
            #     # json.dump used for writing to json file
            #     # 2021 nissan threw: TypeError: Object of type 'datetime' is not JSON serializable
            #     # added default=str
            #     json.dump(my_dict, f, default=str)
            the_response = the_response.replace("'", "''")
            with db_cnx.pg() as pg_con:
                with pg_con.cursor() as pg_cur:
                    sql = """
                        insert into chr.build_data_describe_vehicle(vin,response,the_date)
                        values ('{0}', '{1}', '{2}');
                    """.format(the_vin, the_response, the_date)
                    pg_cur.execute(sql)

                    sql = """
                        update chr.build_data_describe_vehicle y
                        set style_count = x.style_count, source = x.source
                        from (
                            select vin, jsonb_array_length(response->'style') as style_count,  
                                response->'vinDescription'->>'source' as source
                            from chr.build_data_describe_vehicle	
                            where vin = '{0}') x
                        where x.vin = y.vin;  
                    """.format(the_vin)
                    pg_cur.execute(sql)



