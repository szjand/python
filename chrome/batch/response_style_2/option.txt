~input_row_num~,~passthru_id~,~ambiguous_option_id~,~header_id~,~header_name~,~descriptions~,~added_cat_ids~,~removed_cat_ids~,~msrp_low~,~msrp_high~,~invoice_low~,~invoice_high~,~is_unknown_price~,~avail_style_ids~,~install_cause~,~install_cause_detail~,~chrome_code~,~oem_code~,~alt_optioncode~,~is_standard~,~optionkind_id~,~utf~,~is_fleet_only~
~1~,,,~1156~,~EMISSIONS~,~EMISSIONS, FEDERAL REQUIREMENTS~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~FE9~,~FE9~,,~false~,~5~,~5~,~false~
~1~,,,~1156~,~EMISSIONS~,~EMISSIONS, CALIFORNIA STATE REQUIREMENTS~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~YF5~,~YF5~,,~false~,~5~,~5~,~false~
~1~,,,~1156~,~EMISSIONS~,~EMISSIONS, CONNECTICUT, DELAWARE, MAINE, MARYLAND, MASSACHUSETTS, NEW JERSEY, NEW YORK, OREGON, PENNSYLVANIA, RHODE ISLAND, VERMONT AND WASHINGTON STATE REQUIREMENTS~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~NE1~,~NE1~,,~false~,~5~,~5~,~false~
~1~,,,~1156~,~EMISSIONS~,~EMISSIONS OVERRIDE, CALIFORNIA~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~NB8~,~NB8~,,~false~,,~6~,~false~
~1~,,,~1156~,~EMISSIONS~,~EMISSIONS OVERRIDE, STATE-SPECIFIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~NB9~,~NB9~,,~false~,,~6~,~false~
~1~,,,~1156~,~EMISSIONS~,~EMISSIONS OVERRIDE, FEDERAL~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~NC7~,~NC7~,,~false~,,~6~,~false~
~1~,,,~1160~,~ENGINE~,~ENGINE, ECOTEC TURBO 1.4L VARIABLE VALVE TIMING DOHC 4-CYLINDER SEQUENTIAL MFI~,~1048;1054;1059~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,~Engine~,,~LUV~,~LUV~,,~true~,~6~,~0~,~false~
~1~,,,~1379~,~TRANSMISSION~,~TRANSMISSION, 6-SPEED AUTOMATIC~,~1104;1130~,~1131~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~MNK~,~MNK~,,~true~,~7~,~A~,~false~
~1~,,,~1108~,~AXLE~,~AXLE, 3.53 FINAL DRIVE RATIO~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~FXH~,~FXH~,,~false~,~8~,~C~,~false~
~1~,,,~1292~,~PREFERRED EQUIPMENT GROUP~,~LT PREFERRED EQUIPMENT GROUP~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~1LT~,~1LT~,,~false~,~16~,~B~,~false~
~1~,,,~1397~,~WHEELS~,~WHEELS, 16" (40.6 CM) ALUMINUM~,~1123~,~1124~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~RRZ~,~RRZ~,,~true~,~41~,~I~,~false~
~1~,,,~1397~,~WHEELS~,~WHEELS, 18" (45.7 CM) BLACK FINISH ALUMINUM WITH RED ACCENT STRIPES~,~1123~,~1124;1208~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~REN~,~REN~,,~false~,~41~,~I~,~false~
~1~,,,~1397~,~WHEELS~,~WHEELS, 18" (45.7 CM) GLOSS BLACK ALUMINUM~,~1123~,~1124;1208~,~0.0~,~0.0~,~0.0~,~0.0~,~true~,~406524~,,,~RSZ~,~RSZ~,,~false~,~41~,~I~,~false~
~1~,,,~1397~,~WHEELS~,~WHEELS, 18" (45.7 CM) MIDNIGHT SILVER ALUMINUM~,~1092;1097;1123~,~1124;1208~,~595.0~,~595.0~,~541.45~,~541.45~,~false~,~406524~,,,~RRT~,~RRT~,,~false~,~41~,~I~,~false~
~1~,,,~1374~,~TIRES~,~TIRES, P205/70R16 ALL-SEASON, BLACKWALL~,~1088;1093~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~RAJ~,~RAJ~,,~false~,~12~,~J~,~false~
~1~,,,~1374~,~TIRES~,~TIRES, P215/55R18 ALL-SEASON, BLACKWALL~,~1092;1097~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~RAK~,~RAK~,,~false~,~12~,~J~,~false~
~1~,,,~1256~,~PAINT~,~NIGHTFALL GRAY METALLIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~G7Q~,~G7Q~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~SATIN STEEL METALLIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~G9K~,~G9K~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~SILVER ICE METALLIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~GAN~,~GAN~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~RED HOT~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~GG2~,~GG2~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~BLACK CHERRY METALLIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~GNL~,~GNL~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~STONE GRAY METALLIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~GNM~,~GNM~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~SUMMIT WHITE~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~GAZ~,~GAZ~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~MOSAIC BLACK METALLIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~GB0~,~GB0~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~MOSAIC BLACK METALLIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~GB8~,~GB8~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~PACIFIC BLUE METALLIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~GUM~,~GUM~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~PACIFIC BLUE METALLIC~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~G6O~,~G6O~,,~false~,~68~,~D~,~false~
~1~,,,~1256~,~PAINT~,~CAJUN RED TINTCOAT~,~~,~~,~395.0~,~395.0~,~359.45~,~359.45~,~false~,~406524~,,,~GPJ~,~GPJ~,,~false~,~68~,~D~,~false~
~1~,,,~10630~,~PAINT SCHEME~,~STANDARD PAINT~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~-PAINT~,,,~false~,~29~,~0~,~false~
~1~,,,~1348~,~SEAT TYPE~,~SEATS, FRONT BUCKET WITH DRIVER POWER LUMBAR~,~1082;1189~,~1080;1081~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AR9~,~AR9~,,~true~,~27~,~0~,~false~
~1~,,,~1347~,~SEAT TRIM~,~JET BLACK, DELUXE CLOTH SEAT TRIM~,~1077~,~1078;1079;1309~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AFP~,~AFP~,,~false~,~28~,~E~,~false~
~1~,,,~1347~,~SEAT TRIM~,~JET BLACK/LIGHT ASH GRAY, DELUXE CLOTH SEAT TRIM~,~1077~,~1078;1079;1309~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AFQ~,~AFQ~,,~false~,~28~,~E~,~false~
~1~,,,~1347~,~SEAT TRIM~,~JET BLACK/BRANDY, DELUXE CLOTH SEAT TRIM~,~1077~,~1078;1079;1309~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AFR~,~AFR~,,~false~,~28~,~E~,~false~
~1~,,,~1347~,~SEAT TRIM~,~JET BLACK, DELUXE CLOTH/LEATHERETTE SEAT TRIM~,~1077;1309~,~1078;1079~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AES~,~AES~,,~false~,~28~,~E~,~false~
~1~,,,~1347~,~SEAT TRIM~,~JET BLACK/LIGHT ASH GRAY, DELUXE CLOTH/LEATHERETTE SEAT TRIM~,~1077;1309~,~1078;1079~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AET~,~AET~,,~false~,~28~,~E~,~false~
~1~,,,~1347~,~SEAT TRIM~,~JET BLACK/BRANDY, DELUXE CLOTH/LEATHERETTE SEAT TRIM~,~1077;1309~,~1078;1079~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AEV~,~AEV~,,~false~,~28~,~E~,~false~
~1~,,,~1347~,~SEAT TRIM~,~JET BLACK, LEATHERETTE SEAT TRIM~,~1309~,~1077;1078;1079~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AEW~,~AEW~,,~false~,~28~,~E~,~false~
~1~,,,~1301~,~RADIO~,~AUDIO SYSTEM, CHEVROLET INFOTAINMENT SYSTEM~,~1014;1150;1211;1230;1348~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~IOR~,~IOR~,,~true~,~33~,~0~,~false~
~1~,,,~10319~,~OPTION DISCOUNT~,~OPTION/PACKAGE DISCOUNT~,~~,~~,~-250.0~,~-250.0~,~-227.5~,~-227.5~,~false~,~406524~,,,~-RET1SZ~,,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SUN & SOUND PACKAGE~,~1069;1132;1136~,~~,~1400.0~,~1400.0~,~1274.0~,~1274.0~,~false~,~406524~,,,~PCJ~,~PCJ~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~REDLINE EDITION~,~1088;1093;1123~,~1124;1208~,~1350.0~,~1350.0~,~1228.5~,~1228.5~,~false~,~406524~,,,~WBL~,~WBL~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~MIDNIGHT EDITION~,~1092;1097;1123~,~1124;1208~,~0.0~,~0.0~,~0.0~,~0.0~,~true~,~406524~,,,~WJP~,~WJP~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~DRIVER CONFIDENCE PACKAGE~,~1180;1312;1322~,~~,~495.0~,~495.0~,~450.45~,~450.45~,~false~,~406524~,,,~PCW~,~PCW~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LT CONVENIENCE PACKAGE~,~1074;1077;1192;1229;1309~,~1078;1079~,~400.0~,~450.0~,~364.0~,~409.5~,~false~,~406524~,,,~PDB~,~PDB~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~PREMIUM SEAT PACKAGE~,~1156;1309~,~1077;1078;1079~,~650.0~,~650.0~,~591.5~,~591.5~,~false~,~406524~,,,~PCU~,~PCU~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, PROTECTION PACKAGE~,~1055~,~~,~295.0~,~295.0~,~268.45~,~268.45~,~false~,~406524~,,,~PDC~,~PDC~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, INTERIOR PROTECTION PACKAGE~,~1055~,~~,~190.0~,~190.0~,~172.9~,~172.9~,~false~,~406524~,,,~PDH~,~PDH~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, CARGO PACKAGE~,~~,~~,~125.0~,~125.0~,~113.75~,~113.75~,~false~,~406524~,,,~PDD~,~PDD~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~HEATER, OIL PAN~,~~,~~,~100.0~,~100.0~,~91.0~,~91.0~,~false~,~406524~,,,~KPK~,~KPK~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~KEYLESS START~,~1229~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~BTM~,~BTM~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, ACCESSORY CARRIER MOUNT~,~~,~~,~240.0~,~240.0~,~218.4~,~218.4~,~false~,~406524~,,,~SDD~,~SDD~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, WHEEL LOCK KIT~,~1205~,~~,~75.0~,~75.0~,~68.25~,~68.25~,~false~,~406524~,,,~SFE~,~SFE~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SUNROOF, POWER, SLIDING~,~1069;1132~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~CF5~,~CF5~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~GRILLE, BLACK WITH BLACK SURROUND~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~GGC~,~GGC~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, ROOF RACK CROSS RAILS, SET OF 2~,~1172~,~~,~265.0~,~265.0~,~241.15~,~241.15~,~false~,~406524~,,,~VLL~,~VLL~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, FRONT AND REAR SPLASH GUARDS, CUSTOM BLACK MOLDED~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~VQK~,~VQK~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ORNAMENTATION, TRAX BADGE WITH RED OUTLINE~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~CG6~,~CG6~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, BLACK BOWTIE~,~~,~~,~220.0~,~220.0~,~200.2~,~200.2~,~false~,~406524~,,,~SG1~,~SG1~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LICENSE PLATE BRACKET, FRONT~,~~,~~,~40.0~,~40.0~,~36.4~,~36.4~,~false~,~406524~,,,~VK3~,~VK3~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~DOOR HANDLES, BODY-COLOR WITH CHROME ACCENT~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~D7A~,~D7A~,,~false~,,~V~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~AUDIO SYSTEM FEATURE, BOSE PREMIUM 7-SPEAKER SYSTEM~,~1136~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~UQA~,~UQA~,,~false~,,~N~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SEAT TRIM, DELUXE CLOTH/LEATHERETTE~,~1077;1309~,~1078;1079~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~EGI~,~EGI~,,~false~,,~P~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SEAT TRIM, LEATHERETTE~,~1309~,~1077;1078;1079~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~ESN~,~ESN~,,~false~,,~P~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SEAT ADJUSTER, DRIVER 6-WAY POWER~,~1074~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AG9~,~AG9~,,~false~,,~Q~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SEATS, HEATED DRIVER AND FRONT PASSENGER~,~1156~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~KA1~,~KA1~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, ALL-WEATHER FLOOR MATS, FRONT AND REAR~,~1055~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~VAV~,~VAV~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, ALL-WEATHER FLOOR LINERS, FRONT AND REAR, BLACK~,~1055~,~~,~190.0~,~190.0~,~172.9~,~172.9~,~false~,~406524~,,,~S55~,~S55~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~STEERING WHEEL, LEATHER-WRAPPED 3-SPOKE~,~1192~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~N34~,~N34~,,~false~,,~R~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~KEYLESS OPEN~,~1062;1063~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~AVJ~,~AVJ~,,~false~,,~T~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~KEY SYSTEM, 2 SPARE KEYS~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~5H1~,~5H1~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, CARGO AREA ORGANIZER, COLLAPSIBLE~,~~,~~,~140.0~,~140.0~,~127.4~,~127.4~,~false~,~406524~,,,~RWU~,~RWU~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, CARGO MAT, CARPETED~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~VLI~,~VLI~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, CARGO NET~,~~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~W2D~,~W2D~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~LPO, CARGO TRAY~,~~,~~,~115.0~,~115.0~,~104.65~,~104.65~,~false~,~406524~,,,~CAV~,~CAV~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SIDE BLIND ZONE ALERT~,~1312~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~UFT~,~UFT~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~REAR CROSS-TRAFFIC ALERT~,~1322~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~UFG~,~UFG~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~REAR PARK ASSIST, SENSOR INDICATOR~,~1180~,~~,~0.0~,~0.0~,~0.0~,~0.0~,~false~,~406524~,,,~UD7~,~UD7~,,~false~,,~0~,~false~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 21 MONTHS OF ONSTAR FLEET SAFETY AND SECURITY.~,~1212~,~~,~0.0~,~0.0~,~420.0~,~420.0~,~false~,~406524~,,,~P0J~,~P0J~,,~false~,,~Z~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 33 MONTHS OF ONSTAR FLEET SAFETY AND SECURITY.~,~~,~~,~0.0~,~0.0~,~578.0~,~578.0~,~false~,~406524~,,,~P0K~,~P0K~,,~false~,,~Z~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 45 MONTHS OF ONSTAR FLEET SAFETY AND SECURITY.~,~1212~,~~,~0.0~,~0.0~,~698.0~,~698.0~,~false~,~406524~,,,~P0L~,~P0L~,,~false~,,~Z~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 21 MONTHS OF ONSTAR FLEET REMOTE ACCESS.~,~~,~~,~0.0~,~0.0~,~210.0~,~210.0~,~false~,~406524~,,,~P0M~,~P0M~,,~false~,,~U~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 33 MONTHS OF ONSTAR FLEET REMOTE ACCESS.~,~1212~,~~,~0.0~,~0.0~,~330.0~,~330.0~,~false~,~406524~,,,~P0N~,~P0N~,,~false~,,~U~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 45 MONTHS OF ONSTAR FLEET REMOTE ACCESS.~,~1212~,~~,~0.0~,~0.0~,~450.0~,~450.0~,~false~,~406524~,,,~P0O~,~P0O~,,~false~,,~U~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR VEHICLE INSIGHTS - 1 YEAR OF SERVICE.~,~1381~,~~,~0.0~,~0.0~,~183.0~,~183.0~,~false~,~406524~,,,~P0V~,~P0V~,,~false~,,~Y~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR VEHICLE INSIGHTS - 2 YEARS OF SERVICE.~,~1381~,~~,~0.0~,~0.0~,~360.0~,~360.0~,~false~,~406524~,,,~P0W~,~P0W~,,~false~,,~Y~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR VEHICLE INSIGHTS - 3 YEARS OF SERVICE.~,~1381~,~~,~0.0~,~0.0~,~522.0~,~522.0~,~false~,~406524~,,,~P0X~,~P0X~,,~false~,,~Y~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR VEHICLE INSIGHTS - 4 YEARS OF SERVICE.~,~1381~,~~,~0.0~,~0.0~,~672.0~,~672.0~,~false~,~406524~,,,~P0Y~,~P0Y~,,~false~,,~Y~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR VEHICLE INSIGHTS - 5 YEARS OF SERVICE.~,~1381~,~~,~0.0~,~0.0~,~810.0~,~810.0~,~false~,~406524~,,,~P0Z~,~P0Z~,,~false~,,~Y~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 9 MONTHS OF ONSTAR ASSURANCE FOR TIER3 FLEETS.~,~1212;1381~,~~,~0.0~,~0.0~,~90.0~,~90.0~,~false~,~406524~,,,~P1R~,~P1R~,,~false~,,~*~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 21 MONTHS OF ONSTAR ASSURANCE FOR TIER3 FLEETS.~,~1212;1381~,~~,~0.0~,~0.0~,~210.0~,~210.0~,~false~,~406524~,,,~P1S~,~P1S~,,~false~,,~*~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 33 MONTHS OF ONSTAR ONSTAR ASSURANCE FOR TIER3 FLEETS.~,~1212;1381~,~~,~0.0~,~0.0~,~330.0~,~330.0~,~false~,~406524~,,,~P1T~,~P1T~,,~false~,,~*~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~ONSTAR ADDITIONAL 45 MONTHS OF ONSTAR ASSURANCE FOR TIER3 FLEETS.~,~1212;1381~,~~,~0.0~,~0.0~,~450.0~,~450.0~,~false~,~406524~,,,~P1U~,~P1U~,,~false~,,~*~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~GM COMMERCIAL LINK - 1 YEAR OF SERVICE~,~~,~~,~120.0~,~120.0~,~109.2~,~109.2~,~false~,~406524~,,,~P0G~,~P0G~,,~false~,,~<~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~GM COMMERCIAL LINK - 2 YEARS OF SERVICE~,~~,~~,~240.0~,~240.0~,~218.4~,~218.4~,~false~,~406524~,,,~P0H~,~P0H~,,~false~,,~<~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~GM COMMERCIAL LINK - 3 YEARS OF SERVICE~,~~,~~,~360.0~,~360.0~,~327.6~,~327.6~,~false~,~406524~,,,~P0I~,~P0I~,,~false~,,~<~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SIRIUSXM RADIO ADDITIONAL 9 MONTHS OF THE SIRIUSXM ALL ACCESS PACKAGE.~,~1149;1381~,~~,~0.0~,~0.0~,~126.0~,~126.0~,~false~,~406524~,,,~PR6~,~PR6~,,~false~,,~O~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SIRIUSXM RADIO ADDITIONAL 21 MONTHS OF THE SIRIUSXM ALL ACCESS PACKAGE.~,~1149;1381~,~~,~0.0~,~0.0~,~252.0~,~252.0~,~false~,~406524~,,,~PR7~,~PR7~,,~false~,,~O~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~SIRIUSXM RADIO ADDITIONAL 33 MONTHS OF THE SIRIUSXM ALL ACCESS PACKAGE.~,~1149;1381~,~~,~0.0~,~0.0~,~378.0~,~378.0~,~false~,~406524~,,,~PR8~,~PR8~,,~false~,,~O~,~true~
~1~,,,~10750~,~ADDITIONAL EQUIPMENT~,~FLEET FREE MAINTENANCE CREDIT.~,~~,~~,~-33.75~,~-33.75~,~-30.71~,~-30.71~,~false~,~406524~,,,~R9Y~,~R9Y~,,~false~,,~0~,~true~
~1~,,,~10196~,~SHIP THRU CODES~,~SHIP THRU, PRODUCED IN SAN LUIS POTOSI (SLP) MEXICO ASSEMBLY AND SHIPPED TO AUTO TRUCK GROUP, ROANOKE, IN.~,~~,~~,~0.0~,~0.0~,~610.0~,~610.0~,~false~,~406524~,,,~TDL~,~TDL~,,~false~,~156~,~!~,~true~
~1~,,,~10196~,~SHIP THRU CODES~,~SHIP THRU, PRODUCED IN SAN LUIS POTOSI (SLP) MEXICO ASSEMBLY AND SHIPPED TO KERR INDUSTRIES, ARLINGTON, TX~,~~,~~,~0.0~,~0.0~,~505.0~,~505.0~,~false~,~406524~,,,~SVV~,~SVV~,,~false~,~156~,~!~,~true~
~1~,,,~10196~,~SHIP THRU CODES~,~SHIP THRU, PRODUCED IN SAN LUIS POTOSI (SLP) MEXICO ASSEMBLY AND SHIPPED TO GENERAL TRUCK BODY, ARLINGTON, TX, RETURN TO ARLINGTON ASSEMBLY~,~~,~~,~0.0~,~0.0~,~585.0~,~585.0~,~false~,~406524~,,,~TMA~,~TMA~,,~false~,~156~,~!~,~true~