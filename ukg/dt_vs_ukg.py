# encoding=utf-8
"""
"""
import db_cnx
import datetime
import csv

the_dir = '/mnt/hgfs/E/sql/postgresql/ukg_migration/files/test/'
start_date_str = '2021-12-19 01:00:00'
start_date_obj = datetime.datetime.strptime(start_date_str, '%Y-%m-%d %H:%M:%S')
start_date = start_date_obj.date()

# this works for current date - 1 because python does not include the last element in the range def
# range def is essentially (start, stop)
end_date = datetime.date.today()

day_delta = datetime.timedelta(days=1)

with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute('truncate jon.compare_hours;')
        for i in range((end_date - start_date).days):
            the_date = start_date + i*day_delta
            print(the_date)
            sql = "select jon.compare_hours('{0}')".format(the_date)
            pg_cur.execute(sql)
        for i in range((end_date - start_date).days):
            the_date = start_date + i*day_delta
            file_name = str(the_date) + 'todays_diffs.csv'
            sql = """
                select *
                from jon.compare_hours
                where the_date = '{0}';
            """.format(the_date)
            pg_cur.execute(sql)
            with open(the_dir + file_name, 'w') as f:
                csv.writer(f).writerows(pg_cur)




