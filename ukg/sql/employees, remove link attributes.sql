﻿removed link attributes from ukg.ext_employee_details and recreated the table and repopulated the table
also the locked attribute

alter table ukg.employee_details
drop column locked,
drop column demographics,
drop column photo_href,
drop column self,
drop column pay_info,
drop column badges,
drop column profiles,
drop column hcm_profiles,
drop column hcm_fields;

now the ugly part, were there changed rows yesterday or not
dont want to leave what are no longer changed rows (due to removal of the links)
select * from ukg.employee_details limit 10


select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'employee_details';

-- this proves my point, for some of these the new hash is the same for both rows from 8/5 and 8/21
select employee_id, row_from_date, hash,
  ( 
		select md5(x::text) as new_hash
		from (
		select employee_id,username,first_name,last_name,middle_name,external_id,employee_number,status,bsn,
			country,address_line_1,address_line_2,address_line_3,city,state,zip,number,number_addition,
			use_separate_mailing_address,ma_country,ma_address_line_1,ma_address_line_2,ma_address_line_3,
			ma_city,ma_state,ma_zip,ma_number,ma_number_addition,primary_account_id,ein_id,force_change_password,
			first_screen,cell_phone_country_id,cell_phone_country_code,cell_phone,home_phone_country_id,
			home_phone_country_code,home_phone,work_phone_country_id,work_phone_country_code,work_phone,
			preferred_phone,locale,managers,national_insurance,nickname,primary_email,secondary_email,
			salutation,social_insurance,social_security,suffix,timezone,cost_center_index,cost_center_id,
			add_to_new_hire_export,hired_date,deceased_date,re_hired_date,started_date,birthday,
			review_date,seniority_date,frozen_benefits_date,benefits_date,retired_date,terminated_date,
			peo_hired_date,managed_cost_centers_enabled,ssn
		from ukg.employee_details
		where employee_id = a.employee_id
		  and row_from_date = a.row_from_date) x)
from ukg.employee_details a
order by employee_id

select * 
from ukg.employee_details
where employee_id = 12987528130

the easiest approach may be to delete all rows with from date of 8/21 then update current row to true for all remaining rows
then reporcess the files from yesterday

delete from ukg.employee_details where row_from_date <> '08/05/2021';
update ukg.employee_details set current_row = true;

now just need to update the hash to the new rows without the links

update ukg.employee_details a
set hash = b.new_hash
from (
		select employee_id, md5(aa::text) as new_hash
		from (
			select employee_id,username,first_name,last_name,middle_name,external_id,employee_number,status,bsn,
				country,address_line_1,address_line_2,address_line_3,city,state,zip,number,number_addition,
				use_separate_mailing_address,ma_country,ma_address_line_1,ma_address_line_2,ma_address_line_3,
				ma_city,ma_state,ma_zip,ma_number,ma_number_addition,primary_account_id,ein_id,force_change_password,
				first_screen,cell_phone_country_id,cell_phone_country_code,cell_phone,home_phone_country_id,
				home_phone_country_code,home_phone,work_phone_country_id,work_phone_country_code,work_phone,
				preferred_phone,locale,managers,national_insurance,nickname,primary_email,secondary_email,
				salutation,social_insurance,social_security,suffix,timezone,cost_center_index,cost_center_id,
				add_to_new_hire_export,hired_date,deceased_date,re_hired_date,started_date,birthday,
				review_date,seniority_date,frozen_benefits_date,benefits_date,retired_date,terminated_date,
				peo_hired_date,managed_cost_centers_enabled,ssn
			from ukg.employee_details) aa) b
where a.employee_id = b.employee_id			

ok, i think we are good  

select * 
from (
select employee_id, row_from_date, hash,
  ( 
		select md5(x::text) as new_hash
		from (
		select employee_id,username,first_name,last_name,middle_name,external_id,employee_number,status,bsn,
			country,address_line_1,address_line_2,address_line_3,city,state,zip,number,number_addition,
			use_separate_mailing_address,ma_country,ma_address_line_1,ma_address_line_2,ma_address_line_3,
			ma_city,ma_state,ma_zip,ma_number,ma_number_addition,primary_account_id,ein_id,force_change_password,
			first_screen,cell_phone_country_id,cell_phone_country_code,cell_phone,home_phone_country_id,
			home_phone_country_code,home_phone,work_phone_country_id,work_phone_country_code,work_phone,
			preferred_phone,locale,managers,national_insurance,nickname,primary_email,secondary_email,
			salutation,social_insurance,social_security,suffix,timezone,cost_center_index,cost_center_id,
			add_to_new_hire_export,hired_date,deceased_date,re_hired_date,started_date,birthday,
			review_date,seniority_date,frozen_benefits_date,benefits_date,retired_date,terminated_date,
			peo_hired_date,managed_cost_centers_enabled,ssn
		from ukg.employee_details
		where employee_id = a.employee_id
		  and row_from_date = a.row_from_date) x)
from ukg.employee_details a) y
where hash <> new_hash


oops, i dont have yesterdays data, dropped and recreated ukg.ext_employee_details, oh, well, proccess todays data

oops, neet to get rid of the rows in ukg.employee_ids that dont yet exists in ukg.employee_details

delete from ukg.employee_ids a
where not exists (
  select 1
  from ukg.employee_details
  where employee_id = a.employee_id);

select * from 
-- query to see diffs in rows
select employee_id,
	username,first_name,last_name,middle_name,external_id,
	employee_number,status,bsn,	country,address_line_1,	address_line_2,
	address_line_3,	city,	state,	zip,	number,	number_addition,
	use_separate_mailing_address,	ma_country,	ma_address_line_1,	ma_address_line_2,
	ma_address_line_3,	ma_city,	ma_state,	ma_zip,	ma_number,	ma_number_addition,
	primary_account_id,	ein_id,	force_change_password,	first_screen,
	cell_phone_country_id,	cell_phone_country_code,	cell_phone,	home_phone_country_id,
	home_phone_country_code,	home_phone,	work_phone_country_id,	work_phone_country_code,
	work_phone,	preferred_phone,	locale,	managers,	national_insurance,	nickname,
	primary_email,	secondary_email,	salutation,	social_insurance,	social_security,
	suffix,	timezone,	cost_center_index,	cost_center_id,	add_to_new_hire_export,
	hired_date,deceased_date,re_hired_date,started_date,birthday,review_date,
	seniority_date,frozen_benefits_date,benefits_date,retired_date,
	terminated_date,peo_hired_date,
-- 	self,demographics,pay_info,badges,profiles,hcm_profiles,hcm_fields,
	managed_cost_centers_enabled,ssn from ukg.employee_details where employee_id = 12987528889
union all
select * from ukg.ext_employee_details where employee_id = 12987528889  