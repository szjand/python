﻿PAY PERIODS

drop table if exists ukg.json_get_pay_periods cascade;
create unlogged table ukg.json_get_pay_periods (
response jsonb);
comment on table ukg.json_get_pay_periods is 'raw scrape of ukg endpoint Get Pay Periods';

drop table if exists ukg.ext_pay_periods cascade;
create table ukg.ext_pay_periods (
	id bigint primary key,
	display_name citext,
	profile_name citext,
	profile_id integer,
	ein_id integer, 
	ein_name citext,
	ein_tax_id citext,
	total_employees integer not null,
	locked_employees integer not null,
	available_employees integer not null,
	start_date date not null,
	end_date date not null,
	pay_date date,
	submit_date date,
	process_date date,
	locked boolean);

insert into ukg.ext_pay_periods	
select (b->>'id')::bigint,
	b->>'display_name',
	b->>'profile_name',
	(b->'pay_period_profile'->>'id')::integer,
	(b->'ein'->>'id')::integer,
	b->'ein'->>'ein_name',
	b->'ein'->>'ein_tax_id',
	(b->>'total_employees')::integer,
	(b->>'locked_employees')::integer,
	(b->>'available_employees')::integer,
	(b->>'start_date')::date,
	(b->>'end_date')::date,
	(b->>'pay_date')::date,
	(b->>'submit_date')::date,
	(b->>'process_date')::date,
	(b->>'locked')::boolean
-- select a.*	
from ukg.json_pay_periods a
join jsonb_array_elements(a.response->'pay_periods') b on true;
-- select * from ukg.ext_pay_periods;
----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_get_single_pay_period cascade;
create unlogged table ukg.json_get_single_pay_period (
response jsonb);
comment on table ukg.json_get_single_pay_period is 'raw scrape of ukg endpoint Get Pay Periods';

-- 07/30/21 afton has no idea why this is failing in this way
    "errors": [
        {
            "code": 400,
            "message": "Invalid parameter format. Parameter: comp_id"
        }
    ],
    "user_messages": [
        {
            "severity": "ERROR",
            "text": "Invalid parameter format. Parameter: comp_id",
            "details": {
                "code": 400
            }
        }
    ]
----------------------------------------------------------------------------------------------------