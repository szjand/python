﻿select distinct b->>'id'
from ukg.json_employees a
join jsonb_array_elements(a.response->'employees') as b(employees) on true;	

select * from ukg.employee_details
-- 08/21/221 initial load from the 08/05 scrape
-- 08/22 realized that all the links attributes except self said a.response->'phone'->>xxxxxxx
-- fixed that, but am seriously considering removing the links, they are api specific and will
-- be generating bogus changed rows
-- fuck, it lets clean it up now  remove photo href and all links
drop table if exists ukg.employee_details cascade;
create table ukg.employee_details (
	employee_id bigint not null,
	username citext not null,
	first_name citext not null,
	last_name citext not null,
	photo_href citext,
	middle_name citext,
	external_id citext,
	employee_number citext,
	status citext,
	bsn citext,
	country citext not null,
	address_line_1 citext,
	address_line_2 citext,
	address_line_3 citext,
	city citext,
	state citext,
	zip citext,
	number citext,
	number_addition citext,
	use_separate_mailing_address boolean,
	ma_country citext,
	ma_address_line_1 citext,
	ma_address_line_2 citext,
	ma_address_line_3 citext,
	ma_city citext,
	ma_state citext,
	ma_zip citext,
	ma_number citext,
	ma_number_addition citext,
	primary_account_id bigint,
	ein_id bigint,
	force_change_password boolean,
	locked boolean,
	first_screen citext,
	cell_phone_country_id citext,
	cell_phone_country_code citext,
	cell_phone citext,
	home_phone_country_id citext,
	home_phone_country_code citext,
	home_phone citext,
	work_phone_country_id citext,
	work_phone_country_code citext,
	work_phone citext,
	preferred_phone citext check (preferred_phone in ('HOME','CELL','WORK','NONE')),
	locale bigint,
	managers jsonb,
	national_insurance citext,
	nickname citext,
	primary_email citext,
	secondary_email citext,
	salutation citext,
	social_insurance citext,
	social_security citext,
	suffix citext,
	timezone citext,
	cost_center_index integer check (cost_center_index between 0 and 8),
	cost_center_id bigint,
	add_to_new_hire_export boolean,
	hired_date date,
	deceased_date date,
	re_hired_date date,
	started_date date,
	birthday date,
	review_date date,
	seniority_date date,
	frozen_benefits_date date,
	benefits_date date,
	retired_date date,
	terminated_date date,
	peo_hired_date date,
	self citext,
	demographics citext,
	pay_info citext,
	badges citext,
	profiles citext,
	hcm_profiles citext,
	hcm_fields citext,	
	managed_cost_centers_enabled boolean,
	ssn citext,
	current_row boolean not null,
	row_from_date date not null,
	row_thru_date date not null default '12/31/9999',
	hash citext not null,
	primary key (employee_id,row_from_date));
CREATE UNIQUE INDEX ON ukg.employee_details(employee_id) WHERE current_row;
-- 
-- -- -- done 08/22/21
-- alter table ukg.employee_details
-- drop column demographics,
-- drop column photo_href,
-- drop column self,
-- drop column pay_info,
-- drop column badges,
-- drop column profiles,
-- drop column hcm_profiles,
-- drop column hcm_fields;

-- select string_agg(column_name, ',' order by ordinal_position)
-- from information_schema.columns
-- where table_schema = 'ukg'
--   and table_name = 'employee_details';
  
insert into ukg.employee_details (employee_id,username,first_name,last_name,middle_name,
	external_id,employee_number,status,bsn,country,address_line_1,address_line_2,address_line_3,
	city,state,zip,number,number_addition,use_separate_mailing_address,ma_country,ma_address_line_1,
	ma_address_line_2,ma_address_line_3,ma_city,ma_state,ma_zip,ma_number,ma_number_addition,
	primary_account_id,ein_id,force_change_password,locked,first_screen,cell_phone_country_id,
	cell_phone_country_code,cell_phone,home_phone_country_id,home_phone_country_code,home_phone,
	work_phone_country_id,work_phone_country_code,work_phone,preferred_phone,locale,managers,
	national_insurance,nickname,primary_email,secondary_email,salutation,social_insurance,
	social_security,suffix,timezone,cost_center_index,cost_center_id,add_to_new_hire_export,
	hired_date,deceased_date,re_hired_date,started_date,birthday,review_date,seniority_date,
	frozen_benefits_date,benefits_date,retired_date,terminated_date,peo_hired_date,
	managed_cost_centers_enabled,ssn,current_row,
	row_from_date,row_thru_date,hash)
select c.*, true, '08/05/2021', '12/31/9999', md5(c::text)
from (
	select (a.response->>'id')::bigint, 
		lower(a.response->>'username') as username,
		initcap(a.response->>'first_name') as first_name, 
		initcap(a.response->>'last_name') as last_name,
-- 		a.response->>'photo_href', 
		a.response->>'middle_name',
		a.response->>'external_id', 
		a.response->>'employee_id',
		a.response->>'status', 
		a.response->>'bsn',
		a.response->'address'->>'country', 
		a.response->'address'->>'address_line_1',
		a.response->'address'->>'address_line_2', 
		a.response->'address'->>'address_line_3',
		a.response->'address'->>'city', 
		a.response->'address'->>'state',
		a.response->'address'->>'zip', 
		a.response->'address'->>'number',
		a.response->'address'->>'number_adddition',
		(a.response->>'use_separate_mailing_address')::boolean,
		a.response->'mailing_address'->>'country', 
		a.response->'mailing_address'->>'address_line_1',
		a.response->'mailing_address'->>'address_line_2',	
		a.response->'mailing_address'->>'address_line_3',
		a.response->'mailing_address'->>'city',	
		a.response->'mailing_address'->>'state',
		a.response->'mailing_address'->>'zip',	
		a.response->'mailing_address'->>'number',
		a.response->'mailing_address'->>'number_adddition',
		(a.response->>'primary_account_id')::bigint,	
		(a.response->'ein'->>'id')::bigint as ein_id,
		(a.response->>'force_change_password')::boolean,	
		(a.response->>'locked')::boolean,
		a.response->'first_screen'->>'id', 
		a.response->'phones'->>'cell_phone_country_id',
		a.response->'phones'->>'cell_phone_country_code',	
		a.response->'phones'->>'cell_phone',
		a.response->'phones'->>'home_phone_country_id',	
		a.response->'phones'->>'home_phone_country_code',
		a.response->'phones'->>'home_phone',	
		a.response->'phones'->>'work_phone_country_id',
		a.response->'phones'->>'work_phone_country_code',	
		a.response->'phones'->>'work_phone',
		a.response->'phones'->>'preferred_phone' as preferred_phone,
		(a.response->'locale'->>'id')::bigint,
		a.response->'managers',	
		a.response->>'national_insurance',
		a.response->>'nickname',	
		a.response->>'primary_email',
		a.response->>'secondary_email',  
		a.response->>'salutation',
		a.response->>'social_insurance',	
		a.response->>'social_security',
		a.response->>'suffix',	
		a.response->>'timezone',
		(c->>'index')::integer,	
		(c->'value'->>'id')::bigint,
		(a.response->>'add_to_new_hire_export')::boolean,
		(a.response->'dates'->>'hired')::date,	
		(a.response->'dates'->>'deceased')::date,
		(a.response->'dates'->>'re_hired')::date,
		(a.response->'dates'->>'started')::date,
		(a.response->'dates'->>'birthday')::date,
		(a.response->'dates'->>'review')::date,
		(a.response->'dates'->>'seniority')::date,
		(a.response->'dates'->>'frozen_benefits')::date,
		(a.response->'dates'->>'benefits')::date,
		(a.response->'dates'->>'retired')::date,
		(a.response->'dates'->>'terminated')::date,
		(a.response->'dates'->>'peo_hired')::date,
-- 		a.response->'_links'->>'self',
-- 		a.response->'_links'->>'demographics',
-- 		a.response->'_links'->>'pay-info',
-- 		a.response->'_links'->>'badges',  
-- 		a.response->'_links'->>'profiles',
-- 		a.response->'_links'->>'hcm-profiles',
-- 		a.response->'_links'->>'hcm-fields',
		(a.response->>'managed_cost_centers_enabled')::boolean,
		b->>'value'
	from ukg.json_employee_details a 
	left join jsonb_array_elements(a.response->'national_id_numbers') as b(ssn) on true  
	left join jsonb_array_elements(a.response->'cost_centers_info'->'defaults') as c(cost_centers) on true) c limit 10;

-- lets do ext_employees first
-- ukg.ext_employee, truncated and filled nightly
truncate ukg.ext_employee_details;
insert into ukg.ext_employee_details (employee_id,username,first_name,last_name,photo_href,middle_name,external_id,
	employee_number,status,bsn,country,address_line_1,address_line_2,address_line_3,city,state,zip,number,
	number_addition,use_separate_mailing_address,ma_country,ma_address_line_1,ma_address_line_2,
	ma_address_line_3,ma_city,ma_state,ma_zip,ma_number,ma_number_addition,primary_account_id,ein_id,
	force_change_password,locked,first_screen,cell_phone_country_id,cell_phone_country_code,cell_phone,
	home_phone_country_id,home_phone_country_code,home_phone,work_phone_country_id,work_phone_country_code,
	work_phone,preferred_phone,locale,managers,national_insurance,nickname,primary_email,secondary_email,
	salutation,social_insurance,social_security,suffix,timezone,
	cost_center_index,cost_center_id,
	add_to_new_hire_export,hired_date,deceased_date,re_hired_date,started_date,birthday,review_date,
	seniority_date,frozen_benefits_date,benefits_date,retired_date,
	terminated_date,peo_hired_date,
	self,demographics,pay_info,badges,profiles,hcm_profiles,hcm_fields,
	managed_cost_centers_enabled,ssn)
select (a.response->>'id')::bigint, lower(a.response->>'username') as username,
	initcap(a.response->>'first_name') as first_name, initcap(a.response->>'last_name') as last_name,
-- 	a.response->>'photo_href', 
	a.response->>'middle_name',
	a.response->>'external_id', a.response->>'employee_id',
	a.response->>'status', a.response->>'bsn',
	a.response->'address'->>'country', a.response->'address'->>'address_line_1',
	a.response->'address'->>'address_line_2', a.response->'address'->>'address_line_3',
	a.response->'address'->>'city', a.response->'address'->>'state',
	a.response->'address'->>'zip', a.response->'address'->>'number',
	a.response->'address'->>'number_adddition',
	(a.response->>'use_separate_mailing_address')::boolean,
	a.response->'mailing_address'->>'country', a.response->'mailing_address'->>'address_line_1',
	a.response->'mailing_address'->>'address_line_2',	a.response->'mailing_address'->>'address_line_3',
	a.response->'mailing_address'->>'city',	a.response->'mailing_address'->>'state',
	a.response->'mailing_address'->>'zip',	a.response->'mailing_address'->>'number',
	a.response->'mailing_address'->>'number_adddition',
	(a.response->>'primary_account_id')::bigint,	(a.response->'ein'->>'id')::bigint as ein_id,
	(a.response->>'force_change_password')::boolean,	(a.response->>'locked')::boolean,
	a.response->'first_screen'->>'id', a.response->'phones'->>'cell_phone_country_id',
	a.response->'phones'->>'cell_phone_country_code',	a.response->'phones'->>'cell_phone',
	a.response->'phones'->>'home_phone_country_id',	a.response->'phones'->>'home_phone_country_code',
	a.response->'phones'->>'home_phone',	a.response->'phones'->>'work_phone_country_id',
	a.response->'phones'->>'work_phone_country_code',	a.response->'phones'->>'work_phone',
	a.response->'phones'->>'preferred_phone' as preferred_phone,
	(a.response->'locale'->>'id')::bigint,
	a.response->'managers',	a.response->>'national_insurance',
	a.response->>'nickname',	a.response->>'primary_email',
	a.response->>'secondary_email',  a.response->>'salutation',
  a.response->>'social_insurance',	a.response->>'social_security',
	a.response->>'suffix',	a.response->>'timezone',
	(c->>'index')::integer,	(c->'value'->>'id')::bigint,
	(a.response->>'add_to_new_hire_export')::boolean,
	(a.response->'dates'->>'hired')::date, (a.response->'dates'->>'deceased')::date,
	(a.response->'dates'->>'re_hired')::date,	(a.response->'dates'->>'started')::date,
	(a.response->'dates'->>'birthday')::date,	(a.response->'dates'->>'review')::date,
	(a.response->'dates'->>'seniority')::date,	(a.response->'dates'->>'frozen_benefits')::date,
	(a.response->'dates'->>'benefits')::date,	(a.response->'dates'->>'retired')::date,
	(a.response->'dates'->>'terminated')::date,	(a.response->'dates'->>'peo_hired')::date,
-- 	a.response->'_links'->>'self',	a.response->'phones'->>'demographics',
-- 	a.response->'phones'->>'pay-info',	a.response->'phones'->>'badges',  
-- 	a.response->'phones'->>'profiles',	a.response->'phones'->>'hcm-profiles',
-- 	a.response->'phones'->>'hcm-fields',  
	(a.response->>'managed_cost_centers_enabled')::boolean,
  b->>'value'
from ukg.json_employee_details a 
left join jsonb_array_elements(a.response->'national_id_numbers') as b(ssn) on true  
left join jsonb_array_elements(a.response->'cost_centers_info'->'defaults') as c(cost_centers) on true;


/*

-- think i  will need to create an employee_id table
drop table if exists ukg.employee_ids cascade;
create table ukg.employee_ids (
	employee_id bigint primary key);
-- initial load with data from 08/05
insert into ukg.employee_ids
select distinct employee_id from ukg.employee_details;

*/

-- new rows
-- first into ukg.employee_ids
insert into ukg.employee_ids
select employee_id
from ukg.ext_employee_details a
where not exists (
  select 1
  from ukg.employee_details
  where employee_id = a.employee_id);

insert into ukg.employee_details (employee_id,
	username,first_name,last_name,middle_name,external_id,
	employee_number,status,bsn,	country,address_line_1,	address_line_2,
	address_line_3,	city,	state,	zip,	number,	number_addition,
	use_separate_mailing_address,	ma_country,	ma_address_line_1,	ma_address_line_2,
	ma_address_line_3,	ma_city,	ma_state,	ma_zip,	ma_number,	ma_number_addition,
	primary_account_id,	ein_id,	force_change_password,	locked,	first_screen,
	cell_phone_country_id,	cell_phone_country_code,	cell_phone,	home_phone_country_id,
	home_phone_country_code,	home_phone,	work_phone_country_id,	work_phone_country_code,
	work_phone,	preferred_phone,	locale,	managers,	national_insurance,	nickname,
	primary_email,	secondary_email,	salutation,	social_insurance,	social_security,
	suffix,	timezone,	cost_center_index,	cost_center_id,	add_to_new_hire_export,
	hired_date,deceased_date,re_hired_date,started_date,birthday,review_date,
	seniority_date,frozen_benefits_date,benefits_date,retired_date,
	terminated_date,peo_hired_date,
-- 	self,demographics,pay_info,badges,profiles,hcm_profiles,hcm_fields,
	managed_cost_centers_enabled,ssn,current_row,row_from_date,hash)
select a.*, true, current_date, md5(a::text)
from ukg.ext_employee_details a	
where not exists (
  select 1
  from ukg.employee_details
  where employee_id = a.employee_id)


-- -- -- changed rows
-- -- drop table if exists ukg.employees_changed_rows cascade;
-- -- create table ukg.employees_changed_rows (
-- -- 	employee_id bigint primary key);
-- -- 08-25 fucked this up should only be for the current row
-- truncate ukg.employees_changed_rows;
-- insert into ukg.employees_changed_rows	
-- select a.employee_id
-- from ukg.ext_employee_details a
-- join ukg.employee_details b on a.employee_id = b.employee_id
--   and md5(a::text) <> b.hash;

-- 08-25 fucked this up should only be for the current row
truncate ukg.employees_changed_rows;
insert into ukg.employees_changed_rows	
select a.employee_id
from ukg.ext_employee_details a
join ukg.employee_details b on a.employee_id = b.employee_id
  and md5(a::text) <> b.hash
  and b.current_row;

  
-- -- 08-21-21  the links have different urls, all 519 from 08/05
-- select a.*, true, current_date, '12/31/9999', md5(a::text) from ukg.ext_employee_details a where employee_id = 12987528732  
-- union 
-- select * from ukg.employee_details where employee_id = 12987528732
-- 
-- 08-22-21
-- as i redo yesterdays work without the links attributes
-- there are 405 changed rows, all that i have checked only the locked attribute has changed from false to true
-- that feels to me like another field to drop until there is proof that it is required
-- ok, now it makes more sense, only 36 changed rows
-- 1 select md5('GRAND FORKS')
-- union
-- select md5('Grand Forks')
-- 2 cost center changed
-- 1 mgr & cost center
-- 1 phone number format
-- 2 address
-- all the rest are active -> term

-- -- update current row 
-- -- 08-25 fucked this up,
-- update ukg.employee_details a
-- set current_row = false
-- from ukg.employees_changed_rows b
-- where a.employee_id = b.employee_id;

-- update current row 
-- 08-25 fucked this up, need to update row_thru_date as well
update ukg.employee_details a
set current_row = false, 
    row_thru_date = current_date - 1
from ukg.employees_changed_rows b
where a.employee_id = b.employee_id
  and a.current_row;

-- insert the new row
insert into ukg.employee_details (employee_id,
	username,first_name,last_name,middle_name,external_id,
	employee_number,status,bsn,	country,address_line_1,	address_line_2,
	address_line_3,	city,	state,	zip,	number,	number_addition,
	use_separate_mailing_address,	ma_country,	ma_address_line_1,	ma_address_line_2,
	ma_address_line_3,	ma_city,	ma_state,	ma_zip,	ma_number,	ma_number_addition,
	primary_account_id,	ein_id,	force_change_password,first_screen,
	cell_phone_country_id,	cell_phone_country_code,	cell_phone,	home_phone_country_id,
	home_phone_country_code,	home_phone,	work_phone_country_id,	work_phone_country_code,
	work_phone,	preferred_phone,	locale,	managers,	national_insurance,	nickname,
	primary_email,	secondary_email,	salutation,	social_insurance,	social_security,
	suffix,	timezone,	cost_center_index,	cost_center_id,	add_to_new_hire_export,
	hired_date,deceased_date,re_hired_date,started_date,birthday,review_date,
	seniority_date,frozen_benefits_date,benefits_date,retired_date,
	terminated_date,peo_hired_date,
	managed_cost_centers_enabled,ssn,current_row,row_from_date,hash)
select a.*, true, current_date, md5(a::text)
from ukg.ext_employee_details a	
join ukg.employees_changed_rows b on a.employee_id = b.employee_id;

select last_name, first_name, count(*)
from ukg.employee_details
group by last_name, first_name
order by count(*) desc

select * from ukg.employee_details where last_name = 'longoria' and first_name = 'michael' order by employee_id