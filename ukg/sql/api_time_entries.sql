﻿Time ENTRIES

10/12/21
discovered lots of entries for the period 7/1/21 -> 7/30/21
and that is now what is in ukg.json_time_entries



select b->'employee'->>'account_id', b->>'start_date', b->>'end_date', b->'time_entries'--cardinality(b->'time_entries')
from ukg.json_time_entries a
join jsonb_array_elements(a.response->'time_entry_sets') b on true
-- join jsonb_array_elements(b->'time_entries') c on true
where b->'employee'->>'account_id' = '12987991201'



-- 459 records (7/1 -> 7/30/2021), 160 with no time_entries, 299 with at least 1 time entry
select distinct b->'employee'->>'account_id', b->>'start_date', b->>'end_date', jsonb_array_length(b->'time_entries')
from ukg.json_time_entries a
join jsonb_array_elements(a.response->'time_entry_sets') b on true
left join jsonb_array_elements(b->'time_entries') c on true
-- where b->'employee'->>'account_id' in('12987991201','12986298521')
where jsonb_array_length(b->'time_entries') <> 0



select distinct b->'employee'->>'account_id', b->>'start_date', b->>'end_date', jsonb_array_length(b->'time_entries'), c.*
from ukg.json_time_entries a
join jsonb_array_elements(a.response->'time_entry_sets') b on true
left join jsonb_array_elements(b->'time_entries') c on true
where b->'employee'->>'account_id' in('12987991201','12986298521')


drop table if exists ukg.json_time_entries cascade;
create unlogged table ukg.json_time_entries (
	response jsonb);
comment on table ukg.json_time_entries is 'raw scrape of ukg endpoint Time Entries; https://beta.rydellvision.com:8888/ukg/time-entries?start_date=2021-07-01&end_date=2021-07-31';
-- select * from ukg.json_time_entries

drop table if exists ukg.ext_time_entries cascade;
create table ukg.ext_time_entries (
  employee_account_id bigint not null,
  start_date date not null,
  end_date date not null,
  time_entry_id bigint primary key,
  time_entry_type citext check (time_entry_type in ('TIME','EXTRA_PAY')),
  the_date date not null,
  start_time timestamptz,
  end_time timestamptz,
  group_hash integer,
  total bigint,
  time_off_id bigint,
  pay_category_id integer,
  premium_shift_id integer,
  is_raw boolean,
  is_calc boolean,
  calc_start_time timestamptz,
  calc_end_time timestamptz,
  calc_total bigint,
  calc_pay_category_id integer,
  calc_premium_shift_id integer,
  cost_centers jsonb,
  custom_fields jsonb,
  piecework numeric,
  amount numeric,
  from_date date,
  to_date date);
create unique index on ukg.ext_time_entries(employee_account_id,start_time) ;
comment on table ukg.ext_time_entries is 'ukg.json_time_entries parsed, excludes records without a time entry';
comment on column ukg.ext_time_entries.start_date is 'the start of the period from which time entries are extracted';
comment on column ukg.ext_time_entries.end_date is 'the end of the period from which time entries are extracted';

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_time_entries';

  
insert into ukg.ext_time_entries(
	employee_account_id,start_date,end_date,time_entry_id,time_entry_type,the_date,start_time,
	end_time,group_hash,total,time_off_id,pay_category_id,premium_shift_id,is_raw,is_calc,
	calc_start_time,calc_end_time,calc_total,calc_pay_category_id,calc_premium_shift_id,
	cost_centers,custom_fields,piecework,amount,from_date,to_date)
select (b->'employee'->>'account_id')::bigint, 
	(b->>'start_date')::date, 
	(b->>'end_date')::date, 
  (c->>'id')::bigint, 
  c->>'type', 
  (c->>'date')::date,
  (c->>'start_time')::timestamptz, 
  (c->>'end_time')::timestamptz, 
  (c->>'group_hash')::integer, 
  (c->>'total')::bigint,
  (c->'time_off'->>'id')::bigint, 
  (c->'pay_category'->>'id')::bigint,
  (c->'premium_shift'->>'id')::bigint,
  (c->>'is_raw')::boolean, 
  (c->>'is_calc')::boolean,
  (c->>'calc_start_time')::timestamptz, 
  (c->>'calc_end_time')::timestamptz, 
  (c->>'calc_total')::bigint,
  (c->'calc_pay_category'->>'id')::bigint,
  (c->'calc_premium_shift'->>'id')::bigint,
  c->'cost_centers', 
  c->'custom_fields',
  (c->>'piecework')::numeric, 
  (c->>'amount')::numeric,
  (c->>'from_date')::date, 
  (c->>'to_date')::date
from ukg.json_time_entries a
join jsonb_array_elements(a.response->'time_entry_sets') b on true
left join jsonb_array_elements(b->'time_entries') c on true
where jsonb_array_length(b->'time_entries') <> 0;

create or replace function ukg.ext_time_entries()
  returns void as
$BODY$
  delete 
  from ukg.ext_time_entries
  where the_date between the_date - 30 and current_date;
  
	insert into ukg.ext_time_entries(
		employee_account_id,start_date,end_date,time_entry_id,time_entry_type,the_date,start_time,
		end_time,group_hash,total,time_off_id,pay_category_id,premium_shift_id,is_raw,is_calc,
		calc_start_time,calc_end_time,calc_total,calc_pay_category_id,calc_premium_shift_id,
		cost_centers,custom_fields,piecework,amount,from_date,to_date)
	select (b->'employee'->>'account_id')::bigint, 
		(b->>'start_date')::date, 
		(b->>'end_date')::date, 
		(c->>'id')::bigint, 
		c->>'type', 
		(c->>'date')::date,
		(c->>'start_time')::timestamptz, 
		(c->>'end_time')::timestamptz, 
		(c->>'group_hash')::integer, 
		(c->>'total')::bigint,
		(c->'time_off'->>'id')::bigint, 
		(c->'pay_category'->>'id')::bigint,
		(c->'premium_shift'->>'id')::bigint,
		(c->>'is_raw')::boolean, 
		(c->>'is_calc')::boolean,
		(c->>'calc_start_time')::timestamptz, 
		(c->>'calc_end_time')::timestamptz, 
		(c->>'calc_total')::bigint,
		(c->'calc_pay_category'->>'id')::bigint,
		(c->'calc_premium_shift'->>'id')::bigint,
		c->'cost_centers', 
		c->'custom_fields',
		(c->>'piecework')::numeric, 
		(c->>'amount')::numeric,
		(c->>'from_date')::date, 
		(c->>'to_date')::date
	from ukg.json_time_entries a
	join jsonb_array_elements(a.response->'time_entry_sets') b on true
	left join jsonb_array_elements(b->'time_entries') c on true
	where jsonb_array_length(b->'time_entries') <> 0;
$BODY$
language sql;	


delete from ukg.ext_time_entries where the_date between '12/13/2021' and '12/15/2021'
select * from ukg.ext_time_entries 11041

select current_date, current_date - 31, current_date - (current_date - 31) 




11/24/21
this table now includes the imported data for may, june & july 2021
all of which was rounded in the import process to the minute 
!!! DOES NOT INCLUDE THE SECONDS !!!


select * -- 5007 rows
from ukg.ext_time_entries a

select max(the_date) from ukg.ext_time_entries

select b.last_name, b.first_name, b.employee_number, a.* 
from ukg.ext_time_entries a
left join ukg.ext_employees b on a.employee_account_id = b.primary_account_id
order by a.employee_account_id, a.the_date, a.start_time

select distinct time_entry_type from ukg.ext_time_entries

-- 12/13/2021
-- only 1 time entry for november 
-- very little for december, mostly clock ins with no clock outs

----------------------------------------------------
--< 12/16/21
----------------------------------------------------
the query that inserts into ukg.ext_time_entries excludes rows for which there is no time_entries
it appears that ukd creates at least a row for each employee, but not all employees have time_entiries data

select * from ukg.ext_time_entries where the_date > '11/30/2021' order by employee_account_id, the_date 

select the_date, count(*)
from ukg.ext_time_entries
group by the_date
order by the_date

download and import all december data

select * from  ukg.ext_timeoffs 

select b.last_name, b.first_name, b.employee_id, b.employee_number, c.name, --a.*
	a.the_date, a.start_time, a.end_time,  a.end_time - a.start_time as "end - start", 
	-- ukg total is in msec, msec/3600000.0 gives decimal time: eg, 7.85 
	a.total as ukg, (a.total/3600000.0)::numeric(9,2) as ukg_total_dec,
	extract(epoch from a.end_time) - extract(epoch from a.start_time)  as pg_total, 
	((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600)::numeric(6,2) as hours_dec,
	a.calc_start_time, a.calc_end_time, a.calc_total
from ukg.ext_time_entries a
join ukg.ext_employees b on a.employee_account_id = b.employee_id
left join ukg.ext_timeoffs c on a.time_off_id = c.id
where the_date between '12/01/2021' and '12/31/2021' 
	and (a.total/3600000.0)::numeric(9,2) <> ((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600)::numeric(9,2)
--   and employee_account_id = 12987528691 
order by employee_account_id, the_date,  coalesce(a.start_time, a.calc_start_time)


drop table if exists ukg.timesheets_tmp cascade;
-- store the time_off_id rather than the name, that way have access to all the ([possibly]) changing time-offs
create table ukg.timesheets_tmp as
select /*b.last_name, b.first_name,*/ b.employee_id, b.employee_number, a.time_off_id, -- c.name as time_off,
	a.the_date, a.start_time, a.end_time,  a.end_time - a.start_time as total_time, 
	-- ukg total is in msec, msec/3600000.0 gives decimal time: eg, 7.85 
-- 	a.total as ukg, (a.total/3600000.0)::numeric(9,2) as ukg_total_dec,
	extract(epoch from a.end_time) - extract(epoch from a.start_time)  as total_seconds, 
	((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600)::numeric(6,2) as total_dec,
	a.total as ukg_total
-- 	a.calc_start_time, a.calc_end_time, a.calc_total
from ukg.ext_time_entries a
join ukg.ext_employees b on a.employee_account_id = b.employee_id 
-- left join ukg.ext_timeoffs c on a.time_off_id = c.id order by a.time_off_id
where last_name not in ('ee')
order by the_date;
-- where the_date between '11/01/2021' and '12/31/2021' 
--   and employee_account_id = 12987528691 


select * from ukg.timesheets_tmp












----------------------------------------------------
--/> 12/16/21
----------------------------------------------------
-- it appears that there is a separate row, with no start/end/total time, but a time_off_id
-- and calc_start & end times that corresponds to the time off  (lunch) for the day
create table ukg.timesheets_tmp as
select b.last_name, b.first_name, b.employee_id, b.employee_number, a.time_off_id, c.name, --a.*
	a.the_date, a.start_time, a.end_time,  a.end_time - a.start_time as "end - start", a.total as ukg_total, (a.total/3600000.0)::numeric(9,2) as ukg_total_dec,
	extract(epoch from a.end_time) - extract(epoch from a.start_time)  as pg_total, ((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600)::numeric(6,2) as hours_dec,
	a.calc_start_time, a.calc_end_time, a.calc_total
from ukg.ext_time_entries a
join ukg.ext_employees b on a.employee_account_id = b.employee_id
left join ukg.ext_timeoffs c on a.time_off_id = c.id
where c.name is not null order by c.name
-- where (a.total/3600000.0)::numeric(9,2) <> ((extract(epoch from a.end_time) - extract(epoch from a.start_time))/3600)::numeric(9,2)
-- where the_date between '07/01/2021' and '07/31/2021' 
--   and employee_account_id = 12987528691 
order by employee_account_id, the_date,  coalesce(a.start_time, a.calc_start_time)


select distinct name 
from ukg.timesheets_tmp

select *
from ukg.timesheets_tmp
limit 100

select * from arkona.xfm_pypclockin where employee_number = '150126' and the_date > '05/01/2021' and pto_hours <> 0 order by the_date

  select yico_, pymast_employee_number, yiclkind, yiclkoutd, yiclkoutt, yiclkint,
      coalesce(
        sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
          to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
          /3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,    
      coalesce(
        sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
          to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
          /3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours, 
      coalesce(
        sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
          to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
          /3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours, 
      coalesce(
        sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
          to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
          /3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours  
  from ( -- d: clock hours
    select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
    from arkona.ext_pypclockin_tmp a
    where a.yicode in ('HOL','666','O','PTO','VAC')) d
  group by yico_, pymast_employee_number, yiclkind, yiclkoutd, yiclkoutt, yiclkint
  limit 100

select *
from arkona.xfm_pypclockin
limit 100
select 8*60*60*1000

----------------------------------------------------------------------------------------
--< total and calc_total attributes are 12 seconds ofF
----------------------------------------------------------------------------------------
select start_time, end_time, end_time - start_time, total, extract(epoch from start_time), extract(epoch from end_time), extract(epoch from end_time) - extract(epoch from start_time)
from ukg.ext_time_entries
where the_date = '07/06/2021'
  and employee_account_id = 12987528612
  and start_time is not null
----------------------------------------------------------------------------------------
--/> total and calc_total attributes are 12 seconds ofF
----------------------------------------------------------------------------------------

-- are there rows where date of start_time and end_time are different?
-- not yet
select start_time, end_time, extract(doy from start_time), extract(doy from end_time)
from ukg.ext_time_entries
where extract(doy from start_time) <> extract(doy from end_time)


 select *
-- select sum(clock_hours) 
from arkona.xfm_pypclockin
where employee_number = '168573'
  and the_date between '07/04/2021' and '07/17/2021'
  
select * from ukg.ext_time_entries limit 10

select min(the_date), max(the_date) from ukg.ext_time_entries 

select * -- emp# 168573, emp_id: 12987528129
from ukg.ext_employees
where last_name = 'aamodt'

select * from ukg.ext_time_entries where employee_account_id = 12987528129 order by the_date



select b.last_name, b.first_name, b.employee_id, a.*
from arkona.xfm_pypclockin a
join ukg.ext_employees b on a.employee_number = b.employee_number
where a.the_date between '07/04/2021' and '07/17/2021'
  and a.pto_hours <> 0
order by b.last_name  

select b.last_name, b.first_name, b.employee_id, a.*
from arkona.xfm_pypclockin a
join ukg.ext_employees b on a.employee_number = b.employee_number
where a.the_date = '07/05/2021'
order by b.last_name  


select * from ukg.ext_time_entries

at this time thinking separate tables for time entries and clock hours, ie, no need for punch times in the clock_hours table
	clock_hours: total time clocked on a given date which is subsequently split into reg_hours and ot_hours
	
comment on table ukg.clock_hours is 'the purpose of this table is to provide a ukg replacement for advantage dds.edwFactClockHours and postgresql arkona.xfm_pypclockin
  one of the choices i am making in this first iteration is that rather than a row for every emp/date, a row for every emp/date where there are clock hours,
  but for attributes such as ot_hours where there are none, eg overtime, a 0 rather than a null';
drop table if exists ukg.clock_hours cascade;
create table ukg.clock_hours (
  employee_id bigint not null,
  employee_number citext not null,
  clock_hours numeric not null,



select * from ads.ext_edw_clock_hours_fact  limit 100

select min(b.the_date), max(b.the_date) -- max 1/22/2019
from ads.ext_edw_clock_hours_fact a
join dds.dim_date b on a.datekey = b.date_key

so what i am thinking

build a ukg.clock_hours table in both advantage and postgresql, populated with data from arkona/advantage 
keepo them both updated, then it becomes a simple substitution
first i want to verify that edwClockHoursFact and arkona.xfm_pypclockin track
start with ads.ext_edw_clock_hours_fact as a direct copy from advantage
stick with 2021 data

so, on a daily basis update the ukg table 


looks like this is the current ads scrape in ads_for_luigi
select * from sls.xfm_fact_clock_hours where overtime_hours <> 0 and pto_hours <> 0 limit 100

select min(the_date), max(the_date) from sls.xfm_fact_clock_hours  -- 4/1/17 -> 12/13/21

select* from ops.ads_extract where task = 'sls_xfm_fact_clock_hours' order by the_date desc limit 100

select (select employee_name from arkona.ext_pymast where pymast_employee_number = a.employee_number), * 
from (
select * 
from arkona.xfm_pypclockin -- september OT is fucked up
where the_date between '10/01/2021' and '12/30/2021') a
full outer join (
select * -- 
from sls.xfm_fact_clock_hours  -- scrape of edwClockHoursFact, does not include auto lunch (666)
where the_date between '10/01/2021' and '12/30/2021'
  and (clock_hours+vacation_hours+pto_hours+holiday_hours <> 0)) b on a.employee_number = b.employee_number and a.the_date = b.the_date
where a.ot_hours <> b.overtime_hours    
-- where a.ot_hours <> b.overtime_hours  



8/29 - 9/4

select * 
from arkona.xfm_pypclockin
where employee_number = '1124436'
  and the_date between '08/29/2021' and '09/04/2021'
order by the_date  

select * -- 
from sls.xfm_fact_clock_hours
where employee_number = '1124436'
  and the_date between '08/29/2021' and '09/04/2021'
order by the_date  















