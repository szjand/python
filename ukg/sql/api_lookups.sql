﻿LOOKUPS

drop table if exists ukg.json_benefit_types cascade;
create unlogged table ukg.json_benefit_types (
response jsonb);
comment on table ukg.json_benefit_types is 'raw scrape of ukg endpoint Benefit Types';

select * from ukg.json_benefit_types

drop table if exists ukg.ext_benefit_types cascade;
create table ukg.ext_benefit_types (
	id integer primary key,
	type_name citext not null,
	base_type citext check (base_type in ('UNKNOWN','HEALTH','LIFE','FSA','OTHER','DENTAL','VISION')),
	plan_type citext check (plan_type in ('ACTIVE','COBRA','SURVIVING_INSURED','TEFRA')));

insert into ukg.ext_benefit_types	
select (b->>'id')::integer,
	b->>'name',
	b->>'base_type',
	b->>'plan_type'
-- select b.*	
from ukg.json_benefit_types a
join jsonb_array_elements(a.response->'items') b on true;
-- select * from ukg.ext_benefit_types;
----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_benefit_plans cascade;
create unlogged table ukg.json_benefit_plans (
response jsonb);
comment on table ukg.json_benefit_plans is 'raw scrape of ukg endpoint Benefit Plans';

drop table if exists ukg.ext_benefit_plans cascade;
create table ukg.ext_benefit_plans (
  id integer primary key,
  plan_name citext not null,
  description citext,
  effective_from date not null,
  effective_to date not null,
  company_provided boolean not null);

insert into ukg.ext_benefit_plans
select (b->>'id')::integer,
	b->>'name',
	b->>'description',
	(b->>'effective_from')::date,
	(b->>'effective_to')::date,
	(b->>'company_provided')::boolean
 -- select b.*	
from ukg.json_benefit_plans a
join jsonb_array_elements(a.response->'items') b on true; 
-- select * from ukg.ext_benefit_plans;
----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_jobs cascade;
create unlogged table ukg.json_jobs (
response jsonb);
comment on table ukg.json_jobs is 'raw scrape of ukg endpoint Jobs';

drop table if exists ukg.ext_jobs cascade;
create table ukg.ext_jobs (
  id bigint primary key,
  job_name citext not null,
  external_id citext,
  payroll_code citext,
  visible boolean not null);

insert into ukg.ext_jobs
select (b->>'id')::bigint,
	b->>'name',
	b->>'external_id',
	b->>'payroll_code',
	(b->>'visible')::boolean
 -- select b.*	
from ukg.json_jobs a
join jsonb_array_elements(a.response->'items') b on true;  
-- select * from ukg.ext_jobs;
----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_deduction_codes cascade;
create unlogged table ukg.json_deduction_codes (
response jsonb);
comment on table ukg.json_deduction_codes is 'raw scrape of ukg endpoint Deduction Codes';

drop table if exists ukg.ext_deduction_codes cascade;
create table ukg.ext_deduction_codes (
  code citext primary key,
  external_id citext,
  deduction_code_name citext not null);

insert into ukg.ext_deduction_codes
select (b->>'id')::integer,
	b->>'external_id',
	b->>'name'
 -- select b.*	
from ukg.json_deduction_codes a
join jsonb_array_elements(a.response->'items') b on true;  
-- select * from ukg.ext_deduction_codes;
----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_pay_types cascade;
create unlogged table ukg.json_pay_types (
response jsonb);
comment on table ukg.json_pay_types is 'raw scrape of ukg endpoint Pay Types';

drop table if exists ukg.ext_pay_types cascade;
create table ukg.ext_pay_types (
  id integer primary key,
  pay_type_name citext not null,
  external_id citext);

insert into ukg.ext_pay_types
select (b->>'id')::integer,
	b->>'name',
	b->>'external_id'
 -- select b.*	
from ukg.json_pay_types a
join jsonb_array_elements(a.response->'items') b on true;  
-- select * from ukg.ext_pay_types;
----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_pay_calc_profiles cascade;
create unlogged table ukg.json_pay_calc_profiles (
response jsonb);
comment on table ukg.json_pay_calc_profiles is 'raw scrape of ukg endpoint Pay Calculation Profiles';

drop table if exists ukg.ext_pay_calc_profiles cascade;
create table ukg.ext_pay_calc_profiles (
  id bigint primary key,
  description citext not null,
  display_name citext not null);

create or replace function ukg.ext_pay_calc_profiles()
returns void as
$BODY$
/*
  select ukg.ext_pay_calc_profiles();
*/
	truncate ukg.ext_pay_calc_profiles;
	insert into ukg.ext_pay_calc_profiles
	select (b->>'id')::bigint,
		b->>'description',
		b->>'display_name'
	from ukg.json_pay_calc_profiles a
	join jsonb_array_elements(a.response->'items') b on true;  
$BODY$
language sql;

select * from ukg.ext_pay_calc_profiles
----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_payroll_types cascade;
create unlogged table ukg.json_payroll_types (
response jsonb);
comment on table ukg.json_payroll_types is 'raw scrape of ukg endpoint Payroll Types';

drop table if exists ukg.ext_payroll_types cascade;
create table ukg.ext_payroll_types (
  id integer primary key,
  display_name citext);

insert into ukg.ext_payroll_types
select (b->>'id')::integer,
	b->>'display_name'
 -- select b.*	
from ukg.json_payroll_types a
join jsonb_array_elements(a.response->'items') b on true;  
-- select * from ukg.ext_payroll_types;
----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_payroll_batch_types cascade;
create unlogged table ukg.json_payroll_batch_types (
response jsonb);
comment on table ukg.json_payroll_batch_types is 'raw scrape of ukg endpoint Payroll Types';

drop table if exists ukg.payroll_batch_types cascade;
create table ukg.payroll_batch_types (
  id integer primary key,
  display_name citext,
  active boolean);

insert into ukg.payroll_batch_types
select (b->>'id')::integer,
	b->>'display_name',
	(b->>'active')::boolean
 -- select b.*	
from ukg.json_payroll_batch_types a
join jsonb_array_elements(a.response->'items') b on true;  
-- select * from ukg.payroll_batch_types;
----------------------------------------------------------------------------------------------------
-- TODO
-- per afton parameters are required
Pay Categories is erroring
{
    "errors": [
        {
            "code": 400,
            "message": "Missing required: owner_id, date, type"
        }
    ],
    "user_messages": [
        {
            "severity": "ERROR",
            "text": "Missing required: owner_id, date, type",
            "details": {
                "code": 400
            }
        }
    ]
}
----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_pay_statement_types cascade;
create unlogged table ukg.json_pay_statement_types (
response jsonb);
comment on table ukg.json_pay_statement_types is 'raw scrape of ukg endpoint Pay Statement Types';

drop table if exists ukg.pay_statement_types cascade;
create table ukg.pay_statement_types (
  id integer primary key,
  display_name citext);

insert into ukg.pay_statement_types
select (b->>'id')::integer,
	b->>'display_name'
 -- select b.*	
from ukg.json_pay_statement_types a
join jsonb_array_elements(a.response->'items') b on true;  
-- select * from ukg.pay_statement_types;
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
drop table if exists ukg.json_earnings_codes cascade;
create unlogged table ukg.json_earnings_codes (
response jsonb);
comment on table ukg.json_earnings_codes is 'raw scrape of ukg endpoint Earning Codes';

drop table if exists ukg.earnings_codes cascade;
create table ukg.earnings_codes (
  id integer primary key,
  earnings_code citext not null,
  earnings_code_name citext not null);
create unique index on ukg.earnings_codes(earnings_code);

create or replace function ukg.ext_earnings_codes()
returns void as
$BODY$
/*
  select ukg.earnings_codes();
*/
	truncate ukg.earnings_codes;
	insert into ukg.earnings_codes
	select (b->>'id')::integer,
		b->>'code',
	  b->>'name'
	from ukg.json_earnings_codes a
	join jsonb_array_elements(a.response->'items') b on true;
$BODY$
language sql;

select * from ukg.earnings_codes where earnings_code <> earnings_code_name
----------------------------------------------------------------------------------------------------















