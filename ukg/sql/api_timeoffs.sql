﻿drop table if exists ukg.json_timeoffs cascade;
create unlogged table ukg.json_timeoffs (
	response jsonb);
comment on table ukg.json_timeoffs is 'raw scrape of ukg endpoint V1->Company->Time Offs; https://beta.rydellvision.com:8888/ukg/timeoffs';
-- select * from ukg.json_timeoffs

drop table if exists ukg.ext_timeoffs cascade;
-- left off available_dynamic_durations: there are no values for these in the actual data
create table ukg.ext_timeoffs (
	id bigint not null,
	parent_id bigint,
	name citext,
	abbreviation citext,
	description citext,
	external_id citext,
	payroll_code citext,
	time_allocation boolean,
	visible boolean,
	requestable boolean,
	populate_scheduled boolean,
	def_ms_per_day integer,
	def_pay_category citext,
	default_counter citext,
	priority integer);
	
select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_timeoffs';	

insert into ukg.ext_timeoffs (id,parent_id,name,abbreviation,description,external_id,
	payroll_code,time_allocation,visible,requestable,populate_scheduled,def_ms_per_day,
	def_pay_category,default_counter,priority)
select (b->>'id')::bigint,
	(b->>'parent_id')::bigint,
	b->>'name',
	b->>'abbreviation',
	b->>'description',
	b->>'external_id',
	b->>'payroll_code',
	(b->>'time_allocation')::boolean, 
	(b->>'visible')::boolean, 
	(b->>'requestable')::boolean,
	(b->>'populate_schedule')::boolean,
	(b->>'def_ms_per_day')::integer,
	b->>'def_pay_category',
	b->>'def_counter',
	(b->>'priority')::integer
from ukg.json_timeoffs a
join jsonb_array_elements(a.response->'time_offs') b on true;

select * from ukg.ext_timeoffs

create or replace function ukg.ext_timeoffs()
  returns void as 
$BODY$
	truncate ukg.ext_timeoffs;
	insert into ukg.ext_timeoffs (id,parent_id,name,abbreviation,description,external_id,
		payroll_code,time_allocation,visible,requestable,populate_scheduled,def_ms_per_day,
		def_pay_category,default_counter,priority)
	select (b->>'id')::bigint,
		(b->>'parent_id')::bigint,
		b->>'name',
		b->>'abbreviation',
		b->>'description',
		b->>'external_id',
		b->>'payroll_code',
		(b->>'time_allocation')::boolean, 
		(b->>'visible')::boolean, 
		(b->>'requestable')::boolean,
		(b->>'populate_schedule')::boolean,
		(b->>'def_ms_per_day')::integer,
		b->>'def_pay_category',
		b->>'def_counter',
		(b->>'priority')::integer
	from ukg.json_timeoffs a
	join jsonb_array_elements(a.response->'time_offs') b on true;
$BODY$
language sql;	