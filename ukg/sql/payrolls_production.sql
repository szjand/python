﻿/*
01/22/22
most of this (basic table structures) was worked out in python_projects\ukg\sql\api_payrolls.sql

not sure i we really need batches
*/
/*
the lookups are superfluous, id and name are included in the tables derived from the api: ukg.ext_payrolls, ukg.ext_pay_statements
select distinct type_id, display_name
from ukg.ext_payrolls

{"items": [{"id": 706, "display_name": "Correction"}, {"id": 709, "display_name": "FUTA Credit Reduction"}, {"id": 65, "display_name": "Historical"}, {"id": 88065, "display_name": "Historical Void"}, {"id": 284864, "display_name": "Parallel"}, {"id": 193, "display_name": "Reconciliation"}, {"id": 284736, "display_name": "Reconciliation (Historical)"}, {"id": 641, "display_name": "Regular"}, {"id": 707, "display_name": "SPA Correction"}, {"id": 4486, "display_name": "Successorship"}, {"id": 705, "display_name": "Supplemental"}, {"id": 129, "display_name": "Void"}, {"id": 284800, "display_name": "Year End Correction"}]}

{"items": [{"id": 101804109, "display_name": "3rd Party Sick"}, {"id": 101804110, "display_name": "Bonus"}, {"id": 101804108, "display_name": "Correction Delta"}, {"id": 101804098, "display_name": "Gross Up"}, {"id": 101804106, "display_name": "Historical"}, {"id": 101804113, "display_name": "Historical Void"}, {"id": 101804103, "display_name": "Manual"}, {"id": 101804101, "display_name": "Manual Off-Cycle"}, {"id": 101804112, "display_name": "Payroll Adjustment"}, {"id": 101804105, "display_name": "Quarter Reconciliation"}, {"id": 101804100, "display_name": "Quarter Reconciliation Delta"}, {"id": 101804104, "display_name": "Quarter Reconciliation Worksheet"}, {"id": 101804114, "display_name": "Quarter Reconciliation Worksheet (Historical)"}, {"id": 101804102, "display_name": "Quarter Reconciliation Worksheet (Historical) Delta"}, {"id": 101804097, "display_name": "Quarter Reconciliation Worksheet Delta"}, {"id": 101804096, "display_name": "Regular"}, {"id": 101804107, "display_name": "Void"}]}

select distinct type_id, type_display_name
from ukg.ext_pay_statements
*/

select * 
from ukg.ext_payrolls a
left join ukg.ext_pay_statements b on a.payroll_id = b.payroll_id
left join ukg.ext_pay_statement_details c on b.payroll_id = b.payroll_id
  and b.pay_statement_id = c.pay_statement_id
limit 300

select * 
from ukg.ext_pay_statements

select * 
from ukg.ext_pay_statement_details

select a.last_name, a.first_name, a.status, a.primary_account_id, a.employee_id, a.employee_number, hire_date, term_date, rehire_date
from ukg.employees a
join (
SELEct primary_account_id 
from ukg.employees
group by primary_account_id
having count(*) > 1) b on a.primary_account_id = b.primary_account_id

Farley,Ethan,Active,12987528611,12987528611,169846
Farley,Ethan,Terminated,12987528611,12987837784,269846


select * 
from ukg.ext_pay_statement_details
where employee_id in (12987528611,12987837784)


although the api spec specifies i am leery, is that correct or do they refer to the employee_id
account_id is the persistent identifier, employee_id is the transient identifier
no current examples of an employees with 2 employee_ids that existed within the payrolls so far
  "employee": {
    "account_id": 12986719383
  },

ok, this is interesting, but i also am currently somewhat confused, because even though i think i updated all 
the ext tables yesterday, they do not yet show this past weeks biweekly payroll  

select pay_period_start, pay_period_end, count(*)
from ukg.ext_pay_statements
group by pay_period_start, pay_period_end

so, lets move those ext tables to luigi and try it again now

select * from ukg.ext_payrolls -- 14 rows
-- doesn't exist in statements yet because i just ran the python stuff consecutively without doing the sql
select * from ukg.ext_pay_statements where payroll_id = 109159473

-- truncate and repopulate ukg.ext_payrolls
create or replace function ukg.ext_payrolls()
returns void as 
$BODY$
/*

*/
  truncate ukg.ext_payrolls;
	insert into ukg.ext_payrolls (payroll_id,name,start_date,end_date,pay_date,type_id,display_name,
		status,ein_id,ein_name,ein_tax_id,billable,direct_deposit_date,sch_submit_date_time,
		inovice_generator_id,adjustment_grab_ytd_from_payroll_id,dflt_pay_statement_type_id,dflt_pay_stub_note,self)	
	select (b->>'id')::integer,
		b->>'name',
		(b->>'start_date')::date, (b->>'end_date')::date, (b->>'pay_date')::date,
		(b->'type'->>'id')::integer, b->'type'->>'display_name',
		b->>'status',
		(b->'ein'->>'id')::integer, b->'ein'->>'ein_name',b->'ein'->>'ein_tax_id',
		(b->>'billable')::boolean,
		(b->>'direct_deposit_date')::date, 
		(b->>'sch_submit_date_time')::date,
		(b->>'invoice_generator_id')::integer, (b->>'adjustment_grab_YTD_from_payroll_id')::integer, (b->>'dflt_pay_statement_type_id')::integer,
		b->>'dflt_pay_stub_note',
		b->'_links'->>'self'
	from ukg.json_payrolls a
	join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true;
$BODY$
language sql;	


select count(*) from ukg.ext_pay_statements  -- 847/1207
-- truncate and repopulate ukg.ext_pay_statements
-- just the base pay_statement data
-- inner join on jsonb_array_elements to exclude payroll_ids with no pay statements
create or replace function ukg.ext_pay_statements()
returns void as 
$BODY$
/*

*/
  truncate ukg.ext_pay_statements;
	insert into ukg.ext_pay_statements
	select a.id, (b->>'id')::bigint, (b->'employee'->>'account_id')::bigint,
		(b->>'pay_date')::date, b->>'status',
		(b->'type'->>'id')::integer, b->'type'->>'display_name',
		(b->>'pay_period_start')::date, (b->>'pay_period_end')::date,
		(b->>'calcUpto')::integer,
		(b->>'emptyEarning')::boolean,
		b->'_links'->>'self'
	from ukg.json_pay_statements a 
	join jsonb_array_elements(a.response->'pay_statements') b on true;
$BODY$
language sql;	

turns out there is no need for a pay_statement_details table, pay statements has all the base data needed
then from json.pay_statement_details, can get the earnings, deductions, payments, etc data


create or replace function ukg.ext_pay_statement_earnings()
returns void as 
$BODY$
/*
select ukg.ext_pay_statement_earnings();
*/
  truncate ukg.ext_pay_statement_earnings;
	insert into ukg.ext_pay_statement_earnings (payroll_id,pay_statement_id,employee_id,earnings_id,
		batch_id,batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_centers)  
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_id,
		(b->>'id')::bigint as earnings_id, 
		(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
		(b->>'hours')::BIGINT as hours,
		(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
		(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
		b->>'type_name' as type_name,
		b->'cost_centers'
	from ukg.json_pay_statement_details a
	left join jsonb_array_elements(a.response->'earnings') b on true;
$BODY$
language sql;

create or replace function ukg.ext_pay_statement_deductions()
returns void as 
$BODY$
/*
select ukg.ext_pay_statement_deductions();
*/
  truncate ukg.ext_pay_statement_deductions;
	insert into ukg.ext_pay_statement_deductions (payroll_id,pay_statement_id,employee_id,deductions_id,
	batch_id,batch_name,deduction_code,deduction_name,ee_amount,er_amount,type_name,cost_centers)  
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_id,
		(b->>'id')::bigint as deductions_id, 
		(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
		b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
		(b->>'ee_amount')::numeric as ee_amount, (b->>'er_amount')::numeric as er_amount,
		b->>'type_name' as type_name,
		b->'cost_centers'
	from ukg.json_pay_statement_details a
	join jsonb_array_elements(a.response->'deductions') b on true;
$BODY$
language sql;

create or replace function ukg.ext_pay_statement_taxes()
returns void as 
$BODY$
/*
select ukg.ext_pay_statement_taxes();
*/
  truncate ukg.ext_pay_statement_taxes;
	insert into ukg.ext_pay_statement_taxes (payroll_id,pay_statement_id,employee_id,taxes_id,
		batch_id,batch_name,ee_amount,er_amount,type_name,company_tax_id,company_tax_code,company_tax_name,
		company_tax_abbrev,ee_gross_wage,er_gross_wage,ee_subject_wage,er_subject_wage,
		ee_gross_subject_wage,er_gross_subject_wage)
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_id,
		(b->>'id')::bigint as taxes_id, 
		(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
		(b->>'ee_amount')::numeric as ee_amount,
		(b->>'er_amount')::numeric as er_amount,
		b->>'type_name' as type_name,
		(b->'company_tax'->>'id')::integer as company_tax_id, 
		b->'company_tax'->>'code' as company_tax_code,
		b->'company_tax'->>'name' as company_tax_name,
		b->'company_tax'->>'abbrev' as company_tax_abbrev,
		(b->>'ee_gross_wage')::numeric as ee_grosss_wage,
		(b->>'er_gross_wage')::numeric as er_grosss_wage,
		(b->>'ee_subject_wage')::numeric as ee_subject_wage,
		(b->>'er_subject_wage')::numeric as er_subject_wage,
		(b->>'ee_gross_subject_wage')::numeric as ee_gross_subject_wage,
		(b->>'er_gross_subject_wage')::numeric as er_gross_subject_wage
	from ukg.json_pay_statement_details a
	join jsonb_array_elements(a.response->'taxes') b on true;
$BODY$
language sql;


create or replace function ukg.ext_pay_statement_messages()
returns void as 
$BODY$
/*
select ukg.ext_pay_statement_messages();
*/
  truncate ukg.ext_pay_statement_messages;
	insert into ukg.ext_pay_statement_messages(payroll_id,pay_statement_id,employee_id,messages_id,
		batch_id,batch_name,message_text,message_severity,type_name)
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_id,
		(b->>'id')::bigint as messages_id, 
		(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
		b->'message'->>'text' as message_text,
		b->'message'->>'severity' as message_severity,
		b->>'type_name' as type_name
	from ukg.json_pay_statement_details a
	join jsonb_array_elements(a.response->'messages') b on true;	
$BODY$
language sql;

create or replace function ukg.ext_pay_statement_payments()
returns void as 
$BODY$
/*
select ukg.ext_pay_statement_payments();
*/
  truncate ukg.ext_pay_statement_payments;
	insert into ukg.ext_pay_statement_payments(payroll_id,pay_statement_id,employee_id,
		payments_id,batch_id,batch_name,ee_amount,type_name)
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_id,
		(b->>'id')::bigint as messages_id, 
		(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
		(b->>'ee_amount')::numeric as ee_amount,
		b->>'type_name' as type_name
	from ukg.json_pay_statement_details a
	join jsonb_array_elements(a.response->'payments') b on true;	 
$BODY$
language sql;

---------------------------------------------------------------
so, looking at the spreadsheet combining payrolls, payroll statements and payroll statement details


-- payroll start/end = pay statement payroll start/end
select * 
from ukg.ext_payrolls a
join ukg.ext_pay_statements b on a.payroll_id = b.payroll_id
where a.start_date <> b.pay_period_start or a.end_date <> b.pay_period_end

-- all true
select distinct billable from ukg.ext_payrolls

-- pay date & status are the same
select * 
from ukg.ext_payrolls a
join ukg.ext_pay_statements b on a.payroll_id = b.payroll_id
where a.pay_date <> b.pay_date or a.status <> b.status


-- all false
select distinct empty_earning from ukg.ext_pay_statements

drop table if exists ukg.payrolls cascade;
-- comprised of payrolls and pay statements
-- leaves out calc_upto & empty_earning
create table ukg.payrolls (
  payroll_id integer primary key,  -- payrolls
  payroll_name citext not null, 
  payroll_start_date date not null,
  payroll_end_date date not null,
  pay_date date not null,
  payroll_type citext not null, -- display_name
  status citext not null,
  ein_name citext not null,
  billable boolean); 

drop table if exists ukg.pay_statements cascade;
create table ukg.pay_statements (  
  payroll_id integer not null references ukg.payrolls(payroll_id),
  pay_statement_id bigint primary key,  -- pay_statements
  employee_account_id bigint not null,
  pay_statement_type citext not null); -- display_name


/*
01/23
i am struggling with the etl
for payrolls, it is reasonable to do a trunc/repop 
but on the larger tables, pay_statements and pay_statement_details, seems pointless
although, i don't know if payrolls can be updated after being finalized

so do i do 
	1. trunc/repop
	2. new rows only
	3. upsert (this eventually gets into updating every row on very large tables)
	4. new rows and test for changed rows to surface when and if they ever actually change
	5. type 2
leaning towards 4
so, for a while, this will still essentially be some large tables, a few months down the line, the pay_statement_details is going to LARGE
but, by that time, should have a better sense of whether ony of these attributes actually change over time
easiest way to do cdc is with a hash attribute
*/

so, lets start with payrolls

api: all payrolls
populate ext_payrolls table with all data
insert new rows into ukg.payrolls
separate class test for changed rows, any changed rows  will generate an email 

change function ukg.ext_payrolls() to ukg.update_payrolls
	which will now populate ukg.ext_payrolls
	then add new rows to ukg.payrolls

	but wait, do i now need the ukg.ext_payrolls table at all
	just add the new rows from ukg.json_payrolls

	then a separate class, something like PayrollsCDC, with an email function, that just goes through
-------------------------------------------------------------------------------------------
--< seems to work for payrolls
-------------------------------------------------------------------------------------------
-- no longer used
drop table ukg.ext_payrolls cascade;

drop table if exists ukg.payrolls cascade;
create table ukg.payrolls (
  payroll_id integer primary key,  -- payrolls
  payroll_name citext not null, 
  payroll_start_date date not null,
  payroll_end_date date not null,
  pay_date date not null,
  payroll_type citext not null, -- display_name
  status citext not null,
  ein_name citext not null,
  billable boolean); 

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'payrolls'; 
  
insert into ukg.payrolls(payroll_id,payroll_name,payroll_start_date,payroll_end_date,pay_date,payroll_type,status,ein_name,billable)
select (b->>'id')::integer as payroll_id,
	b->>'name' as payroll_name,
	coalesce((b->>'start_date')::date, '12/31/9999'::date) as payroll_start_date, 
	coalesce((b->>'end_date')::date, '12/31/9999'::date) as payroll_end_date, 
	coalesce((b->>'pay_date')::date, '12/31/9999'::date) as pay_date,
	b->'type'->>'display_name' as payroll_type,
	b->>'status',
	b->'ein'->>'ein_name' as ein_name,
	(b->>'billable')::boolean
from ukg.json_payrolls a
join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true
where not exists (
  select 1
  from ukg.payrolls
  where payroll_id = (b->>'id')::integer);
  
-- cdc
-- this will go in the class that generates an email if there is changed data
select * 
from ( -- from ukg.payrolls 
	select payroll_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,payroll_name,payroll_start_date,payroll_end_date,pay_date,payroll_type,status,ein_name,billable 
				from ukg.payrolls
				where payroll_id = a.payroll_id) z)
	from ukg.payrolls a) aa
join (  -- from ukg.json_payrolls
	select payroll_id, md5(a::text) as hash
	from (  
		select (b->>'id')::integer as payroll_id,
			b->>'name' as payroll_name,
			coalesce((b->>'start_date')::date, '12/31/9999'::date) as payroll_start_date, 
			coalesce((b->>'end_date')::date, '12/31/9999'::date) as payroll_end_date, 
			coalesce((b->>'pay_date')::date, '12/31/9999'::date) as pay_date,
			b->'type'->>'display_name' as payroll_type,
			b->>'status',
			b->'ein'->>'ein_name' as ein_name,
			(b->>'billable')::boolean
		from ukg.json_payrolls a
		join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true) a) bb on aa.payroll_id = bb.payroll_id
			and aa.hash <> bb.hash
-------------------------------------------------------------------------------------------
--/> seems to work for payrolls
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
--< seems to work for pay statements
------------------------------------------------------------------------------------------- 
/*
-- i can't yet tell what is up with the 2 employee_ids
-- this appears to say that the active employee_id is always the same as the primary_account_id
-- but that does not make sense because the inactive employee_id is LESS THAN the primary_account_id
-- but currently there are no examples where an employee changed stores or termed and rehired since going live
select a.last_name, a.first_name, a.employee_id, a.primary_account_id, a.status, term_date, rehire_date 
from ukg.employees a
join (
select primary_account_id
from ukg.employees
group by primary_account_id 
having count(*) > 1) b on a.primary_account_id = b.primary_account_id
*/

-- no longer used
drop table ukg.ext_pay_statements cascade;

drop table if exists ukg.pay_statements cascade;
create table ukg.pay_statements (  
  payroll_id integer not null references ukg.payrolls(payroll_id),
  pay_statement_id bigint primary key,  -- pay_statements
  employee_account_id bigint not null,
  pay_statement_type citext not null); -- display_name

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'pay_statements';   
  
insert into ukg.pay_statements(payroll_id,pay_statement_id,employee_account_id,pay_statement_type)
select a.id as payroll_id,
  (b->>'id')::bigint as pay_statement_id,
  (b->'employee'->>'account_id')::bigint as employee_account_id,
  b->'type'->>'display_name' as pay_statement_type
from ukg.json_pay_statements a 
join jsonb_array_elements(a.response->'pay_statements') b on true
where not exists (
  select 1
  from ukg.pay_statements
  where pay_statement_id = (b->>'id')::bigint);  

-- cdc
-- this will go in the class that generates an email if there is changed data
select * 
from ( -- from ukg.pay_statements
	select pay_statement_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,pay_statement_type
				from ukg.pay_statements
				where pay_statement_id = a.pay_statement_id) z)
	from ukg.pay_statements a) aa
	join (  -- from ukg.json_pay_statements
		select pay_statement_id, md5(a::text) as hash
		from (  
			select a.id as payroll_id,
				(b->>'id')::bigint as pay_statement_id,
				(b->'employee'->>'account_id')::bigint as employee_account_id,
				b->'type'->>'display_name' as pay_statement_type
			from ukg.json_pay_statements a 
			join jsonb_array_elements(a.response->'pay_statements') b on true) a) bb on aa.pay_statement_id = bb.pay_statement_id
				and aa.hash <> bb.hash  
-------------------------------------------------------------------------------------------
--/> seems to work for pay statements
------------------------------------------------------------------------------------------- 			


-------------------------------------------------------------------------------------------
--< seems to work for pay statement earnings
------------------------------------------------------------------------------------------- 	
/*
select * 
from ukg.ext_cost_centers

an issue, using me as an example, the cost_centers object in the earnings object of pay_statement_details
shows the following json
the confusion is, there is no cost center with an index of 9 or an id of 51655921124
  "cost_centers": [
    {
      "index": 0,
      "value": {
        "id": 51655491070,
        "name": "Database Developer",
        "display_name": "Rydell GM/Cartiva/Database Developer"
      }
    },
    {
      "index": 9,
      "value": {
        "id": 51655921124,
        "name": "Database Developer",
        "display_name": "Database Developer"
      }
    }
  ]
 so, what is that second entry? is it a job?
yep, it shows up in the cost-center-jobs api 
        {
            "id": 51655921124,
            "job_category": "Cartiva",
            "name": "Database Developer",
            "visible": true,
            "applicant_tracking_display": true,
            "applicant_tracking_display_only": false
        },
so this becomes a TODO, per jeri, most of the time the job name = cost center name, the problem
of course being most of the time
i don't currently understand the use of cost_centers vs jobs  

so, for now, in earnings and any other pay_statement_details with a cost_centers attribute,
i will leave it as a jsonb object in the table for now      
*/

-- also have not yet implemented batchs, don't know how they are used, but they are included in this table
-- no longer used
drop table ukg.ext_pay_statement_earnings cascade;

-- fuck, i went ahead and added the cost center and job shit, don't know if i will do it in all tables
-- it is probably 6 unnecessary attributes
drop table if exists ukg.pay_statement_earnings cascade;
create table ukg.pay_statement_earnings (
  payroll_id integer not null references ukg.payrolls(payroll_id),
  pay_statement_id bigint not null references ukg.pay_statements(pay_statement_id),
  employee_account_id bigint not null,
  earnings_id bigint primary key,
  batch_id integer not null, 
  batch_name citext not null,
  hours bigint,
  earning_id integer not null, 
  earning_code citext not null,
  earning_name citext not null,
  base_rate numeric,
  ee_amount numeric,
  type_name citext not null,
  cost_center_id bigint,
  cost_center_name citext,
  cost_center_display_name citext,
  cost_center_job_id bigint,
  cost_center_job_name citext,
  cost_center_job_display_name citext);
--   cost_centers jsonb);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'pay_statement_earnings';  

create or replace function ukg.update_pay_statement_earnings()
returns void as
$BODY$
/*
  select ukg.update_pay_statement_earnings();
*/
	insert into ukg.pay_statement_earnings (payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
		batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
		cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name)  
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_id,
		(b->>'id')::bigint as earnings_id, 
		(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
		(b->>'hours')::BIGINT as hours,
		(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
		(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
		b->>'type_name' as type_name,
		(c->'value'->>'id')::bigint as cost_center_id,
		c->'value'->>'name' as cost_center_name,
		c->'value'->>'display_name' as cost_center_display_name,
		(d->'value'->>'id')::BIGINT as cost_center_job_id,
		d->'value'->>'name' as cost_center_job_name,
		d->'value'->>'display_name' as cost_center_job_display_name
	from ukg.json_pay_statement_details a
	left join jsonb_array_elements(a.response->'earnings') b on true
	left join jsonb_array_elements(b->'cost_centers') c on true 
		and c->>'index' = '0'
	left join jsonb_array_elements(b->'cost_centers') d on true 
		and d->>'index' = '9'
	where not exists (
		select 1
		from ukg.pay_statement_earnings
		where earnings_id = (b->>'id')::bigint);
$BODY$
language sql;		

-- cdc
-- this will go in the class that generates an email if there is changed data
select * 
from ( -- from ukg.pay_statement_earnings
	select earnings_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
					batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
					cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
				from ukg.pay_statement_earnings
				where earnings_id = a.earnings_id) z)
	from ukg.pay_statement_earnings a) aa
	join (  -- from ukg.json_pay_statement_details
		select earnings_id, md5(a::text) as hash
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_id,
				(b->>'id')::bigint as earnings_id, 
				(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
				(b->>'hours')::BIGINT as hours,
				(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
				(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
				b->>'type_name' as type_name,
				(c->'value'->>'id')::bigint as cost_center_id,
				c->'value'->>'name' as cost_center_name,
				c->'value'->>'display_name' as cost_center_display_name,
				(d->'value'->>'id')::BIGINT as cost_center_job_id,
				d->'value'->>'name' as cost_center_job_name,
				d->'value'->>'display_name' as cost_center_job_display_name
			from ukg.json_pay_statement_details a
			left join jsonb_array_elements(a.response->'earnings') b on true
			left join jsonb_array_elements(b->'cost_centers') c on true 
				and c->>'index' = '0'
			left join jsonb_array_elements(b->'cost_centers') d on true 
				and d->>'index' = '9') a) bb on aa.earnings_id = bb.earnings_id
					and aa.hash <> bb.hash;
-------------------------------------------------------------------------------------------
--/> seems to work for pay statement earnings
------------------------------------------------------------------------------------------- 	

-------------------------------------------------------------------------------------------
--< seems to work for pay statement deductions
------------------------------------------------------------------------------------------- 	 		
-- figured out the cost center stuff, added those attributes to earnings, not going to add it to this table
-- same thing with batches, it it turns out we need this stuff, it can be added
drop table if exists ukg.pay_statement_deductions cascade;
create table ukg.pay_statement_deductions (
  payroll_id integer not null references ukg.payrolls(payroll_id),
  pay_statement_id bigint null references ukg.pay_statements(pay_statement_id),
  employee_account_id bigint not null,
  deductions_id bigint primary key,
  deduction_code citext not null,
  deduction_name citext not null,
  ee_amount numeric,
  er_amount numeric,
  type_name citext not null);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'pay_statement_deductions';  	

create or replace function ukg.update_pay_statement_deductions()
returns void as
$BODY$
/*
	select ukg.update_pay_statement_deductions();
*/
	insert into ukg.pay_statement_deductions (payroll_id,pay_statement_id,
		employee_account_id,deductions_id,deduction_code,deduction_name,ee_amount,er_amount,type_name)  
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_account_id,
		(b->>'id')::bigint as deductions_id, 
		b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
		(b->>'ee_amount')::numeric as ee_amount, (b->>'er_amount')::numeric as er_amount,
		b->>'type_name' as type_name
	from ukg.json_pay_statement_details a
	join jsonb_array_elements(a.response->'deductions') b on true
	where not exists (
		select 1
		from ukg.pay_statement_deductions
		where deductions_id = (b->>'id')::bigint);  			
$BODY$
language sql;

-- cdc
-- this will go in the class that generates an email if there is changed data
select * 
from ( -- from ukg.pay_statement_deductions
	select deductions_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,deductions_id,deduction_code,
					deduction_name,ee_amount,er_amount,type_name
				from ukg.pay_statement_deductions
				where deductions_id = a.deductions_id) z)
	from ukg.pay_statement_deductions a) aa
	join (  -- from ukg.json_pay_statement_details
		select deductions_id, md5(a::text) as hash
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_account_id,
				(b->>'id')::bigint as deductions_id, 
				b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
				(b->>'ee_amount')::numeric as ee_amount, (b->>'er_amount')::numeric as er_amount,
				b->>'type_name' as type_name
			from ukg.json_pay_statement_details a
			join jsonb_array_elements(a.response->'deductions') b on true) a) bb on aa.deductions_id = bb.deductions_id
					and aa.hash <> bb.hash;			
-------------------------------------------------------------------------------------------
--/> seems to work for pay statement deductions
-------------------------------------------------------------------------------------------			

-------------------------------------------------------------------------------------------
--< seems to work for pay statement taxes
------------------------------------------------------------------------------------------- 
drop table if exists ukg.pay_statement_taxes cascade;
create table ukg.pay_statement_taxes (
  payroll_id integer not null references ukg.payrolls(payroll_id),
  pay_statement_id bigint null references ukg.pay_statements(pay_statement_id),
  employee_id bigint not null,
  taxes_id bigint primary key,
  batch_id integer not null, 
  batch_name citext not null,
  ee_amount numeric,
  er_amount numeric,
  type_name citext not null,
  company_tax_id integer,
  company_tax_code citext,
  company_tax_name citext,
  company_tax_abbrev citext,
  ee_gross_wage numeric,
  er_gross_wage numeric,
  ee_subject_wage numeric,
  er_subject_wage numeric,
  ee_gross_subject_wage numeric,
  er_gross_subject_wage numeric);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'pay_statement_taxes';  

create or replace function ukg.update_pay_statement_taxes()
returns void as
$BODY$
/*
	select ukg.update_pay_statement_taxes();
*/
	insert into ukg.pay_statement_taxes (payroll_id,pay_statement_id,employee_id,taxes_id,batch_id,
		batch_name,ee_amount,er_amount,type_name,company_tax_id,company_tax_code,company_tax_name,
		company_tax_abbrev,ee_gross_wage,er_gross_wage,ee_subject_wage,er_subject_wage,
		ee_gross_subject_wage,er_gross_subject_wage)
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_id,
		(b->>'id')::bigint as taxes_id, 
		(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
		(b->>'ee_amount')::numeric as ee_amount,
		(b->>'er_amount')::numeric as er_amount,
		b->>'type_name' as type_name,
		(b->'company_tax'->>'id')::integer as company_tax_id, 
		b->'company_tax'->>'code' as company_tax_code,
		b->'company_tax'->>'name' as company_tax_name,
		b->'company_tax'->>'abbrev' as company_tax_abbrev,
		(b->>'ee_gross_wage')::numeric as ee_grosss_wage,
		(b->>'er_gross_wage')::numeric as er_grosss_wage,
		(b->>'ee_subject_wage')::numeric as ee_subject_wage,
		(b->>'er_subject_wage')::numeric as er_subject_wage,
		(b->>'ee_gross_subject_wage')::numeric as ee_gross_subject_wage,
		(b->>'er_gross_subject_wage')::numeric as er_gross_subject_wage
	from ukg.json_pay_statement_details a
	join jsonb_array_elements(a.response->'taxes') b on true
	where not exists (
		select 1
		from ukg.pay_statement_taxes
		where taxes_id = (b->>'id')::bigint);  			
$BODY$
language sql;

-- cdc
-- this will go in the class that generates an email if there is changed data
select * 
from ( -- from ukg.pay_statement_taxes
	select taxes_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_id,taxes_id,batch_id,
					batch_name,ee_amount,er_amount,type_name,company_tax_id,company_tax_code,company_tax_name,
					company_tax_abbrev,ee_gross_wage,er_gross_wage,ee_subject_wage,er_subject_wage,
					ee_gross_subject_wage,er_gross_subject_wage
				from ukg.pay_statement_taxes
				where taxes_id = a.taxes_id) z)
	from ukg.pay_statement_taxes a) aa
	join (  -- from ukg.json_pay_statement_details
		select taxes_id, md5(a::text) as hash
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_id,
				(b->>'id')::bigint as taxes_id, 
				(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
				(b->>'ee_amount')::numeric as ee_amount,
				(b->>'er_amount')::numeric as er_amount,
				b->>'type_name' as type_name,
				(b->'company_tax'->>'id')::integer as company_tax_id, 
				b->'company_tax'->>'code' as company_tax_code,
				b->'company_tax'->>'name' as company_tax_name,
				b->'company_tax'->>'abbrev' as company_tax_abbrev,
				(b->>'ee_gross_wage')::numeric as ee_grosss_wage,
				(b->>'er_gross_wage')::numeric as er_grosss_wage,
				(b->>'ee_subject_wage')::numeric as ee_subject_wage,
				(b->>'er_subject_wage')::numeric as er_subject_wage,
				(b->>'ee_gross_subject_wage')::numeric as ee_gross_subject_wage,
				(b->>'er_gross_subject_wage')::numeric as er_gross_subject_wage
			from ukg.json_pay_statement_details a
			join jsonb_array_elements(a.response->'taxes') b on true) a) bb on aa.taxes_id = bb.taxes_id
					and aa.hash <> bb.hash;	 
-------------------------------------------------------------------------------------------
--/> seems to work for pay statement taxes
-------------------------------------------------------------------------------------------		

-------------------------------------------------------------------------------------------
--< seems to work for pay statement messages
-------------------------------------------------------------------------------------------		
-- removed batch data
drop table if exists ukg.pay_statement_messages cascade;
create table ukg.pay_statement_messages (
  payroll_id integer not null references ukg.payrolls(payroll_id),
  pay_statement_id bigint null references ukg.pay_statements(pay_statement_id),
  employee_account_id bigint not null,
  messages_id bigint primary key,
	message_text citext,
	message_severity citext,
	type_name citext);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'pay_statement_messages';   

create or replace function ukg.update_pay_statement_messages()
returns void as
$BODY$
/*
	select ukg.update_pay_statement_messages();
*/
	insert into ukg.pay_statement_messages(payroll_id,pay_statement_id,
		employee_account_id,messages_id,message_text,message_severity,type_name)
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_account_id,
		(b->>'id')::bigint as messages_id, 
		b->'message'->>'text' as message_text,
		b->'message'->>'severity' as message_severity,
		b->>'type_name' as type_name
	from ukg.json_pay_statement_details a
	join jsonb_array_elements(a.response->'messages') b on true
	where not exists (
		select 1
		from ukg.pay_statement_messages
		where messages_id = (b->>'id')::bigint);  			
$BODY$
language sql;

-- cdc
-- this will go in the class that generates an email if there is changed data
select * 
from ( -- from ukg.pay_statement_messages
	select messages_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,messages_id,
					message_text,message_severity,type_name
				from ukg.pay_statement_messages
				where messages_id = a.messages_id) z)
	from ukg.pay_statement_messages a) aa
	join (  -- from ukg.json_pay_statement_details
		select messages_id, md5(a::text) as hash
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_account_id,
				(b->>'id')::bigint as messages_id, 
				b->'message'->>'text' as message_text,
				b->'message'->>'severity' as message_severity,
				b->>'type_name' as type_name
			from ukg.json_pay_statement_details a
			join jsonb_array_elements(a.response->'messages') b on true) a) bb on aa.messages_id = bb.messages_id
					and aa.hash <> bb.hash;	 
-------------------------------------------------------------------------------------------
--/> seems to work for pay statement messages
-------------------------------------------------------------------------------------------		

-------------------------------------------------------------------------------------------
--< seems to work for pay statement payments
-------------------------------------------------------------------------------------------		
-- removed batch data
drop table if exists ukg.pay_statement_payments cascade;
create table ukg.pay_statement_payments (
  payroll_id integer not null references ukg.payrolls(payroll_id),
  pay_statement_id bigint null references ukg.pay_statements(pay_statement_id),
  employee_account_id bigint not null,
  payments_id bigint primary key,
	ee_amount numeric,
	type_name citext);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'pay_statement_payments';  

create or replace function ukg.update_pay_statement_payments()
returns void as
$BODY$
/*
	select ukg.update_pay_statement_payments();
*/
	insert into ukg.pay_statement_payments(payroll_id,pay_statement_id,employee_account_id,payments_id,ee_amount,type_name)
	select a.payroll_id, a.pay_statement_id, 
		(a.response->'employee'->>'account_id')::bigint as employee_account_id,
		(b->>'id')::bigint as payments_id, 
		(b->>'ee_amount')::numeric as ee_amount,
		b->>'type_name' as type_name
	from ukg.json_pay_statement_details a
	join jsonb_array_elements(a.response->'payments') b on true    
	where not exists (
		select 1
		from ukg.pay_statement_payments
		where payments_id = (b->>'id')::bigint);  			
$BODY$
language sql;

-- cdc
-- this will go in the class that generates an email if there is changed data
select * 
from ( -- from ukg.pay_statement_payments
	select payments_id, 
		(
			select md5(z::text) as hash
			from (
				select payroll_id,pay_statement_id,employee_account_id,payments_id,ee_amount,type_name
				from ukg.pay_statement_payments
				where payments_id = a.payments_id) z)
	from ukg.pay_statement_payments a) aa
	join (  -- from ukg.json_pay_statement_details
		select payments_id, md5(a::text) as hash
		from (  
			select a.payroll_id, a.pay_statement_id, 
				(a.response->'employee'->>'account_id')::bigint as employee_account_id,
				(b->>'id')::bigint as payments_id, 
				(b->>'ee_amount')::numeric as ee_amount,
				b->>'type_name' as type_name
			from ukg.json_pay_statement_details a
			join jsonb_array_elements(a.response->'payments') b on true ) a) bb on aa.payments_id = bb.payments_id
					and aa.hash <> bb.hash;	 


-------------------------------------------------------------------------------------------
--/> seems to work for pay statement payments
-------------------------------------------------------------------------------------------		

-- and finally the cdc function, until i have a better sense of how this works out, just lump them all together
-- DROP FUNCTION ukg.payrollcdc();
create or replace function ukg.payroll_cdc()
returns integer as
$BODY$
/*
  select * from ukg.payroll_cdc()
*/
	select (
		select count(*)::integer 
		from ( -- from ukg.payrolls 
			select payroll_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,payroll_name,payroll_start_date,payroll_end_date,pay_date,payroll_type,status,ein_name,billable 
						from ukg.payrolls
						where payroll_id = a.payroll_id) z)
			from ukg.payrolls a) aa
		join (  -- from ukg.json_payrolls
			select payroll_id, md5(a::text) as hash
			from (  
				select (b->>'id')::integer as payroll_id,
					b->>'name' as payroll_name,
					coalesce((b->>'start_date')::date, '12/31/9999'::date) as payroll_start_date, 
					coalesce((b->>'end_date')::date, '12/31/9999'::date) as payroll_end_date, 
					coalesce((b->>'pay_date')::date, '12/31/9999'::date) as pay_date,
					b->'type'->>'display_name' as payroll_type,
					b->>'status',
					b->'ein'->>'ein_name' as ein_name,
					(b->>'billable')::boolean
				from ukg.json_payrolls a
				join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true) a) bb on aa.payroll_id = bb.payroll_id
					and aa.hash <> bb.hash)
	+ (
		select count(*)::integer
		from ( -- from ukg.pay_statements
			select pay_statement_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_account_id,pay_statement_type
						from ukg.pay_statements
						where pay_statement_id = a.pay_statement_id) z)
			from ukg.pay_statements a) aa
			join (  -- from ukg.json_pay_statements
				select pay_statement_id, md5(a::text) as hash
				from (  
					select a.id as payroll_id,
						(b->>'id')::bigint as pay_statement_id,
						(b->'employee'->>'account_id')::bigint as employee_account_id,
						b->'type'->>'display_name' as pay_statement_type
					from ukg.json_pay_statements a 
					join jsonb_array_elements(a.response->'pay_statements') b on true) a) bb on aa.pay_statement_id = bb.pay_statement_id
						and aa.hash <> bb.hash)  	
	+ (
		select count(*)::integer
		from ( -- from ukg.pay_statement_earnings
			select earnings_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_account_id,earnings_id,batch_id,
							batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_center_id,
							cost_center_name,cost_center_display_name,cost_center_job_id,cost_center_job_name,cost_center_job_display_name
						from ukg.pay_statement_earnings
						where earnings_id = a.earnings_id) z)
			from ukg.pay_statement_earnings a) aa
			join (  -- from ukg.json_pay_statement_details
				select earnings_id, md5(a::text) as hash
				from (  
					select a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_id,
						(b->>'id')::bigint as earnings_id, 
						(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
						(b->>'hours')::BIGINT as hours,
						(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
						(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
						b->>'type_name' as type_name,
						(c->'value'->>'id')::bigint as cost_center_id,
						c->'value'->>'name' as cost_center_name,
						c->'value'->>'display_name' as cost_center_display_name,
						(d->'value'->>'id')::BIGINT as cost_center_job_id,
						d->'value'->>'name' as cost_center_job_name,
						d->'value'->>'display_name' as cost_center_job_display_name
					from ukg.json_pay_statement_details a
					left join jsonb_array_elements(a.response->'earnings') b on true
					left join jsonb_array_elements(b->'cost_centers') c on true 
						and c->>'index' = '0'
					left join jsonb_array_elements(b->'cost_centers') d on true 
						and d->>'index' = '9') a) bb on aa.earnings_id = bb.earnings_id
							and aa.hash <> bb.hash)				
	+ (
		select count(*)::integer
		from ( -- from ukg.pay_statement_deductions
			select deductions_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_account_id,deductions_id,deduction_code,
							deduction_name,ee_amount,er_amount,type_name
						from ukg.pay_statement_deductions
						where deductions_id = a.deductions_id) z)
			from ukg.pay_statement_deductions a) aa
			join (  -- from ukg.json_pay_statement_details
				select deductions_id, md5(a::text) as hash
				from (  
					select a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_account_id,
						(b->>'id')::bigint as deductions_id, 
						b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
						(b->>'ee_amount')::numeric as ee_amount, (b->>'er_amount')::numeric as er_amount,
						b->>'type_name' as type_name
					from ukg.json_pay_statement_details a
					join jsonb_array_elements(a.response->'deductions') b on true) a) bb on aa.deductions_id = bb.deductions_id
							and aa.hash <> bb.hash)		
	+ (
		select count(*)::integer
		from ( -- from ukg.pay_statement_taxes
			select taxes_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_id,taxes_id,batch_id,
							batch_name,ee_amount,er_amount,type_name,company_tax_id,company_tax_code,company_tax_name,
							company_tax_abbrev,ee_gross_wage,er_gross_wage,ee_subject_wage,er_subject_wage,
							ee_gross_subject_wage,er_gross_subject_wage
						from ukg.pay_statement_taxes
						where taxes_id = a.taxes_id) z)
			from ukg.pay_statement_taxes a) aa
			join (  -- from ukg.json_pay_statement_details
				select taxes_id, md5(a::text) as hash
				from (  
					select a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_id,
						(b->>'id')::bigint as taxes_id, 
						(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
						(b->>'ee_amount')::numeric as ee_amount,
						(b->>'er_amount')::numeric as er_amount,
						b->>'type_name' as type_name,
						(b->'company_tax'->>'id')::integer as company_tax_id, 
						b->'company_tax'->>'code' as company_tax_code,
						b->'company_tax'->>'name' as company_tax_name,
						b->'company_tax'->>'abbrev' as company_tax_abbrev,
						(b->>'ee_gross_wage')::numeric as ee_grosss_wage,
						(b->>'er_gross_wage')::numeric as er_grosss_wage,
						(b->>'ee_subject_wage')::numeric as ee_subject_wage,
						(b->>'er_subject_wage')::numeric as er_subject_wage,
						(b->>'ee_gross_subject_wage')::numeric as ee_gross_subject_wage,
						(b->>'er_gross_subject_wage')::numeric as er_gross_subject_wage
					from ukg.json_pay_statement_details a
					join jsonb_array_elements(a.response->'taxes') b on true) a) bb on aa.taxes_id = bb.taxes_id
							and aa.hash <> bb.hash)	 			
	+ (
		select count(*)::integer 
		from ( -- from ukg.pay_statement_messages
			select messages_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_account_id,messages_id,
							message_text,message_severity,type_name
						from ukg.pay_statement_messages
						where messages_id = a.messages_id) z)
			from ukg.pay_statement_messages a) aa
			join (  -- from ukg.json_pay_statement_details
				select messages_id, md5(a::text) as hash
				from (  
					select a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_account_id,
						(b->>'id')::bigint as messages_id, 
						b->'message'->>'text' as message_text,
						b->'message'->>'severity' as message_severity,
						b->>'type_name' as type_name
					from ukg.json_pay_statement_details a
					join jsonb_array_elements(a.response->'messages') b on true) a) bb on aa.messages_id = bb.messages_id
							and aa.hash <> bb.hash)	
	+ (
		select count(*)::integer
		from ( -- from ukg.pay_statement_payments
			select payments_id, 
				(
					select md5(z::text) as hash
					from (
						select payroll_id,pay_statement_id,employee_account_id,payments_id,ee_amount,type_name
						from ukg.pay_statement_payments
						where payments_id = a.payments_id) z)
			from ukg.pay_statement_payments a) aa
			join (  -- from ukg.json_pay_statement_details
				select payments_id, md5(a::text) as hash
				from (  
					select a.payroll_id, a.pay_statement_id, 
						(a.response->'employee'->>'account_id')::bigint as employee_account_id,
						(b->>'id')::bigint as payments_id, 
						(b->>'ee_amount')::numeric as ee_amount,
						b->>'type_name' as type_name
					from ukg.json_pay_statement_details a
					join jsonb_array_elements(a.response->'payments') b on true ) a) bb on aa.payments_id = bb.payments_id
							and aa.hash <> bb.hash);		
$BODY$
language sql;															


select a.* 
from ukg.pay_statement_earnings a
join ukg.employees b on a.employee_account_id = b.employee_id
  and b.last_name = 'croaker'


select * 
from ukg.payrolls a
join ukg.pay_statements b on a.payroll_id = b.payroll_id
  and b.employee_Account_id = 12987528581
where a.payroll_id = 109196712


-- these are the consultants

	select 
	  case
	    when a.store_code = 'RY1' then 'GM'
	    when a.store_code = 'RY2' then 'HN'
	  end as store, a.employee_number, a.last_name, a.first_name
	from sls.personnel a
	inner join sls.team_personnel c on a.employee_number = c.employee_number
		and c.from_Date < (select first_day_of_next_month from sls.months
			where open_closed = 'open')
		and c.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')
	inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
		and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
		and d.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')									

select distinct payroll_name from ukg.payrolls

looks like draw checks have an earnings type_name of Earning (auto) kim concurs
select a.payroll_start_date, a.payroll_end_date,
  b.employee_account_id, c.last_name, c.first_name,
  d.ee_amount
from ukg.payrolls a
join ukg.pay_statements b on a.payroll_id = b.payroll_id
join ukg.employees c on b.employee_account_id = c.employee_id
join ukg.pay_statement_earnings d on a.payroll_id = d.payroll_id
  and b.pay_statement_id = d.pay_statement_id
  and d.earning_code = 'sales volume commission'
  and d.type_name = 'Earning (Auto)'
where a.payroll_name like '%semi-monthly regular%'



select * from  ukg.get_gm_sales_payroll() populates page
calls
 FUNCTION sls.get_sales_consultant_payroll(
    IN _year_month integer,
    IN _store citext)
which calls    
  sls.consultant_payroll
  sls.paid_by_month

draw is coming from sls.consultant payroll

need to modify FUNCTION sls.update_consultant_payroll() with ukg draw
which is getting from sls.paid_by_month

paid by month is probably something to replace with a ukg equivalent
but for payroll, all i need is the draw amount for the current month
add to google keep

select * from sls.consultant_payroll where year_month = 202201

-- looks good for draw
select * 
from (
	select 
	  case
	    when a.store_code = 'RY1' then 'GM'
	    when a.store_code = 'RY2' then 'HN'
	  end as store, a.employee_number, a.last_name, a.first_name
	from sls.personnel a
	inner join sls.team_personnel c on a.employee_number = c.employee_number
		and c.from_Date < (select first_day_of_next_month from sls.months
			where open_closed = 'open')
		and c.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')
	inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
		and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
		and d.thru_date > (select last_day_of_previous_month from sls.months
			where open_closed = 'open')) aa	
left join ( -- this works, see if i can trim it down, need to identify the month
select a.payroll_start_date, a.payroll_end_date,
  b.employee_account_id, c.last_name, c.first_name, c.employee_number,
  d.ee_amount
from ukg.payrolls a
join ukg.pay_statements b on a.payroll_id = b.payroll_id
join ukg.employees c on b.employee_account_id = c.employee_id
join ukg.pay_statement_earnings d on a.payroll_id = d.payroll_id
  and b.pay_statement_id = d.pay_statement_id
  and d.earning_code = 'sales volume commission'
  and d.type_name = 'Earning (Auto)'
where a.payroll_name like '%semi-monthly regular%') bb on aa.employee_number = bb.employee_number


select * from ukg.pay_statement_earnings

select c.employee_number, d.ee_amount as draw, 
  (select year_month from dds.dim_date where the_date = a.payroll_end_date) as year_month
from ukg.payrolls a
join ukg.pay_statements b on a.payroll_id = b.payroll_id
join ukg.employees c on b.employee_account_id = c.employee_id
join ukg.pay_statement_earnings d on a.payroll_id = d.payroll_id
  and b.pay_statement_id = d.pay_statement_id
  and d.earning_code = 'sales volume commission'
  and d.type_name = 'Earning (Auto)'
where a.payroll_name like '%semi-monthly regular%'

			