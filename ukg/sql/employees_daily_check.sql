﻿09/05/21
-- today's changed rows
select * 
from ukg.employee_details a
where exists (
select 1 
from ukg.employee_Details
where row_from_date = current_date
  and username = a.username)
order by username, employee_number, row_from_date  

select row_from_date, count(*)
from ukg.employee_details
group by row_from_date


select employee_id, username, first_name, last_name, status, /*b.*,*/ 
	b->'empl_ref'->>'account_id' as manager, a.row_from_date, 
	a.row_thru_date, cost_center_id,
  row_number() over (partition by username order by row_from_date desc) as seq
from ukg.employee_details a
left join jsonb_array_elements(a.managers) b on true
where exists (
	select 1 
	from ukg.employee_Details
	where row_from_date = '09/05/2021'
		and username = a.username)
and b->'empl_ref'->>'account_id' not in ('12987528722','12987528722') -- excludes the appleby/hagen mgmt change
order by username, employee_number, row_from_date  

-- changed his address
select * from ukg.employee_details where employee_id = 12987528598

-- dayton, xfring from ry2 to ry1, employee_id changes, employee_number (dt) changes, username does not,
--		primary_account_id changes, ein_id
--		no new cost center for him (yet) ???
select * from ukg.employee_details where username = 'dmarek' order by row_from_date

-- a re-hired date added
select * from ukg.employee_details where employee_id = 12988635307 order by row_from_date



08/27/21
-- today's changed rows
select * 
from ukg.employee_details a
where exists (
select 1 
from ukg.employee_Details
where row_from_date = current_date
  and username = a.username)
order by username, employee_number, row_from_date  


most: mgr changed from hagen to appleby
rehired: guo
coats: missing manager added
miller: preferred phone

select max(row_from_date) from ukg.employee_details

select employee_id, username, first_name, last_name, status, b.*, b->'empl_ref'->>'account_id'
from ukg.employee_details a
left join jsonb_array_elements(a.managers) b on true
where exists (
	select 1 
	from ukg.employee_Details
	where row_from_date = '08/27/2021'
		and username = a.username)
	order by username, employee_number, row_from_date  

select * from ukg.employee_details where username = 'zharren' order by username, employee_number, row_from_date  

--didn't run over the weekend, run on monday 8/30, morning
-- no rows with a row_from_date = 08/30/2021