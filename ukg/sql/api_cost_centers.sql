﻿Cost CENTERS

-- cost centers by index
drop table if exists ukg.json_cost_centers cascade;
create unlogged table ukg.json_cost_centers (
	cost_center_index integer,
	response jsonb);
comment on table ukg.json_cost_centers is 'raw scrape of ukg endpoint Cost Centers';
-- select * from ukg.json_cost_centers

drop table if exists ukg.ext_cost_centers cascade;
create table ukg.ext_cost_centers (
  cost_center_index integer not null,
  id bigint primary key,
  parent_id bigint,
  name citext not null,
  description citext,
  payroll_code citext,
  external_id citext);

-- currently only 3 cost center trees configured:   
-- 	0: Location
-- 	1: Manager Adjustments
-- 	2: Bonus Hours

create or replace function ukg.ext_cost_centers()
returns void as
$BODY$
/*

*/
  truncate ukg.ext_cost_centers;
	insert into ukg.ext_cost_centers
	select a.cost_center_index, 
		(b->>'id')::bigint, (b->>'parent_id')::bigint, b->>'name', b->>'description', b->>'payroll_code', b->>'external_id'
	from ukg.json_cost_centers a 
	left join jsonb_array_elements(a.response->'cost_centers') as b on true
	where position('error' in response::text) = 0
		and position('Cost Center not used' in response::text) = 0;
$BODY$
language sql;	

-- cost center details
drop table if exists ukg.json_cost_center_details cascade;
create unlogged table ukg.json_cost_center_details (
	id bigint,
	response jsonb);
comment on table ukg.json_cost_center_details is 'raw scrape of ukg endpoint Cost Center';
-- select * from ukg.json_cost_center_details
select distinct id from ukg.ext_cost_centers

-- 08/30/21, at this time i have no idea what attributes will be used and what will not
-- currently there is no gl data and i am pretty sure that will have to be populated,
-- for now, persisting all arrays and some objects (eg project_location, job settings))as jsonb attribute
drop table if exists ukg.ext_cost_center_details cascade;
create table ukg.ext_cost_center_details (
	external_id integer,
	abbreviation citext,
	cc_name citext not null,
	payroll_code citext,
	description citext,
	time_allocation boolean,
	manning numeric,
	gl_codes jsonb,
	visible boolean,
	custom_fields jsonb,
	location_factor numeric,
	contacts jsonb,
	country citext,
	address_line_1 citext,
	address_line_2 citext,
	address_line_3 citext,
	city citext,
	override_locals boolean,
	state citext,
	zip citext,
	company_name citext,
	number citext,
	number_addition citext,
	use_employee_home_address boolean,
	geo_fencing_distance_type citext check (geo_fencing_distance_type in ('KM','MILES')),
	geo_fencing_distance numeric,
	state_assigned_tax_code integer,
	is_operating_and_reportable boolean,
	delivery_point_barcode citext,
	primary_comment_code citext,
	secondary_comment_code citext,
	reasons_for_change citext,
	cost_center_skills jsonb,
	job_settings jsonb,
	id bigint,
	parent_cc bigint,
	cc_level integer check (cc_level between 1 and 10),
	workers_comp_code citext,
	job_strain jsonb);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_cost_center_details';	


insert into ukg.ext_cost_center_details (external_id,abbreviation,cc_name,payroll_code,description,time_allocation,
	manning,gl_codes,visible,custom_fields,location_factor,contacts,country,address_line_1,address_line_2,
	address_line_3,city,override_locals,state,zip,company_name,number,number_addition,use_employee_home_address,
	geo_fencing_distance_type,geo_fencing_distance,state_assigned_tax_code,is_operating_and_reportable,
	delivery_point_barcode,primary_comment_code,secondary_comment_code,reasons_for_change,cost_center_skills,
	job_settings,id,parent_cc,cc_level,workers_comp_code,job_strain)  
select (a.response->>'external_id')::integer,
	a.response->>'abbreviation',
	a.response->>'name',
	a.response->>'payroll_code',
	a.response->>'description',
	(a.response->>'time_allocation')::boolean,
	(a.response->>'manning')::numeric,
	a.response->'gl_codes',
	(a.response->>'visible')::boolean,
	a.response->'custom_fields',
	(a.response->>'location_factor')::numeric,
	a.response->'contacts',
	a.response->'contact_address'->>'country',
	a.response->'contact_address'->>'address_line_1',
	a.response->'contact_address'->>'address_line_2',
	a.response->'contact_address'->>'address_line_3',
	a.response->'contact_address'->>'city',
	(a.response->'contact_address'->>'override_locals')::boolean,
	a.response->'contact_address'->>'state',
	a.response->'contact_address'->>'zip',
	a.response->'contact_address'->>'company_name',
	a.response->'contact_address'->>'number',
	a.response->'contact_address'->>'number_addition',
	(a.response->>'use_employee_home_address')::boolean,
	a.response->>'geo_fencing_distance_type',
	(a.response->>'geo_fencing_distance')::numeric,
	(a.response->>'state_assigned_tax_code')::integer,
	(a.response->>'is_operating_and_reportable')::boolean,
	a.response->>'delivery_point_barcode',
	a.response->>'primary_comment_code',
	a.response->>'secondary_comment_code',
	a.response->>'reasons_for_change',
	a.response->'cost_center_skills',
	a.response->'job_settings',
	(a.response->>'id')::bigint,
	(a.response->'parent_cc'->>'id')::bigint,
	(a.response->>'level')::integer,
	a.response->>'workers_comp_code',
	a.response->'job_strain'
-- select *
from ukg.json_cost_center_details a;


-- cost center jobs
drop table if exists ukg.json_cost_center_jobs cascade;
create unlogged table ukg.json_cost_center_jobs (
	response jsonb);
comment on table ukg.json_cost_centers is 'raw scrape of ukg endpoint Cost Center Jobs';
-- select * from ukg.json_cost_center_jobs

-- no unique data in cost center jobs, use it only to get the ids to generate cost center job details

select b->'id'
from ukg.json_cost_center_jobs a
join jsonb_array_elements(a.response->'costCenterJobs') b on true

drop table if exists ukg.json_cost_center_job_details cascade;
create table ukg.json_cost_center_job_details (
	id bigint not null,
	response jsonb);
-- 107 jobs
-- select * from ukg.json_cost_center_job_details

-- guessing on this one, leave out the employee_override attributes, sounds operational rather than transactional
-- lots of "default" objects, don't know if any are or will be used, for now persist as json
drop table if exists ukg.ext_cost_center_job_details cascade;
create table ukg.ext_cost_center_job_details (
	id bigint primary key,
	job_category citext,
	job_name citext not null,
	abbreviation citext,
	description citext,
	job_description citext,
	external_id citext,
	payroll_code citext,
	visible boolean,
	applicant_tracking_display boolean,
	applicant_tracking_display_only boolean,
  standard_work_day integer,
  employee_type bigint,
  eeo_classification citext check (eeo_classification in ('EXECUTIVE_OR_SENIOR_LEVEL_OFFICIALS_AND_MANAGERS','FIRST_OR_MID_LEVEL_OFFICIALS_AND_MANAGERS',
			'PROFESSIONALS','TECHNICIANS','SALES_WORKERS','ADMINISTRATIVE_SUPPORT_WORKERS','CRAFT_WORKERS','OPERATIVES','LABORERS_AND_HELPERS','SERVICE_WORKERS')),
	union_id integer,
	pay_grade integer,
	pay_type integer references ukg.ext_pay_types(id),
	first_screen integer,
	worker_type integer,
	linked_company_eins jsonb,
	worker_comp_code_types jsonb,
	salary_surveys jsonb,
	competency_groups jsonb, 
	notes jsonb,
	aca jsonb,
	access jsonb,
	accruals jsonb,
	accruals_2 jsonb,
	attestation jsonb,
	benefit jsonb,
	bradford_factor jsonb,
	competency jsonb,
	demographic jsonb,
	holiday jsonb,
	labor_distribution jsonb,
	leave_of_absence jsonb,
	new_dashboard_layout jsonb,
	offline_restrictions jsonb,
	pay_calculations jsonb,
	pay_calculations_2 jsonb,
	pay_period jsonb,
	pay_prep jsonb,
	performance_review jsonb,
	points jsonb,
	pst_population jsonb,
	retirement_plan jsonb,
	scheduler jsonb,
	scorecard jsonb,
  security jsonb,
  succession jsonb,
  timesheet jsonb,
  timeoff jsonb,
  ts_auto_population jsonb,
  training jsonb,
  workday_breakdown jsonb,
  work_schedule jsonb,
  working_time_regulation jsonb,
  work_center_skills jsonb,
  job_strain jsonb) ;



select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_cost_center_job_details';	

create or replace function ukg.ext_cost_center_job_details()
returns void as
$BODY$
/*
  select ukg.ext_cost_center_job_details();
*/
  truncate ukg.ext_cost_center_job_details ;
	insert into ukg.ext_cost_center_job_details (id,job_category,job_name,abbreviation,description,job_description,
		external_id,payroll_code,visible,applicant_tracking_display,applicant_tracking_display_only,
		standard_work_day,employee_type,eeo_classification,union_id,pay_grade,pay_type,first_screen,
		worker_type,linked_company_eins,worker_comp_code_types,salary_surveys,competency_groups,notes,
		aca,access,accruals,accruals_2,attestation,benefit,bradford_factor,competency,demographic,
		holiday,labor_distribution,leave_of_absence,new_dashboard_layout,offline_restrictions,pay_calculations,
		pay_calculations_2,pay_period,pay_prep,performance_review,points,pst_population,retirement_plan,
		scheduler,scorecard,security,succession,timesheet,timeoff,ts_auto_population,training,
		workday_breakdown,work_schedule,working_time_regulation,work_center_skills,job_strain)
	select
		(a.response->>'id')::bigint,
		a.response->>'job_category',
		a.response->>'name',
		a.response->>'abbreviation',
		a.response->>'description',
		a.response->>'job_description',
		a.response->>'external_id',
		a.response->>'payroll_code',
		(a.response->>'visible')::boolean,
		(a.response->>'applicant_tracking_display')::boolean,
		(a.response->>'applicant_tracking_display_only')::boolean,
		(a.response->'standard_work_day'->>'value')::integer,
		(a.response->'employee_type'->>'id')::bigint,
		a.response->'eeo_classification'->>'value',
		(a.response->'union'->>'id')::integer,
		(a.response->'pay_grade'->>'id')::integer,
		(a.response->'pay_type'->>'id')::integer,
		(a.response->'first_screen'->>'id')::integer,
		(a.response->'worker_type'->>'id')::integer,
		a.response->'linked_company_eins',
		a.response->'worker_comp_code_types',
		a.response->'salary_surveys',
		a.response->'competency_groups',
		a.response->'notes',
		a.response->'defaults'->'aca',
		a.response->'defaults'->'access',
		a.response->'defaults'->'accruals',
		a.response->'defaults'->'accruals_2',
		a.response->'defaults'->'attestation',
		a.response->'defaults'->'benefit',
		a.response->'defaults'->'bradford_factor',
		a.response->'defaults'->'competency',
		a.response->'defaults'->'demographic',
		a.response->'defaults'->'holiday',
		a.response->'defaults'->'labor_distribution',
		a.response->'defaults'->'leave_of_absence',
		a.response->'defaults'->'new_dashboard_layout',
		a.response->'defaults'->'offline_restrictions',
		a.response->'defaults'->'pay_calculations',
		a.response->'defaults'->'pay_calculations_2',
		a.response->'defaults'->'pay_period',
		a.response->'defaults'->'pay_prep',
		a.response->'defaults'->'performance_review',
		a.response->'defaults'->'points',
		a.response->'defaults'->'pst_population',
		a.response->'defaults'->'retirement_plan',
		a.response->'defaults'->'scheduler',
		a.response->'defaults'->'scorecard',
		a.response->'defaults'->'security',
		a.response->'defaults'->'succession',
		a.response->'defaults'->'timesheet',
		a.response->'defaults'->'timeoff',
		a.response->'defaults'->'ts_auto_population',
		a.response->'defaults'->'training',
		a.response->'defaults'->'workday_breakdown',
		a.response->'defaults'->'work_schedule',
		a.response->'defaults'->'working_time_regulation',
		a.response->'cost_center_skills',
		a.response->'job_strain'
	-- select * 
	from ukg.json_cost_center_job_details a;
$BODY$
language sql;	

select * from ukg.ext_cost_center_job_details where id = 51655921931







	
	