﻿PAYROLLS

-- all payrolls
drop table if exists ukg.json_payrolls cascade;
create unlogged table ukg.json_payrolls (
response jsonb PK);
comment on table ukg.json_payrolls is 'raw scrape of ukg endpoint Get All Employees; https://beta.rydellvision.com:8888/ukg/payrolls';
-- select * from  ukg.json_payrolls

drop table if exists ukg.ext_payrolls cascade;
create table ukg.ext_payrolls (
	payroll_id integer primary key,
	name citext not null,
	start_date date,
	end_date date,
	pay_date date not null,
	type_id integer not null,
	display_name citext,
	status citext check (status in ('NEW','OPEN','OPEN_FAILED','CLOSE_FAILED','CLOSED','SUBMITTED','ROLLBACK_FAILED','FINALIZE_FAILED','FINALIZED')),
	ein_id bigint not null,
	ein_name citext,
	ein_tax_id citext,
	billable boolean,
	direct_deposit_date date,
	sch_submit_date_time date,
	inovice_generator_id integer,
	adjustment_grab_ytd_from_payroll_id integer,
	dflt_pay_statement_type_id integer,
	dflt_pay_stub_note  citext,
	self citext not null);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_payrolls';

insert into ukg.ext_payrolls (payroll_id,name,start_date,end_date,pay_date,type_id,display_name,
	status,ein_id,ein_name,ein_tax_id,billable,direct_deposit_date,sch_submit_date_time,
	inovice_generator_id,adjustment_grab_ytd_from_payroll_id,dflt_pay_statement_type_id,dflt_pay_stub_note,self)	
select (b->>'id')::integer,
	b->>'name',
	(b->>'start_date')::date, (b->>'end_date')::date, (b->>'pay_date')::date,
	(b->'type'->>'id')::integer, b->'type'->>'display_name',
	b->>'status',
	(b->'ein'->>'id')::integer, b->'ein'->>'ein_name',b->'ein'->>'ein_tax_id',
	(b->>'billable')::boolean,
	(b->>'direct_deposit_date')::date, 
	(b->>'sch_submit_date_time')::date,
	(b->>'invoice_generator_id')::integer, (b->>'adjustment_grab_YTD_from_payroll_id')::integer, (b->>'dflt_pay_statement_type_id')::integer,
	b->>'dflt_pay_stub_note',
	b->'_links'->>'self'
from ukg.json_payrolls a
join jsonb_array_elements(a.response->'payrolls') as b(payrolls) on true;
-- select * from ukg.ext_payrolls;

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- Get Single Payroll
this does not appear to offer any new information except date_finalized

select * from ukg.ext_payrolls where payroll_id = 109195529

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

-- Get Single Payroll Batches
requires a payroll id in the request, but  does not reference the payroll id in the response
not sure for what this would be useful
pay statement can be referenced by batch id

drop table if exists ukg.json_payroll_batches cascade;
create unlogged table ukg.json_payroll_batches (
	id integer, -- the payroll_id used to get the batches
	response jsonb);
comment on table ukg.json_payroll_batches is 'raw scrape of ukg endpoint Get All Employees; https://beta.rydellvision.com:8888/ukg/payrolls/payroll_id/batches';
-- select * from  ukg.json_payroll_batches

drop table if exists ukg.ext_payroll_batches cascade;
create table ukg.ext_payroll_batches (
	batch_id integer primary key,
	display_name citext,
	status citext,
	master boolean,
	type_active boolean,
	type_id integer references ukg.payroll_batch_types(id),
	type_display_name citext);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_payroll_batches';

  
insert into ukg.ext_payroll_batches (batch_id,display_name,status,master,type_active,type_id,type_display_name)
select (b->>'id')::integer,
	b->>'display_name', b->>'status',
	(b->>'master')::boolean, 
	(b->'type'->>'active')::boolean,
	(b->'type'->>'id')::integer,
	(b->'type'->>'display_name')
from ukg.json_payroll_batches a 
join jsonb_array_elements(a.response->'batches') b on true;
-- select * from ukg.ext_payroll_batches
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- PAY STATEMENTS

-- Get Single Payroll Pay Statements
this actually returns many statements per payroll id
in accordance with the API Reference, i will treat it as get pay statements
i think this is no more than a link between a payroll id and the individual entries
so, lets give it a try, treat it like employees, for each payroll id in ukg_ext_payrolls, get all the payroll statements
then subsequently, from each payroll statement get the detailed pay statement
to start off, will do 3 payroll_ids, then do the detail pay statement from them ( there are current 132 rows in ukg.ext_payrolls)

drop table if exists ukg.json_pay_statements cascade;
create unlogged table ukg.json_pay_statements (
	id integer, -- the payroll_id used to get the pay_statements
	response jsonb);
comment on table ukg.json_pay_statements is 'raw scrape of ukg endpoint Get All Employees; https://beta.rydellvision.com:8888/ukg/payrolls/payroll_id/pay-statements';
-- select * from  ukg.json_pay_statements

the pay statements data include objects not included in the api reference schema:
            "calcUpto": 0,
            "emptyEarning": false,

-- 01/20/22 as we approach the deadline for the second monthly payroll
-- removing the FK 
-- don't remember what i was thinking, but at this point in the game we are just scrambling to get shit done
-- we will worry about the data model later            
drop table if exists ukg.ext_pay_statements cascade;
create table ukg.ext_pay_statements (
	payroll_id integer, -- references ukg.ext_payrolls(payroll_id),
  pay_statement_id bigint primary key,
  employee_account_id bigint,
	pay_date date not null,
	status citext check (status in ('INVALID','NEW','OPEN','CLOSED','FINALIZED')),
	type_id integer not null references ukg.pay_statement_types(id),
	type_display_name citext,
	pay_period_start date not null,
	pay_period_end date not null,
	calc_upto integer,
	empty_earning boolean,
	self citext not null);

-- select * from ukg.json_pay_statements

-- needed the inner join to exclude the payroll_id's with no pay statements
insert into ukg.ext_pay_statements
select a.id, (b->>'id')::bigint, (b->'employee'->>'account_id')::bigint,
	(b->>'pay_date')::date, b->>'status',
	(b->'type'->>'id')::integer, b->'type'->>'display_name',
	(b->>'pay_period_start')::date, (b->>'pay_period_end')::date,
	(b->>'calcUpto')::integer,
	(b->>'emptyEarning')::boolean,
	b->'_links'->>'self'
from ukg.json_pay_statements a 
join jsonb_array_elements(a.response->'pay_statements') b on true;
-- select * from ukg.ext_pay_statements


-- it appears that a payroll of status OPEN does not have any pay statements
-- that is not true
for those open payrolls that have no pay statements, the  pay_statements json is: {"pay_statements": []}
select * from ukg.ext_payrolls where payroll_id = 109046121
union
select * from ukg.ext_payrolls where payroll_id = 109195529

-- it appears that a payroll of status OPEN does not have any pay statements
-- shit, some open payrolls have pay statements
select a.payroll_id, a.status, c->>'id'  as c_id, c->'type'->>'id' as c_type_id, b.response
from ukg.ext_payrolls a
left join ukg.json_pay_statements b on a.payroll_id = b.id
left join jsonb_array_elements(b.response->'pay_statements') c on true 
-- where a.payroll_id = 109195529
where a.status = 'OPEN'
order by a.payroll_id
limit 100

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- PAY STATEMENT

now we get to the meat, this is the actual paycheck detail, i believe
think i want to do this one in steps
at the ext level, do the arrays (earnings, deductions, etc) as jsonb columns
then break those out into separate tables, they will have multiple rows per employee/check
will also need cost center ids for FKs

SELECT DISTINCT payroll_id, pay_statement_id from ukg.ext_pay_statements limit 5

SELECT DISTINCT payroll_id, pay_statement_id from ukg.ext_pay_statements order by payroll_id limit 5

SELECT DISTINCT payroll_id, pay_statement_id from ukg.ext_pay_statements order by payroll_id;

-- ~ an hour to load all 5477 rows
drop table if exists ukg.json_pay_statement_details cascade;
create unlogged table ukg.json_pay_statement_details (
	payroll_id integer, -- the payroll_id used to get the pay_statements
	pay_statement_id bigint,
	response jsonb);
comment on table ukg.json_pay_statement_details is 'raw scrape of ukg endpoint Get All Employees; https://beta.rydellvision.com:8888/ukg/payrolls/payroll_id/pay-statements/pay_statement_id';
-- select * from  ukg.json_pay_statement_details limit 2

-- don't remember what i was thinking, but at this point in the game we are just scrambling to get shit done
-- we will worry about the data model later    
-- 01/21/22 pay_statement, base pay statement information, remove all the jsonb fields, those will become separate table
drop table if exists ukg.ext_pay_statement_details cascade;
create table ukg.ext_pay_statement_details (
	payroll_id integer,
	pay_statement_id bigint,
  employee_id bigint not null,
  pay_date date,
  status citext check (status in ('INVALID','NEW','OPEN','CLOSED','FINALIZED')),
  type_id integer not null references ukg.pay_statement_types(id),
  display_name citext,
  pay_period_start date,
  pay_period_end date,
  primary key (payroll_id, pay_statement_id));
--   earnings jsonb,
--   deductions jsonb,
--   taxes jsonb,
--   messages jsonb,
--   payments jsonb);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_pay_statement_details';  

-- 01/20/22 after once failing due to a 500 response
-- inserted 218,850 rows
insert into ukg.ext_pay_statement_details (payroll_id, pay_statement_id, employee_id,pay_date,status,type_id,
	display_name,pay_period_start,pay_period_end) -- ,earnings,deductions,taxes,messages,payments)
select a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint,
	(a.response->>'pay_date')::date,
	a.response->>'status',
	(a.response->'type'->>'id')::integer, a.response->'type'->>'display_name',
	(a.response->>'pay_period_start')::date, (a.response->>'pay_period_end')::date
-- 	b.*, c.*, d.*, e.*, f.*
from ukg.json_pay_statement_details a
-- left join jsonb_array_elements(a.response->'earnings') b on true
-- left join jsonb_array_elements(a.response->'deductions') c on true
-- left join jsonb_array_elements(a.response->'taxes') d on true
-- left join jsonb_array_elements(a.response->'messages') e on true
-- left join jsonb_array_elements(a.response->'payments') f on true

select * from ukg.ext_pay_statement_details where employee_id = 12986719383




/*
01/21/22
looks like we are going to need some kind of data model to handle the pay stuff
*/

select pay_statement_id from ukg.json_pay_statement_details group by pay_statement_id having count(*) > 1
-- so far, just Regular and Historical
select distinct response->'type'->>'display_name' from ukg.json_pay_statement_details

-- earnings  2291  rows, clearly, multiple earnings rows per pay statement
-- who has the most rows
-- select employee_id, last_name, first_name, count(*) from (
select c.last_name, c.first_name, a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
-- 	(a.response->>'pay_date')::date,
-- 	a.response->>'status',
-- 	(a.response->'type'->>'id')::integer, a.response->'type'->>'display_name',
-- 	(a.response->>'pay_period_start')::date, (a.response->>'pay_period_end')::date,
--   b.*
 	(b->>'id')::bigint as earnings_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
 	(b->>'hours')::BIGINT as hours,
 	(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
 	(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
 	b->>'type_name' as type_name
from ukg.json_pay_statement_details a
left join jsonb_array_elements(a.response->'earnings') b on true 	
LEFT JOIN ukg.employees c on (a.response->'employee'->>'account_id')::bigint = c.employee_id
-- where (a.response->'employee'->>'account_id')::bigint = 12986719383
-- ) x group by employee_id, last_name, first_name order by count(*) desc
order by last_name, first_name, payroll_id

most rows
12987528633,Gothberg,Conner,15
12987529017,Pederson,David,15
12987528805,Rodriguez,Justin,13
12987528171,Brooks Jr,Drew,13
12986298520,Miller,Kim,13
12987528873,Troftgruben,Rodney,13
12987528886,Warmack,James,13

-- looks like this will work
lets start building some tables

--TODO have not figured out the dual cost center stuff yet
drop table if exists ukg.ext_pay_statement_earnings cascade;
create table ukg.ext_pay_statement_earnings (
  payroll_id integer not null,
  pay_statement_id bigint null,
  employee_id bigint not null,
  earnings_id bigint not null,
  batch_id integer not null, 
  batch_name citext not null,
  hours bigint,
  earning_id integer not null, 
  earning_code citext not null,
  earning_name citext not null,
  base_rate numeric,
  ee_amount numeric,
  type_name citext not null,
  cost_centers jsonb,
  primary key(payroll_id,pay_statement_id,earnings_id));

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_pay_statement_earnings';  

insert into ukg.ext_pay_statement_earnings (payroll_id,pay_statement_id,employee_id,earnings_id,
	batch_id,batch_name,hours,earning_id,earning_code,earning_name,base_rate,ee_amount,type_name,cost_centers)  
select a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
 	(b->>'id')::bigint as earnings_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
 	(b->>'hours')::BIGINT as hours,
 	(b->'earning'->>'id')::integer as earning_id, b->'earning'->>'code' as earning_code, b->'earning'->>'name' as earning_name,
 	(b->>'base_rate')::numeric as base_rate, (b->>'ee_amount')::numeric as ee_amount,
 	b->>'type_name' as type_name,
 	b->'cost_centers'
from ukg.json_pay_statement_details a
left join jsonb_array_elements(a.response->'earnings') b on true;

select b.last_name ||', ' || b.first_name, a.* from ukg.ext_pay_statement_earnings a
join ukg.employees b on a.employee_id = b.employee_id 
order by employee_id, payroll_id, pay_statement_id

-- deductions
-- most rows
12987528148,Aubol,Thomas,42
12987528866,Tarr,Jeff,35
12987528743,Monreal,Jessie,34
12987528859,Stein,Donald,28
12987528581,Croaker,Craig,27

-- select employee_id, last_name, first_name, count(*) from (
select c.last_name, c.first_name, a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
--   b.*
 	(b->>'id')::bigint as earnings_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
--  	(b->>'hours')::BIGINT as hours,
--  	(b->'earning'->>'id')::integer as earning_id, 
 	b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
--  	(b->>'base_rate')::numeric as base_rate, 
 	(b->>'ee_amount')::numeric as ee_amount,
 	(b->>'er_amount')::numeric as er_amount,
 	b->>'type_name' as type_name,
 	b->'cost_centers'
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'deductions') b on true 	
LEFT JOIN ukg.employees c on (a.response->'employee'->>'account_id')::bigint = c.employee_id
-- where (a.response->'employee'->>'account_id')::bigint = 12987528859
where payroll_id = 109131718 and pay_statement_id =  43052898406 
-- ) x group by employee_id, last_name, first_name order by count(*) desc
order by last_name, first_name, payroll_id


drop table if exists ukg.ext_pay_statement_deductions cascade;
create table ukg.ext_pay_statement_deductions (
  payroll_id integer not null,
  pay_statement_id bigint null,
  employee_id bigint not null,
  deductions_id bigint not null,
  batch_id integer not null, 
  batch_name citext not null,
--   hours bigint,
--   earning_id integer not null, 
  deduction_code citext not null,
  deduction_name citext not null,
--   base_rate numeric,
  ee_amount numeric,
  er_amount numeric,
  type_name citext not null,
  cost_centers jsonb,
  primary key(payroll_id,pay_statement_id,deductions_id));

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_pay_statement_deductions';   

insert into ukg.ext_pay_statement_deductions (payroll_id,pay_statement_id,employee_id,deductions_id,
batch_id,batch_name,deduction_code,deduction_name,ee_amount,er_amount,type_name,cost_centers)  
select a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
 	(b->>'id')::bigint as deductions_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
 	b->'deduction'->>'code' as deduction_code, b->'deduction'->>'name' as deduction_name,
 	(b->>'ee_amount')::numeric as ee_amount, (b->>'er_amount')::numeric as er_amount,
 	b->>'type_name' as type_name,
 	b->'cost_centers'
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'deductions') b on true;

select * from ukg.ext_pay_statement_deductions
-- taxes --------------------------------------------------------------------------------------------------------------------------
-- most rows
-- select employee_id, last_name, first_name, count(*) from (
select c.last_name, c.first_name, a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
--   b.*
 	(b->>'id')::bigint as taxes_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
 	(b->>'ee_amount')::numeric as ee_amount,
 	(b->>'er_amount')::numeric as er_amount,
 	b->>'type_name' as type_name,
 	(b->'company_tax'->>'id')::integer as company_tax_id, 
  b->'company_tax'->>'code' as company_tax_code,
  b->'company_tax'->>'name' as company_tax_name,
  b->'company_tax'->>'abbrev' as company_tax_abbrev,
  (b->>'ee_gross_wage')::numeric as ee_grosss_wage,
  (b->>'er_gross_wage')::numeric as er_grosss_wage,
  (b->>'ee_subject_wage')::numeric as ee_subject_wage,
  (b->>'er_subject_wage')::numeric as er_subject_wage,
  (b->>'ee_gross_subject_wage')::numeric as ee_gross_subject_wage,
  (b->>'er_gross_subject_wage')::numeric as er_gross_subject_wage
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'taxes') b on true 	
LEFT JOIN ukg.employees c on (a.response->'employee'->>'account_id')::bigint = c.employee_id
where (a.response->'employee'->>'account_id')::bigint = 12987528859
-- where payroll_id = 109131718 and pay_statement_id =  43052898406 
-- ) x group by employee_id, last_name, first_name order by count(*) desc
order by last_name, first_name, payroll_id;

drop table if exists ukg.ext_pay_statement_taxes cascade;
create table ukg.ext_pay_statement_taxes (
  payroll_id integer not null,
  pay_statement_id bigint null,
  employee_id bigint not null,
  taxes_id bigint not null,
  batch_id integer not null, 
  batch_name citext not null,
  ee_amount numeric,
  er_amount numeric,
  type_name citext not null,
  company_tax_id integer,
  company_tax_code citext,
  company_tax_name citext,
  company_tax_abbrev citext,
  ee_gross_wage numeric,
  er_gross_wage numeric,
  ee_subject_wage numeric,
  er_subject_wage numeric,
  ee_gross_subject_wage numeric,
  er_gross_subject_wage numeric,
  primary key(payroll_id,pay_statement_id,taxes_id));

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_pay_statement_taxes';    

insert into ukg.ext_pay_statement_taxes (payroll_id,pay_statement_id,employee_id,taxes_id,
	batch_id,batch_name,ee_amount,er_amount,type_name,company_tax_id,company_tax_code,company_tax_name,
	company_tax_abbrev,ee_gross_wage,er_gross_wage,ee_subject_wage,er_subject_wage,
	ee_gross_subject_wage,er_gross_subject_wage)
select a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
 	(b->>'id')::bigint as taxes_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
 	(b->>'ee_amount')::numeric as ee_amount,
 	(b->>'er_amount')::numeric as er_amount,
 	b->>'type_name' as type_name,
 	(b->'company_tax'->>'id')::integer as company_tax_id, 
  b->'company_tax'->>'code' as company_tax_code,
  b->'company_tax'->>'name' as company_tax_name,
  b->'company_tax'->>'abbrev' as company_tax_abbrev,
  (b->>'ee_gross_wage')::numeric as ee_grosss_wage,
  (b->>'er_gross_wage')::numeric as er_grosss_wage,
  (b->>'ee_subject_wage')::numeric as ee_subject_wage,
  (b->>'er_subject_wage')::numeric as er_subject_wage,
  (b->>'ee_gross_subject_wage')::numeric as ee_gross_subject_wage,
  (b->>'er_gross_subject_wage')::numeric as er_gross_subject_wage
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'taxes') b on true;

-- messages
select c.last_name, c.first_name, a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
 	(b->>'id')::bigint as messages_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
 	b->'message'->>'text' as message_text,
 	b->'message'->>'severity' as message_severity,
 	b->>'type_name' as type_name
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'messages') b on true 	
LEFT JOIN ukg.employees c on (a.response->'employee'->>'account_id')::bigint = c.employee_id
-- where (a.response->'employee'->>'account_id')::bigint = 12987528859
-- where payroll_id = 109131718 and pay_statement_id =  43052898406 
-- ) x group by employee_id, last_name, first_name order by count(*) desc
order by last_name, first_name, payroll_id;

drop table if exists ukg.ext_pay_statement_messages cascade;
create table ukg.ext_pay_statement_messages (
  payroll_id integer not null,
  pay_statement_id bigint null,
  employee_id bigint not null,
  messages_id bigint not null,
  batch_id integer not null, 
  batch_name citext not null,
	message_text citext,
	message_severity citext,
	type_name citext,
  primary key(payroll_id,pay_statement_id,messages_id));

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_pay_statement_messages';   

insert into ukg.ext_pay_statement_messages(payroll_id,pay_statement_id,employee_id,messages_id,
	batch_id,batch_name,message_text,message_severity,type_name)
select a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
 	(b->>'id')::bigint as messages_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
 	b->'message'->>'text' as message_text,
 	b->'message'->>'severity' as message_severity,
 	b->>'type_name' as type_name
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'messages') b on true;	  

-- payments --------------------------------------------------------------------------------------------------------------------------
select c.last_name, c.first_name, a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
 	(b->>'id')::bigint as payments_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
 	(b->>'ee_amount')::numeric as ee_amount,
 	b->>'type_name' as type_name
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'payments') b on true 	
LEFT JOIN ukg.employees c on (a.response->'employee'->>'account_id')::bigint = c.employee_id
-- where (a.response->'employee'->>'account_id')::bigint = 12987528859
-- where payroll_id = 109131718 and pay_statement_id =  43052898406 
-- ) x group by employee_id, last_name, first_name order by count(*) desc
order by last_name, first_name, payroll_id;

drop table if exists ukg.ext_pay_statement_payments cascade;
create table ukg.ext_pay_statement_payments (
  payroll_id integer not null,
  pay_statement_id bigint null,
  employee_id bigint not null,
  payments_id bigint not null,
  batch_id integer not null, 
  batch_name citext not null,
	ee_amount numeric,
	type_name citext,
  primary key(payroll_id,pay_statement_id,payments_id));

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_pay_statement_payments';  


insert into ukg.ext_pay_statement_payments(payroll_id,pay_statement_id,employee_id,
	payments_id,batch_id,batch_name,ee_amount,type_name)
select a.payroll_id, a.pay_statement_id, 
	(a.response->'employee'->>'account_id')::bigint as employee_id,
 	(b->>'id')::bigint as messages_id, 
 	(b->'batch'->>'id')::integer as batch_id, b->'batch'->>'display_name' as batch_name,
 	(b->>'ee_amount')::numeric as ee_amount,
 	b->>'type_name' as type_name
from ukg.json_pay_statement_details a
join jsonb_array_elements(a.response->'payments') b on true;	    