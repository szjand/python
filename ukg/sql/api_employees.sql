﻿EMPLOYEES

/*
	08/05/21
	in all these tables, i have renamed the field id to employee_id and the field employee_id to employee_number (dealertrack emp#)
	skipped endpoint Get Single Employee Badges
*/
-- all employees
drop table if exists ukg.json_employees cascade;
create unlogged table ukg.json_employees (
response jsonb);
comment on table ukg.json_employees is 'raw scrape of ukg endpoint Get All Employees; https://beta.rydellvision.com:8888/ukg/employees';
-- select * from ukg.json_employees

drop table if exists ukg.ext_employees cascade;
create table ukg.ext_employees (
	employee_id bigint primary key,
	user_name citext not null,
	employee_number citext not null,
	external_id citext,
	first_name citext not null,
	last_name citext not null,
	primary_account_id bigint,
	ein_name citext,
	hired date not null,
	re_hired date,
	started date,
	terminated date,
	status citext not null,
	self citext not null,
	demographics citext,
	pay_info citext,
	badges citext,
	profiles citext,
	hcm_profiles citext,
	hcm_fields citext,
	ext_date date not null);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_employees';
	
insert into ukg.ext_employees (employee_id,user_name,employee_number,external_id,first_name,last_name,
	primary_account_id,ein_name,hired,re_hired,started,terminated,status,self,demographics,
	pay_info,badges,profiles,hcm_profiles,hcm_fields,ext_date)
select (b->>'id')::bigint,
	lower(b->>'username'),
	b->>'employee_id',
	b->>'external_id',
	initcap(b->>'first_name'),
	initcap(b->>'last_name'),
	(b->>'primary_account_id')::bigint,
	b->>'ein_name',
	(b->'dates'->>'hired')::date, 
	(b->'dates'->>'re_hired')::date,
	(b->'dates'->>'started')::date,
	(b->'dates'->>'terminated')::date,
	b->>'status',
  b->'_links'->>'self',
  b->'_links'->>'demographics',
  b->'_links'->>'pay-info',
  b->'_links'->>'badges',
  b->'_links'->>'profiles',
  b->'_links'->>'hcm_profiles',
	b->'_links'->>'hcm_fields', 
  current_date
-- select b.*  
from ukg.json_employees a
join jsonb_array_elements(a.response->'employees') as b(employees) on true;	
-- select * from ukg.ext_employees where last_name = 'foster';  -- 519/602

create or replace function ukg.ext_employees()
  returns void as
$BODY$
  truncate ukg.ext_employees; 
   
	insert into ukg.ext_employees (employee_id,user_name,employee_number,external_id,first_name,last_name,
		primary_account_id,ein_name,hired,re_hired,started,terminated,status,self,demographics,
		pay_info,badges,profiles,hcm_profiles,hcm_fields,ext_date)
	select (b->>'id')::bigint,
		lower(b->>'username'),
		b->>'employee_id',
		b->>'external_id',
		initcap(b->>'first_name'),
		initcap(b->>'last_name'),
		(b->>'primary_account_id')::bigint,
		b->>'ein_name',
		(b->'dates'->>'hired')::date, 
		(b->'dates'->>'re_hired')::date,
		(b->'dates'->>'started')::date,
		(b->'dates'->>'terminated')::date,
		b->>'status',
		b->'_links'->>'self',
		b->'_links'->>'demographics',
		b->'_links'->>'pay-info',
		b->'_links'->>'badges',
		b->'_links'->>'profiles',
		b->'_links'->>'hcm_profiles',
		b->'_links'->>'hcm_fields', 
		current_date
	from ukg.json_employees a
	join jsonb_array_elements(a.response->'employees') as b(employees) on true;	
$BODY$
language sql;		  
----------------------------------------------------------------------------------------------------
-- changed employees
drop table if exists ukg.json_changed_employees cascade;
create unlogged table ukg.json_changed_employees (
response jsonb);
comment on table ukg.json_changed_employees is 'raw scrape of ukg endpoint Employees Changed; https://beta.rydellvision.com:8888/ukg/employees/changed';

drop table if exists ukg.ext_changed_employees cascade;
create table ukg.ext_changed_employees (
	employee_id bigint primary key,
	status citext check(status in ('CREATED','MODIFIED','DELETED')),
	id_1 bigint not null, -- don't know what this is, but it exists in schema @ 2nd (object) level and in the data
	primary_account_id citext,
	username citext not null,
	employee_number citext not null,
	external_id citext,
	first_name citext not null,
	last_name citext not null,
	self citext,
	demographics citext,
	pay_info citext,
	badges citext,
	profiles citext,
	base_compensation citext,
	additional_compensation citext,
	total_compensation citext, 
	hcm_profiles citext,
	hcm_fields citext,
	hr_custom_fields citext,
	ext_date date not null);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_changed_employees';
  	
insert into ukg.ext_changed_employees (employee_id,status,id_1,primary_account_id,username,employee_number,external_id,
	first_name,last_name,self,demographics,pay_info,badges,profiles,base_compensation,
	additional_compensation,total_compensation,hcm_profiles,hcm_fields,hr_custom_fields,ext_date)
select (b->>'id')::bigint,
  b->>'status',
  (b->'object'->>'id')::bigint,
	(b->'object'->>'primary_account_id')::bigint,
	lower(b->'object'->>'username'),
	b->'object'->>'employee_id',
	b->'object'->>'external_id',
	initcap(b->'object'->>'first_name'),
	initcap(b->'object'->>'last_name'),
  b->'object'->'_links'->>'self',
  b->'object'->'_links'->>'demographics',
  b->'object'->'_links'->>'pay-info',
  b->'object'->'_links'->>'badges',
  b->'object'->'_links'->>'profiles',
  b->'object'->'_links'->>'base_compensation',
  b->'object'->'_links'->>'additional_compensation',
  b->'object'->'_links'->>'total_compensation',
  b->'object'->'_links'->>'hcm_profiles',
	b->'object'->'_links'->>'hcm_fields', 
	b->'object'->'_links'->>'hr_custom_fields', 
  current_date
-- select *
from ukg.json_changed_employees a
join jsonb_array_elements(a.response->'entries') as b(employees) on true;	
-- select * from ukg.ext_changed_employees;
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- employee details


drop table if exists ukg.json_employee_details cascade;
create unlogged table ukg.json_employee_details (
	id bigint not null,
	response jsonb);
comment on table ukg.json_employee_details is 'raw scrape of ukg endpoint Employees; https://beta.rydellvision.com:8888/ukg/employees/12986719382';
-- select * from ukg.json_employee_details

select count(*)
from ukg.json_employee_details

alter table ukg.ext_employee_details
rename column id to employee_id;

drop table if exists ukg.ext_employee_details cascade;
-- assuming single cost center
-- only 1 employee (emily olson) at this time with more than one manager, so, for now, managers will be a jsonb attribute
-- excluded: custome_dates, account_extra_fields, time_entry_extra_fields, hardware_settings,managed_cost_centers_settings, cost_center_limits
--		dates->custome_dates, account_extra_fields, time_entry_extra_fields, national_id_numbers_type
-- photo_href, links (self, demographics, pay_info, profile, hcm_profiles, hcm_fields)
-- locked
-- national_id_numbers: assume only a SSN
create table ukg.ext_employee_details (
	employee_id bigint primary key,
	username citext not null,
	first_name citext not null,
	last_name citext not null,
-- 	photo_href citext,
	middle_name citext,
	external_id citext,
	employee_number citext,
	status citext,
	bsn citext,
	country citext not null,
	address_line_1 citext,
	address_line_2 citext,
	address_line_3 citext,
	city citext,
	state citext,
	zip citext,
	number citext,
	number_addition citext,
	use_separate_mailing_address boolean,
	ma_country citext,
	ma_address_line_1 citext,
	ma_address_line_2 citext,
	ma_address_line_3 citext,
	ma_city citext,
	ma_state citext,
	ma_zip citext,
	ma_number citext,
	ma_number_addition citext,
	primary_account_id bigint,
	ein_id bigint,
-- 	force_change_password boolean,
-- 	locked boolean,
	first_screen citext,
	cell_phone_country_id citext,
	cell_phone_country_code citext,
	cell_phone citext,
	home_phone_country_id citext,
	home_phone_country_code citext,
	home_phone citext,
	work_phone_country_id citext,
	work_phone_country_code citext,
	work_phone citext,
	preferred_phone citext check (preferred_phone in ('HOME','CELL','WORK','NONE')),
	locale bigint,
-- 	manager_index integer check (manager_index between 0 and 5),
-- 	manager_account_id bigint,
	managers jsonb,
	national_insurance citext,
	nickname citext,
	primary_email citext,
	secondary_email citext,
	salutation citext,
	social_insurance citext,
	social_security citext,
	suffix citext,
	timezone citext,
	cost_center_index integer check (cost_center_index between 0 and 8),
	cost_center_id bigint,
-- 	cost_center_type citext check (cost_center_type in ('ROOT','LIST')),
-- 	cost_center_from_date date,
	add_to_new_hire_export boolean,
	hired_date date,
	deceased_date date,
	re_hired_date date,
	started_date date,
	birthday date,
	review_date date,
	seniority_date date,
	frozen_benefits_date date,
	benefits_date date,
	retired_date date,
	terminated_date date,
	peo_hired_date date,
-- 	self citext,
-- 	demographics citext,
-- 	pay_info citext,
-- 	badges citext,
-- 	profiles citext,
-- 	hcm_profiles citext,
-- 	hcm_fields citext,	
	managed_cost_centers_enabled boolean,
	ssn citext);

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_employee_details';
  	
insert into ukg.ext_employee_details (employee_id,username,first_name,last_name,/*photo_href,*/middle_name,external_id,
	employee_number,status,bsn,country,address_line_1,address_line_2,address_line_3,city,state,zip,number,
	number_addition,use_separate_mailing_address,ma_country,ma_address_line_1,ma_address_line_2,
	ma_address_line_3,ma_city,ma_state,ma_zip,ma_number,ma_number_addition,primary_account_id,ein_id,
-- 	force_change_password, locked,
	first_screen,cell_phone_country_id,cell_phone_country_code,cell_phone,
	home_phone_country_id,home_phone_country_code,home_phone,work_phone_country_id,work_phone_country_code,
	work_phone,preferred_phone,locale,managers,national_insurance,nickname,primary_email,secondary_email,
	salutation,social_insurance,social_security,suffix,timezone,
	cost_center_index,cost_center_id,
	add_to_new_hire_export,hired_date,deceased_date,re_hired_date,started_date,birthday,review_date,
	seniority_date,frozen_benefits_date,benefits_date,retired_date,
	terminated_date,peo_hired_date,
	self,demographics,pay_info,badges,profiles,hcm_profiles,hcm_fields,
	managed_cost_centers_enabled,ssn)
select (a.response->>'id')::bigint, lower(a.response->>'username') as username,
	initcap(a.response->>'first_name') as first_name, initcap(a.response->>'last_name') as last_name,
	a.response->>'photo_href', a.response->>'middle_name',
	a.response->>'external_id', a.response->>'employee_id',
	a.response->>'status', a.response->>'bsn',
	a.response->'address'->>'country', a.response->'address'->>'address_line_1',
	a.response->'address'->>'address_line_2', a.response->'address'->>'address_line_3',
	a.response->'address'->>'city', a.response->'address'->>'state',
	a.response->'address'->>'zip', a.response->'address'->>'number',
	a.response->'address'->>'number_adddition',
	(a.response->>'use_separate_mailing_address')::boolean,
	a.response->'mailing_address'->>'country', a.response->'mailing_address'->>'address_line_1',
	a.response->'mailing_address'->>'address_line_2',	a.response->'mailing_address'->>'address_line_3',
	a.response->'mailing_address'->>'city',	a.response->'mailing_address'->>'state',
	a.response->'mailing_address'->>'zip',	a.response->'mailing_address'->>'number',
	a.response->'mailing_address'->>'number_adddition',
	(a.response->>'primary_account_id')::bigint,	(a.response->'ein'->>'id')::bigint as ein_id,
-- 	(a.response->>'force_change_password')::boolean,	
-- 	(a.response->>'locked')::boolean,
	a.response->'first_screen'->>'id', a.response->'phones'->>'cell_phone_country_id',
	a.response->'phones'->>'cell_phone_country_code',	a.response->'phones'->>'cell_phone',
	a.response->'phones'->>'home_phone_country_id',	a.response->'phones'->>'home_phone_country_code',
	a.response->'phones'->>'home_phone',	a.response->'phones'->>'work_phone_country_id',
	a.response->'phones'->>'work_phone_country_code',	a.response->'phones'->>'work_phone',
	a.response->'phones'->>'preferred_phone' as preferred_phone,
	(a.response->'locale'->>'id')::bigint,
	a.response->'managers',	a.response->>'national_insurance',
	a.response->>'nickname',	a.response->>'primary_email',
	a.response->>'secondary_email',  a.response->>'salutation',
  a.response->>'social_insurance',	a.response->>'social_security',
	a.response->>'suffix',	a.response->>'timezone',
	(c->>'index')::integer,	(c->'value'->>'id')::bigint,
	(a.response->>'add_to_new_hire_export')::boolean,
	(a.response->'dates'->>'hired')::date,	
	(a.response->'dates'->>'deceased')::date,
	(a.response->'dates'->>'re_hired')::date,
	(a.response->'dates'->>'started')::date,
	(a.response->'dates'->>'birthday')::date,
	(a.response->'dates'->>'review')::date,
	(a.response->'dates'->>'seniority')::date,
	(a.response->'dates'->>'frozen_benefits')::date,
	(a.response->'dates'->>'benefits')::date,
	(a.response->'dates'->>'retired')::date,
	(a.response->'dates'->>'terminated')::date,
	(a.response->'dates'->>'peo_hired')::date,
	a.response->'_links'->>'self',
	a.response->'phones'->>'demographics',
	a.response->'phones'->>'pay-info',
	a.response->'phones'->>'badges',  
	a.response->'phones'->>'profiles',
	a.response->'phones'->>'hcm-profiles',
	a.response->'phones'->>'hcm-fields',
  (a.response->>'managed_cost_centers_enabled')::boolean,
  b->>'value'
-- select *
from ukg.json_employee_details a 
left join jsonb_array_elements(a.response->'national_id_numbers') as b(ssn) on true  
left join jsonb_array_elements(a.response->'cost_centers_info'->'defaults') as c(cost_centers) on true
-- select * from ukg.ext_employee_details limit 10
-- select distinct cost_center_type, cost_center_from_date from ukg.ext_employee_details
select count(*) from ukg.ext_employee_details


 
                    
create or replace function ukg.ext_employee_details()
  returns void as
$BODY$
	truncate ukg.ext_employee_details;

	insert into ukg.ext_employee_details (employee_id,username,first_name,last_name,/*photo_href,*/middle_name,external_id,
		employee_number,status,bsn,country,address_line_1,address_line_2,address_line_3,city,state,zip,number,
		number_addition,use_separate_mailing_address,ma_country,ma_address_line_1,ma_address_line_2,
		ma_address_line_3,ma_city,ma_state,ma_zip,ma_number,ma_number_addition,primary_account_id,ein_id,
-- 		force_change_password,	locked,
		first_screen,cell_phone_country_id,cell_phone_country_code,cell_phone,
		home_phone_country_id,home_phone_country_code,home_phone,work_phone_country_id,work_phone_country_code,
		work_phone,preferred_phone,locale,managers,national_insurance,nickname,primary_email,secondary_email,
		salutation,social_insurance,social_security,suffix,timezone,
		cost_center_index,cost_center_id,
		add_to_new_hire_export,hired_date,deceased_date,re_hired_date,started_date,birthday,review_date,
		seniority_date,frozen_benefits_date,benefits_date,retired_date,
		terminated_date,peo_hired_date,
-- 		self, demographics,
-- 		pay_info,badges,profiles,hcm_profiles,hcm_fields,
		managed_cost_centers_enabled,ssn)
	select (a.response->>'id')::bigint, lower(a.response->>'username') as username,
		initcap(a.response->>'first_name') as first_name, initcap(a.response->>'last_name') as last_name,
-- 		a.response->>'photo_href', 
		a.response->>'middle_name',
		a.response->>'external_id', a.response->>'employee_id',
		a.response->>'status', a.response->>'bsn',
		a.response->'address'->>'country', a.response->'address'->>'address_line_1',
		a.response->'address'->>'address_line_2', a.response->'address'->>'address_line_3',
		a.response->'address'->>'city', a.response->'address'->>'state',
		a.response->'address'->>'zip', a.response->'address'->>'number',
		a.response->'address'->>'number_adddition',
		(a.response->>'use_separate_mailing_address')::boolean,
		a.response->'mailing_address'->>'country', a.response->'mailing_address'->>'address_line_1',
		a.response->'mailing_address'->>'address_line_2',	a.response->'mailing_address'->>'address_line_3',
		a.response->'mailing_address'->>'city',	a.response->'mailing_address'->>'state',
		a.response->'mailing_address'->>'zip',	a.response->'mailing_address'->>'number',
		a.response->'mailing_address'->>'number_adddition',
		(a.response->>'primary_account_id')::bigint,	(a.response->'ein'->>'id')::bigint as ein_id,
-- 		(a.response->>'force_change_password')::boolean,	
	-- 	(a.response->>'locked')::boolean,
		a.response->'first_screen'->>'id', a.response->'phones'->>'cell_phone_country_id',
		a.response->'phones'->>'cell_phone_country_code',	a.response->'phones'->>'cell_phone',
		a.response->'phones'->>'home_phone_country_id',	a.response->'phones'->>'home_phone_country_code',
		a.response->'phones'->>'home_phone',	a.response->'phones'->>'work_phone_country_id',
		a.response->'phones'->>'work_phone_country_code',	a.response->'phones'->>'work_phone',
		a.response->'phones'->>'preferred_phone' as preferred_phone,
		(a.response->'locale'->>'id')::bigint,
		a.response->'managers',	a.response->>'national_insurance',
		a.response->>'nickname',	a.response->>'primary_email',
		a.response->>'secondary_email',  a.response->>'salutation',
		a.response->>'social_insurance',	a.response->>'social_security',
		a.response->>'suffix',	a.response->>'timezone',
		(c->>'index')::integer,	(c->'value'->>'id')::bigint,
		(a.response->>'add_to_new_hire_export')::boolean,
		(a.response->'dates'->>'hired')::date,	
		(a.response->'dates'->>'deceased')::date,
		(a.response->'dates'->>'re_hired')::date,
		(a.response->'dates'->>'started')::date,
		(a.response->'dates'->>'birthday')::date,
		(a.response->'dates'->>'review')::date,
		(a.response->'dates'->>'seniority')::date,
		(a.response->'dates'->>'frozen_benefits')::date,
		(a.response->'dates'->>'benefits')::date,
		(a.response->'dates'->>'retired')::date,
		(a.response->'dates'->>'terminated')::date,
		(a.response->'dates'->>'peo_hired')::date,
-- 		a.response->'_links'->>'self',
-- 		a.response->'phones'->>'demographics',
-- 		a.response->'phones'->>'pay-info',
-- 		a.response->'phones'->>'badges',  
-- 		a.response->'phones'->>'profiles',
-- 		a.response->'phones'->>'hcm-profiles',
-- 		a.response->'phones'->>'hcm-fields',
		(a.response->>'managed_cost_centers_enabled')::boolean,
		b->>'value'
	from ukg.json_employee_details a 
	left join jsonb_array_elements(a.response->'national_id_numbers') as b(ssn) on true  
	left join jsonb_array_elements(a.response->'cost_centers_info'->'defaults') as c(cost_centers) on true;
$BODY$
language sql;	





  
looks like emily olson has 2 managers
mgr 1 jun, mgr 2 laura
select * 
from test_1 a
join (
  select username
  from test_1 
  group by username
  having count(*) > 1) b on a.username = b.username
  order by a.username


select (a.response->>'id')::bigint, lower(a.response->>'username') as username,
	initcap(a.response->>'first_name') as first_name, initcap(a.response->>'last_name') as last_name,
	a.response->'phones'->>'preferred_phone' as preferred_phone,
	(a.response->'locale'->>'id')::integer,
	b.*
from ukg.json_employee_details a 
left join jsonb_array_elements(a.response->'managers') with ordinality as b(managers) on true  
  and b.ordinality = 2
where lower(a.response->>'username') = 'eolson';

select (a.response->>'id')::bigint, lower(a.response->>'username') as username,
	initcap(a.response->>'first_name') as first_name, initcap(a.response->>'last_name') as last_name,
	a.response->'phones'->>'preferred_phone' as preferred_phone,
	(a.response->'locale'->>'id')::integer,
	a.response->'managers'
from ukg.json_employee_details a 
where lower(a.response->>'username') = 'eolson';
----------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------
-- employee demographics

drop table if exists ukg.json_employee_demographics cascade;
create unlogged table ukg.json_employee_demographics (
	id bigint not null,
	response jsonb);
comment on table ukg.json_employee_demographics is 'raw scrape of ukg endpoint Get Single Employee Demographics; https://beta.rydellvision.com:8888/ukg/employees/12986719382/demographics';

select * from  ukg.json_employee_demographics

"https://beta.rydellvision.com:8888/ukg/employees/12986719382/demographics"

drop table if exists ukg.ext_employee_demographics cascade;
create table ukg.ext_employee_demographics (
	employee_id bigint primary key,
	citizenship citext check (citizenship in ('US_CITIZEN','PERMANENT_RESIDENT','ALIEN_WITH_WORK_PERMIT','NONCITIZEN_NATIONAL_OF_US')),
	ethnicity citext check (ethnicity in ('HISPANIC_OR_LATINO','WHITE', 'BLACK_OR_AFRICAN_AMERICAN' ,
			'NATIVE_HAWAIIAN_OR_OTHER_PACIFIC_ISLANDER' ,'ASIAN', 'AMERICAN_INDIAN_OR_ALASKA_NATIVE', 'TWO_OR_MORE_RACES')),
	eye_color citext,
	full_time_student boolean,
	gender citext check (gender in ('MALE','FEMALE','UNDEFINED')),
	height citext,
	referral integer,
	seasonal boolean,
	visa_type citext,
	visa_number citext,
	visa_expiration_date date);
		

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_employee_demographics';

insert into ukg.ext_employee_demographics (employee_id,citizenship,ethnicity,eye_color,full_time_student,gender,height,referral,
	seasonal,visa_type,visa_number,visa_expiration_date)  
select a.id,
	a.response->>'citizenship',
	a.response->>'ethnicity',
	a.response->>'eye_color',
	(a.response->>'full_time_student')::boolean,
	a.response->>'gender',
	a.response->>'height',
	(a.response->>'referral')::bigint,
	(a.response->>'seasonal')::boolean,
	a.response->>'visa_type',
	a.response->>'visa_number',
	(a.response->>'visa_expiration_date')::date
-- select *
from ukg.json_employee_demographics a;

-- select * from ukg.ext_employee_demographics;
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- employee pay information

drop table if exists ukg.json_employee_pay_info cascade;
create unlogged table ukg.json_employee_pay_info (
	id bigint not null,
	response jsonb);
comment on table ukg.json_employee_pay_info is 'raw scrape of ukg endpoint Get Single Employee Pay Info; https://beta.rydellvision.com:8888/ukg/employees/12986719382/pay-info';

select * from ukg.json_employee_pay_info

drop table if exists ukg.ext_employee_pay_info cascade;
create table ukg.ext_employee_pay_info (
	employee_id bigint,
	default_job_id bigint,
	job_change_reason_code citext,
	job_last_changed date,
	standard_work_day integer,
	employee_type_id bigint,
	pay_type_id integer,
	shift_premium_id integer,
	medical_eligibility citext check (medical_eligibility in ('ELIGIBLE','NOT_ELIGIBLE')),
	dependent_benefits_eligibility citext check(dependent_benefits_eligibility in ('ELIGIBLE','NOT_ELIGIBLE')),
	benefit_auto_enrollment_eligibility citext check (benefit_auto_enrollment_eligibility in ('ELIGIBLE','NOT_ELIGIBLE')),
	eeo_classification citext check (eeo_classification in('EXECUTIVE_OR_SENIOR_LEVEL_OFFICIALS_AND_MANAGERS', 
			'FIRST_OR_MID_LEVEL_OFFICIALS_AND_MANAGERS','PROFESSIONALS',  'TECHNICIANS','SALES_WORKERS',
			'ADMINISTRATIVE_SUPPORT_WORKERS','CRAFT_WORKERS','OPERATIVES', 'LABORERS_AND_HELPERS','SERVICE_WORKERS','')), 
	workers_comp_code citext,
	union_id integer,
	payroll_classification citext check (payroll_classification in ('REGULAR','HOUSEHOLD','FORM_943')),
	bank_account_number citext,
	pay_grade_id integer,
	gl_codes jsonb); -- don't know if this will be used or not

select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_employee_pay_info';

create or replace function ukg.ext_employee_pay_info()
returns void as
$BODY$
/*

*/
	truncate ukg.ext_employee_pay_info;
	insert into ukg.ext_employee_pay_info (employee_id,default_job_id,job_change_reason_code,
		job_last_changed,standard_work_day,employee_type_id,pay_type_id,shift_premium_id,
		medical_eligibility,dependent_benefits_eligibility,benefit_auto_enrollment_eligibility,
		eeo_classification,workers_comp_code,union_id,payroll_classification,bank_account_number,pay_grade_id,gl_codes)  
	select a.id,
		(a.response->'default_job'->>'id')::bigint,
		a.response->'job_change_reason_code'->>'display_name',
		(a.response->>'job_last_changed')::date,
		(a.response->>'standard_work_day')::bigint,
		(a.response->'employee_type'->>'id')::bigint,
		(a.response->'pay_type'->>'id')::integer, 
		(a.response->'shift_premium'->>'id')::integer,
		a.response->>'medical_eligibility',
		a.response->>'dependent_benefits_eligibility',
		a.response->>'benefit_auto_enrollment_eligibility',
		a.response->>'eeo_classification',
		a.response->'workers_comp_code'->>'external_id',
		(a.response->'union'->>'id')::integer,
		a.response->>'payroll_classification',
		a.response->>'bank_account_number',
		(a.response->'pay_grade'->>'id')::integer,
		b.*
	from ukg.json_employee_pay_info a 
	left join jsonb_array_elements(a.response->'gl_codes') as b(ssn) on true;
$BODY$
language sql;


----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- employee profiles

drop table if exists ukg.json_employee_profiles cascade;
create unlogged table ukg.json_employee_profiles (
	id bigint not null,
	response jsonb);
comment on table ukg.json_employee_profiles is 'raw scrape of ukg endpoint Get Single Employee Profiles; https://beta.rydellvision.com:8888/ukg/employees/12986719382/profiles';

select a.* 
from ukg.json_employee_profiles a

drop table if exists ukg.ext_employee_profiles cascade;
create table ukg.ext_employee_profiles (
	employee_id bigint primary key,
  attestation jsonb,
  access jsonb,
  accruals jsonb,
  benefit jsonb,
  bradford_factor jsonb,
  competency jsonb,
  counter_distribution jsonb,
  data_retention jsonb,
  delivery_policy_1099 jsonb,
  delivery_policy_pst jsonb,
  delivery_policy_w2 jsonb,
  demographic jsonb,
  employee_sync jsonb,
  holiday jsonb,
  labor_distribution jsonb,
  leave_of_absence jsonb,
  pay_calculations jsonb,
  pay_period jsonb,
  pay_prep jsonb,
  pension jsonb,
  performance_review jsonb,
  points jsonb,
  pst_population jsonb,
  rate_tables jsonb,    
  scheduler jsonb,
  scorecard jsonb,
  security jsonb,
  succession jsonb,
  time_off_planning jsonb,
  timesheet jsonb,
  training jsonb,
  training jsonb,
  ts_auto_population jsonb,
  work_schedule jsonb,
  workday_breakdown jsonb,
  working_time_regulations jsonb,
  mercury_layout jsonb,
  offline_restrictions jsonb,
  onboarding jsonb);


select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = 'ext_employee_profiles';

create or replace function ukg.ext_employee_profiles()
returns void as
$BODY$
/*

*/
	truncate ukg.ext_employee_profiles;
	insert into ukg.ext_employee_profiles (employee_id,attestation,access,accruals,benefit,bradford_factor,
		competency,counter_distribution,data_retention,delivery_policy_1099,delivery_policy_pst,
		delivery_policy_w2,demographic,employee_sync,holiday,labor_distribution,leave_of_absence,
		pay_calculations,pay_period,pay_prep,pension,performance_review,points,pst_population,rate_tables,
		scheduler,scorecard,security,succession,time_off_planning,timesheet,training,ts_auto_population,work_schedule,
		workday_breakdown,working_time_regulations,mercury_layout,offline_restrictions,onboarding)
	select a.id, a.response->'attestation' as attestation,a.response->'access' as access,a.response->'accruals' as accruals,a.response->'benefit' as benefit,
		a.response->'bradford_factor' as bradford_factor,a.response->'competency' as competency,a.response->'counter_distribution' as counter_distribution,a.response->'data_retention' as data_retention,
		a.response->'delivery_policy_1099' as delivery_policy_1099,a.response->'delivery_policy_pst' as delivery_policy_pst,a.response->'delivery_policy_w2' as delivery_policy_w2,a.response->'demographic' as demographic,
		a.response->'employee_sync' as employee_sync,a.response->'holiday' as holiday,a.response->'labor_distribution' as labor_distribution,a.response->'leave_of_absence' as leave_of_absence,
		a.response->'pay_calculations' as pay_calculations,a.response->'pay_period' as pay_period,a.response->'pay_prep' as pay_prep,a.response->'pension' as pension,
		a.response->'performance_review' as performance_review,a.response->'points' as points,a.response->'pst_population' as pst_population,a.response->'rate_tables' as rate_tables,
		a.response->'scheduler' as scheduler,a.response->'scorecard' as scorecard,a.response->'security' as security,a.response->'succession' as succession,
		a.response->'time_off_planning' as time_off_planning,a.response->'timesheet' as timesheet,a.response->'training' as training,a.response->'ts_auto_population' as ts_auto_population,
		a.response->'work_schedule' as work_schedule,a.response->'workday_breakdown' as workday_breakdown,a.response->'working_time_regulations' as working_time_regulations,a.response->'mercury_layout' as mercury_layout,
		a.response->'offline_restrictions' as offline_restrictions,a.response->'onboarding' as onboarding
	from ukg.json_employee_profiles a;
$BODY$
language sql;

drop table if exists ukg.employee_profiles cascade;
create table ukg.employee_profiles (
  employee_id bigint primary key,
  rate_table_id bigint,
  rate_table_effective_from date);
comment on table ukg.employee_profiles is 'ok, this is a weird one, this will be the table to house all the different profile ids,
  but, starting with just what is immediately needed, in this case, rate table.  This table serves to link ukg.employees with,
  in this initial case, ukg.personal_rate_tables.  if this model persists, then as they are needed, additional profile ids
  will be added.  Table is updated nightly in luigl, ukg.py with a call to function ukg.update_employee_profiles()';

create or replace function ukg.update_employee_profiles()
returns void as
$BODY$
/*
  select ukg.update_employee_profiles();
*/
truncate ukg.employee_profiles;
insert into ukg.employee_profiles
select a.employee_id, (c->>'id')::bigint as rate_table_id,
  (c->>'effective_from')::date as effeective_from
from ukg.ext_employee_profiles a 
left join jsonb_array_elements(a.rate_tables) b on true
  and b->>'index' = '0'
left join jsonb_array_elements(b->'refs') c on true; 
$BODY$
language sql;

comment on function ukg.update_employee_profiles() is 'called nightly from luigi, ukg.py to truncate and repopulate table ukg.employee_profiles,
  1/28/22 at this time populating rate_table data only';

select * from ukg.employee_profiles
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- personal rate tables

drop table if exists ukg.json_personal_rate_tables cascade;
create unlogged table ukg.json_personal_rate_tables (
	id bigint not null, -- rate table id, derived from employee profile
	response jsonb);
comment on table ukg.json_personal_rate_tables is 'raw scrape of ukg endpoint Get Single Rate Table Schedule; https://beta.rydellvision.com:8888/ukg/rates-tables/id/schedules';

-- this is used to get all the rate_table_ids
select a.employee_id, (c->>'id')::bigint as rate_table_id
from ukg.ext_employee_profiles a 
left join jsonb_array_elements(a.rate_tables) b on true
  and b->>'index' = '0'
left join jsonb_array_elements(b->'refs') c on true  
limit 10

drop table if exists ukg.personal_rate_tables cascade;
create table ukg.personal_rate_tables (
  rate_table_id bigint not null,
  rate_item_id bigint not null,
  current_rate numeric not null,
  rate_schedule_type citext,
  current_rate_effective_from date,
  current_rate_effective_to date,
  effective_from date,
  effective_to date,
  description citext,
  counter citext,
  primary key (rate_table_id,rate_item_id));
comment on table ukg.personal_rate_tables is 'updated nightly in luigi, ukg.py by a call to ukg.update_personal_rate_tables(),
		data maintained for active employees only. currently, the data loaded nightly is for active employees only';

create or replace function ukg.update_personal_rate_tables()
returns void as
$BODY$
/*
  select ukg.update_personal_rate_tables();
*/
truncate ukg.personal_rate_tables;  
insert into ukg.personal_rate_tables  
select id as rate_table_id, (c->>'id')::bigint as rate_item_id,
	(b->'rate_table'->>'current_rate')::numeric as current_rate, 
	b->>'type' as type,
	(b->'rate_table'->>'current_effective_from')::date as current_rate_effective_from, 	
	(b->'rate_table'->>'current_effective_to')::date as current_rate_effective_to, 
	(b->'rate_table'->>'effective_from')::date as effective_from, 
	(b->'rate_table'->>'effective_to')::date as effective_to, 
	b->>'description' as description, 
	b->'counter_rec'->'counter'->>'name' as counter
from ukg.json_personal_rate_tables a
left join jsonb_array_elements(response->'rate_schedules') b on 1 = 1
left join jsonb_array_elements(b->'rate_items') c on true
  and c->>'effective_from' = b->'rate_table'->>'current_effective_from'
where exists ( -- active employees only
  select 1
  from ukg.employee_profiles m
  join ukg.employees n on m.employee_id = n.employee_id
    and n.status = 'active'
  join ukg.employee_profiles o on n.employee_id = o.employee_id
    and o.rate_table_id = a.id);
$BODY$
language sql;    

comment on function ukg.update_personal_rate_tables() is 'called nightly from luigi, ukg.py to truncate and repopulate table ukg.personal_rate_tables.
  function only loads data for active employees. this clause in the c join, and c->>''effective_from'' = b->''rate_table''->>''current_effective_from''
  is to insure that the rate_items object returns a single row';

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- employee accrual balances
-- optional date parameter: /accrual/1/balances?asOfDate=2022-02-18

drop table if exists ukg.json_employee_accrual_balances cascade;
create unlogged table ukg.json_employee_accrual_balances (
	id bigint not null, 
	response jsonb);
comment on table ukg.json_personal_rate_tables is 'raw scrape of ukg endpoint Get Single Rate Table Schedule; https://beta.rydellvision.com:8888/ukg/employees/12987528631/accrual/1/balances';

{{vision}}/employees/12986298520/accrual/1/balances?asOfDate=2022-02-18

drop table if exists ukg.ext_employee_accrual_balances cascade;
create table ukg.ext_employee_accrual_balances (
  start_date date,
  end_date date,
  employee_id bigint primary key,
  pending_approval integer default '0',
  regular_day_time integer,
  remaining integer default '0',
  scheduled integer default '0',
  taken integer default '0',
  time_off_name citext);
comment on table ukg.personal_rate_tables is 'updated nightly in luigi';


create or replace function ukg.ext_employee_accrual_balances()
returns void as
$BODY$
/*
  select ukg.ext_employee_accrual_balances();
*/
truncate ukg.ext_employee_accrual_balances;  
insert into ukg.ext_employee_accrual_balances  
select (b->'accrual_year'->>'start_date')::date as start_date, 
	(b->'accrual_year'->>'end_date')::date as end_date, 
	id as employee_id,
	(b->>'pending_approval')::integer as pending_approval, 	
	(b->>'regular_day_time')::integer as regular_day_time,
	(b->>'remaining')::integer as remaining,
	(b->>'scheduled')::integer as scheduled,
	(b->>'taken')::integer as taken,
	b->'time_off'->>'name' as time_off_name
from ukg.json_employee_accrual_balances a
left join jsonb_array_elements(response->'accrual_balances') b on true;
$BODY$
language sql;    

comment on function ukg.ext_employee_accrual_balances() is 'called nightly from luigi, ukg.py, truncates and repopulates ukg.ext_employee_accrual_balances';

select * from ukg.ext_employee_accrual_balances
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- some ts & dev queries

select id, c->>'id', 
	b->'rate_table'->>'current_rate' as current_rate, 
	b->>'type' as type,
	b->'rate_table'->>'current_effective_from' as current_rate_effective_from, 	
	b->'rate_table'->>'current_effective_to' as current_rate_effective_to, 
	b->'rate_table'->>'effective_from' as effective_from, 
	b->'rate_table'->>'effective_to' as effective_to, 
	b->>'description' as description, 
	b->'counter_rec'->'counter'->>'name' as counter

-- select *
from ukg.json_personal_rate_tables a
left join jsonb_array_elements(response->'rate_schedules') b on 1 = 1
left join jsonb_array_elements(b->'rate_items') c on true
  and c->>'effective_from' = b->'rate_table'->>'current_effective_from'
-- order by id
where id = 4401193539

select a.last_name, a.first_name, c.*
from ukg.employees a
join ukg.employee_profiles b on a.employee_id = b.employee_id
join ukg.personal_rate_tables c on b.rate_table_id = c.rate_table_id
  and c.counter in ('volunteer pto','holiday')
order by a.last_name

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- employee 

drop table if exists ukg.json_ cascade;
create unlogged table ukg.json_ (
	id bigint not null,
	response jsonb);
comment on table ukg.json_ is 'raw scrape of ukg endpoint Get Single Employee Demographics; https://beta.rydellvision.com:8888/ukg/employees/12986719382/demographics';


drop table if exists ukg.ext_ cascade;
create table ukg.ext_ ()



select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = '';

insert into ukg.ext_ ()  
select (a.response->>'id')::bigint
-- select *
from ukg.json_s a 
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- employee 

drop table if exists ukg.json_ cascade;
create unlogged table ukg.json_ (
	id bigint not null,
	response jsonb);
comment on table ukg.json_ is 'raw scrape of ukg endpoint Get Single Employee Demographics; https://beta.rydellvision.com:8888/ukg/employees/12986719382/demographics';


drop table if exists ukg.ext_ cascade;
create table ukg.ext_ ()



select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = '';

insert into ukg.ext_ ()  
select (a.response->>'id')::bigint
-- select *
from ukg.json_s a 
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- employee 

drop table if exists ukg.json_ cascade;
create unlogged table ukg.json_ (
	id bigint not null,
	response jsonb);
comment on table ukg.json_ is 'raw scrape of ukg endpoint Get Single Employee Demographics; https://beta.rydellvision.com:8888/ukg/employees/12986719382/demographics';


drop table if exists ukg.ext_ cascade;
create table ukg.ext_ ()



select string_agg(column_name, ',' order by ordinal_position)
from information_schema.columns
where table_schema = 'ukg'
  and table_name = '';

insert into ukg.ext_ ()  
select (a.response->>'id')::bigint
-- select *
from ukg.json_s a 
----------------------------------------------------------------------------------------------------




































































	