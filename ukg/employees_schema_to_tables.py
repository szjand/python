# encoding=utf-8
"""
"""
import json
from jsonschema2db import JSONSchemaToPostgres
import db_cnx

schema = json.load(open('json_files/employees_schema.json'))
translator = JSONSchemaToPostgres(
    schema,
    postgres_schema='ukg_test',
    item_col_name='item_id',
    item_col_type='integer'
)
with db_cnx.pg() as pg_con:
    translator.create_tables(pg_con)