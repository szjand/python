# encoding=utf-8
"""
"""
import db_cnx
import requests
import time


# ************************************************************
#  EMPLOYEES
# ************************************************************

def get_all_employees_file():
    url = "https://beta.rydellvision.com:8888/ukg/employees"
    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    print(response.text)


def get_all_employees():
    url = 'https://beta.rydellvision.com:8888/ukg/employees'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_employees')
            sql = """
                insert into ukg.json_employees (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def get_changed_employees():
    url = 'https://beta.rydellvision.com:8888/ukg/employees/changed'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_changed_employees')
            sql = """
                insert into ukg.json_changed_employees (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def get_employee_details():
    """
    8/21/21 5 minutes
    :return:
    """
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            id_array = []
            pg_cur.execute('truncate ukg.json_employee_details')
            sql = """
                select distinct b->>'id'
                from ukg.json_employees a
                join jsonb_array_elements(a.response->'employees') as b(employees) on true;	            
            """
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                id_array.append(str(row[0]))
            for the_id in id_array:
                url = 'https://beta.rydellvision.com:8888/ukg/employees/' + the_id
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_employee_details (id,response) values ({0},'{1}');
                    """.format(the_id, data)
                    pg_cur_2.execute(sql)


def get_employee_demographics():
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            id_array = []
            pg_cur.execute('truncate ukg.json_employee_demographics')
            sql = 'select distinct employee_id from ukg.ext_employees;'
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                id_array.append(str(row[0]))
            for the_id in id_array:
                url = 'https://beta.rydellvision.com:8888/ukg/employees/' + the_id + '/demographics'
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_employee_demographics (id,response) values ({0},'{1}');
                    """.format(the_id, data)
                    pg_cur_2.execute(sql)


def get_employee_pay_info():
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            id_array = []
            pg_cur.execute('truncate ukg.json_employee_pay_info')
            sql = 'select distinct employee_id from ukg.ext_employees;'
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                id_array.append(str(row[0]))
            for the_id in id_array:
                url = 'https://beta.rydellvision.com:8888/ukg/employees/' + the_id + '/pay-info'
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_employee_pay_info (id,response) values ({0},'{1}');
                    """.format(the_id, data)
                    pg_cur_2.execute(sql)


def get_employee_profiles():
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            id_array = []
            pg_cur.execute('truncate ukg.json_employee_profiles')
            sql = 'select distinct employee_id from ukg.ext_employees;'
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                id_array.append(str(row[0]))
            for the_id in id_array:
                url = 'https://beta.rydellvision.com:8888/ukg/employees/' + the_id + '/profiles'
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_employee_profiles (id,response) values ({0},'{1}');
                    """.format(the_id, data)
                    pg_cur_2.execute(sql)


def get_time_entries():
    """
    """
    url = 'https://beta.rydellvision.com:8888/ukg/time-entries?start_date=2021-12-01&end_date=2021-12-16'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_time_entries')
            sql = """
                insert into ukg.json_time_entries (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


# ************************************************************
#  LOOKUPS
# ************************************************************
def benefit_types():
    url = 'https://beta.rydellvision.com:8888/ukg/lookup/benefit-types'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_benefit_types')
            sql = """
                insert into ukg.json_benefit_types (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def benefit_plans():
    url = 'https://beta.rydellvision.com:8888/ukg/lookup/benefit-plans'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_benefit_plans')
            sql = """
                insert into ukg.json_benefit_plans (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def jobs():
    url = 'https://beta.rydellvision.com:8888/ukg/lookup/jobs'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_jobs')
            sql = """
                insert into ukg.json_jobs (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def deduction_codes():
    url = 'https://beta.rydellvision.com:8888/ukg/lookup/deduction-codes'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_deduction_codes')
            sql = """
                insert into ukg.json_deduction_codes (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def pay_types():
    url = 'https://beta.rydellvision.com:8888/ukg/lookup/pay-types'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_pay_types')
            sql = """
                insert into ukg.json_pay_types (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def payroll_types():
    url = 'https://beta.rydellvision.com:8888/ukg/lookup/payroll-types'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_payroll_types')
            sql = """
                insert into ukg.json_payroll_types (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def payroll_batch_types():
    url = 'https://beta.rydellvision.com:8888/ukg/lookup/payroll-batch-types'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_payroll_batch_types')
            sql = """
                insert into ukg.json_payroll_batch_types (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def pay_statement_types():
    url = 'https://beta.rydellvision.com:8888/ukg/lookup/pay-statement-types'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_pay_statement_types')
            sql = """
                insert into ukg.json_pay_statement_types (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


# ************************************************************
#  PAY PERIODS
# ************************************************************
def get_pay_periods():
    url = 'https://beta.rydellvision.com:8888/ukg/pay-periods'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_pay_periods')
            sql = """
                insert into ukg.json_pay_periods (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


# ************************************************************
#  COST CENTERS
# ************************************************************
def get_cost_centers():
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_cost_centers')
            for idx in range(0, 9):
                url = 'https://beta.rydellvision.com:8888/ukg/cost-centers?tree_index=' + str(idx)
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_cost_centers (cost_center_index,response) values ({0},'{1}');
                    """.format(idx, data)
                    pg_cur_2.execute(sql)


def get_cost_center_details():
    """
    """
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            id_array = []
            pg_cur.execute('truncate ukg.json_cost_center_details')
            sql = """
                select distinct id from ukg.ext_cost_centers;	            
            """
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                id_array.append(str(row[0]))
            for the_id in id_array:
                url = 'https://beta.rydellvision.com:8888/ukg/cost-centers/' + the_id
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_cost_center_details (id,response) values ({0},'{1}');
                    """.format(the_id, data)
                    pg_cur_2.execute(sql)


def get_cost_centers_jobs():
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_cost_center_jobs')
            url = 'https://beta.rydellvision.com:8888/ukg/cost-center-jobs'
            the_response = requests.request('GET', url)
            data = the_response.text
            data = data.replace("'", "''")
            with pg_con.cursor() as pg_cur_2:
                sql = """
                    insert into ukg.json_cost_center_jobs (response) values ('{0}');
                """.format(data)
                pg_cur_2.execute(sql)


def get_cost_center_job_details():
    """
    """
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            id_array = []
            pg_cur.execute('truncate ukg.json_cost_center_job_details')
            sql = """
                select b->'id'
                from ukg.json_cost_center_jobs a
                join jsonb_array_elements(a.response->'costCenterJobs') b on true;            
            """
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                id_array.append(str(row[0]))
            for the_id in id_array:
                url = 'https://beta.rydellvision.com:8888/ukg/cost-center-jobs/' + the_id
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_cost_center_job_details (id,response) values ({0},'{1}');
                    """.format(the_id, data)
                    pg_cur_2.execute(sql)


# ************************************************************
#  PAYROLLS
# ************************************************************
def get_payrolls():
    url = 'https://beta.rydellvision.com:8888/ukg/payrolls'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_payrolls')
            sql = """
                insert into ukg.json_payrolls (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


def get_pay_statements():
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            id_array = []
            pg_cur.execute('truncate ukg.json_pay_statements')
            sql = 'select distinct payroll_id from ukg.ext_payrolls;'
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                id_array.append(str(row[0]))
            for the_id in id_array:
                url = 'https://beta.rydellvision.com:8888/ukg/payrolls/' + the_id + '/pay-statements'
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_pay_statements (id,response) values ({0},'{1}');
                    """.format(the_id, data)
                    pg_cur_2.execute(sql)


def get_payroll_batches():
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            id_array = []
            pg_cur.execute('truncate ukg.json_payroll_batches')
            sql = 'select distinct payroll_id from ukg.ext_payrolls;'
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                id_array.append(str(row[0]))
            for the_id in id_array:
                url = 'https://beta.rydellvision.com:8888/ukg/payrolls/' + the_id + '/batches'
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_payroll_batches (id,response) values ({0},'{1}');
                    """.format(the_id, data)
                    pg_cur_2.execute(sql)


def get_pay_statement_details():
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            id_array = []
            pg_cur.execute('truncate ukg.json_pay_statement_details')
            sql = """
                SELECT DISTINCT payroll_id, pay_statement_id from ukg.ext_pay_statements order by payroll_id;
            """
            pg_cur.execute(sql)
            for row in pg_cur.fetchall():
                id_array.append([str(row[0]), str(row[1])])
            for item in id_array:
                print(time.localtime())
                url = 'https://beta.rydellvision.com:8888/ukg/payrolls/' + item[0] + '/pay-statements/' + item[1]
                the_response = requests.request('GET', url)
                data = the_response.text
                data = data.replace("'", "''")
                print(the_response)
                with pg_con.cursor() as pg_cur_2:
                    sql = """
                        insert into ukg.json_pay_statement_details 
                            (payroll_id, pay_statement_id,response) values ({0},{1},'{2}');
                    """.format(item[0], item[1],  data)
                    pg_cur_2.execute(sql)


def get_timeoffs():
    """
    V1->Company->Time Offs: the time off identifiers
    """
    url = 'https://beta.rydellvision.com:8888/ukg/timeoffs'
    the_response = requests.request('GET', url)
    data = the_response.text
    data = data.replace("'", "''")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute('truncate ukg.json_timeoffs')
            sql = """
                insert into ukg.json_timeoffs (response) values ('{0}');
            """.format(data)
            pg_cur.execute(sql)


if __name__ == '__main__': get_pay_statement_details()

# for idx, the_id in enumerate(id_array):
#     print(idx)
#     # print(idx, the_id)
#     # print(time.localtime())
#     # time.sleep(2)