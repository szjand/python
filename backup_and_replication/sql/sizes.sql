﻿SELECT *, pg_size_pretty(total_bytes) AS total
    , pg_size_pretty(index_bytes) AS INDEX
    , pg_size_pretty(toast_bytes) AS toast
    , pg_size_pretty(table_bytes) AS TABLE
  FROM (
  SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes FROM (
      SELECT c.oid,nspname AS table_schema, relname AS TABLE_NAME
              , c.reltuples AS row_estimate
              , pg_total_relation_size(c.oid) AS total_bytes
              , pg_indexes_size(c.oid) AS index_bytes
              , pg_total_relation_size(reltoastrelid) AS toast_bytes
          FROM pg_class c
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE relkind = 'r'
  ) a
) a
order by index_bytes desc
order by total_bytes desc
limit 50
order by table_schema, table_name


-- table sizes  sums total disk space used by the table including indexes and toasted data rather than breaking out the individual piece
SELECT nspname || '.' || relname AS "relation",
    pg_size_pretty(pg_total_relation_size(C.oid)) AS "total_size"
  FROM pg_class C
  LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema')
    AND C.relkind <> 'i'
    AND nspname !~ '^pg_toast'
  ORDER BY pg_total_relation_size(C.oid) DESC
  LIMIT 2000;

-- schema size
select distinct schema_name, the_size, schema_size
from (
  select a.*, sum(size) over (partition by schema_name) as the_size, pg_size_pretty(sum(size) over (partition by schema_name)) as schema_size
  from (
    SELECT nspname as schema_name, relname "table", pg_total_relation_size(C.oid) as size,
        pg_size_pretty(pg_total_relation_size(C.oid)) AS "total_size"
      FROM pg_class C1
      LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
      WHERE nspname NOT IN ('pg_catalog', 'information_schema')
        AND C.relkind <> 'i'
        AND nspname !~ '^pg_toast') a) b
ORDER BY the_size desc


SELECT schema_name, 
       pg_size_pretty(sum(table_size)::bigint),
       (sum(table_size) / pg_database_size(current_database())) * 100
FROM (
  SELECT pg_catalog.pg_namespace.nspname as schema_name,
         pg_relation_size(pg_catalog.pg_class.oid) as table_size
  FROM   pg_catalog.pg_class
     JOIN pg_catalog.pg_namespace ON relnamespace = pg_catalog.pg_namespace.oid
) t
GROUP BY schema_name
order by sum(table_size) desc 
ORDER BY schema_name


chr;    10423336960;  9940 MB
fin;    9570705408;   9127 MB
dds;    8757067776;   8351 MB
arkona; 6655238144;   6347 MB
afton;  5565104128;   5307 MB
greg;   4268195840;   4070 MB
ads;    2877693952;   2744 MB
cw;     2719268864;   2593 MB
dc;     2495569920;   2380 MB
 



-- pg_database_size() includes the sizes for indexes – a_horse_with_no_name Sep 20 
select pg_database_size('cartiva'), pg_size_pretty(pg_database_size('cartiva'))

59,523,258,904


SELECT 'drop table chr.' || table_name || ' cascade;' FROM information_schema.tables 
WHERE table_schema = 'chr'
  and table_name <> 'describe_vehicle'

drop table chr.style_generic_equipment cascade;
drop table chr.body_styles cascade;
drop table chr.category_headers cascade;
drop table chr.chrome_log cascade;
drop table chr.cons_info cascade;
drop table chr.std_headers cascade;
drop table chr.categories cascade;
drop table chr.colors cascade;
drop table chr.divisions cascade;
drop table chr.style_wheel_base cascade;
drop table chr.category cascade;
drop table chr.ci_types cascade;
drop table chr.jpgs cascade;
drop table chr.maint_nvd cascade;
drop table chr.maint_vindata cascade;
drop table chr.manufacturers cascade;
drop table chr.mkt_class cascade;
drop table chr.models cascade;
drop table chr.norm_ci_labels cascade;
drop table chr.norm_cons_info cascade;
drop table chr.opt_headers cascade;
drop table chr.opt_kinds cascade;
drop table chr.prices cascade;
drop table chr.options cascade;
drop table chr.standards cascade;
drop table chr.style_cats cascade;
drop table chr.styles cascade;
drop table chr.subdivisions cascade;
drop table chr.tech_specs cascade;
drop table chr.tech_title_header cascade;
drop table chr.tech_titles cascade;
drop table chr.version cascade;
drop table chr.vin_equipment cascade;
drop table chr.vin_pattern cascade;
drop table chr.vin_pattern_style_mapping cascade;
drop table chr.year_make_model_style cascade;
drop table chr.z_grouped_make_models cascade;
drop table chr.z_test_for_dups cascade;
drop table chr.z_tool_1 cascade;
drop table chr.z_vehicle_items cascade;
drop table chr.tmp_standards cascade;
