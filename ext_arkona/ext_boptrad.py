# encoding=utf-8
import csv
import db_cnx
import ops
import string


file_name = 'files/ext_boptrad.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),KEY,TRIM(VIN),TRIM(STOCK_),TRADE_ALLOWANCE,ACV,PAYOFF_AMOUNT,TRIM(PAYOFF_TO),
              TRIM(PAYOFF_ALIAS),TRIM(PAYOFF_ADDR_1),TRIM(PAYOFF_ADDR_2),TRIM(PAYOFF_CITY),TRIM(PAYOFF_STATE),
              PAYOFF_ZIP,PAYOFF_PHONE_NO,TRIM(PAYOFF_VENDOR),TRIM(PAYOFF_LOAN_),PAYOFF_EXP_DATE,TRIM(FROM_LEASE),
              ODOMETER,TRIM(INTL_POSTAL)
            FROM  rydedata.boptrad
            where company_number not in ('SYS')
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_boptrad")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_boptrad from stdin with csv encoding 'latin-1 '""", io)

