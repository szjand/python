# encoding=utf-8
import db_cnx

pg_con = None
file_name = 'files/zip_codes_states.csv'

try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate jon.lat_long_zip")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy jon.lat_long_zip from stdin with csv HEADER encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()
