# encoding=utf-8
import csv
import db_cnx
import ops
import string

# task = 'ext_bopmast'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_bopmast_partial.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(BOPMAST_COMPANY_NUMBER),RECORD_KEY,TRIM(RECORD_STATUS),TRIM(RECORD_TYPE),
                  TRIM(SALE_TYPE),TRIM(FRANCHISE_CODE),TRIM(VEHICLE_TYPE),TRIM(BOPMAST_STOCK_NUMBER),
                  TRIM(BOPMAST_VIN),ODOMETER_AT_SALE,BUYER_NUMBER,CO_BUYER_NUMBER,
                  TRIM(PRIMARY_SALESPERS),TRIM(SECONDARY_SLSPERS2),trim(LEFT(PD_POLICY_NUMBER, 3)),
                  ORIGINATION_DATE,DATE_APPROVED,DATE_CAPPED,DELIVERY_DATE,
                  GAP_PREMIUM,SERV_CONT_AMOUNT,AMO_TOTAL,coalesce(TRADE_ALLOWANCE, 0),
                  coalesce(TRADE_ACV, 0), coalesce(TRADE_ALLOWANCE, 0) - coalesce(TRADE_ACV, 0),
                  leasing_source, lending_source
                from rydedata.bopmast
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate sls.ext_bopmast_partial")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy sls.ext_bopmast_partial from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
