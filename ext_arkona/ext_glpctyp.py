# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_glpctyp'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_glpctyp.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
              SELECT TRIM(COMPANY_NUMBER),RECORD_KEY,TRIM(DESCRIPTION),TRIM(CUSTOMER_TYPE),TRIM(A_R_ACCOUNT),
                TRIM(DISCOUNT_ACCOUNT),DEF_CREDIT_LIMIT,TRIM(A_R_ACCOUNT_TYPE),TRIM(INV_TRACKING_METH),
                TRIM(PRINT_STATEMENTS),TRIM(PRT_STMT_W_0_BAL),TRIM(ASSES_FIN_CHARGE),MNTHLY_INT_RATE,
                MINIMUM_INTEREST,INT_ON_BAL_OVER,TRIM(CHG_INT_ON_INT),TRIM(INT_INCOME_ACCT),DATE_LAST_STATMNT,
                TRIM(DEPOSIT_BANK_ACCT),TRIM(A_R_STATEMENT_FORM)
              from rydedata.glpctyp
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_glpctyp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_glpctyp from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
