# encoding=utf-8
import db_cnx
import csv

pg_con = None
db2_con = None
file_name = 'files/ext_open_ro_headers.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                with 
                    headers as (
                        select ptpkey
                        from RYDEDATA.PDPPHDR a
                        where a.document_type = 'RO'
                            and a.document_number <> ''
                            and a.company_number in ('RY1','RY2')
                            and a.customer_key <> 0)
                select ro, ptpkey, service_type, payment_type, sum(flag_hours) as flag_hours, 
                  sum(labor_sales) as labor_sales, sum(labor_cost) as labor_cost, 
                  sum(labor_gross) as labor_gross, sum(parts_gross) as parts_gross, curdate()
                from ( -- and take the line out of the grouping
                    select trim(dd.document_number) as ro, aa.*, coalesce(bb.flag_hours,0) as flag_hours, 
                      coalesce(bb.labor_sales, 0) as labor_sales,
                      coalesce(bb.labor_cost, 0) as labor_cost, coalesce(bb.labor_gross, 0) as labor_gross,
                      coalesce(cc.parts_gross, 0) as parts_gross
                    from ( -- service type, payment type
                        select a.ptpkey, a.ptline, a.ptsvctyp as service_type, a.ptlpym as payment_type
                        from rydedata.pdppdet a
                        inner join headers b on a.ptpkey = b.ptpkey
                        where a.ptline < 900
                          and a.ptltyp = 'A'
                          and a.ptsvctyp <> ''
                          and a.ptlpym <> ''
                        group by a.ptpkey, a.ptline, a.ptsvctyp, a.ptlpym) aa
                    left join ( -- labor
                        select a.ptpkey, a.ptline, sum(coalesce(a.ptlhrs, 0)) as flag_hours, 
                          sum(coalesce(a.ptnet, 0)) as labor_sales, 
                          sum(coalesce(a.ptcost, 0)) as labor_cost, 
                          sum(coalesce(a.ptnet, 0) - coalesce(a.ptcost, 0)) as labor_gross
                        from rydedata.pdppdet a
                        inner join headers b on a.ptpkey = b.ptpkey
                        where a.ptline < 900
                          and a.ptcode = 'TT'
                        group by a.ptpkey, a.ptline) bb on aa.ptpkey = bb.ptpkey and aa.ptline = bb.ptline
                    left join (
                        select ptpkey, ptline, sum(parts_gross) as parts_gross
                        from ( -- parts
                            select a.ptpkey, a.ptline, a.ptqty * (a.ptnet - a.ptcost) as parts_gross
                            from rydedata.pdppdet a
                            inner join headers b on a.ptpkey = b.ptpkey
                            where a.ptline < 900
                              and a.ptltyp = 'P'
                              and a.ptcode in ('CP','SC','WS','IS')) c
                        group by ptpkey, ptline) cc on aa.ptpkey = cc.ptpkey and aa.ptline = cc.ptline
                    left join rydedata.pdpphdr dd on aa.ptpkey = dd.ptpkey) ee
                group by ro, ptpkey, service_type, payment_type, curdate()
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy jeri.open_ro_details from stdin with csv encoding 'latin-1 '""", io)
except Exception, error:
    print error
finally:
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
