# encoding=utf-8

import csv
import db_cnx
"""
technician time log
"""
file_name = 'files/ext_sdpsvcd.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
                TRIM(COMPANY_NUMBER),TRIM(RO_NUMBER),TRIM(TECHNICIAN_ID),LINE_NUMBER,TRANSACTION_DATE,
                START_TIME,END_TIME,HOURS,TRIM(VOIDED_ENTRY),TRIM(BILLING_CODE),SEQUENCE_NUMBER
            from rydedata.sdptime
            where transaction_date > '12/31/2021'
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_sdptime")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_sdptime from stdin with csv encoding 'latin-1 '""", io)
