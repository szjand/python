# encoding=utf-8

import csv
import db_cnx
"""

"""
file_name = 'files/ext_sdprdet_toyota_history.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select 
                TRIM(a.COMPANY_NUMBER),TRIM(a.RO_NUMBER),a.LINE_NUMBER,TRIM(a.LINE_TYPE),a.SEQUENCE_NUMBER,
                TRIM(a.TRANSACTION_CODE),a.TRANSACTION_DATE,TRIM(a.LINE_PAYMENT_METHOD),TRIM(a.SERVICE_TYPE),
                TRIM(a.POLICY_ADJUSTMENT),TRIM(a.TECHNICIAN_ID),TRIM(a.LABOR_OPERATION_CODE),
                TRIM(a.CORRECTION_CODE),a.LABOR_HOURS,a.LABOR_AMOUNT,a.COST,TRIM(a.ACTUAL_RETAIL_FLAG),
                TRIM(a.FAILURE_CODE),TRIM(a.SUBLET_VENDOR_NUMBER),TRIM(a.SUBLET_INVOICE_NUMBER),
                a.DISCOUNT_KEY,TRIM(a.DISCOUNT_BASIS),TRIM(a.VAT_CODE),a.VAT_AMOUNT,
                TRIM(a.DISPATCH_PRIORITY_1),TRIM(a.DISPATCH_PRIORITY_2),a.DISPATCH_RANK_1,
                a.DISPATCH_RANK_2,a.POLICY_TAX_AMOUNT,a.KIT_TAXABLE_AMOUNT
            from rydedata.sdprdet a
            where a.company_number = 'RY8'
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_sdprdet_toyota_history")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_sdprdet_toyota_history from stdin with csv 
                                  encoding 'latin-1 '""", io)

