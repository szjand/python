# encoding=utf-8
import db_cnx
import csv

pg_con = None
db2_con = None
file_name = 'files/ext_open_ro_headers.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select a.company_number, trim(a.document_number) as ro, a.ptpkey, trim(a.cust_name) as customer, 
                    a.service_writer_id,
                    case a.ro_status
                      when 'L' then 'G/L Error'
                      when 'S' then 'Cashier - Waiting for Special Order Part'
                      when '1' then 'Open'
                      when '2' then 'In Process'
                      when '3' then 'Approved by Parts'
                      when '4' then 'Cashier'
                      when '5' then 'Cashier, Delayed Close'
                      when '6' then 'Pre-Invoice'
                      when '7' then 'Odom Required'
                      when '8' then 'Waiting for Parts'
                      when '9' then 'Parts Approval Reqd'
                      else a.ro_status
                    end as ro_status,
                    cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2) ||'-'||
                        right(trim(open_tran_date),2) as date) as open_date,
                    days(curdate()) - days(cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2)
                                    ||'-'||right(trim(open_tran_date),2) as date)) as days_open,
                    0 as days_in_cashier, curdate()
                from RYDEDATA.PDPPHDR a
                where a.document_type = 'RO'
                    and a.document_number <> ''
                    and a.company_number in ('RY1','RY2')
                    and a.customer_key <> 0
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
                # print db2_cur.fetchall()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy jeri.open_ro_headers from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    # accomodate pypyodbc, if db2_con throws a closing closed connection error
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
