# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_pyptbdta'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pyptbdta.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),PAYROLL_CEN_YEAR,PAYROLL_RUN_NUMBER,SEQUENCE,
                  PAYROLL_START_DATE,PAYROLL_END_DATE,CHECK_DATE,TRIM(SELECTED_DEPTS),
                  TRIM(SELECTED_CLASS),TRIM(SELECTED_PAY_PER_),TRIM(DESCRIPTION),
                  TRIM(BATCH_STATUS),STATUS_DATE,STATUS_TIME,TRIM(OPERATION),TRIM(STEP),
                  TRIM(STEP_YBLSTEP),TRIM(PROGRAM),TRIM(USER),TRIM(WORK_STATION),TRIM(LDA),
                  CHECK_COUNT,GROSS_PAY,NET_PAY,TRIM(DD_TRANS_USER),TRIM(DD_TRANS_WORKSTATION),
                  DD_TRANS_DATE,DD_TRANS_TIME,TRIM(DD_TRANS_STATUS),TRIM(DD_TRANS_REPORT_FLAGS),
                  TRIM(SELECTED_RPT_CD1),TRIM(SELECTED_RPT_CD2),TRIM(PAY_PERIOD_OVERRIDE),
                  TRIM(FLAG_1),TRIM(FLAG_2),TRIM(FLAG_3),TRIM(FLAG_4),TRIM(FLAG_5)
                from rydedata.pyptbdta a
            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_pyptbdta")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_pyptbdta from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print(error)
finally:
    # syntax required for pypyodbc
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
