# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_pyhscdta'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pyhscdta.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),PAYROLL_CEN_YEAR,PAYROLL_RUN_NUMBER,TRIM(EMPLOYEE_NUMBER),
                  TRIM(SEQ_VOID),TRIM(CODE_TYPE),CODE_SEQ_,TRIM(CODE_ID),TRIM(CODE_SW),TRIM(FIXED_DEDUCTION),
                  TRIM(FIXED_DEDUCTION_YHCCRT),TRIM(BENIFIT_ADJUST_SW),AMOUNT,ENTERED_AMOUNT,PAY_RATE,
                  TRIM(DESCRIPTION),TRIM(PERCNT_CODE),TRIM(LIMIT_REACHED),TRIM(DIST_OVERRIDE),
                  TRIM(DIST_ACCT),REFERENCE_CHECK_
                FROM  rydedata.pyhscdta a
                where payroll_cen_year > 119
            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_pyhscdta")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_pyhscdta from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
except Exception as error:
    print(error)
finally:
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
