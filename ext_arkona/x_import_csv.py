import db_cnx
"""
import any csv file into a pg table
"""

path = "/mnt/hgfs/E/python projects/misc_sql/dealer_fx/docs/campaign_lists_12-18-19/"
import_file_name = "service_smarts.csv"
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        with open(path + import_file_name, 'r') as io:
            # the header parameter excludes the first row from the import
            pg_cur.copy_expert("""copy dfx.ext_service_smarts from stdin with csv header encoding 'latin-1 '""", io)
