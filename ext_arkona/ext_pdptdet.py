# encoding=utf-8
import pyodbc
import csv
import psycopg2
import pypyodbc

try:
    # with psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'") as pgCon:
    # pgCon = psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")
    pgCon = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
    pgCursor = pgCon.cursor()
    date_cursor = pgCon.cursor()
    # with pyodbc.connect('DSN=iSeries System DSN; UID=rydejon; PWD=fuckyou5') as db2Con:
    db2Con = pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver};system=REPORT1.ARKONA.COM;uid=rydejon;pwd=fuckyou5')
    db2Cursor = db2Con.cursor()
    file_name = 'files/ext_pdptdet.csv'
    # delete all pdptdet data for the
    sql = """
      delete
      from arkona.ext_pdptdet
      where ptdate > 20190000
    """
    pgCursor.execute(sql)
    pgCon.commit()
    date_sql = """
        select the_date 
        from (
          select *, the_year * 10000 + the_month * 100 + the_day as the_date
          from (
          select * from generate_series(2010, 2029, 1) as the_year) a
          cross join (
          select * from generate_series(1, 12, 1) as the_month) b
          cross join (
          select * from generate_series(1, 31, 1) as the_day) c) d
        where the_date between 20190101 and 20190605
        order by the_year,the_month,the_day;  

    """
    date_cursor.execute(date_sql)
    for dt_record in date_cursor:
        print(str(dt_record[0]))
        db2_sql = """
        select TRIM(PTCO#),TRIM(PTINV#),PTLINE,PTSEQ#,TRIM(PTTGRP),
          TRIM(PTCODE),TRIM(PTSOEP),PTDATE,PTCDATE,TRIM(PTCPID),
          TRIM(PTMANF),TRIM(PTPART),TRIM(PTSGRP),PTQTY,PTCOST,
          PTLIST,PTNET,PTEPCDIFF,TRIM(PTSPCD),TRIM(PTORSO),
          TRIM(PTPOVR),TRIM(PTGPRC),TRIM(PTXCLD),TRIM(PTFPRT),TRIM(PTRTRN),
          PTOHAT,TRIM(PTVATCODE),PTVATAMT,PTTIME,PTQOHCHG,
          PTLASTCHG,TRIM(PTUPDUSER),PTIDENTITY,PTKTXAMT,TRIM(PTLNTAXE),
          TRIM(PTDUPDATE),TRIM(PTRIM)
        FROM  rydedata.pdptdet a
        where ptdate =   """ + str(dt_record[0])
        db2Cursor.execute(db2_sql)

        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2Cursor)
        f.close()
        with open(file_name, 'r') as io:
            pgCursor.copy_expert("""copy arkona.ext_pdptdet from stdin with csv encoding 'latin-1'""", io)
        pgCon.commit()
except Exception as error:
    print (str(error))
