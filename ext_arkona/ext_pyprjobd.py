# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_pyprjobd'
# pg_con = None
# db2_con = None
run_id = None
file_name = 'files/ext_pyprjobd.csv'


with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),TRIM(JOB_DESCRIPTION),TRIM(JOB_LEVEL),SEQ_,TRIM(PERFORMANCE_RATING),TRIM(DATA)
            from rydedata.pyprjobd
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_pyprjobd")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_pyprjobd from stdin with csv encoding 'latin-1 '""", io)

