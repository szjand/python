# encoding=utf-8
import csv
import db_cnx
import ops
import string

pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_bopname.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
              SELECT TRIM(BOPNAME_COMPANY_NUMBER),BOPNAME_RECORD_KEY,TRIM(COMPANY_INDIVID),SOC_SEC_FED_ID_,
                TRIM(BOPNAME_SEARCH_NAME),TRIM(LAST_COMPANY_NAME),TRIM(FIRST_NAME),TRIM(MIDDLE_INIT),
                TRIM(SALUTATION),TRIM(GENDER),TRIM(LANGUAGE),TRIM(ADDRESS_1),TRIM(ADDRESS_2),TRIM(CITY),
                TRIM(COUNTY),TRIM(STATE_CODE),ZIP_CODE,PHONE_NUMBER,BUSINESS_PHONE,BUSINESS_EXT,
                FAX_NUMBER,BIRTH_DATE,TRIM(DRIVERS_LICENSE),TRIM(CONTACT),TRIM(PREFERRED_CONTACT),
                TRIM(MAIL_CODE),TRIM(TAX_EXMPT_NO_),TRIM(ASSIGNED_SLSP),TRIM(CUSTOMER_TYPE),
                TRIM(PREFERRED_PHONE),CELL_PHONE,PAGE_PHONE,OTHER_PHONE,TRIM(OTHER_PHONE_DESC),
                TRIM(EMAIL_ADDRESS),TRIM(OPTIONAL_FIELD),TRIM(ALLOW_CONTACT_BY_POSTAL),
                TRIM(ALLOW_CONTACT_BY_PHONE),TRIM(ALLOW_CONTACT_BY_EMAIL),TRIM(ADDRESS_LINE_3),
                TRIM(BUSINESS_PHONE_EXTENSION),TRIM(INTERNATIONAL_BUSINESS_PHONE),LAST_CHANGE_DATE,
                TRIM(INTERNATIONAL_CELL_PHONE),EXTERNAL_CROSS_REFERENCE_KEY,TRIM(SECOND_EMAIL_ADDRESS2),
                TRIM(INTERNATIONAL_FAX_NUMBER),TRIM(INTERNATIONAL_OTHER_PHONE),
                TRIM(INTERNATIONAL_HOME_PHONE),TRIM(CUSTOMER_PREFERRED_NAME),
                TRIM(INTERNATIONAL_PAGER_PHONE),TRIM(PREFERRED_LANGUAGE),TRIM(INTERNATIONAL_ZIP_CODE),
                TRIM(TOKEN_ID),TRIM(COMMON_CUSTOMER_ID),TRIM(SOUNDEX_FIRST_NAME),TRIM(SOUNDEX_LAST_NAME),
                TRIM(SOUNDEX_ADDRESS1),TRIM(OPTIONAL_FIELD_2),TRIM(TAX_EXMPT_NO_2),TRIM(TAX_EXMPT_NO_3),
                TRIM(TAX_EXMPT_NO_4),TRIM(GST_REGISTRANT_),PST_EXMPT_DATE,DEALER_CODE,CC_ID,
                CUSTOMER_VERSION,ADDRESS_NUMBER,ADDRESS_VERSION,CREATED_BY_USER,TIMESTAMP_CREATED,
                UPDATED_BY_USER,TIMESTAMP_UPDATED,TRIM(COUTRY_CODE),TRIM(DL_STATE),
                TRIM(OPENTRACK_PARTNER_ID),TRIM(STATUS),TRIM(SHARE_INFO)
              from rydedata.bopname
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_bopname")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_bopname from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
