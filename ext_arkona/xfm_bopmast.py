# encoding=utf-8
"""
script to extract bopmast history from ubuntu_cdc (10.130.196.73)
"""
import csv
import db_cnx
from datetime import date, timedelta

pg_server = '173'
cdc_server = '73'

d1 = date(2019, 3, 8)  # start date
d2 = date(2019, 3, 12)  # end date
delta = d2 - d1         # timedelta
for i in range(delta.days + 1):
    the_date = d1 + timedelta(i)
    table_name = 'test.ext_bopmast_' + the_date.strftime('%m') + the_date.strftime('%d')
    print(the_date)
    print(table_name)
    with db_cnx.new_pg(cdc_server) as cdc_con:
        with cdc_con.cursor() as cdc_cur:
            sql = """
                truncate arkona.ext_bopmast;
            """
            cdc_cur.execute(sql)
            sql = """
                insert into arkona.ext_bopmast
                select *
                from {}
            """.format(table_name)
            cdc_cur.execute(sql)
            sql = """
                select arkona.xfm_bopmast_script('{}')
            """.format(the_date)
            cdc_cur.execute(sql)