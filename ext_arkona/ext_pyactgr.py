# encoding=utf-8
import csv
import db_cnx
import ops
import string

***********************************
moved to luigi/ext_arkona.py
***********************************

task = 'ext_pyactgr'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pyactgr.csv'

try:
    # with db_cnx.arkona_report() as db2_con:
    #     with db2_con.cursor() as db2_cur:
    #         sql = """
    #             select
    #               TRIM(COMPANY_NUMBER),TRIM(DIST_CODE),SEQ_NUMBER,TRIM(DESCRIPTION),TRIM(DIST_CO_),GROSS_DIST,
    #               TRIM(GROSS_EXPENSE_ACT_),TRIM(OVERTIME_ACT_),TRIM(VACATION_EXPENSE_ACT_),
    #               TRIM(HOLIDAY_EXPENSE_ACT_),TRIM(SICK_LEAVE_EXPENSE_ACT_),TRIM(RETIRE_EXPENSE_ACT_),
    #               TRIM(EMPLR_FICA_EXPENSE),TRIM(EMPLR_MED_EXPENSE),TRIM(FEDERAL_UN_EMP_ACT_),TRIM(STATE_UNEMP_ACT_),
    #               TRIM(STATE_COMP_ACT_),TRIM(STATE_SDI_ACT_),TRIM(EMPLR_CONTRIBUTIONS)
    #             from rydedata.pyactgr
    #         """
    #         db2_cur.execute(sql)
    #         with open(file_name, 'wb') as f:
    #             csv.writer(f).writerows(db2_cur)
    # with db_cnx.pg() as pg_con:
    #     with pg_con.cursor() as pg_cur:
    #         pg_cur.execute("truncate arkona.ext_pyactgr")
    #         with open(file_name, 'r') as io:
    #             pg_cur.copy_expert("""copy arkona.ext_pyactgr from stdin with csv encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
