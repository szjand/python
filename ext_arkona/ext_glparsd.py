# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_glparsd'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_glparsd.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRANS_NUMBER,TRIM(CONTROL_NUMBER),TRANS_NUMBER_GTTRN#,
                  TRAN_SEQ_NUMBER,TRANS_DATE,TRIM(DOCUMENT_NUMBER),TRIM(DOC_TYPE),TRIM(JOURNAL),
                  TRIM(REFERENCE_NUMBER),TRIM(DESCRIPTION),TRANSACTION_AMT,TRIM(ORIGINATING_CO_)
                from rydedata.glparsd
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_glparsd")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_glparsd from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
