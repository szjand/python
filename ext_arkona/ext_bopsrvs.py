# encoding=utf-8

import csv
import db_cnx
"""
service contracts
"""
file_name = 'files/ext_bopsrvs.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),DEAL_KEY,BO_RECORD_KEY,SD_RECORD_KEY,TRIM(VIN),CUSTOMER_KEY,
              SERVICE_CONTRACT_SEQUENCE_NU,TRIM(CONTRACT_NAME),CONTRACT_START_DATE,CONTRACT_EXPIRATION_DATE,
              CONTRACT_COST,CONTRACT_DEDUCTIBLE,CONTRACT_EXPIRATION_MILES,CONTRACT_EXPIRATION_MONTHS,
              TRIM(CONTRACT_CAPITALIZED),CONTRACT_AMOUNT,TRIM(AGREEMENT_TYPE),TRIM(CONTRACT_),TRIM(PLAN_CODE),
              TRIM(PRODUCT_CODE),STARTING_ODOMETER,CO_BUYER_KEY,SALES_TAX,SALES_TAX_RATE,TRIM(SALESPERSON),
              TRIM(POSTED_TO_G_L),TRIM(RECEIPT_NUMBER),TRIM(OPT_FIELD_1),TRIM(OPT_FIELD_2),TRIM(OPT_FIELD_3),
              TRIM(OPT_FIELD_4),TRIM(OPT_FIELD_5),TRIM(OPT_FIELD),ENDING_MILES,TRIM(DEALER_)
            from rydedata.bopsrvs
            where company_number in ('RY1','RY2','RY8')
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_bopsrvs")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_bopsrvs from stdin with csv encoding 'latin-1 '""", io)

