# encoding=utf-8
import datetime
import db_cnx
import ops
import string
task = 'unposted_lines'
db2_con = None
run_id = None
yesterday = datetime.date.today() - datetime.timedelta(days=2)
fc_date = 10000 * int(yesterday.strftime("%Y")) + 100 * int(yesterday.strftime("%m")) + int(yesterday.strftime("%d"))
print fc_date
try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select count(*)
                from (
                  select a.gtdoc#
                  from rydedata.glptrns a
                  inner join rydedata.sdprhdr b on trim(a.gtdoc#) = trim(b.ro_number)
                    and final_close_date > {}
                  where year(a.gtdate) > 2016
                    and a.gtpost not in ('Y', 'V')
                    and left(trim(gtdoc#), 1) = '1'
                  group by a.gtdoc#) x
            """ .format(fc_date)
            db2_cur.execute(sql)
            the_count = db2_cur.fetchone()[0]
            if the_count != 0:
                raise Exception(str(the_count) + ' ros with unposted lines')
    ops.log_pass(run_id)
    ops.email_error(task, run_id, str(fc_date) + ' Pass')
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    ops.email_error(task, run_id, error)
    print error
finally:
    if db2_con.connected:
        db2_con.close()
