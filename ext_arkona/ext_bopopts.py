# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_bopopts'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_bopopts.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  TRIM(COMPANY_NUMBER),CONTRACT_KEY,TRIM(RECORD_TYPE),SEQUENCE_NO,TRIM(FIELD_DESCRIP),
                  AMO_FEE_AMOUNT,COST,TRIM(ADD_TO_CAP_COST),TRIM(ADD_TO_PRICE),ADD_TO_COST_SEQ_,
                  TRIM(TABLE_FEE_OVERRDE),SALES_TAX_AMOUNT,RESIDUAL_AMOUNT,TRIM(RESIDUAL_PERCENT),
                  TRIM(ASSIGN_GROSS)
                from rydedata.bopopts
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_bopopts")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_bopopts from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
