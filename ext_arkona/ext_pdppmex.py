# encoding=utf-8

import csv
import db_cnx

file_name = 'files/ext_pdppmex.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),TRIM(MANUFACTURER),TRIM(PART_NUMBER),TRIM(STOCK_CODE),STOCK_LEVEL,
              MFR_REORDER_LEVEL,MFR_REORDER_MAX,TRIM(MFR_AUTO_REPLEN),TRIM(MFR_ASS_CLASS),
              TRIM(MFR_ASS_CLASS_PMASSSAP),MFR_FORECAST,TRIM(EXCLUDE_REASON)
            from rydedata.pdppmex
            where company_number in ('RY1','RY2')
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_pdppmex")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_pdppmex from stdin with csv encoding 'latin-1 '""", io)

