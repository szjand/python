# encoding=utf-8
import csv
import db_cnx
import ops
import string

# task = 'ext_sdpxtim'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_bopssrc.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  TRIM(COMPANY_NUMBER),KEY,TRIM(LESSOR_NAME),TRIM(ADDRESS),TRIM(CITY),TRIM(STATE_CODE),ZIP_CODE,
                  PHONE_NUMBER,TRIM(CONTACT),TRIM(PAYMENT_FORMULA),APR_BUY_RATE,FACTOR_BUY_RATE,RESERVE_HOLD_BACK,
                  INITIAL_TERM,INITIAL_FACTOR,INITIAL_APR,INITIAL_RESID_,MILEAGE_YR_ALLOW,EXCS_MI_RATE_INC,
                  EXCS_MI_RATE_PEN2,ACQUISTION_FEE,TRIM(ACQ_FEE_TAXABLE),MAX_ADVANCE_,AMO_COST_LIST_,
                  TRIM(AMO_COST_LIST),AMO_LIMIT,TRIM(ADD_FET_TO_MAX_AV),TRIM(FET_OPTIONS),
                  TRIM(CAPITALIZE_SLS_TX),TRIM(CAPITALIZE_CR_TAX),TRIM(FIN_RESERVE_METH),
                  SEC_DEP_MULTIPLE,SECURITY_DEPOSIT,MIN_SEC_DEPOSIT,MAX_SEC_DEPOSIT,TRIM(TRADE_SALESTAX_CR),
                  TRIM(REDUCE_TAX_BASIS),RED_TAX_BASIS_AMT,TERM_1,TERM_2,TERM_3,TERM_4,TERM_5,TERM_6,TERM_7,
                  TERM_8,TRIM(A_R_CUST_NUMBER),TRIM(A_R_ACCOUNT),TRIM(ACQ_FEE_ACCOUNT),TRIM(SALES_TAX_ON_PAYM),
                  MAX_VALUE_1,MAX_VALUE_2,MAX_VALUE_3,MAX_VALUE_4,MAX_VALUE_5,MAX_VALUE_6,MAX_VALUE_7,
                  MAX_VALUE_8,TRIM(INSURANCE_FORMULA),TRIM(PROP_TAX_ON_PYMNT),TRIM(PROP_TAX_BASE),
                  DISCOUNT_FACTOR,TRIM(LIEN_HOLDER_NAME),TRIM(LIEN_HOLD_ADDR_1),TRIM(LIEN_HOLD_ADDR_2),
                  TRIM(LIEN_HOLD_CITY),TRIM(LIEN_HOLD_STATE),LIEN_HOLD_ZIP,LIEN_HOLD_PHONE_,
                  TRIM(OPTIONAL_FIELD),TRIM(INTL_POSTAL),TRIM(LIEN_INTL_POSTAL),PAYBACK_RATE,
                  TRIM(DEFAULT_SALES_TYPE),TRIM(MISC_INFORMATION_1),TRIM(MISC_INFORMATION_2),
                  DEFAULT_THRESHOLD_MILES,TRIM(PRO_RATA),DISPOSITION_FEE,PURCHASE_OPTION_FEE,
                  TRIM(CALCULATE_VW_EFFECTIVE_APR)
                from rydedata.bopssrc
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_bopssrc")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_bopssrc from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    # print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
 except Exception as error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print(error)
    # accomodate pypyodbc, if db2_con throws a closing closed connection error
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
