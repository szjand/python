# encoding=utf-8
import csv
import db_cnx
import ops
import string


# pg_con = None
# db2_con = None
run_id = None
file_name = 'files/ext_sdprhdr_uc_recon.csv'

with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        vin_array = []
        sql = """
            select vin 
            from cu.used_3
            where delivery_date between '06/01/2022' and  current_date
              and days_available > 0      
        """
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            vin_array.append(str(row[0]))
        for the_vin in vin_array:
            with db_cnx.arkona_report() as db2_con:
                with db2_con.cursor() as db2_cur:
                    sql = """
                      SELECT TRIM(CUST_NAME),TRIM(VIN),TRIM(RO_NUMBER)
                      from rydedata.sdprhdr
                      where vin = '{}'
                        and cust_name like 'INVENTORY%'
                    """.format(the_vin)
                    db2_cur.execute(sql)
                    with open(file_name, 'a') as f:
                        csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con_2:
    with pg_con_2.cursor() as pg_cur_2:
        pg_cur_2.execute("truncate cu.recon_ro_numbers")
        with open(file_name, 'r') as io:
            pg_cur_2.copy_expert("""copy cu.recon_ro_numbers from stdin with csv encoding 'latin-1 '""", io)
