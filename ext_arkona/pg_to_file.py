# encoding=utf-8
import db_cnx
import csv
import json

pg_con = None
file_name = 'wtf.json'

try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                with 
                  in_stock_data as (
                    select cab,split_part(engine,'L',1) as engine , vehicle_trim,color,
                      coalesce(sum(count_per_trim) filter (where category = 'In Transit'),0) + coalesce(sum(count_per_trim) filter (where category = 'In Stock'),0) as in_stock
                    from ( -- FF
                      select c.category,model_code, make,e.description as engine, vehicle_trim,model_year,d.description as color, count(*) as count_per_trim
                      from gmgl.vehicle_orders  a
                      inner join gmgl.vehicle_order_events b on a.order_number = b.order_number and thru_date = '12/31/9999'
                      inner join gmgl.vehicle_event_codes c on b.event_code = c.code
                      inner join gmgl.vehicle_option_codes d on color = d.option_code and d.category = 'Color'
                      inner join ( -- E
                        select order_number, gg.*
                        from gmgl.vehicle_order_options hh
                        inner join gmgl.vehicle_option_codes gg on hh.option_code = gg.option_code and category = 'engine') e on a.order_number = e.order_number
                        where alloc_group in ('CCRULD', 'CDBLLD','CREGLD')
                          and order_type in ('TRE', 'SRE')
                          and c.category in ('In Stock', 'In Transit')
                      group by c.category,model_code,make, vehicle_trim ,d.description,model_year,e.description) FF
                    inner join gmgl.models c on ff.model_code = c.model_code
                    group by cab, vehicle_trim,model_year, color, split_part(engine,'L',1)), 
                  sold_data as (
                    select cab,split_part(engine,'L',1)as engine, the_trim, c.color,  
                --       count(*) filter (where delivery_date between '05/12/2018'::date - interval '90 days' and '05/12/2018'::date) as sold90,
                --       count(*) filter (where delivery_date between '05/12/2018'::date - interval '365 days' and '05/12/2018'::date) as sold365
                      count(*) filter (where delivery_date between current_date - 90 and current_date::date) as sold90,
                      count(*) filter (where delivery_date between current_date - 365 and current_date::date) as sold365      
                    from gmgl.sold_vehicles a
                    inner join ( -- B
                      select vin, description as engine
                      from gmgl.sold_vehicle_options a
                      inner join gmgl.vehicle_option_codes b on a.option_code = b.option_code and category = 'engine') b on a.vin = b.vin
                    inner join ( -- C
                      select vin, b.description as color
                      from gmgl.sold_vehicle_options a
                      inner join gmgl.vehicle_option_codes b on a.option_code = b.option_code and category = 'color') c on a.vin = c.vin
                    inner join ( -- D
                      select vin, a.option_code as the_trim
                      from gmgl.sold_vehicle_options a
                      inner join gmgl.vehicle_option_codes b on a.option_code = b.option_code and category = 'trim') d on a.vin = d.vin
                    inner join nc.silverado_sales g on a.vin = g.vin and model_year = '2018' -- and delivery_date between current_date - interval '90 days' and current_date
                    inner join gmgl.models wq on a.model = wq.model_code
                    group by  cab, split_part(engine,'L',1), the_trim, c.color,model_year ),
                  daily as (
                    with 
                      sku_days as (
                        select a.the_date, b.sku_id
                        from dds.dim_date a
                        left join gmgl.sku b on 1 = 1
                        where a.the_date between current_Date - 180 and current_date - 1)
                    select c.the_date, c.sku_id, c.inventory, g.sold
                    from (    
                      select a.the_date, a.sku_id, count(b.sku_id) as inventory
                      from sku_days a
                      left join ( -- inventory
                          select bb.sku_id, ground, delivery_date
                          from jon.test_4 aa
                          inner join gmgl.sku bb on aa.engine = bb.engine
                            and aa.color = bb.color
                            and
                              case
                                when bb.cab = 'regular' then aa.cab = 'reg'
                                else aa.cab = bb.cab
                              end
                            and 
                              case
                                when bb.vehicle_trim = '1WT' then  aa.trim_level = 'WT'
                                when bb.vehicle_trim = 'GAJ' then aa.trim_level = 'high country'
                                when bb.vehicle_Trim = '1LS' then aa.trim_level = 'LS'
                                else bb.vehicle_trim = aa.trim_level
                              end
                          where model_year = '2018') b on a.sku_id = b.sku_id and a.the_date between b.ground and coalesce(b.delivery_date, current_date) - 1
                      group by a.the_date, a.sku_id) c
                    left join (
                      select d.the_date, d.sku_id, count(e.sku_id) as sold
                      from sku_days d
                      left join ( -- sales
                        select bb.sku_id, ground, delivery_date
                        from jon.test_4 aa
                        inner join gmgl.sku bb on aa.engine = bb.engine
                          and aa.color = bb.color
                          and
                            case
                              when bb.cab = 'regular' then aa.cab = 'reg'
                              else aa.cab = bb.cab
                            end
                          and 
                            case
                              when bb.vehicle_trim = '1WT' then  aa.trim_level = 'WT'
                              when bb.vehicle_trim = 'GAJ' then aa.trim_level = 'high country'
                              when bb.vehicle_Trim = '1LS' then aa.trim_level = 'LS'
                              else bb.vehicle_trim = aa.trim_level
                            end
                        where model_year = '2018') e on d.sku_id = e.sku_id and d.the_date = e.delivery_date
                      group by d.the_date, d.sku_id) g on c.the_date = g.the_date and c.sku_id = g.sku_id)  
                select row_to_json(B)
                from (
                  select json_agg(row_to_json(A) order by sold90 desc) as skus
                  from (
                    select a.sku_id as id, make, a.cab, a.vehicle_trim  as trim, a.engine, a.color, group_id, coalesce(in_stock,0) as in_stock, 
                      coalesce(sold90,0) as sold90, coalesce(sold365,0) as sold365,
                      (
                        select coalesce(array_to_json(array_agg(row_to_json(AA))), '[]')
                        from (
                          select the_date, inventory, sold
                          from daily
                          where sku_id = a.sku_id) aa) as stats
                    from gmgl.sku  a
                    left join (
                      select *
                      from in_stock_data ) b  on a.cab = b.cab and b.engine = a.engine and a.vehicle_trim = b.vehicle_trim and a.color = b.color
                    left join (
                      select *
                       from sold_data ) c on a.cab = c.cab 
                        and c.engine = a.engine 
                        and a.vehicle_trim = c.the_trim 
                        and a.color = c.color) A) B            
            """
            pg_cur.execute(sql)
            json.dump(pg_cur)

except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()

