# encoding=utf-8
import csv
import db_cnx


task = 'ext_sepuser'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_sepuser.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  TRIM(COMPANY_NUMBER),TRIM(USER),TRIM(USER_NAME),TRIM(USER_INITIALS),TRIM(DEFAULT_CO_),
                  TRIM(DEFAULT_APPL),TRIM(GL_FISCAL_ANNUAL),TRIM(RELEASE_NUMBER),TRIM(PTF_NUMBER),
                  TRIM(NOTIFY_RELEASE_),TRIM(NOTIFY_PTF_NUMBER),TRIM(COL_1_GL_INQ_CODE1CD),
                  TRIM(COL_2_GL_INQ_CODE2CD),TRIM(COL_3_GL_INQ_CODE3CD),TRIM(COL_4_GL_INQ_CODE4CD),
                  COL_1_GL_INQ_YEAR1YR,COL_2_GL_INQ_YEAR2YR,COL_3_GL_INQ_YEAR3YR,COL_4_GL_INQ_YEAR4YR,
                  TRIM(SECURITY_CLASS),TRIM(CREDIT_BUREAU_PASSWORD),TRIM(PREFERRED_LANGUAGE),TRIM(EMAIL_ADDRESS),
                  CEL_PHONE,DESK_PHONE,TRIM(TITLE),TRIM(DEPARTMENT),DATE_CREATED,TRIM(FIRST_NAME),TRIM(LAST_NAME),
                  TRIM(LONG_EMAIL_ADDRESS2),TRIM(WIRELESS_PROVIDER),TRIM(EMAIL_YES_NO),TRIM(TEXT_YES_NO)
                from rydedata.sepuser
            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_sepuser")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_sepuser from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print(error)
finally:
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
