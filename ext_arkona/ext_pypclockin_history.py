# encoding=utf-8

import csv
import db_cnx

task = 'ext_pypclockin'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pypclockin.csv'

try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as date_cur:
            date_sql = """
                select 
                  (select min(the_date)
                   from dds.dim_date
                   where the_year = a.the_year) as from_date,
                  (select max(the_date)
                   from dds.dim_date
                   where the_year = a.the_year) as from_date
                from  (     
                  select distinct the_year      
                  from dds.dim_date 
                  where the_year 
                    between 
                      (select the_year from dds.dim_date where the_date = current_Date) - 11
                    and 
                      (select the_year from dds.dim_date where the_date = current_Date)) a;
            """
            date_cur.execute(date_sql)
            for row in date_cur:
                print(str(row[0]) + '-' + str(row[1]))
                with db_cnx.arkona_report() as db2_con:
                    with db2_con.cursor() as db2_cur:
                        sql = """
                            select
                              TRIM(YICO#),TRIM(PYMAST_EMPLOYEE_NUMBER),YICLKIND,
                              case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,
                              YICLKOUTD,
                              case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end as YICLKOUTT,
                              TRIM(YISTAT),
                              TRIM(YIINFO),TRIM(YICODE),TRIM(YIUSRIDI),TRIM(YIWSIDI),TRIM(YIUSRIDO),TRIM(YIWSIDO)
                            from rydedata.pypclockin
                            where yico# in ('RY1','RY2')
                              and yiclkind between '{0}' and '{1}'
                        """.format(row[0], row[1])
                        db2_cur.execute(sql)
                        with open(file_name, 'w') as f:
                            csv.writer(f).writerows(db2_cur)
                with pg_con.cursor() as pg_cur:
                    with open(file_name, 'r') as io:
                        pg_cur.copy_expert("""copy jon.ext_pypclockin from stdin with csv encoding 'latin-1 '""", io)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    print(error)

