# encoding=utf-8
import csv
import db_cnx
import ops
import string

# task = 'ext_glpdba'
# pg_con = None
# db2_con = None
# run_id = None
file_name = 'files/ext_glpdba.csv'

# try:
with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),RECORD_KEY,TRIM(DBA_NAME)
            from rydedata.glpdba
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_glpdba")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_glpdba from stdin with csv encoding 'latin-1 '""", io)
#     # ops.log_pass(run_id)
#     print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
# except Exception, error:
#     # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
#     print error
# finally:
#     if db2_con.connected:
#         db2_con.close()
#     if pg_con:
#         pg_con.close()
