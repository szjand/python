# encoding=utf-8

import csv
import db_cnx
"""
service contracts
"""
file_name = 'files/ext_bopsrvc.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),RECORD_KEY,TRIM(COMPANY_NAME),TRIM(ADDRESS),TRIM(ADDRESS2),TRIM(ADDRESS3),
              TRIM(CITY),TRIM(COUNTY),TRIM(STATE_CODE),ZIP_CODE,TRIM(INT_ZIP_CODE),PHONE_NUMBER,TRIM(INT_PHONE_NUMBER),
              TRIM(CONTACT),DEDUCTABLE,RETAIL_PRICE,COST,MILES_ALLOWED,MONTHS_ALLOWED,TRIM(TAX_CODE),
              TRIM(ADD_TO_CAP_COST),TRIM(VENDOR_NUMBER),TRIM(PAYABLE_ACCOUNT),SERVICE_XREFF_KEY,TRIM(INCLUDE_NEW),
              TRIM(INCLUDE_USED),TRIM(SERVICE_CONT_TYPE),TRIM(ACTIVE_),TRIM(DEALER_NUMBER)
            from rydedata.bopsrvc
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_bopsrvc")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_bopsrvc from stdin with csv encoding 'latin-1 '""", io)

