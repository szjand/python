# encoding=utf-8
from datetime import timedelta
import datetime
start_date = datetime.datetime.now() - timedelta(days=60)
start_date = 100 * (100 * start_date.year + start_date.month) + start_date.day
end_date = datetime.datetime.now()
end_date = 100 * (100 * end_date.year + end_date.month) + end_date.day
print(start_date, end_date)


# ----------------------------------------------------------------------------------------------------------------------
# import csv
# import db_cnx

# file_name = 'files/ext_pdptdet.csv'
#
# with db_cnx.arkona_report() as db2_con:
#     with db2_con.cursor() as db2_cur:
#         sql = """
#             with
#               years(the_year) as (select year(curdate() - 60 days) as y from sysibm.sysdummy1),
#               months(the_month) as (select month(curdate() - 60 days) as y from sysibm.sysdummy1),
#               days(the_day) as (select day(curdate() - 60 days)from sysibm.sysdummy1)
#             select 100 * ( 100 * the_year + the_month) + the_day
#             from years, months, days
#         """
#         db2_cur.execute(sql)
#         start_date = db2_cur.fetchone()[0]
#         sql = """
#             with
#               years(the_year) as (select year(curdate()) as y from sysibm.sysdummy1),
#               months(the_month) as (select month(curdate()) as y from sysibm.sysdummy1),
#               days(the_day) as (select day(curdate())from sysibm.sysdummy1)
#             select 100 * ( 100 * the_year + the_month) + the_day
#             from years, months, days
#         """
#         db2_cur.execute(sql)
#         end_date = db2_cur.fetchone()[0]
#         sql = """
#             delete
#             from arkona.ext_pdptdet
#             where ptdate between {0} and {1}
#         """.format(start_date, end_date)
#         db2_cur.execute(sql)
#         sql = """
#             select TRIM(PTCO#),TRIM(PTINV#),PTLINE,PTSEQ#,TRIM(PTTGRP),
#               TRIM(PTCODE),TRIM(PTSOEP),PTDATE,PTCDATE,TRIM(PTCPID),
#               TRIM(PTMANF),TRIM(PTPART),TRIM(PTSGRP),PTQTY,PTCOST,
#               PTLIST,PTNET,PTEPCDIFF,TRIM(PTSPCD),TRIM(PTORSO),
#               TRIM(PTPOVR),TRIM(PTGPRC),TRIM(PTXCLD),TRIM(PTFPRT),TRIM(PTRTRN),
#               PTOHAT,TRIM(PTVATCODE),PTVATAMT,PTTIME,PTQOHCHG,
#               PTLASTCHG,TRIM(PTUPDUSER),PTIDENTITY,PTKTXAMT,TRIM(PTLNTAXE),
#               TRIM(PTDUPDATE),TRIM(PTRIM)
#             FROM  rydedata.pdptdet a
#             where ptdate between {0} and {1}
#         """.format(start_date, end_date)
#         print(sql)
# ----------------------------------------------------------------------------------------------------------------------