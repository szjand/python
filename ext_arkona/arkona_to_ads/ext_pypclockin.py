# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_pypclockin'
ads_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pypclockin.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            ads_cur.execute("""delete from ext_pypclockin""")
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select yico#, trim(yiemp#) as yiemp#, yiclkind,
                case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,
                  yiclkoutd,
                  max(case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end) as yiclkoutt,
                  trim(yicode) as yicode
                from rydedata.pypclockin
                where yiclkind >= (SELECT CURRENT DATE - 40 DAY FROM sysibm.sysdummy1)
                group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode
            """
            db2_cur.execute(sql)
            for row in db2_cur.fetchall():
                # sql = """insert into ext_pypclockin values({0},{1},{2},{3},{4},{5},{6})""".format(
                #                 row[0],row[1],row[2],row[3],row[4],row[5],row[6])
                # print sql
                # print row[0]
                # print row[1]
                # print row[2]
                # print row[3]
                # print row[4]
                # print row[5]
                # print row[6]
                with db_cnx.ads_dds() as ads_con:
                    with ads_con.cursor() as ads_cur:
                        ads_cur.execute("""
                            insert into ext_pypclockin values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')""".format(
                                row[0],row[1],row[2],row[3],row[4],row[5],row[6]))
                        # ads_cur.execute("""
                        #     insert into ext_pypclockin values({0},{1},{2},{3},{4},{5},{6})""".format(
                        #         row[0],row[1],row[2],row[3],row[4],row[5],row[6]))
                        # ads_cur.execute("""
                        #     insert into ext_pypclockin values('%s','%s','%s','%s','%s','%s','%s')""", (
                        #         row[0],row[1],row[2],row[3],row[4],row[5],row[6]))

                # print row
                # print str(row)
            # with open(file_name, 'wb') as f:
            #     csv.writer(f).writerows(db2_cur)
    # with db_cnx.ads_dds() as ads_con:
    #     with ads_con.cursor() as pg_cur:
    #         pg_cur.execute("delete from ext_pypclockin")
    #         with open(file_name, 'r') as io:
    #             pg_cur.copy_expert("""copy arkona.ext_pyptbdta from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con.connected:
        db2_con.close()
    if ads_con:
        ads_con.close()
