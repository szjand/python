"""
  scrapes boptrad to /files/ext_arkona_boptrad.csv
  6/5/15 added where clause to eliminate some useless bogus data
"""
import pyodbc
import csv
import time
import psycopg2

db2Con = pyodbc.connect('DSN=Arkona64; UID=rydejon; PWD=fuckyou5')
db2Cursor = db2Con.cursor()
out_file = 'files/ext_arkona_boptrad.csv'
t1 = time.time()
db2Cursor.execute("""
select
  COMPANY_NUMBER,KEY,VIN,STOCK_,TRADE_ALLOWANCE,ACV,PAYOFF_AMOUNT,PAYOFF_TO,PAYOFF_ALIAS,PAYOFF_ADDR_1,
  PAYOFF_ADDR_2,PAYOFF_CITY,PAYOFF_STATE,PAYOFF_ZIP,PAYOFF_PHONE_NO,PAYOFF_VENDOR,PAYOFF_LOAN_,
  PAYOFF_EXP_DATE,FROM_LEASE,ODOMETER,INTL_POSTAL
FROM  rydedata.boptrad a
where trim(stock_) not in (
  select trim(stock_)
  from rydedata.boptrad
  group by trim(stock_)
  having count(*) > 1)
 and exists ( -- eliminates a few with no matching key in bopmast
   select 1
   from rydedata.bopmast
   where bmkey = a.btkey)
""")


with open(out_file, 'wb') as f:
    csv.writer(f).writerows(db2Cursor)
db2Cursor.close()
db2Con.close()
f.close()

pgCon = psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'")
pgCursor = pgCon.cursor()
pgCursor.execute("truncate test.ext_arkona_boptrad")

pgCon.commit()

io = open(out_file, 'r')

pgCursor.copy_expert("""copy test.ext_arkona_boptrad from stdin with (format csv)""", io)


pgCon.commit()
io.close()

t2 = time.time()
print t2 - t1

io.close()
pgCursor.close()
pgCon.close()
