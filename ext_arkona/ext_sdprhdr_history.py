# encoding=utf-8
import csv
import db_cnx

task = 'ext_sdprhdr_toyota_history'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_sdprhdr_toyota_history.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(company_number),TRIM(ro_number),TRIM(ro_type),warranty_ro_,TRIM(service_writer_id),
                    TRIM(ro_technician_id),customer_key,TRIM(cust_name),a_r_cust_key,TRIM(payment_method),open_date,
                    close_date,final_close_date,TRIM(vin),total_estimate,serv_cont_comp,serv_cont_comp_2,
                    serv_cont_deduct,serv_cont_deduct2,warranty_deduct,TRIM(franchise_code),odometer_in,odometer_out,
                    TRIM(check_number),TRIM(purchase_order_),receipt_number,parts_total,labor_total,sublet_total,
                    sc_deduct_paid,serv_cont_total,spec_ord_depos,cust_pay_hzrd_mat,cust_pay_sale_tax,
                    cust_pay_sale_tax_2,cust_pay_sale_tax_3,cust_pay_sale_tax_4,warranty_sale_tax,warranty_sale_tax_2,
                    warranty_sale_tax_3,warranty_sale_tax_4,internal_sale_tax,internal_sale_tax_2,internal_sale_tax_3,
                    internal_sale_tax_4,svc_cont_sale_tax,svc_cont_sale_tax_2,svc_cont_sale_tax_3,svc_cont_sale_tax_4,
                    cust_pay_shop_sup,warranty_shop_sup,internal_shop_sup,svc_cont_shop_sup,coupon_number,
                    coupon_discount,total_coupon_disc,total_sale,total_gross,TRIM(internal_auth_by),TRIM(tag_number),
                    date_time_of_appointment,date_time_last_line_complete,date_time_pre_invoiced,
                    TRIM(towed_in_indicator),doc_create_timestamp,TRIM(dispatch_team),tax_group_override,
                    discount_taxable_amount,parts_discount_allocation_amount,labor_discount_allocation_amount
                from RYDEDATA.SDPRHDR
                where company_number = 'RY8'
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_sdprhdr_toyota_history")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_sdprhdr_toyota_history from stdin with csv encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
