# encoding=utf-8
import csv
import db_cnx
import ops
import string
import pypyodbc


task = 'ext_glpdept'
pg_con = None
db2_con = None
# run_id = None
file_name = 'files/ext_glpdept.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    # with db_cnx.arkona_report() as db2_con:
    with pypyodbc.connect('DRIVER={iSeries Access ODBC Driver};'
                          'system=REPORT1.DMS.DEALERTRACK.COM;uid=rydejon;pwd=fuckyou5') as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRIM(DEPARTMENT_CODE),TRIM(DEPT_DESCRIPTION)
                from rydedata.glpdept a
            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
                # print db2_cur.fetchall()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_glpdept")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_glpdept from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print(error)
finally:
    # accomodate pypyodbc, if db2_con throws a closing closed connection error
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
