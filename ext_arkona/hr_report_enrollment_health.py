# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'hr_report_enrollment_health'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/hr_report_enrollment_health.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select trim(pymast_employee_number),
                  case when pymast_company_number = 'RY1' then 'GM' else 'Honda Nissan' end as store_location,
                  trim(employee_last_name), trim(employee_first_name), trim(employee_middle_name), 'ssn' as ssn, '' as relationship,
                  birth_date, sex, trim(address_1), trim(address_2), trim(city_name), state_code_Address_, zip_code, 
                  tel_area_code ||'-'||  telephone_number
                -- Select *
                from RYDEDATA.pymast
                where active_code <> 'T'
                  and pymast_company_number in ('RY1','RY2')
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate jon.enrollment_census_health")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy jon.enrollment_census_health from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
