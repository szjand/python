# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_inpvinv'
pg_con = None
db2_con = None
file_name = 'files/ext_inpvinv.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRIM(MANUFACTURER_CODE),TRIM(DOCUMENT_NUMBER),TRIM(VIN),
                    TRIM(STOCK_NUMBER),TRIM(VEHICLE_CODE),YEAR,TRIM(MAKE),TRIM(MODEL_CODE),TRIM(MODEL),
                    TRIM(BODY_STYLE),TRIM(COLOR),TRIM(TRIM),CYLINDERS,TRIM(TRUCK),TRIM(WHEEL_DRIVE4WD),
                    TRIM(TURBO),TRIM(COLOR_CODE),TRIM(ENGINE_CODE),TRIM(TRANSMISSION_CODE),TRIM(RADIO_CODE),
                    DATE_DELIVERED,DATE_ORDERED,LIST_PRICE,VEHICLE_COST,GROSS_WEIGHT,CREATE_DATE,
                    TRIM(MODEL_DESC),TRIM(MANF_MODEL_CODE),TRIM(FEATURE_CODES),TRIM(KEY_TO_CAP_EXPLOSION_DATA),
                    TRIM(CO2_EMISSION_CODE2),VAT_AMOUNT,DELIVERY_COST,AMOUNT_1,AMOUNT_2,TRIM(FIELD_1),
                    TRIM(FIELD_2),TRIM(FIELD_3),TRIM(FIELD_4),TRIM(FIELD_5),TRIM(PERSON_SALUTATION),
                    TRIM(PERSON_GIVEN_NAME),TRIM(PERSON_MIDDLE_NAME),TRIM(PERSON_FAMILY_NAME),
                    TRIM(PERSON_SUFFIX),TRIM(PERSON_ADDRESS1),TRIM(PERSON_ADDRESS2),TRIM(PERSON_CITY),
                    TRIM(PERSON_STATE),TRIM(PERSON_COUNTRY),PERSON_ZIP_CODE,TRIM(SOLD_TO_PARTID),
                    TRIM(SHIP_TO_PARTID),TRIM(SHIP_DATE),TRIM(VEHICLE_STATUS)
                from rydedata.inpvinv a
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_inpvinv")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_inpvinv from stdin with csv encoding 'latin-1 '""", io)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception error:
    print(error)
finally:
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
