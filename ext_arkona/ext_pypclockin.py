# encoding=utf-8

import csv
import db_cnx

task = 'ext_pypclockin'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pypclockin_2018_test.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  TRIM(YICO#),TRIM(PYMAST_EMPLOYEE_NUMBER),YICLKIND,
                  case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,
                  YICLKOUTD,
                  case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end as YICLKOUTT,
                  TRIM(YISTAT),
                  TRIM(YIINFO),TRIM(YICODE),TRIM(YIUSRIDI),TRIM(YIWSIDI),TRIM(YIUSRIDO),TRIM(YIWSIDO)
                from rydedata.pypclockin
                where yico# in ('RY1','RY2')
                  and yiclkind between '12/31/2017' and '01/05/2019'

            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate jon.ext_pypclockin_2018_test")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy jon.ext_pypclockin_2018_test from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    print(error)

