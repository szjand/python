# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_pdppdet'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pdppdet.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(PTCO#),PTPKEY,PTLINE,TRIM(PTLTYP),PTSEQ#,TRIM(PTCPID),TRIM(PTCODE),TRIM(PTSOEP),
                  PTDATE,TRIM(PTMANF),TRIM(PTPART),PTPLNG,TRIM(PTSGRP),TRIM(PTBINL),PTQTY,PTCOST,PTLIST,PTTRAD,
                  PTFPRC,PTNET,PTBLIST,TRIM(PTMETH),PTPCT,TRIM(PTBASE),TRIM(PTSPCD),TRIM(PTPOVR),TRIM(PTGPRC),
                  TRIM(PTCORE),TRIM(PTADDP),PTSPPC,TRIM(PTORSO),TRIM(PTXCLD),TRIM(PTCMNT),TRIM(PTLSTS),
                  TRIM(PTSVCTYP),TRIM(PTLPYM),TRIM(PTPADJ),TRIM(PTTECH),TRIM(PTLOPC),TRIM(PTCRLO),PTLHRS,
                  PTLCHR,PTARLA,TRIM(PTFAIL),PTPLVL,PTPSAS,TRIM(PTSLV#),TRIM(PTSLI#),PTDCPN,PTFACT,TRIM(PTACODE),
                  TRIM(PTBAWC),PTDTLC,TRIM(PTINTA),TRIM(PTLAUTH),PTNTOT,PTOAMT,PTOHRS,PTPMCS,PTPMRT,TRIM(PTRPRP),
                  TRIM(PTTXIN),TRIM(PTUPSL),TRIM(PTWCTP),TRIM(PTVATCODE),PTVATAMT,TRIM(PTRTNINVF),TRIM(PTSKLLVL),
                  TRIM(PTOEMRPLF),PTCSTDIF$,PTOPTSEQ#,PTDISSTRZ,PTDSCLBR$,PTDSCPRT$,TRIM(PTPICKZNE),TRIM(PTDISPTY1),
                  TRIM(PTDISPTY2),PTDISRANK1,PTDISRANK2,PTDSPADATE,PTDSPATIME,PTDSPMDATE,PTDSPMTIME,PTPTAXAMT,
                  PTLTXGRP,PTKTXAMT,TRIM(PTDUPDATE)
                FROM  rydedata.pdppdet a
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_pdppdet")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_pdppdet from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
