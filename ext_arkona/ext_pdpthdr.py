# encoding=utf-8
"""
11/8/21
    scraped 01/01/19 -> 11/7/21
"""
import csv
import db_cnx

task = 'ext_pdpthdr'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pdpthdr.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRIM(INVOICE_NUMBER),TRIM(DOCUMENT_TYPE),TRIM(TRANSACTION_TYPE),
                    TRIM(COUNTER_PERSON_ID),TRIM(AUTHOR_CTRP_ID),TRANS_DATE,CUSTOMER_KEY,SHIP_TO_KEY,CUST_PHONE_NO,
                    TRIM(SORT_NAME),TRIM(PAYMENT_METHOD),TRIM(SALE_TYPE),TRIM(PURCHASE_ORDER_),RECEIPT_NUMBER,
                    PRICE_LEVEL,PARTS_TOTAL,SHIPPING_TOTAL,SPEC_ORD_DEPOSIT,SALES_TAX,SALES_TAX_LEVEL_2,
                    SALES_TAX_LEVEL_3,SALES_TAX_LEVEL_4,GROSS_PROFIT,DOC_CREATE_TIMESTAMP,CT_HOLD_TIMESTAMP,
                    TRIM(INVOICE_LIMIT_AUTH_),TRIM(LIMIT_OVERRIDE_USER),DELIVERY_METHOD,MULTI_SHIP_SEQ_,
                    TAX_GROUP_OVERRIDE,TRIM(UPDATED_BY_OBJECT),SUMMED_TOTAL,TRIM(CUSTOMER_ORDER_NUMBER),
                    CUSTOMER_ORDER_SEQUENCE_NUMBER,TRIM(WHOLESALE_REPRESENTATIVE),TERRITORY,
                    ROUTE,TRIM(SALES_SUB_TYPE)
                from rydedata.pdpthdr
                where trans_date > 20190000
            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_pdpthdr")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_pdpthdr from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print(error)
finally:
    # syntax required for pypyodbc
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
