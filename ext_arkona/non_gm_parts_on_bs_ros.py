# encoding=utf-8

import csv
import db_cnx
"""
non_gm_parts_on_bs_ros
for parts inventory
"""
file_name = 'files/non_gm_parts_on_bs_ros.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
                select ptdoc# as RO, ptpart as "Part Number", ptqty as Quantity, pmcost as Cost,
                  pmlist as List
                --select sum(ptqty * pmcost), sum(ptqty * pmlist)  -- 35913/54398
                from rydedata.pdpphdr a
                left join rydedata.pdppdet b on a.ptpkey = b.ptpkey
                left join rydedata.pdpmast c on trim(b.ptpart) = trim(c.pmpart)
                where a.ptdtyp = 'RO'
                  and a.ptco# = 'RY1'
                  and b.ptpart <> ''
                  and c.pmmanf = 'OT'
                  and c.stocking_group = '920'
                  and ptqty > 0
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)

