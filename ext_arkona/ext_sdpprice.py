# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_sdpprice_tmp'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_sdpprice_tmp.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(SPCO#),TRIM(SPSVCTYP),TRIM(SPLPYM),TRIM(SPFRAN),SPSVCO,SPSPCT,TRIM(SPSLBR),TRIM(SPSPRT),
                    TRIM(SPSSBL),SPMDY1,SPMDY2,SPMDY3,SPMDY4,SPLR1A,SPLR2A,SPLR3A,SPLR4A,SPLR1B,SPLR2B,SPLR3B,SPLR4B,
                    SPLR1C,SPLR2C,SPLR3C,SPLR4C,SPLR1D,SPLR2D,SPLR3D,SPLR4D,SPPLV1,SPPLV2,SPPLV3,SPPLV4,SPPSA1,SPPSA2,
                    SPPSA3,SPPSA4,TRIM(SPCLAC),TRIM(SPTLAC),TRIM(SPWRAC),TRIM(SPFRAC),TRIM(SPCUS#),SPSPCTP,SPLMA1,
                    SPLMA2,SPLMA3,SPLMA4
                from rydedata.sdpprice
            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_sdpprice_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_sdpprice_tmp from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
#     print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
# except Exception, error:
#     # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
#     print error
finally:
    # required syntax for pypyodbc
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
