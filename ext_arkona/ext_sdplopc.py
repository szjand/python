# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_sdplopc'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_sdplopc.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRIM(OPERACTION_CODE),TRIM(DESCRIPTION_1),TRIM(DESCRIPTION_2),
                  TRIM(DESCRIPTION_3),TRIM(DESCRIPTION_4),TRIM(MAJOR_CODE),TRIM(MINOR_CODE),TRIM(MANUFACTURER),
                  TRIM(MAKE),TRIM(MODEL),TRIM(ENGINE),TRIM(YEAR),TRIM(DEF_LINE_PYM_METH),TRIM(CORRECTION_LOP),
                  TRIM(TYPE),TRIM(EXCLUDE_DISCOUNT),TRIM(A_B_C_OR_D),TRIM(TAXABLE),HAZARD_MATER_CHG,
                  TRIM(SHOP_SUPPLY_CHG),TRIM(RETAIL_METHOD),RETAIL_AMOUNT,TRIM(COST_METHOD),COST_AMOUNT,
                  TRIM(PARTS_METHOD),PARTS_AMOUNT,TRIM(ESTIMATE),ESTIMATED_HRS,TRIM(WAITER_FLAG),
                  TRIM(PREDEFINED_CAUSE_DESCRIPTION1),TRIM(ADDITIONAL_DESCRIPTION_LINE5),
                  TRIM(ADDITIONAL_DESCRIPTION_LINE6),TRIM(PREDEFINED_COMPLAINT_DESCRIP1),TRIM(DEFAULT_SERVICE_TYPE)
                from rydedata.sdplopc
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_sdplopc")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_sdplopc from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    # required syntax for pypyodbc
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
