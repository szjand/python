# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_glparsh'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_glparsh.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRANSACTION_KEY,CUST_TYPE_KEY,TRIM(CUSTOMER_NUMBER),
                  TRIM(CUSTOMER_NAME),TRIM(INV_TRACKING_METH),TRIM(PRT_STMT_W_0_BAL),LAST_STATMENT_BAL,
                  LAST_STMT_TRAN_NO,CURRENT1,BALANCE_01_30,BALANCE_31_60,BALANCE_61_,TOTAL_DUE,PAST_DUE,
                  INTEREST_CHG,DATE_LAST_PAYMENT,BALANCE_120_
                from rydedata.glparsh
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_glparsh")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_glparsh from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
