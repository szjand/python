# encoding=utf-8
import csv
import db_cnx
import ops
import string

# task = 'ext_sdpxtim'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_boplsrc.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  TRIM(COMPANY_NUMBER),KEY,NAME_KEY,TRIM(LENDOR_NAME),TRIM(ADDRESS),TRIM(ADDRESS2),TRIM(ADDRESS3),
                  TRIM(CITY),TRIM(COUNTY),TRIM(STATE_CODE),ZIP_CODE,PHONE_NUMBER,TRIM(INT_ZIP_CODE),
                  TRIM(INT_PHONE_NUMBER),TRIM(CONTACT),BUY_RATE,LOAN_ORIG_FEE,TRIM(FIN_RESERVE_METH),
                  RES_HLD_BCK_TRM_1,RES_HLD_BCK_TRM_2,RES_HLD_BCK_TRM_3,RES_HOLD_BACK_1,RES_HOLD_BACK_2,
                  RES_HOLD_BACK_3,MAXIMUM_TERM,TRIM(INSUR_REQUIRED),TRIM(A_R_ACCOUNT),TRIM(A_R_CUST_NUMBER),
                  TRIM(BALLOON_PAY_METH),TRIM(LOF_TO_G_L),TRIM(ODD_DAYS_METHOD),TRIM(OPTIONAL_FIELDS),
                  TRIM(LIEN_HOLD_ADDR_1),TRIM(LIEN_HOLD_ADDR_2),TRIM(LIEN_HOLD_ADDR_3),TRIM(LIEN_HOLD_CITY),
                  TRIM(LIEN_HOLD_CITY_BLLHCNTY),TRIM(LIEN_HOLDER_NAME),LIEN_HOLD_PHONE_,
                  TRIM(INT_LIEN_HOLD_PHONE_),TRIM(LIEN_HOLD_STATE),LIEN_HOLD_ZIP,TRIM(INT_LIEN_HOLD_ZIP),
                  TRIM(MISC_INFORMATION_1),TRIM(MISC_INFORMATION_2),TRIM(PAYMENT_FORMULA),
                  TRIM(SPLIT_TERM_ALLOWED),MILEAGE_YR_ALLOWED,EXCS_MI_RATE_INC,EXCS_MI_RATE_PEN2,
                  TRIM(CALCULATE_VW_EFFECTIVE_APR),VSI_FEE
                from rydedata.boplsrc
            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_boplsrc")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_boplsrc from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    # print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception as error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print(error)
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
