# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_sdphist'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_sdphist.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  TRIM(COMPANY_NUMBER),TRIM(REPAIR_ORDER_NUMBER),UPDATED_TIME,LINE_NUMBER,
                  TRIM(RO_LINE_STATUS),SEQUENCE_NUMBER,TRIM(REPAIR_ORDER_STATUS),
                  TRIM(LINE_TYPE),TRIM(SERVICE_TYPE),TRIM(MISC_TYPE),TRIM(TRANSACTION_CODE),
                  TRIM(PAY_METHOD),TRIM(LABOR_CODE),LABOR_HOURS,LABOR_COST,LABOR_NET,
                  ESTIMATE_HOURS,ESTIMATE_TOTAL,AMOUNT,TRIM(PRINTER),PRINTED_COPIES,
                  TRIM(CONVERT_FROM),ODOMETER,TRIM(TAG_NUMBER),TRIM(COUNTER_PERSON),
                  TRIM(SERVICE_WRITER),TRIM(TEAM_ID),TRIM(TECHNICIAN_ID),TRIM(UPDATE_USER),
                  TRIM(UPDATE_PROGRAM),TRIM(EVENT_TYPE),TRIM(UPDATE_VENDOR)
                from rydedata.sdphist
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_sdphist")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_sdphist from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    # required syntax for pypyodbc
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
