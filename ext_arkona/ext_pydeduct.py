# encoding=utf-8
import csv
import db_cnx
import ops
import string

# ************** moved to luigi 9/25/19 **************************

# task = 'ext_pydeduct'
# pg_con = None
# db2_con = None
# run_id = None
file_name = 'files/ext_pydeduct.csv'

# try:
with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select TRIM(COMPANY_NUMBER),TRIM(EMPLOYEE_NUMBER),TRIM(CODE_TYPE),TRIM(DED_PAY_CODE),SEQUENCE,
                FIXED_DED_AMT,TRIM(VARIABLE_AMOUNT),TRIM(FIXED_DED_PCT),DED_LIMIT,TRIM(DED_FREQ),TOTAL,
                TRIM(TAXING_UNIT_ID_NUMBER),TRIM(TAXING_UNIT_TYPE),BANK_ROUTING_,BANK_ACCOUNT,
                CODE_BEGINNING_DATE,CODE_ENDING_DATE,FIXED_DED_AMT_2,TRIM(VARIABLE_AMOUNT_2),
                TRIM(FIXED_DED_PCT_2),TRIM(EMP_LEVEL_CTL_),TRIM(PLAN_POLICY_OF_GARN),TRIM(FUTURE_1),
                TRIM(FUTURE_2),YEAR_FOR_USE,DED_PRIORITY_SEQ
            from rydedata.pydeduct
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_pydeduct")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_pydeduct from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
#     print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
# except Exception, error:
#     # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
#     print error
# finally:
#     if db2_con.connected:
#         db2_con.close()
#     if pg_con:
#         pg_con.close()
