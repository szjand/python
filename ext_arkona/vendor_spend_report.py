# encoding=utf-8
import csv
import db_cnx

# import locale
# # if using *nix
# locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
"""
1/13/2020
    tried to run this in the linux dream, failed with an ambiguous error: [<class 'decimal.ConversionSyntax'>]
    made no sense, google no help, time is short
    ran it in then W10_python dream and everything worked

    plus it reminded me of this construct for arkona reports ...

1/11/2021
    same thing (though i don't know what the plus it reminded ... means ???)
    ran this script in the W10 dream, no problems    
    
05/14/21
midyear request from June
Interpreter: Python 2.712 virtualenv at Z:\E\python_venvs\luigi_2712
    
01/10/2022  
01/13/2023
  
"""
task = 'vendor_spend_report'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/vendor_spend_report.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            # sql = """
            #     select b.company_number, b.record_key, gtctl#, a.gtvnd# as VendorNumber,
            #       b.gcsnam as Name, b.gcftid as TaxID,
            #       coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
            #       case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
            #       c.bncity as City, c.bnstcd as State, c.bnzip as zip, b.corporation_1099_
            #     from (
            #       select trim(gtvnd#) as gtvnd#, trim(gtctl#) as gtctl#,
            #       sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
            #       sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount
            #       from rydedata.glptrns
            #       where gtpost = 'Y'
            #         and trim(gtacct) in( '120300','220200')
            #         and gtdate between '01/01/2022' and '12/31/2022' ---------set the date range ------
            #         and gttamt < 0
            #         and gtvnd# <> ''
            #       group by gtvnd#, gtctl#) a
            #     inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
            #     left join rydedata.bopname c on b.gckey = c.bnkey
            #       and c.bntype = 'C'
            #     where b.gcvnd# is not null
            # """
            sql = """
                select b.company_number, b.record_key, gtctl#, a.gtvnd# as VendorNumber, 
                  b.gcsnam as Name, b.gcftid as TaxID, 
                  coalesce(RY1Amount, 0) as "RY1 Amount", coalesce(RY2Amount, 0) as "RY2 Amount",
                  coalesce(RY8Amount, 0) as "RY8 Amount",
                  case when c.bnadr1 = '' then c.bnadr2 else c.bnadr1 end as address,
                  c.bncity as City, c.bnstcd as State, c.bnzip as zip, b.corporation_1099_
                from (
                  select trim(gtvnd#) as gtvnd#, trim(gtctl#) as gtctl#,
                  sum(case when trim(gtacct) = '120300' then abs(gttamt) end) as RY1Amount,
                  sum(case when trim(gtacct) = '220200' then abs(gttamt) end) as RY2Amount,
                  sum(case when trim(gtacct) = '3000' then abs(gttamt) end) as RY8Amount
                  from rydedata.glptrns
                  where gtpost = 'Y'
                    and trim(gtacct) in( '120300','220200', '3000')
                    and gtdate between '01/01/2022' and '12/31/2022' ---------set the date range ------
                    and gttamt < 0
                    and gtvnd# <> ''
                  group by gtvnd#, gtctl#) a
                inner join rydedata.glpcust b on a.gtvnd# = trim(b.gcvnd#)
                  and b.company_number in ('RY1','RY2','RY8') --- exclude RY6, RY7
                left join rydedata.bopname c on b.gckey = c.bnkey
                  and c.bntype = 'C'
                  and c.bopname_company_number in ('RY1', 'RY2', 'RY8') --- exclude RY6, RY7
                where b.gcvnd# is not null             
            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.vendor_spend")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.vendor_spend from stdin with csv encoding 'latin-1 '""", io)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    print(error)
finally:
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
