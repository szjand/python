# encoding=utf-8

import csv
import db_cnx

file_name = 'files/ext_pdpmrpl.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),TRIM(MANUFACTURER),TRIM(OLD_PART_NUMBER),
              TRIM(REPLACE_TYPE),TRIM(ACTIVE_PART_NO_)
            from rydedata.pdpmrpl
            where company_number in ('RY1','RY2')
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_pdpmrpl")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_pdpmrpl from stdin with csv encoding 'latin-1 '""", io)

