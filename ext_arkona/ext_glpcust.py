# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_glpcust'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_glpcust.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
              SELECT TRIM(COMPANY_NUMBER),RECORD_KEY,TRIM(ACTIVE),TRIM(CUSTOMER_NUMBER),TRIM(VENDOR_NUMBER),
                TRIM(SEARCH_NAME),TRIM(BILL_CONTACT),TRIM(BILL_ADDRESS_1),TRIM(BILL_ADDRESS_2),TRIM(BILL_CITY),
                TRIM(BILL_STATE_CODE),BILL_ZIP_CODE,BILL_PHONE_NUMBER,BILL_PHONE_EXT,BILL_FAX_NUMBER,
                TRIM(PYMT_CONTACT),TRIM(PYMT_ADDRESS_1),TRIM(PYMT_ADDRESS_2),TRIM(PYMT_CITY),
                TRIM(PYMT_STATE_CODE),PYMT_ZIP_CODE,PYMT_PHONE_NUMBER,PYMT_PHONE_EXT,PYMT_FAX_NUMBER,
                TRIM(FEDERAL_TAX_ID),SOC_SEC_,TRIM(PO_REQUIRED),CUSTOMER_TYPE,VENDOR_TYPE,AR_PAYMENT_TERMS,
                AP_PAYMENT_TERMS,TAX_GROUP,CREDIT_LIMIT,TRIM(FINANCE_CHARGE),TRIM(CORPORATION_1099_),
                TRIM(CHECK_STUB_DETAIL),CREATION_DATE,TRIM(OPEN_PO_),TRIM(CUSTOMER_ACCOUNT_),
                LAST_INVOICE_DATE,LAST_INVOICE_AMT,LAST_PAYMENT_DATE,LAST_PAYMENT_AMT,LAST_STATMNT_DATE,
                LAST_STATMENT_BAL,LAST_STMT_TRAN_NO,TRIM(DISPLAY_COMMENTS),TRIM(DIRECTED_ACCOUNT_),
                TRIM(SECOND_NAME2),TRIM(INTERNATIONAL_BILL_ADDRESS_2),TRIM(INTERNATIONAL_BILL_ZIP_CODE),
                TRIM(INTERNATIONAL_BILL_PHONE_),TRIM(INTERNATIONAL_BILL_PHONE_EXT),TRIM(INTERNATIONAL_BILL_FAX_),
                TRIM(INTERNATIONAL_PYMT_ADDRESS_2),TRIM(INTERNATIONAL_PYMT_ZIP_CODE),
                TRIM(INTERNATIONAL_PYMT_PHONE_),TRIM(INTERNATIONAL_PYMT_PHONE_EXT),TRIM(INTERNATIONAL_PYMT_FAX_),
                TRIM(BANK_ACCOUNT_NO_),TRIM(BANK_ROUTING_NO_),TRIM(WORK1),TRIM(EMAIL_TEXT_APPROVAL)
              from rydedata.glpcust
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_glpcust")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_glpcust from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    # syntax required for pypyodbc
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
