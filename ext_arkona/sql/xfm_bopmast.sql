﻿add columns: 
  bopmast_key serial NOT NULL,
  row_from_date date NOT NULL,
  row_thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  current_row boolean NOT NULL,
  date_deleted date NOT NULL DEFAULT '9999-12-31'::date,
  hash text NOT NULL,  

CREATE TABLE arkona.xfm_bopmast_changed_rows(
  bopmast_company_number citext NOT NULL,
  record_key integer NOT NULL,
  PRIMARY KEY (bopmast_company_number, record_key));

  
SELECT array_agg(attname)
FROM  pg_attribute
WHERE attrelid = 'arkona.ext_bopmast'::regclass  -- table name, optionally schema-qualified
  AND attnum > 0
  AND NOT attisdropped 

3/11/19
the new attributes that i added to the structure today, 
trade_tax_credit,estimated_Fees_and_taxes,mapped_tax_zip_code,mapped_tax_city,mapped_tax_location_id 
checked in db2, there are no values for them yet

-- get started with the oldest file
insert into arkona.ext_bopmast
select a.*, null, null, null, null, null
from test.ext_bopmast_0110 a

delete from arkona.ext_bopmast;
-- ahh, don't need to specify the nulls
insert into arkona.ext_bopmast
select a.*
from test.ext_bopmast_0111 a
-- new rows

SELECT array_agg(attname)
FROM  pg_attribute
WHERE attrelid = 'arkona.xfm_bopmast'::regclass  -- table name, optionally schema-qualified
  AND attnum > 0
  AND NOT attisdropped 

1. populate arkona.ext_bopmast with test.ext_bopmast_0110  -- row_from_date = '01/10/2019'
2. new rows into arkona.xfm_bopmast
3. populate arkona.ext_bopmast with test.ext_bopmast_0111  -- row_from_date = '01/11/2019'
4. populate arkona.xfm_bopmast_changed_rows with changed row data
5. update current version of changed row
6. add new row for each changed row 
7. update arkona.xfm_bopmast.date_deleted for rows that no longer exist in arkona.ext_bopmast

3/12/19
looks ok, turn it into a script and do all 2019 dates, then can export to pg

row_thru_date is fucked up

delete from arkona.xfm_bopmast;

select record_key, count(*)
from arkona.xfm_bopmast
group by record_key
order by count(*) desc 

select * from arkona.xfm_bopmast where record_key = 52887 order by bopmast_key


select record_key, bopmast_stock_number
from arkona.xfm_bopmast
where date_deleted < current_date
order by bopmast_key desc 

select * from arkona.xfm_bopmast where record_key = 53323 order by bopmast_key


select record_key, bopmast_stock_number
from arkona.xfm_bopmast a
where exists (
  select 1
  from arkona.xfm_bopmast
  where bopmast_company_number = a.bopmast_company_number
    and record_key = a.record_key
    and date_deleted < current_date)
order by record_key desc 

select * from arkona.xfm_bopmast where record_key = 52708 order by bopmast_key

select date_deleted, count(*) from arkona.xfm_bopmast group by date_deleted

-- shit, all deleted dates are 3/12, must be overwriting it each day after it was deleted







insert into arkona.xfm_bopmast (
  bopmast_company_number,record_key,record_status,record_type,vehicle_type,
  franchise_code,sale_type,bopmast_stock_number,bopmast_vin,buyer_number,
  co_buyer_number,bopmast_search_name,retail_price,down_payment,trade_allowance,
  trade_acv,trade_payoff,apr,term,insurance_term,days_to_1st_paymt,
  payment_frequency,payment,amount_financed,origination_date,date_1st_payment,
  date_last_payment,date_sticker_exp,tax_group,lending_source,leasing_source,
  insurance_source,gap_source,serv_cont_company,serv_cont_amount,serv_cont_deduct,
  serv_cont_miles,serv_cont_months,serv_cont_cost,serv_cont_str_dat,
  serv_cont_exp_dat,add_serv_to_cap_c,credit_life_premium,level_cl_premium,
  a_h_premium,gap_premium,gap_cost,credit_life_code,a_h_code,gap_code,tax_1,
  tax_2,tax_3,tax_4,tax_5,buy_rate,reserve_holdback_,number_of_trades,
  temporarysticker_,serv_cont_number,cred_life_cert_,a_h_cert_,gap_cert_,
  flooring_vendor,pd_insurance_co,pd_insur_co_alias,pd_policy_number,
  pd_policy_exp_dat,pd_agent_name,pd_agent_address,pd_agent_city,pd_agent_state,
  pd_agent_zip,pd_agent_phone_no,pd_coll_coverage,pd_comp_coverage,
  pd_fire_theft_cov,pd_coll_deduct,pd_comp_deduct,pd_fire_theft_ded,
  pd_vehicle_use,pd_premium,amo_total,fee_total,total_of_payments,
  interest_charged,vehicle_cost,internal_cost,adds_to_cost_tot,
  hold_back,pack_amount,comm_gross,incentive,rebate,odometer_at_sale,
  pickup_amount,pickup_beg_date,pickup_freq,pickup_no_pay,life_reserve,
  a_h_reserve,gap_reserve,pdi_reserve,amo_reserve,serv_reserve,
  finance_reserve,total_f_i_reserve,primary_salespers,secondary_slspers2,
  def_down_payment,def_down_due_date,loan_orig_fee,lof_override,effective_apr,
  msrp,lease_price,capitalized_cost,cap_cost_reduct,apr_bmlapr,lease_factor,
  lease_term,residual_,residual_amt,net_residual,tot_depreciation,
  miles_per_year,miles_year_allow,excess_mile_rate,excess_mile_rate2,
  excess_mile_chg,acquisition_fee,lease_payment,lease_paymt_tax,
  lease_sales_tax,mo_lease_charge,cap_reduction_tax,cap_reduct_tax_1,
  cap_reduct_tax_2,cap_reduct_tax_3,cap_reduct_tax_4,cap_reduct_tax_6,
  security_deposit,deposit_received,cash_received,factor_buy_rate,
  total_initial_chg,cap_lease_sls_tax,secur_depos_oride,acqui_fee_oride,
  fet_options,capitalize_sls_tx,adv_payment_lease,oride_tax_rate_1,
  oride_tax_rate_2,oride_tax_rate_3,oride_tax_rate_4,oride_tax_rate_6,
  oride_exc_trade_1,oride_exc_trade_2,oride_exc_trade_3,oride_exc_trade_4,
  tax_override_1,tax_override_2,tax_override_3,tax_override_4,tax_override_6,
  fin_reserv_oride,cost_override,holdback_override,tax_override,
  rebate_down_pay,contract_selected,chg_int_on_1_pymt,sale_account,
  unwind_flag,document_number,buyer_age,co_buyer_age,life_coverage,
  a_h_coverage,insur_term_date,lease_tax_group,co_buyer_life_prem,
  date_saved,date_trade_payoff,capitalize_cr_tax,rebate_as_down,total_down,
  tot_cap_reduction,gross_cap_cost,dealer_incentive,forms_printed,
  new_veh_deliv_rpt,date_approved,date_capped,excess_mileage_charge_ball,
  penalty_excess_mileage_charg,prepaid_excess_mileage_charg,
  miles_per_year_balloon,miles_per_year_allowed_bal,net_residual_balloon,
  residual_amount_balloon,residual_percent_balloon,after_market_tax,
  exempt_payments,full_payments,ford_plan_code,a_h_term,lease_payment_tax,
  trade_from_lease,orig_net_trade,ntax_trade_equity,pd_policy_number_b9pdp_,
  part_exempt_pymt,part_exempt_tax,service_cont_tax,total_a_h_coverag,
  tax_base_amount,work_in_process,date_last_lease_payment,
  aftermarket_tax_rate5,service_contract_tax_rate7,odd_last_payment,
  original_contract_date,level_life_optional,reg_fee_tax_1,reg_fee_tax_2,
  pymt_excluding_ins,payback_rate,buyer_red_rt_ind,co_buyer_red_rt_ind,
  buyer_gender,co_buyer_gender,crit_ill_code,crit_ill_premium,
  loss_of_employment_code,loss_of_employment_premium,upfront_fee_tax_1,
  upfront_fee_tax_2,residual_insurance,disability_elig,rem_ins_code,
  premium_percent,gap_tax_rate8,delivery_date,initial_miles,initial_mile_rate,
  initial_mile_chg,lease_pmts_remaining,f_i_gross,house_gross,monthly_gap_premium,
  initial_miles_chg_flag,initial_miles_threshold,gap_tax,pro_rata_amount,
  pro_rata_tax_1,pro_rata_tax_2,opentrack_vendor,f_i_manager,sales_manager,
  trade_upfront,trade_capitalized,rebate_upfront,rebate_capitalized,
  cash_upfront,cash_capitalized,sign_and_drive,roll_price_flag,saved_price,
  critical_illness_term,loss_of_employment_term,credit_life_source,a_h_source,
  critical_illness_source,loss_of_employment_source,a_h_coverage_bmahcov,
  level_ci_premium,critical_illnes_cert_,loss_of_employment_cert_,
  critical_illness_reserve,loss_of_employment_reserve,vw_effective_apr,
  unrealized_rebate,a_h_insurance_term_date,ci_insurance_term_date,
  le_insurance_term_date,first_payment_waiver,trade_tax_credit,
  estimated_fees_and_taxes,mapped_tax_zip_code,mapped_tax_city,
  mapped_tax_location_id,
  row_from_date, current_row, hash)
select bopmast_company_number,record_key,record_status,record_type,vehicle_type,
  franchise_code,sale_type,bopmast_stock_number,bopmast_vin,buyer_number,
  co_buyer_number,bopmast_search_name,retail_price,down_payment,trade_allowance,
  trade_acv,trade_payoff,apr,term,insurance_term,days_to_1st_paymt,
  payment_frequency,payment,amount_financed,origination_date,date_1st_payment,
  date_last_payment,date_sticker_exp,tax_group,lending_source,leasing_source,
  insurance_source,gap_source,serv_cont_company,serv_cont_amount,serv_cont_deduct,
  serv_cont_miles,serv_cont_months,serv_cont_cost,serv_cont_str_dat,
  serv_cont_exp_dat,add_serv_to_cap_c,credit_life_premium,level_cl_premium,
  a_h_premium,gap_premium,gap_cost,credit_life_code,a_h_code,gap_code,tax_1,
  tax_2,tax_3,tax_4,tax_5,buy_rate,reserve_holdback_,number_of_trades,
  temporarysticker_,serv_cont_number,cred_life_cert_,a_h_cert_,gap_cert_,
  flooring_vendor,pd_insurance_co,pd_insur_co_alias,pd_policy_number,
  pd_policy_exp_dat,pd_agent_name,pd_agent_address,pd_agent_city,pd_agent_state,
  pd_agent_zip,pd_agent_phone_no,pd_coll_coverage,pd_comp_coverage,
  pd_fire_theft_cov,pd_coll_deduct,pd_comp_deduct,pd_fire_theft_ded,
  pd_vehicle_use,pd_premium,amo_total,fee_total,total_of_payments,
  interest_charged,vehicle_cost,internal_cost,adds_to_cost_tot,
  hold_back,pack_amount,comm_gross,incentive,rebate,odometer_at_sale,
  pickup_amount,pickup_beg_date,pickup_freq,pickup_no_pay,life_reserve,
  a_h_reserve,gap_reserve,pdi_reserve,amo_reserve,serv_reserve,
  finance_reserve,total_f_i_reserve,primary_salespers,secondary_slspers2,
  def_down_payment,def_down_due_date,loan_orig_fee,lof_override,effective_apr,
  msrp,lease_price,capitalized_cost,cap_cost_reduct,apr_bmlapr,lease_factor,
  lease_term,residual_,residual_amt,net_residual,tot_depreciation,
  miles_per_year,miles_year_allow,excess_mile_rate,excess_mile_rate2,
  excess_mile_chg,acquisition_fee,lease_payment,lease_paymt_tax,
  lease_sales_tax,mo_lease_charge,cap_reduction_tax,cap_reduct_tax_1,
  cap_reduct_tax_2,cap_reduct_tax_3,cap_reduct_tax_4,cap_reduct_tax_6,
  security_deposit,deposit_received,cash_received,factor_buy_rate,
  total_initial_chg,cap_lease_sls_tax,secur_depos_oride,acqui_fee_oride,
  fet_options,capitalize_sls_tx,adv_payment_lease,oride_tax_rate_1,
  oride_tax_rate_2,oride_tax_rate_3,oride_tax_rate_4,oride_tax_rate_6,
  oride_exc_trade_1,oride_exc_trade_2,oride_exc_trade_3,oride_exc_trade_4,
  tax_override_1,tax_override_2,tax_override_3,tax_override_4,tax_override_6,
  fin_reserv_oride,cost_override,holdback_override,tax_override,
  rebate_down_pay,contract_selected,chg_int_on_1_pymt,sale_account,
  unwind_flag,document_number,buyer_age,co_buyer_age,life_coverage,
  a_h_coverage,insur_term_date,lease_tax_group,co_buyer_life_prem,
  date_saved,date_trade_payoff,capitalize_cr_tax,rebate_as_down,total_down,
  tot_cap_reduction,gross_cap_cost,dealer_incentive,forms_printed,
  new_veh_deliv_rpt,date_approved,date_capped,excess_mileage_charge_ball,
  penalty_excess_mileage_charg,prepaid_excess_mileage_charg,
  miles_per_year_balloon,miles_per_year_allowed_bal,net_residual_balloon,
  residual_amount_balloon,residual_percent_balloon,after_market_tax,
  exempt_payments,full_payments,ford_plan_code,a_h_term,lease_payment_tax,
  trade_from_lease,orig_net_trade,ntax_trade_equity,pd_policy_number_b9pdp_,
  part_exempt_pymt,part_exempt_tax,service_cont_tax,total_a_h_coverag,
  tax_base_amount,work_in_process,date_last_lease_payment,
  aftermarket_tax_rate5,service_contract_tax_rate7,odd_last_payment,
  original_contract_date,level_life_optional,reg_fee_tax_1,reg_fee_tax_2,
  pymt_excluding_ins,payback_rate,buyer_red_rt_ind,co_buyer_red_rt_ind,
  buyer_gender,co_buyer_gender,crit_ill_code,crit_ill_premium,
  loss_of_employment_code,loss_of_employment_premium,upfront_fee_tax_1,
  upfront_fee_tax_2,residual_insurance,disability_elig,rem_ins_code,
  premium_percent,gap_tax_rate8,delivery_date,initial_miles,initial_mile_rate,
  initial_mile_chg,lease_pmts_remaining,f_i_gross,house_gross,monthly_gap_premium,
  initial_miles_chg_flag,initial_miles_threshold,gap_tax,pro_rata_amount,
  pro_rata_tax_1,pro_rata_tax_2,opentrack_vendor,f_i_manager,sales_manager,
  trade_upfront,trade_capitalized,rebate_upfront,rebate_capitalized,
  cash_upfront,cash_capitalized,sign_and_drive,roll_price_flag,saved_price,
  critical_illness_term,loss_of_employment_term,credit_life_source,a_h_source,
  critical_illness_source,loss_of_employment_source,a_h_coverage_bmahcov,
  level_ci_premium,critical_illnes_cert_,loss_of_employment_cert_,
  critical_illness_reserve,loss_of_employment_reserve,vw_effective_apr,
  unrealized_rebate,a_h_insurance_term_date,ci_insurance_term_date,
  le_insurance_term_date,first_payment_waiver,trade_tax_credit,
  estimated_fees_and_taxes,mapped_tax_zip_code,mapped_tax_city,
  mapped_tax_location_id,
  '01/11/2019' as row_from_date, true as current_row,
    (
      select md5(z::text) as hash
      from (
        select bopmast_company_number,record_key,record_status,record_type,vehicle_type,
          franchise_code,sale_type,bopmast_stock_number,bopmast_vin,buyer_number,
          co_buyer_number,bopmast_search_name,retail_price,down_payment,trade_allowance,
          trade_acv,trade_payoff,apr,term,insurance_term,days_to_1st_paymt,
          payment_frequency,payment,amount_financed,origination_date,date_1st_payment,
          date_last_payment,date_sticker_exp,tax_group,lending_source,leasing_source,
          insurance_source,gap_source,serv_cont_company,serv_cont_amount,serv_cont_deduct,
          serv_cont_miles,serv_cont_months,serv_cont_cost,serv_cont_str_dat,
          serv_cont_exp_dat,add_serv_to_cap_c,credit_life_premium,level_cl_premium,
          a_h_premium,gap_premium,gap_cost,credit_life_code,a_h_code,gap_code,tax_1,
          tax_2,tax_3,tax_4,tax_5,buy_rate,reserve_holdback_,number_of_trades,
          temporarysticker_,serv_cont_number,cred_life_cert_,a_h_cert_,gap_cert_,
          flooring_vendor,pd_insurance_co,pd_insur_co_alias,pd_policy_number,
          pd_policy_exp_dat,pd_agent_name,pd_agent_address,pd_agent_city,pd_agent_state,
          pd_agent_zip,pd_agent_phone_no,pd_coll_coverage,pd_comp_coverage,
          pd_fire_theft_cov,pd_coll_deduct,pd_comp_deduct,pd_fire_theft_ded,
          pd_vehicle_use,pd_premium,amo_total,fee_total,total_of_payments,
          interest_charged,vehicle_cost,internal_cost,adds_to_cost_tot,
          hold_back,pack_amount,comm_gross,incentive,rebate,odometer_at_sale,
          pickup_amount,pickup_beg_date,pickup_freq,pickup_no_pay,life_reserve,
          a_h_reserve,gap_reserve,pdi_reserve,amo_reserve,serv_reserve,
          finance_reserve,total_f_i_reserve,primary_salespers,secondary_slspers2,
          def_down_payment,def_down_due_date,loan_orig_fee,lof_override,effective_apr,
          msrp,lease_price,capitalized_cost,cap_cost_reduct,apr_bmlapr,lease_factor,
          lease_term,residual_,residual_amt,net_residual,tot_depreciation,
          miles_per_year,miles_year_allow,excess_mile_rate,excess_mile_rate2,
          excess_mile_chg,acquisition_fee,lease_payment,lease_paymt_tax,
          lease_sales_tax,mo_lease_charge,cap_reduction_tax,cap_reduct_tax_1,
          cap_reduct_tax_2,cap_reduct_tax_3,cap_reduct_tax_4,cap_reduct_tax_6,
          security_deposit,deposit_received,cash_received,factor_buy_rate,
          total_initial_chg,cap_lease_sls_tax,secur_depos_oride,acqui_fee_oride,
          fet_options,capitalize_sls_tx,adv_payment_lease,oride_tax_rate_1,
          oride_tax_rate_2,oride_tax_rate_3,oride_tax_rate_4,oride_tax_rate_6,
          oride_exc_trade_1,oride_exc_trade_2,oride_exc_trade_3,oride_exc_trade_4,
          tax_override_1,tax_override_2,tax_override_3,tax_override_4,tax_override_6,
          fin_reserv_oride,cost_override,holdback_override,tax_override,
          rebate_down_pay,contract_selected,chg_int_on_1_pymt,sale_account,
          unwind_flag,document_number,buyer_age,co_buyer_age,life_coverage,
          a_h_coverage,insur_term_date,lease_tax_group,co_buyer_life_prem,
          date_saved,date_trade_payoff,capitalize_cr_tax,rebate_as_down,total_down,
          tot_cap_reduction,gross_cap_cost,dealer_incentive,forms_printed,
          new_veh_deliv_rpt,date_approved,date_capped,excess_mileage_charge_ball,
          penalty_excess_mileage_charg,prepaid_excess_mileage_charg,
          miles_per_year_balloon,miles_per_year_allowed_bal,net_residual_balloon,
          residual_amount_balloon,residual_percent_balloon,after_market_tax,
          exempt_payments,full_payments,ford_plan_code,a_h_term,lease_payment_tax,
          trade_from_lease,orig_net_trade,ntax_trade_equity,pd_policy_number_b9pdp_,
          part_exempt_pymt,part_exempt_tax,service_cont_tax,total_a_h_coverag,
          tax_base_amount,work_in_process,date_last_lease_payment,
          aftermarket_tax_rate5,service_contract_tax_rate7,odd_last_payment,
          original_contract_date,level_life_optional,reg_fee_tax_1,reg_fee_tax_2,
          pymt_excluding_ins,payback_rate,buyer_red_rt_ind,co_buyer_red_rt_ind,
          buyer_gender,co_buyer_gender,crit_ill_code,crit_ill_premium,
          loss_of_employment_code,loss_of_employment_premium,upfront_fee_tax_1,
          upfront_fee_tax_2,residual_insurance,disability_elig,rem_ins_code,
          premium_percent,gap_tax_rate8,delivery_date,initial_miles,initial_mile_rate,
          initial_mile_chg,lease_pmts_remaining,f_i_gross,house_gross,monthly_gap_premium,
          initial_miles_chg_flag,initial_miles_threshold,gap_tax,pro_rata_amount,
          pro_rata_tax_1,pro_rata_tax_2,opentrack_vendor,f_i_manager,sales_manager,
          trade_upfront,trade_capitalized,rebate_upfront,rebate_capitalized,
          cash_upfront,cash_capitalized,sign_and_drive,roll_price_flag,saved_price,
          critical_illness_term,loss_of_employment_term,credit_life_source,a_h_source,
          critical_illness_source,loss_of_employment_source,a_h_coverage_bmahcov,
          level_ci_premium,critical_illnes_cert_,loss_of_employment_cert_,
          critical_illness_reserve,loss_of_employment_reserve,vw_effective_apr,
          unrealized_rebate,a_h_insurance_term_date,ci_insurance_term_date,
          le_insurance_term_date,first_payment_waiver,trade_tax_credit,
          estimated_fees_and_taxes,mapped_tax_zip_code,mapped_tax_city,
          mapped_tax_location_id    
        from arkona.ext_bopmast
        where record_key = a.record_key
          and bopmast_company_number = a.bopmast_company_number) z)
from arkona.ext_bopmast a
where not exists (
  select 1
  from arkona.xfm_bopmast
  where bopmast_company_number = a.bopmast_company_number
    and record_key = a.record_key);    

-- changed rows
truncate arkona.xfm_bopmast_changed_rows;
insert into arkona.xfm_bopmast_changed_rows
select a.bopmast_company_number, a.record_key
from (
  select bopmast_company_number, record_key,
    (
      select md5(z::text) as hash
      from (
        select bopmast_company_number,record_key,record_status,record_type,vehicle_type,
          franchise_code,sale_type,bopmast_stock_number,bopmast_vin,buyer_number,
          co_buyer_number,bopmast_search_name,retail_price,down_payment,trade_allowance,
          trade_acv,trade_payoff,apr,term,insurance_term,days_to_1st_paymt,
          payment_frequency,payment,amount_financed,origination_date,date_1st_payment,
          date_last_payment,date_sticker_exp,tax_group,lending_source,leasing_source,
          insurance_source,gap_source,serv_cont_company,serv_cont_amount,serv_cont_deduct,
          serv_cont_miles,serv_cont_months,serv_cont_cost,serv_cont_str_dat,
          serv_cont_exp_dat,add_serv_to_cap_c,credit_life_premium,level_cl_premium,
          a_h_premium,gap_premium,gap_cost,credit_life_code,a_h_code,gap_code,tax_1,
          tax_2,tax_3,tax_4,tax_5,buy_rate,reserve_holdback_,number_of_trades,
          temporarysticker_,serv_cont_number,cred_life_cert_,a_h_cert_,gap_cert_,
          flooring_vendor,pd_insurance_co,pd_insur_co_alias,pd_policy_number,
          pd_policy_exp_dat,pd_agent_name,pd_agent_address,pd_agent_city,pd_agent_state,
          pd_agent_zip,pd_agent_phone_no,pd_coll_coverage,pd_comp_coverage,
          pd_fire_theft_cov,pd_coll_deduct,pd_comp_deduct,pd_fire_theft_ded,
          pd_vehicle_use,pd_premium,amo_total,fee_total,total_of_payments,
          interest_charged,vehicle_cost,internal_cost,adds_to_cost_tot,
          hold_back,pack_amount,comm_gross,incentive,rebate,odometer_at_sale,
          pickup_amount,pickup_beg_date,pickup_freq,pickup_no_pay,life_reserve,
          a_h_reserve,gap_reserve,pdi_reserve,amo_reserve,serv_reserve,
          finance_reserve,total_f_i_reserve,primary_salespers,secondary_slspers2,
          def_down_payment,def_down_due_date,loan_orig_fee,lof_override,effective_apr,
          msrp,lease_price,capitalized_cost,cap_cost_reduct,apr_bmlapr,lease_factor,
          lease_term,residual_,residual_amt,net_residual,tot_depreciation,
          miles_per_year,miles_year_allow,excess_mile_rate,excess_mile_rate2,
          excess_mile_chg,acquisition_fee,lease_payment,lease_paymt_tax,
          lease_sales_tax,mo_lease_charge,cap_reduction_tax,cap_reduct_tax_1,
          cap_reduct_tax_2,cap_reduct_tax_3,cap_reduct_tax_4,cap_reduct_tax_6,
          security_deposit,deposit_received,cash_received,factor_buy_rate,
          total_initial_chg,cap_lease_sls_tax,secur_depos_oride,acqui_fee_oride,
          fet_options,capitalize_sls_tx,adv_payment_lease,oride_tax_rate_1,
          oride_tax_rate_2,oride_tax_rate_3,oride_tax_rate_4,oride_tax_rate_6,
          oride_exc_trade_1,oride_exc_trade_2,oride_exc_trade_3,oride_exc_trade_4,
          tax_override_1,tax_override_2,tax_override_3,tax_override_4,tax_override_6,
          fin_reserv_oride,cost_override,holdback_override,tax_override,
          rebate_down_pay,contract_selected,chg_int_on_1_pymt,sale_account,
          unwind_flag,document_number,buyer_age,co_buyer_age,life_coverage,
          a_h_coverage,insur_term_date,lease_tax_group,co_buyer_life_prem,
          date_saved,date_trade_payoff,capitalize_cr_tax,rebate_as_down,total_down,
          tot_cap_reduction,gross_cap_cost,dealer_incentive,forms_printed,
          new_veh_deliv_rpt,date_approved,date_capped,excess_mileage_charge_ball,
          penalty_excess_mileage_charg,prepaid_excess_mileage_charg,
          miles_per_year_balloon,miles_per_year_allowed_bal,net_residual_balloon,
          residual_amount_balloon,residual_percent_balloon,after_market_tax,
          exempt_payments,full_payments,ford_plan_code,a_h_term,lease_payment_tax,
          trade_from_lease,orig_net_trade,ntax_trade_equity,pd_policy_number_b9pdp_,
          part_exempt_pymt,part_exempt_tax,service_cont_tax,total_a_h_coverag,
          tax_base_amount,work_in_process,date_last_lease_payment,
          aftermarket_tax_rate5,service_contract_tax_rate7,odd_last_payment,
          original_contract_date,level_life_optional,reg_fee_tax_1,reg_fee_tax_2,
          pymt_excluding_ins,payback_rate,buyer_red_rt_ind,co_buyer_red_rt_ind,
          buyer_gender,co_buyer_gender,crit_ill_code,crit_ill_premium,
          loss_of_employment_code,loss_of_employment_premium,upfront_fee_tax_1,
          upfront_fee_tax_2,residual_insurance,disability_elig,rem_ins_code,
          premium_percent,gap_tax_rate8,delivery_date,initial_miles,initial_mile_rate,
          initial_mile_chg,lease_pmts_remaining,f_i_gross,house_gross,monthly_gap_premium,
          initial_miles_chg_flag,initial_miles_threshold,gap_tax,pro_rata_amount,
          pro_rata_tax_1,pro_rata_tax_2,opentrack_vendor,f_i_manager,sales_manager,
          trade_upfront,trade_capitalized,rebate_upfront,rebate_capitalized,
          cash_upfront,cash_capitalized,sign_and_drive,roll_price_flag,saved_price,
          critical_illness_term,loss_of_employment_term,credit_life_source,a_h_source,
          critical_illness_source,loss_of_employment_source,a_h_coverage_bmahcov,
          level_ci_premium,critical_illnes_cert_,loss_of_employment_cert_,
          critical_illness_reserve,loss_of_employment_reserve,vw_effective_apr,
          unrealized_rebate,a_h_insurance_term_date,ci_insurance_term_date,
          le_insurance_term_date,first_payment_waiver,trade_tax_credit,
          estimated_fees_and_taxes,mapped_tax_zip_code,mapped_tax_city,
          mapped_tax_location_id    
        from arkona.ext_bopmast
        where record_key = aa.record_key
          and bopmast_company_number = aa.bopmast_company_number) z) as hash
  from arkona.ext_bopmast aa) a
join arkona.xfm_bopmast b on a.bopmast_company_number = b.bopmast_company_number
  and a.record_key = b.record_key
  and a.hash <> b.hash
  and b.current_row;  

-- update current version of changed row
update arkona.xfm_bopmast x
set current_Row = false,
    row_thru_date = current_date - 1
from arkona.xfm_bopmast_changed_rows z
where x.bopmast_company_number = z.bopmast_company_number
  and x.record_key = z.record_key
  and x.current_row = true;   

-- add new row for each changed row 
insert into arkona.xfm_bopmast (
  bopmast_company_number,record_key,record_status,record_type,vehicle_type,
  franchise_code,sale_type,bopmast_stock_number,bopmast_vin,buyer_number,
  co_buyer_number,bopmast_search_name,retail_price,down_payment,trade_allowance,
  trade_acv,trade_payoff,apr,term,insurance_term,days_to_1st_paymt,
  payment_frequency,payment,amount_financed,origination_date,date_1st_payment,
  date_last_payment,date_sticker_exp,tax_group,lending_source,leasing_source,
  insurance_source,gap_source,serv_cont_company,serv_cont_amount,serv_cont_deduct,
  serv_cont_miles,serv_cont_months,serv_cont_cost,serv_cont_str_dat,
  serv_cont_exp_dat,add_serv_to_cap_c,credit_life_premium,level_cl_premium,
  a_h_premium,gap_premium,gap_cost,credit_life_code,a_h_code,gap_code,tax_1,
  tax_2,tax_3,tax_4,tax_5,buy_rate,reserve_holdback_,number_of_trades,
  temporarysticker_,serv_cont_number,cred_life_cert_,a_h_cert_,gap_cert_,
  flooring_vendor,pd_insurance_co,pd_insur_co_alias,pd_policy_number,
  pd_policy_exp_dat,pd_agent_name,pd_agent_address,pd_agent_city,pd_agent_state,
  pd_agent_zip,pd_agent_phone_no,pd_coll_coverage,pd_comp_coverage,
  pd_fire_theft_cov,pd_coll_deduct,pd_comp_deduct,pd_fire_theft_ded,
  pd_vehicle_use,pd_premium,amo_total,fee_total,total_of_payments,
  interest_charged,vehicle_cost,internal_cost,adds_to_cost_tot,
  hold_back,pack_amount,comm_gross,incentive,rebate,odometer_at_sale,
  pickup_amount,pickup_beg_date,pickup_freq,pickup_no_pay,life_reserve,
  a_h_reserve,gap_reserve,pdi_reserve,amo_reserve,serv_reserve,
  finance_reserve,total_f_i_reserve,primary_salespers,secondary_slspers2,
  def_down_payment,def_down_due_date,loan_orig_fee,lof_override,effective_apr,
  msrp,lease_price,capitalized_cost,cap_cost_reduct,apr_bmlapr,lease_factor,
  lease_term,residual_,residual_amt,net_residual,tot_depreciation,
  miles_per_year,miles_year_allow,excess_mile_rate,excess_mile_rate2,
  excess_mile_chg,acquisition_fee,lease_payment,lease_paymt_tax,
  lease_sales_tax,mo_lease_charge,cap_reduction_tax,cap_reduct_tax_1,
  cap_reduct_tax_2,cap_reduct_tax_3,cap_reduct_tax_4,cap_reduct_tax_6,
  security_deposit,deposit_received,cash_received,factor_buy_rate,
  total_initial_chg,cap_lease_sls_tax,secur_depos_oride,acqui_fee_oride,
  fet_options,capitalize_sls_tx,adv_payment_lease,oride_tax_rate_1,
  oride_tax_rate_2,oride_tax_rate_3,oride_tax_rate_4,oride_tax_rate_6,
  oride_exc_trade_1,oride_exc_trade_2,oride_exc_trade_3,oride_exc_trade_4,
  tax_override_1,tax_override_2,tax_override_3,tax_override_4,tax_override_6,
  fin_reserv_oride,cost_override,holdback_override,tax_override,
  rebate_down_pay,contract_selected,chg_int_on_1_pymt,sale_account,
  unwind_flag,document_number,buyer_age,co_buyer_age,life_coverage,
  a_h_coverage,insur_term_date,lease_tax_group,co_buyer_life_prem,
  date_saved,date_trade_payoff,capitalize_cr_tax,rebate_as_down,total_down,
  tot_cap_reduction,gross_cap_cost,dealer_incentive,forms_printed,
  new_veh_deliv_rpt,date_approved,date_capped,excess_mileage_charge_ball,
  penalty_excess_mileage_charg,prepaid_excess_mileage_charg,
  miles_per_year_balloon,miles_per_year_allowed_bal,net_residual_balloon,
  residual_amount_balloon,residual_percent_balloon,after_market_tax,
  exempt_payments,full_payments,ford_plan_code,a_h_term,lease_payment_tax,
  trade_from_lease,orig_net_trade,ntax_trade_equity,pd_policy_number_b9pdp_,
  part_exempt_pymt,part_exempt_tax,service_cont_tax,total_a_h_coverag,
  tax_base_amount,work_in_process,date_last_lease_payment,
  aftermarket_tax_rate5,service_contract_tax_rate7,odd_last_payment,
  original_contract_date,level_life_optional,reg_fee_tax_1,reg_fee_tax_2,
  pymt_excluding_ins,payback_rate,buyer_red_rt_ind,co_buyer_red_rt_ind,
  buyer_gender,co_buyer_gender,crit_ill_code,crit_ill_premium,
  loss_of_employment_code,loss_of_employment_premium,upfront_fee_tax_1,
  upfront_fee_tax_2,residual_insurance,disability_elig,rem_ins_code,
  premium_percent,gap_tax_rate8,delivery_date,initial_miles,initial_mile_rate,
  initial_mile_chg,lease_pmts_remaining,f_i_gross,house_gross,monthly_gap_premium,
  initial_miles_chg_flag,initial_miles_threshold,gap_tax,pro_rata_amount,
  pro_rata_tax_1,pro_rata_tax_2,opentrack_vendor,f_i_manager,sales_manager,
  trade_upfront,trade_capitalized,rebate_upfront,rebate_capitalized,
  cash_upfront,cash_capitalized,sign_and_drive,roll_price_flag,saved_price,
  critical_illness_term,loss_of_employment_term,credit_life_source,a_h_source,
  critical_illness_source,loss_of_employment_source,a_h_coverage_bmahcov,
  level_ci_premium,critical_illnes_cert_,loss_of_employment_cert_,
  critical_illness_reserve,loss_of_employment_reserve,vw_effective_apr,
  unrealized_rebate,a_h_insurance_term_date,ci_insurance_term_date,
  le_insurance_term_date,first_payment_waiver,trade_tax_credit,
  estimated_fees_and_taxes,mapped_tax_zip_code,mapped_tax_city,
  mapped_tax_location_id,
  row_from_date, current_row, hash)
select bopmast_company_number,record_key,record_status,record_type,vehicle_type,
  franchise_code,sale_type,bopmast_stock_number,bopmast_vin,buyer_number,
  co_buyer_number,bopmast_search_name,retail_price,down_payment,trade_allowance,
  trade_acv,trade_payoff,apr,term,insurance_term,days_to_1st_paymt,
  payment_frequency,payment,amount_financed,origination_date,date_1st_payment,
  date_last_payment,date_sticker_exp,tax_group,lending_source,leasing_source,
  insurance_source,gap_source,serv_cont_company,serv_cont_amount,serv_cont_deduct,
  serv_cont_miles,serv_cont_months,serv_cont_cost,serv_cont_str_dat,
  serv_cont_exp_dat,add_serv_to_cap_c,credit_life_premium,level_cl_premium,
  a_h_premium,gap_premium,gap_cost,credit_life_code,a_h_code,gap_code,tax_1,
  tax_2,tax_3,tax_4,tax_5,buy_rate,reserve_holdback_,number_of_trades,
  temporarysticker_,serv_cont_number,cred_life_cert_,a_h_cert_,gap_cert_,
  flooring_vendor,pd_insurance_co,pd_insur_co_alias,pd_policy_number,
  pd_policy_exp_dat,pd_agent_name,pd_agent_address,pd_agent_city,pd_agent_state,
  pd_agent_zip,pd_agent_phone_no,pd_coll_coverage,pd_comp_coverage,
  pd_fire_theft_cov,pd_coll_deduct,pd_comp_deduct,pd_fire_theft_ded,
  pd_vehicle_use,pd_premium,amo_total,fee_total,total_of_payments,
  interest_charged,vehicle_cost,internal_cost,adds_to_cost_tot,
  hold_back,pack_amount,comm_gross,incentive,rebate,odometer_at_sale,
  pickup_amount,pickup_beg_date,pickup_freq,pickup_no_pay,life_reserve,
  a_h_reserve,gap_reserve,pdi_reserve,amo_reserve,serv_reserve,
  finance_reserve,total_f_i_reserve,primary_salespers,secondary_slspers2,
  def_down_payment,def_down_due_date,loan_orig_fee,lof_override,effective_apr,
  msrp,lease_price,capitalized_cost,cap_cost_reduct,apr_bmlapr,lease_factor,
  lease_term,residual_,residual_amt,net_residual,tot_depreciation,
  miles_per_year,miles_year_allow,excess_mile_rate,excess_mile_rate2,
  excess_mile_chg,acquisition_fee,lease_payment,lease_paymt_tax,
  lease_sales_tax,mo_lease_charge,cap_reduction_tax,cap_reduct_tax_1,
  cap_reduct_tax_2,cap_reduct_tax_3,cap_reduct_tax_4,cap_reduct_tax_6,
  security_deposit,deposit_received,cash_received,factor_buy_rate,
  total_initial_chg,cap_lease_sls_tax,secur_depos_oride,acqui_fee_oride,
  fet_options,capitalize_sls_tx,adv_payment_lease,oride_tax_rate_1,
  oride_tax_rate_2,oride_tax_rate_3,oride_tax_rate_4,oride_tax_rate_6,
  oride_exc_trade_1,oride_exc_trade_2,oride_exc_trade_3,oride_exc_trade_4,
  tax_override_1,tax_override_2,tax_override_3,tax_override_4,tax_override_6,
  fin_reserv_oride,cost_override,holdback_override,tax_override,
  rebate_down_pay,contract_selected,chg_int_on_1_pymt,sale_account,
  unwind_flag,document_number,buyer_age,co_buyer_age,life_coverage,
  a_h_coverage,insur_term_date,lease_tax_group,co_buyer_life_prem,
  date_saved,date_trade_payoff,capitalize_cr_tax,rebate_as_down,total_down,
  tot_cap_reduction,gross_cap_cost,dealer_incentive,forms_printed,
  new_veh_deliv_rpt,date_approved,date_capped,excess_mileage_charge_ball,
  penalty_excess_mileage_charg,prepaid_excess_mileage_charg,
  miles_per_year_balloon,miles_per_year_allowed_bal,net_residual_balloon,
  residual_amount_balloon,residual_percent_balloon,after_market_tax,
  exempt_payments,full_payments,ford_plan_code,a_h_term,lease_payment_tax,
  trade_from_lease,orig_net_trade,ntax_trade_equity,pd_policy_number_b9pdp_,
  part_exempt_pymt,part_exempt_tax,service_cont_tax,total_a_h_coverag,
  tax_base_amount,work_in_process,date_last_lease_payment,
  aftermarket_tax_rate5,service_contract_tax_rate7,odd_last_payment,
  original_contract_date,level_life_optional,reg_fee_tax_1,reg_fee_tax_2,
  pymt_excluding_ins,payback_rate,buyer_red_rt_ind,co_buyer_red_rt_ind,
  buyer_gender,co_buyer_gender,crit_ill_code,crit_ill_premium,
  loss_of_employment_code,loss_of_employment_premium,upfront_fee_tax_1,
  upfront_fee_tax_2,residual_insurance,disability_elig,rem_ins_code,
  premium_percent,gap_tax_rate8,delivery_date,initial_miles,initial_mile_rate,
  initial_mile_chg,lease_pmts_remaining,f_i_gross,house_gross,monthly_gap_premium,
  initial_miles_chg_flag,initial_miles_threshold,gap_tax,pro_rata_amount,
  pro_rata_tax_1,pro_rata_tax_2,opentrack_vendor,f_i_manager,sales_manager,
  trade_upfront,trade_capitalized,rebate_upfront,rebate_capitalized,
  cash_upfront,cash_capitalized,sign_and_drive,roll_price_flag,saved_price,
  critical_illness_term,loss_of_employment_term,credit_life_source,a_h_source,
  critical_illness_source,loss_of_employment_source,a_h_coverage_bmahcov,
  level_ci_premium,critical_illnes_cert_,loss_of_employment_cert_,
  critical_illness_reserve,loss_of_employment_reserve,vw_effective_apr,
  unrealized_rebate,a_h_insurance_term_date,ci_insurance_term_date,
  le_insurance_term_date,first_payment_waiver,trade_tax_credit,
  estimated_fees_and_taxes,mapped_tax_zip_code,mapped_tax_city,
  mapped_tax_location_id,
  '01/11/2019' as row_from_date, true as current_row,
    (
      select md5(z::text) as hash
      from (
        select bopmast_company_number,record_key,record_status,record_type,vehicle_type,
          franchise_code,sale_type,bopmast_stock_number,bopmast_vin,buyer_number,
          co_buyer_number,bopmast_search_name,retail_price,down_payment,trade_allowance,
          trade_acv,trade_payoff,apr,term,insurance_term,days_to_1st_paymt,
          payment_frequency,payment,amount_financed,origination_date,date_1st_payment,
          date_last_payment,date_sticker_exp,tax_group,lending_source,leasing_source,
          insurance_source,gap_source,serv_cont_company,serv_cont_amount,serv_cont_deduct,
          serv_cont_miles,serv_cont_months,serv_cont_cost,serv_cont_str_dat,
          serv_cont_exp_dat,add_serv_to_cap_c,credit_life_premium,level_cl_premium,
          a_h_premium,gap_premium,gap_cost,credit_life_code,a_h_code,gap_code,tax_1,
          tax_2,tax_3,tax_4,tax_5,buy_rate,reserve_holdback_,number_of_trades,
          temporarysticker_,serv_cont_number,cred_life_cert_,a_h_cert_,gap_cert_,
          flooring_vendor,pd_insurance_co,pd_insur_co_alias,pd_policy_number,
          pd_policy_exp_dat,pd_agent_name,pd_agent_address,pd_agent_city,pd_agent_state,
          pd_agent_zip,pd_agent_phone_no,pd_coll_coverage,pd_comp_coverage,
          pd_fire_theft_cov,pd_coll_deduct,pd_comp_deduct,pd_fire_theft_ded,
          pd_vehicle_use,pd_premium,amo_total,fee_total,total_of_payments,
          interest_charged,vehicle_cost,internal_cost,adds_to_cost_tot,
          hold_back,pack_amount,comm_gross,incentive,rebate,odometer_at_sale,
          pickup_amount,pickup_beg_date,pickup_freq,pickup_no_pay,life_reserve,
          a_h_reserve,gap_reserve,pdi_reserve,amo_reserve,serv_reserve,
          finance_reserve,total_f_i_reserve,primary_salespers,secondary_slspers2,
          def_down_payment,def_down_due_date,loan_orig_fee,lof_override,effective_apr,
          msrp,lease_price,capitalized_cost,cap_cost_reduct,apr_bmlapr,lease_factor,
          lease_term,residual_,residual_amt,net_residual,tot_depreciation,
          miles_per_year,miles_year_allow,excess_mile_rate,excess_mile_rate2,
          excess_mile_chg,acquisition_fee,lease_payment,lease_paymt_tax,
          lease_sales_tax,mo_lease_charge,cap_reduction_tax,cap_reduct_tax_1,
          cap_reduct_tax_2,cap_reduct_tax_3,cap_reduct_tax_4,cap_reduct_tax_6,
          security_deposit,deposit_received,cash_received,factor_buy_rate,
          total_initial_chg,cap_lease_sls_tax,secur_depos_oride,acqui_fee_oride,
          fet_options,capitalize_sls_tx,adv_payment_lease,oride_tax_rate_1,
          oride_tax_rate_2,oride_tax_rate_3,oride_tax_rate_4,oride_tax_rate_6,
          oride_exc_trade_1,oride_exc_trade_2,oride_exc_trade_3,oride_exc_trade_4,
          tax_override_1,tax_override_2,tax_override_3,tax_override_4,tax_override_6,
          fin_reserv_oride,cost_override,holdback_override,tax_override,
          rebate_down_pay,contract_selected,chg_int_on_1_pymt,sale_account,
          unwind_flag,document_number,buyer_age,co_buyer_age,life_coverage,
          a_h_coverage,insur_term_date,lease_tax_group,co_buyer_life_prem,
          date_saved,date_trade_payoff,capitalize_cr_tax,rebate_as_down,total_down,
          tot_cap_reduction,gross_cap_cost,dealer_incentive,forms_printed,
          new_veh_deliv_rpt,date_approved,date_capped,excess_mileage_charge_ball,
          penalty_excess_mileage_charg,prepaid_excess_mileage_charg,
          miles_per_year_balloon,miles_per_year_allowed_bal,net_residual_balloon,
          residual_amount_balloon,residual_percent_balloon,after_market_tax,
          exempt_payments,full_payments,ford_plan_code,a_h_term,lease_payment_tax,
          trade_from_lease,orig_net_trade,ntax_trade_equity,pd_policy_number_b9pdp_,
          part_exempt_pymt,part_exempt_tax,service_cont_tax,total_a_h_coverag,
          tax_base_amount,work_in_process,date_last_lease_payment,
          aftermarket_tax_rate5,service_contract_tax_rate7,odd_last_payment,
          original_contract_date,level_life_optional,reg_fee_tax_1,reg_fee_tax_2,
          pymt_excluding_ins,payback_rate,buyer_red_rt_ind,co_buyer_red_rt_ind,
          buyer_gender,co_buyer_gender,crit_ill_code,crit_ill_premium,
          loss_of_employment_code,loss_of_employment_premium,upfront_fee_tax_1,
          upfront_fee_tax_2,residual_insurance,disability_elig,rem_ins_code,
          premium_percent,gap_tax_rate8,delivery_date,initial_miles,initial_mile_rate,
          initial_mile_chg,lease_pmts_remaining,f_i_gross,house_gross,monthly_gap_premium,
          initial_miles_chg_flag,initial_miles_threshold,gap_tax,pro_rata_amount,
          pro_rata_tax_1,pro_rata_tax_2,opentrack_vendor,f_i_manager,sales_manager,
          trade_upfront,trade_capitalized,rebate_upfront,rebate_capitalized,
          cash_upfront,cash_capitalized,sign_and_drive,roll_price_flag,saved_price,
          critical_illness_term,loss_of_employment_term,credit_life_source,a_h_source,
          critical_illness_source,loss_of_employment_source,a_h_coverage_bmahcov,
          level_ci_premium,critical_illnes_cert_,loss_of_employment_cert_,
          critical_illness_reserve,loss_of_employment_reserve,vw_effective_apr,
          unrealized_rebate,a_h_insurance_term_date,ci_insurance_term_date,
          le_insurance_term_date,first_payment_waiver,trade_tax_credit,
          estimated_fees_and_taxes,mapped_tax_zip_code,mapped_tax_city,
          mapped_tax_location_id    
        from arkona.ext_bopmast
        where record_key = x.record_key
          and bopmast_company_number = x.bopmast_company_number) z)
from (
  select a.*
  from arkona.ext_bopmast a
  join arkona.xfm_bopmast_changed_rows b on a.bopmast_company_number = b.bopmast_company_number
    and a.record_key = b.record_key) x;

         
-- deleted deals
update arkona.xfm_bopmast 
set date_deleted = '01/11/2019'
where bopmast_key in (
  select bopmast_key
  from arkona.xfm_bopmast a
  where current_row
    and not exists (
      select 1
      from arkona.ext_bopmast
      where bopmast_company_number = a.bopmast_company_number
        and record_key = a.record_key));








    