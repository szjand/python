﻿DROP TABLE IF EXISTS arkona.ext_inpcmnt;
CREATE TABLE IF NOT EXISTS arkona.ext_inpcmnt(
    COMPANY_NUMBER CITEXT,
    VIN CITEXT,
    TRANSACTION_DATE INTEGER,
    TRANSACTION_TIME INTEGER,
    SEQUENCE_NUMBER INTEGER,
    COMMENT CITEXT)
WITH (OIDS=FALSE);
COMMENT ON COLUMN arkona.ext_INPCMNT.COMPANY_NUMBER IS 'INCO# : Company Number';
COMMENT ON COLUMN arkona.ext_INPCMNT.VIN IS 'INVIN : VIN';
COMMENT ON COLUMN arkona.ext_INPCMNT.TRANSACTION_DATE IS 'INDATE : Transaction Date';
COMMENT ON COLUMN arkona.ext_INPCMNT.TRANSACTION_TIME IS 'INTIME : Transaction Time';
COMMENT ON COLUMN arkona.ext_INPCMNT.SEQUENCE_NUMBER IS 'INSEQ# : Sequence Number';
COMMENT ON COLUMN arkona.ext_INPCMNT.COMMENT IS 'INCMNT : Comment';
-- {TRIM(COMPANY_NUMBER),TRIM(VIN),TRANSACTION_DATE,TRANSACTION_TIME,SEQUENCE_NUMBER,TRIM(COMMENT)}


select *
from arkona.ext_inpcmnt
where transaction_date > 20170000 
--   and comment like '%change:%'
  and comment like 'Stock# change:%'

select *
from arkona.xfm_inpmast
where inpmast_vin = '1G11C5SL4FF268774'
where inpmast_stock_number = '25046'

create index on arkona.xfm_inpmast(inpmast_stock_number);
create index on arkona.xfm_inpmast(inpmast_vin);

-- vins with multiple stock numbers
select a.inpmast_vin, a.inpmast_stock_number
from arkona.xfm_inpmast a
inner join (
  select inpmast_vin
  from (
    select inpmast_vin, inpmast_stock_number
    from arkona.xfm_inpmast
    group by inpmast_vin, inpmast_stock_number) a
  group by inpmast_vin
  having count(*) > 1) b  on a.inpmast_vin = b.inpmast_vin
group by a.inpmast_vin, a.inpmast_stock_number 



select *
from arkona.xfm_inpmast
where inpmast_vin = '1GTDT196X48169441'


select *
from arkona.ext_bopvref
where vin = '1GTDT196X48169441'
