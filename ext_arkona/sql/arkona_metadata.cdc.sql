﻿select table_name, trim(coalesce(table_text, '')) 
from dds.ext_qsys2_systables a
except
select table_name, trim(coalesce(table_text, ''))  
from dds.ext_qsys2_systables_tmp b


select table_name, trim(coalesce(table_text, ''))  
from dds.ext_qsys2_systables_tmp c
except
select table_name, trim(coalesce(table_text, '')) 
from dds.ext_qsys2_systables d


select 'tmp', c.* 
from dds.ext_qsys2_systables_tmp c
where table_name in ('PDPIHDV',
'OEMXLOG',
'PDPSPDTX',
'PDPIAME',
'PDPTRRT',
'PDPPHDV')
union
select 'orig', d.* 
from dds.ext_qsys2_systables d
where table_name in ('PDPIHDV',
'OEMXLOG',
'PDPSPDTX',
'PDPIAME',
'PDPTRRT',
'PDPPHDV')

select * from dds.ext_qsys2_systables where table_name = 'PDPPHDV'

select * from dds.ext_qsys2_systables_tmp where table_name = 'PDPPHDV'

update dds.ext_qsys2_systables
set table_text = (
  select table_text 
  from dds.ext_qsys2_systables_tmp 
  where table_name = 'PDPPHDV')
where table_name = 'PDPPHDV'

update dds.ext_qsys2_systables 
set table_name = trim(table_name),
    table_text = trim(table_text);

update dds.ext_qsys2_systables
set table_text = coalesce(table_text, '');    


select * from dds.ext_qsys2_systables_tmp where table_text is null    

select *
from dds.ext_qsys2_systables a
except
select *
from dds.ext_qsys2_systables_tmp b

-- ok, now that i've got the fucking trim/null/empty string shit consistent
-- these are the new tables to add
insert into dds.ext_qsys2_systables (table_name, table_text)
select *
from dds.ext_qsys2_systables_tmp c
except
select *
from dds.ext_qsys2_systables d;

-- now on to syscolumns

select *
from dds.ext_qsys2_syscolumns
where column_Text is null 

update dds.ext_qsys2_syscolumns
set table_name = trim(table_name),
    system_column_name = trim(system_column_name),
    column_name = trim(column_name)
    
update dds.ext_qsys2_syscolumns_tmp
set column_text = ''
where column_text is null;

drop table if exists changes;
create temp table changes as
select table_name,system_column_name,column_name,ordinal_position,data_type,
  length,coalesce(numeric_scale, 0),column_text
from dds.ext_qsys2_syscolumns a
except
select table_name,system_column_name,column_name,ordinal_position,data_type,
  length,coalesce(numeric_scale, 0),column_text
from dds.ext_qsys2_syscolumns_tmp b;

drop table if exists new_rows;
create temp table new_rows as
select table_name,system_column_name,column_name,ordinal_position,data_type,
  length,coalesce(numeric_scale, 0),column_text
from dds.ext_qsys2_syscolumns_tmp c
except
select table_name,system_column_name,column_name,ordinal_position,data_type,
  length,coalesce(numeric_scale, 0),column_text
from dds.ext_qsys2_syscolumns d;

select 'tmp' as source, a.* 
from dds.ext_qsys2_syscolumns_tmp a
inner join changes b on a.table_name = b.table_name 
  and a.system_column_name = b.system_column_name
union  
select 'orig', a.* 
from dds.ext_qsys2_syscolumns a
inner join changes b on a.table_name = b.table_name 
  and a.system_column_name = b.system_column_name
order by table_name, system_column_name, source


select table_name,trim(system_column_name)--,column_name,ordinal_position,data_type,
--  length,coalesce(numeric_scale, 0),column_text
from dds.ext_qsys2_syscolumns_tmp c
except
select table_name,trim(system_column_name)--,column_name,ordinal_position,data_type,
--  length,coalesce(numeric_scale, 0),column_text
from dds.ext_qsys2_syscolumns d;