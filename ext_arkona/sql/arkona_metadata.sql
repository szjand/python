﻿do i need systables?
yes, for description andd for new or deleted tables

drop table if exists dds.ext_qsys2_systables cascade;
CREATE TABLE dds.ext_qsys2_systables(
  table_name citext primary key,
  table_text citext);

select * from dds.ext_qsys2_systables

drop table if exists dds.ext_qsys2_syscolumns cascade;
CREATE TABLE dds.ext_qsys2_syscolumns (
  table_name citext references dds.ext_qsys2_systables (table_name),
  system_column_name citext not null,
  column_name citext not null ,
  ordinal_position integer not null,
  data_type citext not null,
  length integer,
  numeric_scale integer,
  column_text citext,
  constraint ext_qsys2_syscolumns_pk primary key (table_name, system_column_name));

alter table dds.ext_qsys2_syscolumns add constraint ext_qsys2_syscolumns_fk foreign key (table_name) references dds.ext_qsys2_systables (table_name);

select system_column_name from dds.ext_qsys2_syscolumns group by system_column_name having count(*) > 1

delete from dds.ext_qsys2_syscolumns


drop table if exists dds.ext_qsys2_syscst;
create table dds.ext_qsys2_syscst (
  constraint_name citext primary key,
  table_name citext references dds.ext_qsys2_systables not null,
  constraint_keys integer);

delete from dds.ext_qsys2_syscst;  

drop table if exists dds.ext_qsys2_syscstcol;
create table dds.ext_qsys2_syscstcol (
  constraint_name citext references dds.ext_qsys2_syscst (constraint_name) not null,
  column_name citext not null,
  constraint ext_qsys2_syscstcol_pk primary key (constraint_name, column_name));


  
select * 
from dds.ext_qsys2_systables a
left join dds.ext_qsys2_syscolumns b on a.table_name = b.table_name
where a.table_name = 'glpmast'
order by b.ordinal_position

select column_name
from (
  select column_name, system_column_name
  from dds.ext_qsys2_syscolumns 
  group by column_name, system_column_name) a
group by column_name having count(*) > 1  

select *
from dds.ext_qsys2_syscolumns
where column_name = 'customer'


select table_name, column_name
from dds.ext_qsys2_syscolumns
group by table_name, column_name
having count(*) > 1

/*
6/15/16
  the whole notion of a fully normalized metadata structure is more a 
  distraction than a necessity at this time
  neither column_names nor system_column_names are unique alone, only in combination with table_name, then both are
  bring over the the ddl function from ubuntu server for generating postgres ddl from syscolumns

  still it is worthwhile checking to see if the db2 table has a PK
*/


-- generates postgress ddl from db2 metadata
-- decimal/numeric with scale = 0 to integer
-- decimal in db2 -> numeric in postgres
-- converting a numeric/decimal to integer and length > 9, -> bigint rather than int
-- uses column_names, includes system_column_names in comment

-- 6/6/17: pymast changed, the python script populates dds.ext_qsys2_syscolumns_tmp, changed this script
-- to read from that table
do
$$
declare 
  _row RECORD;
  _last_field INTEGER;
  _schema_prefix citext;
  _str citext;
  _table_name citext;
begin
  drop table if exists _ddl ;
  _schema_prefix := 'arkona.ext_';  ---------------------------------------------------------------------
  _table_name = 'SDPSVCD'; ---------------------------------------------------------------------------
  _last_field = (
    SELECT max(ordinal_position)
    FROM arkona.ext_qsys2_syscolumns_tmp
    WHERE lower(table_name) = lower(_table_name));
  _str = 'DROP TABLE IF EXISTS ' 
    || _schema_prefix     
    || _table_name 
    || ';' || CHR(10);    
  _str = _str ||'CREATE TABLE IF NOT EXISTS ' 
    || _schema_prefix  
    || _table_name 
    || '(' || CHR(10);
  FOR _row in
    SELECT table_name, replace(column_name, '#','_') as column_name,
      ordinal_position, data_type, length, numeric_scale
    FROM arkona.ext_qsys2_syscolumns_tmp a 
    WHERE lower(table_name) = lower(_table_name) 
    ORDER BY ordinal_position
  LOOP
    CASE 
      WHEN lower(_row.data_type) IN ('char','varchar','clob','blob','binary') THEN _str = _str 
        || '    ' || _row.column_name 
        || ' CITEXT';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;           
      WHEN lower(_row.data_type) = 'decimal' and _row.numeric_scale = 0 and _row.length > 9 THEN _str = _str 
        || '    ' || _row.column_name 
        || ' BIGINT';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;  
      WHEN lower(_row.data_type) = 'decimal' and _row.numeric_scale = 0 and _row.length < 10 THEN _str = _str 
        || '    ' || _row.column_name 
        || ' INTEGER';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;                
      WHEN lower(_row.data_type) = 'decimal' and _row.numeric_scale <> 0 THEN _str = _str 
        || '    ' || _row.column_name 
        || ' NUMERIC (' || _row.length || ',' || _row.numeric_scale || ')';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;         
      WHEN lower(_row.data_type) in ('integer','smallint','bigint') THEN _str = _str 
        || '    ' || _row.column_name 
        || ' INTEGER';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;          
      WHEN lower(_row.data_type) = 'numeric' and _row.numeric_scale = 0 and _row.length > 9 THEN _str = _str 
        || '    ' || _row.column_name 
        || ' BIGINT';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF; 
      WHEN lower(_row.data_type) = 'numeric' and _row.numeric_scale = 0 and _row.length < 10 THEN _str = _str 
        || '    ' || _row.column_name 
        || ' INTEGER';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;         
      WHEN lower(_row.data_type) = 'numeric'  and _row.numeric_scale <> 0 THEN _str = _str 
        || '    ' || _row.column_name 
        || ' NUMERIC (' || _row.length || ',' || _row.numeric_scale || ')';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;                 
      WHEN lower(_row.data_type) = 'date' THEN _str = _str 
        || '    ' || _row.column_name 
        || ' DATE';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF; 
      WHEN lower(_row.data_type) = 'timestmp' THEN _str = _str 
        || '    ' || _row.column_name 
        || ' TIMESTAMPTZ'; 
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;        
      WHEN lower(_row.data_type) = 'time' THEN _str = _str 
        || '    ' || _row.column_name 
        || ' TIME'; 
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;                                         
    END CASE;
  END LOOP;

  FOR _row in
    SELECT table_name, column_name, system_column_name, column_text,
      ordinal_position, data_type, length, numeric_scale
    FROM arkona.ext_qsys2_syscolumns_tmp a 
    WHERE lower(table_name) = lower(_table_name) 
    ORDER BY ordinal_position
  LOOP 
    _str = _str || 'COMMENT ON COLUMN ' || _schema_prefix ||  _row.table_name     
      || '.' || replace(_row.column_name, '#', '_') || ' IS ''' 
      || trim(_row.system_column_name) 
      || ' : ' || trim(coalesce(_row.column_text, 'None'))
      || ''';' || CHR(10);
  END LOOP;  
  -- generates a single string of column names for the select (from db2) statement:
  _str = (
    select _str || '-- ' || array_agg(column_name)::citext
    from (
    select
      case
        when data_type = 'CHAR' then 'TRIM(' || column_name || ')'
        else column_name
      end as column_name
    from arkona.ext_qsys2_syscolumns_tmp
    where table_name = _table_name
    order by ordinal_position) x);    

  create temp table _ddl as select _str;
end
$$;

select * from _ddl;  
    

