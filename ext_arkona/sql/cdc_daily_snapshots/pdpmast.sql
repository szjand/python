﻿create schema arkona_cdc;
comment on schema arkona_cdc is 'schema for daily snapshots of arkona tables, starting with only pdpmast, where tables will be named like: arkona_cdc.pdpmast_20200206, 
the table name indicating the day on which the snapshot was taken. populated nightly in luigi';

insert into arkona_cdc.pdpmast_20200206
select * from arkona.ext_pdpmast;

select count(*) from arkona_cdc.pdpmast_20200206;

CREATE TABLE arkona_cdc.pdpmast_20200206
(
  company_number citext NOT NULL, -- PMCO# : Company Number
  manufacturer citext NOT NULL, -- PMMANF : Manufacturer
  part_number citext NOT NULL, -- PMPART : Part Number
  part_description citext, -- PMDESC : Part Description
  stocking_group citext, -- PMSGRP : Stocking Group
  status citext, -- PMSTAT : Status
  obsolete citext, -- PMOBSL : Obsolete
  return_code citext, -- PMRTRN : Return Code
  arrival_code citext, -- PMARRV : Arrival Code
  stock_promo_code citext, -- PMSTKO : Stock Promo Code
  spec_order_code citext, -- PMSPOC : Spec Order Code
  price_update citext, -- PMPUPD : Price Update
  kit_part citext, -- PMKIT : Kit Part
  component_part citext, -- PMCMPN : Component Part
  assoc_with_part citext, -- PMASSC : Assoc With Part
  alternate_part citext, -- PMALTN : Alternate Part
  is_core_part citext, -- PMCORE : Is Core Part
  merch_code citext, -- PMMERC : Merch Code
  price_symbol citext, -- PMPSYM : Price Symbol
  prod_code_class citext, -- PMPRDC : Prod Code/Class
  fleet_allowance citext, -- PMFLTA : Fleet Allowance
  group_code citext, -- PMGRPC : Group Code
  bin_location citext, -- PMBINL : Bin Location
  shelf_locatiion citext, -- PMSHLF : Shelf Locatiion
  qty_on_hand integer, -- PMONHD : Qty On Hand
  qty_reserved integer, -- PMRSRV : Qty Reserved
  qty_on_order integer, -- PMORDR : Qty On Order
  qty_on_back_order integer, -- PMBCKO : Qty On Back Order
  qty_on_spec_order integer, -- PMSPOR : Qty On Spec Order
  place_on_order integer, -- PMPORD : Place on Order
  place_on_spec_ord integer, -- PMPSPO : Place on Spec Ord
  pack_qty integer, -- PMPACK : Pack Qty
  min_sales_qty integer, -- PMMQTY : Min Sales Qty
  cost numeric(9,2), -- PMCOST : Cost
  list_price numeric(9,2), -- PMLIST : List Price
  trade_price numeric(9,2), -- PMTRAD : Trade Price
  whlse_comp numeric(9,2), -- PMCOMP : Whlse Comp
  whlse_comp_fleet numeric(9,2), -- PMCOMPF : Whlse Comp Fleet
  flat_price numeric(9,2), -- PMFPRC : Flat Price
  min_max_pack_adj integer, -- PMLDTM : Min,Max,Pack Adj
  dynam_days_supply integer, -- PMRSTK : Dynam Days Supply
  minimum_on_hand integer, -- PMMIN : Minimum On Hand
  maximum_on_hand integer, -- PMMAX : Maximum On Hand
  low_model_year integer, -- PMLMYR : Low Model Year
  high_model_year integer, -- PMHMYR : High Model Year
  date_in_inventory integer, -- PMDTIN : Date In Inventory
  date_phased_out integer, -- PMDTPO : Date Phased Out
  date_last_sold integer, -- PMDTLS : Date Last Sold
  inventory_turns numeric(5,1), -- PMTRNS : Inventory Turns
  best_stock_level integer, -- PMBSLV : Best Stock Level
  stock_status integer, -- PMSTKS : Stock Status
  recent_demand integer, -- PMRDMD : Recent Demand
  prior_demand integer, -- PMPDMD : Prior Demand
  recent_work_days numeric(4,1), -- PMRWDY : Recent Work Days
  prior_work_days numeric(4,1), -- PMPWDY : Prior Work Days
  weighted_dly_avg numeric(7,3), -- PMWDAV : Weighted Dly Avg
  sales_demand integer, -- PMSADM : Sales Demand
  lost_sales_demand integer, -- PMLSDM : Lost Sales Demand
  spec_order_demand integer, -- PMSODM : Spec Order Demand
  non_stock_demand integer, -- PMNSDM : Non Stock Demand
  return_demand integer, -- PMRTDM : Return Demand
  inventory_value numeric(9,2), -- PMIVAL : Inventory Value
  tot_qty_sold integer, -- PMTQTY : Tot Qty Sold
  tot_cost_sales numeric(11,2), -- PMCSAL : Tot Cost Sales
  stock_purchases numeric(11,2), -- PMSTKP : Stock Purchases
  display_part_no_ citext, -- PMDPRT : Display Part No.
  sort_part_number citext, -- PMSPRT : Sort Part Number
  old_part_number citext, -- PMOPRT : Old Part Number
  new_part_number citext, -- PMNPRT : New Part Number
  core_part_number citext, -- PMCPRT : Core Part Number
  remarks citext, -- PMRMRK : Remarks
  display_remarks citext, -- PMDSPR : Display Remarks
  bulk_item_actual_weight integer, -- PMBIAW : Bulk item actual weight
  secondary_bin_location2 citext, -- PMBIN2 : Secondary bin location
  class_code citext, -- PMCLCD : Class code
  part_dealer_to_dealer_price numeric(7,2), -- PMCOMPD : Part dealer to dealer price
  part_subsidiary_price numeric(7,2), -- PMCOMPS : Part subsidiary price
  part_dealer_to_dealer_code citext, -- PMDTDC : Part dealer to dealer code
  emergency_repair_package_qua integer, -- PMERPQ : Emergency repair package qua
  part_lsg_code citext, -- PMLSGC : Part LSG code
  brand_code citext, -- PMPBCD : Brand code
  part_commond_code citext, -- PMPCCD : Part commond code
  part_cross_ship_code citext, -- PMPCSC : Part cross ship code
  part_discount_code citext, -- PMPDCD : Part discount code
  part_direct_ship_code citext, -- PMPDSC : Part direct ship code
  part_hazard_code citext, -- PMPHCD : Part hazard code
  part_interchange_substitutio citext, -- PMPISC : Part interchange/substitutio
  part_large_quantity_code citext, -- PMPLQC : Part large quantity code
  part_maximum_order_quantity integer, -- PMPMOQ : Part maximum order quantity
  part_minimum_order_quantity integer, -- PMPNOQ : Part minimum order quantity
  part_obsolete_status citext, -- PMPOBS : Part obsolete status
  program_part_allowance_code citext, -- PMPPAC : Program part allowance code
  part_pricing_discount_percen numeric(5,3), -- PMPPDP : Part pricing discount percen
  program_part_returnable_code citext, -- PMPPRC : Program part returnable code
  part_size_code citext, -- PMPTSC : Part size code
  part_type citext, -- PMPTYP : Part type
  part_volume_discount_flag citext, -- PMPVDF : Part volume discount flag
  secondary_shelf2 citext, -- PMSHL2 : Secondary shelf
  weight_code citext, -- PMUNWC : Weight code
  unit_of_measure numeric(4,3), -- PMUOM : Unit of measure
  last_receipt_date date, -- PMLRCDT : Last Receipt Date
  first_receipt_date date, -- PMFRCDT : First Receipt Date
  part_zone citext, -- PMZONE : Part Zone
  spiff_bonus_amount numeric(5,2), -- PMSBAMT : Spiff/Bonus Amount
  alias citext, -- PMALIAS : Alias
  haz_mat_disposal_fee numeric(5,2), -- PMHZDFEE : Haz Mat/Disposal Fee
  manual_order_part citext, -- PMMANORD : Manual Order Part
  last_change_timestamp timestamp with time zone, -- PMLASTCHG : Last Change timestamp
  last_update_user citext, -- PMUPDUSER : Last Update User
  part_weight numeric(5,2), -- PMWEIGHT : Part Weight
  shipping_height numeric(3,1), -- PMSHIPHT : Shipping Height
  shipping_length numeric(3,1), -- PMSHIPLEN : Shipping Length
  shipping_depth numeric(3,1), -- PMSHIPDPTH : Shipping Depth
  turnover_code citext -- PMTOCODE : Turnover Code
);
ALTER TABLE arkona_cdc.pdpmast_20200206
  OWNER TO rydell;
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.company_number IS 'PMCO# : Company Number';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.manufacturer IS 'PMMANF : Manufacturer';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_number IS 'PMPART : Part Number';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_description IS 'PMDESC : Part Description';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.stocking_group IS 'PMSGRP : Stocking Group';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.status IS 'PMSTAT : Status';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.obsolete IS 'PMOBSL : Obsolete';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.return_code IS 'PMRTRN : Return Code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.arrival_code IS 'PMARRV : Arrival Code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.stock_promo_code IS 'PMSTKO : Stock Promo Code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.spec_order_code IS 'PMSPOC : Spec Order Code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.price_update IS 'PMPUPD : Price Update';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.kit_part IS 'PMKIT : Kit Part';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.component_part IS 'PMCMPN : Component Part';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.assoc_with_part IS 'PMASSC : Assoc With Part';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.alternate_part IS 'PMALTN : Alternate Part';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.is_core_part IS 'PMCORE : Is Core Part';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.merch_code IS 'PMMERC : Merch Code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.price_symbol IS 'PMPSYM : Price Symbol';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.prod_code_class IS 'PMPRDC : Prod Code/Class';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.fleet_allowance IS 'PMFLTA : Fleet Allowance';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.group_code IS 'PMGRPC : Group Code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.bin_location IS 'PMBINL : Bin Location';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.shelf_locatiion IS 'PMSHLF : Shelf Locatiion';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.qty_on_hand IS 'PMONHD : Qty On Hand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.qty_reserved IS 'PMRSRV : Qty Reserved';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.qty_on_order IS 'PMORDR : Qty On Order';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.qty_on_back_order IS 'PMBCKO : Qty On Back Order';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.qty_on_spec_order IS 'PMSPOR : Qty On Spec Order';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.place_on_order IS 'PMPORD : Place on Order';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.place_on_spec_ord IS 'PMPSPO : Place on Spec Ord';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.pack_qty IS 'PMPACK : Pack Qty';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.min_sales_qty IS 'PMMQTY : Min Sales Qty';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.cost IS 'PMCOST : Cost';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.list_price IS 'PMLIST : List Price';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.trade_price IS 'PMTRAD : Trade Price';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.whlse_comp IS 'PMCOMP : Whlse Comp';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.whlse_comp_fleet IS 'PMCOMPF : Whlse Comp Fleet';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.flat_price IS 'PMFPRC : Flat Price';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.min_max_pack_adj IS 'PMLDTM : Min,Max,Pack Adj';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.dynam_days_supply IS 'PMRSTK : Dynam Days Supply';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.minimum_on_hand IS 'PMMIN : Minimum On Hand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.maximum_on_hand IS 'PMMAX : Maximum On Hand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.low_model_year IS 'PMLMYR : Low Model Year';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.high_model_year IS 'PMHMYR : High Model Year';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.date_in_inventory IS 'PMDTIN : Date In Inventory';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.date_phased_out IS 'PMDTPO : Date Phased Out';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.date_last_sold IS 'PMDTLS : Date Last Sold';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.inventory_turns IS 'PMTRNS : Inventory Turns';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.best_stock_level IS 'PMBSLV : Best Stock Level';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.stock_status IS 'PMSTKS : Stock Status';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.recent_demand IS 'PMRDMD : Recent Demand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.prior_demand IS 'PMPDMD : Prior Demand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.recent_work_days IS 'PMRWDY : Recent Work Days';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.prior_work_days IS 'PMPWDY : Prior Work Days';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.weighted_dly_avg IS 'PMWDAV : Weighted Dly Avg';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.sales_demand IS 'PMSADM : Sales Demand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.lost_sales_demand IS 'PMLSDM : Lost Sales Demand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.spec_order_demand IS 'PMSODM : Spec Order Demand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.non_stock_demand IS 'PMNSDM : Non Stock Demand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.return_demand IS 'PMRTDM : Return Demand';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.inventory_value IS 'PMIVAL : Inventory Value';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.tot_qty_sold IS 'PMTQTY : Tot Qty Sold';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.tot_cost_sales IS 'PMCSAL : Tot Cost Sales';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.stock_purchases IS 'PMSTKP : Stock Purchases';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.display_part_no_ IS 'PMDPRT : Display Part No.';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.sort_part_number IS 'PMSPRT : Sort Part Number';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.old_part_number IS 'PMOPRT : Old Part Number';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.new_part_number IS 'PMNPRT : New Part Number';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.core_part_number IS 'PMCPRT : Core Part Number';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.remarks IS 'PMRMRK : Remarks';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.display_remarks IS 'PMDSPR : Display Remarks';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.bulk_item_actual_weight IS 'PMBIAW : Bulk item actual weight';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.secondary_bin_location2 IS 'PMBIN2 : Secondary bin location';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.class_code IS 'PMCLCD : Class code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_dealer_to_dealer_price IS 'PMCOMPD : Part dealer to dealer price';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_subsidiary_price IS 'PMCOMPS : Part subsidiary price';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_dealer_to_dealer_code IS 'PMDTDC : Part dealer to dealer code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.emergency_repair_package_qua IS 'PMERPQ : Emergency repair package qua';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_lsg_code IS 'PMLSGC : Part LSG code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.brand_code IS 'PMPBCD : Brand code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_commond_code IS 'PMPCCD : Part commond code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_cross_ship_code IS 'PMPCSC : Part cross ship code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_discount_code IS 'PMPDCD : Part discount code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_direct_ship_code IS 'PMPDSC : Part direct ship code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_hazard_code IS 'PMPHCD : Part hazard code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_interchange_substitutio IS 'PMPISC : Part interchange/substitutio';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_large_quantity_code IS 'PMPLQC : Part large quantity code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_maximum_order_quantity IS 'PMPMOQ : Part maximum order quantity';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_minimum_order_quantity IS 'PMPNOQ : Part minimum order quantity';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_obsolete_status IS 'PMPOBS : Part obsolete status';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.program_part_allowance_code IS 'PMPPAC : Program part allowance code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_pricing_discount_percen IS 'PMPPDP : Part pricing discount percen';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.program_part_returnable_code IS 'PMPPRC : Program part returnable code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_size_code IS 'PMPTSC : Part size code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_type IS 'PMPTYP : Part type';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_volume_discount_flag IS 'PMPVDF : Part volume discount flag';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.secondary_shelf2 IS 'PMSHL2 : Secondary shelf';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.weight_code IS 'PMUNWC : Weight code';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.unit_of_measure IS 'PMUOM : Unit of measure';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.last_receipt_date IS 'PMLRCDT : Last Receipt Date';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.first_receipt_date IS 'PMFRCDT : First Receipt Date';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_zone IS 'PMZONE : Part Zone';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.spiff_bonus_amount IS 'PMSBAMT : Spiff/Bonus Amount';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.alias IS 'PMALIAS : Alias';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.haz_mat_disposal_fee IS 'PMHZDFEE : Haz Mat/Disposal Fee';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.manual_order_part IS 'PMMANORD : Manual Order Part';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.last_change_timestamp IS 'PMLASTCHG : Last Change timestamp';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.last_update_user IS 'PMUPDUSER : Last Update User';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.part_weight IS 'PMWEIGHT : Part Weight';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.shipping_height IS 'PMSHIPHT : Shipping Height';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.shipping_length IS 'PMSHIPLEN : Shipping Length';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.shipping_depth IS 'PMSHIPDPTH : Shipping Depth';
COMMENT ON COLUMN arkona_cdc.pdpmast_20200206.turnover_code IS 'PMTOCODE : Turnover Code';

