﻿create table dds.ext_eisglobal_sypfflout (
  flcode citext not null,
  flcyy integer not null,
  flpage integer not null,
  flseq numeric(4,1) not null,
  flline numeric(4,1) not null,
  flflne integer not null,
  flflsq integer not null,
  fldata citext,
  flcont citext,
  constraint ext_eisglobal_sypfflout_pk Primary key(flcode,flcyy,flpage,flseq,flflne,flflsq));
  

comment on column dds.ext_eisglobal_sypfflout.flcode is 'Factory Code';
comment on column dds.ext_eisglobal_sypfflout.flcyy is 'Factory Financial Year';
comment on column dds.ext_eisglobal_sypfflout.flpage is 'Page Number';
comment on column dds.ext_eisglobal_sypfflout.flseq is 'Layout Seq #';
comment on column dds.ext_eisglobal_sypfflout.flline is 'Prt Line #';
comment on column dds.ext_eisglobal_sypfflout.flflne is 'Factory Form Line #';
comment on column dds.ext_eisglobal_sypfflout.flflsq is 'Factory Form Seq #';
comment on column dds.ext_eisglobal_sypfflout.fldata is 'Line Layout';
comment on column dds.ext_eisglobal_sypfflout.flcont is 'Line Layout';

select * from ops.tasks

insert into ops.tasks values('ext_eisglobal_sypfflout','Daily');


select flcode, flcyy,flpage,flseq,flline,flflne,flflsq,fldata, 
--  right(fldata,131), position('|' in fldata),
  trim(left(right(fldata,131), position('|' in right(fldata,131)) - 1))
from dds.ext_eisglobal_sypfflout
where trim(flcode) = 'GM'
  and flcyy = 2016
and flflsq = 0
order by flpage, flflne



select flcyy,flpage,flflne,fldata, length(trim(fldata)),
  case
    when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
    else fldata
  end as fldata_1
from dds.ext_eisglobal_sypfflout
where trim(flcode) = 'GM'
  and flcyy = 2016
and flflsq = 0
order by flpage, flflne


select flcyy,flpage,flflne,left(fldata_1, position('|' in fldata_1) - 1) as line_label
from (
  select flcyy,flpage,flflne,fldata, length(trim(fldata)),
    case
      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
      else fldata
    end as fldata_1
  from dds.ext_eisglobal_sypfflout
  where trim(flcode) = 'GM'
--    and flcyy = 2016
    and flflsq = 0) a
order by flpage, flflne, flcyy



select * from dds.ext_eisglobal_sypfflout where flcyy = 2012 and flpage = 4 and flflne = 58