﻿-- i can find nothing in account definitions that assigns parts split accounts to anything other than parts dept
-- honda looks like it could be a bit more complicated

drop table if exists fs;
create temp table fs as
select * 
from dds.ext_eisglobal_sypffxmst m 
left join (
  select a.account_number, a.account_type, a.account_desc, a.account_sub_type, a.account_ctl_type,
    a.department, a.typical_balance, 
    b.consolidation_grp, b.factory_account, b.fact_account_
  from dds.ext_glpmast a
  inner join dds.ext_ffpxrefdta b on a.account_number = b.g_l_acct_number
    and b.factory_financial_year = 2016
    and coalesce(b.consolidation_grp, 'X') <> '3'
    and b.factory_account <> '331A'
  where a.year = 2016
    and a.active = 'Y'
  union -- add the non parts depts (sd,bs,ql) for the parts split
  select a.account_number, a.account_type, a.account_desc, a.account_sub_type, a.account_ctl_type,
    case 
      when left(account_number, 3) in ('146','166') then 'SD'::citext 
      when left(account_number, 4) in ('1477','1677') then 'BS'::citext
      when left(account_number, 4) in ('1478','1678') then 'QL'::citext
    end as department, 
    a.typical_balance, 
    b.consolidation_grp, b.factory_account, b.fact_account_
  from dds.ext_glpmast a
  inner join dds.ext_ffpxrefdta b on a.account_number = b.g_l_acct_number
    and b.factory_financial_year = 2016
    and coalesce(b.consolidation_grp, 'X') <> '3'
    and b.factory_account <> '331A'
  where a.year = 2016
    and a.active = 'Y'
    and fact_account_ = .5
    and consolidation_grp is null) n on m.fxmact = n.factory_account       
left join (
  select year_month, gtacct, sum(amount) as amount
  from (
    select gtacct, gttamt as amount,
      case extract(month from gtdate)
        when 1 then 201601
        when 2 then 201602
        when 3 then 201603
        when 4 then 201604
        when 5 then 201605
      end as year_month
    from dds.ext_glptrns
    where gtdate between '05/01/2016' and '05/31/2016'
      and gtacct is not null
      and gtpost = 'Y') z
  group by gtacct, year_month ) o on n.account_number = o.gtacct
where m.fxmcyy = 2016; 

-- bad account set up, acct 12224, salaries clerical
-- apparently eisglobal_sypffxmst routes account 147701 incorrectly
-- fuck me, seems like this has to be done
update fs
set fxmpge = 16,
    fxmlne = 49.0,
    fxmcol = 2,
    fxmstr = 46
where fxmact = '477S' and account_number = '147701' and fxmpge = 4 and fxmlne = 59.0;    

update fs
set fxmpge = 16,
    fxmlne = 49.0,
    fxmcol = 3,
    fxmstr = 58
where fxmact = '677S' and account_number = '167701' and fxmpge = 4 and fxmlne = 59.0;  

select * from fs where fxmact = '677a'
select * from fs where gtacct = '167700'
select * from fs where fxmpge = 16 and fxmlne = 49 order by fxmcol

-- new car net sales: 5,810,879.87
SELECT -SUM(amount) from fs WHERE fxmpge BETWEEN 5 AND 14 AND fxmcol = 1 and year_month = 201605 and consolidation_grp is null
-- new car cogs: 5,728,203.77
SELECT SUM(amount) from fs WHERE fxmpge BETWEEN 5 AND 14 AND fxmcol = 3 and year_month = 201605 and consolidation_grp is null
-- new car gross profit/income: 82,676.10
SELECT -SUM(amount) from fs WHERE fxmpge BETWEEN 5 AND 14 and year_month = 201605 and consolidation_grp is null

-- used car net sales: 3,089,920.91
select -SUM(amount) FROM fs WHERE fxmpge = 16 and fxmlne between 1 and 13 AND fxmcol = 1 and year_month = 201605 and consolidation_grp is null
-- used car cogs 2867878.25
select -SUM(amount) FROM fs WHERE fxmpge = 16 and fxmlne between 1 and 13 AND fxmcol = 3 and year_month = 201605 and consolidation_grp is null
-- used gross profit/income: 222042.66
select -SUM(amount) FROM fs WHERE fxmpge = 16 and fxmlne between 1 and 13 and year_month = 201605 and consolidation_grp is null

-- f/i net sales: 334365.31
select -SUM(amount) FROM fs WHERE fxmpge = 17 and fxmlne between 1 and 21 AND fxmcol = 1 and year_month = 201605 and consolidation_grp is null
-- f/i cogs: : -218092.55
select -SUM(amount) FROM fs WHERE fxmpge = 17 and fxmlne between 1 and 21 AND fxmcol = 3 and year_month = 201605 and consolidation_grp is null
-- f/i gross profit/income: 116272.76
select -SUM(amount) FROM fs WHERE fxmpge = 17 and fxmlne between 1 and 21 and year_month = 201605 and consolidation_grp is null

-- variable net sales
select -sum(amount) 
FROM fs 
WHERE (
  (fxmpge BETWEEN 5 AND 14 AND fxmcol = 1 )
  OR
  (fxmpge = 16 and fxmlne between 1 and 13 AND fxmcol = 1)
  OR
  (fxmpge = 17 and fxmlne between 1 and 21 AND fxmcol = 1))
  and year_month = 201605 and consolidation_grp is null
  
-- variable gross  
select -sum(amount) 
FROM fs 
WHERE (
  (fxmpge BETWEEN 5 AND 14)
  OR
  (fxmpge = 16 and fxmlne between 1 and 13)
  OR
  (fxmpge = 17 and fxmlne between 1 and 21))
  and year_month = 201605 and consolidation_grp is null


-- variable expenses: matches budget and 
select 
  sum(case when fxmlne between 4 and 6 then amount else 0 end) as total_variable,
  sum(case when fxmlne between 8 and 16 then amount else 0 end) as total_personnel,
  sum(case when fxmlne between 18 and 39 then amount else 0 end) as total_semi_fixed,
  sum(case when fxmlne between 41 and 48 then amount else 0 end) as subtotal_rent,
  sum(case when fxmlne between 41 and 54 then amount else 0 end) as total_fixed,
  sum(case when fxmlne between 4 and 57 then amount else 0 end)as total_expenses
from fs
where fxmpge = 3
  and fxmcol in (1, 5)
  and year_month = 201605
  and consolidation_grp is null 


-- fixed net sales : P6L62: 2,376,253.40
select -sum(amount * fact_account_) 
FROM fs 
WHERE fxmpge = 16
  and fxmlne between 21 and 61
  and fxmcol <> 3
  and consolidation_grp is null 
  and year_month = 201605

-- fixed cogs : P6L62 : 1369093.60
select sum(amount * fact_account_) 
FROM fs 
WHERE fxmpge = 16
  and fxmlne between 21 and 61
  and fxmcol =3
  and consolidation_grp is null 
  and year_month = 201605

-- fixed gross : P6L62 : 1007159.80
select sum(amount * fact_account_) 
FROM fs 
WHERE fxmpge = 16
  and fxmlne between 21 and 61
  and consolidation_grp is null 
  and year_month = 201605


-- fixed gross by department
select department, -sum(amount * fact_account_) 
FROM fs 
WHERE fxmpge = 16
  and fxmlne between 21 and 61
  and consolidation_grp is null 
  and year_month = 201605
group by department  


select year_month, department, 
  round(sum(case when fxmlne between 4 and 6 then amount else 0 end), 0) as total_variable,
  round(sum(case when fxmlne between 8 and 16 then amount else 0 end), 0) as total_personnel,
  round(sum(case when fxmlne between 18 and 39 then amount else 0 end), 0) as total_semi_fixed,
  round(sum(case when fxmlne between 41 and 54 then amount else 0 end), 0) as total_fixed,
  round(sum(case when fxmlne between 4 and 57 then amount else 0 end), 0) as total_expenses
from fs
where fxmpge = 4
  and fxmcol in (1,11,13)
--  and coalesce(year_month, 201605) = 201605
  and consolidation_grp is null 
  and department is not null
group by year_month, department
order by year_month, department
  

select * from fs where coalesce(year_month, 201601) = 201601
order by fxmpge, fxmlne, fxmcol


select * from ext_eisglobal_sypffxmst order by fxmpge, fxmlne, fxmcol

select * from fs where fxmpge = 1 and fxmlne < 8 order by fxmpge, fxmlne, fxmcol

select fxmcyy, fxmact, fxmpge, fxmlne, fxmcol, fxmstr, account_number, account_desc, consolidation_grp, year_month,
  coalesce(year_month, 201601)
from fs
where fxmpge = 1 and fxmlne < 8 
order by fxmpge, fxmlne, fxmcol

select * from fs where fxmact like '477%' and year_month = 201605

select *
from (
  select left(fxmact, 3) as base_acct, count(*)
  from (
    select fxmact
    from fs
    where left(fxmact,1) not in ('<','>','&','*')
    group by fxmact) a
  group by left(fxmact, 3)) x 
left join (
    select fxmact
    from fs
    where left(fxmact,1) not in ('<','>','&','*')
    group by fxmact) y on x.base_acct = left(y.fxmact, 3)
order by x.base_Acct, y.fxmact

select left(fxmact, 3) as base, fxmact, account_number, consolidation_grp, 
  account_desc, account_type, max(amount)
from fs
where left(fxmact,1) not in ('<','>','&','*')
  and account_type = '4'
group by consolidation_grp, left(fxmact, 3), fxmact, account_number, account_desc, account_type
order by coalesce(consolidation_grp, '1'), left(fxmact, 3), fxmact, account_number

select * from dds.ext_eisglobal_sypffxmst

select * from fs limit 100

select * from dds.ext_glpmast where year = 2016 limit 100


select * from dds.ext_glptrns where gtdate = '06/02/2016'

-- this should be the whole shebang
select * from fs where coalesce(consolidation_grp, '1') = '1' and coalesce(year_month, 201605) = 201605 and fxmpge = 3 order by fxmpge, fxmlne, fxmcol, account_number

select fxmact, fxmpge, fxmlne, account_number, department from fs where coalesce(consolidation_grp, '1') = '1' and coalesce(year_month, 201605) = 201605 group by fxmact, fxmpge, fxmlne, account_number, department having count(*) > 1 order by fxmact
-- without department, these are doubled one for each parts split dept
select * from fs where coalesce(consolidation_grp, '1') = '1' and coalesce(year_month, 201605) = 201605 and fxmact in ('467a','477a','468a','667a','668a','478a','677a','678a')order by fxmpge, fxmlne, fxmcol, account_number

-- this should be the whole shebang
select * from fs where coalesce(consolidation_grp, '1') = '1' and coalesce(year_month, 201605) = 201605 and fxmpge = 3 and fxmlne = 6 order by fxmpge, fxmlne, fxmcol, account_number

-- missing 11501 D/E

select * from dds.ext_ffpxrefdta where factory_financial_year = 2016 and consolidation_grp is null order by g_l_acct_number


select * from fs where fxmpge = 3 and fxmlne = 6    


select fxmpge, fxmlne, fxmcol, fxmact, account_number, department from fs where coalesce(consolidation_grp, '1') = '1' and coalesce(year_month, 201605) = 201605 group by fxmact, fxmpge, fxmlne, fxmcol, account_number, department order by fxmpge, fxmlne, fxmcol, account_number


-- trying to pare it down
-- but this still includes multiple "things" in one query: fs: gm account to page/line/col and gm_account to gl_account

create temp table page_3 as
select a.fxmcyy, a.fxmact, a.fxmpge, a.fxmlne, a.fxmcol, 
  case 
    when coalesce(consolidation_grp, 'RY1') = '2' then 'RY2'
    else coalesce(consolidation_grp, 'RY1')
  end as store_code, 
  b.g_l_acct_number, b.fact_account_,
  c.account_type, c.account_desc, c.account_ctl_type, c.department, c.typical_balance
--select *
from dds.ext_eisglobal_sypffxmst a
left join dds.ext_ffpxrefdta b on a.fxmact  = b.factory_account
  and b.factory_financial_year = 2016
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_account <> '331A'
left join dds.ext_glpmast c on b.g_l_acct_number = c.account_number
  and c.year = 2016
  and c.account_sub_type in ('A','B')  
where a.fxmcyy = 2016
  and a.fxmpge = 3
  and b.consolidation_grp is null 
order by coalesce(consolidation_grp, '1'), a.fxmpge, a.fxmlne, a.fxmcol, b.g_l_acct_number

select * from page_3

drop table if exists full_page_3;
create temp table full_page_3 as
select *
from page_3 c
left join (
    select gtacct, gttamt as amount,
      case extract(month from gtdate)
        when 1 then 201601
        when 2 then 201602
        when 3 then 201603
        when 4 then 201604
        when 5 then 201605
      end as year_month      
    from dds.ext_glptrns a
    inner join page_3 b on a.gtacct = b.g_l_acct_number
    where gtdate between '05/01/2016' and '05/31/2016'
      and gtacct is not null
      and gtpost = 'Y') d on c.g_l_acct_number = d.gtacct
--where c.fxmpge = 3      
order by fxmpge, fxmlne, fxmcol, fxmact, g_l_acct_number      


select a.*, b.fxmcol, b.amount
from (
  select fxmpge, fxmlne, fxmcol, sum(amount) as amount
  from full_page_3
  where fxmcol = 1
  group by fxmpge, fxmlne, fxmcol) a
left join (
  select fxmpge, fxmlne, fxmcol, sum(amount) as amount
  from full_page_3
  where fxmcol = 5
  group by fxmpge, fxmlne, fxmcol) b on a.fxmpge = b.fxmpge and a.fxmlne = b.fxmlne
order by a.fxmpge, a.fxmlne



select distinct fxmact from page_3 
where left(fxmact, 1)not in ('<','>','&','*')
order by fxmact

select distinct fxmact 
from dds.ext_eisglobal_sypffxmst 
where left(fxmact, 1)not in ('<','>','&','*')
order by fxmact 


select distinct fxmact, left(fxmact, 3) as base_account 
from dds.ext_eisglobal_sypffxmst 
where left(fxmact, 1) in ('1','2','3','4','5','6','7','8','9','0')
order by fxmact 

drop table if exists fin.dim_coa_factory cascade;
create table fin.dim_coa_factory (
  account_key serial primary key,
  manufacturer citext not null,
  account citext not null,
  base_account citext not null,
  description citext not null default 'tbd');
create unique index dim_coa_factory_nk on fin.dim_coa_factory (manufacturer,account);

select * from fin.dim_coa_factory

insert into fin.dim_coa_factory (manufacturer, account, base_account)
select 'GM', fxmact, left(fxmact, 3)
from dds.ext_eisglobal_sypffxmst 
where left(fxmact, 1) in ('1','2','3','4','5','6','7','8','9','0')
group by fxmact, left(fxmact, 3);

select * from fin.dim_coa_factory order by account


drop table if exists fin.dim_account cascade;
create table fin.dim_account (
  account_key serial primary key,
  account citext not null,
  account_type_code citext not null,
  account_type citext not null,
  account_description citext not null,
  store_code citext not null,
  department_code citext not null,
  department citext not null, 
  typical_balance citext not null,
  active citext not null);
create unique index dim_account_nk on fin.dim_account (account);

insert into fin.dim_account(account,account_type_code, account_type,account_description,store_code,department_code, department,typical_balance,active)
select account_number, account_type_code, account_type, account_desc, storecode, coalesce(department_code, 'UNK'), department, typical_balance, active
from (
  select account_number, 
    account_type as account_type_code,
    case account_type
      when '1' then 'Asset'
      when '2' then 'Liability'
      when '3' then 'Equity'
      when '4' then 'Sale'
      when '5' then 'COGS'
      when '6' then 'Income'
      when '7' then 'Other Income'
      when '8' then 'Expense'
      when '9' then 'Other Expense'
    end as account_type, account_desc, 
    case account_sub_type
      when 'A' then 'RY1'
      when 'B' then 'RY2'
    end as storecode, 
    department as department_code,
    case department
      when 'AD' then 'Admin'
      when 'BS' then 'Body Shop'
      when 'CW' then 'Car Wash'
      when 'FI' then 'Finance'
      when 'GN' then 'General'
      when 'LR' Then 'National'
      when 'NC' then 'New Vehicle'
      when 'PD' then 'Parts'
      when 'QL' then 'Quick Lane'
      when 'RE' then 'Detail'
      when 'SD' then 'Service'
      when 'UC' then 'Used Vehicle'    
      else 'UNK'
    end as department, 
    case typical_balance
      when 'D' then 'Debit'
      when 'C' then 'Credit'
    end as typical_balance, active
  from dds.ext_glpmast 
  where account_sub_type <> 'C') x
group by account_number, account_type_code, account_type, account_desc, storecode, department_code, department, typical_balance, active;

insert into fin.dim_account (account,account_type_code,account_type,account_description,store_code,department_code,department,typical_balance,active)
values('unknown','unknown','unknown','unknown','unknown','unknown','unknown','unknown','unknown');


-- --------------------------- 6/26 this is a big todo
-- ------- along with fixing the bad routing of 147701
-- -- split multiplier in dim_acct?
-- -- add the non parts depts (sd,bs,ql) for the parts split
--   select a.account_number, a.account_type, a.account_desc, a.account_sub_type, a.account_ctl_type,
--     case 
--       when left(account_number, 3) in ('146','166') then 'SD'::citext 
--       when left(account_number, 4) in ('1477','1677') then 'BS'::citext
--       when left(account_number, 4) in ('1478','1678') then 'QL'::citext
--     end as department, 
--     a.typical_balance, 
--     b.consolidation_grp, b.factory_account, b.fact_account_
--   from dds.ext_glpmast a
--   inner join dds.ext_ffpxrefdta b on a.account_number = b.g_l_acct_number
--     and b.factory_financial_year = 2016
--     and coalesce(b.consolidation_grp, 'X') <> '3'
--     and b.factory_account <> '331A'
--   where a.year = 2016
--     and a.active = 'Y'
--     and fact_account_ = .5
--     and consolidation_grp is null
-- 
select * 
from fin.dim_account a
inner join dds.ext_ffpxrefdta b on a.account = b.g_l_acct_number
  and b.factory_financial_year = 2016
  and b.factory_code = 'GM'
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_account <> '331A'
  and b.fact_account_ = .5
order by a.account  
-- 
--   select a.account_number, a.account_type, a.account_desc, a.account_sub_type, a.account_ctl_type,
--     case 
--       when left(account_number, 3) in ('146','166') then 'SD'::citext 
--       when left(account_number, 4) in ('1477','1677') then 'BS'::citext
--       when left(account_number, 4) in ('1478','1678') then 'QL'::citext
--     end as split_department, 
--     department, 
--     a.typical_balance, 
--     b.consolidation_grp, b.factory_account, b.fact_account_
--   from dds.ext_glpmast a
--   inner join dds.ext_ffpxrefdta b on a.account_number = b.g_l_acct_number
--     and b.factory_financial_year = 2016
--     and coalesce(b.consolidation_grp, 'X') <> '3'
--     and b.factory_account <> '331A'
--   where a.year = 2016
--     and a.active = 'Y'
--     and fact_account_ = .5
--     and consolidation_grp is null

 
drop table if exists fin.dim_journal_doc_type cascade;
create table fin.dim_journal_doc_type (
  journal_doc_type_key serial primary key,
  journal_code citext not null,
  journal_description citext not null,
  doc_type_code citext not null,
  doc_type_description citext not null);
create unique index dim_journal_doc_type_nk on fin.dim_journal_doc_type (journal_code, doc_type_code);  

insert into fin.dim_journal_doc_type (journal_code, journal_description, doc_type_code, doc_type_description)
select * from (
  select journal_code, journal_desc
  from dds.ext_glpjrnd
  where journal_code is not null 
  group by journal_code, journal_desc
  union
  select 'none','none') a
cross join (
  select 'b' as doc_type_code, 'deals' as doc_type
  union
  select 'c','checks'
  union
  select 'd','deposits'
  union
  select 'i','inventory purchase/stock in'
  union
  select 'j','conversion or gje'
  union
  select 'o','invoice/po entry'
  union
  select 'p','parts ticket'
  union
  select 'r','cash receipts'
  union
  select 's','service tickets'
  union
  select 'w','handwritten check'
  union
  select 'x','bank rec'
  union
  select 'h','i do not know'
  union
  select 't','i do not know'  ) b;




update fin.dim_journal_doc_type
set doc_type_description = 'handwritten check'
where doc_type_description = 'handwritten chesc'

select a.gttrn_, a.gtseq_, a.gtctl_, a.gtdoc_, a.gtref_, a.gttamt,
  b.account_key, c.journal_doc_type_key, a.gtpost
-- select *  
from dds.ext_glptrns a
left join fin.dim_account b on a.gtacct = b.account
left join fin.dim_journal_doc_type c on a.gtjrnl = c.journal_code
  and a.gtdtyp = c.doc_type_code
where gtdate between '05/01/2016' and '05/31/2016'
limit 100

drop table if exists fin.fact_gl cascade;
create table fin.fact_gl (
  trans bigint not null,
  seq integer not null,
  control citext not null,
  doc citext not null, 
  refer citext not null, 
  amount numeric(12,2) default 0,
  account_key integer not null references fin.dim_account(account_key),
  journal_doc_type_key integer not null references fin.dim_journal_doc_type(journal_doc_type_key),
  trans_date_key integer not null references dds.day(datekey),
  rec_date_key integer not null references dds.day(datekey),
  post citext not null);

insert into fin.fact_gl  
select a.gttrn_, a.gtseq_, a.gtctl_, a.gtdoc_, a.gtref_, a.gttamt,
  coalesce(b.account_key, bb.account_key), 
  coalesce(c.journal_doc_type_key, cc.journal_doc_type_key), 
  case a.gtdate
    when null then f.datekey
    when '9999-01-01' then f.datekey
    else d.datekey
  end as gtdate,
  case a.gtrdate
    when null then f.datekey
    when '9999-01-01' then f.datekey
    else e.datekey
  end as gtrdate,  
  a.gtpost
-- select *  
from dds.ext_glptrns a
left join fin.dim_account b on coalesce(a.gtacct, 'unknown') = b.account
left join fin.dim_account bb on 1 = 1
  and bb.account = 'unknown'
left join fin.dim_journal_doc_type c on a.gtjrnl = c.journal_code
  and a.gtdtyp = c.doc_type_code
left join fin.dim_journal_doc_type cc on cc.journal_code = 'none'
  and a.gtdtyp = cc.doc_type_code  
left join dds.day d on a.gtdate = d.thedate
left join dds.day e on a.gtrdate = e.thedate
left join dds.day f on 1 = 1
  and f.thedate = '12/31/9999'
where a.gtdate between '05/01/2016' and '05/31/2016'
  and a.gtpost = 'Y' ; 

create index on fin.fact_gl (account_key);
create index on fin.fact_gl (journal_doc_type_key);
create index on fin.fact_gl (trans_date_key);
create index on fin.fact_gl (rec_date_key);
create index on fin.fact_gl (post);


select * from fin.dim_Account where account = '147701'

select a.* 
from fin.fact_gl a
inner join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '147701'
inner join dds.day c on a.trans_date_key = c.datekey
  and c.yearmonth = 201605
where a.post = 'Y'  


-- 6/24 --------------------------------------------------------------------------------------------------
select * from dds.ext_eisglobal_sypffxmst where fxmcyy = 2016 order by fxmpge, fxmlne, fxmcol

select * from  dds.ext_ffpxrefdta where factory_financial_year = 2016 order by factory_accountlimit 100

select coalesce(consolidation_grp, '1'), factory_account, count(*) from  dds.ext_ffpxrefdta where factory_financial_year = 2016 and coalesce(consolidation_grp, '1') <> '3' group by  coalesce(consolidation_grp, '1'), factory_account order by factory_account

select * from  dds.ext_ffpxrefdta where factory_financial_year = 2016 and factory_account= '002A'

select * from fin.dim_account

-- leaf node accounts: resolve to gl account(s)
select a.*, b.g_l_acct_number, b.fact_account_, c.*
from dds.ext_eisglobal_sypffxmst a 
left join dds.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and b.factory_financial_year = 2016
  and coalesce(b.consolidation_grp, '1') = '1'
  and b.factory_code = 'GM'
left join fin.dim_account c on b.g_l_acct_number = c.account
--  and c.store_code = 'ry2'
where a.fxmcyy = 2016 
  and left(a.fxmact, 1) in ('1','2','3','4','5','6','7','8','9','0') 
order by a.fxmpge, a.fxmlne, a.fxmcol


-- honda may statement
select a.fxmpge, a.fxmlne, a.fxmcol, sum(d.amount), a.fxmact, account
from dds.ext_eisglobal_sypffxmst a 
left join dds.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and b.factory_financial_year = 2016
  and coalesce(b.consolidation_grp, '1') = '2'
  and b.factory_code = 'GM'
left join fin.dim_account c on b.g_l_acct_number = c.account
  and c.store_code = 'ry2'
left join fin.fact_gl d on c.account_key = d.account_key
  and d.post = 'Y'
inner join dds.day e on d.trans_date_key = e.datekey
  and e.yearmonth = 201605
where a.fxmcyy = 2016 
  and a.fxmpge = 17
--  and left(a.fxmact, 1) in ('1','2','3','4','5','6','7','8','9','0') 
group by a.fxmpge, a.fxmlne, a.fxmcol, a.fxmact, account
order by a.fxmpge, a.fxmlne, a.fxmcol


select * from fin.dim_account where account = '145400'
select * from fin.dim_Account where department = 'finance' order by account


-- still struggling with the question of dept/org is it an attribute or a dimension
-- from taccounting, the only way i havce to separate out car wash/detail/quick lane/service is with the department attribut of dim_account
-- but in sales and finance, would have to rely on decoding the account_description

so maybe, the fact_fs has a gl_department_key as well as a fs_org_key

drop table if exists fin.dim_fs_org;
create table fin.dim_fs_org (
  fs_org_key serial primary key,
  market citext not null,
  store citext not null,
  area citext not null,
  department citext not null,
  sub_department citext not null default 'none');
create unique index fs_org_nk on fin.dim_fs_org ( market,store,area,department,sub_department); 
insert into fin.dim_fs_org (market,store,area,department,sub_department) values
  ('grand forks','ry1','variable','sales','new'),  
  ('grand forks','ry1','variable','sales','used'),
  ('grand forks','ry1','variable','finance','new'),
  ('grand forks','ry1','variable','finance','used'),
  ('grand forks','ry1','fixed','body shop','none'),
  ('grand forks','ry1','fixed','parts','none'),
  ('grand forks','ry1','fixed','service','mechanical'),
  ('grand forks','ry1','fixed','service','detail'),
  ('grand forks','ry1','fixed','service','quick lane'),
  ('grand forks','ry1','fixed','service','car wash'),
  ('grand forks','ry2','variable','sales','new'),  
  ('grand forks','ry2','variable','sales','used'),
  ('grand forks','ry2','variable','finance','new'),
  ('grand forks','ry2','variable','finance','used'),
  ('grand forks','ry2','fixed','parts','none'),
  ('grand forks','ry2','fixed','service','mechanical'),
  ('grand forks','ry2','fixed','service','quick lane');


-- 6/25 ---------------------------------------------------------------------------------------------------------
-- still stuck
-- feel really shitty today  
 
select * from dds.ext_ffpxrefdta where factory_financial_year = 2016 and coalesce(consolidation_grp, '1') = '2' limit 100


select *
from (
select factory_code, g_l_acct_number, factory_account from dds.ext_ffpxrefdta where factory_financial_year = 2016 and coalesce(consolidation_grp, '1') = '2' and factory_code = 'GM') a
full outer join (
select factory_code, g_l_acct_number, factory_account from dds.ext_ffpxrefdta where factory_financial_year = 2016 and coalesce(consolidation_grp, '1') = '2' and factory_code <> 'GM') b
on a.g_l_acct_number = b.g_l_acct_number


-- aha some page/line/col resolve to multiple accounts
select fxmpge, fxmlne, fxmcol, fxmstr
from (
  select fxmact, fxmpge, fxmlne, fxmcol, fxmstr 
  from dds.ext_eisglobal_sypffxmst a 
  where left(a.fxmact, 1) in ('1','2','3','4','5','6','7','8','9','0') 
    and fxmcyy = 2016 
  group by fxmact, fxmpge, fxmlne, fxmcol, fxmstr) b
group by  fxmpge, fxmlne, fxmcol, fxmstr
having count(*) > 1
order by fxmpge, fxmlne, fxmcol


select * from dds.ext_eisglobal_sypffxmst where fxmcyy = 2016 and fxmpge = 16 and fxmlne = 4.0 and fxmcol = 3

select fxmpge, fxmlne, fxmcol, fxmact
from dds.ext_eisglobal_sypffxmst 
where fxmcyy = 2016
group by fxmpge, fxmlne, fxmcol, fxmact
order by fxmpge, fxmlne, fxmcol, fxmact

-- does on account occur on more than one p/l/c?
-- nope
select fxmact
from (
  select fxmpge, fxmlne, fxmcol, fxmact
  from dds.ext_eisglobal_sypffxmst 
  where fxmcyy = 2016
  group by fxmpge, fxmlne, fxmcol, fxmact) a
group by fxmact 
having count(*) > 1  
order by fxmpge, fxmlne, fxmcol, fxmact

drop table if exists fs_layout;
create temp table fs_layout as
select fxmpge as page, fxmlne as line, fxmcol as col, fxmact as gm_account
from dds.ext_eisglobal_sypffxmst 
where fxmcyy = 2016
group by fxmpge, fxmlne, fxmcol, fxmact
order by fxmpge, fxmlne, fxmcol, fxmact;

select * from dds.ext_ffpxrefdta limit 100
-- more than one account applied to page/line/col;
select *
from fs_layout a
inner join (
  select page,line,col
  from fs_layout
  group by page,line,col
  having count(*) > 1) b on a.page = b.page and a.line = b.line and a.col = b.col

select a.*, b.g_l_acct_number, b.fact_account_, c.*
from fs_layout a
left join dds.ext_ffpxrefdta b on a.gm_account = b.factory_account
  and b.factory_code = 'GM'
  and coalesce(consolidation_grp, '1') <> '3'
  and b.factory_financial_year = 2016
left join fin.dim_account c on b.g_l_acct_number = c.account  
where a.page = 4
order by page,line,col,gm_account
  
-- 6/26----------------------------------------------------------------------------------------------------------
-- think i am closer

select distinct page, line from fs_layout where page = 2 order by page, line

this does not include subtotals lines eg pg 2 line 7,17,40,49, etc

select a.* , c.*
from fin.fact_gl a
inner join dds.day b on a.trans_date_key = b.datekey
  and b.yearmonth = 201605
inner join fin.dim_account c on a.account_key = c.account_key  
WHERE a.post = 'Y'
limit 1000

-- 6/27 ------------------------------------------------------------------------------------------------------------
-- non formula cells
-- with struggling with the notion of department still, i think

select a.*, b.g_l_acct_number, b.fact_account_, c.*
from fs_layout a
inner join dds.ext_ffpxrefdta b on a.gm_account = b.factory_account
  and b.factory_code = 'GM'
  and coalesce(consolidation_grp, '1') <> '3'
  and b.factory_financial_year = 2016
inner join fin.dim_account c on b.g_l_acct_number = c.account  
-- where a.page = 4
order by page,line,col,gm_account

-- so if i add a dim_fs_org key to this am i covered?
-- also need a description for the line eg salaries supervision, employee benefits, rent, etc
-- line level description also, maybe, account level description eg finance income used honda, finance income used nissan etc

drop table if exists wtf;
create temp table wtf as
select e.yearmonth, a.page, a.line, a.col, gm_account, account, store_code, department, sum(d.amount) as amount
from fs_layout a
inner join dds.ext_ffpxrefdta b on a.gm_account = b.factory_account
  and b.factory_code = 'GM'
  and coalesce(consolidation_grp, '1') <> '3'
  and b.factory_financial_year = 2016
  and factory_account <> '331A'
inner join fin.dim_account c on b.g_l_acct_number = c.account  
left join fin.fact_gl d on c.account_key = d.account_key
  and d.post_status = 'Y'
inner join dds.day e on d.date_key = e.datekey
  and e.yearmonth = 201605  
--where a.page = 4 
group by e.yearmonth, page, line, col, gm_account, account, store_code, department -- limit 10
order by store_code, page,line,col,gm_account

select distinct department from wtf

select * from wtf order by yearmonth,page,line,col limit 100

select yearmonth,page,line,col, gm_account from wtf group by yearmonth,page,line,col, gm_account having count(*) > 1

drop table if exists wtf_1;
create temp table wtf_1 as
select a.yearmonth, a.page, a.line, a.col, a.gm_Account, a.account, 
  b.store, b.area, b.department, b.sub_department, a.amount
from wtf a
left join fin.dim_fs_org b on a.store_code = b.store
  and
    case 
      when a.department = 'body shop' then b.department = 'body shop'
      when a.department = 'car wash' then b.sub_Department = 'car wash'
      when a.department = 'detail' then b.sub_department = 'detail'
      when a.department = 'finance' and a.line < 10 then b.department = 'finance' and b.sub_department = 'new'
      when a.department = 'finance' and a.line > 10 then b.department = 'finance' and b.sub_department = 'used'
      when a.department = 'new vehicle' then b.department = 'sales' and b.sub_department = 'new'
      when a.department = 'parts' then b.department = 'parts'
      when a.department = 'quick lane' then b.sub_department = 'quick lane'
      when a.department = 'service' then b.sub_department = 'mechanical'
      when a.department = 'used vehicle' then b.department = 'sales' and b.sub_department = 'used'
    end 
order by b.store, a.page, a.line, a.col

select yearmonth,page,line,col,account from wtf_1 group by yearmonth,page,line,col,account having count(*) > 1

-- page 2 store level - the sum of page 3 and page 4
select yearmonth, store, page, line, round(sum(amount), 0) as amount
from wtf_1
where page in (3, 4)
--  and yearmonth = 201511
group by yearmonth, store, page, line
order by store, line, yearmonth

select store, line,
  sum(case when yearmonth = 201501 then amount end) as "201501",
  sum(case when yearmonth = 201502 then amount end) as "201502",
  sum(case when yearmonth = 201503 then amount end) as "201503",
  sum(case when yearmonth = 201504 then amount end) as "201504",
  sum(case when yearmonth = 201505 then amount end) as "201505",
  sum(case when yearmonth = 201506 then amount end) as "201506",
  sum(case when yearmonth = 201507 then amount end) as "201507",
  sum(case when yearmonth = 201508 then amount end) as "201508",
  sum(case when yearmonth = 201509 then amount end) as "201509",
  sum(case when yearmonth = 201510 then amount end) as "201510",
  sum(case when yearmonth = 201511 then amount end) as "201511",
  sum(case when yearmonth = 201512 then amount end) as "201512"
from (  
  select yearmonth, store, line, round(sum(amount), 0) as amount
  from wtf_1
  where page in (3, 4)
    and store in ('ry1','ry2')
  group by yearmonth, store, line) a
group by store, line
order by store, line

-- page 2 - variable only: page 3
select store, line, round(sum(amount), 0) as amount
from wtf_1
where page = 3
  and area = 'variable'
group by store, line
order by store, line

-- page 2 - fixed only: page 4
select store, line, round(sum(amount), 0) as amount
from wtf_1
where page = 4
  and area = 'fixed'
group by store, line
order by store, line

select *
from wtf_1
where store = 'ry1'

select * from wtf a left join fin.dim_coa_factory b on a.gm_account = b.account where b.account is null

select sum(amount)
from wtf_1
where page > 1
  and store = 'ry1'

-- slspl compensation
select * from wtf_1 where page = 3 and line = 4 and yearmonth = 201605
-- and the individual transactions
select a.*, b.*
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account in ('11101','11102')
inner join dds.day c on a.trans_date_key = c.datekey
  and c.yearmonth = 201605
order by a.control, c.thedate  

select * from wtf_1 where page = 4 and store = 'ry1'
-- 7/1 ----------------------------------------------------------------------------
-- been a couple of days, what's next
-- define the actual tables and process for populating
-- 
-- one of the things that has been annoying me lately is why the fuck am i doing half a dozen
-- different glptrns (for example, and not just glptrns) scrapes and using them in different projects
-- fuck me that is not good
-- 
-- anyway, what's missing from the tables i need for this
-- 
-- fix the bad 147701 routing
-- add the split accounts: 
--    add the non parts departments
--    where do i put the .5 factor
-- also need a description for the line eg salaries supervision, employee benefits, rent, etc
-- line level description also, maybe, account level description eg finance income used honda, finance income used nissan etc
-- translation between fs and budget
-- need to add those non single account summary/sub total/total type lines

-- are page/line/col department specific?ii
-- temp tables
drop table if exists fs_layout;
create temp table fs_layout as
select fxmpge as page, fxmlne as line, fxmcol as col, fxmact as gm_account
from dds.ext_eisglobal_sypffxmst 
where fxmcyy = 2016
group by fxmpge, fxmlne, fxmcol, fxmact
order by fxmpge, fxmlne, fxmcol, fxmact;  

-- don't remember why but long ascertained that 331A was of no use
drop table if exists wtf;
create temp table wtf as
select e.yearmonth, page, line, col, gm_account, account, store_code, department, sum(d.amount) as amount
from fs_layout a
inner join dds.ext_ffpxrefdta b on a.gm_account = b.factory_account
  and b.factory_code = 'GM'
  and coalesce(consolidation_grp, '1') <> '3'
  and b.factory_financial_year = 2016
  and factory_account <> '331A'
inner join fin.dim_account c on b.g_l_acct_number = c.account  
left join fin.fact_gl d on c.account_key = d.account_key
  and d.post = 'Y'
inner join dds.day e on d.trans_date_key = e.datekey
group by e.yearmonth, page, line, col, gm_account, account, store_code, department;
-- routing error
-- update
drop table if exists wtf_1;
create temp table wtf_1 as
select a.yearmonth, a.page, a.line, a.col, a.gm_Account, a.account, 
  b.store, b.area, b.department, b.sub_department, a.amount
from wtf a
left join fin.dim_fs_org b on a.store_code = b.store
  and
    case 
      when a.department = 'body shop' then b.department = 'body shop'
      when a.department = 'car wash' then b.sub_Department = 'car wash'
      when a.department = 'detail' then b.sub_department = 'detail'
      when a.department = 'finance' and a.line < 10 then b.department = 'finance' and b.sub_department = 'new'
      when a.department = 'finance' and a.line > 10 then b.department = 'finance' and b.sub_department = 'used'
      when a.department = 'new vehicle' then b.department = 'sales' and b.sub_department = 'new'
      when a.department = 'parts' then b.department = 'parts'
      when a.department = 'quick lane' then b.sub_department = 'quick lane'
      when a.department = 'service' then b.sub_department = 'mechanical'
      when a.department = 'used vehicle' then b.department = 'sales' and b.sub_department = 'used'
    end;

select *
from wtf_1    
where account = '147701'

select * from wtf limit 1000

issue shows up on page 6 line 49
page 4 line 59 is not a single account assigned amount

update fs
set fxmpge = 16,
    fxmlne = 49.0,
    fxmcol = 2,
    fxmstr = 46
where fxmact = '477S' and account_number = '147701' and fxmpge = 4 and fxmlne = 59.0;    

update fs
set fxmpge = 16,
    fxmlne = 49.0,
    fxmcol = 3,
    fxmstr = 58
where fxmact = '677S' and account_number = '167701' and fxmpge = 4 and fxmlne = 59.0;  

select * from fs_layout where gm_Account in ('477s','677s')

-- page 6 parts
select line, col, sum(amount)
from wtf_1
where yearmonth = 201605
  and store = 'ry1'
  and page = 16
  and department = 'parts'
group by line, col  
order by line, col

-- page 4 parts
select line, col, sum(amount)
from wtf_1
where yearmonth = 201606
  and store = 'ry1'
  and page = 4
  and department = 'parts'
group by line, col  
order by line, col

select * from fs_layout where page = 4 and line = 59

select * from wtf where gm_account = '477s' and yearmonth = 201605

-- the glpmast department conundrum
select * from wtf_1
where page = 3 and line = 67

select * from wtf
where page = 3 and line = 67

select * 
from wtf
where gm_account in (
select gm_Account from wtf_1 where store is null) 

select * from wtf_1 where page = 1