﻿select *
from arkona.xfm_inpmast a
where not exists (
  select 1
  from arkona.ext_inpmast
  where inpmast_vin = a.inpmast_vin)

delete from arkona.xfm_inpmast where inpmast_vin = '2HKRW2H26LH673292';  

----------------------------------------------------------------
--< 05/28/21 
----------------------------------------------------------------
----------------------------------------------------------------
--< 03/11/21 
----------------------------------------------------------------
vin changed on vehicle with 18 existing rows in xfm_inpmast
old vin : 1GCRYCEFXKZ177569
new vin : 1GCRYDED5KZ177659

-- this one has the same license number for all rows
select * 
from arkona.xfm_inpmast
where license_number = 'BST584'
order by row_from_date;

-- the old rows
drop table if exists inpmast_keys;
create temp table inpmast_keys as
select inpmast_key
from arkona.xfm_inpmast
where inpmast_vin = '1GCRYCEFXKZ177569';

-- change row_from_date on last row for old vin
update arkona.xfm_inpmast
set row_thru_date = '03/09/2021',
    current_row = false
-- select * from arkona.xfm_inpmast
where inpmast_key = (
  select max(inpmast_key)
  from inpmast_keys);

-- change the vin on the old rows
update arkona.xfm_inpmast
set inpmast_vin = '1GCRYDED5KZ177659'
-- select * from arkona.xfm_inpmast
where inpmast_vin = '1GCRYCEFXKZ177569';  

-- update hash on all old vin rows
update arkona.xfm_inpmast aa
set hash = bb.hash
from (
  select inpmast_key,
    (
        select md5(z::text) as hash
        from (
          select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
            status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
            year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
            turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
            radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
            date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
            warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
            gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
            key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
            inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
            sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
            common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
            updated_timestamp,stocked_company,engine_hours
          from arkona.xfm_inpmast 
          where inpmast_key = a.inpmast_key) z)
  from inpmast_keys a) bb      
where aa.inpmast_key = bb.inpmast_key;  
----------------------------------------------------------------
--/> 03/11/21 
----------------------------------------------------------------



----------------------------------------------------------------
--< 4/25/20 finally actually fix changed vins
----------------------------------------------------------------

vin changed on vehicle with 9 existing rows in xfm_inpmast

1. change row_thru_date & current_row, on last row of bad vin
2. update vin in the old rows
3. update hash in the old rows

old vin: 3GCUKSEJ6FG365070
new vin: 1GCVKREC8FZ365070

select *
from arkona.xfm_inpmast
where inpmast_vin = '1GCVKREC8FZ365070'
order by inpmast_key

-- the old rows
drop table if exists inpmast_keys;
create temp table inpmast_keys as
select inpmast_key
from arkona.xfm_inpmast
where inpmast_vin = '3GCUKSEJ6FG365070'

-- change row_from_date on last row for old vin
update arkona.xfm_inpmast
set row_thru_date = '04/23/2020',
    current_row = false
-- select * from arkona.xfm_inpmast
where inpmast_key = 804763;

-- change the vin on the old rows
update arkona.xfm_inpmast
set inpmast_vin = '1GCVKREC8FZ365070'
-- select * from arkona.xfm_inpmast
where inpmast_vin = '3GCUKSEJ6FG365070';

update arkona.xfm_inpmast aa
set hash = bb.hash
from (
  select inpmast_key,
    (
        select md5(z::text) as hash
        from (
          select inpmast_company_number,inpmast_vin,vin_last_6,inpmast_stock_number,inpmast_document_number,
            status,g_l_applied,type_n_u,bus_off_fran_code,service_fran_code,manufacturer_code,vehicle_code,
            year,make,model_code,model,body_style,color,trim,fuel_type,mpg,cylinders,truck,wheel_drive4wd,
            turbo,color_code,engine_code,transmission_code,ignition_key_code,trunk_key_code,keyless_code,
            radio_code,wheel_lock_code,dealer_code,location,odometer,date_in_invent,date_in_service,
            date_delivered,date_ordered,inpmast_sale_account,inventory_account,demo_name,warranty_months,
            warranty_miles,warranty_deduct,list_price,inpmast_vehicle_cost,option_package,license_number,
            gross_weight,work_in_process,inspection_month,odometer_actual,bopname_key,
            key_to_cap_explosion_data,co2_emission_code2,registration_date1,funding_expiration_date2,
            inspection_date3,drivers_side,free_flooring_period,ordered_status,publish_vehicle_info_to_web,
            sale,certified_used_car,last_service_date4,next_service_date5,dealer_code_imdlrcd,
            common_vehicle_id,chrome_style_id,created_user_code,updated_user_code,created_timestamp,
            updated_timestamp,stocked_company,engine_hours
          from arkona.xfm_inpmast 
          where inpmast_key = a.inpmast_key) z)
  from inpmast_keys a) bb      
where aa.inpmast_key = bb.inpmast_key;  


----------------------------------------------------------------
--/> 4/25/20 finally actually fix changed vins
----------------------------------------------------------------

/*
9/23/19
*/

select *
from arkona.xfm_inpmast a    
where vin_last_6 = '418410'

old: 5TDDZRFH1H5418410
new: 5TDDZRFH1HS418410

select inpmast_vin, bopname_key, odometer, inpmast_key, row_from_date, row_thru_date, current_row, hash
from (
select * from arkona.xfm_inpmast where inpmast_vin = '5TDDZRFH1H5418410'
union
select * from arkona.xfm_inpmast where inpmast_vin = '5TDDZRFH1HS418410') X
order by row_from_date

update arkona.xfm_inpmast
set row_thru_date = '09/21/2019', current_row = false
where inpmast_key = 768123;

update arkona.xfm_inpmast
set inpmast_vin =   '5TDDZRFH1HS418410'
where inpmast_vin = '5TDDZRFH1H5418410';

/*
9/14/19
*/

delete 
-- select *
from arkona.xfm_inpmast where inpmast_vin in ('001739384*','001739688*','001736054*','001740319*')

old: 1GC2KXC64D2380062
new: 1GC2KXCG4DZ380062

select inpmast_vin, bopname_key, odometer, inpmast_key, row_from_date, row_thru_date, current_row, hash
from (
select * from arkona.xfm_inpmast where inpmast_vin = '1GC2KXC64D2380062'
union
select * from arkona.xfm_inpmast where inpmast_vin = '1GC2KXCG4DZ380062') X
order by row_from_date

update arkona.xfm_inpmast
set row_thru_date = '09/12/2019', current_row = false
where inpmast_key = 705217;

-- now this works
update arkona.xfm_inpmast
set inpmast_vin =   '1GC2KXCG4DZ380062'
where inpmast_vin = '1GC2KXC64D2380062';


/*
9/11/19
failed TestExtInpmast

*/

              select *
                    from arkona.xfm_inpmast a
                    where inpmast_vin <> '2HKRM4H73EH722710'
                      and not exists (
                        select *
                        from arkona.ext_inpmast
                        where inpmast_vin = a.inpmast_vin)   

JA32V2FV5CV022736 -- old vin
JA32U2FU5CU022736 -- new vin                       
1120351 -- bopname_key

-- so it looks like inpmast, bopvref and sdprhdr both get updated as a result of the vin changing
select * from arkona.ext_bopvref where vin in ('JA32V2FV5CV022736','JA32U2FU5CU022736')

select * from arkona.ext_sdprhdr where vin in ('JA32V2FV5CV022736','JA32U2FU5CU022736') 

-- ERROR:  duplicate key value violates unique constraint "xfm_inpmast_inpmast_vin_row_thru_date_idx"
-- DETAIL:  Key (inpmast_vin, row_thru_date)=(JA32U2FU5CU022736, 9999-12-31) already exists.
update arkona.xfm_inpmast
set inpmast_vin =   'JA32U2FU5CU022736'
where inpmast_vin = 'JA32V2FV5CV022736'

select inpmast_vin, bopname_key, odometer, inpmast_key, row_from_date, row_thru_date, current_row, hash
from (
select * from arkona.xfm_inpmast where inpmast_vin = 'JA32U2FU5CU022736'
union
select * from arkona.xfm_inpmast where inpmast_vin = 'JA32V2FV5CV022736') X
order by row_from_date

update arkona.xfm_inpmast
set row_thru_date = '09/09/2019', current_row = false
where inpmast_key = 763353

-- now this works
update arkona.xfm_inpmast
set inpmast_vin =   'JA32U2FU5CU022736'
where inpmast_vin = 'JA32V2FV5CV022736'






-- did cleanup on 9/11/18
-- vins that no longer exists in inpmast
select length(a.inpmast_vin), a.*
from arkona.xfm_inpmast a
where not exists (
  select *
  from arkona.ext_inpmast
  where inpmast_vin = a.inpmast_vin)
order by inpmast_stock_number

-- for the luigi test
select count(inpmast_vin)
from arkona.xfm_inpmast a
where not exists (
  select *
  from arkona.ext_inpmast
  where inpmast_vin = a.inpmast_vin)

1.
-- 32851 had wrong vin
-- was 2G1WT58K889265599
-- should be 2G1WT58K889267899
-- one goofy from/thru
-- the old hashes will be wrong, don't believe that is a problem
-- select * from arkona.xfm_inpmast where inpmast_vin = '2G1WT58K889265599'
-- select * from arkona.xfm_inpmast where inpmast_stock_number = '32851a' order by inpmast_key

update arkona.xfm_inpmast
set row_thru_date = '05/09/2018'
where inpmast_key = 629748;

update arkona.xfm_inpmast
set inpmast_vin = '2G1WT58K889267899'
where inpmast_vin = '2G1WT58K889265599';   

2.
-- 32243B 1 row with bad vin, vin exists nowhere else, dates don't jive
-- just get rid of the row
-- select * from arkona.xfm_inpmast where inpmast_stock_number = '32243b' order by inpmast_key
-- select * from arkona.xfm_inpmast where inpmast_vin = 'KM8SRDHF6HU197612'

delete 
from arkona.xfm_inpmast
where inpmast_vin = 'KM8SRDHF6HU197612';

3. 
-- G34879 
-- in xfm with 2 vins
-- 3GNAXWEX6JL264358 - no longer exists inpmast or in tool, shows deleted in vauto
-- LRBFX3SXXKD011860 - correct vin for G34879
-- 
-- select * from arkona.xfm_inpmast where inpmast_stock_number = 'g34879' order by inpmast_key
-- select * from arkona.xfm_inpmast where inpmast_vin = '3GNAXWEX6JL264358'

delete
from arkona.xfm_inpmast
where inpmast_vin = '3GNAXWEX6JL264358';

4.
-- H11746
-- in xfm with 2 vins
-- 5FNYF6H99KB004947 no longer exists inpmast
-- 5FNYF6H92KB008595
-- select * from arkona.xfm_inpmast where inpmast_stock_number = 'H11746' order by inpmast_key 
-- select * from arkona.xfm_inpmast where inpmast_vin = '5FNYF6H99KB004947'

delete
from arkona.xfm_inpmast
where inpmast_vin = '5FNYF6H99KB004947';

5.
-- G33975A
-- in xfm with 3 vins
-- 1GNKVHD7FJ265548 : 16 digits
-- 1GNKVHKD0FJ192877 : actually 32961A, temporarily assigned to 33975A mistakenly, can be deleted
-- 1GNKVHKD7FJ265548
-- select * from arkona.xfm_inpmast where inpmast_stock_number = 'G33975A' order by inpmast_key

6.
-- H11450
-- 2HKRW6H36JY220691
-- 2HKRW6H36JH220691
-- select * from arkona.xfm_inpmast where inpmast_stock_number = 'H11450' order by inpmast_key
-- typo in vin

update arkona.xfm_inpmast
set inpmast_vin = '2HKRW6H36JH220691'
where inpmast_vin = '2HKRW6H36JY220691';



7.
-- get rid of those where length <> 17
delete 
from arkona.xfm_inpmast
where inpmast_vin in (
  select a.inpmast_vin
  from arkona.xfm_inpmast a
  where length(a.inpmast_vin) <> 17
    AND not exists (
      select *
      from arkona.ext_inpmast
      where inpmast_vin = a.inpmast_vin))

-- ok, here are the remaining vins that exists in xfm but no long exists in ext_inpmast
select length(a.inpmast_vin), a.*
from arkona.xfm_inpmast a
where not exists (
  select *
  from arkona.ext_inpmast
  where inpmast_vin = a.inpmast_vin)
order by inpmast_vin, inpmast_key

-- 12 vins remaining
select distinct a.inpmast_vin
from arkona.xfm_inpmast a
where not exists (
  select *
  from arkona.ext_inpmast
  where inpmast_vin = a.inpmast_vin)

-- there are ros on some of them (dim_vehicle based on inpmast exclusively)
-- that indicate the vin has been changed/corrected
select b.the_date, a.ro, c.vin
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
  and c.vin in (
    '1D7RV1GT7BS584618',  -- 1CGRR7LTXES151412  11 white ram 1500
    '1FTFX1EF9UFC80688',  -- 1FTFX1EF9DFC80688
    '1GCGTDE32G1184118',  -- 1GCGTDE38G1184110 16 white colorado
    '1GCVKREC8GZ209371',  -- 1GCVKREC9GZ209377 16 black silverado
    '1GCVKREC8GZ209377',  -- looks like this is another wrong version of above
    '1N4AL3AP0FL282470',  -- 1N4AL3AP0FC282470 15 black altima
    '3LNHU26T27R610251')  -- 3LNHM26T27R610251 07 lincoln mkz
group by b.the_date, a.ro, c.vin    
order by c.vin

-- none of these without ros exist in the tool, vehicle_orders or vauto
-- select * from arkona.xfm_inpmast where inpmast_vin in ( '1G11C5S33EF108691','1GCVKREC1JZ268768','1GTHK23648F106663','2LMDJ8JKBBBJ18317','4UBBS0P2031J35434')
-- select * from gmgl.vehicle_orders where vin in ( '1G11C5S33EF108691','1GCVKREC1JZ268768','1GTHK23648F106663','2LMDJ8JKBBBJ18317','4UBBS0P2031J35434')
-- select * from vauto.ext_finalized_appraisal_report where vin in ( '1G11C5S33EF108691','1GCVKREC1JZ268768','1GTHK23648F106663','2LMDJ8JKBBBJ18317','4UBBS0P2031J35434')
-- whack em all
delete from arkona.xfm_inpmast where inpmast_vin in ( '1G11C5S33EF108691','1GCVKREC1JZ268768','1GTHK23648F106663','2LMDJ8JKBBBJ18317','4UBBS0P2031J35434')


select * from arkona.ext_inpmast where inpmast_vin in ( '1GCGTDE38G1184110','1GCGTDE32G1184118') 

select * from arkona.xfm_inpmast where inpmast_vin in ( '3LNHM26T27R610251','3LNHU26T27R610251') order by row_from_date

-- and update the "corrected"vins or delete as the rest of the data dictates
delete from arkona.xfm_inpmast where inpmast_vin = '1D7RV1GT7BS584618'; 

delete from arkona.xfm_inpmast where inpmast_vin = '1FTFX1EF9UFC80688';

update arkona.xfm_inpmast
set row_thru_date =   '06/13/2018',
    current_row = false
where inpmast_key = 643307;
update arkona.xfm_inpmast
set inpmast_vin = '1GCGTDE38G1184110'
where inpmast_vin = '1GCGTDE32G1184118';

update arkona.xfm_inpmast
set row_thru_date =   '09/25/2017',
    current_row = false
where inpmast_key = 546358;
update arkona.xfm_inpmast
set row_thru_date =   '10/02/2017',
    current_row = false
where inpmast_key = 554163;
update arkona.xfm_inpmast
set inpmast_vin = '1GCVKREC9GZ209377'
where inpmast_vin in ( '1GCVKREC8GZ209371','1GCVKREC8GZ209377');

update arkona.xfm_inpmast
set row_thru_date =   '11/29/2018',
    current_row = false
where inpmast_key = 543352;
update arkona.xfm_inpmast
set inpmast_vin = '1N4AL3AP0FC282470'
where inpmast_vin = '1N4AL3AP0FL282470';

update arkona.xfm_inpmast
set row_thru_date =   '10/24/2018',
    current_row = false
where inpmast_key = 507021;
update arkona.xfm_inpmast
set inpmast_vin = '3LNHM26T27R610251'
where inpmast_vin = '3LNHU26T27R610251';

