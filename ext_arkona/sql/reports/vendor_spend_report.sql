﻿/*
01/10/2022
Good Afternoon! 

How are you! Hope you had a nice Christmas/NY!!! 😊
Can you please pull the Vendor spend report for 2021 for me when you have time to do so?  I will attach the one from 2020 so you know what I am referring to 😊
Thanks !!! 

June Gagnon

1. ran z:\E:\python_projects\ext_arkona\vendor_spend_report.py which truncates and populates arkona.vendor_spend
2. ran z:\E:\python_projects\ext_arkona\ext_glpcrmk.py 
3. ran z:\E:\python_projects\ext_arkona\ext_glpdba.py 
*/

/*
01/13/2023
In the past you have run this report for me – this year I will need the Toyota store also 😊    
I have attached the one that you ran in Jan of last year. The report should include all vendors we made payments to in the year 2022.

Toyota account number: 3000

*/

select * from arkona.vendor_spend

 
select name_record_key, string_agg(remarks, ' ' order by note_date, sequence_number)
from arkona.ext_glpcrmk
group by name_record_key

-- this table is populated by script at :E:\python projects\ext_arkona\vendor_spend_report.py

!!! That script needs to be run in the W10 dream !!!

drop table if exists arkona.vendor_spend;
create table arkona.vendor_spend (
  company_number citext, -- from glpcust
  record_key integer,
  control citext,
  vendor_number citext,
  vendor_name citext,
  taxid citext,
  ry1_amount numeric(9,2),
  ry2_amount numeric(9,2),
  ry8_amount numeric(9,2),
  address citext,
  city citext,
  state citext,
  zip citext,
  vendor_1099_type citext);

create table arkona.ext_glpdba (
  company_number citext,
  record_key integer, 
  dba_name citext,
  primary key (company_number,record_key));

  select * from arkona.ext_glpdba
  
/*
1/10/19  
  BEFORE RUNNING THIS QUERY:
    RUN EXT_GLPCRMK.PY 
    run ext.glpdba
*/
-- this generated the spreadsheet 10/10/18, & 1/10/19, 1/13/2020, 05/14/2021, 1/10/2022, 1/14/2023
-- saved file to E:\python_projects\ext_arkona\sql\reports\vendor_spend_2021.csv
select a.vendor_number, a.vendor_name, c.dba_name, a.taxid, a.ry1_amount, a.ry2_amount, a.ry8_amount,
   a.address, a.city, a.state, a.zip, 
  case  a.vendor_1099_type
    when '1' then 'Rent'
    when '2' then 'Royalties'
    when '3' then 'Other Income'
    when '6' then 'Medical & Health Care'
    when '7' then 'Nonemeployee Compensation'
    when '8' then 'Interest Income'
    when 'S' then 'Sole Proprietor'
    when '14' then 'Attorney'
    else 'Not a 1099 Vendor'
  end as "1099 Vendor Type",
  b.remarks
-- select *  
from arkona.vendor_spend  a
left join (
  select name_record_key, string_agg(remarks, ' ' order by note_date, sequence_number) as remarks
  from arkona.ext_glpcrmk
  group by name_record_key) b on a.record_key = b.name_record_key
left join arkona.ext_glpdba c on a.company_number = c.company_number  
  and a.record_key = c.record_key
order by vendor_name



