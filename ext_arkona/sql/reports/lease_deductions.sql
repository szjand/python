﻿-- lease_deductions report
-- requires pydeduct refresh

select company_number as store, employee_name as name, employee_number,
  fixed_ded_amt::integer as amount, 
  case
    when end_date = '12/31/9999' then null::date
    else end_date
  end
from (  
Select a.company_number, b.employee_name, a.employee_number, a.fixed_ded_amt, 
  a.bank_account, (select dds.db2_integer_to_date(a.bank_account)) as end_date
from arkona.ext_PYDEDUCT a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row
  and b.active_code <> 'T'
where a.ded_pay_code = '500'
order by a.company_number, b.employee_name) x


