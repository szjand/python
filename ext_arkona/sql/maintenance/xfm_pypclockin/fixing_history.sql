﻿/*
10/09/20
after going through all of this in this script
i have decided that the new extract, in jon.xfm_pypclockin is correct
and the existing arkona.xfm_pypclockin is wrong in many ways

easy full scrape of rydedata.pypclockin in    python project/ext_arkona/ext_pypclockin_history.py

so, right now, i am going to replace all the data in arkona.xfm_pypclockin with the data from jon.xfm_pypclockin
i will also be change the FUNCTION arkona.xfm_pypclockin() & FUNCTION arkona.xfm_pypclockin_email() 
*/
---------------------------------------------------------------------------------------
--< new data time
---------------------------------------------------------------------------------------

do $$
declare
  _the_year integer := 2020;
begin  
delete
from arkona.xfm_pypclockin
where extract(year from the_Date) = _the_year;

insert into arkona.xfm_pypclockin
select *
from jon.xfm_pypclockin
where extract(year from the_date) = _the_year;

end $$;


---------------------------------------------------------------------------------------
--/> new data time
---------------------------------------------------------------------------------------


select extract(year from yiclkind), count(*)
from arkona.ext_pypclockin
group by extract(year from yiclkind)
order by extract(year from yiclkind)

-- full year counts arkona vs jon
select *
from (
select extract(year from the_date) as the_year, count(*)
from jon.xfm_pypclockin
group by extract(year from the_date)
order by extract(year from the_date)) a
left join (
select extract(year from the_date) as the_year, count(*)
from arkona.xfm_pypclockin
group by extract(year from the_date)
order by extract(year from the_date)) b on a.the_year = b.the_year


-- hash and compare jon vs arkona
select * 
from (
  select employee_number, the_date, md5(a::text) as hash
  from jon.xfm_pypclockin a
  where extract(year from the_date) = 2020) a
join (
  select employee_number, the_date, md5(a::text) as hash
  from arkona.xfm_pypclockin a
  where extract(year from the_date) = 2020) b on a.employee_number = b.employee_number
    and a.the_date = b.the_date
where a.hash <> b.hash
order by a.the_date desc


select 'arkona', a.*
from arkona.xfm_pypclockin a
where employee_number = '1130090'
  and the_date = '09/05/2020'
union
select 'jon', a.*
from jon.xfm_pypclockin a
where employee_number = '1130090'
  and the_date = '09/05/2020'
union  
select 'wtf', yico_,pymast_employee_number,yiclkind,clock,reg,ot,vac,pto,hol
from wtf a
where pymast_employee_number = '1130090'
  and yiclkind = '09/05/2020'  

fuck me, looks like the fix is not the fix

select b.day_name, a.*
from wtf a
join dds.dim_date b on a.yiclkind = b.the_date
where pymast_employee_number = '1130090'
  and the_week = 870
order by yiclkind

select 7.99+8.16+8.05+8.15+7.65

select 'arkona', a.*
from arkona.xfm_pypclockin a
where employee_number = '1130090'
  and the_date between '08/31/2020' and '09/05/2020' 
  
on the contrary, looks like the fix is correct and arkona is more fucked up than i new
lets check for some recent (as being used in sap) dates
!!!BUT ADVANTAGE EDWCLOCKHOURSFACT SEEMS OK !!!

select sunday_to_saturday_week from dds.dim_date where the_date = '09/30/2020'

select b.day_name, a.*
from wtf a
join dds.dim_date b on a.yiclkind = b.the_date
where pymast_employee_number = '13725'
  and the_week = 874
order by yiclkind

select 'arkona', a.*
from arkona.xfm_pypclockin a
where employee_number = '13725'
  and the_date between '09/28/2020' and '10/02/2020' 

OK, the last 2 weeks look good
go ahead and update everything  
---------------------------------------------------------------------------------------------------
--< holy shit, jon is fucked up, clock hours only, no reg hours
---------------------------------------------------------------------------------------------------

reformulate this reg/ot part to simulate https://rickosborne.org/blog/2006/01/calculating-overtime-with-pure-sql/
so i can leave an explanation

select a.the_week, a.pymast_employee_number, a.yiclkind, coalesce(sum(b.clock_hours), 0) as hours_before,
  a.clock_hours, coalesce(sum(b.clock_hours), 0) + a.clock_hours as hours_after
from (
  select b.sunday_to_saturday_week as the_week, a.* 
  from arkona.xfm_pypclockin_tmp a
  join dds.dim_date b on a.yiclkind = b.the_date 
  WHERE pymast_employee_number = '13725') a
left join (
  select b.sunday_to_saturday_week as the_week, a.* 
  from arkona.xfm_pypclockin_tmp a
  join dds.dim_date b on a.yiclkind = b.the_date
  WHERE pymast_employee_number = '13725') b on a.pymast_employee_number = b.pymast_employee_number
    and a.the_week = b.the_week
    and b.yiclkind < a.yiclkind  
group by a.the_week, a.pymast_employee_number, a.yiclkind, a.clock_hours
order by a.yiclkind

-- now convert this to our actual usage
-- and it works great, this is the fix
drop table if exists wtf;
create temp table wtf as
select bb.the_week, aa.yico_, aa.pymast_employee_number, aa.yiclkind, coalesce(aa.clock_hours, 0) as clock, 
  coalesce(bb.reg_hours, 0) as reg, coalesce(bb.ot_hours, 0) as ot,
  coalesce(aa.vac_hours, 0) as vac, coalesce(aa.pto_hours, 0) as pto, 
  coalesce(aa.hol_hours, 0) as hol
from jon.xfm_pypclockin_tmp aa
left join (
  select a.yico_, a.pymast_employee_number, a.the_week, a.yiclkind,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) > 40 then 0
        else 40 - coalesce(sum(b.clock_hours), 0)
      end, 2) as reg_hours,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) >= 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then 0
        else a.clock_hours - (40 -coalesce(sum(b.clock_hours), 0))
      end, 2) as ot_hours   
  from (
    select b.sunday_to_saturday_week as the_week, a.* 
    from jon.xfm_pypclockin_tmp a
    join dds.dim_date b on a.yiclkind = b.the_date) a
  left join (
    select b.sunday_to_saturday_week as the_week, a.* 
    from jon.xfm_pypclockin_tmp a
    join dds.dim_date b on a.yiclkind = b.the_date) b on a.pymast_employee_number = b.pymast_employee_number
      and a.the_week = b.the_week
      and b.yiclkind < a.yiclkind  
  group by a.yico_, a.the_week, a.pymast_employee_number, a.yiclkind, a.clock_hours) bb on aa.yico_ = bb.yico_
    and aa.pymast_employee_number = bb.pymast_employee_number
    and aa.yiclkind = bb.yiclkind
order by yiclkind desc, pymast_employee_number asc


select * from dds.dim_Date where the_date = current_Date
select * from arkona.ext_pymast where employee_first_name = 'shawn'

select '01/06/2019'::date + interval '52 weeks'

12/25/2016  1/7/18

---------------------------------------------------------------------------------------------------
--/> holy shit, jon is fucked up, clock hours only, no reg hours
---------------------------------------------------------------------------------------------------
    
-- rows in jon not in arkona
-- 2017:10, 2018:7, 2019:24, 2020:18
select * 
from (
  select a.*
  from jon.xfm_pypclockin a
  where extract(year from the_date) = 2020) a
left join (
  select a.*
  from arkona.xfm_pypclockin a
  where extract(year from the_date) = 2020) b on a.employee_number = b.employee_number
    and a.the_date = b.the_date
where b.the_date is null 

---------------------------------------------------------------------------------------
--< rows in arkona not in jon 
-- checked in DB2, nothing there, so apparently, this clock event has been deleted
-- also nothing in pypclkl (corrections)
---------------------------------------------------------------------------------------
2020 : 1
select * 
from (
  select a.*
  from arkona.xfm_pypclockin a
  where extract(year from the_date) = 2020) a
left join (
  select a.*
  from jon.xfm_pypclockin a
  where extract(year from the_date) = 2020) b on a.employee_number = b.employee_number
    and a.the_date = b.the_date
where b.the_date is null   

select *
from jon.xfm_pypclockin_tmp
where pymast_employee_number = '168573'
  and yiclkind = '09/21/2020'

select *
from arkona.ext_pypclockin
where pymast_employee_number = '168573'
  and yiclkind = '09/21/2020'
---------------------------------------------------------------------------------------
--/> rows in arkona not in jon 
---------------------------------------------------------------------------------------


----------------------------------------------------------------------------
--< need to get the overlapping holiday stuff cleaned up
----------------------------------------------------------------------------
-- looks like the car wash folks don't have holiday pay here, these are all part time
select b.employee_name, b.distrib_code, b.active_code, a.*
from jon.xfm_pypclockin a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where the_date = '09/07/2020'
  and clock_hours <> 0
  and hol_hours = 0

-- some part timers have holiday pay (but they have 0 clock hours)
select b.employee_name, b.distrib_code, b.active_code, a.*
from jon.xfm_pypclockin a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where the_date = '09/07/2020'
  and hol_hours <> 0
  and b.active_code = 'P'

select *
from arkona.ext_pypclockin
where yiclkind = '09/07/2020'
  and pymast_employee_number in ('169753','198532','178060')

-- here are all the clock events that on the same day for the same emp have HOL hours and clock hours
select *
from (
  select bb.employee_name, bb.distrib_code, aa.pymast_employee_number, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin aa
  join arkona.ext_pymast bb on aa.pymast_employee_number = bb.pymast_employee_number
  where aa.yicode = 'HOL'
--     and aa.yiclkint = '08:00:00'
    and extract(year from aa.yiclkind) = 2020) a
join (
  select pymast_employee_number, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin
  where yicode = 'O') b on a.pymast_employee_number = b.pymast_employee_number and a.yiclkind = b.yiclkind
order by a.yiclkind, employee_name

select *
from (
  select bb.employee_name, bb.distrib_code, aa.pymast_employee_number, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin aa
  join arkona.ext_pymast bb on aa.pymast_employee_number = bb.pymast_employee_number
  where aa.yicode = 'HOL'
    and aa.yiclkind = '09/07/2020') a
join (
  select pymast_employee_number, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin
  where yicode = 'O') b on a.pymast_employee_number = b.pymast_employee_number and a.yiclkind = b.yiclkind
order by a.yiclkind, employee_name

select a.yico_, a.employee_name, a.distrib_code, a.pymast_employee_number, a.pymast_employee_number, a.yiclkind, 
  coalesce(sum((extract(epoch from (b.yiclkoutt - b.yiclkint))/3600.0)::numeric(6,2)) filter (where b.yicode in ('666','O')), 0) as clock_hours,
  coalesce(sum((extract(epoch from (a.yiclkoutt - a.yiclkint))/3600.0)::numeric(6,2)) filter (where a.yicode = 'HOL'), 0) as hol_hours
from (
  select bb.employee_name, bb.distrib_code, aa.pymast_employee_number, yico_, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin aa
  join arkona.ext_pymast bb on aa.pymast_employee_number = bb.pymast_employee_number
  where aa.yicode = 'HOL'
    and aa.yiclkint = '08:00:00'
    and extract(year from aa.yiclkind) = 2020) a
join (
  select pymast_employee_number, yico_, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin
  where yicode = 'O') b on a.pymast_employee_number = b.pymast_employee_number and a.yiclkind = b.yiclkind  
group by a.yico_, a.employee_name, a.distrib_code, a.pymast_employee_number, a.yiclkind  
order by a.yiclkind, a.employee_name


select * from jon.xfm_pypclockin
where employee_number = '1130420'
  and the_date = '09/07/2020'

ok wait a minute, allowing myself to get distracted
the issue is overlapping clock events that get bypassed, not to stay on task

eg abby sorum has clock hours and holiday hours on 9/7, but they both got picked up because they do not overlap 
checked out all the others in 2020, same thing, only the overlaps are missing

so lets see if we can get just the overlaps

select *
from (
  select bb.employee_name, bb.distrib_code, aa.pymast_employee_number, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin aa
  join arkona.ext_pymast bb on aa.pymast_employee_number = bb.pymast_employee_number
  where aa.yicode = 'HOL'
    and aa.yiclkind = '09/07/2020') a
join (
  select pymast_employee_number, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin
  where yicode = 'O') b on a.pymast_employee_number = b.pymast_employee_number and a.yiclkind = b.yiclkind
and (a.yiclkint between b.yiclkint and b.yiclkoutt or b.yiclkint between a.yiclkint and a.yiclkoutt)
order by a.pymast_employee_number

152510, 175839, 193542, 195675

select * 
from arkona.ext_pypclockin
where yiclkind = '09/07/2020'
  and pymast_employee_number = '175839'
order by yicode, yiclkint  

select yiclkint, yiclkint + interval '1 sec'
from arkona.ext_pypclockin 
limit 10
-- i am thinking this is a more rigorous test
select *
from (
  select bb.employee_name, bb.distrib_code, aa.pymast_employee_number, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin aa
  join arkona.ext_pymast bb on aa.pymast_employee_number = bb.pymast_employee_number
  where aa.yiclkind = '09/07/2020') a
join (
  select pymast_employee_number, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  from arkona.ext_pypclockin) b on a.pymast_employee_number = b.pymast_employee_number and a.yiclkind = b.yiclkind
and (a.yiclkint between b.yiclkint and b.yiclkoutt or b.yiclkint between a.yiclkint and a.yiclkoutt)
and a.yicode <> b.yicode
order by a.pymast_employee_number  

the issue is 175839, 
O: 07:41:15 - 14:02:04, 14:30:46 - 18:10:09
H: 03:41:00 - 07:41:00, 18:10:00 - 22:10:00
but jon.xfm_pypclockin shows clock_hours: 10.01, hol_hours: 8.00
as does arkona.xfm_pypclockin
second H event overlaps with the second O event, so how did it process?

lets look at the overlap exclusion (at this time only used in arkona)
note that this exclusion does not pick up all overlaps as indicated by 175839
but, 175839 processed correctly anyway (in spite of an overlap)
2 issues, why was it not picked up, why was it processed correctly
processed correctly because reg & ot are generated separately from clock,vac,pto,hol
clock,vac,pto & hol processed into xxx.xfm_pypclockin_tmp 
which is then left joined to the reg/ot generation, which, in arkona includes the overlap exclusion

    select yico_, pymast_employee_number, yiclkind, min(yiclkint) as min_in, max(yiclkint) as max_in, min(yiclkoutt) as min_out, max(yiclkoutt) as max_out
    from arkona.ext_pypclockin
    where yicode in ('HOL','666','O','PTO','VAC')
      and yiclkind in ('09/07/2020')
    group by yico_, pymast_employee_number, yiclkind
    having count(*) > 1
      and max(yiclkint) < min(yiclkoutt)


select * 
from arkona.ext_pypclockin
where yiclkind = '09/07/2020'
  and pymast_employee_number = '1130700'
order by yicode, yiclkint        

https://stackoverflow.com/questions/25733385/postgresql-query-to-detect-overlapping-time-ranges
select f1.*
from my_features f1
where exists (select 1
              from my_features f2
              where tsrange(f2.begin_time, f2.end_time, '[]') && tsrange(f1.begin_time, f1.end_time, '[]')
                and f2.feature_id = f1.feature_id
                and f2.id <> f1.id);

it appears the postresql cant cast a time to a timestamp, so need to create it

select *
from arkona.ext_pypclockin
where yiclkind = '09/07/2020'
  and pymast_employee_number = '1100625'
                  


-- finally, this returns all 10, the 6 exposed by the old exclude functionality plus the additional 4
-- that have overlaps, but process ok: 152510, 175839, 193542, 195675
-- so wtf, 
-- omg, the other overlaps included in the old overlap, 1130700, 137593, 147852, 165748, 165972, 289563
-- ARE ALL FUCKING PROCESSED CORRECTLY IN JON, that means there was no need to exclude them
-- now what
-- leaning toward no overlap exclusions at all
-- still need someway to verify clock hour totals

select distinct a.pymast_employee_number
from arkona.ext_pypclockin a
where yiclkind = '09/07/2020'     
-- and pymast_employee_number = '1130700'
  and exists (
    select 1
    from arkona.ext_pypclockin b  
    where tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
          && -- overlaps
          tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')
      and tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
          <>-- not equal
            tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')                    
      and b.pymast_employee_number = a.pymast_employee_number
      and b.yiclkind = a.yiclkind)        
order by pymast_employee_number


select * 
from jon.xfm_pypclockin_tmp
where yiclkind = '09/07/2020'
  and pymast_employee_number in ('1130700', '137593', '147852', '165748', '165972', '289563')  

select * from jon.xfm_pypclockin
where the_date = '09/07/2020'
  and employee_number in ( '152510', '175839', '193542', '195675')


-- 10/04
lets see if my theory is true for 5/25/20
if true, in spite of overlaps, all folks with both hol hours and clock hours should be accurately represented 
in jon.xfm_pypclockin  

-- hooray, looks good, it's all here
select * from jon.xfm_pypclockin
where the_date = '05/25/2020'
  and employee_number in (
select distinct a.pymast_employee_number
from arkona.ext_pypclockin a
where yiclkind = '05/25/2020'     
-- and pymast_employee_number = '1130700'
  and exists (
    select 1
    from arkona.ext_pypclockin b  
    where tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
          && -- overlaps
          tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')
      and tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
            <> -- not equal
            tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')                    
      and b.pymast_employee_number = a.pymast_employee_number
      and b.yiclkind = a.yiclkind))        
order by employee_number


-- ads.ext vs jon close but ...
select *
from (
select b.year_month, sum(reg_hours+ot_hours) as clock, sum(reg_hours) as reg, sum(ot_hours) as ot, sum(vac_hours) as vac, sum(pto_hours) as pto, sum(hol_hours) as hol
from jon.xfm_pypclockin a
join dds.dim_date b on a.the_date = b.the_date
  and b.the_year = 2018
group by b.year_month) aa
join (
select b.year_month, sum(regularhours+overtimehours) as clock, sum(regularhours) as reg,sum(overtimehours) as ot, sum(vacationhours) as vac,sum(ptohours) as pto,sum(holidayhours) as hol
from ads.ext_edw_clock_hours_fact a
join dds.dim_date b on a.datekey = b.date_key
  and b.the_year = 2018
group by year_month) bb on aa.year_month = bb.year_month
order by aa.year_month  


select min(b.the_Date), max(b.the_date)
from ads.ext_edw_clock_hours_fact a
join dds.dim_date b on a.datekey = b.date_key


select * 
from jeri.clock_hour_tracking
where year_month = 202007
limit 100

select * 
from jeri.hourly_employees   
limit 100

-- this is the query that populates jeri.clock_hour_Tracking BASED ON ARKONA.XFM_PYPCLOCKIN
-- modify it to use jon.xfm_pypclockin, then compare it to jeri.clock_hour_7racking
select b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, max(b.employee_name), b.employee_number,
  sum(a.reg_hours) as reg_hours, sum(a.ot_hours) as ot_hours, 
  sum(ot_hours * 1.5 * hourly_rate) as ot_comp
from jon.xfm_pypclockin a
inner join dds.dim_date aa on a.the_date = aa.the_date
  and aa.year_month = 202007
inner join jeri.hourly_employees b on a.employee_number = b.employee_number
  and a.the_date between b.ek_from_date and b.ek_thru_date
  -- 7/7 add term date
  and a.the_date <= b.term_date
where a.the_date between '01/01/2017' and current_date
group by  b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, /*b.employee_name,*/ b.employee_number      

-- just what i expected
-- 202007 arkona does not break out reg & ot, 202009 looks bpretty good, although the ot calc may be different
select x.*, y.reg_hours, y.ot_hours, y.reg_hours + y.ot_hours
from (
  select * 
  from jeri.clock_hour_tracking
  where year_month = 202009) x
join (
select b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, max(b.employee_name), b.employee_number,
  sum(a.reg_hours) as reg_hours, sum(a.ot_hours) as ot_hours, 
  sum(ot_hours * 1.5 * hourly_rate) as ot_comp
from jon.xfm_pypclockin a
inner join dds.dim_date aa on a.the_date = aa.the_date
  and aa.year_month = 202009
inner join jeri.hourly_employees b on a.employee_number = b.employee_number
  and a.the_date between b.ek_from_date and b.ek_thru_date
  -- 7/7 add term date
  and a.the_date <= b.term_date
where a.the_date between '01/01/2017' and current_date
group by  b.store_Code, b.department, b.dist_code, b.dist_code_description, 
  aa.the_year, aa.year_month, /*b.employee_name,*/ b.employee_number) y on x.employee_number = y.employee_number


 -- for comparison to attendance time card reports
select * from jon.xfm_pypclockin limit 10

-- oct jon 20 reg hours short
-- 8/30 - 9/26
select a.employee_number, b.employee_name, sum(reg_hours) as reg, sum(ot_hours) as ot, sum(clock_hours) as clock, 
  sum(vac_hours) as vac, sum(pto_hours) as pto, sum(hol_hours) as hol,
  sum(reg_hours+ot_hours+vac_hours+pto_hours+hol_hours) as total
from jon.xfm_pypclockin a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where the_date between '09/01/2020' and  '09/30/2020'
  and store_code = 'ry1'
group by a.employee_number, b.employee_name  
order by b.employee_name

-- import dt attendance monthly summary report
drop table if exists arkona.dt_attendance_monthly_summary_report cascade;
create table arkona.dt_attendance_monthly_summary_report (
  employee_number citext,
  name citext,
  reg numeric,
  ot numeric,
  clock numeric,
  vac numeric,
  pto numeric,
  hol numeric,
  cmp numeric,
  other numeric,
  total numeric);

-- perfect fucking match all fields on jon
select *
from arkona.dt_attendance_monthly_summary_report aa
left join (
select a.employee_number, b.employee_name, sum(reg_hours) as reg, sum(ot_hours) as ot, sum(clock_hours) as clock, 
  sum(vac_hours) as vac, sum(pto_hours) as pto, sum(hol_hours) as hol,
  sum(reg_hours+ot_hours+vac_hours+pto_hours+hol_hours) as total
from jon.xfm_pypclockin a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where the_date between '09/01/2020' and  '09/30/2020'
  and store_code = 'ry1'
group by a.employee_number, b.employee_name) bb on aa.employee_number = bb.employee_number
where aa.ot <> bb.ot
  or aa.clock <> bb.clock
  or aa.total <> bb.total

-- 106 discrepancies with arkona, ot is wrong, but total is correct
select *
from arkona.dt_attendance_monthly_summary_report aa
left join (
select a.employee_number, b.employee_name, sum(reg_hours) as reg, sum(ot_hours) as ot, sum(clock_hours) as clock, 
  sum(vac_hours) as vac, sum(pto_hours) as pto, sum(hol_hours) as hol,
  sum(reg_hours+ot_hours+vac_hours+pto_hours+hol_hours) as total
from arkona.xfm_pypclockin a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where the_date between '09/01/2020' and  '09/30/2020'
  and store_code = 'ry1'
group by a.employee_number, b.employee_name) bb on aa.employee_number = bb.employee_number
where aa.ot <> bb.ot
  or aa.clock <> bb.clock
  or aa.total <> bb.total  


  -- lets look at a 3 month report, 6/20 - 8/20

  -- import dt attendance monthly summary report
drop table if exists arkona.dt_attendance_monthly_summary_report cascade;
create table arkona.dt_attendance_monthly_summary_report (
  employee_number citext,
  name citext,
  month integer,
  reg numeric,
  ot numeric,
  clock numeric,
  vac numeric,
  pto numeric,
  hol numeric,
  cmp numeric,
  other numeric,
  total numeric);


select *
from arkona.dt_attendance_monthly_summary_report aa
left join (
select a.employee_number, b.employee_name, sum(reg_hours) as reg, sum(ot_hours) as ot, sum(clock_hours) as clock, 
  sum(vac_hours) as vac, sum(pto_hours) as pto, sum(hol_hours) as hol,
  sum(reg_hours+ot_hours+vac_hours+pto_hours+hol_hours) as total
from jon.xfm_pypclockin a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where the_date between '09/01/2020' and  '09/30/2020'
  and store_code = 'ry1'
group by a.employee_number, b.employee_name) bb on aa.employee_number = bb.employee_number
where aa.ot <> bb.ot
  or aa.clock <> bb.clock
  or aa.total <> bb.total

-- including the yiclkoutd and generating a timestamp fixed all anomalies except nick neumann
select *
from (
select max(employee_number), name, sum(reg) as reg, sum(ot) as ot, sum(clock) as clock, sum(vac) as vac,
  sum(pto) as pto, sum(hol) as hol, sum(total) as total
from arkona.dt_attendance_monthly_summary_report    
group by name) aa
join (
select max(employee_number), b.employee_name, sum(reg_hours) as reg, sum(ot_hours) as ot, sum(clock_hours) as clock, 
  sum(vac_hours) as vac, sum(pto_hours) as pto, sum(hol_hours) as hol,
  sum(reg_hours+ot_hours+vac_hours+pto_hours+hol_hours) as total
from jon.xfm_pypclockin a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where the_date between '06/01/2020' and  '08/31/2020'
  and store_code = 'ry1'
group by b.employee_name) bb on aa.name = bb.employee_name
where aa.ot <> bb.ot
  or aa.clock <> bb.clock
  or aa.total <> bb.total

1. nick neumann has no clock hours in report
attendance shows only pto & hol
same thing in rydedata.pypclockin
select * from arkona.xfm_pymast where pymast_employee_number = '1102198' order by pymast_key
in payroll, he is bi-weekly salaried, each check just shows 80 hours
check with andrew
----------------------- this brings up the possibility of deleted clock hours -------------------------------------------
select *
from arkona.dt_attendance_monthly_summary_report 
where name like 'NEUMANN%'    



-----------------
-- ok, now the full year of 2018
-- 47 anomalies out of 2900 rows
-- all but 2, dt shows more hours
1. tom aubol 80 hours vac added to clock months after it was taken
2. same thing a michael, 48 hours
every other one i look at is some fucked up data, 4 clock events on a single day
fuck it, it is history, so be it
select *, aa.total - bb.total
-- select sum(aa.total - bb.total)
from (
select max(employee_number), name, sum(reg) as reg, sum(ot) as ot, sum(clock) as clock, sum(vac) as vac,
  sum(pto) as pto, sum(hol) as hol, sum(total) as total
from arkona.dt_attendance_monthly_summary_report    
group by name) aa
join (
select max(employee_number), b.employee_name, sum(reg_hours) as reg, sum(ot_hours) as ot, sum(clock_hours) as clock, 
  sum(vac_hours) as vac, sum(pto_hours) as pto, sum(hol_hours) as hol,
  sum(reg_hours+ot_hours+vac_hours+pto_hours+hol_hours) as total
from jon.xfm_pypclockin a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where the_date between '01/01/2018' and  '12/31/2018'
  and store_code = 'ry1'
group by b.employee_name) bb on aa.name = bb.employee_name
where aa.ot <> bb.ot
  or aa.clock <> bb.clock
  or aa.reg <> bb.reg
  or aa.total <> bb.total
  or aa.vac <> bb.vac
  or aa.pto <> bb.pto
  or aa.hol <> bb.hol
order by aa.total - bb.total desc

select sum(total) from arkona.dt_attendance_monthly_summary_report    -- 486 hours diff of 415035 total

so, do i reload 2018 ext_pypclockin and see the diff?
yep, lets do it 


CREATE TABLE jon.ext_pypclockin_2018_test
(
  yico_ citext, -- YICO# : Company Number
  pymast_employee_number citext, -- YIEMP# : EMPLOYEE #
  yiclkind date, -- YICLKIND : DATE IN
  yiclkint time without time zone, -- YICLKINT : TIME IN
  yiclkoutd date, -- YICLKOUTD : DATE OUT
  yiclkoutt time without time zone, -- YICLKOUTT : TIME OUT
  yistat citext, -- YISTAT : ATTENDANCE STATUS
  yiinfo citext, -- YIINFO : MSG TEXT
  yicode citext, -- YICODE : STATUS
  yiusridi citext, -- YIUSRIDI : USERID IN
  yiwsidi citext, -- YIWSIDI : WSID IN
  yiusrido citext, -- YIUSRIDO : USERID OUT
  yiwsido citext); -- YIWSIDO : WSID OUT

that changes things dramatically, 2 anomalies for a total of 1.86 hours, 

select count(*) from jon.ext_pypclockin_2018_test
 select *, aa.total - bb.total
-- select sum(aa.total - bb.total)
from (
select max(employee_number), name, sum(reg) as reg, sum(ot) as ot, sum(clock) as clock, sum(vac) as vac,
  sum(pto) as pto, sum(hol) as hol, sum(total) as total
from arkona.dt_attendance_monthly_summary_report    
group by name) aa
join (
select max(employee_number), b.employee_name, sum(reg_hours) as reg, sum(ot_hours) as ot, sum(clock_hours) as clock, 
  sum(vac_hours) as vac, sum(pto_hours) as pto, sum(hol_hours) as hol,
  sum(reg_hours+ot_hours+vac_hours+pto_hours+hol_hours) as total
from jon.xfm_pypclockin a
join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
where the_date between '01/01/2018' and  '12/31/2018'
  and store_code = 'ry1'
group by b.employee_name) bb on aa.name = bb.employee_name
where aa.ot <> bb.ot
  or aa.clock <> bb.clock
  or aa.reg <> bb.reg
  or aa.total <> bb.total
  or aa.vac <> bb.vac
  or aa.pto <> bb.pto
  or aa.hol <> bb.hol
order by aa.total - bb.total desc

-- FACT: rescraping pypclockin is worthwhile to establish clean history

come up with a query to simulate the max sunday in the previous years as from and the min saturday in the following year as thru

select 
  (select min(the_date)
   from dds.dim_date
   where the_year = a.the_year) as from_date,
  (select max(the_date)
   from dds.dim_date
   where the_year = a.the_year) as from_date
from  (     
  select distinct the_year      
  from dds.dim_date 
  where the_year 
    between 
      (select the_year from dds.dim_date where the_date = current_Date) - 11 
    and 
      (select the_year from dds.dim_date where the_date = current_Date) - 3) a


-- FACT: i can still test for overlaps and dups, but no need to exclude them from processing
--  when i get an email, verify that the overlaps have been processed correctly,
-- notify relevant managers of dups

so, now i am going to re-scrape all of pypclockin into jon.ext_pypclockin

  yiwsido citext); -- YIWSIDO : WSID OUT

 drop table if exists jon.ext_pypclockin; 
CREATE TABLE jon.ext_pypclockin(
  yico_ citext, -- YICO# : Company Number
  pymast_employee_number citext, -- YIEMP# : EMPLOYEE #
  yiclkind date, -- YICLKIND : DATE IN
  yiclkint time without time zone, -- YICLKINT : TIME IN
  yiclkoutd date, -- YICLKOUTD : DATE OUT
  yiclkoutt time without time zone, -- YICLKOUTT : TIME OUT
  yistat citext, -- YISTAT : ATTENDANCE STATUS
  yiinfo citext, -- YIINFO : MSG TEXT
  yicode citext, -- YICODE : STATUS
  yiusridi citext, -- YIUSRIDI : USERID IN
  yiwsidi citext, -- YIWSIDI : WSID IN
  yiusrido citext, -- YIUSRIDO : USERID OUT
  yiwsido citext); -- YIWSIDO : WSID OUT

select extract(year from yiclkind), count(*)
from jon.ext_pypclockin
group by extract(year from yiclkind)  
order by extract(year from yiclkind)  

select count(*)
from arkona.ext_pypclockin
where extract(year from yiclkind) >= 2020

select *
from jon.ext_pypclockin
where yiclkind > current_date

select extract(year from the_date), count(*)
from jon.xfm_pypclockin
group by extract(year from the_date)

-- FACT jon.xfm_pypclockin has been updated with fresh scrape of pypclockin on 10/5/20
----------------------------------------------------------------------------
--/> need to get the overlapping holiday stuff cleaned up
----------------------------------------------------------------------------




  