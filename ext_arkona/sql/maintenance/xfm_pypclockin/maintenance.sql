﻿1. overlapping clock events
2. dup clock events
3. deleted rows
4. clockin > clockout
------------------------------------------------------------------------------
--< 1. overlapping clock events
------------------------------------------------------------------------------
10/10/2020
the notion here is that while multiple clock transactions on a single day for a single person overlapping
normally indicates a problem, car wash, for example, work on holidays, so, they generate both
regular clock hours as well as holiday hours. it appears that the rewritten function arkona.xfm_pypclockin
processes these clock events correctly, ie, for the date, generates both clock hours and holiday hours,
it bears checking
and then, of course, there are those overlapping clock events that are mistakes


-- 10/10/20
-- put this into function arkona.xfm_pypclockin_email()
select count(distinct b.employee_name)::integer
from arkona.ext_pypclockin_tmp a
join arkona.ext_pymast b on a.pymast_employee_number = b.pymast_employee_number
where a.yiclkind not in ('09/07/2020','07/28/2020','07/15/2020')
  and (a.yiclkind <> '11/20/2020' and a.pymast_employee_number <> '191060')
  and (a.yiclkind <> '11/27/2020' and a.pymast_employee_number <> '196341')  -- kim clocked an hour on pto day
  and (a.yiclkind <> '01/12/2021' and a.pymast_employee_number <> '291582')
  and (a.yiclkind <> '02/22/2021' and a.pymast_employee_number <> '132987')
  and (a.yiclkind <> '02/26/2021' and a.pymast_employee_number <> '149878') -- another stupid 2 minute overlap
  and (a.yiclkind <> '03/12/2021' and a.pymast_employee_number <> '152499')  -- conner clocked 2 hhours on pto day
  and (a.yiclkind <> '03/01/2021' and a.pymast_employee_number <> '264110')  -- ry2 waiting for nick
  and (a.yiclkind <> '04/06/2021' and a.pymast_employee_number <> '150126')  -- small pto overlap
  and (a.yiclkind <> '04/16/2021' and a.pymast_employee_number <> '1117960')  -- rogne, salaried, doesn't matter
  and (a.yiclkind <> '05/31/2021' and a.pymast_employee_number not in ('165748','172581','185462'))  -- BDC memorial day 2021
  and exists (
    select 1
    from arkona.ext_pypclockin_tmp b  
    where tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
          && -- overlaps
          tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')
      and tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
          <>-- not equal
            tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')                    
      and b.pymast_employee_number = a.pymast_employee_number
      and b.yiclkind = a.yiclkind
      and b.yicode not in ('I','666'))  
 and a.yicode not in ('I','666') -- exclude auto deduct (negative duration) and clock events with no clock out       

-- the query to expose the offending records
select b.employee_name as employee, a.pymast_employee_number as "emp #", a.yiclkind as date, a.yiclkint as "clock in",
  a.yiclkoutt as "clock out", a.yicode as code, d.employee_name as "mgr name"
from arkona.ext_pypclockin_tmp a
join arkona.ext_pymast b on a.pymast_employee_number = b.pymast_employee_number
join arkona.ext_pyprhead c on a.pymast_employee_number = c.employee_number
join arkona.ext_pymast d on c.employee_number_yrmgrn = d.pymast_employee_number
where a.yiclkind not in ('09/07/2020','07/28/2020','07/15/2020', '05/31/2021')
    and not (a.yiclkind in ('07/30/2021', '06/21/2021', '09/23/2021') and a.pymast_employee_number = '111210') -- another stupid 5 minute overlap
    and not (a.yiclkind = '08/06/2021' and a.pymast_employee_number = '154976')  -- bina, 7 min pto overlap
    and not (a.yiclkind = '10/19/2021' and a.pymast_employee_number = '295435')
    and not (a.yiclkind = '10/13/2021' and a.pymast_employee_number = '263975')
  and exists (
    select 1
    from arkona.ext_pypclockin_tmp b  
    where tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
          && -- overlaps
          tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')
      and tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
          <>-- not equal
            tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')                    
      and b.pymast_employee_number = a.pymast_employee_number
      and b.yiclkind = a.yiclkind
      and b.yicode not in ('I','666'))  
  and a.yicode not in ('I','666') -- exclude auto deduct (negative duration) and clock events with no clock out  
order by a.pymast_employee_number, a.yiclkind, a.yiclkint


-- 10/13/20
  all the labor day 2020 overlaps now show up, in addition to 2 oldies (having gone to replacing 100 days of data in xfm_pypclockin)
-- this query exposes that status in xfm_pypclockin of the overlapping clock events
-- what i am looking for is, did the folks working on a holiday get paid
-- for 9/7, it all looks good, so exclude that date from the overlap test
-- for the oldies, oh well, they got overpaid, too long ago
select cc.employee_name, aa.*
from arkona.xfm_pypclockin aa
join arkona.ext_pymast cc on aa.employee_number = cc.pymast_employee_number
join (
  select distinct a.pymast_employee_number, a.yiclkind
  from arkona.ext_pypclockin_tmp a
  join arkona.ext_pymast b on a.pymast_employee_number = b.pymast_employee_number
  where a.yiclkind not in ('09/07/2020','07/28/2020','07/15/2020', '05/31/2021')
    and not (a.yiclkind = '10/23/2020' and a.pymast_employee_number = '274225') -- vacation, came in for 1 hour
    and not (a.yiclkind = '11/20/2020' and a.pymast_employee_number = '191060')
    and not (a.yiclkind = '11/27/2020' and a.pymast_employee_number = '196341') -- kim clocked an hour on pto day
    and not (a.yiclkind = '01/12/2021' and a.pymast_employee_number = '291582') -- trivial 2 minute overlap
    and not (a.yiclkind = '02/22/2021' and a.pymast_employee_number = '149878') -- trivial 4 minute overlap
    and not (a.yiclkind = '02/26/2021' and a.pymast_employee_number = '132987') -- trivial 2 minute overlap
    and not (a.yiclkind = '02/26/2021' and a.pymast_employee_number = '149878') -- another stupid 2 minute overlap
    and not (a.yiclkind in ('06/08/2021', '03/12/2021') and a.pymast_employee_number = '152499')  -- conner clocked 2 hhours on pto day
    and not (a.yiclkind = '03/01/2021' and a.pymast_employee_number = '264110')  -- ry2 waiting for nick
    and not (a.yiclkind = '04/06/2021' and a.pymast_employee_number = '150126')  -- small pto overlap
    and not (a.yiclkind = '04/16/2021' and a.pymast_employee_number = '1117960')  -- rogne, salaried, doesn't matter
    and not (a.yiclkind in ('07/30/2021', '06/21/2021') and a.pymast_employee_number = '111210') -- another stupid 5 minute overlap
    and exists (
      select 1
      from arkona.ext_pypclockin_tmp b  
      where tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
            && -- overlaps
            tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')
        and tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                        to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
            <>-- not equal
              tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                        to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')                    
        and b.pymast_employee_number = a.pymast_employee_number
        and b.yiclkind = a.yiclkind
        and b.yicode not in ('I','666'))  
    and a.yicode not in ('I','666')) bb on aa.employee_number = bb.pymast_employee_number 
      and aa.the_date = bb.yiclkind

  
------------------------------------------------------------------------------
--/> 1. overlapping clock events
------------------------------------------------------------------------------


------------------------------------------------------------------------------
--< 2. dup clock events
------------------------------------------------------------------------------
168264 Matthew Hanson has 2 clock events on 10/05/20  both are 16:00:00 - 20:00:00
this does not show up in advantage because in the processing of rydedata.pypclockin into tmpPypclockin
  the raw data is grouped
the question now is, what does the dealertrack time clock show  
  in the attendance app view detail, both clock events show up
  the attendance clock_in detail report shows both events and totals them
Conclusion: these need to be fixed
  
-- this is the query that detects dups:
  select yico_, pymast_employee_number, yiclkind, yiclkint, yicode
  from arkona.ext_pypclockin_tmp
  where yiclkind not in ('07/15/2020')
  group by yico_, pymast_employee_number, yiclkind, yiclkint, yicode
  having count(*) > 1

-- this is the query to generate the email body for the manager
  select c.employee_name as employee, a.pymast_employee_number as "emp #", a.yiclkind as date, a.yiclkint as "clock in", 
    a.yiclkoutt as "clock out", b.yicode as code, e.employee_name as "mgr name"
  from arkona.ext_pypclockin_tmp a
  join (
    select yico_, pymast_employee_number, yiclkind, yiclkint, yicode
    from arkona.ext_pypclockin_tmp
    where yiclkind not in ('07/15/2020')
    group by yico_, pymast_employee_number, yiclkind, yiclkint, yicode
    having count(*) > 1) b on a.pymast_employee_number = b.pymast_employee_number and a.yiclkind = b.yiclkind
  join arkona.ext_pymast c on a.pymast_employee_number = c.pymast_employee_number
  join arkona.ext_pyprhead d on a.pymast_employee_number = d.employee_number
  join arkona.ext_pymast e on d.employee_number_yrmgrn = e.pymast_employee_number
  
------------------------------------------------------------------------------
--/> 2. dup clock events
------------------------------------------------------------------------------

------------------------------------------------------------------------------
--< 3. deleted rows
------------------------------------------------------------------------------

create index on arkona.ext_pypclockin (yiclkind);
create index on arkona.ext_pypclockin (pymast_employee_number);

select aa.employee_name, a.*
from arkona.xfm_pypclockin a
join arkona.ext_pymast aa on a.employee_number = aa.pymast_employee_number
left join arkona.ext_pypclockin b on a.the_date = b.yiclkind
  and a.employee_number = b.pymast_employee_number
where a.the_date > '12/31/2019' 
  and b.yiclkind is null  
order by the_date  


this is expanding, ie fucking nick shireck, months after the fact added 48 hours of pto from bryn seay 1124625
so
added pypclkl (timeclock corrections) to ext_arkona in luigi
now need to figure out how to sort this all out

select *
from arkona.ext_pypclkl
limit 100

select c.employee_name, a.*, b.*
from arkona.xfm_pypclockin a
join arkona.ext_pypclkl b on a.employee_number = b.pymast_employee_number
  and a.the_date = b.ylclkind
  and b.ylchgc = 'DEL'
join arkona.ext_pymast c on a.employee_number = c.pymast_employee_number  
where extract(year from a.the_date) = 2020 
order by a.the_date

select distinct yiclkind from arkona.xfm_pypclockin_tmp where yiclkind <= current_date order by yiclkind

select current_Date - 32

select min(yiclkind) from arkona.ext_pypclockin_tmp
FACT: it is freak out time, FUNCTION arkona.xfm_pypclockin() i think i should be deleting 32 days worth of data from arkona.xfm_pypclockin
in addition i totally fucked up the notion of arkona.ext_pypclockin being accurate, each day delete 32 days worth of data only, 
and refill with arkona.ext_pypclockin_tmp, which does not handle deletions or any changes older than 32 days
so, that is a mess
 *** but, can recreate arkona.ext_pypclockin at any time ***

 looking at pypclkl is making my head swim, add the 32 days deletion to the function for now


 select *
 from arkona.xfm_pypclockin a
 left join arkona.xfm_pypclockin_tmp b on a.the_date = b.yiclkind
   and a.employee_number = b.pymast_employee_number
 where a.the_date between current_date - 32 and current_date
   and b.yiclkind is null

select * 
from arkona.ext_pypclkl 
where ylclkind > '08/31/2020'


select ylchgc, count(*)
from arkona.ext_pypclkl
group by ylchgc

UPB : before
UPP : after 

select * 
from arkona.ext_pypclkl a
left join arkona.xfm_pypclockin b on a.ylclkind = b.the_Date
  and a.pymast_employee_number = b.employee_number
where a.ylclkind > '08/31/2020'
  and b.the_date is null 
  and a.ylchgc = 'ADD'


select * from arkona.ext_pypclkl where pymast_employee_number = '17534' and ylclkind = '09/02/2020'

FACT: all of which leads me to say fuck pypclkl, any hope of accuracy will probably depend on updating
exp_pypclockin retroactively (pypclkl updates pypclockin)
but i do not know what that looks like yet
maybe after regular processing
download a years worth of pypclockin, process it into xfm_pypclockin form then compare
why the double duty
each night pypclockin for the past year instead of 32 days
yes, by god, do it starting tonight
update arkona.ext_pypclockin
as well as xfm_pypclockin
ok
final answer, at least for now, changed ext_pypclockin to be 100 days of data
-- actually 100 days is good enough
select ylchgd - ylclkind, count(*)
from arkona.ext_pypclkl
group by ylchgd - ylclkind
order by ylchgd - ylclkind




------------------------------------------------------------------------------
--/> 3. deleted rows
------------------------------------------------------------------------------

------------------------------------------------------------------------------
--< 4. clockin > clockout
------------------------------------------------------------------------------
psycopg2.errors.DataException: range lower bound must be less than or equal to range upper bound
CONTEXT:  SQL function "xfm_pypclockin_email" statement 1

select *
from arkona.ext_pypclockin_tmp
where 
  to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss') > to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss')
  and yicode not in ('I','666')
      
select * from arkona.ext_pymast where pymast_employee_number = '159873'


select b.employee_name as employee, a.pymast_employee_number as "emp #", a.yiclkind as date, a.yiclkint as "clock in",
  a.yiclkoutt as "clock out", a.yicode as code, d.employee_name as "mgr name"
from arkona.ext_pypclockin_tmp a
join arkona.ext_pymast b on a.pymast_employee_number = b.pymast_employee_number
join arkona.ext_pyprhead c on a.pymast_employee_number = c.employee_number
join arkona.ext_pymast d on c.employee_number_yrmgrn = d.pymast_employee_number
where a.yiclkind not in ('09/07/2020','07/28/2020','07/15/2020')
  and to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss') > to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss')
  and a.yicode not in ('I','666') -- exclude auto deduct (negative duration) and clock events with no clock out  
order by a.pymast_employee_number, a.yiclkind, a.yiclkint

------------------------------------------------------------------------------
--/> 4. clockin > clockout
------------------------------------------------------------------------------



-- the query to expose the offending records
select b.employee_name as employee, a.pymast_employee_number as "emp #", a.yiclkind as date, a.yiclkint as "clock in",
  a.yiclkoutt as "clock out", a.yicode as code, d.employee_name as "mgr name"
from arkona.ext_pypclockin_tmp a
join arkona.ext_pymast b on a.pymast_employee_number = b.pymast_employee_number
join arkona.ext_pyprhead c on a.pymast_employee_number = c.employee_number
join arkona.ext_pymast d on c.employee_number_yrmgrn = d.pymast_employee_number
where a.yiclkind not in ('09/07/2020','07/28/2020','07/15/2020')
  and (a.yiclkind <> '11/20/2020' and a.pymast_employee_number <> '191060')
  and exists (
    select 1
    from arkona.ext_pypclockin_tmp b  
    where tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
          && -- overlaps
          tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                    to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')
      and tstzrange(to_timestamp(b.yiclkind::text || '-' || b.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(b.yiclkoutd::text || '-' || b.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()') 
          <>-- not equal
            tstzrange(to_timestamp(a.yiclkind::text || '-' || a.yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss'), 
                      to_timestamp(a.yiclkoutd::text || '-' || a.yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss'), '()')                    
      and b.pymast_employee_number = a.pymast_employee_number
      and b.yiclkind = a.yiclkind
      and b.yicode not in ('I','666'))  
  and a.yicode not in ('I','666') -- exclude auto deduct (negative duration) and clock events with no clock out  
order by a.pymast_employee_number, a.yiclkind, a.yiclkint