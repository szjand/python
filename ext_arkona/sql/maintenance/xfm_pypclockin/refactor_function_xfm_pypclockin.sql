﻿-- FACT: this is the script i developed to regenerate a full set of data from pypclockin


-- drop table if exists jon.xfm_pypclockin_tmp cascade;
-- CREATE TABLE jon.xfm_pypclockin_tmp
-- (
--   yico_ citext NOT NULL,
--   pymast_employee_number citext NOT NULL,
--   yiclkind date NOT NULL,
--   clock_hours numeric(6,2) NOT NULL,
--   hol_hours numeric(6,2) NOT NULL,
--   pto_hours numeric(6,2) NOT NULL,
--   vac_hours numeric(6,2) NOT NULL,
--   CONSTRAINT xfm_pypclockin_tmp_pkey PRIMARY KEY (yico_, pymast_employee_number, yiclkind));
--
-- drop table if exists jon.xfm_pypclockin cascade;
-- CREATE TABLE jon.xfm_pypclockin
-- (
--  store_code citext NOT NULL,
--  employee_number citext NOT NULL,
--  the_date date NOT NULL,
--  clock_hours numeric(6,2) NOT NULL,
--  reg_hours numeric(6,2) NOT NULL,
--  ot_hours numeric(6,2) NOT NULL,
--  vac_hours numeric(6,2) NOT NULL,
--  pto_hours numeric(6,2) NOT NULL,
--  hol_hours numeric(6,2) NOT NULL,
--  CONSTRAINT xfm_pypclockin_pkey PRIMARY KEY (store_code, employee_number, the_date));

-- populate a year at a time
do $$
declare
  _from_date date := (  -- the last sunday of the previous year
    select max(the_date)
    from dds.dim_date
    where the_year = 2019
      and day_name = 'sunday');
--   _thru_date DATE := current_date;
  _thru_date date := ( -- the first saturday of the following year
    select min(the_date)
    from dds.dim_date
    where the_year = 2020
      and day_name = 'saturday');      
begin  
-- FACT: including clock out date above resulted in multiple rows for emp/date
-- generate the raw clock hours
truncate jon.xfm_pypclockin_tmp;
insert into jon.xfm_pypclockin_tmp
select yico_, pymast_employee_number, yiclkind, sum(clock_hours), sum(hol_hours), sum(pto_hours), sum(vac_hours)
from (
  select yico_, pymast_employee_number, yiclkind, 
  --   coalesce(sum((extract(epoch from (yiclkoutt - yiclkint))/3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,
  --   coalesce(sum((extract(epoch from (yiclkoutt - yiclkint))/3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours,
  --   coalesce(sum((extract(epoch from (yiclkoutt - yiclkint))/3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours,
  --   coalesce(sum((extract(epoch from (yiclkoutt - yiclkint))/3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours
    coalesce(
      sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
        to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
        /3600.0)::numeric(6,2)) filter (where yicode in ('666','O')), 0) as clock_hours,    
    coalesce(
      sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
        to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
        /3600.0)::numeric(6,2)) filter (where yicode = 'HOL'), 0) as hol_hours, 
    coalesce(
      sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
        to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
        /3600.0)::numeric(6,2)) filter (where yicode = 'PTO'), 0) as pto_hours, 
    coalesce(
      sum((extract(epoch from ( -- epoch: seconds since 1970-01-01 00:00:00+00
        to_timestamp(yiclkoutd::text || '-' || yiclkoutt::text, 'YYYY-MM-DD-HH24:MI:ss') - to_timestamp(yiclkind::text || '-' || yiclkint::text, 'YYYY-MM-DD-HH24:MI:ss')))
        /3600.0)::numeric(6,2)) filter (where yicode = 'VAC'), 0) as vac_hours                  
  from ( -- d: clock hours
  --   select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutt, a.yicode
    select a.yico_, a.pymast_employee_number, a.yiclkind, a.yiclkint, a.yiclkoutd, a.yiclkoutt, a.yicode
    from jon.ext_pypclockin a -- arkona.ext_pypclockin a
  --   left join ( -- exclude overlap
  --     select yico_, pymast_employee_number, yiclkind, min(yiclkint), max(yiclkint), min(yiclkoutt), max(yiclkoutt)
  --     from arkona.ext_pypclockin
  --     where yicode in ('HOL','666','O','PTO','VAC')
  -- --       and yiclkind not in ('09/07/2020')
  --       and extract(year from yiclkind) = 2009
  --     group by yico_, pymast_employee_number, yiclkind
  --     having count(*) > 1
  --       and max(yiclkint) < min(yiclkoutt)) b on a.yico_ = b.yico_
  --         and a.pymast_employee_number = b.pymast_employee_number
  --         and a.yiclkind = b.yiclkind
  --   left join ( -- exclude dup
  --     select yico_, pymast_employee_number, yiclkind, yiclkint, yicode
  --     from arkona.ext_pypcloc
  --     group by yico_, pymast_employee_number, yiclkind, yiclkint, yicode
  --     having count(*) > 1) c on a.yico_ = c.yico_
  --         and a.pymast_employee_number = c.pymast_employee_number
  --         and a.yiclkind = c.yiclkind  
    where a.yicode in ('HOL','666','O','PTO','VAC')
      and a.yiclkind between _from_date and _thru_date) d
  --     and b.yico_ is null
  --     and c.yico_ is null) d
  -- group by yico_, pymast_employee_number, yiclkind;
  group by yico_, pymast_employee_number, yiclkind, yiclkoutd) aa
group by yico_, pymast_employee_number, yiclkind;  
end $$;

-- FACT: including clock out date above resulted in multiple rows for emp/date
-- truncate jon.xfm_pypclockin_tmp;
-- insert into jon.xfm_pypclockin_tmp
-- select yico_, pymast_employee_number, yiclkind,
--   sum(clock_hours), sum(hol_hours), sum(pto_hours), sum(vac_hours)
-- from jon.xfm_pypclockin_tmp_1
-- group by yico_, pymast_employee_number, yiclkind;


-- ok, this has been rewritten to simplify it a bit, include the week in each of the self join tables
-- inspiration from https://rickosborne.org/blog/2006/01/calculating-overtime-with-pure-sql/
insert into jon.xfm_pypclockin
select -- bb.the_week, 
  aa.yico_, aa.pymast_employee_number, aa.yiclkind, coalesce(aa.clock_hours, 0) as clock, 
  coalesce(bb.reg_hours, 0) as reg, coalesce(bb.ot_hours, 0) as ot,
  coalesce(aa.vac_hours, 0) as vac, coalesce(aa.pto_hours, 0) as pto, 
  coalesce(aa.hol_hours, 0) as hol
from jon.xfm_pypclockin_tmp aa
left join ( -- reg & ot hours
  select a.yico_, a.pymast_employee_number, a.the_week, a.yiclkind,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) > 40 then 0
        else 40 - coalesce(sum(b.clock_hours), 0)
      end, 2) as reg_hours,
    round(
      case
        when coalesce(sum(b.clock_hours), 0) >= 40 then a.clock_hours
        when coalesce(sum(b.clock_hours), 0) + a.clock_hours < 40 then 0
        else a.clock_hours - (40 -coalesce(sum(b.clock_hours), 0))
      end, 2) as ot_hours   
  from ( -- a
    select b.sunday_to_saturday_week as the_week, a.* 
    from jon.xfm_pypclockin_tmp a
    join dds.dim_date b on a.yiclkind = b.the_date) a
  left join ( -- b
    select b.sunday_to_saturday_week as the_week, a.* 
    from jon.xfm_pypclockin_tmp a
    join dds.dim_date b on a.yiclkind = b.the_date) b on a.pymast_employee_number = b.pymast_employee_number
      and a.the_week = b.the_week
      and b.yiclkind < a.yiclkind  -- this is the magix
  group by a.yico_, a.the_week, a.pymast_employee_number, a.yiclkind, a.clock_hours) bb on aa.yico_ = bb.yico_
    and aa.pymast_employee_number = bb.pymast_employee_number
    and aa.yiclkind = bb.yiclkind
on conflict (store_code,employee_number,the_date)
do update
  set(clock_hours,reg_hours,ot_hours,vac_hours,pto_hours,hol_hours)
  =
  (excluded.clock_hours,excluded.reg_hours,excluded.ot_hours,excluded.vac_hours,excluded.pto_hours,excluded.hol_hours);
  
