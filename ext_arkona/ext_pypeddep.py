# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_pypeddep'
run_id = None
file_name = 'files/ext_pypeddep.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),TRIM(EMPLOYEE_NUMBER),ACCT_SEQ_,TRIM(BANK_ACCOUNT_NO_),TRIM(BANK_ROUTING_NO_),
              DEPOSIT_AMOUNT_PCT,TRIM(PERCENT_FLAG),TRIM(DEP_ACCT_TYPE),TRIM(APPROVAL_STATUS),APPROVAL_PROCESSED,
              TRIM(ACTIVE_STATUS),DEPOSIT_LIMIT,TRIM(INSTITUTION_NAME),TRIM(INSTITUTION_ADD1),TRIM(INSTITUTION_ADD2),
              TRIM(INSTITUTION_CITY),TRIM(INSTITUTION_STATE),INSTITUTION_ZIP,TRIM(ASSOC_CODE),TRIM(ASSOC_CODE_TYPE)
            from rydedata.pypeddep
        """
        db2_cur.execute(sql)
        with open(file_name, 'w', newline="") as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_pypeddep")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_pypeddep from stdin with csv encoding 'latin-1 '""", io)
