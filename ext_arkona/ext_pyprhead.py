# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_pyprhead'
# pg_con = None
# db2_con = None
run_id = None
file_name = 'files/ext_pyprhead.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),TRIM(EMPLOYEE_NUMBER),TRIM(EMPLOYEE_NUMBER_YRMGRN),TRIM(JOB_DESCRIPTION),
              TRIM(JOB_LEVEL),HIRE_DATE,ORG_HIRE_DATE,TERMINATION_DATE,LAST_REVIEW,NEXT_REVIEW,LAST_CONTACT,
              BASE_SALARY01,BASE_SALARY02,BASE_SALARY03,BASE_SALARY04,BASE_SALARY05,BASE_SALARY06,BASE_SALARY07,
              BASE_SALARY08,BASE_SALARY09,BASE_SALARY10,BASE_SALARY11,BASE_SALARY12,BASE_SALARY13,BASE_SALARY14,
              BASE_SALARY15,BASE_SALARY16,RAISE_DATE01,RAISE_DATE02,RAISE_DATE03,RAISE_DATE04,RAISE_DATE05,
              RAISE_DATE06,RAISE_DATE07,RAISE_DATE08,RAISE_DATE09,RAISE_DATE10,RAISE_DATE11,RAISE_DATE12,
              RAISE_DATE13,RAISE_DATE14,RAISE_DATE15,RAISE_DATE16
            from rydedata.pyprhead
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_pyprhead")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_pyprhead from stdin with csv encoding 'latin-1 '""", io)

