# encoding=utf-8
import csv
import db_cnx
import ops
import string

moved to luigi 9/18/19

task = 'ext_pypcodes'
# pg_con = None
# db2_con = None
# run_id = None
file_name = 'files/ext_pypcodes.csv'


# try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),TRIM(CODE_TYPE),TRIM(DED_PAY_CODE),TRIM(DESCRIPTION),TRIM(ACCOUNT_NUMBER),
              TRIM(EXEMPT_1_FED_TAX_),TRIM(EXEMPT_2_FICA_),TRIM(EXEMPT_3_FUTA_),TRIM(EXEMPT_4_STATE_TAX_),
              TRIM(EXEMPT_5_CITY_TAX_),TRIM(EXEMPT_6_MEDC_TAX_),TRIM(EXEMPT_7_SUTA_),TRIM(EXEMPT_8_WORKMEN_COMP8),
              TRIM(EXEMPT_9_COUNTY_TAX_),TRIM(EXEMPT_10_SDI_TAX_),TRIM(EXEMPT_11_NOT_ASSIGNED_),
              TRIM(EXEMPT_FIXED_DED),TRIM(PAY_AMT_HOURS),TRIM(FRINGE_BENEFIT),TRIM(EXEMPT_FROM_GROSS),
              TRIM(INCENTIVE_PAY),TRIM(DED_CONTROL_NUMBER),TRIM(PRINT_EMPLOYEE_NAME),TRIM(W2_CODE2C),
              TRIM(W2_BOX2B),TRIM(DISIBILITY_SWITCHES),DEDUCTION_PRIORITY_SEQ,TRIM(RD_PARTY_PAY3RD),
              TRIM(EXEMPT_PAY),TRIM(PAY_CODE_IN_ATTENDANCE),TRIM(DEDUCTION_CODE_LINK),TRIM(GARNISHMENT_DED),
              TRIM(DEFERRED_COMP_DED),TRIM(K_LIKE_DED401),TRIM(OVERTIME_PAY)
            from rydedata.pypcodes
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_pypcodes")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_pypcodes from stdin with csv encoding 'latin-1 '""", io)
# ops.log_pass(run_id)
print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
# except Exception, error:
#     # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
#     print error
# finally:
#     if db2_con:
#         db2_con.close()
#     if pg_con:
#         pg_con.close()
