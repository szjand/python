# encoding=utf-8

import csv
import db_cnx
"""
service contract definition
"""
file_name = 'files/ext_sdpsvcd.csv'

with db_cnx.arkona_report() as db2_con:
    with db2_con.cursor() as db2_cur:
        sql = """
            select
              TRIM(COMPANY_NUMBER),RECORD_KEY,TRIM(COMPANY_NAME),TRIM(RECEIVABLE_ACCT),TRIM(CUSTOMER_NUMBER),
              F_I_RECORD_KEY,TRIM(DOWNLOAD_MANUF),TRIM(PRINT_COST),TRIM(PRINT_STR_STP_TIM),TRIM(ACTIVE),
              TRIM(TAX_PART_COST),TRIM(TAX_EXEMPT)
            from rydedata.sdpsvcd
        """
        db2_cur.execute(sql)
        with open(file_name, 'w') as f:
            csv.writer(f).writerows(db2_cur)
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate arkona.ext_sdpsvcd")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy arkona.ext_sdpsvcd from stdin with csv encoding 'latin-1 '""", io)

