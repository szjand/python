# encoding=utf-8
import csv
import db_cnx


task = 'ext_bopopts'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_bopopts.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  TRIM(COMPANY_NUMBER),TRIM(ID),TRIM(NAME),TRIM(INITIALS),TRIM(PASSWORD),TRIM(EMPLOYEE_),
                  TRIM(TECH_TIME_ENTRY01),TRIM(CHG_RO_SERV_WTR02),TRIM(APPROVE_RO03),TRIM(SUBLET_ENTRY04),
                  TRIM(PRE_INVOICE05),TRIM(UNLOCK_RO06),TRIM(CLOSE07),TRIM(PARTS_ENTRY08),TRIM(ACCEPT_C_L_EXCEED09),
                  TRIM(CLS_CUST_PAY_ONLY10),TRIM(COUPONS11),TRIM(REVIEW_SPEC_ORDER12),TRIM(BODY_SHOP_SWTR13),
                  TRIM(OVERIDE_DEPOSIT14),TRIM(DISP_GROSS_PROFIT15),TRIM(OVERIDE_SKILL_LEV16),TRIM(WARRANTY_CLERK17),
                  TRIM(DELETE_LABPR_OP18),TRIM(DELETE_SHOP_SUPP19),TRIM(OPEN_ADDITIONL_RO20),TRIM(CHANGE_INTER_ACCT21),
                  TRIM(AVAILABLE_TO_USE22),TRIM(AVAILABLE_TO_USE23),TRIM(AVAILABLE_TO_USE24),TRIM(AVAILABLE_TO_USE25),
                  TRIM(AVAILABLE_TO_USE26),TRIM(AVAILABLE_TO_USE27),TRIM(AVAILABLE_TO_USE28),TRIM(AVAILABLE_TO_USE29),
                  TRIM(AVAILABLE_TO_USE30),TRIM(SEARCH_TYPE),TRIM(SORT_BY),TRIM(SERV_WRITER_SELEC),
                  TRIM(STATUS_SELECTION),TRIM(ACTIVE),TRIM(MISC_FIELDS)
                from rydedata.sdpswtr
            """
            db2_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_sdpswtr")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_sdpswtr from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print(error)
finally:
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
