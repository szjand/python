import db_cnx
import ops

pg_con = None
try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ select count(*) from arkona.xfm_pyactgr_changed_rows_history"""
            pg_cur.execute(sql)
            for row in pg_cur:
                the_count = row[0]
                ops.email_error('Hi',' from arkona.xfm_pyactgr_changed_rows_history is ' + str(the_count), the_count)
except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()
