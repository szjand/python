# encoding=utf-8
import csv
import db_cnx

pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_query.csv'
new_file_name = None
store = None
batch = None
try:
    with db_cnx.pg() as pg_con:
        store = 'RY2'
        batch = 112180
        new_file_name = 'files/' + store + '_' + str(batch) + '.csv'
        with pg_con.cursor() as pg_cur:
            sql = """
                select a.employee_number,b.ssn,a.name,a.address_1,
                  a.address_2,a.city,a.state,a.zip,a.div_loc,a.employee_type,
                  (select dds.db2_integer_to_date(a.birth_date)) as birth_date,
                  (select dds.db2_integer_to_date(a.orig_hire_date)) as orig_hire_date,
                  b.eligible_date,
                  (select dds.db2_integer_to_date(a.rehire_date)) as rehire_date,
                  case
                    when term_date = 0 then null
                    else (select dds.db2_integer_to_date(a.term_date))
                  end as term_date,
                  a.hours,  a.gross,a.excluded_compensation,a.expected_annual_salary,
                  a.employee_deferral,a.roth_deferral,a.after_tax,a.employer_match,
                  a.employer_discretionary,a.safe_harbor,a.safe_harbor_match,
                  a.pension,a.loan_number,a.loan_payments,a.payroll_frequency
                from jon.ext_alerus_401k a
                left join jon.ssn b on a.employee_number = b.employee_number
            """
            pg_cur.execute(sql)
            with open(new_file_name, 'w') as f:
                writer = csv.writer(f, delimiter=',')
                writer.writerow(['employee_number', 'ssn', 'name', 'address_1', 'address_2', 'city', 'state', 'zip',
                                 'div_loc', 'employee_type', 'birth_date', 'orig_hire_date', 'eligibility_date',
                                 'rehire_date', 'term_date', 'hours', 'gross', 'excluded_compensation',
                                 'expected_annual_salary', 'employee_deferral', 'roth_deferral', 'after_tax',
                                 'employer_match', 'employer_discretionary', 'safe_harbor', 'safe_harbor_match',
                                 'pension', 'loan_number', 'loan_payments', 'payroll_frequency'])
                for row in pg_cur.fetchall():
                    writer.writerow(row)

    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    print(error)
finally:
    if pg_con:
        pg_con.close()
