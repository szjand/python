# encoding=utf-8
import csv
import db_cnx

pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_query.csv'
new_file_name = None
store = None
batch = None
try:
    store = 'RY2'
    batch = 112180
    new_file_name = 'files/' + store + '_' + str(batch) + '.csv'
    print new_file_name
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select g.company_number as store_code,
                  g.payroll_run_number as batch,
                  trim(p.pymast_employee_number) as employee_number,
                  space(9) as "Social Sec.Number",
                  trim(p.employee_name) as name,
                  trim(p.address_1),   trim(p.address_2),
                  trim(p.city_name) as City, p.state_code_address_ as state, p.zip_code as zip,
                  space(4) as "Div./Loc.",
                  coalesce(
                  case p.ymactv
                    when 'A' then '1'
                    when 'P' then '2'
                  else null
                  end, space(1)) as employee_type,
                  p.birth_date, p.org_hire_date as orig_hire_date,
                  cast(null as date) as eligibility_date,
                  case when p.hire_date <> p.org_hire_date then p.hire_date else null end as rehire_date,
                  p.termination_Date as term_date,
                  g.hours, g.gross,
                  space(4) as excluded_compensation, space(4) as expected_annual_salary,
                  coalesce(b.k401, 0) as employee_deferral,
                  coalesce(b.roth, 0) as roth_deferral,
                  space(4) as after_tax,
                  g.emplr_curr_ret as employer_match,
                  space(4) as employer_discretionary,
                  space(4) as safe_harbor,
                  space(4) as safe_harbor_match,
                  space(4) as pension,
                  space(4) as loan_number,
                  space(4) as loan_payments,
                  p.pay_period as payroll_frequence
                from rydedata.pymast p
                inner join (
                  select company_number, payroll_run_number, trim(employee_) as employee_number,
                    sum(reg_hours) as hours,
                    sum(total_gross_pay) as gross,
                    sum(emplr_curr_ret) as emplr_curr_ret
                  from rydedata.pyhshdta
                  where seq_void in ('00','01')
                    and payroll_run_number in (
                      select distinct payroll_run_number
                      from rydedata.pyptbdta
                      where payroll_cen_year = 118)
                    and company_number = '{0}'
                    and payroll_run_number = {1}
                  group by company_number, payroll_run_number, employee_) g
                    on trim(p.pymast_employee_number) = trim(g.employee_number)
                left join ( -- folks can have both roth and 401k
                  select company_number, employee_number, payroll_run_number,
                    sum(case when code_id in ('99','99A') then amount end) as roth,
                    sum(case when code_id = '91' then amount end) as k401
                  from rydedata.pyhscdta
                  where code_id in ('91','99','99A')
                    and code_type = '2'
                    and payroll_run_number in (
                      select payroll_run_number
                      from rydedata.pyptbdta
                      where payroll_cen_year = 118)
                  group by company_number, employee_number, payroll_run_number) b
                    on trim(p.pymast_employee_number) = trim(b.employee_number)
                        and g.payroll_run_number = b.payroll_run_number
            """.format(store, batch)
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate jon.ext_alerus_401k")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy jon.ext_alerus_401k from stdin with csv encoding 'latin-1 '""", io)
except Exception as error_1:  # python3 syntax
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print(error_1)
finally:
    # pypyodbc syntax
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()

try:
    with db_cnx.pg() as pg_con:
        pass
        # with pg_con.cursor() as pg_cur:
            # sql = """
            #     select a.employee_number,b.ssn,a.name,a.address_1,
            #       a.address_2,a.city,a.state,a.zip,a.div_loc,a.employee_type,
            #       (select dds.db2_integer_to_date(a.birth_date)) as birth_date,
            #       (select dds.db2_integer_to_date(a.orig_hire_date)) as orig_hire_date,
            #       b.eligible_date,
            #       (select dds.db2_integer_to_date(a.rehire_date)) as rehire_date,
            #       case
            #         when term_date = 0 then null
            #         else (select dds.db2_integer_to_date(a.term_date))
            #       end as term_date,
            #       a.hours,  a.gross,a.excluded_compensation,a.expected_annual_salary,
            #       a.employee_deferral,a.roth_deferral,a.after_tax,a.employer_match,
            #       a.employer_discretionary,a.safe_harbor,a.safe_harbor_match,
            #       a.pension,a.loan_number,a.loan_payments,a.payroll_frequency
            #     from jon.ext_alerus_401k a
            #     left join jon.ssn b on a.employee_number = b.employee_number
            # """
            # pg_cur.execute(sql)
            # with open(new_file_name, 'w') as f:
            #     writer = csv.writer(f, delimiter=',')
            #     for row in pg_cur.fetchall():
            #         writer.writerow(row)

    # ops.log_pass(run_id)
    print('Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:  # python3 syntax
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print(error)
finally:
    # pypyodbc syntax
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
