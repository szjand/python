#!/usr/bin/python
import sys
import csv
import pypyodbc
import psycopg2
import json
from collections import OrderedDict
"""
1/8/20
    failed for kim
    copied this from beta.rydellvision.com : /home/rydell/create_401k_report.py
    on beta.rydellvision.com called from /new-vision/lib/plugins/vision/routes/pay-roll.js
    
"""
def main():
    file_name = '/home/rydell/new-vision/ext_query.csv'
    store = sys.argv[1]
    batch = sys.argv[2]
    with pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver 64-bit};system=REPORT1.ARKONA.COM;uid=rydejon;pwd=fuckyou5') as cn:
        with cn.cursor() as cur:
            sql = """
                select g.company_number as store_code,
                  g.payroll_run_number as batch,
                  trim(p.pymast_employee_number) as employee_number,
                  space(9) as "Social Sec.Number",
                  trim(p.employee_name) as name,
                  trim(p.address_1),   trim(p.address_2),
                  trim(p.city_name) as City, p.state_code_address_ as state, p.zip_code as zip,
                  space(4) as "Div./Loc.",
                  coalesce(
                  case p.ymactv
                    when 'A' then '1'
                    when 'P' then '2'
                  else null
                  end, space(1)) as employee_type,
                  p.birth_date, p.org_hire_date as orig_hire_date,
                  cast(null as date) as eligibility_date,
                  case when p.hire_date <> p.org_hire_date then p.hire_date else null end as rehire_date,
                  p.termination_Date as term_date,
                  g.hours, g.gross,
                  space(4) as excluded_compensation, space(4) as expected_annual_salary,
                  coalesce(b.k401, 0) as employee_deferral,
                  coalesce(b.roth, 0) as roth_deferral,
                  space(4) as after_tax,
                  g.emplr_curr_ret as employer_match,
                  space(4) as employer_discretionary,
                  space(4) as safe_harbor,
                  space(4) as safe_harbor_match,
                  space(4) as pension,
                  space(4) as loan_number,
                  space(4) as loan_payments,
                  p.pay_period as payroll_frequence
                from rydedata.pymast p
                inner join (
                  select company_number, payroll_run_number, trim(employee_) as employee_number,
                    sum(reg_hours) as hours,
                    sum(total_gross_pay) as gross,
                    sum(emplr_curr_ret) as emplr_curr_ret
                  from rydedata.pyhshdta
                  where seq_void in ('00','01')
                    and payroll_run_number in (
                      select distinct payroll_run_number
                      from rydedata.pyptbdta
                      where payroll_cen_year = 119)
                    and company_number = '{0}'
                    and payroll_run_number = {1}
                  group by company_number, payroll_run_number, employee_) g
                    on trim(p.pymast_employee_number) = trim(g.employee_number)
                left join ( -- folks can have both roth and 401k
                  select company_number, employee_number, payroll_run_number,
                    sum(case when code_id in ('99','99A') then amount end) as roth,
                    sum(case when code_id = '91' then amount end) as k401
                  from rydedata.pyhscdta
                  where code_id in ('91','99','99A')
                    and code_type = '2'
                    and payroll_run_number in (
                      select payroll_run_number
                      from rydedata.pyptbdta
                      where payroll_cen_year = 119)
                  group by company_number, employee_number, payroll_run_number) b
                    on trim(p.pymast_employee_number) = trim(b.employee_number)
                        and g.payroll_run_number = b.payroll_run_number
            """.format(store, batch)
            cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(cur)
    with psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'") as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate jon.ext_alerus_401k")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy jon.ext_alerus_401k from stdin with csv encoding 'latin-1 '""", io)
            pg_con.commit()
            sql = """
              select * from jon.json_get_401k_look_for_missing_ssn()
            """
            pg_cur.execute(sql)
            result = pg_cur.fetchall()
            if result[0][0]['missing_ssn']:
                print(json.dumps(result))
                exit()
            else:
                sql = "select * from jon.json_get_401k_report()"
                pg_cur.execute(sql)
                result = pg_cur.fetchall()
                # ordered = OrderedDict(result[0][0]['report'])
                print(json.dumps(result))

                # print(json.dumps(OrderedDict(remove_array)))




if __name__ == '__main__':
    main()