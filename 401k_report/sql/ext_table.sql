﻿-----------------------------------------------------------------------
--< 06/05/2020  add email address as last field in table
-----------------------------------------------------------------------

alter table jon.ext_alerus_401k
add column email_address citext;

modified the db2 query @ beta.rydellvision.com ~/python-scripts/create_401k_report.py

select * from jon.ext_alerus_401k
-----------------------------------------------------------------------
--< 06/05/2020  add email address as last field in table
-----------------------------------------------------------------------


drop table if exists jon.ext_alerus_401k;
create table jon.ext_alerus_401k (
  store_code citext,
  batch bigint,
  employee_number citext,
  ssn citext,
  name citext,
  address_1 citext,
  address_2 citext,
  city citext,
  state citext,
  zip citext,
  div_loc citext,
  employee_type citext,
  birth_date integer,
  orig_hire_date integer,
  eligibility_date integer,
  rehire_date integer,
  term_date integer,
  hours numeric(8,2),
  gross numeric(8,2),
  excluded_compensation citext,
  expected_annual_salary citext, 
  employee_deferral numeric(8,2),
  roth_deferral numeric(8,2),
  after_tax citext,
  employer_match numeric(8,2),
  employer_discretionary citext,
  safe_harbor citext,
  safe_harbor_match citext,
  pension citext,
  loan_number citext,
  loan_payments citext,
  payroll_frequency citext,
  primary key (store_code, batch, employee_number));
  
  

select company_number, employee_number, payroll_run_number,
  coalesce(sum(amount) filter (where code_id in ('99','99A')), 0) as roth,
  coalesce(sum(amount) filter (where code_id = '91'), 0) as k401
from arkona.ext_pyhscdta
where code_id in ('91','99','99A')
  and code_type = '2'
  and payroll_run_number = 1126000
group by company_number, employee_number, payroll_run_number  
order by employee_number


select * from arkona.ext_pyhscdta a limit 100


from (
select employee_number, payroll_run_number, code_id
from arkona.ext_pyhscdta a
where code_id = '99'
  and code_type = '2') c
inner join (
select employee_number, payroll_run_number, code_id
from arkona.ext_pyhscdta a
where code_id = '99A'
  and code_type = '2') d on c.employee_number = d.employee_number and c.payroll_run_number = d.payroll_run_number


-- misc info from pyptbdta
-- same batch number used for each store, ie, 309180 exists as the same type & date for each store
select * 
from arkona.ext_pyptbdta
where payroll_cen_year = 118


select company_number, description, count(*)
from arkona.ext_pyptbdta
where payroll_cen_year = 117
group by company_number, description
order by company_number, description


select company_number, payroll_run_number, description, (select dds.db2_integer_to_date(payroll_end_date))
from arkona.ext_pyptbdta
where payroll_cen_year = 118
order by company_number, payroll_run_number, description


truncate jon.ext_alerus_401k


SELECT array_agg('a.'||attname)
FROM  pg_attribute
WHERE attrelid = 'jon.ext_alerus_401k'::regclass  -- table name, optionally schema-qualified
  AND attnum > 0
  AND NOT attisdropped 

SELECT array_agg(''''||attname||'''')
FROM  pg_attribute
WHERE attrelid = 'jon.ext_alerus_401k'::regclass  -- table name, optionally schema-qualified
  AND attnum > 0
  AND NOT attisdropped 


select a.store_code,a.batch,a.employee_number,b.ssn,a.name,a.address_1,
  a.address_2,a.city,a.state,a.zip,a.div_loc,a.employee_type,
  (select dds.db2_integer_to_date(a.birth_date)) as birth_date,
  (select dds.db2_integer_to_date(a.orig_hire_date)) as orig_hire_date,
  b.eligible_date,
  (select dds.db2_integer_to_date(a.rehire_date)) as rehire_date,
  case 
    when term_date = 0 then null
    else (select dds.db2_integer_to_date(a.term_date)) 
  end as term_date,
  a.hours,  a.gross,a.excluded_compensation,a.expected_annual_salary,
  a.employee_deferral,a.roth_deferral,a.after_tax,a.employer_match,
  a.employer_discretionary,a.safe_harbor,a.safe_harbor_match,
  a.pension,a.loan_number,a.loan_payments,a.payroll_frequency  
from jon.ext_alerus_401k a
left join jon.ssn b on a.employee_number = b.employee_number
where store_code = 'RY2'
  and batch = 1126000  
order by name  


alter table jon.ssn
add column eligible_date date;  

see .../401k_report/sql/eligible_date.sql for how this is generated

/*
3/27/18
have not been thinking about this clearly
yesterday, sent kim 6 spreadsheets and she responded that the totals are correct
i have not been thinking thru the  whole process
have to talk to afton today, because i am still thinking python
anyway
process looks like this
1.kim enter store and batch
2.query dealertrack and populate jon.ext_alerus_401k
3.join jon.ext_alerus_401k to jon.ssn and generate a CSV to download
*/


select a.employee_number,b.ssn,a.name,a.address_1,
  a.address_2,a.city,a.state,a.zip,a.div_loc,a.employee_type,
  (select dds.db2_integer_to_date(a.birth_date)) as birth_date,
  (select dds.db2_integer_to_date(a.orig_hire_date)) as orig_hire_date,
  b.eligible_date,
  (select dds.db2_integer_to_date(a.rehire_date)) as rehire_date,
  case 
    when term_date = 0 then null
    else (select dds.db2_integer_to_date(a.term_date)) 
  end as term_date,
  a.hours,  a.gross,a.excluded_compensation,a.expected_annual_salary,
  a.employee_deferral,a.roth_deferral,a.after_tax,a.employer_match,
  a.employer_discretionary,a.safe_harbor,a.safe_harbor_match,
  a.pension,a.loan_number,a.loan_payments,a.payroll_frequency  
from jon.ext_alerus_401k a
left join jon.ssn b on a.employee_number = b.employee_number



