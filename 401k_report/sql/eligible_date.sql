﻿select a.employee_name, a.pymast_employee_number, a.active_code, b.*
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
where b.ssn is null

select extract(year from b.the_date), count(*)
from ads.ext_edw_clock_hours_fact a
inner join dds.dim_date b on a.datekey = b.date_key
group by extract(year from b.the_date)


a.the_date - interval '6 month'

drop table if exists emps cascade;
create temp table emps as
select employee_name, pymast_employee_number as employee_number, active_code, payroll_class,
  (select dds.db2_integer_to_date(birth_date)) as bday,
  (select dds.db2_integer_to_date(hire_Date)) as hire_date, 
  (select dds.db2_integer_to_date(org_hire_date)) as orig_hire_date,
  ((select dds.db2_integer_to_date(birth_date)) + interval '21 year')::date as bday_21,
  ((select dds.db2_integer_to_date(org_hire_date)) + interval '1 year'):: date as anniv_1,
  case when ((select dds.db2_integer_to_date(org_hire_date)) + interval '1 year'):: date  >= ((select dds.db2_integer_to_date(birth_date)) + interval '21 year')::date then true else false end as _21_on_anniv_1
-- select *
from arkona.ext_pymast
where active_code <> 'T'
  and pymast_company_number in ('RY1','RY2');
create unique index on emps(employee_number);
create index on emps(hire_date);
create index on emps(anniv_1);  
alter table emps
add column eligible date;


drop table if exists clockhours cascade;
create temp table clockhours as
select a.clockhours as clock_hours, b.the_date, c.employeenumber as employee_number
from ads.ext_edw_clock_hours_fact a
inner join dds.dim_date b on a.datekey = b.date_key
inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
where a.clockhours <> 0;
create unique index on clockhours(the_date,employee_number);


-- commision, salary, not 21 on 1st anniv, elig = 21st bday
update emps 
set eligible = bday_21
where payroll_class in ('C','S')
  and not _21_on_anniv_1;
-- commision, salary, 21 on 1st anniv, elig = 1st anniv
update emps
set eligible = anniv_1
where payroll_class in ('C','S')
  and _21_on_anniv_1;


-- hourly, 21 on 1st anniv, > 1000 hours on 1st aniv
update emps x
set eligible = y.anniv_1
from (
  select a.employee_name, a.employee_number, a.anniv_1, sum(b.clock_hours)
  from emps a
  left join clockhours b on a.employee_number = b.employee_number
    and b.the_date between a.hire_date and a.anniv_1
  where a._21_on_anniv_1
    and a.payroll_class = 'H'
  group by a.employee_name, a.employee_number, a.anniv_1
  having sum(b.clock_hours) >= 1000)  y
where x.employee_number = y.employee_number  


-- hourly, full time, 21 on 1st anniv hired before 2010
update emps
set eligible = anniv_1
where active_code = 'A'
  and eligible is null
  and _21_on_anniv_1
  and payroll_class = 'H'
  and extract(year from orig_hire_date) < 2010;

-- 21 & clock hours >= 1000 on 1st anniv
update emps 
set eligible = anniv_1
where employee_number in (
  select a.employee_number
  from emps a
  left join clockhours b on a.employee_number = b.employee_number
    and b.the_date between a.orig_hire_date and a.orig_hire_date + interval '1 year'
  where a.eligible is null
  group by a.employee_number
  having sum(b.clock_hours) >= 1000) ;

-- old timers
update emps
set eligible = 
  case
    when _21_on_anniv_1 then anniv_1
    else bday_21
  end
-- select * from emps
where eligible is null  
  and extract(year from anniv_1) < 2011

-- exclude newbies & kids (1st anniv and 21 bday)
select *
from emps c
left join (
  select a.employee_number, sum(b.clock_hours)
  from emps a
  left join clockhours b on a.employee_number = b.employee_number
    and b.the_Date between orig_hire_date and anniv_1  
  where a.eligible is null 
    and bday_21 < current_Date 
    and anniv_1 < current_date
  group by a.employee_number) d on c.employee_number = d.employee_number
where c.eligible is null
  and bday_21 < current_date 
  and anniv_1 < current_date


fuck it close enough
select * from emps

select *
from jon.ext_alerus_401k a
 

update jon.ext_alerus_401k a
set eligibility_date = (
  select eligible
  from emps
  where employee_number = a.employee_number);

-- 3/26/18, added eligible attribute to table jon.ssn
-- 12/23/19 i overwrote jon.ssn generating some hr report
-- so 401k failed for kim, program gave a stupid error
-- but dug into json_get_401k_report() and realized jon.ssn(eligible_date) was gone
alter table jon.ssn
add column eligible_date date;

select *
from jon.ssn a
left join emps b on a.employee_number = b.employee_number

update jon.ssn a
set eligible_date = (
  select eligible
  from emps
  where employee_number = a.employee_number)
  