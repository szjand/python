﻿select distinct a.name, a.employee_number, b.*
from jon.ext_alerus_401k a
left join jon.ssn b on a.employee_number = b.employee_number
where b.ssn is null
order by a.name

select *
from jon.ext_alerus_401k a
order by batch, name

select distinct batch
from jon.ext_alerus_401k a
order by batch

select * from jon.ssn order by full_name

select employee_name, pymast_employee_number, b.ssn
from arkona.ext_pymast a
left join jon.ssn b on a.pymast_employee_number = b.employee_number
where active_code <> 'T'
  and b.ssn is null



insert into jon.ssn values ('195632','ANDERSON, CODY','502-13-7131');
insert into jon.ssn values ('284625','LANGENSTEIN, JARED','504-11-4409');
insert into jon.ssn values ('168942','HALL, AERICA','420-43-1538');
insert into jon.ssn values ('125689','JOHNSON, JEFFREY','501-68-4728');
insert into jon.ssn values ('135987','CLARK, ADAM','520-19-4088');
insert into jon.ssn values ('159863','FERRY, CARTER','502-23-5913');
insert into jon.ssn values ('298543','SCHINDLER, TIM','473-76-3598');
insert into jon.ssn values ('185624','ADALETE, BRYCE','615-80-1751');
insert into jon.ssn values ('165983','MENARD, MICHAEL','502-02-8533');
insert into jon.ssn values ('265348','PIERCE, BRETT','501-17-8285');
insert into jon.ssn values ('176324','BARRETT, THOMAS','471-29-2641');
insert into jon.ssn values ('158763','BRUNK, JUSTIN','508-37-5317');
insert into jon.ssn values ('156894','ROHLFS, SPENCER','508-37-5317');
insert into jon.ssn values ('175642','VANCAMP, KEVIN S','501-06-5527');
insert into jon.ssn values ('195876','OEHLKE, AUSTIN S','501-27-4832');
insert into jon.ssn values ('265849','MOORE, JOSHUA','501-25-3724');
 