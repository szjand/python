# encoding=utf-8
import csv
import db_cnx
import inspect
import datetime
import os
import smtplib
from email.message import EmailMessage


"""
locally, when openvpn is connected, postgresql can not connect
presumably because postgresql is connecting via the vpn to the store
so, for the purpose of testing and running locally, need to separate download and parse operations

        for filename in os.listdir(self.csv_dir):
            os.unlink(self.csv_dir + filename)
   
think i will simulate luigi in generating a text file when each process completes (local_target)     

todo in production need file structure

04-10-21
the purpose of this script is solely to daily download each redshift table into a csv file    

postgresql query to generate the calls in main
SELECT substring(table_name, 5, 50)||'(csv_path + '||''''||substring(table_name, 5, 50)||'\\'
    ||table_name||'.csv'||''''||','||''''||'rydell.'||substring(table_name, 5, 50)||''''||')'
FROM information_schema.tables
WHERE table_schema = 'dc'
  and table_name like 'ext_dw_%' or table_name like 'ext_rpt_%'
order by substring(table_name, 5, 50)  

the only thing needed in each function is the list of attributes in the query

04/14/21 ~12 tables failed with the message: out of memory for query result
the fix was, for those table, change:
    with rs_con.cursor() as rs_cur:
    to with rs_con.cursor(name='wtf') as rs_cur:
the default behavior of a psycopg named cursor includes iterating over 2000 (default) records at a time    
the large files (dw_fact_customer_communication: 1,212,321 KB) seem to continue updating for a significant (minutes)
    period of time after the commit
dw_fact_notes, dw_fact_phone_call, rpt_appointment

dw_fact_deal
    while troubleshooting an encoding issue, discoverd multiple rows in the table
    change the query to distinct with an order by deal_id
    print the deal_id
    
2 files dw_fact_notes & dw_fact_deal threw an encoding error:
    'charmap' codec can't encode character '\u05e5' in position 864: character maps to <undefined>  
fix was to change    
    with open(file_name, 'w', newline='') as f:
to   
    with open(file_name, 'w', newline='', encoding='utf-8') as f:
    
04/15/21
    added DISTINCT to queries for dw_fact_deal & rpt_appointment
decided to only do CDC on dw tables, don't know what rpt tables are are how we might use them,
    initially, it makes no sense to do cdc on them    
do a fresh download of dw tables. data currently in the dc.ext_dw_ tables is from 4/14    
the complete dw download 4/15 took 35 minutes 

04/18/21
everything should be in place for a production type run
04/19/21 
limit stores to grand forks 39 & 40, except dw_dim_customer where store_id is a uuid

11/14/21
    none of this data has been used in 7 months, and i am starting to stress out about disk space
    had thought i might refactor, but went drastic, truncated most tables, tables sales_appointments,
    phone_call, service_appointments have been failing daily: duplicate key value violates unique constraint 
    and i just decided to stop it all for now
"""


# local_or_production = 'production'
local_or_production = 'local'

if local_or_production == 'local':
    csv_path = 'Z:\\E\\python_projects\\redshift_drive_centric\\redshift_csv_files\\'
    local_target_path = 'Z:\\E\\python_projects\\redshift_drive_centric\\local_target\\'
elif local_or_production == 'production':
    csv_path = 'blah blah blah'


def email_errors(failed_function, error_message):
    msg = EmailMessage()
    msg.set_content(error_message)
    msg['Subject'] = 'redshift download of ' + failed_function + ' has failed'
    msg['From'] = 'jandrews@cartiva.com'
    msg['To'] = 'jandrews@cartiva.com'
    s = smtplib.SMTP('mail.cartiva.com')
    s.send_message(msg)
    s.quit()


def dw_dim_customer(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select distinct customer_id,store_id,date_created,date_modified,customer_type,is_company,
                        first_name,last_name,company_name,primary_photo_url,customer_primary_email,
                        customer_primary_phone,customer_secodary_phone,customer_third_phone,
                        email_unsubscribe_date,phone_unsubscribe_date,snailmail_unsubscribe_date,
                        text_unsubscribe_date,customer_optin_date,is_text_opt_out,is_text_opt_in,
                        first_opted_out_phone_number,second_opted_out_phone_number,
                        third_opted_out_phone_number,best_contact_method,customer_city,
                        customer_state,customer_zip,date_of_birth,primary_sales_user_id,
                        secondary_sales_user_id,bdc_sales_user_id
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as file:
            file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_dim_date(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select date_day,prior_date_day,next_date_day,prior_year_date_day,prior_year_over_year_date_day,
                        day_of_week,day_of_week_name,day_of_week_name_short,day_of_month,day_of_year,week_start_date,
                        week_end_date,week_of_year,prior_year_week_start_date,prior_year_week_end_date,
                        prior_year_week_of_year,month_of_year,month_name,month_name_short,month_start_date,
                        month_end_date,prior_year_month_start_date,prior_year_month_end_date,quarter_of_year,
                        quarter_start_date,quarter_end_date,year_number,year_start_date,year_end_date
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_dim_store(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select store_id,store_group_id,date_created,date_modified,store_name,store_city,store_state,
                        store_zipcode,store_phone,store_nick_name,store_web_site,store_dms_type
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:  # the newline is REQUIRED for python 3
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_dim_user(file_name, table_name):
    """
        to handle some odd anomalies, user_team_name is part of the primary key for the xfm table
        in some instances it is null, so coalesce it here
    """
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select user_id,store_id,first_name,last_name,user_type,user_title,user_photo_url,user_email,
                        is_user_active,user_role,user_team_id,is_user_clocked_in,user_role_value,
                        coalesce(user_team_name, 'none'),
                        users_in_team,date_created,date_modified,user_lookup_value,user_lookup_type
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


# def dw_fact_closed_ro(file_name, table_name):
#     try:
#         print(inspect.currentframe().f_code.co_name)
#         # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
#         local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
#                         inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
#         try:  # clear out the csv directory
#             os.remove(file_name)
#         except FileNotFoundError:
#             pass
#         with db_cnx.dc_redshift() as rs_con:
#             with rs_con.cursor() as rs_cur:
#                 sql = """
#                     select ro_number,customer_id,vehicle_id,date_ro_open,date_ro_close,date_created,date_modified,
#                         vehicle_vin,vehicle_year,vehicle_make,vehicle_model,vehicle_trim,vehicle_mileage
#                     from {}
#                 """.format(table_name)
#                 rs_cur.execute(sql)
#                 with open(file_name, 'w', newline='') as f:
#                     csv.writer(f).writerows(rs_cur)
#         with open(local_target, 'w') as the_file:
#             the_file.write('pass')
#     except Exception as e:
#         email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_fact_customer_communication(file_name, table_name):
    """
        to handle some odd anomalies, prev_interaction_timestamp is part of the primary key for the xfm table
        in some instances it is null, so coalesce it here
    """
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='wtf') as rs_cur:
                sql = """
                    select communication_id,deal_id,user_id,store_id,communication_date,communication_date_short,
                        ilm_override_start,ilm_override_end,ilm_date_start,ilm_date_end,is_business_hour_communication,
                        communication_modified_date,communication_type,user_type,user_type_name,
                        customer_communication_type,customer_communication_group,customer_id,hasvideo,
                        hasduplicatevideo,hasphoto,is_from_customer,is_to_customer,sentiment_type_mixed,
                        sentiment_type_positive,sentiment_type_neutral,sentiment_type_negative,
                        rank_to_customer_deal_interaction,rank_deal_interaction,rank_deal_interaction_desc,
                        rank_communication_type,rank_communication_group,next_interaction_timestamp,
                        coalesce(prev_interaction_timestamp,'1970-01-01 00:00:01'),
                        next_interaction_usertype,prev_interaction_usertype,
                        next_communication_type,prev_communication_type,next_is_to_customer,prev_is_to_customer,
                        next_is_from_customer,prev_is_from_customer,next_response_time_secs,
                        next_response_less_than_15m,next_response_15m_to_30m,next_response_30m_to_60m,
                        next_response_gt_than_60m,next_response_less_than_60m,response_time_secs,
                        response_less_than_15m,response_15m_to_30m,response_30m_to_60m,
                        response_gt_than_60m,response_less_than_60m
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_fact_deal(file_name, table_name):
    """
        to handle some odd anomalies, delivery_total_gross is part of the primary key for the xfm table
        in some instances it is null, so coalesce it here
    """
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select DISTINCT deal_id,store_id,customer_name,customer_id,cobuyer_customer_id,sales_1_user_id,
                        sales_2_user_id,deal_user_credit,ilm_user_id,closer_user_id,deal_status_id,source_type,
                        source,source_description,source_group_name,is_bad_deal,lost_reason,lost_reason_description,
                        date_created,date_modified,date_duplicate_confirmed,duplicate_clear_time,
                        date_engagement_genius_hired,date_engagement_long_term_genius_hired,deal_derived_date,
                        deal_derived_date_abs,is_deal_derived_business_hour,first_resp_to_genius,latest_resp_to_genius,
                        user_response_date,current_stage,current_stage_entry_date,stage_before_dead,dead_deal_date,
                        new_deal_date,engaged_deal_date,visit_deal_date,proposal_date,delivered_date,sold_date,
                        snoozed_date,duration_new_to_engaged,duration_engaged_to_visit,duration_visit_to_proposal,
                        duration_porposal_to_sold,duration_sold_to_delivered,duration_new_to_delivered,
                        duration_new_to_dead,duration_latest_active_to_dead,was_snoozed,is_snoozed,
                        genius_phone_captured,genius_email_captured,user_captured,customer_create_date_first_phone,
                        customer_create_date_first_email,customer_create_date_first_facebook
                        ,customer_create_date_last_phone,customer_create_date_last_email,
                        customer_create_date_last_facebook,phone_type_none,phone_type_mobile,phone_type_landline,
                        phone_type_na,phone_type_voip,phone_type_unknown,eligible_for_text,eligible_for_email,
                        eligible_for_video,eligible_for_call,is_genius_phone_captured,is_genius_email_captured,
                        is_user_captured,total_checkins,first_checkin_date,last_checkin_date,has_bad_email,
                        first_appointment_created_date,last_appointment_created_date,first_appointment_scheduled_date,
                        last_appointment_scheduled_date,first_appointment_confirmed_date,
                        last_appointment_confirmed_date,total_appointments,appointment_attended_count,
                        delivery_front_end_gross,delivery_back_end_gross,coalesce(delivery_total_gross,0),
                        delivery_service_contract,delivery_gap,dms_delivery_date,vehicle_interest_type,vehicle_vin,
                        vehicle_id,vehicle_year,vehicle_status,vehicle_make,vehicle_model,vehicle_trim,
                        vehicle_odometer_status,vehicle_new_used_type,vehicle_interest_date_modified,vehicle_payoff,
                        vehicle_allowance,vehicle_cost,vehicle_stock_number,vehicle_interest_rank
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='', encoding='utf-8') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_fact_market_scan(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select deal_id,user_id,store_id,proposal_date,retail_proposal_count,lease_proposal_count,
                        total_proposal_count
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_fact_notes(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='abc') as rs_cur:
                sql = """
                    select deal_id,user_id,store_id,date_created,notes
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='', encoding='utf-8') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_fact_phone_call(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='bcd') as rs_cur:
                sql = """
                    select task_id,customer_id,deal_id,store_id,for_user_id,created_by_user_id,date_created,
                        date_modified,date_due,completed_by_user_id,date_completed,date_completed_short,result,
                        result_name,title,is_deleted,call_queue_id,from_phone_number,to_phone_number,
                        duration_in_seconds,is_from_customer,is_to_customer,recording_url
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_fact_sales_appointments(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select appointment_id,deal_id,customer_id,store_id,appointment_type,date_created,date_modified,
                        created_by_user_id,date_start,is_completed,date_completed,is_confirmed,date_confirmed,
                        assigned_user_id,confirmed_by_user_id,completed_by_user_id,was_attended_by_customer,notes,
                        first_appointment,first_scheduled_appointment,subsequent_appointment,
                        subsequent_scheduled_appointment,is_appt_created_by_user,date_start_short,date_completed_short,
                        date_confirmed_short,date_created_short
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_fact_service_appointments(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select appointment_id,deal_id,customer_id,store_id,appointment_type,date_created,date_modified,
                        created_by_user_id,date_start,date_completed,date_confirmed,assigned_user_id,
                        confirmed_by_user_id,completed_by_user_id,was_attended_by_customer,notes,first_appointment,
                        subsequent_appointment,is_appt_created_by_user,date_start_short,date_completed_short,
                        date_confirmed_short
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_fact_store_app(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select store_id,store_app_name,is_app_enabled,store_app_id,date_started,date_modified
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def dw_fact_text_email_communication(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select communication_id,deal_id,user_id,store_id,communication_date,communication_date_short,
                        ilm_override_start,ilm_override_end,ilm_date_start,ilm_date_end,is_business_hour_communication,
                        communication_modified_date,communication_type,user_type,user_type_name,
                        customer_communication_type,customer_communication_group,customer_id,hasvideo,
                        hasduplicatevideo,hasphoto,is_from_customer,is_to_customer,sentiment_type_mixed,
                        sentiment_type_positive,sentiment_type_neutral,sentiment_type_negative,next_response_time_secs,
                        is_next_response_fumble,adjusted_next_response_time_secs,next_response_less_than_15m,
                        next_response_15m_to_30m,next_response_30m_to_60m,next_response_gt_than_60m,
                        next_response_less_than_60m
                    from {}
                    where store_id in (39,40)
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_appointment(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='cde') as rs_cur:
                sql = """
                    select DISTINCT date_report_ran,entity,store_id,user_id,calendar_date,source,
                        team_name,user_name,store_name,user_type,user_key,
                        vehicle_make,vehicle_model,vehicle_status,unique_set,total_set,
                        confirmed_appointments,unique_scheduled,total_scheduled,total_scheduled_w_video,
                        total_scheduled_w_no_video,total_attended,video_day_appointment,video_show,no_video_show,
                        total_visits,total_beback,total_show,total_delivered
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_dailyactivity(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,team_name,user_type,calendar_date,user_key,
                    total_opportunities,total_showroom_opportunities,total_phone_opportunities,
                    total_internet_opportunities,total_campaign_opportunities,set_appointments,scheduled_appointments,
                    confirmed_appointments,show_appointments,noshow_appointments,total_calls_made,total_contacts_made,
                    total_texts,total_emails,total_peronalized_video,notes,open_tasks,complete_tasks,sold_deals,
                    delivered_deals
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_delivery(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='def') as rs_cur:
                sql = """
                    select entity,store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,
                        vehicle_model,team_name,user_type,calendar_date,total_opportunities,total_sold,total_delivered,
                        total_delivered_units,delivery_front_end_gross,delivery_back_end_gross,delivery_total_gross,
                        delivery_service_contract,delivery_gap,date_report_ran
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_after_delivery(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='efg') as rs_cur:
                sql = """
                    select entity,store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,
                        vehicle_model,team_name,user_type,calendar_date,user_key,total_delivered,
                        email_at_time_of_delivery,mobile_phone_at_time_of_delivery,birthday_at_time_of_delivery,
                        video_sent,eligible_for_phonecall,delivery_call_attempt,eligible_for_text,
                        delivery_text_attempt,eligible_for_email,delivery_email_attempt,eligible_for_video,
                        delivery_video_attempt,total_attempts,total_engaged_communications,total_eligible_for_contact,
                        total_attempts_after_60_days,total_engaged_comm_after_60_days,eligible_birthday_contact,
                        birthday_call,birthday_text,birthday_video,eligible_anniversary_contact,anniversary_call,
                        anniversary_text,anniversary_video
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_dailyactivity(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,team_name,user_type,calendar_date,user_key,
                        total_opportunities,total_showroom_opportunities,total_phone_opportunities,
                        total_internet_opportunities,total_campaign_opportunities,set_appointments,
                        scheduled_appointments,confirmed_appointments,show_appointments,noshow_appointments,
                        total_calls_made,total_contacts_made,total_texts,total_emails,total_peronalized_video,notes,
                        open_tasks,complete_tasks,sold_deals,delivered_deals,total_incoming_msgs,
                        total_response_less_than_15_mins,total_response_15m_to_30m,total_response_30m_to_60m,
                        total_fumbled_over_1_hour,total_ignored_incoming_msgs
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_days_to_conversion(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='fgh') as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,vehicle_model,
                        team_name,user_type,user_key,calendar_date,total_leads,engaged_day_1,engaged_day_2,
                        engaged_day_3,engaged_day_4,engaged_day_5_9,engaged_day_10_30,engaged_day_31_60,
                        engaged_day_61_90,engaged_some_time,total_engaged,visit_day_1,visit_day_2,visit_day_3,
                        visit_day_4,visit_day_5_9,visit_day_10_30,visit_day_31_60,visit_day_61_90,visit_some_time,
                        total_visit,delivery_day_1,delivery_day_2,delivery_day_3,delivery_day_4,delivery_day_5_9,
                        delivery_day_10_30,delivery_day_31_60,delivery_day_61_90,delivery_some_time
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_days_to_delivered(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='ghi') as rs_cur:
                sql = """
                    select entity,store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,
                        vehicle_model,team_name,user_type,calendar_date,user_key,net_deals,delivery_day_1,
                        delivery_day_2,delivery_day_3,delivery_day_4,delivery_day_5,delivery_day_6,delivery_day_7,
                        delivery_day_8,delivery_day_9,delivery_day_10_30,delivery_day_31_60,delivery_day_61_90,
                        total_delivered
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_engaged_visit(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='hij') as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,vehicle_model,
                        team_name,user_type,calendar_date,user_key,net_engaged_deals,no_email_available,
                        email_added_sameday,bad_email,no_mobile_available,mobile_added_sameday,
                        genius_mobile_added_sameday,genius_email_added_sameday,genius_hangoff_user_bh,
                        gen_user_avg_response_time_bh,gen_user_fumbled_over_1_hour_bh,gen_user_message_ignored_bh,
                        total_incoming_msgs_bh,total_incoming_text_msgs_bh,user_text_avg_response_time_bh,
                        user_text_fumbled_over_1_hour_bh,user_text_message_ignored_bh,genius_avg_response_time_bh,
                        total_incoming_email_msgs_bh,user_email_avg_response_time_bh,user_email_fumbled_over_1_hour_bh,
                        user_email_ignored_communication_bh,genius_email_avg_response_time_bh,
                        user_total_avg_response_time_bh,user_total_fumbled_over_1_hour_bh,user_total_message_ignored_bh,
                        genius_total_avg_response_time_bh,genius_hangoff_user_ah,gen_user_avg_response_time_ah,
                        gen_user_fumbled_over_1_hour_ah,gen_user_message_ignored_ah,total_incoming_msgs_ah,
                        total_incoming_text_msgs_ah,user_text_avg_response_time_ah,user_text_fumbled_over_1_hour_ah,
                        user_text_message_ignored_ah,genius_avg_response_time_ah,total_incoming_email_msgs_ah,
                        user_email_avg_response_time_ah,user_email_fumbled_over_1_hour_ah,
                        user_email_ignored_communication_ah,genius_email_avg_response_time_ah,
                        user_total_avg_response_time_ah,user_total_fumbled_over_1_hour_ah,user_total_message_ignored_ah,
                        genius_total_avg_response_time_ah,video_eligible_opp,personalized_video,user_appt_created,
                        genius_appt_created,total_appt_created,appt_video_eligible,appt_person_video,
                        confirmed_appointments,video_show,no_video_show,appt_show,total_visits,total_dead,avg_dead_age
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_lead_engaged(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='ijk') as rs_cur:
                sql = """
                    select calendar_date,store_id,user_id,store_name,user_name,source,vehicle_status,vehicle_make,
                        vehicle_model,team_name,user_type,user_key,total_leads,bad_lead_count,net_leads,
                        first_response_bh_time_seconds,fumble_count_bh,first_response_ah_time_seconds,
                        fumble_count_ah,first_response_bh_genius,first_response_ah_genius,bh_total_leads,
                        bh_bad_lead_count,bh_net_leads,ah_total_leads,ah_bad_lead_count,ah_net_leads,
                        eligible_call_bh_day_1,eligible_text_bh_day_1,eligible_email_bh_day_1,eligible_video_bh_day_1,
                        day_1_bh_call_count,day_1_bh_text_count,day_1_bh_email_count,day_1_bh_video_count,
                        day_1_bh_avg_response_time_call,day_1_bh_call_fumble_count,day_1_bh_avg_response_time_email,
                        day_1_bh_email_fumble_count,day_1_bh_avg_response_time_text,day_1_bh_text_fumble_count,
                        day_1_bh_avg_response_time_video,day_1_bh_video_fumble_count,user_engaged_day_1,
                        genius_engaged_bh_day_1,eligible_call_ah_day_1,eligible_text_ah_day_1,eligible_email_ah_day_1,
                        eligible_video_ah_day_1,day_1_ah_call_count,day_1_ah_text_count,day_1_ah_email_count,
                        day_1_ah_video_count,day_1_ah_avg_response_time_call,day_1_ah_call_fumble_count,
                        day_1_ah_avg_response_time_email,day_1_ah_email_fumble_count,day_1_ah_avg_response_time_text,
                        day_1_ah_text_fumble_count,day_1_ah_avg_response_time_video,day_1_ah_video_fumble_count,
                        genius_engaged_ah_day_1,day_2_not_engaged,day_3_not_engaged,day_4_not_engaged,
                        day_5_not_engaged,day_2_non_genius_attempt,day_2_non_genius_engaged,day_2_genius_engaged,
                        day_3_non_genius_attempt,day_3_non_genius_engaged,day_3_genius_engaged,
                        day_4_non_genius_attempt,day_4_non_genius_engaged,day_4_genius_engaged,day_5_non_genius_attempt,
                        day_5_non_genius_engaged,day_5_genius_engaged,email_engaged,text_engaged,call_engaged,
                        video_engaged,engaged_with_in_5_days,engaged_after_5_days,user_engaged,genius_engaged,
                        dead_before_engaged_count,avg_days_dead_before_engaged,duplicate_clear_count,
                        bh_duplicate_clear_count,ah_duplicate_clear_count
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_lead_summary(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,vehicle_model,
                        team_name,user_type,calendar_date,user_key,total_leads,bad_deals,net_leads,bh_avg_response_time,
                        bh_fumbled_over_1_hour,video_in_24hrs,total_messages,response_within_15mins,
                        response_within_15to30,response_within_30to60,response_fumble,message_ignored,
                        user_avg_response_time,set_appointments,genius_set_appointments,confirmed_appointment,
                        showed_appointment,total_sold,total_delivered,non_sold,video_in_24hrs_visit,total_dead,
                        avg_dead_age
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_phone(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,vehicle_model,
                        team_name,user_type,calendar_date,user_key,total_deals,no_email_available,email_added_sameday
                        ,no_mobile_available,mobile_added_sameday,user_avg_response_time,fumbled_over_1_hour,
                        ignored_communication,total_appointments,confirmed_appointments,appointment_showed,
                        total_visits,total_delivered,total_dead,avg_dead_age
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_pipeline_health(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,source,vehicle_make,vehicle_model,vehicle_status,
                        team_name,user_type,user_key,total_leads,leads_day_1_4,leads_day_5_9,leads_day_10_30,
                        leads_day_31_60,leads_day_61_90,leads_day_91_on,total_engaged,engaged_day_1_4,engaged_day_5_9,
                        engaged_day_10_30,engaged_day_31_60,engaged_day_61_90,engaged_day_91_on,total_visits,
                        visit_day_1_4,visit_day_5_9,visit_day_10_30,visit_day_31_60,visit_day_61_90,visit_day_91_on,
                        total_proposal,proposal_day_1_4,proposal_day_5_9,proposal_day_10_30,proposal_day_31_60,
                        proposal_day_61_90,proposal_day_91_on,total_sold,sold_day_1_4,sold_day_5_9,sold_day_10_30,
                        sold_day_31_60,sold_day_61_90,sold_day_91_on
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_response_time(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='jkl') as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,vehicle_model,
                        team_name,user_type,calendar_date,user_key,total_incoming_msgs_bh,total_incoming_text_msgs_bh,
                        text_avg_response_time_bh,text_fumbled_over_1_hour_bh,text_ignored_communication_bh,
                        total_incoming_email_msgs_bh,email_avg_response_time_bh,email_fumbled_over_1_hour_bh,
                        email_ignored_communication_bh,avg_incoming_response_time_bh,response_less_than_15_mins_bh,
                        response_15m_to_30m_bh,response_30m_to_60m_bh,fumbled_over_1_hour_bh,ignored_incoming_msgs_bh,
                        avg_response_time_genius_bh,total_incoming_msgs_ah,total_incoming_text_msgs_ah,
                        text_avg_response_time_ah,text_fumbled_over_1_hour_ah,text_ignored_communication_ah,
                        total_incoming_email_msgs_ah,email_avg_response_time_ah,email_fumbled_over_1_hour_ah,
                        email_ignored_communication_ah,avg_incoming_response_time_ah,response_less_than_15_mins_ah,
                        response_15m_to_30m_ah,response_30m_to_60m_ah,fumbled_over_1_hour_ah,ignored_incoming_msgs_ah,
                        avg_response_time_genius_ah
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_source(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='klm') as rs_cur:
                sql = """
                    select store_id,store_name,source,source_description,vehicle_status,vehicle_make,vehicle_model,
                        calendar_date,opportunities_from_all_sources,total_opportunities,total_appointments,
                        appointment_show,total_show,total_sold,total_delivered,total_dead,delivery_front_end_gross,
                        delivery_back_end_gross,delivery_total_gross
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_visit(file_name, table_name):
    """
        04/15/21: lots of these integer fields, somehow, ended up as numeric, 1.0, etc
    """
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,vehicle_model,
                        team_name,user_type,user_key,calendar_date,
                        total_visits::integer,eligible_email_opportunity::integer,
                        visit_email_capture_count::integer,
                        eligible_mobile_opportunity::integer,visit_mobile_capture_count::integer,
                        total_nonsold::integer,avg_response_time::integer,fumbled_over_1_hour::integer,
                        ignored_communication::integer,
                        scheduled_appointments::integer,confirmed_appointments::integer,show_appointments::integer,
                        unique_bebacks::integer,total_sold::integer,
                        total_delivered::integer,non_sold::integer,total_dead::integer,avg_dead_age::integer
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_enhanced_visit_delivered(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select store_id,store_name,user_id,user_name,calendar_date,source,vehicle_status,vehicle_make,
                        vehicle_model,team_name,user_type,user_key,total_visits,eligible_email_opportunities,
                        captured_email_opportunities,eligible_phone_opportunities,captured_mobile_opportunities,
                        total_proposal_count,total_day1_sold,total_day1_delivered,non_sold_traffic,
                        response_eligible_by_call,non_genius_call_attempted,response_eligible_by_text,
                        non_genius_text_attempted,response_eligible_by_email,non_genius_email_attempted,
                        response_eligible_by_video,video_sent,day2_non_sold,day2_attempted,day2_engaged_by_user,
                        day3_non_sold,day3_attempted,day3_engaged_by_user,day4_non_sold,day4_attempted,
                        day4_engaged_by_user,appointments_created,appointments_confirmed,appointments_shown,
                        total_beback_credits,total_sold_beback_credits,total_delivered_beback_credits,total_sold
                        ,total_delivered
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_lead(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='lmn') as rs_cur:
                sql = """
                    select date_report_ran,entity,store_id,store_name,user_id,user_name,source,vehicle_status,
                        vehicle_make,vehicle_model,team_name,user_type,calendar_date,user_key,total_leads,bad_leads,
                        net_leads,net_responded_bh_leads,bh_sum_response_time,ah_avg_response_time,
                        bh_avg_response_time,bh_fumbled_over_1_hour,set_appointments,genius_set_appointments,
                        confirmed_appointment,showed_appointment,total_visits,total_proposals,total_sold,
                        total_delivered,total_half_deals,total_delivered_units,total_dead,avg_dead_age,
                        sum_days_to_dead,total_incoming_msgs_bh,total_incoming_msgs_bh_responded,
                        sum_incoming_response_time_bh,avg_incoming_response_time_bh,response_less_than_15_mins_bh,
                        response_15m_to_30m_bh,response_30m_to_60m_bh,fumbled_over_1_hour_bh,ignored_incoming_msgs_bh,
                        video_in_24hrs
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_phone(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor() as rs_cur:
                sql = """
                    select date_report_ran,entity,store_id,store_name,user_id,user_name,source,vehicle_status,
                        vehicle_make,vehicle_model,team_name,user_type,calendar_date,user_key,total_deals,
                        deals_created_without_email,email_added_after_creation,deals_created_without_phone,
                        phone_added_after_creation,user_avg_response_time,fumbled_over_1_hour,ignored_communication,
                        total_appointments,confirmed_appointments,appointment_showed,total_visits,total_proposals,
                        total_delivered,total_half_deals,total_delivered_units,total_dead,avg_dead_age,
                        sum_days_to_dead,total_incoming_msgs_bh,total_incoming_msgs_bh_responded,
                        sum_incoming_response_time_bh,avg_incoming_response_time_bh,response_less_than_15_mins_bh,
                        response_15m_to_30m_bh,response_30m_to_60m_bh,fumbled_over_1_hour_bh,ignored_incoming_msgs_bh,
                        video_in_24hrs
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_popup(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='mno') as rs_cur:
                sql = """
                    select store_name,customer_name,sales_1_user_id,vehicle_year,vehicle_make,vehicle_model,
                        date_created,source,user_name,user_key
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def rpt_visit(file_name, table_name):
    try:
        print(inspect.currentframe().f_code.co_name)
        # file_name = csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv'
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                        inspect.currentframe().f_code.co_name + '.txt')  # returns the running function name
        try:  # clear out the csv directory
            os.remove(file_name)
        except FileNotFoundError:
            pass
        with db_cnx.dc_redshift() as rs_con:
            with rs_con.cursor(name='nop') as rs_cur:
                sql = """
                    select date_report_ran,entity,store_id,store_name,user_id,user_name,source,vehicle_status,
                        vehicle_make,vehicle_model,team_name,user_type,calendar_date,user_key,total_visits,
                        total_proposals,eligible_email_opportunity,visit_email_capture_count,
                        eligible_mobile_opportunity,visit_mobile_capture_count,unique_bebacks,total_sold,
                        total_delivered,total_half_deals,total_delivered_units,non_sold,total_dead,avg_dead_age,
                        sum_days_to_dead,total_incoming_msgs_bh,total_incoming_msgs_bh_responded,
                        sum_incoming_response_time_bh,avg_incoming_response_time_bh,response_less_than_15_mins_bh,
                        response_15m_to_30m_bh,response_30m_to_60m_bh,fumbled_over_1_hour_bh,ignored_incoming_msgs_bh,
                        video_in_24hrs
                    from {}
                """.format(table_name)
                rs_cur.execute(sql)
                with open(file_name, 'w', newline='') as f:
                    csv.writer(f).writerows(rs_cur)
        with open(local_target, 'w') as the_file:
            the_file.write('pass')
    except Exception as e:
        email_errors(inspect.currentframe().f_code.co_name + '.txt', str(e))


def main():
    dw_dim_store(csv_path + 'dw_dim_store\\ext_dw_dim_store.csv', 'rydell.dw_dim_store')

    dw_dim_customer(csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv', 'rydell.dw_dim_customer')

    # dw_dim_date(csv_path + 'dw_dim_date\\ext_dw_dim_date.csv', 'rydell.dw_dim_date')

    dw_dim_user(csv_path + 'dw_dim_user\\ext_dw_dim_user.csv', 'rydell.dw_dim_user')

    dw_fact_customer_communication(csv_path + 'dw_fact_customer_communication\\ext_dw_fact_customer_communication.csv',
                                   'rydell.dw_fact_customer_communication')

    dw_fact_deal(csv_path + 'dw_fact_deal\\ext_dw_fact_deal.csv', 'rydell.dw_fact_deal')

    dw_fact_market_scan(csv_path + 'dw_fact_market_scan\\ext_dw_fact_market_scan.csv', 'rydell.dw_fact_market_scan')

    dw_fact_notes(csv_path + 'dw_fact_notes\\ext_dw_fact_notes.csv', 'rydell.dw_fact_notes')

    dw_fact_phone_call(csv_path + 'dw_fact_phone_call\\ext_dw_fact_phone_call.csv', 'rydell.dw_fact_phone_call')

    dw_fact_sales_appointments(csv_path + 'dw_fact_sales_appointments\\ext_dw_fact_sales_appointments.csv',
                               'rydell.dw_fact_sales_appointments')

    dw_fact_service_appointments(csv_path + 'dw_fact_service_appointments\\ext_dw_fact_service_appointments.csv',
                                 'rydell.dw_fact_service_appointments')

    dw_fact_store_app(csv_path + 'dw_fact_store_app\\ext_dw_fact_store_app.csv', 'rydell.dw_fact_store_app')

    dw_fact_text_email_communication(csv_path + 'dw_fact_text_email_communication\\'
                                     'ext_dw_fact_text_email_communication.csv',
                                     'rydell.dw_fact_text_email_communication')



if __name__ == '__main__':
    main()
