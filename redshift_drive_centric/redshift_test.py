# encoding=utf-8
import csv
import db_cnx
"""
locally, when openvpn is connected, postgresql can not connect
presumably because postgresql is connecting via the vpn to the store
so, for the purpose of testing and running locally, need to separate download and parse operations
"""

def dw_dim_customer():
    try:
        file_name = 'files/ext_dw_dim_customer.csv'
        # with db_cnx.dc_redshift() as rs_con:
        #     with rs_con.cursor() as rs_cur:
        #         sql = """
        #             select customer_id,store_id,date_created,date_modified,customer_type,is_company,
        #                 first_name,last_name,company_name,primary_photo_url,customer_primary_email,
        #                 customer_primary_phone,customer_secodary_phone,customer_third_phone,
        #                 email_unsubscribe_date,phone_unsubscribe_date,snailmail_unsubscribe_date,
        #                 text_unsubscribe_date,customer_optin_date,is_text_opt_out,is_text_opt_in,
        #                 first_opted_out_phone_number,second_opted_out_phone_number,
        #                 third_opted_out_phone_number,best_contact_method,customer_city,
        #                 customer_state,customer_zip,date_of_birth,primary_sales_user_id,
        #                 secondary_sales_user_id,bdc_sales_user_id
        #             from rydell.dw_dim_customer;
        #         """
            #     rs_cur.execute(sql)
            #     with open(file_name, 'w') as f:
            #         csv.writer(f).writerows(rs_cur)
                # with open(file_name) as in_file, open(clean_file_name, 'w') as out_file:
                #     writer = csv.writer(out_file)
                #     for row in csv.reader(in_file):
                #         if any(field.strip() for field in row):
                #             writer.writerow(row)
        with db_cnx.pg() as pg_con:
            with pg_con.cursor() as pg_cur:
                with open(file_name, 'r') as io:
                    pg_cur.copy_expert("""copy dc.ext_dw_dim_customer from stdin with csv encoding 'latin-1'""", io)
    finally:
        # if rs_con:
        #     rs_con.close()
        if pg_con:
            pg_con.close()


def main():
    dw_dim_customer()


if __name__ == '__main__':
    main()
