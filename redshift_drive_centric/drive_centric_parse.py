# encoding=utf-8
import db_cnx

import smtplib
from email.message import EmailMessage
import datetime
import inspect

"""
locally, when openvpn is connected, postgresql can not connect
presumably because postgresql is connecting via the vpn to the store
so, for the purpose of testing and running locally, need to separate download and parse operations

may want  to move the parsing part to luigi/linux, in which case th this would look quite different, in fact, 
why not, the only dependencies are the csv files and postgresql
ehh, the csv files, can i get to those (on a windows server) from the luigi server? 
hmmm

04/14/21
first full run
    several tables failed on data type, numeric data to an integer field
        -ext_dw_fact_customer_communication
        -ext_dw_fact_deal
        -ext_rpt_dailyactivity
        -ext_rpt_delivery
        -ext_rpt_enhanced_dailyactivity 
        -ext_rpt_enhanced_days_to_conversion
        -ext_rpt_enhanced_days_to_delivered 
        -ext_rpt_enhanced_engaged_visit
        -ext_rpt_enhanced_phone
        -ext_rpt_enhanced_pipeline_health
        -ext_rpt_enhanced_source
        -ext_rpt_enhanced_visit
        -ext_rpt_lead
        -ext_rpt_phone
        ext_rpt_visit
    one table with a decoding error:
        COPY from stdin failed: error in .read() call: UnicodeDecodeError 'charmap' codec can't 
        decode byte 0x9d in position 6853: character maps to <undefined>
        CONTEXT:  COPY ext_dw_fact_notes, line 13936
darn, i never did document to ddl generating process, guess its time to do that now
good, found the bug in generate_table_ddl.sql
            
4/15/21
missed 2 in data type fix:
    ext_dw_fact_deal: this one was a data type goof
    ext_rpt_enhanced_visit: this one is weird, data type correct, in the redshift table, the data are integers,
        but the csv file shows 1.0, 0.0, etc, for what appears to be the first 5 integer fields
        1st, i will try a new download and see what that looks like, in that download i will cast those 5 fields
        total_visists to integer:  select ...  total_visits::integer ...
    that took care of the first 5 fields, but still others scattered througout the data, cast all the fields
and the decoding error   
    in function ext_tables
    changed:
        with open(file_name, 'r') as io:
    to:
        with open(file_name, 'r', encoding='utf-8') as io: 
    
skipping tables: dw_fact_closed_ro         
4/18/21
ready for the first production run, download ran successfully   
    1. dw_dim_user - i didn't save the function - fixed
    2. dw_dim_customer: COPY from stdin failed: error in .read() call: UnicodeDecodeError 'utf-8' codec can't 
            decode byte 0xc3 in position 1427: invalid continuation byte
        in function ext_tables
        changed:
            with open(file_name, 'r', encoding='utf-8') as io: 
        to:
            with open(file_name, 'r') as io:  
    3. dw_fact_sales_appointments: COPY from stdin failed: error in .read() call: UnicodeDecodeError 'utf-8' codec 
            can't decode bytes in position 1588-1590: invalid continuation byte     
        fixed by 2.    
        
4/19/21 
    fixed my goofs in the the functions, i negected the > current_date in both the changed_rows and update old row   
    but now have 2, dw_fact_notes & dw_fact_deal that failed with:
        COPY from stdin failed: error in .read() call: UnicodeDecodeError 'charmap' codec can't decode byte ...  
    which in the download of the same 2 files, was fixed by adding encoding='utf-8' to the open(file_name ..., 
    but the failure there was can't encode
    above, actually removed that encoding in function ext_tables() and replaced it with encoding 'latin-1'
    so, first thought is, need different encoding for different files, seems logical but goofy as well
    ok, not sure i really understand
    but the fix was to change in function ext_Tables:
        with open(file_name, 'r') as io:
        to
        with open(file_name, 'r', encoding='latin-1') as io:
    we will see tomorrow, when i run it all again
11/14/21
    none of this data has been used in 7 months, and i am starting to stress out about disk space
    had thought i might refactor, but went drastic, truncated most tables, tables sales_appointments,
    phone_call, service_appointments have been failing daily: duplicate key value violates unique constraint 
    and i just decided to stop it all for now
"""

# local_or_production = 'production'
local_or_production = 'local'

if local_or_production == 'local':
    csv_path = 'Z:\\E\\python_projects\\redshift_drive_centric\\redshift_csv_files\\'
    local_target_path = 'Z:\\E\\python_projects\\redshift_drive_centric\\local_target\\'
elif local_or_production == 'production':
    csv_path = 'blah blah blah'


def email_errors(ext_table_name, error_message):
    msg = EmailMessage()
    msg.set_content(error_message)
    msg['Subject'] = 'processing of ' + ext_table_name + ' has failed'
    msg['From'] = 'jandrews@cartiva.com'
    msg['To'] = 'jandrews@cartiva.com'
    s = smtplib.SMTP('mail.cartiva.com')
    s.send_message(msg)
    s.quit()


def ext_tables(file_name, ext_table_name, xfm_function):
    try:
        print(ext_table_name)
        local_target = (local_target_path + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' + xfm_function)
        with db_cnx.pg() as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "truncate " + ext_table_name
                pg_cur.execute(sql)
                with open(file_name, 'r', encoding='latin-1') as io:
                    pg_cur.copy_expert("""copy {} from stdin with csv encoding 'latin-1'""".format(ext_table_name), io)
                pg_con.commit()
                sql = "select " + xfm_function
                pg_cur.execute(sql)
        with open(local_target, 'w') as file:
            file.write('pass')
    except Exception as e:
        email_errors(ext_table_name, str(e))


def ext_dw_dim_customer(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)
    

def ext_dw_dim_date(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


def ext_dw_dim_store(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


def ext_dw_dim_user(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


# def ext_dw_fact_closed_ro(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)


def ext_dw_fact_customer_communication(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


def ext_dw_fact_deal(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


def ext_dw_fact_market_scan(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


def ext_dw_fact_notes(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


def ext_dw_fact_phone_call(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


def ext_dw_fact_sales_appointments(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


def ext_dw_fact_service_appointments(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)


def ext_dw_fact_store_app(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)

def ext_dw_fact_text_email_communication(file_name, ext_table_name, xfm_function):
    ext_tables(file_name, ext_table_name, xfm_function)

# def ext_rpt_appointment(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_dailyactivity(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_delivery(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_after_delivery(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_dailyactivity(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_days_to_conversion(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_days_to_delivered(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_engaged_visit(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_lead_engaged(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_lead_summary(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_phone(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_pipeline_health(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_response_time(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_source(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_visit(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_enhanced_visit_delivered(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_lead(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_phone(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_popup(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)
#
#
# def ext_rpt_visit(file_name, ext_table_name):
#     ext_tables(file_name, ext_table_name)


def main():
    ext_dw_dim_store(csv_path + 'dw_dim_store\\ext_dw_dim_store.csv', 'dc.ext_dw_dim_store',
                     'dc.xfm_dw_dim_store()')

    ext_dw_dim_customer(csv_path + 'dw_dim_customer\\ext_dw_dim_customer.csv', 'dc.ext_dw_dim_customer',
                        'dc.xfm_dw_dim_customer()')

    # # ext_dw_dim_date(csv_path + 'dw_dim_date\\ext_dw_dim_date.csv', 'dc.ext_dw_dim_date', 'dc.xfm_dw_dim_date()')


    ext_dw_dim_user(csv_path + 'dw_dim_user\\ext_dw_dim_user.csv', 'dc.ext_dw_dim_user', 'dc.xfm_dw_dim_user()')

    ext_dw_fact_customer_communication(csv_path + 'dw_fact_customer_communication\\'
                                       'ext_dw_fact_customer_communication.csv',
                                       'dc.ext_dw_fact_customer_communication',
                                       'dc.xfm_dw_fact_customer_communication()')

    ext_dw_fact_deal(csv_path + 'dw_fact_deal\\ext_dw_fact_deal.csv', 'dc.ext_dw_fact_deal', 'dc.xfm_dw_fact_deal()')

    ext_dw_fact_market_scan(csv_path + 'dw_fact_market_scan\\ext_dw_fact_market_scan.csv', 'dc.ext_dw_fact_market_scan',
                            'dc.xfm_dw_fact_market_scan()')

    ext_dw_fact_notes(csv_path + 'dw_fact_notes\\ext_dw_fact_notes.csv', 'dc.ext_dw_fact_notes',
                      'dc.xfm_dw_fact_notes()')

    ext_dw_fact_phone_call(csv_path + 'dw_fact_phone_call\\ext_dw_fact_phone_call.csv', 'dc.ext_dw_fact_phone_call',
                           'dc.xfm_dw_fact_phone_call()')

    ext_dw_fact_sales_appointments(csv_path + 'dw_fact_sales_appointments\\ext_dw_fact_sales_appointments.csv',
                                   'dc.ext_dw_fact_sales_appointments', 'dc.xfm_dw_fact_sales_appointments()')

    ext_dw_fact_service_appointments(csv_path + 'dw_fact_service_appointments\\ext_dw_fact_service_appointments.csv',
                                     'dc.ext_dw_fact_service_appointments', 'dc.xfm_dw_fact_service_appointments()')

    ext_dw_fact_store_app(csv_path + 'dw_fact_store_app\\ext_dw_fact_store_app.csv', 'dc.ext_dw_fact_store_app',
                          'dc.xfm_dw_fact_store_app()')

    ext_dw_fact_text_email_communication(csv_path + 'dw_fact_text_email_communication\\'
                                     'ext_dw_fact_text_email_communication.csv',
                                     'dc.ext_dw_fact_text_email_communication',
                                     'dc.xfm_dw_fact_text_email_communication()')

    # ext_dw_fact_closed_ro(csv_path + 'dw_fact_closed_ro\\ext_dw_fact_closed_ro.csv', 'dc.ext_dw_fact_closed_ro')


if __name__ == '__main__':
    main()
