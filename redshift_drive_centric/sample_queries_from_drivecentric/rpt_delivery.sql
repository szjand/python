{{ config(materialized='table', sort=['store_id','user_id','user_name','source', 'vehicle_make', 'vehicle_model','vehicle_status','user_type'], dist='calendar_date') }}

WITH sold_cte_1 AS (
SELECT
  'User' entity
  , dfd.store_id
  , dds.store_name store_name
  , ddu.user_id user_id
  , nvl(ddu.first_name || ' ' || ddu.last_name,'Unknown_' || ddu.user_id) user_name
  , dfd.source
  , dfd.vehicle_new_used_type vehicle_status
  , dfd.vehicle_make vehicle_make
  , dfd.vehicle_model vehicle_model
  , null team_name
  , nvl(ddu.user_type,'None') user_type
  , trunc(dfd.sold_date) calendar_date
  , 0 total_opportunities
  , count (distinct dfd.deal_id) total_sold
  , 0 total_delivered
  , 0 total_delivered_units
  , 0 delivery_front_end_gross
  , 0 delivery_back_end_gross
  , 0 delivery_total_gross
  , 0 delivery_service_contract
  , 0 delivery_gap
  from {{ ref('stg_rpt_deal') }} dfd
  join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id IN (dfd.sales_1_user_id,dfd.sales_2_user_id,dfd.ilm_user_id)		
  where sold_date is not null
  group by
    dfd.store_id
  , dds.store_name
  , ddu.user_id
  , user_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , ddu.user_type
  , trunc(dfd.sold_date)
  ),
delivered_cte_1 AS (
SELECT
  'User' entity
  , dfd.store_id
  , dds.store_name store_name
  , ddu.user_id user_id
  , nvl(ddu.first_name || ' ' || ddu.last_name,'Unknown_' || ddu.user_id) user_name
  , dfd.source
  , dfd.vehicle_new_used_type vehicle_status
  , dfd.vehicle_make vehicle_make
  , dfd.vehicle_model vehicle_model
  , null team_name
  , nvl(ddu.user_type,'None') user_type
  , trunc(dfd.delivered_date) calendar_date
  , 0 total_opportunities
  , 0 total_sold
  , count (distinct dfd.deal_id) total_delivered
  , total_delivered - (cast(count(distinct case when dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) / 2) total_delivered_units
  , sum(delivery_front_end_gross) delivery_front_end_gross
  , sum(delivery_back_end_gross) delivery_back_end_gross
  , sum(delivery_total_gross) delivery_total_gross
  , sum(delivery_service_contract) delivery_service_contract
  , sum(delivery_gap) delivery_gap
  from {{ ref('stg_rpt_deal') }} dfd
  join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id IN (dfd.sales_1_user_id,dfd.sales_2_user_id,dfd.ilm_user_id)		
  where dfd.delivered_date is not null
  group by
    dfd.store_id
  , dds.store_name
  , ddu.user_id
  , user_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , ddu.user_type
  , dfd.delivered_date
),
opportunities_cte_1 AS (
SELECT
  'User' entity
  , dfd.store_id
  , dds.store_name store_name
  , ddu.user_id user_id
  , nvl(ddu.first_name || ' ' || ddu.last_name,'Unknown_' || ddu.user_id) user_name
  , dfd.source
  , dfd.vehicle_new_used_type vehicle_status
  , dfd.vehicle_make vehicle_make
  , dfd.vehicle_model vehicle_model
  , null team_name
  , nvl(ddu.user_type,'None') user_type
  , trunc(dfd.date_created) calendar_date
  , count (distinct dfd.deal_id) total_opportunities
  , 0 total_sold
  , 0 total_delivered
  , 0 total_delivered_units
  , 0 delivery_front_end_gross
  , 0 delivery_back_end_gross
  , 0 delivery_total_gross
  , 0 delivery_service_contract
  , 0 delivery_gap
  from {{ ref('stg_rpt_deal') }} dfd
  join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id IN (dfd.sales_1_user_id,dfd.sales_2_user_id,dfd.ilm_user_id)		  
  group by
    dfd.store_id
  , dds.store_name
  , ddu.user_id
  , user_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , ddu.user_type
  , trunc(dfd.date_created)
),
store_sold_cte as (
   select
        'Store' entity
        , dfd.store_id
        , nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
			  , 0 user_id
			  , 'Store total' user_name
			  , dfd.source
			  , dfd.vehicle_new_used_type vehicle_status
			  , dfd.vehicle_make
			  , dfd.vehicle_model
			  , null team_name
			  , null user_type
			  , trunc(dfd.sold_date) calendar_date
			  , 0 total_opportunities
			  , count(distinct dfd.deal_id) total_sold
			  , 0 total_delivered
        , 0 total_delivered_units
			  , sum(delivery_front_end_gross) delivery_front_end_gross
			  , sum(delivery_back_end_gross) delivery_back_end_gross
			  , sum(delivery_total_gross) delivery_total_gross
			  , sum(delivery_service_contract) delivery_service_contract
			  , sum(delivery_gap) delivery_gap
	   from {{ ref('stg_rpt_deal') }} dfd
     join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
		  where dfd.sold_date is not null
	    group by
			    dfd.store_id, store_name, source, vehicle_new_used_type, vehicle_make, vehicle_model, trunc(sold_date)
),
store_deli_cte as (
   select
         'Store' entity
        , dfd.store_id
        , nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
			  , 0 user_id
			  , 'Store total' user_name
			  , dfd.source
			  , dfd.vehicle_new_used_type vehicle_status
			  , dfd.vehicle_make
			  , dfd.vehicle_model
			  , null team_name
			  , null user_type
			  , trunc(dfd.delivered_date) calendar_date
			  , 0 total_opportunities
			  , 0 total_sold
			  , count(distinct dfd.deal_id) total_delivered
        , count(distinct dfd.deal_id) total_delivered_units
			  , sum(delivery_front_end_gross) delivery_front_end_gross
			  , sum(delivery_back_end_gross) delivery_back_end_gross
			  , sum(delivery_total_gross) delivery_total_gross
			  , sum(delivery_service_contract) delivery_service_contract
			  , sum(delivery_gap) delivery_gap
	  from {{ ref('stg_rpt_deal') }} dfd
    join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
    where  delivered_date is not null
			  group by
			    dfd.store_id, store_name, source, vehicle_new_used_type, vehicle_make, vehicle_model, trunc(delivered_date)
),
store_opp_cte as (
    SELECT
         'Store' entity
        , dfd.store_id
        , nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
			  , 0 user_id
			  , 'Store total' user_name
			  , dfd.source
			  , dfd.vehicle_new_used_type vehicle_status
			  , dfd.vehicle_make
			  , dfd.vehicle_model
			  , null team_name
			  , null user_type
			  , trunc(dfd.date_created) calendar_date
			  , count(distinct deal_id) total_opportunities
			  , 0 total_sold
			  , 0 total_delivered
        , 0 total_delivered_units
			  , sum(delivery_front_end_gross) delivery_front_end_gross
			  , sum(delivery_back_end_gross) delivery_back_end_gross
			  , sum(delivery_total_gross) delivery_total_gross
			  , sum(delivery_service_contract) delivery_service_contract
			  , sum(delivery_gap) delivery_gap
		  from {{ ref('stg_rpt_deal') }} dfd
      join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
      group by
			    dfd.store_id, store_name, source, vehicle_new_used_type, vehicle_make, vehicle_model, trunc(dfd.date_created)
),
team_sold_cte as (
              select
                'Team' entity
                , dfd.store_id              
                , nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
	      			  , 0 user_id
	      			  , 'Team total' user_name
	      			  , dfd.source
	      			  , dfd.vehicle_new_used_type vehicle_status
	      			  , dfd.vehicle_make
	      			  , dfd.vehicle_model
	      			  , nvl('Team '||ddu.user_team_name, 'No Team') team_name
	      			  , null user_type
	      			  , trunc(dfd.sold_date) calendar_date
	      			  , 0 total_opportunities
	      			  , count(distinct dfd.deal_id) total_sold
	      			  , 0 total_delivered
                , 0 total_delivered_units
	      			  , sum(delivery_front_end_gross) delivery_front_end_gross
	      			  , sum(delivery_back_end_gross) delivery_back_end_gross
	      			  , sum(delivery_total_gross) delivery_total_gross
	      			  , sum(delivery_service_contract) delivery_service_contract
	      			  , sum(delivery_gap) delivery_gap
              from {{ ref('stg_rpt_deal') }} dfd
              join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
              left join {{ ref('stg_rpt_user') }}  ddu on   ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
            where dfd.sold_date is not null   
	      	    group by
                 dfd.store_id, store_name, user_team_name, source, vehicle_new_used_type, vehicle_make, vehicle_model, trunc(sold_date)
),
team_deli_cte as (
                         select
              'Team' entity
              , dfd.store_id              

              , nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
		   			  , 0 user_id
		   			  , 'Team total' user_name
		   			  , dfd.source
		   			  , dfd.vehicle_new_used_type vehicle_status
		   			  , dfd.vehicle_make
		   			  , dfd.vehicle_model
		   			  , nvl('Team '||ddu.user_team_name, 'No Team') team_name
		   			  , null user_type
		   			  , trunc(dfd.delivered_date) calendar_date
		   			  , 0 total_opportunities
		   			  , 0 total_sold
		   			  , count(distinct dfd.deal_id) total_delivered
              , count(distinct dfd.deal_id) total_delivered_units               
		   			  , sum(delivery_front_end_gross) delivery_front_end_gross
		   			  , sum(delivery_back_end_gross) delivery_back_end_gross
		   			  , sum(delivery_total_gross) delivery_total_gross
		   			  , sum(delivery_service_contract) delivery_service_contract
		   			  , sum(delivery_gap) delivery_gap
		   	   from {{ ref('stg_rpt_deal') }} dfd
		          join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
		          left join {{ ref('stg_rpt_user') }}  ddu on   ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
		   		  where dfd.delivered_date is not null   
		   	    group by
        dfd.store_id, store_name, user_team_name, source, vehicle_new_used_type, vehicle_make, vehicle_model, trunc(delivered_date)
),
team_opp_cte as (
		 select
            'Team' entity
            , dfd.store_id              
            , nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
					  , 0 user_id
					  , 'Team total' user_name
					  , dfd.source
					  , dfd.vehicle_new_used_type vehicle_status
					  , dfd.vehicle_make
					  , dfd.vehicle_model
					  , nvl('Team '||ddu.user_team_name, 'No Team') team_name
					  , null user_type
					  , trunc(dfd.date_created) calendar_date
					  , count(distinct dfd.deal_id) total_opportunities
					  , 0 total_sold
					  , 0 total_delivered
            , 0 total_delivered_units
					  , sum(delivery_front_end_gross) delivery_front_end_gross
					  , sum(delivery_back_end_gross) delivery_back_end_gross
					  , sum(delivery_total_gross) delivery_total_gross
					  , sum(delivery_service_contract) delivery_service_contract
					  , sum(delivery_gap) delivery_gap
			   from {{ ref('stg_rpt_deal') }} dfd
		       join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
		       left join {{ ref('stg_rpt_user') }}  ddu on   ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
			    group by
    dfd.store_id, store_name, user_team_name, source, vehicle_new_used_type, vehicle_make, vehicle_model, trunc(dfd.date_created)
)
select
    nvl(dlv.entity) entity
  , nvl(dlv.store_id,0) store_id
  , nvl(dlv.store_name, 'None') store_name
  , dlv.user_id
  , nvl(dlv.user_name, 'None') user_name
  , dlv.source
  , nvl(dlv.vehicle_status, 'None') vehicle_status
  , nvl(dlv.vehicle_make,'None') vehicle_make
  , nvl(dlv.vehicle_model,'None') vehicle_model
  , dlv.team_name
  , nvl(dlv.user_type, 'None') user_type
  , dlv.calendar_date
  , sum(total_opportunities) total_opportunities
  , sum(total_sold) total_sold
  , sum(total_delivered) total_delivered
  , sum(total_delivered_units) total_delivered_units
  , sum(delivery_front_end_gross) delivery_front_end_gross
  , sum(delivery_back_end_gross) delivery_back_end_gross
  , sum(delivery_total_gross) delivery_total_gross
  , sum(delivery_service_contract) delivery_service_contract
  , sum(delivery_gap) delivery_gap,
  CURRENT_TIMESTAMP AS date_report_ran
FROM (
  select * from opportunities_cte_1
  union
  select * from delivered_cte_1
  union
  select * from sold_cte_1
  union
  select * From store_sold_cte
  union
  select * From store_deli_cte
  union
  select * from store_opp_Cte
  union
  select * from team_sold_cte
  union
  select * from team_deli_cte
  union
  select * from team_opp_cte
) dlv
group by
    dlv.entity
  , dlv.store_id
  , dlv.store_name
  , dlv.user_id
  , dlv.user_name
  , dlv.source
  , dlv.vehicle_status
  , dlv.vehicle_make
  , dlv.vehicle_model
  , dlv.team_name
  , dlv.user_type
  , dlv.calendar_date