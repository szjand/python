{{ config(materialized='table', sort=['store_id','user_id','user_name','source', 'vehicle_make', 'vehicle_model','vehicle_status','user_type'], dist='calendar_date') }}

WITH appt_rpt_vw AS 
( SELECT dfs.*, 
    row_number() OVER (partition BY dfs.appointment_id ORDER BY dfs.date_modified DESC)	appt_rank
  FROM {{ ref('dw_fact_sales_appointments') }}  dfs 
),
apt_created AS
( SELECT
    dfsa.store_id
  , NVL(dds.store_name, 'Unknown_' || dfsa.store_id) store_name
  , dfsa.created_by_user_id user_id
  , NVL(ddu.first_name || ' ' || ddu.last_name,'Unknown_' || dfsa.created_by_user_id) user_name
  , dfd.source
  , dfd.vehicle_new_used_type vehicle_status
  , dfd.vehicle_make vehicle_make
  , dfd.vehicle_model vehicle_model
  , nvl('Team '||ddu.user_team_name, 'No Team') team_name
  , NVL(ddu.user_type,'None') user_type
  , TRUNC(dfsa.date_created) calendar_date
  , user_name || '|' || dfsa.created_by_user_id user_key
  , CAST(SUM(first_appointment) AS int) unique_set
  , CAST(COUNT(distinct dfsa.appointment_id) AS int) total_set
  , 0 confirmed_appointments
  , 0 unique_scheduled
  , 0 total_scheduled
  , 0 total_scheduled_w_video
  , 0 total_scheduled_w_no_video
  , 0 total_attended
  , 0 video_day_appointment
  , 0 video_show
  , 0 no_video_show
  , 0 total_visits
  , 0 total_beback
  , 0 total_show
  , 0 total_delivered
  FROM  appt_rpt_vw dfsa
  JOIN  {{ ref('stg_rpt_deal') }}  dfd ON dfsa.deal_id = dfd.deal_id
  JOIN  {{ ref('dw_dim_store') }}  dds ON dfsa.store_id = dds.store_id
  JOIN  {{ ref('stg_rpt_user') }}  ddu ON dfsa.created_by_user_id = ddu.user_id
  WHERE dfsa.appt_rank =1 AND  dfsa.appointment_type = 'Sales'
  GROUP BY
   dfsa.store_id
  , dds.store_name
  , dfsa.created_by_user_id
  , user_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , team_name
  , ddu.user_type
  , TRUNC(dfsa.date_created)
),
apt_scheduled AS
(SELECT
   dfsa.store_id
  , NVL(dds.store_name, 'Unknown_' || dfsa.store_id) store_name
  , dfsa.assigned_user_id user_id
  , NVL(ddu.first_name || ' ' || ddu.last_name,'Unknown_' || dfsa.assigned_user_id) user_name
  , dfd.source
  , dfd.vehicle_new_used_type vehicle_status
  , dfd.vehicle_make vehicle_make
  , dfd.vehicle_model vehicle_model
  , nvl('Team '||ddu.user_team_name, 'No Team') team_name
  , NVL(ddu.user_type,'None') user_type
  , TRUNC(dfsa.date_start) calendar_date
  , user_name || '|' || dfsa.assigned_user_id user_key
  , 0 unique_set
  , 0 total_set
  , CAST(SUM(CASE WHEN dfsa.date_confirmed IS NOT NULL AND TRUNC(dfsa.date_confirmed) >= TRUNC(dfsa.date_created) THEN 1  ELSE 0 END) AS int) confirmed_appointments
  , CAST(SUM(first_scheduled_appointment) AS int) unique_scheduled
  , CAST(COUNT(DISTINCT dfsa.appointment_id) AS int) total_scheduled
  , CAST(COUNT(DISTINCT CASE WHEN dfsa.date_start >= dfsa.date_created AND dfcc.hasvideo = 1 THEN dfsa.appointment_id END) AS int) total_scheduled_w_video
  , CAST(COUNT(DISTINCT CASE WHEN dfcc.hasvideo <> 1 THEN dfsa.appointment_id END) AS int) total_scheduled_w_no_video
  , CAST(COUNT(DISTINCT CASE WHEN dfsa.was_attended_by_customer = 1 THEN dfsa.appointment_id END) AS int) total_attended
  , CAST(COUNT(DISTINCT CASE WHEN dfcc.hasvideo = 1 THEN dfsa.appointment_id END) AS int) video_day_appointment
  , CAST(COUNT(DISTINCT CASE WHEN dfcc.hasvideo = 1 AND dfsa.was_attended_by_customer = 1 THEN dfsa.deal_id END) AS int) video_show
  , CAST(COUNT(DISTINCT CASE WHEN (dfcc.hasvideo <> 1) AND dfsa.was_attended_by_customer = 1 THEN dfsa.appointment_id END) AS int) no_video_show
  , CAST(SUM(CASE WHEN dfsa.was_attended_by_customer = 1 AND dfsa.date_completed BETWEEN dfd.first_checkin_date - interval '5 seconds' 
                          AND  dfd.first_checkin_date + interval '5 seconds' THEN 1  ELSE 0  END) AS int)  total_visits
  , CAST(SUM(CASE WHEN dfsa.was_attended_by_customer = 1 AND dfsa.date_completed > dfd.first_checkin_date + interval '5 seconds' THEN 1 ELSE 0 END) AS int) total_beback
  , CAST(COUNT(DISTINCT CASE WHEN dfsa.was_attended_by_customer = 1 THEN dfsa.appointment_id  END) AS int)  total_show
  , CAST(COUNT(CASE WHEN dfsa.was_attended_by_customer = 1 AND dfd.delivered_date IS NOT NULL THEN dfsa.appointment_id END) AS int)  total_delivered
  FROM  appt_rpt_vw dfsa
  LEFT JOIN  {{ ref('dw_fact_customer_communication') }}  dfcc
  ON dfsa.deal_id = dfcc.deal_id AND dfcc.hasvideo = 1 AND TRUNC(dfsa.date_start) = TRUNC(dfcc.communication_date) AND  dfcc.communication_date < dfsa.date_start
  JOIN {{ ref('stg_rpt_deal') }}  dfd
  ON dfsa.deal_id = dfd.deal_id
  JOIN  {{ ref('dw_dim_store') }}  dds ON dfsa.store_id = dds.store_id
  JOIN  {{ ref('stg_rpt_user') }}  ddu ON dfsa.assigned_user_id = ddu.user_id
  WHERE dfsa.appt_rank =1 AND dfsa.appointment_type = 'Sales'
  GROUP BY
    dfsa.store_id
  , dds.store_name
  , dfsa.assigned_user_id
  , user_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , team_name
  , ddu.user_type
  , TRUNC(dfsa.date_start)
),
apt_team_summary AS
(
    SELECT
           'Team' entity
        ,  nvl(appt.store_id,0) store_id
        , 0  user_id
        , appt.calendar_date
        , NVL(appt.source, 'None') source
        , NVL(appt.team_name, 'None') team_name
        , null user_name
        , NVL(appt.store_name, 'None') store_name
        , null user_type
        , null user_key 
        , NVL(appt.vehicle_make, 'None') vehicle_make
        , NVL(appt.vehicle_model, 'None') vehicle_model
        , NVL(appt.vehicle_status, 'None') vehicle_status
        , SUM(appt.unique_set) unique_set
        , SUM(appt.total_set) total_set
        , SUM(appt.confirmed_appointments) confirmed_appointments
        , SUM(appt.unique_scheduled) unique_scheduled
        , SUM(appt.total_scheduled) total_scheduled
        , SUM(appt.total_scheduled_w_video) total_scheduled_w_video
        , SUM(appt.total_scheduled_w_no_video) total_scheduled_w_no_video
        , SUM(appt.total_attended) total_attended
        , SUM(appt.video_day_appointment) video_day_appointment
        , SUM(appt.video_show) video_show
        , SUM(appt.no_video_show) no_video_show
        , SUM(appt.total_visits) total_visits
        , SUM(appt.total_beback) total_beback
        , SUM(appt.total_show) total_show
        , SUM(appt.total_delivered) total_delivered
      FROM
      (
        SELECT * FROM apt_created
        UNION
        SELECT * FROM apt_scheduled
      ) appt
      GROUP BY 
         appt.store_id, 
         appt.store_name, 
         appt.team_name, 
         appt.source, 
         appt.vehicle_status, 
         appt.vehicle_make, 
         appt.vehicle_model, 
         appt.calendar_date
),
apt_store_summary AS
(
    SELECT
           'Store' entity
        ,  nvl(appt.store_id,0) store_id
        , 0  user_id
        , appt.calendar_date
        , NVL(appt.source, 'None') source
        , null team_name
        , null user_name
        , NVL(appt.store_name, 'None') store_name
        , null user_type
        , null user_key 
        , NVL(appt.vehicle_make, 'None') vehicle_make
        , NVL(appt.vehicle_model, 'None') vehicle_model
        , NVL(appt.vehicle_status, 'None') vehicle_status
        , SUM(appt.unique_set) unique_set
        , SUM(appt.total_set) total_set
        , SUM(appt.confirmed_appointments) confirmed_appointments
        , SUM(appt.unique_scheduled) unique_scheduled
        , SUM(appt.total_scheduled) total_scheduled
        , SUM(appt.total_scheduled_w_video) total_scheduled_w_video
        , SUM(appt.total_scheduled_w_no_video) total_scheduled_w_no_video
        , SUM(appt.total_attended) total_attended
        , SUM(appt.video_day_appointment) video_day_appointment
        , SUM(appt.video_show) video_show
        , SUM(appt.no_video_show) no_video_show
        , SUM(appt.total_visits) total_visits
        , SUM(appt.total_beback) total_beback
        , SUM(appt.total_show) total_show
        , SUM(appt.total_delivered) total_delivered
      FROM
      (
        SELECT * FROM apt_created
        UNION
        SELECT * FROM apt_scheduled
      ) appt
      GROUP BY 
         appt.store_id, 
         appt.store_name, 
         appt.source, 
         appt.vehicle_status, 
         appt.vehicle_make, 
         appt.vehicle_model, 
         appt.calendar_date
),
apt_user_summary AS
(
    SELECT
           'User' entity
        ,  nvl(appt.store_id,0) store_id
        , 0  user_id
        , appt.calendar_date
        , NVL(appt.source, 'None') source
        , null team_name
        , user_name user_name
        , NVL(appt.store_name, 'None') store_name
        , null user_type
        , null user_key 
        , NVL(appt.vehicle_make, 'None') vehicle_make
        , NVL(appt.vehicle_model, 'None') vehicle_model
        , NVL(appt.vehicle_status, 'None') vehicle_status
        , SUM(appt.unique_set) unique_set
        , SUM(appt.total_set) total_set
        , SUM(appt.confirmed_appointments) confirmed_appointments
        , SUM(appt.unique_scheduled) unique_scheduled
        , SUM(appt.total_scheduled) total_scheduled
        , SUM(appt.total_scheduled_w_video) total_scheduled_w_video
        , SUM(appt.total_scheduled_w_no_video) total_scheduled_w_no_video
        , SUM(appt.total_attended) total_attended
        , SUM(appt.video_day_appointment) video_day_appointment
        , SUM(appt.video_show) video_show
        , SUM(appt.no_video_show) no_video_show
        , SUM(appt.total_visits) total_visits
        , SUM(appt.total_beback) total_beback
        , SUM(appt.total_show) total_show
        , SUM(appt.total_delivered) total_delivered
      FROM
      (
        SELECT * FROM apt_created
        UNION
        SELECT * FROM apt_scheduled
      ) appt
      GROUP BY 
        appt.store_id
        , appt.user_id
        , appt.user_name
        , appt.source
        , appt.store_name
        , appt.user_type
        , appt.vehicle_make
        , appt.vehicle_model
        , appt.vehicle_status
        , appt.calendar_date

)


SELECT  CURRENT_TIMESTAMP AS date_report_ran, *
FROM
      (
          SELECT * FROM apt_team_summary
          UNION
          SELECT * FROM apt_store_summary
          UNION
          SELECT * FROM apt_user_summary

      ) totals
