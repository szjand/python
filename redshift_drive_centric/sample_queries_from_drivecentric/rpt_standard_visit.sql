{{ config(materialized='table', sort=['store_id','user_id','user_name','source', 'vehicle_make', 'vehicle_model','vehicle_status','user_type'], dist='calendar_date') }}

WITH deallog_checkin as (
   SELECT 
     pkdeallogid,
     datecreated as checkin_date, 
     fkdealid 
   FROM {{ source ('drive_data', 'deallog') }}
   WHERE deallogtype = 108
   AND fkdealid > 0
),
deallog_demo as (
   SELECT 
     pkdeallogid,
     datecreated as demo_date, 
     fkdealid 
   FROM {{ source ('drive_data', 'deallog') }}
   WHERE deallogtype = 2001
   AND fkdealid > 0
),
deallog_to as (
   SELECT 
     pkdeallogid,
     datecreated as to_date, 
     fkdealid 
   FROM {{ source ('drive_data', 'deallog') }}
   WHERE deallogtype = 2004
   AND fkdealid > 0
),
deallog_proposal as (
   SELECT 
     pkdeallogid,
     datecreated as proposal_date, 
     fkdealid 
   FROM {{ source ('drive_data', 'deallog') }}
   WHERE deallogtype = 2000
   AND fkdealid > 0
),
standard_visit_user AS
(select
  'User' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, ddu.user_id user_id
, nvl(ddu.first_name || ' ' || ddu.last_name,'Unknown_' || ddu.user_id) user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, null team_name
, nvl(ddu.user_type,'None') user_type
, trunc(dfd.visit_deal_date) calendar_date
, user_name || '|' || ddu.user_id user_key
, cast(count(distinct dl_checkin.pkdeallogid) as real) total_visits
, cast(count(distinct dfd.deal_id) as real) unique_visits
, cast(count(distinct case when trunc(dfd.first_checkin_date) = trunc(dl_checkin.checkin_date) and dfd.last_checkin_date is null then dfd.deal_id end) as real) initial_visits
, null beback_visits--, cast(count(distinct case when trunc(dfd.first_checkin_date) <> trunc(dl.datecreated) or count(*) > 1 end) as real) beback_visits -- todo
, cast(count(distinct case when dfd.source = 'Showroom' then dfd.deal_id end) as real) unique_visits_showroom
, cast(count(distinct case when dfd.source = 'Phone' then dfd.deal_id end) as real) unique_visits_phone
, cast(count(distinct case when dfd.source = 'Internet' then dfd.deal_id end) as real) unique_visits_internet
, cast(count(distinct case when dfd.source = 'Campaign' then dfd.deal_id end) as real) unique_visits_campaign
, cast(count(distinct case when dl_demo.pkdeallogid is not null then dfd.deal_id end) as real) unique_visits_w_demo
, cast(count(distinct case when dl_to.pkdeallogid is not null then dfd.deal_id end) as real) unique_visits_w_to
, cast(count(distinct case when dl_proposal.pkdeallogid is not null then dfd.deal_id end) as real) unique_visits_w_proposal
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date then dfd.deal_id end) as int) unique_visits_delivered
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, unique_visits_delivered - (total_half_deals / 2) total_delivered_units
from deallog_checkin dl_checkin
  inner join {{ ref('stg_rpt_deal') }} dfd on dfd.deal_id = dl_checkin.fkdealid
  left join deallog_demo dl_demo on dfd.deal_id = dl_demo.fkdealid
  left join deallog_to dl_to on dfd.deal_id = dl_to.fkdealid
  left join deallog_proposal dl_proposal on dfd.deal_id = dl_proposal.fkdealid
  left join {{ ref('dw_fact_customer_communication') }} dfcc on dfd.deal_id = dfcc.deal_id
  inner join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
group by dfd.deal_id
  , dfd.store_id
  , dds.store_name
  , ddu.user_id
  , user_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , ddu.user_type
  , trunc(dfd.visit_deal_date)
),
standard_visit_team AS
(select
  'Team' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, 0 user_id
, null user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, nvl('Team '||ddu.user_team_name, 'No Team') team_name
, null user_type
, trunc(dfd.visit_deal_date) calendar_date
, null user_key
, cast(count(distinct dl_checkin.pkdeallogid) as real) total_visits
, cast(count(distinct dfd.deal_id) as real) unique_visits
, cast(count(distinct case when trunc(dfd.first_checkin_date) = trunc(dl_checkin.checkin_date) and dfd.last_checkin_date is null then dfd.deal_id end) as real) initial_visits
, null beback_visits--, cast(count(distinct case when trunc(dfd.first_checkin_date) <> trunc(dl.datecreated) or count(*) > 1 end) as real) beback_visits -- todo
, cast(count(distinct case when dfd.source = 'Showroom' then dfd.deal_id end) as real) unique_visits_showroom
, cast(count(distinct case when dfd.source = 'Phone' then dfd.deal_id end) as real) unique_visits_phone
, cast(count(distinct case when dfd.source = 'Internet' then dfd.deal_id end) as real) unique_visits_internet
, cast(count(distinct case when dfd.source = 'Campaign' then dfd.deal_id end) as real) unique_visits_campaign
, cast(count(distinct case when dl_demo.pkdeallogid is not null then dfd.deal_id end) as real) unique_visits_w_demo
, cast(count(distinct case when dl_to.pkdeallogid is not null then dfd.deal_id end) as real) unique_visits_w_to
, cast(count(distinct case when dl_proposal.pkdeallogid is not null then dfd.deal_id end) as real) unique_visits_w_proposal
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date then dfd.deal_id end) as int) unique_visits_delivered
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, unique_visits_delivered - (total_half_deals / 2) total_delivered_units
from deallog_checkin dl_checkin
  inner join {{ ref('stg_rpt_deal') }} dfd on dfd.deal_id = dl_checkin.fkdealid
  left join deallog_demo dl_demo on dfd.deal_id = dl_demo.fkdealid
  left join deallog_to dl_to on dfd.deal_id = dl_to.fkdealid
  left join deallog_proposal dl_proposal on dfd.deal_id = dl_proposal.fkdealid
  left join {{ ref('dw_fact_customer_communication') }} dfcc on dfd.deal_id = dfcc.deal_id
  inner join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
group by dfd.deal_id
  , dfd.store_id
  , dds.store_name
  , ddu.user_team_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , trunc(dfd.visit_deal_date)
),
standard_visit_store AS
(select
  'Store' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, 0 user_id
, null user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, null team_name
, null user_type
, trunc(dfd.visit_deal_date) calendar_date
, null user_key
, cast(count(distinct dl_checkin.pkdeallogid) as real) total_visits
, cast(count(distinct dfd.deal_id) as real) unique_visits
, cast(count(distinct case when trunc(dfd.first_checkin_date) = trunc(dl_checkin.checkin_date) and dfd.last_checkin_date is null then dfd.deal_id end) as real) initial_visits
, null beback_visits--, cast(count(distinct case when trunc(dfd.first_checkin_date) <> trunc(dl.datecreated) or count(*) > 1 end) as real) beback_visits -- todo
, cast(count(distinct case when dfd.source = 'Showroom' then dfd.deal_id end) as real) unique_visits_showroom
, cast(count(distinct case when dfd.source = 'Phone' then dfd.deal_id end) as real) unique_visits_phone
, cast(count(distinct case when dfd.source = 'Internet' then dfd.deal_id end) as real) unique_visits_internet
, cast(count(distinct case when dfd.source = 'Campaign' then dfd.deal_id end) as real) unique_visits_campaign
, cast(count(distinct case when dl_demo.pkdeallogid is not null then dfd.deal_id end) as real) unique_visits_w_demo
, cast(count(distinct case when dl_to.pkdeallogid is not null then dfd.deal_id end) as real) unique_visits_w_to
, cast(count(distinct case when dl_proposal.pkdeallogid is not null then dfd.deal_id end) as real) unique_visits_w_proposal
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date then dfd.deal_id end) as int) unique_visits_delivered
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, unique_visits_delivered - (total_half_deals / 2) total_delivered_units
from deallog_checkin dl_checkin
  inner join {{ ref('stg_rpt_deal') }} dfd on dfd.deal_id = dl_checkin.fkdealid
  left join deallog_demo dl_demo on dfd.deal_id = dl_demo.fkdealid
  left join deallog_to dl_to on dfd.deal_id = dl_to.fkdealid
  left join deallog_proposal dl_proposal on dfd.deal_id = dl_proposal.fkdealid
  left join {{ ref('dw_fact_customer_communication') }} dfcc on dfd.deal_id = dfcc.deal_id
  inner join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
group by dfd.deal_id
  , dfd.store_id
  , dds.store_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , trunc(dfd.visit_deal_date)
)

SELECT
  CURRENT_TIMESTAMP AS date_report_ran, 
  *
FROM
  (SELECT * FROM standard_visit_user
    UNION
   SELECT * FROM standard_visit_team
    UNION
   SELECT * FROM standard_visit_store
  ) standard_visits