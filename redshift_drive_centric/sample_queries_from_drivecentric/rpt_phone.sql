{{ config(materialized='table', sort=['store_id','user_id','user_name','source', 'vehicle_make', 'vehicle_model','vehicle_status','user_type'], dist='calendar_date') }}

WITH std_phone_user_rpt AS (
select
  'User' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, ddu.user_id user_id
, nvl(ddu.first_name || ' ' || ddu.last_name,'Unknown_' || ddu.user_id) user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, null team_name
, nvl(ddu.user_type,'None') user_type
, trunc(dfd.date_created) calendar_date
, user_name || '|' || ddu.user_id user_key
, cast(count(distinct dfd.deal_id) as int) total_deals
, cast(count(distinct case when customer_create_date_first_email is null or trunc(dfd.customer_create_date_first_email) > trunc(dfd.date_created)  then dfd.deal_id end) as real) deals_created_without_email
, cast(count(distinct case when trunc(dfd.customer_create_date_first_email) > trunc(dfd.date_created) then dfd.deal_id end) as real) email_added_after_creation
, cast(count(distinct case when customer_create_date_first_phone is null or trunc(dfd.customer_create_date_first_phone) > trunc(dfd.date_created) then dfd.deal_id end) as real) deals_created_without_phone
, cast(count(distinct case when trunc(dfd.customer_create_date_first_phone) > trunc(dfd.date_created) then dfd.deal_id end) as real) phone_added_after_creation
, avg (case when response_time_secs < 3600  and is_business_hour_communication = 'Y' and is_from_customer = 1 then response_time_secs end) user_avg_response_time
, cast(count(distinct case when response_gt_than_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1 and ddu.user_type <> 'Genius/Caddy' then dfcc.communication_id  end) as int) fumbled_over_1_hour
, cast(count(distinct case when response_time_secs is null and is_business_hour_communication = 'Y' and is_from_customer = 1 and ddu.user_type <> 'Genius/Caddy'  then dfcc.communication_id end) as int) ignored_communication
, cast(count(distinct case when dfsa.created_by_user_id not in (select user_id from {{ ref('stg_rpt_user') }} ddu where user_type = 'Genius/Caddy') then dfsa.deal_id end) as int)    total_appointments
, cast(count(distinct case when dfsa.is_confirmed = 1 then dfsa.deal_id end) as int)    confirmed_appointments
, cast(count(distinct case when dfsa.is_completed = 1 and dfsa.was_attended_by_customer = 1 then dfsa.deal_id end) as int)    appointment_showed
, cast(count(distinct case when visit_deal_date >= dfd.date_created then dfd.deal_id end) as real) total_visits
, cast(count(distinct case when proposal_date >= dfd.date_created then dfd.deal_id end) as real) total_proposals
, cast(count(distinct case when delivered_date >= dfd.date_created then dfd.deal_id end) as real) total_delivered
, cast(count(distinct case when delivered_date >= dfd.date_created AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, total_delivered - (total_half_deals / 2) total_delivered_units
, cast(count(distinct case when dead_deal_date >= dfd.date_created then dfd.deal_id end) as real) total_dead
, cast(avg(case when dfd.dead_deal_date >= dfd.date_created then datediff(day,dfd.date_created, dead_deal_date) end ) as int) avg_dead_age
, cast(count(distinct case when dfcc.communication_date BETWEEN dfd.date_created and (dfd.date_created + 1) and dfcc.hasvideo = 1 then dfd.deal_id end) as int) video_in_24hrs
from {{ ref('stg_rpt_deal') }}  dfd
  left join {{ ref('dw_fact_customer_communication') }} dfcc on dfd.deal_id = dfcc.deal_id
  left join {{ ref('stg_rpt_appt') }} dfsa on dfd.deal_id = dfsa.deal_id and dfd.date_created < dfsa.date_created
  join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
where source = 'Phone'
group by
    dfd.store_id
  , dds.store_name
  , ddu.user_id
  , user_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , ddu.user_type
  , trunc(dfd.date_created)
  ),
std_phone_team_rpt AS (
select
  'Team' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, 0 user_id
, null user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, nvl('Team '||ddu.user_team_name, 'No Team') team_name
, null user_type
, trunc(dfd.date_created) calendar_date
, null user_key
, cast(count(distinct dfd.deal_id) as int) total_deals
, cast(count(distinct case when customer_create_date_first_email is null or trunc(dfd.customer_create_date_first_email) > trunc(dfd.date_created)  then dfd.deal_id end) as real) deals_created_without_email
, cast(count(distinct case when trunc(dfd.customer_create_date_first_email) > trunc(dfd.date_created) then dfd.deal_id end) as real) email_added_after_creation
, cast(count(distinct case when customer_create_date_first_phone is null or trunc(dfd.customer_create_date_first_phone) > trunc(dfd.date_created) then dfd.deal_id end) as real) deals_created_without_phone
, cast(count(distinct case when trunc(dfd.customer_create_date_first_phone) > trunc(dfd.date_created) then dfd.deal_id end) as real) phone_added_after_creation
, avg (case when response_time_secs < 3600  and is_business_hour_communication = 'Y' and is_from_customer = 1 then response_time_secs end) user_avg_response_time
, cast(count(distinct case when response_gt_than_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1 and ddu.user_type <> 'Genius/Caddy' then dfcc.communication_id  end) as int) fumbled_over_1_hour
, cast(count(distinct case when response_time_secs is null and is_business_hour_communication = 'Y' and is_from_customer = 1 and ddu.user_type <> 'Genius/Caddy'  then dfcc.communication_id end) as int) ignored_communication
, cast(count(distinct case when dfsa.created_by_user_id not in (select user_id from {{ ref('stg_rpt_user') }} ddu where user_type = 'Genius/Caddy') then dfsa.deal_id end) as int)    total_appointments
, cast(count(distinct case when dfsa.is_confirmed = 1 then dfsa.deal_id end) as int)    confirmed_appointments
, cast(count(distinct case when dfsa.is_completed = 1 and dfsa.was_attended_by_customer = 1 then dfsa.deal_id end) as int)    appointment_showed
, cast(count(distinct case when visit_deal_date >= dfd.date_created then dfd.deal_id end) as real) total_visits
, cast(count(distinct case when proposal_date >= dfd.date_created then dfd.deal_id end) as real) total_proposals
, cast(count(distinct case when delivered_date >= dfd.date_created then dfd.deal_id end) as real) total_delivered
, cast(count(distinct case when delivered_date >= dfd.date_created AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, total_delivered - (total_half_deals / 2) total_delivered_units
, cast(count(distinct case when dead_deal_date >= dfd.date_created then dfd.deal_id end) as real) total_dead
, cast(avg(case when dfd.dead_deal_date >= dfd.date_created then datediff(day,dfd.date_created, dead_deal_date) end ) as int) avg_dead_age
, cast(count(distinct case when dfcc.communication_date BETWEEN dfd.date_created and (dfd.date_created + 1) and dfcc.hasvideo = 1 then dfd.deal_id end) as int) video_in_24hrs
from {{ ref('stg_rpt_deal') }}  dfd
  left join {{ ref('dw_fact_customer_communication') }} dfcc on dfd.deal_id = dfcc.deal_id
  left join {{ ref('stg_rpt_appt') }} dfsa on dfd.deal_id = dfsa.deal_id and dfd.date_created < dfsa.date_created
  join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
where source = 'Phone'
group by
    dfd.store_id
  , dds.store_name
  , ddu.user_team_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , trunc(dfd.date_created)
  ),
std_phone_store_rpt AS (
select
  'Store' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, 0 user_id
, null user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, null team_name
, null user_type
, trunc(dfd.date_created) calendar_date
, null user_key
, cast(count(distinct dfd.deal_id) as int) total_deals
, cast(count(distinct case when customer_create_date_first_email is null or trunc(dfd.customer_create_date_first_email) > trunc(dfd.date_created)  then dfd.deal_id end) as real) deals_created_without_email
, cast(count(distinct case when trunc(dfd.customer_create_date_first_email) > trunc(dfd.date_created) then dfd.deal_id end) as real) email_added_after_creation
, cast(count(distinct case when customer_create_date_first_phone is null or trunc(dfd.customer_create_date_first_phone) > trunc(dfd.date_created) then dfd.deal_id end) as real) deals_created_without_phone
, cast(count(distinct case when trunc(dfd.customer_create_date_first_phone) > trunc(dfd.date_created) then dfd.deal_id end) as real) phone_added_after_creation
, avg (case when response_time_secs < 3600  and is_business_hour_communication = 'Y' and is_from_customer = 1 then response_time_secs end) user_avg_response_time
, cast(count(distinct case when response_gt_than_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1 and ddu.user_type <> 'Genius/Caddy' then dfcc.communication_id  end) as int) fumbled_over_1_hour
, cast(count(distinct case when response_time_secs is null and is_business_hour_communication = 'Y' and is_from_customer = 1 and ddu.user_type <> 'Genius/Caddy'  then dfcc.communication_id end) as int) ignored_communication
, cast(count(distinct case when dfsa.created_by_user_id not in (select user_id from {{ ref('stg_rpt_user') }} ddu where user_type = 'Genius/Caddy') then dfsa.deal_id end) as int) total_appointments
, cast(count(distinct case when dfsa.is_confirmed = 1 then dfsa.deal_id end) as int) confirmed_appointments
, cast(count(distinct case when dfsa.is_completed = 1 and dfsa.was_attended_by_customer = 1 then dfsa.deal_id end) as int) appointment_showed
, cast(count(distinct case when visit_deal_date >= dfd.date_created then dfd.deal_id end) as real) total_visits
, cast(count(distinct case when proposal_date >= dfd.date_created then dfd.deal_id end) as real) total_proposals
, cast(count(distinct case when delivered_date >= dfd.date_created then dfd.deal_id end) as real) total_delivered
, cast(count(distinct case when delivered_date >= dfd.date_created AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, total_delivered - (total_half_deals / 2) total_delivered_units
, cast(count(distinct case when dead_deal_date >= dfd.date_created then dfd.deal_id end) as real) total_dead
, cast(avg(case when dfd.dead_deal_date >= dfd.date_created then datediff(day,dfd.date_created, dead_deal_date) end ) as int) avg_dead_age
, cast(count(distinct case when dfcc.communication_date BETWEEN dfd.date_created and (dfd.date_created + 1) and dfcc.hasvideo = 1 then dfd.deal_id end) as int) video_in_24hrs
from {{ ref('stg_rpt_deal') }}  dfd
  left join {{ ref('dw_fact_customer_communication') }} dfcc on dfd.deal_id = dfcc.deal_id
  left join {{ ref('stg_rpt_appt') }} dfsa on dfd.deal_id = dfsa.deal_id and dfd.date_created < dfsa.date_created
  join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
where source = 'Phone'
group by
    dfd.store_id
  , dds.store_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , trunc(dfd.date_created)
  )  
SELECT
  CURRENT_TIMESTAMP AS date_report_ran, 
  *
FROM
  (
    SELECT * FROM std_phone_user_rpt   
    UNION
    SELECT * FROM std_phone_team_rpt   
    UNION
    SELECT * FROM std_phone_store_rpt   
  ) stdPhone