{{ config(materialized='table', sort=['store_id','user_id','user_name','source', 'vehicle_make', 'vehicle_model','vehicle_status','user_type'], dist='calendar_date') }}

with std_lead_user as (
 select
  'User' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, ddu.user_id user_id
, nvl(ddu.first_name || ' ' || ddu.last_name,'Unknown_' || ddu.user_id) user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, null team_name
, nvl(ddu.user_type,'None') user_type
, trunc(dfd.date_created) calendar_date
, user_name || '|' || ddu.user_id user_key
, cast(count (distinct dfd.deal_id ) as int) total_leads
, cast(count (distinct case when dfd.is_bad_deal = 1 then dfd.deal_id end) as int ) bad_leads
, cast(count (distinct case when dfd.is_bad_deal = 0 then dfd.deal_id end) as int ) net_leads
, cast(count (distinct case when dfd.is_bad_deal = 0 and  is_deal_derived_business_hour = 'Y' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then dfd.deal_id end) as int) net_responded_bh_leads
--  This has risk of "lowering" a respoinse time artificially if a user has the exact same seconds response time on the same day.
--  The Distinct is done because of the left join on customer communication (4 emails would sum the same value 4 times and have an inflated response time)
--  This query would need to be reworked completely and this was a simple fix with unlikely edge cases to make sure we can add up all their response times 
--  and divide it down below.
, sum(distinct case when is_bad_deal = 0 and is_deal_derived_business_hour = 'Y' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then datediff(secs, deal_derived_date, dfd.user_response_date) end) bh_sum_response_time
, avg(case when is_bad_deal = 0 and is_deal_derived_business_hour = 'N' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then datediff(secs, deal_derived_date, dfd.user_response_date) end) ah_avg_response_time
, avg(case when is_bad_deal = 0 and is_deal_derived_business_hour = 'Y' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then datediff(secs, deal_derived_date, dfd.user_response_date) end) bh_avg_response_time
, cast(count(distinct case when dfd.user_response_date is null or datediff(secs, deal_derived_date, dfd.user_response_date) > 3600 then dfd.deal_id end ) as int) bh_fumbled_over_1_hour
, cast(count(distinct dfsa.deal_id) as int) set_appointments
, cast(count(distinct case when dfsa.created_by_user_id in (select distinct user_id from  {{ ref('stg_rpt_user') }} where trim(user_type) = 'Genius/Caddy') then dfsa.deal_id end) as int) genius_set_appointments
, cast(count(distinct case when is_confirmed = 1 then dfsa.deal_id end) as int) confirmed_appointment
, cast(count(distinct case when is_completed = 1 and dfsa.was_attended_by_customer = 1 then dfsa.deal_id end) as int) showed_appointment
, cast(count(distinct case when visit_deal_date is not null and is_bad_deal = 0 then dfd.deal_id end) as int) total_visits
, cast(count(distinct case when proposal_date is not null and is_bad_deal = 0 then dfd.deal_id end) as int) total_proposals
, cast(count(distinct case when sold_date is not null and is_bad_deal = 0  then dfd.deal_id end) as int) total_sold
, cast(count(distinct case when delivered_date is not null and is_bad_deal = 0 then dfd.deal_id end) as int) total_delivered
, cast(count(distinct case when delivered_date is not null and is_bad_deal = 0 AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, total_delivered - (total_half_deals / 2) total_delivered_units
, cast(count(distinct case when dead_deal_date is not null and is_bad_deal = 0  then dfd.deal_id end) as int) total_dead
, avg(case when dead_deal_date > dfd.date_created and is_bad_deal = 0 then datediff(day,dfd.date_created,dead_deal_date) end) avg_dead_age --In Days
, cast(sum(distinct case when dead_deal_date > dfd.date_created and is_bad_deal = 0 then datediff(day,dfd.date_created,dead_deal_date) + (dfd.deal_id * .000000001) end) as int) sum_days_to_dead --In Days
, cast(sum(case when is_from_customer = 1 and is_business_hour_communication = 'Y' and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) total_incoming_msgs_bh
, cast(sum(case when response_less_than_60m = 1 and is_from_customer = 1 and is_business_hour_communication = 'Y' and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) total_incoming_msgs_bh_responded
, cast(sum(case when response_less_than_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then response_time_secs end ) as int) sum_incoming_response_time_bh
, nvl(avg (case when response_time_secs < 3600 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then response_time_secs end ),0) avg_incoming_response_time_bh
, cast(sum(case when response_less_than_15m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) response_less_than_15_mins_bh
, cast(sum(case when response_15m_to_30m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) response_15m_to_30m_bh
, cast(sum(case when response_30m_to_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) response_30m_to_60m_bh
, cast(sum(case when response_gt_than_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) fumbled_over_1_hour_bh
, cast(sum(case when response_time_secs is null and is_business_hour_communication = 'Y' and is_from_customer = 1 and ddu.user_type != 'Genius/Caddy' then  1 else 0 end) as int) ignored_incoming_msgs_bh
, cast(count(distinct case when dfcc.communication_date BETWEEN dfd.date_created and (dfd.date_created + 1) and dfcc.hasvideo = 1 then dfd.deal_id end) as int) video_in_24hrs
from  {{ ref('stg_rpt_deal') }}  dfd
  left join   {{ ref('dw_fact_customer_communication') }}  dfcc on dfd.deal_id = dfcc.deal_id and dfcc.communication_date > dfd.date_created
  left join   {{ ref('stg_rpt_appt') }} dfsa on dfd.deal_id = dfsa.deal_id and dfsa.date_created > dfd.date_created
  join   {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
where source = 'Internet'
group by
    dfd.store_id
  , dds.store_name
  , ddu.user_id
  , user_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , ddu.user_type
  , trunc(dfd.date_created)
),
std_lead_team as
 (
 select
  'Team' entity
  , dfd.store_id
  , nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
  , 0 user_id
  , null user_name
  , dfd.source
  , dfd.vehicle_new_used_type vehicle_status
  , dfd.vehicle_make vehicle_make
  , dfd.vehicle_model vehicle_model
  , nvl('Team '||ddu.user_team_name, 'No Team') team_name
  , null user_type
  , trunc(dfd.date_created) calendar_date
  , null user_key
, cast(count (distinct dfd.deal_id ) as int) total_leads
, cast(count (distinct case when dfd.is_bad_deal = 1 then dfd.deal_id end) as int ) bad_leads
, cast(count (distinct case when dfd.is_bad_deal = 0 then dfd.deal_id end) as int ) net_leads
, cast(count (distinct case when dfd.is_bad_deal = 0 and  is_deal_derived_business_hour = 'Y' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then dfd.deal_id end) as int) net_responded_bh_leads
--  This has risk of "lowering" a respoinse time artificially if a user has the exact same seconds response time on the same day.
--  The Distinct is done because of the left join on customer communication (4 emails would sum the same value 4 times and have an inflated response time)
--  This query would need to be reworked completely and this was a simple fix with unlikely edge cases to make sure we can add up all their response times 
--  and divide it down below.
, sum(distinct case when is_bad_deal = 0 and is_deal_derived_business_hour = 'Y' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then datediff(secs, deal_derived_date, dfd.user_response_date) end) bh_sum_response_time
, avg(case when is_bad_deal = 0 and is_deal_derived_business_hour = 'N' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then datediff(secs, deal_derived_date, dfd.user_response_date) end) ah_avg_response_time
, avg(case when is_bad_deal = 0 and is_deal_derived_business_hour = 'Y' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then datediff(secs, deal_derived_date, dfd.user_response_date) end) bh_avg_response_time
, cast(count(distinct case when dfd.user_response_date is null or datediff(secs, deal_derived_date, dfd.user_response_date) > 3600 then dfd.deal_id end ) as int) bh_fumbled_over_1_hour
, cast(count(distinct dfsa.deal_id) as int) set_appointments
, cast(count(distinct case when dfsa.created_by_user_id in (select distinct user_id from  {{ ref('stg_rpt_user') }} where trim(user_type) = 'Genius/Caddy') then dfsa.deal_id end) as int) genius_set_appointments
, cast(count(distinct case when is_confirmed = 1 then dfsa.deal_id end) as int) confirmed_appointment
, cast(count(distinct case when is_completed = 1 and dfsa.was_attended_by_customer = 1 then dfsa.deal_id end) as int) showed_appointment
, cast(count(distinct case when visit_deal_date is not null and is_bad_deal = 0 then dfd.deal_id end) as int) total_visits
, cast(count(distinct case when proposal_date is not null and is_bad_deal = 0 then dfd.deal_id end) as int) total_proposals
, cast(count(distinct case when sold_date is not null and is_bad_deal = 0  then dfd.deal_id end) as int) total_sold
, cast(count(distinct case when delivered_date is not null and is_bad_deal = 0 then dfd.deal_id end) as int) total_delivered
, cast(count(distinct case when delivered_date is not null and is_bad_deal = 0 AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, total_delivered - (total_half_deals / 2) total_delivered_units
, cast(count(distinct case when dead_deal_date is not null and is_bad_deal = 0  then dfd.deal_id end) as int) total_dead
, avg(case when dead_deal_date > dfd.date_created and is_bad_deal = 0 then datediff(day,dfd.date_created,dead_deal_date) end) avg_dead_age --In Days
, cast(sum(distinct case when dead_deal_date > dfd.date_created and is_bad_deal = 0 then datediff(day,dfd.date_created,dead_deal_date) + (dfd.deal_id * .000000001) end) as int) sum_days_to_dead --In Days
, cast(sum(case when is_from_customer = 1 and is_business_hour_communication = 'Y' and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) total_incoming_msgs_bh
, cast(sum(case when response_less_than_60m = 1 and is_from_customer = 1 and is_business_hour_communication = 'Y' and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) total_incoming_msgs_bh_responded
, cast(sum(case when response_less_than_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then response_time_secs end ) as int) sum_incoming_response_time_bh
, nvl(avg (case when response_time_secs < 3600 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then response_time_secs end ),0) avg_incoming_response_time_bh
, cast(sum(case when response_less_than_15m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) response_less_than_15_mins_bh
, cast(sum(case when response_15m_to_30m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) response_15m_to_30m_bh
, cast(sum(case when response_30m_to_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) response_30m_to_60m_bh
, cast(sum(case when response_gt_than_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) fumbled_over_1_hour_bh
, cast(sum(case when response_time_secs is null and is_business_hour_communication = 'Y' and is_from_customer = 1 and ddu.user_type != 'Genius/Caddy' then  1 else 0 end) as int) ignored_incoming_msgs_bh
, cast(count(distinct case when dfcc.communication_date BETWEEN dfd.date_created and (dfd.date_created + 1) and dfcc.hasvideo = 1 then dfd.deal_id end) as int) video_in_24hrs
from  {{ ref('stg_rpt_deal') }}  dfd
left join   {{ ref('dw_fact_customer_communication') }}  dfcc on dfd.deal_id = dfcc.deal_id and dfcc.communication_date > dfd.date_created
left join   {{ ref('stg_rpt_appt') }} dfsa on dfd.deal_id = dfsa.deal_id and dfsa.date_created > dfd.date_created
join   {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
where source = 'Internet'
group by
    dfd.store_id
  , dds.store_name
  , ddu.user_team_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , trunc(dfd.date_created)
),
std_lead_store as
 (
 select 
  'Store' entity
  , dfd.store_id
  , nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
  , 0 user_id
  , null user_name
  , dfd.source
  , dfd.vehicle_new_used_type vehicle_status
  , dfd.vehicle_make vehicle_make
  , dfd.vehicle_model vehicle_model
  , null team_name
  , null user_type
  , trunc(dfd.date_created) calendar_date
  , null user_key
, cast(count (distinct dfd.deal_id ) as int) total_leads
, cast(count (distinct case when dfd.is_bad_deal = 1 then dfd.deal_id end) as int ) bad_leads
, cast(count (distinct case when dfd.is_bad_deal = 0 then dfd.deal_id end) as int ) net_leads
, cast(count (distinct case when dfd.is_bad_deal = 0 and  is_deal_derived_business_hour = 'Y' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then dfd.deal_id end) as int) net_responded_bh_leads
--  This has risk of "lowering" a respoinse time artificially if a user has the exact same seconds response time on the same day.
--  The Distinct is done because of the left join on customer communication (4 emails would sum the same value 4 times and have an inflated response time)
--  This query would need to be reworked completely and this was a simple fix with unlikely edge cases to make sure we can add up all their response times 
--  and divide it down below.
, sum(distinct case when is_bad_deal = 0 and is_deal_derived_business_hour = 'Y' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then datediff(secs, deal_derived_date, dfd.user_response_date) end) bh_sum_response_time
, avg(case when is_bad_deal = 0 and is_deal_derived_business_hour = 'N' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then datediff(secs, deal_derived_date, dfd.user_response_date) end) ah_avg_response_time
, avg(case when is_bad_deal = 0 and is_deal_derived_business_hour = 'Y' and datediff(secs, deal_derived_date, dfd.user_response_date) < 3600 then datediff(secs, deal_derived_date, dfd.user_response_date) end) bh_avg_response_time
, cast(count(distinct case when dfd.user_response_date is null or datediff(secs, deal_derived_date, dfd.user_response_date) > 3600 then dfd.deal_id end ) as int) bh_fumbled_over_1_hour
, cast(count(distinct dfsa.deal_id) as int) set_appointments
, cast(count(distinct case when dfsa.created_by_user_id in (select distinct user_id from  {{ ref('stg_rpt_user') }} where trim(user_type) = 'Genius/Caddy') then dfsa.deal_id end) as int) genius_set_appointments
, cast(count(distinct case when is_confirmed = 1 then dfsa.deal_id end) as int) confirmed_appointment
, cast(count(distinct case when is_completed = 1 and dfsa.was_attended_by_customer = 1 then dfsa.deal_id end) as int) showed_appointment
, cast(count(distinct case when visit_deal_date is not null and is_bad_deal = 0 then dfd.deal_id end) as int) total_visits
, cast(count(distinct case when proposal_date is not null and is_bad_deal = 0 then dfd.deal_id end) as int) total_proposals
, cast(count(distinct case when sold_date is not null and is_bad_deal = 0  then dfd.deal_id end) as int) total_sold
, cast(count(distinct case when delivered_date is not null and is_bad_deal = 0 then dfd.deal_id end) as int) total_delivered
, cast(count(distinct case when delivered_date is not null and is_bad_deal = 0 AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, total_delivered - (total_half_deals / 2) total_delivered_units
, cast(count(distinct case when dead_deal_date is not null and is_bad_deal = 0  then dfd.deal_id end) as int) total_dead
, avg(case when dead_deal_date > dfd.date_created and is_bad_deal = 0 then datediff(day,dfd.date_created,dead_deal_date) end) avg_dead_age --In Days
, cast(sum(distinct case when dead_deal_date > dfd.date_created and is_bad_deal = 0 then datediff(day,dfd.date_created,dead_deal_date) + (dfd.deal_id * .000000001) end) as int) sum_days_to_dead --In Days
, cast(sum(case when is_from_customer = 1 and is_business_hour_communication = 'Y' and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) total_incoming_msgs_bh
, cast(sum(case when response_less_than_60m = 1 and is_from_customer = 1 and is_business_hour_communication = 'Y' and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) total_incoming_msgs_bh_responded
, cast(sum(case when response_less_than_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then response_time_secs end ) as int) sum_incoming_response_time_bh
, nvl(avg (case when response_time_secs < 3600 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then response_time_secs end ),0) avg_incoming_response_time_bh
, cast(sum(case when response_less_than_15m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) response_less_than_15_mins_bh
, cast(sum(case when response_15m_to_30m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) response_15m_to_30m_bh
, cast(sum(case when response_30m_to_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) response_30m_to_60m_bh
, cast(sum(case when response_gt_than_60m = 1 and is_business_hour_communication = 'Y' and is_from_customer = 1  and ddu.user_type != 'Genius/Caddy' then 1 else 0 end) as int) fumbled_over_1_hour_bh
, cast(sum(case when response_time_secs is null and is_business_hour_communication = 'Y' and is_from_customer = 1 and ddu.user_type != 'Genius/Caddy' then  1 else 0 end) as int) ignored_incoming_msgs_bh
, cast(count(distinct case when dfcc.communication_date BETWEEN dfd.date_created and (dfd.date_created + 1) and dfcc.hasvideo = 1 then dfd.deal_id end) as int) video_in_24hrs
from  {{ ref('stg_rpt_deal') }}  dfd
  left join   {{ ref('dw_fact_customer_communication') }}  dfcc on dfd.deal_id = dfcc.deal_id and dfcc.communication_date > dfd.date_created
  left join   {{ ref('stg_rpt_appt') }} dfsa on dfd.deal_id = dfsa.deal_id and dfsa.date_created > dfd.date_created
  join   {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
where source = 'Internet'
group by
    dfd.store_id
  , dds.store_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , trunc(dfd.date_created)
)

select   
  CURRENT_TIMESTAMP AS date_report_ran, 
  *
FROM
   (SELECT * fROM std_lead_user
     UNION
    SELECT * FROM std_lead_team
     UNION
    SELECT * fROM std_lead_store
   )  std_lead_rpt
