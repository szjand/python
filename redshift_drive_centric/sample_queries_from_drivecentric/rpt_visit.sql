{{ config(materialized='table', sort=['store_id','user_id','user_name','source', 'vehicle_make', 'vehicle_model','vehicle_status','user_type'], dist='calendar_date') }}

WITH visit_user AS
(select
  'User' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, ddu.user_id user_id
, nvl(ddu.first_name || ' ' || ddu.last_name,'Unknown_' || ddu.user_id) user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, null team_name
, nvl(ddu.user_type,'None') user_type
, trunc(dfd.visit_deal_date) calendar_date
, user_name || '|' || ddu.user_id user_key
, cast(count(distinct dfd.deal_id) as real) total_visits
, cast(count(distinct case when proposal_date is not null then dfd.deal_id end) as real) total_proposals
, cast(count(distinct case when customer_create_date_first_email is null or trunc(customer_create_date_first_email) > trunc(visit_deal_date)  then dfd.deal_id end) as real)   eligible_email_opportunity --5.a
, cast(count(distinct case when trunc(customer_create_date_first_email) = trunc(visit_deal_date)  then dfd.deal_id end) as real)  visit_email_capture_count --5.b
, cast(count(distinct case when customer_create_date_first_phone is null or trunc(customer_create_date_first_phone) > trunc(visit_deal_date)   then dfd.deal_id end) as real)   eligible_mobile_opportunity --5.c
, cast(count(distinct case when trunc(customer_create_date_first_phone) = trunc(visit_deal_date) then dfd.deal_id end) as real)  visit_mobile_capture_count --5.d
, cast(count(distinct case when dfd.total_checkins > 1 then dfd.deal_id end) as real) unique_bebacks
, cast(count(distinct case when dfd.sold_date > dfd.visit_deal_date then dfd.deal_id end) as real) total_sold
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date then dfd.deal_id end) as real) total_delivered
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, total_delivered - (total_half_deals / 2) total_delivered_units
, cast(total_visits - total_sold as real) non_sold
, cast(count( distinct case when  dfd.dead_deal_date > dfd.visit_deal_date then dfd.deal_id end) as real) total_dead
, avg(case when dead_deal_date > dfd.date_created then datediff(day,dfd.date_created,dead_deal_date) end) avg_dead_Age --In Days
, cast(count(distinct case when dfcc.communication_date BETWEEN dfd.date_created and (dfd.date_created + 1) and dfcc.hasvideo = 1 then dfd.deal_id end) as int) video_in_24hrs
from {{ ref('stg_rpt_deal') }} dfd
  left join   {{ ref('dw_fact_customer_communication') }}  dfcc on dfd.deal_id = dfcc.deal_id and dfcc.communication_date > dfd.date_created
  join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
where dfd.visit_deal_date is not null and dfd.source = 'Showroom' and dfd.is_bad_deal = 0
group by
    dfd.store_id
  , dds.store_name
  , ddu.user_id
  , user_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , ddu.user_type
  , trunc(dfd.visit_deal_date)
),
visit_team AS
(select
  'Team' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, 0 user_id
, null user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, nvl('Team '||ddu.user_team_name, 'No Team') team_name
, null user_type
, trunc(dfd.visit_deal_date) calendar_date
, null user_key
, cast(count(distinct dfd.deal_id) as real) total_visits
, cast(count(distinct case when proposal_date is not null then dfd.deal_id end) as real) total_proposals
, cast(count( distinct case when customer_create_date_first_email is null or trunc(customer_create_date_first_email) > trunc(visit_deal_date)  then dfd.deal_id end) as real)   eligible_email_opportunity --5.a
, cast(count( distinct case when trunc(customer_create_date_first_email) = trunc(visit_deal_date)  then dfd.deal_id end) as real)  visit_email_capture_count --5.b
, cast(count( distinct case when customer_create_date_first_phone is null or trunc(customer_create_date_first_phone) > trunc(visit_deal_date)   then dfd.deal_id end) as real)   eligible_mobile_opportunity --5.c
, cast(count( distinct case when trunc(customer_create_date_first_phone) = trunc(visit_deal_date) then dfd.deal_id end) as real)  visit_mobile_capture_count --5.d
, cast(count( distinct  case when  dfd.total_checkins > 1 then dfd.deal_id end) as real) unique_bebacks
, cast(count( distinct  case when  dfd.sold_date > dfd.visit_deal_date then dfd.deal_id end) as real) total_sold
, cast(count( distinct  case when  dfd.delivered_date > dfd.visit_deal_date then dfd.deal_id end) as real) total_delivered
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, total_delivered - (total_half_deals / 2) total_delivered_units
, cast(total_visits - total_sold as real) non_sold
, cast(count( distinct case when  dfd.dead_deal_date > dfd.visit_deal_date then dfd.deal_id end) as real) total_dead
, avg(case when dead_deal_date > dfd.date_created then datediff(day,dfd.date_created,dead_deal_date) end) avg_dead_Age --In Days
, cast(count(distinct case when dfcc.communication_date BETWEEN dfd.date_created and (dfd.date_created + 1) and dfcc.hasvideo = 1 then dfd.deal_id end) as int) video_in_24hrs
from {{ ref('stg_rpt_deal') }} dfd
  left join   {{ ref('dw_fact_customer_communication') }}  dfcc on dfd.deal_id = dfcc.deal_id and dfcc.communication_date > dfd.date_created
  join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
where dfd.visit_deal_date is not null and dfd.source = 'Showroom' and dfd.is_bad_deal = 0
group by
    dfd.store_id
  , dds.store_name
  , ddu.user_team_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , trunc(dfd.visit_deal_date)
),
visit_store AS
(select
  'Store' entity
, dfd.store_id
, nvl(dds.store_name, 'Unknown_' || dfd.store_id) store_name
, 0 user_id
, null user_name
, dfd.source
, dfd.vehicle_new_used_type vehicle_status
, dfd.vehicle_make vehicle_make
, dfd.vehicle_model vehicle_model
, null team_name
, null user_type
, trunc(dfd.visit_deal_date) calendar_date
, null user_key
, cast(count(distinct dfd.deal_id) as real) total_visits
, cast(count(distinct case when proposal_date is not null then dfd.deal_id end) as real) total_proposals
, cast(count( distinct case when customer_create_date_first_email is null or trunc(customer_create_date_first_email) > trunc(visit_deal_date)  then dfd.deal_id end) as real)   eligible_email_opportunity --5.a
, cast(count( distinct case when trunc(customer_create_date_first_email) = trunc(visit_deal_date)  then dfd.deal_id end) as real)  visit_email_capture_count --5.b
, cast(count( distinct case when customer_create_date_first_phone is null or trunc(customer_create_date_first_phone) > trunc(visit_deal_date)   then dfd.deal_id end) as real)   eligible_mobile_opportunity --5.c
, cast(count( distinct case when trunc(customer_create_date_first_phone) = trunc(visit_deal_date) then dfd.deal_id end) as real)  visit_mobile_capture_count --5.d
, cast(count( distinct  case when  dfd.total_checkins > 1 then dfd.deal_id end) as real) unique_bebacks
, cast(count( distinct  case when  dfd.sold_date > dfd.visit_deal_date then dfd.deal_id end) as real) total_sold
, cast(count( distinct  case when  dfd.delivered_date > dfd.visit_deal_date then dfd.deal_id end) as real) total_delivered
, cast(count(distinct case when dfd.delivered_date > dfd.visit_deal_date AND dfd.sales_1_user_id <> dfd.sales_2_user_id AND dfd.ilm_user_id <> ddu.user_id then dfd.deal_id end) as int) total_half_deals
, total_delivered - (total_half_deals / 2) total_delivered_units
, cast(total_visits - total_sold as real) non_sold
, cast(count( distinct case when  dfd.dead_deal_date > dfd.visit_deal_date then dfd.deal_id end) as real) total_dead
, avg(case when dead_deal_date > dfd.date_created then datediff(day,dfd.date_created,dead_deal_date) end) avg_dead_Age --In Days
, cast(count(distinct case when dfcc.communication_date BETWEEN dfd.date_created and (dfd.date_created + 1) and dfcc.hasvideo = 1 then dfd.deal_id end) as int) video_in_24hrs
from {{ ref('stg_rpt_deal') }} dfd
  left join   {{ ref('dw_fact_customer_communication') }}  dfcc on dfd.deal_id = dfcc.deal_id and dfcc.communication_date > dfd.date_created
  join {{ ref('dw_dim_store') }} dds on dfd.store_id = dds.store_id
  left join {{ ref('stg_rpt_user') }} ddu on ddu.user_id in (dfd.ilm_user_id, dfd.sales_1_user_id, dfd.sales_2_user_id)
where dfd.visit_deal_date is not null and dfd.source = 'Showroom' and dfd.is_bad_deal = 0
group by
    dfd.store_id
  , dds.store_name
  , dfd.source
  , vehicle_new_used_type
  , vehicle_make
  , vehicle_model
  , trunc(dfd.visit_deal_date)
)

SELECT
  CURRENT_TIMESTAMP AS date_report_ran, 
  *
FROM
  (SELECT * FROM visit_user
    UNION
   SELECT * FROM  visit_team
    UNION
   SELECT * FROM  visit_store
  ) vsts
