﻿struggled with no mix and matching of leader node and compute node while trying to generate ddl
finally ended up with generating a csv file of insert statements

-- generate the insert statements as text
-- run this in dBeaver
select 'insert into dc.redshift_metadata values(' ||''''||table_name||''''||','||''''||column_name||''''||','||''''||data_type||''''||','||ordinal_position||');'
from information_schema.columns
where table_schema = 'rydell'
  and (
    (table_name like 'rpt_%')
    or 
    (table_name like 'dw_%'))
order by table_name, ordinal_position


