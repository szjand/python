﻿/*
modified version of python projects/ext_arkona/sql/arkona_metadata.sql
all the metadata has been loaded into the pg table dc.redshift_metadata
this script generates the ddl for a postgresql table, one table at a time
*/
do
$$
declare 
  _row RECORD;
  _last_field INTEGER;
  _schema_prefix citext;
  _str citext;
  _table_name citext;
begin
  drop table if exists _ddl ;
  _schema_prefix := 'dc.ext_';  ---------------------------------------------------------------------
  _table_name = 'dw_fact_text_email_communication'; ---------------------------------------------------------------------------
  _last_field = (
    SELECT max(ordinal_position)
    FROM dc.redshift_metadata
    WHERE lower(table_name) = lower(_table_name));
  _str = 'DROP TABLE IF EXISTS ' 
    || _schema_prefix     
    || _table_name 
    || ';' || CHR(10);    
  _str = _str ||'CREATE TABLE IF NOT EXISTS ' 
    || _schema_prefix  
    || _table_name 
    || '(' || CHR(10);
  FOR _row in
    SELECT table_name, replace(column_name, '#','_') as column_name,
      ordinal_position, data_type -- , length, numeric_scale
    FROM dc.redshift_metadata a 
    WHERE lower(table_name) = _table_name -- lower(_table_name) 
    ORDER BY ordinal_position
  LOOP
    CASE 
      WHEN lower(_row.data_type) IN ('character varying') THEN _str = _str 
        || '    ' || _row.column_name 
        || ' CITEXT';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;           
      WHEN lower(_row.data_type) = 'bigint' THEN _str = _str 
        || '    ' || _row.column_name 
        || ' BIGINT';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;  
      WHEN lower(_row.data_type) in ('double precision','real','numeric') THEN _str = _str 
        || '    ' || _row.column_name 
        || ' NUMERIC';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;                     
      WHEN lower(_row.data_type) in ('integer','smallint','bigint') THEN _str = _str 
        || '    ' || _row.column_name 
        || ' INTEGER';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;                       
      WHEN lower(_row.data_type) = 'date' THEN _str = _str 
        || '    ' || _row.column_name 
        || ' DATE';
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF; 
      WHEN lower(_row.data_type) = 'timestamp with time zone' THEN _str = _str 
        || '    ' || _row.column_name 
        || ' TIMESTAMPTZ'; 
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;        
      WHEN lower(_row.data_type) = 'timestamp without time zone' THEN _str = _str 
        || '    ' || _row.column_name 
        || ' TIMESTAMP'; 
        IF _row.ordinal_position = _last_field THEN
          _str = _str || ')' || CHR(10) || 'WITH (OIDS=FALSE);' || CHR(10);
        ELSE
          _str = _str || ',' || CHR(10);
        END IF;                                         
    END CASE;
  END LOOP;

  -- generates a single string of column names for the select (from db2) statement:
  _str = (
    select _str || '-- ' || array_agg(column_name)::citext
    from (
    select
      case
        when data_type = 'CHAR' then 'TRIM(' || column_name || ')'
        else column_name
      end as column_name
    from dc.redshift_metadata
    where table_name = _table_name
    order by ordinal_position) x);    

  create temp table _ddl as select _str;
end
$$;

select * from _ddl;  