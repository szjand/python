﻿going to need a natural key for all these tables to do updates

should the hash exclude from, thru & hash  YES

dont really need tmp tables, ext -> xfm

ext_dw_dim_customer: customer_id
dc.ext_dw_dim_date: date_day
ext_dw_dim_Store: store_id
ext_dw_dim_user: user_id, user_team_name, -- it is nonsensical, but user_team_name is what distinguishes these 2 dups
-- ext_dw_fact_closed_ro: goofy data, narrow it down to ro_number,customer_id,vehicle_id,vehicle_vin
-- in fact, skip it for now, doubt this is anything of use
ext_dw_fact_customer_communication: communication_id, deal_id, communication_date,prev_interaction_timestamp
ext_dw_fact_deal: deal_id, delivery_total_gross -- 10 fucking deals with multiple gross values
ext_dw_fact_market_scan: deal_id,user_id,store_id,proposal_date
ext_dw_fact_notes: deal_id, date_created, md5(notes)
ext_dw_fact_phone_call: task_id
ext_dw_fact_sales_appointments: appointment_id
ext_dw_fact_service_appointments: appointment_id
ext_dw_fact_store_app: store_app_id
-- at this point, i don't understand what the rpt tables are or are for, initially it makes no sense to version them
-- until i understand a valid use, i am going to leave them out of this exercise

truncate dc.xfm_dw_dim_customer;

ok, heres the process
make sure the ext table has a unique index.
add the PK to xfm table (unique index from ext + row_thru_date)
		ALTER TABLE dc.xfm_table ADD PRIMARY KEY (unique_index,row_thru_date)
add check constraint to xfm table (row_thru_date > row_from_date)
		ALTER TABLE dc.xfm_table add constraint thru_gt_from check (row_thru_date > row_from_date);
drop the tmp table
		drop table dc.tmp_table_name;
create unlogged the changed row table (tmp_tablename_changed_rows) consists of unique index fields
    create unlogged table dc.tmp_tablename_changed_rows (unique_index_fields);

ALTER TABLE dc.xfm_table ADD PRIMARY KEY (unique_index,row_thru_date);
ALTER TABLE dc.xfm_table add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_tablename_changed_rows (unique_index_fields);		


then run the this script ( to be put into a function)
some sort of test in the function, or maybe the python to verify local target and date

-- dw_fact_text_email_communication
-- communication_id


CREATE OR REPLACE FUNCTION dc.xfm_dw_fact_text_email_communication()
  RETURNS void AS
$BODY$  
/*
select dc.xfm_dw_fact_text_email_communication()
*/
	insert into dc.xfm_dw_fact_text_email_communication
	select a.*, current_date, '12/31/9999', md5(a::text)
	from dc.ext_dw_fact_text_email_communication a
	where not exists (
		select 1
		from dc.xfm_dw_fact_text_email_communication
		where communication_id = a.communication_id);

	-- populate dc.tmp_dw_dim_customer_changed_rows
	truncate dc.tmp_dw_fact_text_email_communication_changed_rows;
	insert into dc.tmp_dw_fact_text_email_communication_changed_rows
	select a.communication_id
	from (
		select aa.communication_id,md5(aa::text) as hash
		from dc.ext_dw_fact_text_email_communication aa) a
	join dc.xfm_dw_fact_text_email_communication b on a.communication_id = b.communication_id
		and b.row_thru_date > current_date
		and a.hash <> b.hash;

	-- update the old row
	update dc.xfm_dw_fact_text_email_communication x
	set row_thru_date = current_date - 1
	from dc.tmp_dw_fact_text_email_communication_changed_rows z
	where x.communication_id = z.communication_id
		and x.row_thru_date > current_date;

	-- insert the new changed row
	insert into dc.xfm_dw_fact_text_email_communication
	select a.*, current_date, '12/31/9999', md5(a::text)
	from dc.ext_dw_fact_text_email_communication a
	join dc.tmp_dw_fact_text_email_communication_changed_rows b on a.communication_id = b.communication_id;
$BODY$
LANGUAGE SQL;







