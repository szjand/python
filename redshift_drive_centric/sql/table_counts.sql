﻿select 'select count(*) from dc.'||table_name
from information_schema.tables a
where table_schema = 'dc'
  and table_name like 'ext_%'
  and table_name <> 'ext_localstore'

-- estimated table counts
SELECT schemaname,relname,n_live_tup 
FROM pg_stat_user_tables 
where schemaname = 'dc' 
  and relname like 'ext_dw%'
  and relname <> 'ext_localstore'
ORDER BY relname -- n_live_tup DESC; 

-- accurate table counts
with 
	tbl as (
		SELECT table_schema,table_name 
		FROM information_schema.tables   
		where table_schema = 'dc'
			and table_name like 'ext_%'
			and table_name <> 'ext_localstore')   
select table_schema, table_name, (xpath('/row/c/text()', 
	query_to_xml(format('select count(*) as c from %I.%I', table_schema, table_name), false, true, '')))[1]::text::int as rows_n 
from tbl ORDER BY 3 DESC;

