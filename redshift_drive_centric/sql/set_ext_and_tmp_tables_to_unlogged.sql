﻿04/15/21
cleaning up the ddl yesterday to fix my numeric/integer goof, regenerated ddl
but forgot about the unlogged option, so, this covers that
select * 
from information_schema.tables 
where table_schema = 'dc'
  and (table_name like '%ext_%' or table_name like '%tmp_%%' or table_name like '%xfm_%')
order by table_name 


-- make sure i have all the tables
select * 
from (
select table_name 
from information_schema.tables 
where table_schema = 'dc'
  and table_name like '%ext_%'
  and table_name <> 'ext_localstore') a
left join (
select table_name 
from information_schema.tables 
where table_schema = 'dc'
  and table_name like '%tmp_%') b on substring(a.table_name, 5,35) = substring(b.table_name, 5,35)
left join (
select table_name 
from information_schema.tables 
where table_schema = 'dc'
  and table_name like '%xfm_%') c on substring(a.table_name, 5,35) = substring(c.table_name, 5,35)



-- this generates all the sql statements
select 'alter table dc.' ||table_name||' set unlogged;'
from information_schema.tables 
where table_schema = 'dc'
  and table_name <> 'ext_localstore'
  and (table_name like '%ext_%' or table_name like '%tmp_%%');

alter table dc.ext_rpt_enhanced_lead_engaged set unlogged;
alter table dc.tmp_dw_fact_customer_communication set unlogged;
alter table dc.ext_dw_dim_customer set unlogged;
alter table dc.ext_rpt_dailyactivity set unlogged;
alter table dc.tmp_rpt_dailyactivity set unlogged;
alter table dc.tmp_dw_dim_customer set unlogged;
alter table dc.tmp_dw_dim_date set unlogged;
alter table dc.ext_rpt_enhanced_lead_summary set unlogged;
alter table dc.tmp_rpt_delivery set unlogged;
alter table dc.ext_rpt_enhanced_dailyactivity set unlogged;
alter table dc.tmp_rpt_enhanced_dailyactivity set unlogged;
alter table dc.ext_dw_fact_store_app set unlogged;
alter table dc.ext_rpt_enhanced_response_time set unlogged;
alter table dc.ext_rpt_enhanced_days_to_conversion set unlogged;
alter table dc.ext_rpt_enhanced_visit set unlogged;
alter table dc.tmp_rpt_enhanced_days_to_conversion set unlogged;
alter table dc.ext_rpt_enhanced_visit_delivered set unlogged;
alter table dc.ext_rpt_appointment set unlogged;
alter table dc.ext_rpt_enhanced_days_to_delivered set unlogged;
alter table dc.tmp_rpt_enhanced_days_to_delivered set unlogged;
alter table dc.ext_rpt_enhanced_engaged_visit set unlogged;
alter table dc.ext_rpt_popup set unlogged;
alter table dc.tmp_rpt_enhanced_engaged_visit set unlogged;
alter table dc.ext_rpt_enhanced_phone set unlogged;
alter table dc.tmp_rpt_enhanced_phone set unlogged;
alter table dc.tmp_dw_fact_closed_ro set unlogged;
alter table dc.ext_rpt_enhanced_pipeline_health set unlogged;
alter table dc.ext_rpt_enhanced_after_delivery set unlogged;
alter table dc.tmp_rpt_enhanced_pipeline_health set unlogged;
alter table dc.ext_dw_fact_deal set unlogged;
alter table dc.tmp_dw_fact_deal set unlogged;
alter table dc.tmp_rpt_enhanced_source set unlogged;
alter table dc.ext_rpt_enhanced_source set unlogged;
alter table dc.tmp_dw_fact_phone_call set unlogged;
alter table dc.tmp_rpt_lead set unlogged;
alter table dc.ext_rpt_lead set unlogged;
alter table dc.tmp_rpt_phone set unlogged;
alter table dc.tmp_dw_fact_sales_appointments set unlogged;
alter table dc.tmp_dw_fact_service_appointments set unlogged;
alter table dc.tmp_dw_fact_store_app set unlogged;
alter table dc.ext_rpt_visit set unlogged;
alter table dc.tmp_rpt_visit set unlogged;
alter table dc.tmp_rpt_appointment set unlogged;
alter table dc.ext_rpt_delivery set unlogged;
alter table dc.tmp_rpt_enhanced_response_time set unlogged;
alter table dc.ext_dw_fact_customer_communication set unlogged;
alter table dc.ext_rpt_phone set unlogged;
alter table dc.tmp_rpt_enhanced_after_delivery set unlogged;
alter table dc.ext_dw_dim_date set unlogged;
alter table dc.ext_dw_dim_store set unlogged;
alter table dc.ext_dw_dim_user set unlogged;
alter table dc.tmp_rpt_enhanced_lead_engaged set unlogged;
alter table dc.tmp_rpt_enhanced_lead_summary set unlogged;
alter table dc.ext_dw_fact_closed_ro set unlogged;
alter table dc.tmp_rpt_enhanced_visit set unlogged;
alter table dc.ext_dw_fact_market_scan set unlogged;
alter table dc.ext_dw_fact_notes set unlogged;
alter table dc.tmp_rpt_enhanced_visit_delivered set unlogged;
alter table dc.ext_dw_fact_phone_call set unlogged;
alter table dc.tmp_rpt_popup set unlogged;
alter table dc.ext_dw_fact_sales_appointments set unlogged;
alter table dc.ext_dw_fact_service_appointments set unlogged;
alter table dc.tmp_dw_dim_store set unlogged;
alter table dc.tmp_dw_dim_user set unlogged;
alter table dc.tmp_dw_fact_market_scan set unlogged;
alter table dc.tmp_dw_fact_notes set unlogged;

