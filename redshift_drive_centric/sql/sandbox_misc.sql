﻿-- table counts --------------------------------------------------------
SELECT schemaname,relname,n_live_tup 
FROM pg_stat_user_tables 
where schemaname = 'dc' 
  and relname like 'ext_dw%'
  and relname <> 'ext_localstore'
ORDER BY relname

SELECT schemaname,relname,n_live_tup 
FROM pg_stat_user_tables 
where schemaname = 'dc' 
  and relname like 'xfm_dw%'
  and relname <> 'ext_localstore'
ORDER BY relname

SELECT schemaname,relname,n_live_tup 
FROM pg_stat_user_tables 
where schemaname = 'dc' 
  and relname like 'tmp_dw%'
  and relname <> 'ext_localstore'
ORDER BY relname


select aa.store_id,md5(aa::text) as hash
from dc.ext_dw_dim_store aa

select * from dc.tmp_dw_dim_store_changed_rows

select * from dc.xfm_dw_dim_store

select row_from_Date, count(*) from dc.xfm_Dw_dim_customer group by row_from_date

select a.*
from dc.xfm_dw_fact_sales_appointments a
join (
  select appointment_id
  from dc.xfm_dw_fact_sales_appointments
  where row_from_date = current_date) b on a.appointment_id = b.appointment_id
order by a.appointment_id, a.row_From_date  
limit 100

select store_id, count(*)
from dc.xfm_dw_fact_customer_communication
group by store_id
order by count(*) desc

