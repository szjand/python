﻿DROP TABLE IF EXISTS dc.ext_rpt_enhanced_days_to_conversion;
CREATE TABLE IF NOT EXISTS dc.ext_rpt_enhanced_days_to_conversion(
    store_id INTEGER,
    store_name CITEXT,
    user_id INTEGER,
    user_name CITEXT,
    source CITEXT,
    vehicle_status CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    team_name CITEXT,
    user_type CITEXT,
    user_key CITEXT,
    calendar_date DATE,
    total_leads NUMERIC,
    engaged_day_1 NUMERIC,
    engaged_day_2 NUMERIC,
    engaged_day_3 NUMERIC,
    engaged_day_4 NUMERIC,
    engaged_day_5_9 NUMERIC,
    engaged_day_10_30 NUMERIC,
    engaged_day_31_60 NUMERIC,
    engaged_day_61_90 NUMERIC,
    engaged_some_time NUMERIC,
    total_engaged NUMERIC,
    visit_day_1 NUMERIC,
    visit_day_2 NUMERIC,
    visit_day_3 NUMERIC,
    visit_day_4 NUMERIC,
    visit_day_5_9 NUMERIC,
    visit_day_10_30 NUMERIC,
    visit_day_31_60 NUMERIC,
    visit_day_61_90 NUMERIC,
    visit_some_time NUMERIC,
    total_visit NUMERIC,
    delivery_day_1 NUMERIC,
    delivery_day_2 NUMERIC,
    delivery_day_3 NUMERIC,
    delivery_day_4 NUMERIC,
    delivery_day_5_9 NUMERIC,
    delivery_day_10_30 NUMERIC,
    delivery_day_31_60 NUMERIC,
    delivery_day_61_90 NUMERIC,
    delivery_some_time NUMERIC)
WITH (OIDS=FALSE);
-- {store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,vehicle_model,team_name,user_type,user_key,calendar_date,total_leads,engaged_day_1,engaged_day_2,engaged_day_3,engaged_day_4,engaged_day_5_9,engaged_day_10_30,engaged_day_31_60,engaged_day_61_90,engaged_some_time,total_engaged,visit_day_1,visit_day_2,visit_day_3,visit_day_4,visit_day_5_9,visit_day_10_30,visit_day_31_60,visit_day_61_90,visit_some_time,total_visit,delivery_day_1,delivery_day_2,delivery_day_3,delivery_day_4,delivery_day_5_9,delivery_day_10_30,delivery_day_31_60,delivery_day_61_90,delivery_some_time}


DROP TABLE IF EXISTS dc.tmp_rpt_enhanced_days_to_conversion;
CREATE TABLE IF NOT EXISTS dc.tmp_rpt_enhanced_days_to_conversion(
    store_id INTEGER,
    store_name CITEXT,
    user_id INTEGER,
    user_name CITEXT,
    source CITEXT,
    vehicle_status CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    team_name CITEXT,
    user_type CITEXT,
    user_key CITEXT,
    calendar_date DATE,
    total_leads NUMERIC,
    engaged_day_1 NUMERIC,
    engaged_day_2 NUMERIC,
    engaged_day_3 NUMERIC,
    engaged_day_4 NUMERIC,
    engaged_day_5_9 NUMERIC,
    engaged_day_10_30 NUMERIC,
    engaged_day_31_60 NUMERIC,
    engaged_day_61_90 NUMERIC,
    engaged_some_time NUMERIC,
    total_engaged NUMERIC,
    visit_day_1 NUMERIC,
    visit_day_2 NUMERIC,
    visit_day_3 NUMERIC,
    visit_day_4 NUMERIC,
    visit_day_5_9 NUMERIC,
    visit_day_10_30 NUMERIC,
    visit_day_31_60 NUMERIC,
    visit_day_61_90 NUMERIC,
    visit_some_time NUMERIC,
    total_visit NUMERIC,
    delivery_day_1 NUMERIC,
    delivery_day_2 NUMERIC,
    delivery_day_3 NUMERIC,
    delivery_day_4 NUMERIC,
    delivery_day_5_9 NUMERIC,
    delivery_day_10_30 NUMERIC,
    delivery_day_31_60 NUMERIC,
    delivery_day_61_90 NUMERIC,
    delivery_some_time NUMERIC,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);


DROP TABLE IF EXISTS dc.xfm_rpt_enhanced_days_to_conversion;
CREATE TABLE IF NOT EXISTS dc.xfm_rpt_enhanced_days_to_conversion(
    store_id INTEGER,
    store_name CITEXT,
    user_id INTEGER,
    user_name CITEXT,
    source CITEXT,
    vehicle_status CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    team_name CITEXT,
    user_type CITEXT,
    user_key CITEXT,
    calendar_date DATE,
    total_leads NUMERIC,
    engaged_day_1 NUMERIC,
    engaged_day_2 NUMERIC,
    engaged_day_3 NUMERIC,
    engaged_day_4 NUMERIC,
    engaged_day_5_9 NUMERIC,
    engaged_day_10_30 NUMERIC,
    engaged_day_31_60 NUMERIC,
    engaged_day_61_90 NUMERIC,
    engaged_some_time NUMERIC,
    total_engaged NUMERIC,
    visit_day_1 NUMERIC,
    visit_day_2 NUMERIC,
    visit_day_3 NUMERIC,
    visit_day_4 NUMERIC,
    visit_day_5_9 NUMERIC,
    visit_day_10_30 NUMERIC,
    visit_day_31_60 NUMERIC,
    visit_day_61_90 NUMERIC,
    visit_some_time NUMERIC,
    total_visit NUMERIC,
    delivery_day_1 NUMERIC,
    delivery_day_2 NUMERIC,
    delivery_day_3 NUMERIC,
    delivery_day_4 NUMERIC,
    delivery_day_5_9 NUMERIC,
    delivery_day_10_30 NUMERIC,
    delivery_day_31_60 NUMERIC,
    delivery_day_61_90 NUMERIC,
    delivery_some_time NUMERIC)
WITH (OIDS=FALSE);