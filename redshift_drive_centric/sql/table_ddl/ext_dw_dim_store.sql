﻿DROP TABLE IF EXISTS dc.ext_dw_dim_store;
CREATE TABLE IF NOT EXISTS dc.ext_dw_dim_store(
    store_id INTEGER,
    store_group_id INTEGER,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    store_name CITEXT,
    store_city CITEXT,
    store_state CITEXT,
    store_zipcode CITEXT,
    store_phone CITEXT,
    store_nick_name CITEXT,
    store_web_site CITEXT,
    store_dms_type CITEXT)
WITH (OIDS=FALSE);
-- {store_id,store_group_id,date_created,date_modified,store_name,store_city,store_state,store_zipcode,store_phone,store_nick_name,store_web_site,store_dms_type}

create unique index on dc.ext_dw_dim_store(store_id);

select store_id
from dc.ext_dw_dim_store
group by store_id
having count(*) > 1

ALTER TABLE dc.xfm_dw_dim_store ADD PRIMARY KEY (store_id,row_thru_date);
ALTER TABLE dc.xfm_dw_dim_store add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_dim_store_changed_rows (store_id integer);	


DROP TABLE IF EXISTS dc.xfm_dw_dim_store;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_dim_store(
    store_id INTEGER,
    store_group_id INTEGER,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    store_name CITEXT,
    store_city CITEXT,
    store_state CITEXT,
    store_zipcode CITEXT,
    store_phone CITEXT,
    store_nick_name CITEXT,
    store_web_site CITEXT,
    store_dms_type CITEXT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);