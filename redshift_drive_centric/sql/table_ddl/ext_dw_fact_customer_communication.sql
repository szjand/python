﻿
DROP TABLE IF EXISTS dc.ext_dw_fact_customer_communication;
CREATE TABLE IF NOT EXISTS dc.ext_dw_fact_customer_communication(
    communication_id INTEGER,
    deal_id INTEGER,
    user_id INTEGER,
    store_id INTEGER,
    communication_date TIMESTAMP,
    communication_date_short DATE,
    ilm_override_start CITEXT,
    ilm_override_end CITEXT,
    ilm_date_start TIMESTAMP,
    ilm_date_end TIMESTAMP,
    is_business_hour_communication CITEXT,
    communication_modified_date TIMESTAMP,
    communication_type INTEGER,
    user_type INTEGER,
    user_type_name CITEXT,
    customer_communication_type CITEXT,
    customer_communication_group CITEXT,
    customer_id INTEGER,
    hasvideo INTEGER,
    hasduplicatevideo INTEGER,
    hasphoto INTEGER,
    is_from_customer INTEGER,
    is_to_customer INTEGER,
    sentiment_type_mixed NUMERIC,
    sentiment_type_positive NUMERIC,
    sentiment_type_neutral NUMERIC,
    sentiment_type_negative NUMERIC,
    rank_to_customer_deal_interaction BIGINT,
    rank_deal_interaction BIGINT,
    rank_deal_interaction_desc BIGINT,
    rank_communication_type BIGINT,
    rank_communication_group BIGINT,
    next_interaction_timestamp TIMESTAMP,
    prev_interaction_timestamp TIMESTAMP,
    next_interaction_usertype INTEGER,
    prev_interaction_usertype INTEGER,
    next_communication_type INTEGER,
    prev_communication_type INTEGER,
    next_is_to_customer INTEGER,
    prev_is_to_customer INTEGER,
    next_is_from_customer INTEGER,
    prev_is_from_customer INTEGER,
    next_response_time_secs BIGINT,
    next_response_less_than_15m INTEGER,
    next_response_15m_to_30m INTEGER,
    next_response_30m_to_60m INTEGER,
    next_response_gt_than_60m INTEGER,
    next_response_less_than_60m INTEGER,
    response_time_secs BIGINT,
    response_less_than_15m INTEGER,
    response_15m_to_30m INTEGER,
    response_30m_to_60m INTEGER,
    response_gt_than_60m INTEGER,
    response_less_than_60m INTEGER)
WITH (OIDS=FALSE);
-- {communication_id,deal_id,user_id,store_id,communication_date,communication_date_short,ilm_override_start,ilm_override_end,ilm_date_start,ilm_date_end,is_business_hour_communication,communication_modified_date,communication_type,user_type,user_type_name,customer_communication_type,customer_communication_group,customer_id,hasvideo,hasduplicatevideo,hasphoto,is_from_customer,is_to_customer,sentiment_type_mixed,sentiment_type_positive,sentiment_type_neutral,sentiment_type_negative,rank_to_customer_deal_interaction,rank_deal_interaction,rank_deal_interaction_desc,rank_communication_type,rank_communication_group,next_interaction_timestamp,prev_interaction_timestamp,next_interaction_usertype,prev_interaction_usertype,next_communication_type,prev_communication_type,next_is_to_customer,prev_is_to_customer,next_is_from_customer,prev_is_from_customer,next_response_time_secs,next_response_less_than_15m,next_response_15m_to_30m,next_response_30m_to_60m,next_response_gt_than_60m,next_response_less_than_60m,response_time_secs,response_less_than_15m,response_15m_to_30m,response_30m_to_60m,response_gt_than_60m,response_less_than_60m}

create unique index on dc.ext_dw_fact_customer_communication(communication_id, deal_id, communication_date,prev_interaction_timestamp);

select communication_id, deal_id, communication_date,prev_interaction_timestamp
from dc.ext_dw_fact_customer_communication
group by communication_id, deal_id, communication_date,prev_interaction_timestamp
having count(*) > 1

select * from dc.ext_dw_fact_customer_communication
where communication_id = 978686 and deal_id = 969894 and communication_date = '2018-05-25 11:28:41.147'


damnit, prev_interaction_timestamp has null values
select count(*) from dc.ext_dw_fact_customer_communication where prev_interaction_timestamp is null
yeah, like 453038 times

use this value in the coalesce: 1970-01-01 00:00:01


ALTER TABLE dc.xfm_dw_fact_customer_communication ADD PRIMARY KEY (communication_id,deal_id,
	communication_date,prev_interaction_timestamp,row_thru_date);
ALTER TABLE dc.xfm_dw_fact_customer_communication add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_fact_customer_communication_changed_rows (communication_id integer,
	deal_id integer,communication_date timestamp,prev_interaction_timestamp timestamp);		

DROP TABLE IF EXISTS dc.xfm_dw_fact_customer_communication;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_fact_customer_communication(
    communication_id INTEGER,
    deal_id INTEGER, 
    user_id INTEGER,
    store_id INTEGER,
    communication_date TIMESTAMP,
    communication_date_short DATE,
    ilm_override_start CITEXT,
    ilm_override_end CITEXT,
    ilm_date_start TIMESTAMP,
    ilm_date_end TIMESTAMP,
    is_business_hour_communication CITEXT,
    communication_modified_date TIMESTAMP,
    communication_type INTEGER,
    user_type INTEGER,
    user_type_name CITEXT,
    customer_communication_type CITEXT,
    customer_communication_group CITEXT,
    customer_id INTEGER,
    hasvideo INTEGER,
    hasduplicatevideo INTEGER,
    hasphoto INTEGER,
    is_from_customer INTEGER,
    is_to_customer INTEGER,
    sentiment_type_mixed NUMERIC,
    sentiment_type_positive NUMERIC,
    sentiment_type_neutral NUMERIC,
    sentiment_type_negative NUMERIC,
    rank_to_customer_deal_interaction BIGINT,
    rank_deal_interaction BIGINT,
    rank_deal_interaction_desc BIGINT,
    rank_communication_type BIGINT,
    rank_communication_group BIGINT,
    next_interaction_timestamp TIMESTAMP,
    prev_interaction_timestamp TIMESTAMP,
    next_interaction_usertype INTEGER,
    prev_interaction_usertype INTEGER,
    next_communication_type INTEGER,
    prev_communication_type INTEGER,
    next_is_to_customer INTEGER,
    prev_is_to_customer INTEGER,
    next_is_from_customer INTEGER,
    prev_is_from_customer INTEGER,
    next_response_time_secs BIGINT,
    next_response_less_than_15m INTEGER,
    next_response_15m_to_30m INTEGER,
    next_response_30m_to_60m INTEGER,
    next_response_gt_than_60m INTEGER,
    next_response_less_than_60m INTEGER,
    response_time_secs BIGINT,
    response_less_than_15m INTEGER,
    response_15m_to_30m INTEGER,
    response_30m_to_60m INTEGER,
    response_gt_than_60m INTEGER,
    response_less_than_60m INTEGER,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);