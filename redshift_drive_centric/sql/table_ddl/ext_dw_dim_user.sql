﻿DROP TABLE IF EXISTS dc.ext_dw_dim_user;
CREATE TABLE IF NOT EXISTS dc.ext_dw_dim_user(
    user_id INTEGER,
    store_id INTEGER,
    first_name CITEXT,
    last_name CITEXT,
    user_type CITEXT,
    user_title CITEXT,
    user_photo_url CITEXT,
    user_email CITEXT,
    is_user_active INTEGER,
    user_role INTEGER,
    user_team_id INTEGER,
    is_user_clocked_in INTEGER,
    user_role_value CITEXT,
    user_team_name CITEXT,
    users_in_team CITEXT,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    user_lookup_value CITEXT,
    user_lookup_type CITEXT)
WITH (OIDS=FALSE);
-- {user_id,store_id,first_name,last_name,user_type,user_title,user_photo_url,user_email,is_user_active,user_role,user_team_id,is_user_clocked_in,user_role_value,user_team_name,users_in_team,date_created,date_modified,user_lookup_value,user_lookup_type}

create unique index on dc.ext_dw_dim_user(user_id,user_team_name);


select user_id
from dc.ext_dw_dim_user
group by user_id
having count(*) > 1;

-- it is nonsensical, but user_team_name is what distinguishes these 2 dups
select * 
from dc.ext_dw_dim_user
where user_id in (1910, 5467)

ALTER TABLE dc.xfm_dw_dim_user ADD PRIMARY KEY (user_id,user_team_name,row_thru_date);
ALTER TABLE dc.xfm_dw_dim_user add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_dim_user_changed_rows (user_id integer,user_team_name citext);	




DROP TABLE IF EXISTS dc.xfm_dw_dim_user;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_dim_user(
    user_id INTEGER,
    store_id INTEGER,
    first_name CITEXT,
    last_name CITEXT,
    user_type CITEXT,
    user_title CITEXT,
    user_photo_url CITEXT,
    user_email CITEXT,
    is_user_active INTEGER,
    user_role INTEGER,
    user_team_id INTEGER,
    is_user_clocked_in INTEGER,
    user_role_value CITEXT,
    user_team_name CITEXT,
    users_in_team CITEXT,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    user_lookup_value CITEXT,
    user_lookup_type CITEXT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);