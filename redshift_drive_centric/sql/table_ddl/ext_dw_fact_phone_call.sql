﻿DROP TABLE IF EXISTS dc.ext_dw_fact_phone_call;
CREATE TABLE IF NOT EXISTS dc.ext_dw_fact_phone_call(
    task_id INTEGER,
    customer_id INTEGER,
    deal_id INTEGER,
    store_id INTEGER,
    for_user_id INTEGER,
    created_by_user_id INTEGER,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    date_due TIMESTAMP,
    completed_by_user_id INTEGER,
    date_completed TIMESTAMP,
    date_completed_short DATE,
    result INTEGER,
    result_name CITEXT,
    title CITEXT,
    is_deleted CITEXT,
    call_queue_id INTEGER,
    from_phone_number CITEXT,
    to_phone_number CITEXT,
    duration_in_seconds INTEGER,
    is_from_customer INTEGER,
    is_to_customer INTEGER,
    recording_url CITEXT)
WITH (OIDS=FALSE);
-- {task_id,customer_id,deal_id,store_id,for_user_id,created_by_user_id,date_created,date_modified,date_due,completed_by_user_id,date_completed,date_completed_short,result,result_name,title,is_deleted,call_queue_id,from_phone_number,to_phone_number,duration_in_seconds,is_from_customer,is_to_customer,recording_url}

create unique index on dc.ext_dw_fact_phone_call(task_id);

select task_id from dc.ext_dw_fact_phone_call
group by task_id having count(*) > 1

ALTER TABLE dc.xfm_dw_fact_phone_call ADD PRIMARY KEY (task_id,row_thru_date);
ALTER TABLE dc.xfm_dw_fact_phone_call add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_fact_phone_call_changed_rows (task_id integer);	


DROP TABLE IF EXISTS dc.xfm_dw_fact_phone_call;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_fact_phone_call(
    task_id INTEGER,
    customer_id INTEGER,
    deal_id INTEGER,
    store_id INTEGER,
    for_user_id INTEGER,
    created_by_user_id INTEGER,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    date_due TIMESTAMP,
    completed_by_user_id INTEGER,
    date_completed TIMESTAMP,
    date_completed_short DATE,
    result INTEGER,
    result_name CITEXT,
    title CITEXT,
    is_deleted CITEXT,
    call_queue_id INTEGER,
    from_phone_number CITEXT,
    to_phone_number CITEXT,
    duration_in_seconds INTEGER,
    is_from_customer INTEGER,
    is_to_customer INTEGER,
    recording_url CITEXT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);