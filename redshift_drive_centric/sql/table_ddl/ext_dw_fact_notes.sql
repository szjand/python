﻿DROP TABLE IF EXISTS dc.ext_dw_fact_notes;
CREATE TABLE IF NOT EXISTS dc.ext_dw_fact_notes(
    deal_id INTEGER,
    user_id INTEGER,
    store_id INTEGER,
    date_created TIMESTAMP,
    notes CITEXT)
WITH (OIDS=FALSE);
-- {deal_id,user_id,store_id,date_created,notes}

create unique index on dc.ext_dw_fact_notes(deal_id,date_created,md5(notes));

can not create an index on the notes field:
	ERROR:  index row size 2880 exceeds maximum 2712 for index "ext_dw_fact_notes_deal_id_date_created_notes_idx"
	HINT:  Values larger than 1/3 of a buffer page cannot be indexed.
	Consider a function index of an MD5 hash of the value, or use full text indexing.
hashing the notes field works, but will need to add that as a field to the xfm table	


select deal_id, date_created, md5(notes)
from dc.ext_dw_fact_notes
group by deal_id, date_created, md5(notes)
having count(*) > 1


ALTER TABLE dc.xfm_dw_fact_notes ADD PRIMARY KEY (deal_id,date_created,notes_hash,row_thru_date);
ALTER TABLE dc.xfm_dw_fact_notes add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_fact_notes_changed_rows (deal_id integer,date_created timestamp,notes_hash text);	


select count(*) from dc.ext_dw_fact_notes

DROP TABLE IF EXISTS dc.xfm_dw_fact_notes;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_fact_notes(
    deal_id INTEGER,
    user_id INTEGER,
    store_id INTEGER,
    date_created TIMESTAMP,
    notes CITEXT,
    notes_hash text,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);