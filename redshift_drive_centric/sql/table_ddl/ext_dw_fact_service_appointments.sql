﻿DROP TABLE IF EXISTS dc.ext_dw_fact_service_appointments;
CREATE TABLE IF NOT EXISTS dc.ext_dw_fact_service_appointments(
    appointment_id INTEGER,
    deal_id INTEGER,
    customer_id INTEGER,
    store_id INTEGER,
    appointment_type CITEXT,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    created_by_user_id INTEGER,
    date_start TIMESTAMP,
    date_completed TIMESTAMP,
    date_confirmed TIMESTAMP,
    assigned_user_id INTEGER,
    confirmed_by_user_id INTEGER,
    completed_by_user_id INTEGER,
    was_attended_by_customer CITEXT,
    notes CITEXT,
    first_appointment INTEGER,
    subsequent_appointment INTEGER,
    is_appt_created_by_user INTEGER,
    date_start_short DATE,
    date_completed_short DATE,
    date_confirmed_short DATE)
WITH (OIDS=FALSE);
-- {appointment_id,deal_id,customer_id,store_id,appointment_type,date_created,date_modified,created_by_user_id,date_start,date_completed,date_confirmed,assigned_user_id,confirmed_by_user_id,completed_by_user_id,was_attended_by_customer,notes,first_appointment,subsequent_appointment,is_appt_created_by_user,date_start_short,date_completed_short,date_confirmed_short}

create unique index on dc.ext_dw_fact_service_appointments(appointment_id);

select appointment_id
from dc.ext_dw_fact_service_appointments
group by appointment_id
having count(*) > 1

ALTER TABLE dc.xfm_dw_fact_service_appointments ADD PRIMARY KEY (appointment_id,row_thru_date);
ALTER TABLE dc.xfm_dw_fact_service_appointments add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_fact_service_appointments_changed_rows (appointment_id integer);

DROP TABLE IF EXISTS dc.xfm_dw_fact_service_appointments;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_fact_service_appointments(
    appointment_id INTEGER,
    deal_id INTEGER,
    customer_id INTEGER,
    store_id INTEGER,
    appointment_type CITEXT,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    created_by_user_id INTEGER,
    date_start TIMESTAMP,
    date_completed TIMESTAMP,
    date_confirmed TIMESTAMP,
    assigned_user_id INTEGER,
    confirmed_by_user_id INTEGER,
    completed_by_user_id INTEGER,
    was_attended_by_customer CITEXT,
    notes CITEXT,
    first_appointment INTEGER,
    subsequent_appointment INTEGER,
    is_appt_created_by_user INTEGER,
    date_start_short DATE,
    date_completed_short DATE,
    date_confirmed_short DATE,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);