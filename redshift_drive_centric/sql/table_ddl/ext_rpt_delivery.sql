﻿DROP TABLE IF EXISTS dc.ext_rpt_delivery;
CREATE TABLE IF NOT EXISTS dc.ext_rpt_delivery(
    entity CITEXT,
    store_id INTEGER,
    store_name CITEXT,
    user_id INTEGER,
    user_name CITEXT,
    source CITEXT,
    vehicle_status CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    team_name CITEXT,
    user_type CITEXT,
    calendar_date DATE,
    total_opportunities BIGINT,
    total_sold BIGINT,
    total_delivered BIGINT,
    total_delivered_units NUMERIC,
    delivery_front_end_gross NUMERIC,
    delivery_back_end_gross NUMERIC,
    delivery_total_gross NUMERIC,
    delivery_service_contract NUMERIC,
    delivery_gap NUMERIC,
    date_report_ran TIMESTAMPTZ)
WITH (OIDS=FALSE);
-- {entity,store_id,store_name,user_id,user_name,source,vehicle_status,vehicle_make,vehicle_model,team_name,user_type,calendar_date,total_opportunities,total_sold,total_delivered,total_delivered_units,delivery_front_end_gross,delivery_back_end_gross,delivery_total_gross,delivery_service_contract,delivery_gap,date_report_ran}


DROP TABLE IF EXISTS dc.tmp_rpt_delivery;
CREATE TABLE IF NOT EXISTS dc.tmp_rpt_delivery(
    entity CITEXT,
    store_id INTEGER,
    store_name CITEXT,
    user_id INTEGER,
    user_name CITEXT,
    source CITEXT,
    vehicle_status CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    team_name CITEXT,
    user_type CITEXT,
    calendar_date DATE,
    total_opportunities BIGINT,
    total_sold BIGINT,
    total_delivered BIGINT,
    total_delivered_units NUMERIC,
    delivery_front_end_gross NUMERIC,
    delivery_back_end_gross NUMERIC,
    delivery_total_gross NUMERIC,
    delivery_service_contract NUMERIC,
    delivery_gap NUMERIC,
    date_report_ran TIMESTAMPTZ,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);


DROP TABLE IF EXISTS dc.xfm_rpt_delivery;
CREATE TABLE IF NOT EXISTS dc.xfm_rpt_delivery(
    entity CITEXT,
    store_id INTEGER,
    store_name CITEXT,
    user_id INTEGER,
    user_name CITEXT,
    source CITEXT,
    vehicle_status CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    team_name CITEXT,
    user_type CITEXT,
    calendar_date DATE,
    total_opportunities BIGINT,
    total_sold BIGINT,
    total_delivered BIGINT,
    total_delivered_units NUMERIC,
    delivery_front_end_gross NUMERIC,
    delivery_back_end_gross NUMERIC,
    delivery_total_gross NUMERIC,
    delivery_service_contract NUMERIC,
    delivery_gap NUMERIC,
    date_report_ran TIMESTAMPTZ,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);