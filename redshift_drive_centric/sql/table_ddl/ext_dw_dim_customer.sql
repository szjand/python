﻿DROP TABLE IF EXISTS dc.ext_dw_dim_customer;
CREATE unlogged TABLE IF NOT EXISTS dc.ext_dw_dim_customer(
    customer_id CITEXT,
    store_id CITEXT,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    customer_type INTEGER,
    is_company INTEGER,
    first_name CITEXT,
    last_name CITEXT,
    company_name CITEXT,
    primary_photo_url CITEXT,
    customer_primary_email CITEXT,
    customer_primary_phone CITEXT,
    customer_secodary_phone CITEXT,
    customer_third_phone CITEXT,
    email_unsubscribe_date TIMESTAMP,
    phone_unsubscribe_date TIMESTAMP,
    snailmail_unsubscribe_date TIMESTAMP,
    text_unsubscribe_date TIMESTAMP,
    customer_optin_date TIMESTAMP,
    is_text_opt_out INTEGER,
    is_text_opt_in INTEGER,
    first_opted_out_phone_number CITEXT,
    second_opted_out_phone_number CITEXT,
    third_opted_out_phone_number CITEXT,
    best_contact_method CITEXT,
    customer_city CITEXT,
    customer_state CITEXT,
    customer_zip CITEXT,
    date_of_birth TIMESTAMP,
    primary_sales_user_id CITEXT,
    secondary_sales_user_id CITEXT,
    bdc_sales_user_id CITEXT)
WITH (OIDS=FALSE);
create unique index on dc.ext_dw_dim_customer(customer_id);


select * from dc.ext_dw_dim_customer limit 100
select count(*) from dc.ext_dw_dim_customer

select customer_id 
from dc.ext_dw_dim_customer
group by customer_id
having count(*) > 1

ALTER TABLE dc.xfm_dw_dim_customer ADD PRIMARY KEY (customer_id,row_thru_date);
ALTER TABLE dc.xfm_dw_dim_customer add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_dim_customer_changed_rows (customer_id citext primary key);


DROP TABLE IF EXISTS dc.xfm_dw_dim_customer;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_dim_customer(
    customer_id CITEXT,
    store_id CITEXT,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    customer_type INTEGER,
    is_company INTEGER,
    first_name CITEXT,
    last_name CITEXT,
    company_name CITEXT,
    primary_photo_url CITEXT,
    customer_primary_email CITEXT,
    customer_primary_phone CITEXT,
    customer_secodary_phone CITEXT,
    customer_third_phone CITEXT,
    email_unsubscribe_date TIMESTAMP,
    phone_unsubscribe_date TIMESTAMP,
    snailmail_unsubscribe_date TIMESTAMP,
    text_unsubscribe_date TIMESTAMP,
    customer_optin_date TIMESTAMP,
    is_text_opt_out INTEGER,
    is_text_opt_in INTEGER,
    first_opted_out_phone_number CITEXT,
    second_opted_out_phone_number CITEXT,
    third_opted_out_phone_number CITEXT,
    best_contact_method CITEXT,
    customer_city CITEXT,
    customer_state CITEXT,
    customer_zip CITEXT,
    date_of_birth TIMESTAMP,
    primary_sales_user_id CITEXT,
    secondary_sales_user_id CITEXT,
    bdc_sales_user_id CITEXT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash citext)
WITH (OIDS=FALSE);