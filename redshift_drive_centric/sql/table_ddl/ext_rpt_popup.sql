﻿DROP TABLE IF EXISTS dc.ext_rpt_popup;
CREATE TABLE IF NOT EXISTS dc.ext_rpt_popup(
    store_name CITEXT,
    customer_name CITEXT,
    sales_1_user_id INTEGER,
    vehicle_year INTEGER,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    date_created DATE,
    source CITEXT,
    user_name CITEXT,
    user_key CITEXT)
WITH (OIDS=FALSE);
-- {store_name,customer_name,sales_1_user_id,vehicle_year,vehicle_make,vehicle_model,date_created,source,user_name,user_key}


DROP TABLE IF EXISTS dc.tmp_rpt_popup;
CREATE TABLE IF NOT EXISTS dc.tmp_rpt_popup(
    store_name CITEXT,
    customer_name CITEXT,
    sales_1_user_id INTEGER,
    vehicle_year INTEGER,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    date_created DATE,
    source CITEXT,
    user_name CITEXT,
    user_key CITEXT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);


DROP TABLE IF EXISTS dc.xfm_rpt_popup;
CREATE TABLE IF NOT EXISTS dc.xfm_rpt_popup(
    store_name CITEXT,
    customer_name CITEXT,
    sales_1_user_id INTEGER,
    vehicle_year INTEGER,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    date_created DATE,
    source CITEXT,
    user_name CITEXT,
    user_key CITEXT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);