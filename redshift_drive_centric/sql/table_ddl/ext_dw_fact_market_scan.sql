﻿DROP TABLE IF EXISTS dc.ext_dw_fact_market_scan;
CREATE TABLE IF NOT EXISTS dc.ext_dw_fact_market_scan(
    deal_id INTEGER,
    user_id INTEGER,
    store_id INTEGER,
    proposal_date DATE,
    retail_proposal_count BIGINT,
    lease_proposal_count BIGINT,
    total_proposal_count BIGINT)
WITH (OIDS=FALSE);
-- {deal_id,user_id,store_id,proposal_date,retail_proposal_count,lease_proposal_count,total_proposal_count}

create unique index on dc.ext_dw_fact_market_scan(deal_id,user_id,store_id,proposal_date);

select deal_id, user_id, store_id, proposal_date
from dc.ext_dw_fact_market_scan
group by deal_id, user_id, store_id, proposal_date
having count(*) > 1

select * from dc.ext_dw_Fact_market_scan
where deal_id = 0 and user_id = 1889 and proposal_date = '07/26/2016'

ALTER TABLE dc.xfm_dw_fact_market_scan ADD PRIMARY KEY (deal_id,user_id,store_id,proposal_date,row_thru_date);
ALTER TABLE dc.xfm_dw_fact_market_scan add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_fact_market_scan_changed_rows (deal_id integer,user_id integer,store_id integer,proposal_date date);		


DROP TABLE IF EXISTS dc.tmp_dw_fact_market_scan;
CREATE TABLE IF NOT EXISTS dc.tmp_dw_fact_market_scan(
    deal_id INTEGER,
    user_id INTEGER,
    store_id INTEGER,
    proposal_date DATE,
    retail_proposal_count BIGINT,
    lease_proposal_count BIGINT,
    total_proposal_count BIGINT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);

DROP TABLE IF EXISTS dc.xfm_dw_fact_market_scan;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_fact_market_scan(
    deal_id INTEGER,
    user_id INTEGER,
    store_id INTEGER,
    proposal_date DATE,
    retail_proposal_count BIGINT,
    lease_proposal_count BIGINT,
    total_proposal_count BIGINT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);