﻿DROP TABLE IF EXISTS dc.ext_dw_fact_store_app;
CREATE TABLE IF NOT EXISTS dc.ext_dw_fact_store_app(
    store_id INTEGER,
    store_app_name CITEXT,
    is_app_enabled INTEGER,
    store_app_id INTEGER,
    date_started TIMESTAMP,
    date_modified TIMESTAMP)
WITH (OIDS=FALSE);
-- {store_id,store_app_name,is_app_enabled,store_app_id,date_started,date_modified}

create unique index on dc.ext_dw_fact_store_app(store_app_id);

select store_app_id
from dc.ext_dw_fact_store_app
group by store_app_id
having count(*) > 1

select * 
from dc. ext_dw_fact_store_app
where store_id in (26,25,24,27)
  and store_app_name = 'scout'
order by store_id  

ALTER TABLE dc.xfm_dw_fact_store_app ADD PRIMARY KEY (store_app_id,row_thru_date);
ALTER TABLE dc.xfm_dw_fact_store_app add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_fact_store_app_changed_rows (store_app_id integer);		



DROP TABLE IF EXISTS dc.xfm_dw_fact_store_app;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_fact_store_app(
    store_id INTEGER,
    store_app_name CITEXT,
    is_app_enabled INTEGER,
    store_app_id INTEGER,
    date_started TIMESTAMP,
    date_modified TIMESTAMP,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);