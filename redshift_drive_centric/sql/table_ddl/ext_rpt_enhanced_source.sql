﻿DROP TABLE IF EXISTS dc.ext_rpt_enhanced_source;
CREATE TABLE IF NOT EXISTS dc.ext_rpt_enhanced_source(
    store_id INTEGER,
    store_name CITEXT,
    source CITEXT,
    source_description CITEXT,
    vehicle_status CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    calendar_date DATE,
    opportunities_from_all_sources INTEGER,
    total_opportunities INTEGER,
    total_appointments INTEGER,
    appointment_show INTEGER,
    total_show INTEGER,
    total_sold INTEGER,
    total_delivered INTEGER,
    total_dead INTEGER,
    delivery_front_end_gross NUMERIC,
    delivery_back_end_gross NUMERIC,
    delivery_total_gross NUMERIC)
WITH (OIDS=FALSE);
-- {store_id,store_name,source,source_description,vehicle_status,vehicle_make,vehicle_model,calendar_date,opportunities_from_all_sources,total_opportunities,total_appointments,appointment_show,total_show,total_sold,total_delivered,total_dead,delivery_front_end_gross,delivery_back_end_gross,delivery_total_gross}


DROP TABLE IF EXISTS dc.tmp_rpt_enhanced_source;
CREATE TABLE IF NOT EXISTS dc.tmp_rpt_enhanced_source(
    store_id INTEGER,
    store_name CITEXT,
    source CITEXT,
    source_description CITEXT,
    vehicle_status CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    calendar_date DATE,
    opportunities_from_all_sources INTEGER,
    total_opportunities INTEGER,
    total_appointments INTEGER,
    appointment_show INTEGER,
    total_show INTEGER,
    total_sold INTEGER,
    total_delivered INTEGER,
    total_dead INTEGER,
    delivery_front_end_gross NUMERIC,
    delivery_back_end_gross NUMERIC,
    delivery_total_gross NUMERIC,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);


DROP TABLE IF EXISTS dc.xfm_rpt_enhanced_source;
CREATE TABLE IF NOT EXISTS dc.xfm_rpt_enhanced_source(
    store_id INTEGER,
    store_name CITEXT,
    source CITEXT,
    source_description CITEXT,
    vehicle_status CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    calendar_date DATE,
    opportunities_from_all_sources INTEGER,
    total_opportunities INTEGER,
    total_appointments INTEGER,
    appointment_show INTEGER,
    total_show INTEGER,
    total_sold INTEGER,
    total_delivered INTEGER,
    total_dead INTEGER,
    delivery_front_end_gross NUMERIC,
    delivery_back_end_gross NUMERIC,
    delivery_total_gross NUMERIC,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);