﻿04/15/21 decided this is of no use

DROP TABLE IF EXISTS dc.ext_dw_fact_closed_ro;
CREATE TABLE IF NOT EXISTS dc.ext_dw_fact_closed_ro(
    ro_number CITEXT,
    customer_id INTEGER,
    vehicle_id INTEGER,
    date_ro_open TIMESTAMP,
    date_ro_close TIMESTAMP,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    vehicle_vin CITEXT,
    vehicle_year INTEGER,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    vehicle_trim CITEXT,
    vehicle_mileage INTEGER)
WITH (OIDS=FALSE);
-- {ro_number,customer_id,vehicle_id,date_ro_open,date_ro_close,date_created,date_modified,vehicle_vin,vehicle_year,vehicle_make,vehicle_model,vehicle_trim,vehicle_mileage}

select ro_number
from dc.ext_Dw_fact_closed_ro
group by ro_number
having count(*) > 1

-- these are goofy, 
select * 
from dc.ext_dw_fact_closed_ro
where ro_number in ('16440838','16440843','16440860')
order by ro_number

select ro_number,customer_id,vehicle_id,vehicle_vin
from dc.ext_dw_fact_closed_ro
group by ro_number,customer_id,vehicle_id,vehicle_vin

DROP TABLE IF EXISTS dc.tmp_dw_fact_closed_ro;
CREATE TABLE IF NOT EXISTS dc.tmp_dw_fact_closed_ro(
    ro_number CITEXT,
    customer_id INTEGER,
    vehicle_id INTEGER,
    date_ro_open TIMESTAMP,
    date_ro_close TIMESTAMP,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    vehicle_vin CITEXT,
    vehicle_year INTEGER,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    vehicle_trim CITEXT,
    vehicle_mileage INTEGER,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);


DROP TABLE IF EXISTS dc.xfm_dw_fact_closed_ro;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_fact_closed_ro(
    ro_number CITEXT,
    customer_id INTEGER,
    vehicle_id INTEGER,
    date_ro_open TIMESTAMP,
    date_ro_close TIMESTAMP,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    vehicle_vin CITEXT,
    vehicle_year INTEGER,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    vehicle_trim CITEXT,
    vehicle_mileage INTEGER,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);