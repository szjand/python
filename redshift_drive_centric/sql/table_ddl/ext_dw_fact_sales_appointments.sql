﻿DROP TABLE IF EXISTS dc.ext_dw_fact_sales_appointments;
CREATE TABLE IF NOT EXISTS dc.ext_dw_fact_sales_appointments(
    appointment_id INTEGER,
    deal_id INTEGER,
    customer_id INTEGER,
    store_id INTEGER,
    appointment_type CITEXT,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    created_by_user_id INTEGER,
    date_start TIMESTAMP,
    is_completed INTEGER,
    date_completed TIMESTAMP,
    is_confirmed INTEGER,
    date_confirmed TIMESTAMP,
    assigned_user_id INTEGER,
    confirmed_by_user_id INTEGER,
    completed_by_user_id INTEGER,
    was_attended_by_customer INTEGER,
    notes CITEXT,
    first_appointment INTEGER,
    first_scheduled_appointment INTEGER,
    subsequent_appointment INTEGER,
    subsequent_scheduled_appointment INTEGER,
    is_appt_created_by_user INTEGER,
    date_start_short DATE,
    date_completed_short DATE,
    date_confirmed_short DATE,
    date_created_short DATE)
WITH (OIDS=FALSE);
-- {appointment_id,deal_id,customer_id,store_id,appointment_type,date_created,date_modified,created_by_user_id,date_start,is_completed,date_completed,is_confirmed,date_confirmed,assigned_user_id,confirmed_by_user_id,completed_by_user_id,was_attended_by_customer,notes,first_appointment,first_scheduled_appointment,subsequent_appointment,subsequent_scheduled_appointment,is_appt_created_by_user,date_start_short,date_completed_short,date_confirmed_short,date_created_short}

create unique index on dc.tmp_dw_fact_sales_appointments(appointment_id);

select appointment_id
from dc.ext_dw_fact_sales_appointments
group by appointment_id
having count(*) > 1

ALTER TABLE dc.xfm_dw_fact_sales_appointments ADD PRIMARY KEY (appointment_id,row_thru_date);
ALTER TABLE dc.xfm_dw_fact_sales_appointments add constraint thru_gt_from check (row_thru_date > row_from_date);    
create unlogged table dc.tmp_dw_fact_sales_appointments_changed_rows (appointment_id integer);		



DROP TABLE IF EXISTS dc.xfm_dw_fact_sales_appointments;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_fact_sales_appointments(
    appointment_id INTEGER,
    deal_id INTEGER,
    customer_id INTEGER,
    store_id INTEGER,
    appointment_type CITEXT,
    date_created TIMESTAMP,
    date_modified TIMESTAMP,
    created_by_user_id INTEGER,
    date_start TIMESTAMP,
    is_completed INTEGER,
    date_completed TIMESTAMP,
    is_confirmed INTEGER,
    date_confirmed TIMESTAMP,
    assigned_user_id INTEGER,
    confirmed_by_user_id INTEGER,
    completed_by_user_id INTEGER,
    was_attended_by_customer INTEGER,
    notes CITEXT,
    first_appointment INTEGER,
    first_scheduled_appointment INTEGER,
    subsequent_appointment INTEGER,
    subsequent_scheduled_appointment INTEGER,
    is_appt_created_by_user INTEGER,
    date_start_short DATE,
    date_completed_short DATE,
    date_confirmed_short DATE,
    date_created_short DATE,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);