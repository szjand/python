﻿DROP TABLE IF EXISTS dc.ext_dw_fact_text_email_communication;
CREATE unlogged TABLE IF NOT EXISTS dc.ext_dw_fact_text_email_communication(
    communication_id INTEGER,
    deal_id INTEGER,
    user_id INTEGER,
    store_id INTEGER,
    communication_date TIMESTAMP,
    communication_date_short DATE,
    ilm_override_start TIMESTAMP,
    ilm_override_end TIMESTAMP,
    ilm_date_start TIMESTAMP,
    ilm_date_end TIMESTAMP,
    is_business_hour_communication CITEXT,
    communication_modified_date TIMESTAMP,
    communication_type INTEGER,
    user_type INTEGER,
    user_type_name CITEXT,
    customer_communication_type CITEXT,
    customer_communication_group CITEXT,
    customer_id INTEGER,
    hasvideo INTEGER,
    hasduplicatevideo INTEGER,
    hasphoto INTEGER,
    is_from_customer INTEGER,
    is_to_customer INTEGER,
    sentiment_type_mixed NUMERIC,
    sentiment_type_positive NUMERIC,
    sentiment_type_neutral NUMERIC,
    sentiment_type_negative NUMERIC,
    next_response_time_secs BIGINT,
    is_next_response_fumble INTEGER,
    adjusted_next_response_time_secs BIGINT,
    next_response_less_than_15m INTEGER,
    next_response_15m_to_30m INTEGER,
    next_response_30m_to_60m INTEGER,
    next_response_gt_than_60m INTEGER,
    next_response_less_than_60m INTEGER)
WITH (OIDS=FALSE);
-- {communication_id,deal_id,user_id,store_id,communication_date,communication_date_short,ilm_override_start,ilm_override_end,ilm_date_start,ilm_date_end,is_business_hour_communication,communication_modified_date,communication_type,user_type,user_type_name,customer_communication_type,customer_communication_group,customer_id,hasvideo,hasduplicatevideo,hasphoto,is_from_customer,is_to_customer,sentiment_type_mixed,sentiment_type_positive,sentiment_type_neutral,sentiment_type_negative,next_response_time_secs,is_next_response_fumble,adjusted_next_response_time_secs,next_response_less_than_15m,next_response_15m_to_30m,next_response_30m_to_60m,next_response_gt_than_60m,next_response_less_than_60m}
create unique index on dc.ext_dw_Fact_Text_email_communication(communication_id);

select count(*) from dc.ext_dw_fact_text_email_communication  --919513

select communication_id
from dc.ext_dw_Fact_Text_email_communication
group by communication_id
having count(*) > 1

ALTER TABLE dc.xfm_dw_fact_text_email_communication ADD PRIMARY KEY (communication_id,row_thru_date);
ALTER TABLE dc.xfm_dw_fact_text_email_communication add constraint thru_gt_from check (row_thru_date >= row_from_date);    
create unlogged table dc.tmp_dw_fact_text_email_communication_changed_rows (communication_id integer primary key);

 
DROP TABLE IF EXISTS dc.xfm_dw_fact_text_email_communication;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_fact_text_email_communication(
    communication_id INTEGER,
    deal_id INTEGER,
    user_id INTEGER,
    store_id INTEGER,
    communication_date TIMESTAMP,
    communication_date_short DATE,
    ilm_override_start TIMESTAMP,
    ilm_override_end TIMESTAMP,
    ilm_date_start TIMESTAMP,
    ilm_date_end TIMESTAMP,
    is_business_hour_communication CITEXT,
    communication_modified_date TIMESTAMP,
    communication_type INTEGER,
    user_type INTEGER,
    user_type_name CITEXT,
    customer_communication_type CITEXT,
    customer_communication_group CITEXT,
    customer_id INTEGER,
    hasvideo INTEGER,
    hasduplicatevideo INTEGER,
    hasphoto INTEGER,
    is_from_customer INTEGER,
    is_to_customer INTEGER,
    sentiment_type_mixed NUMERIC,
    sentiment_type_positive NUMERIC,
    sentiment_type_neutral NUMERIC,
    sentiment_type_negative NUMERIC,
    next_response_time_secs BIGINT,
    is_next_response_fumble INTEGER,
    adjusted_next_response_time_secs BIGINT,
    next_response_less_than_15m INTEGER,
    next_response_15m_to_30m INTEGER,
    next_response_30m_to_60m INTEGER,
    next_response_gt_than_60m INTEGER,
    next_response_less_than_60m INTEGER,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash citext)
WITH (OIDS=FALSE);