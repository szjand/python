﻿DROP TABLE IF EXISTS dc.ext_rpt_appointment;
CREATE TABLE IF NOT EXISTS dc.ext_rpt_appointment(
    date_report_ran TIMESTAMPTZ,
    entity CITEXT,
    store_id INTEGER,
    user_id INTEGER,
    calendar_date DATE,
    source CITEXT,
    team_name CITEXT,
    user_name CITEXT,
    store_name CITEXT,
    user_type CITEXT,
    user_key CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    vehicle_status CITEXT,
    unique_set BIGINT,
    total_set BIGINT,
    confirmed_appointments BIGINT,
    unique_scheduled BIGINT,
    total_scheduled BIGINT,
    total_scheduled_w_video BIGINT,
    total_scheduled_w_no_video BIGINT,
    total_attended BIGINT,
    video_day_appointment BIGINT,
    video_show BIGINT,
    no_video_show BIGINT,
    total_visits BIGINT,
    total_beback BIGINT,
    total_show BIGINT,
    total_delivered BIGINT)
WITH (OIDS=FALSE);
-- {date_report_ran,entity,store_id,user_id,calendar_date,source,team_name,user_name,store_name,user_type,user_key,vehicle_make,vehicle_model,vehicle_status,unique_set,total_set,confirmed_appointments,unique_scheduled,total_scheduled,total_scheduled_w_video,total_scheduled_w_no_video,total_attended,video_day_appointment,video_show,no_video_show,total_visits,total_beback,total_show,total_delivered}


select date_report_ran,entity,store_id,user_id,calendar_date,source,vehicle_model, vehicle_status, count(*)
from dc.ext_rpt_appointment
group by date_report_ran,entity,store_id,user_id,calendar_date,source,vehicle_model, vehicle_status
having count(*) > 1

-- looks like this is another case of base table not having a natrual key
-- need to do a distinct in the extract
-- doesn't help, still no natural key short of all fields
select * from dc.ext_rpt_appointment
where date_report_ran = '2021-04-15 02:02:32.74312-05'
  and entity = 'SourceDescription'
  and store_id = 39
  and user_id = 0
  and calendar_date = '2014-09-10'
  and source = 'showroom'
  and vehicle_model = 'Suburban'
  and vehicle_status = 'used'
  


  
DROP TABLE IF EXISTS dc.tmp_rpt_appointment;
CREATE TABLE IF NOT EXISTS dc.tmp_rpt_appointment(
    date_report_ran TIMESTAMPTZ,
    entity CITEXT,
    store_id INTEGER,
    user_id INTEGER,
    calendar_date DATE,
    source CITEXT,
    team_name CITEXT,
    user_name CITEXT,
    store_name CITEXT,
    user_type CITEXT,
    user_key CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    vehicle_status CITEXT,
    unique_set BIGINT,
    total_set BIGINT,
    confirmed_appointments BIGINT,
    unique_scheduled BIGINT,
    total_scheduled BIGINT,
    total_scheduled_w_video BIGINT,
    total_scheduled_w_no_video BIGINT,
    total_attended BIGINT,
    video_day_appointment BIGINT,
    video_show BIGINT,
    no_video_show BIGINT,
    total_visits BIGINT,
    total_beback BIGINT,
    total_show BIGINT,
    total_delivered BIGINT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);


DROP TABLE IF EXISTS dc.xfm_rpt_appointment;
CREATE TABLE IF NOT EXISTS dc.xfm_rpt_appointment(
    date_report_ran TIMESTAMPTZ,
    entity CITEXT,
    store_id INTEGER,
    user_id INTEGER,
    calendar_date DATE,
    source CITEXT,
    team_name CITEXT,
    user_name CITEXT,
    store_name CITEXT,
    user_type CITEXT,
    user_key CITEXT,
    vehicle_make CITEXT,
    vehicle_model CITEXT,
    vehicle_status CITEXT,
    unique_set BIGINT,
    total_set BIGINT,
    confirmed_appointments BIGINT,
    unique_scheduled BIGINT,
    total_scheduled BIGINT,
    total_scheduled_w_video BIGINT,
    total_scheduled_w_no_video BIGINT,
    total_attended BIGINT,
    video_day_appointment BIGINT,
    video_show BIGINT,
    no_video_show BIGINT,
    total_visits BIGINT,
    total_beback BIGINT,
    total_show BIGINT,
    total_delivered BIGINT,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash text)
WITH (OIDS=FALSE);