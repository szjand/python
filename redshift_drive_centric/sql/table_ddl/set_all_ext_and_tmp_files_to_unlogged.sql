﻿SELECT 'ALTER TABLE ' || table_schema || '.' || table_name || ' SET UNLOGGED;'
-- select *
FROM information_schema.tables
WHERE table_schema = 'dc'
  and table_name like 'tmp_rpt_%' or table_name like 'ext_rpt_%'
ORDER BY table_name;


ALTER TABLE dc.ext_rpt_appointment SET UNLOGGED;
ALTER TABLE dc.ext_rpt_dailyactivity SET UNLOGGED;
ALTER TABLE dc.ext_rpt_delivery SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_after_delivery SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_dailyactivity SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_days_to_conversion SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_days_to_delivered SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_engaged_visit SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_lead_engaged SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_lead_summary SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_phone SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_pipeline_health SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_response_time SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_source SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_visit SET UNLOGGED;
ALTER TABLE dc.ext_rpt_enhanced_visit_delivered SET UNLOGGED;
ALTER TABLE dc.ext_rpt_lead SET UNLOGGED;
ALTER TABLE dc.ext_rpt_phone SET UNLOGGED;
ALTER TABLE dc.ext_rpt_popup SET UNLOGGED;
ALTER TABLE dc.ext_rpt_visit SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_appointment SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_dailyactivity SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_delivery SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_after_delivery SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_dailyactivity SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_days_to_conversion SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_days_to_delivered SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_engaged_visit SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_lead_engaged SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_lead_summary SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_phone SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_pipeline_health SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_response_time SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_source SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_visit SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_enhanced_visit_delivered SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_lead SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_phone SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_popup SET UNLOGGED;
ALTER TABLE dc.tmp_rpt_visit SET UNLOGGED;


SELECT 'ALTER TABLE ' || table_schema || '.' || table_name || ' SET UNLOGGED;'
-- select *
FROM information_schema.tables
WHERE table_schema = 'dc'
  and table_name like 'tmp_dw_%' or table_name like 'ext_dw_%'
ORDER BY table_name;


ALTER TABLE dc.ext_dw_dim_customer SET UNLOGGED;
ALTER TABLE dc.ext_dw_dim_date SET UNLOGGED;
ALTER TABLE dc.ext_dw_dim_store SET UNLOGGED;
ALTER TABLE dc.ext_dw_dim_user SET UNLOGGED;
ALTER TABLE dc.ext_dw_fact_closed_ro SET UNLOGGED;
ALTER TABLE dc.ext_dw_fact_customer_communication SET UNLOGGED;
ALTER TABLE dc.ext_dw_fact_deal SET UNLOGGED;
ALTER TABLE dc.ext_dw_fact_market_scan SET UNLOGGED;
ALTER TABLE dc.ext_dw_fact_notes SET UNLOGGED;
ALTER TABLE dc.ext_dw_fact_phone_call SET UNLOGGED;
ALTER TABLE dc.ext_dw_fact_sales_appointments SET UNLOGGED;
ALTER TABLE dc.ext_dw_fact_service_appointments SET UNLOGGED;
ALTER TABLE dc.ext_dw_fact_store_app SET UNLOGGED;
ALTER TABLE dc.tmp_dw_dim_customer SET UNLOGGED;
ALTER TABLE dc.tmp_dw_dim_date SET UNLOGGED;
ALTER TABLE dc.tmp_dw_dim_store SET UNLOGGED;
ALTER TABLE dc.tmp_dw_dim_user SET UNLOGGED;
ALTER TABLE dc.tmp_dw_fact_closed_ro SET UNLOGGED;
ALTER TABLE dc.tmp_dw_fact_customer_communication SET UNLOGGED;
ALTER TABLE dc.tmp_dw_fact_deal SET UNLOGGED;
ALTER TABLE dc.tmp_dw_fact_market_scan SET UNLOGGED;
ALTER TABLE dc.tmp_dw_fact_notes SET UNLOGGED;
ALTER TABLE dc.tmp_dw_fact_phone_call SET UNLOGGED;
ALTER TABLE dc.tmp_dw_fact_sales_appointments SET UNLOGGED;
ALTER TABLE dc.tmp_dw_fact_service_appointments SET UNLOGGED;
ALTER TABLE dc.tmp_dw_fact_store_app SET UNLOGGED;




SELECT 'ALTER TABLE ' || table_schema || '.' || table_name || ' SET UNLOGGED;'
-- select *
FROM information_schema.tables
WHERE table_schema = 'dc'
  and table_name like 'tmp_dw_%' or table_name like 'ext_dw_%'
ORDER BY table_name;


SELECT substring(table_name, 5, 50)||'(csv_path + '||''''||substring(table_name, 5, 50)||'\\'||table_name||'.csv'||''''||
	','||''''||'rydell.'||substring(table_name, 5, 50)||''''||')'
FROM information_schema.tables
WHERE table_schema = 'dc'
  and table_name like 'ext_dw_%' or table_name like 'ext_rpt_%'
order by substring(table_name, 5, 50)  































