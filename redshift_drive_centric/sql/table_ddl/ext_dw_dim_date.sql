﻿DROP TABLE IF EXISTS dc.ext_dw_dim_date;
CREATE TABLE IF NOT EXISTS dc.ext_dw_dim_date(
    date_day DATE,
    prior_date_day DATE,
    next_date_day DATE,
    prior_year_date_day DATE,
    prior_year_over_year_date_day DATE,
    day_of_week INTEGER,
    day_of_week_name CITEXT,
    day_of_week_name_short CITEXT,
    day_of_month INTEGER,
    day_of_year INTEGER,
    week_start_date DATE,
    week_end_date DATE,
    week_of_year INTEGER,
    prior_year_week_start_date DATE,
    prior_year_week_end_date DATE,
    prior_year_week_of_year INTEGER,
    month_of_year INTEGER,
    month_name CITEXT,
    month_name_short CITEXT,
    month_start_date DATE,
    month_end_date DATE,
    prior_year_month_start_date DATE,
    prior_year_month_end_date DATE,
    quarter_of_year INTEGER,
    quarter_start_date DATE,
    quarter_end_date DATE,
    year_number INTEGER,
    year_start_date DATE,
    year_end_date DATE)
WITH (OIDS=FALSE);
-- {date_day,prior_date_day,next_date_day,prior_year_date_day,prior_year_over_year_date_day,day_of_week,day_of_week_name,day_of_week_name_short,day_of_month,day_of_year,week_start_date,week_end_date,week_of_year,prior_year_week_start_date,prior_year_week_end_date,prior_year_week_of_year,month_of_year,month_name,month_name_short,month_start_date,month_end_date,prior_year_month_start_date,prior_year_month_end_date,quarter_of_year,quarter_start_date,quarter_end_date,year_number,year_start_date,year_end_date}

create unique index on dc.ext_dw_dim_date(date_day);

select date_day
from dc.ext_dw_dim_date
group by date_day
having count(*) > 1;

ALTER TABLE dc.xfm_dw_dim_date ADD PRIMARY KEY (date_day,row_thru_date);
ALTER TABLE dc.xfm_dw_dim_date add constraint thru_gt_from check (row_thru_date > row_from_date);
drop table dc.tmp_dw_dim_date;
create unlogged table dc.tmp_dw_dim_date_changed_rows (date_day date);


DROP TABLE IF EXISTS dc.xfm_dw_dim_date;
CREATE TABLE IF NOT EXISTS dc.xfm_dw_dim_date(
    date_day DATE,
    prior_date_day DATE,
    next_date_day DATE,
    prior_year_date_day DATE,
    prior_year_over_year_date_day DATE,
    day_of_week INTEGER,
    day_of_week_name CITEXT,
    day_of_week_name_short CITEXT,
    day_of_month INTEGER,
    day_of_year INTEGER,
    week_start_date DATE,
    week_end_date DATE,
    week_of_year INTEGER,
    prior_year_week_start_date DATE,
    prior_year_week_end_date DATE,
    prior_year_week_of_year INTEGER,
    month_of_year INTEGER,
    month_name CITEXT,
    month_name_short CITEXT,
    month_start_date DATE,
    month_end_date DATE,
    prior_year_month_start_date DATE,
    prior_year_month_end_date DATE,
    quarter_of_year INTEGER,
    quarter_start_date DATE,
    quarter_end_date DATE,
    year_number INTEGER,
    year_start_date DATE,
    year_end_date DATE,
    row_from_date date,
    row_thru_date date default '12/31/9999',
    hash citext)
WITH (OIDS=FALSE);