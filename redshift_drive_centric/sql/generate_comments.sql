﻿
-- generate some table comments

SELECT 'comment on table '||schemaname||'.'||relname||' is '||''''||'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches'||''''||';'
FROM pg_stat_user_tables 
where schemaname = 'dc' 
  and relname like 'ext_dw%'
  and relname <> 'ext_localstore'
ORDER BY relname;

comment on table dc.ext_dw_dim_customer is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_dim_date is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_dim_store is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_dim_user is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_fact_customer_communication is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_fact_deal is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_fact_market_scan is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_fact_notes is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_fact_phone_call is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_fact_sales_appointments is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_fact_service_appointments is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.ext_dw_fact_store_app is 'scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';


SELECT 'comment on table '||schemaname||'.'||relname||' is '||''''||'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches'||''''||';'
FROM pg_stat_user_tables 
where schemaname = 'dc' 
  and relname like 'tmp_dw%'
ORDER BY relname;

comment on table dc.tmp_dw_dim_customer_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_dim_date_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_dim_store_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_dim_user_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_fact_customer_communication_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_fact_deal_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_fact_market_scan_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_fact_notes_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_fact_phone_call_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_fact_sales_appointments_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_fact_service_appointments_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';
comment on table dc.tmp_dw_fact_store_app_changed_rows is 'changed rows generated daily from daily scrape of redshift dw tables, used to generate change data capture tables (xfm),  currently being done locally by jon due to VPN headaches';


SELECT 'comment on table '||schemaname||'.'||relname||' is '||''''||'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches'||''''||';'
FROM pg_stat_user_tables 
where schemaname = 'dc' 
  and relname like 'xfm_dw%'
ORDER BY relname;

comment on table dc.xfm_dw_dim_customer is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables';
comment on table dc.xfm_dw_dim_date is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_dim_store is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_dim_user is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_fact_customer_communication is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_fact_deal is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_fact_market_scan is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_fact_notes is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_fact_phone_call is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_fact_sales_appointments is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_fact_service_appointments is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';
comment on table dc.xfm_dw_fact_store_app is 'type 2 change data capture tables, grain = day, generated daily from daily scrape of redshift dw tables, currently being done locally by jon due to VPN headaches';





-- functions

SELECT 'comment on function '||n.nspname||'.'||p.proname||'() is '||''''||'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches'||''''||';'
-- select n.nspname as function_schema,
--        p.proname as function_name,
--        l.lanname as function_language,
--        case when l.lanname = 'internal' then p.prosrc
--             else pg_get_functiondef(p.oid)
--             end as definition,
--        pg_get_function_arguments(p.oid) as function_arguments,
--        t.typname as return_type
from pg_proc p
left join pg_namespace n on p.pronamespace = n.oid
left join pg_language l on p.prolang = l.oid
left join pg_type t on t.oid = p.prorettype 
where n.nspname not in ('pg_catalog', 'information_schema')
  and n.nspname = 'dc';

comment on function dc.xfm_dw_dim_date() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_fact_market_scan() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_fact_deal() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_fact_notes() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_fact_phone_call() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_fact_customer_communication() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_fact_sales_appointments() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_fact_service_appointments() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_fact_store_app() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_dim_store() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_dim_user() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';
comment on function dc.xfm_dw_dim_customer() is 'inserts new rows in the xfm table, populates the changed_rows table and manages the changed rows in the xfm table, currently being done locally by jon due to VPN headaches';



         