# encoding=utf-8
import db_cnx
import requests
# eventually this should all point to the ops server: 10.130.196.88, i think?
# rather than a schema in production/localhost
# 4/25/16 rethinking this, actually more comfortable with ops being a schema in the production database
#   rather than a separate server, for now at least
# datetime should come from db server, not client


def log_start(_task):
    with db_cnx.pg() as con:
        with con.cursor() as cur:
            cur.execute("""select current_date""")
            run_date = cur.fetchone()[0]
            cur.execute("""select current_timestamp""")
            run_start_ts = cur.fetchone()[0]
            sql = """
              insert into ops.task_log (task,run_date,run_start_ts,run_status)
              values('%s','%s','%s','%s')
            """ % (_task, run_date, run_start_ts, 'Started')
            cur.execute(sql)
            sql = """
                select run_id
                from ops.task_log
                where task = '%s'
                  and run_start_ts = '%s'
                  and run_status = 'Started'
            """ % (_task, run_start_ts)
            cur.execute(sql)
            return cur.fetchone()[0]


def log_pass(_run_id, _message='Pass'):
    with db_cnx.pg() as con:
        with con.cursor() as cur:
            cur.execute("""select current_timestamp""")
            run_complete_ts = cur.fetchone()[0]
            sql = """
                update ops.task_log
                set run_status = 'Finished',
                    message = '%s',
                    run_complete_ts = '%s'
                where run_id = '%s'
            """ % (_message, run_complete_ts, _run_id)
            cur.execute(sql)


def log_error(_run_id, _message):
    with db_cnx.pg() as con:
        with con.cursor() as cur:
            sql = """
                update ops.task_log
                set run_status = 'Error',
                    message = '%s'
                where run_id = '%s'
            """ % (_message, _run_id)
            cur.execute(sql)


def dependency_check(_task):
    # any predecessor for this task that does not have a log entry for the_date
    # where run_complete_ts is not null and is Finshed/Pass
    with db_cnx.pg() as con:
        with con.cursor() as cur:
            cur.execute("""select current_date""")
            run_date = cur.fetchone()[0]
            sql = """
                select count(*)
                from ops.task_dependencies a
                where a.successor = '%s'
                  and not exists (
                    select 1
                    from ops.task_log
                    where task = a.predecessor
                      and run_date = '%s'
                      and run_complete_ts is not null
                      and run_status = 'Finished')
            """ % (_task, run_date)
            cur.execute(sql)
            return cur.fetchone()[0]


def log_dependency_failure(_run_id):
    with db_cnx.pg() as con:
        with con.cursor() as cur:
            cur.execute("""select current_timestamp""")
            run_complete_ts = cur.fetchone()[0]
            sql = """
                update ops.task_log
                set run_status = 'Error',
                    message = 'Failed Dependency Check',
                    run_complete_ts = '%s'
                where run_id = '%s'
            """ % (run_complete_ts, _run_id)
            cur.execute(sql)


def email_error(_task, _run_id, _error):
    return requests.post(
        "https://api.mailgun.net/v3/notify.cartiva.com/messages",
        auth=("api", "23540ba98150b9a975b02adcef355bd1-059e099e-0d759d36"),
        data={"from": "jandrews@cartiva.com <jon@notify.cartiva.com>",
              "to": ['jandrews@cartiva.com'],
              "subject": "Failed task: " + _task,
              "text": _error})

def luigi_log_start(_task):
    """
        in luigi, don't need to return the run_id, just log the task as having started
    """
    with db_cnx.pg_jon_localhost() as con:
        with con.cursor() as cur:
            cur.execute("""select current_date""")
            run_date = cur.fetchone()[0]
            cur.execute("""select current_timestamp""")
            run_start_ts = cur.fetchone()[0]
            sql = """
              insert into ops.luigi_task_log (task,run_date,run_start_ts,run_status)
              values('%s','%s','%s','%s')
            """ % (_task, run_date, run_start_ts, 'Started')
            cur.execute(sql)
            sql = """
                select run_id
                from ops.luigi_task_log
                where task = '%s'
                  and run_start_ts = '%s'
                  and run_status = 'Started'
            """ % (_task, run_start_ts)
            cur.execute(sql)
            return cur.fetchone()[0]


def luigi_log_error(_run_id, _message):
    with db_cnx.pg_jon_localhost() as con:
        with con.cursor() as cur:
            sql = """
                update ops.luigi_task_log
                set run_status = 'Error',
                    message = '%s'
                where run_id = '%s'
            """ % (_message, _run_id)
            cur.execute(sql)


def luigi_log_pass(_run_id, _message='Pass'):
    with db_cnx.pg_jon_localhost() as con:
        with con.cursor() as cur:
            cur.execute("""select current_timestamp""")
            run_complete_ts = cur.fetchone()[0]
            sql = """
                update ops.luigi_task_log
                set run_status = 'Finished',
                    message = '%s',
                    run_complete_ts = '%s'
                where run_id = '%s'
            """ % (_message, run_complete_ts, _run_id)
            cur.execute(sql)
