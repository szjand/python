import psycopg2
import pyodbc
import pypyodbc

"""
# 3/25/16
# this is WAY minimal for now
# the immediate problem was this:
#     i need to develop and test scpp against local host
#     but to do that i had to change the connection string for pg in all files
#     for every connection.  pain in the ass, and messy
#     this way, i change the pg connection string in one place, always refer
#     to pg connection as db_cnx.pg() and it's all good
#
# pyodbc seems like the best way for connection to ads, adsdb is a bit buggy, pyodbc supports python context manager
# http://stackoverflow.com/questions/33532699/how-can-i-connect-to-local-advantage-database-using-pyodbc-in-python
# http://mkleehammer.github.io/pyodbc/api.html
#
# added cartiva_py_lib to the project structure of the different projects, rather than
# relying on the files being in C:\Python27\Lib\plat-win, that will work for deploy but not for dev
# i had edited (added connections) via the individual projects, which resulted in the file in plat-win being
# modified but not in the cartiva_py_lib project
# let's see if i got the sharing right across projects, yep, looks good

6/1/17
  added new_pg(server) as i work toward deploying luigi to production
  specifically, at first, for the ads_for_luigi project that has to run on windows (.22)
  
1/1/22
    add ads ukg  
"""


# production ####################################################

def dc_redshift():
    return psycopg2.connect("host='10.25.84.126' dbname='rpt' port='5439' user='rydell' password='r8!SYU8*BHmHw@vZ'")

def pg():
    return psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")


def new_pg(server):
    if server == 'local':
        return psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")
    elif server == '173':
        return psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
    elif server == '174':
        return psycopg2.connect("host='10.130.196.174' dbname='cartiva' user='postgres' password='cartiva'")
    elif server == '88':
        return psycopg2.connect("host='10.130.196.88' dbname='cartiva' user='rydell' password='cartiva'")
    elif server == '73':
        return psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'")
    elif server == '139':  # jon's local vm for luigi dev
        return psycopg2.connect("host='192.168.43.139' dbname='Cartiva' user='postgres' password='cartiva'")


# connect from windows VM (w10_python) to pg instance on linux VM (ubuntu_no_env)


def pg_ubuntu_no_env():
    return psycopg2.connect("host='192.168.43.139' dbname='cartiva' user='postgres' password='cartiva'")


def ops():
    return psycopg2.connect("host='10.130.196.88' dbname='cartiva' user='rydell' password='cartiva'")


def pg_jon_localhost():
    return psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")


# Win7 Apache PHP Postgres VM local host
def win_local_pg():
    return psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")


def ads_sco():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\10.130.196.252:6363\\advantage\\scotest\\sco.add;UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')


def ads_dds():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\10.130.196.252:6363\\advantage\\dds\\dds.add;UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')


def ads_dps():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\10.130.196.252:6363\\advantage\\dpsVSeries\\dpsVSeries.add;'
                          'UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')


def ads_ukg():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\10.130.196.252:6363\\advantage\\ukg\\ukg.add;'
                          'UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')


def arkona_report():
    # return pyodbc.connect('DRIVER={iSeries Access ODBC Driver};system=REPORT2.ARKONA.COM;uid=rydejon;pwd=fuckyou5')
    return pypyodbc.connect(
        'DRIVER={iSeries Access ODBC Driver};system=REPORT1.ARKONA.COM;uid=rydejon;pwd=fuckyou5')


def dealertrack_report2():
    return pyodbc.connect('DRIVER={iSeries Access ODBC Driver};'
                          'system=REPORT2.DMS.DEALERTRACK.COM;uid=rydejon;pwd=fuckyou5')


def dealertrack_report1():
    return pyodbc.connect('DRIVER={iSeries Access ODBC Driver};'
                          'system=REPORT1.DMS.DEALERTRACK.COM;uid=rydejon;pwd=fuckyou5')


def postgres_ubuntu():
    return psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'")


def ads_dpsvseries():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\10.130.196.252:6363\\advantage\\dpsvseries\\dpsvseries.add;'
                          'UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')


def mysql_shoretel_config():
    return pyodbc.connect("Provider=MSDASQL; DRIVER={MySQL ODBC 5.2a Driver};SERVER=192.168.100.10; "
                          "Port=4308;DATABASE=shoreware;USER=st_configread;PASSWORD=passwordconfigread;OPTION=3;")


def arkona_production():
    # return pyodbc.connect('DRIVER={iSeries Access ODBC Driver};system=RYDELL.ARKONA.COM;uid=rydejon;pwd=fuckyou5')
    return pypyodbc.connect(
        'DRIVER={iSeries Access ODBC Driver};system=RYDELL.ARKONA.COM;uid=rydejon;pwd=fuckyou5')


def ads_service_scheduler():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\10.130.196.83:2001\\advantage\\rydellservice'
                          '\\rydellservice.add;UID=adssys;PWD=cartiva22;'
                          'TrimTrailingSpaces=True')


def ads_service_scheduler_honda():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\10.130.190.63:6363\\Advantage\\NewHondaService\\NewHondaService.add;'
                          'UID=adssys;PWD=cartiva22;'
                          'TrimTrailingSpaces=True')


def drive_centric():
    return pyodbc.connect('Driver={SQL Server}; Server=52.22.117.38;Database=leadcrumb_daily_copy;'
                          'uid=store-access;pwd=h%Mf4Ze5L#yQ9n*')


# def keyper():
#     return pyodbc.connect('Driver={SQL Server}; Server=10.134.196.201,1433\\keyperserver;Database=KeyperData;'
#                           'uid=sa;pwd=Key539737')


# this is the connection string for the new keyper server, installed 5/25/21
def keyper():
    return pyodbc.connect('Driver={SQL Server}; Server=10.134.196.200,1433\keyperserver;Database=KeyperData;'
                          'uid=sa;pwd=Key539737')

def ads_bs():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\10.130.196.252:6363\\advantage\\BodyShop\\BodyShop.add;'
                          'UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')
