# encoding=utf-8
import db_cnx
from openpyxl import Workbook

"""
47 workbooks or 1 workbook with 94 worksheets?
go with 47

no need to create tables, all the data is in jeri.fte_census_data

for the weekly report, just set the week_id in the first query

a copy of base_40_history.py
on 08/11/20 jeri asked for regenerated weekly fte reports for april thru the current date
data is generated with fte_orig_40_weekly_rerun_20200812.sql
changes made to base py file:
    created _week_id variable
    changed wb_1_file_name to save all the files to a specific sub-directory2
"""
# create the 2 new fte tables
_week_id = 866
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        sql = """
            select distinct week_id, 
             'fte_orig_40_' || (select lpad(extract(month from from_date)::text, 2, '0')) 
                  || '-' || (select lpad(extract(day from from_date)::text, 2, '0')) 
                  || '-' || (select right(extract(year from from_date)::text, 2))
            from jeri.fte_weeks
            where week_id = {};       
        """.format(_week_id)
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            week_id = row[0]
            table_name = row[1]

            # variables for worksheet names
            ry1_ws = table_name + '_ry1'
            ry2_ws = table_name + '_ry2'
            # variables for spreadsheet (workbook) names
            wb_1_file_name = 'rerun_20200812/' + table_name + '_rerun_20200812.xlsx'

            wb_1 = Workbook()
            ws1 = wb_1.active
            ws1.title = ry1_ws

            # ry1
            store = 'RY1'
            ws1.append(["store", "emp #", "name", "dist code", "hire date", "term date",
                        "payroll class", "full/part", "clock_hours", "fte"])
            sql = """
                select jeri.fte_orig_40_get_spreadsheet_table_rerun_20200812('{0}',{1})
            """.format(store, week_id)
            pg_cur.execute(sql)
            sql = """
                select * from jeri.fte_tmp order by name;
            """
            pg_cur.execute(sql)
            for row_1 in pg_cur.fetchall():
                ws1.append(row_1)
            sql = """
                select null,null,null,null,null,null,null,null,sum(clock_hours),sum(fte)
                from jeri.fte_tmp;
            """
            pg_cur.execute(sql)
            for row_2 in pg_cur.fetchall():
                ws1.append(row_2)

            # ry2 scenario 1
            ws2 = wb_1.create_sheet(title=ry2_ws)
            store = 'RY2'
            ws2.append(["store", "emp #", "name", "dist code", "hire date", "term date",
                        "payroll class", "full/part", "clock_hours", "fte"])
            sql = """
                select jeri.fte_orig_40_get_spreadsheet_table_rerun_20200812('{0}',{1})
            """.format(store, week_id)
            pg_cur.execute(sql)
            sql = """
                select * from jeri.fte_tmp order by name;
            """
            pg_cur.execute(sql)
            for row_3 in pg_cur.fetchall():
                ws2.append(row_3)
            sql = """
                select null,null,null,null,null,null,null,null,sum(clock_hours),sum(fte)
                from jeri.fte_tmp;
            """
            pg_cur.execute(sql)
            for row_4 in pg_cur.fetchall():
                ws2.append(row_4)

            wb_1.save(filename=wb_1_file_name)
