﻿do $$
declare
  cp_from_date date := '04/15/2020';
  cp_thru_date date := '07/10/2020';
  lb_from_date date := '01/10/2020';
  lb_thru_date date := '03/31/2020';
  cp_weeks_B integer := 14;
  cp_denominator_S integer := 12;
  lb_denominator integer := 12;
begin
  drop table if exists wage_comparison;
  create temp table wage_comparison as
    
  select aa.*, 
    bb.lb_gross_pay, bb.lb_gross_pay_with_cap, bb.lb_annualized_salary, 
    bb.lb_annualized_salary_with_cap, bb.lb_total_hours, 
    bb.lb_avg_hourly_wage,
    cc.cp_gross_pay, cc.cp_gross_pay_with_cap, cc.cp_annualized_salary, 
    cc.cp_annualized_salary_with_cap, cc.cp_total_hours, 
    cc.cp_avg_hourly_wage,
    round(
      case
        when bb.lb_avg_hourly_wage is null or cc.cp_avg_hourly_wage is null then null
        else cc.cp_avg_hourly_wage / bb.lb_avg_hourly_wage
      end, 2) as wage_comparison,
    dd.check_date
  from ppp.employees_wage_comparison aa
  left join ( -- lookback, replace 25000 with 23076.92
    select count(*),a.employee_number, 
      sum(b.total_gross_pay) as lb_gross_pay,  --G
      case when sum(b.total_gross_pay) > 23076.92 then 23076.92 else sum(b.total_gross_pay) end as lb_gross_pay_with_cap, --H
      sum(b.total_gross_pay) * 4 as lb_annualized_salary, -- I 
      4 * (case when sum(b.total_gross_pay) > 23076.92 then 23076.92 else sum(b.total_gross_pay) end) as lb_annualized_salary_with_cap,  --J
      round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as lb_total_hours, --K,
      round(
        case
          when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0
            then null
          else
            (case when sum(b.total_gross_pay) > 23076.92 then 23076.92 else sum(b.total_gross_pay) end)
            /
            sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
        end, 2) as lb_avg_hourly_wage -- L  H/K  
    -- select sum(b.total_gross_pay) 
    from ppp.employees_wage_comparison a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      And (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between lb_from_date and lb_thru_date
    group by a.employee_number) bb on aa.employee_number = bb.employee_number
   left join ( -- covered period 
     select a.employee_number,
      sum(b.total_gross_pay) as cp_gross_pay, -- M
      case
        when a.pay_period = 'B' then
          case
            when sum(b.total_gross_pay) > (cp_weeks_B * 1923.08) then (cp_weeks_B * 1923.08)
            else sum(b.total_gross_pay)
          end
        when a.pay_period like '%S%' then
          case 
            when sum(b.total_gross_pay) > 25000 then 25000
            else sum(b.total_gross_pay)
          end
      end as cp_gross_pay_with_cap, -- N
      case
        when a.pay_period = 'B' then round(52 * (sum(b.total_gross_pay)/cp_weeks_B), 2) 
        when a.pay_period like '%S%' then round(12 * sum(b.total_gross_pay)/3, 2)
      end as cp_annualized_salary, -- O 
      case
        when 52 * (sum(b.total_gross_pay)/cp_weeks_B) > 100000 then 100000
        else round(52 * (sum(b.total_gross_pay)/cp_weeks_B), 2)
      end as cp_annualized_salary_with_cap,  -- P   
      round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as cp_total_hours, -- Q      
      round(
        case
          when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0
            then null
          else
            case 
              when a.pay_period = 'B' then
                (case when sum(b.total_gross_pay) > (cp_weeks_B * 1923.08) then (cp_weeks_B * 1923.08) else sum(b.total_gross_pay) end)
                /
                sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
              when a.pay_period like '%S%' then
                (case  when sum(b.total_gross_pay) > 25000 then 25000 else sum(b.total_gross_pay) end)
                /
                sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
            end
        end, 2) as cp_avg_hourly_wage -- R: N/Q  cp_gross_pay_with_cap/total_hours
    from ppp.employees_wage_comparison a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      And (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between cp_from_date and cp_thru_date
    group by a.employee_number) cc on aa.employee_number = cc.employee_number
  left join (  -- check dates annualized > 100000
    select employee_number, employee_name, min(check_date) as check_date
    from (-- yep, these are the folks who had at least one check that annualized for > 100K and the first date it happened
      select min(check_date) as check_date, employee_number, employee_name
      from (  -- all checks that annualize > 100K
        select distinct a.employee_number, a.employee_name,
        (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date
        from ppp.employees_wage_comparison a
        left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
          And check_year = 19
        where b.total_gross_pay * 26 > 100000) aa 
      group by employee_number, employee_name  
      union
      -- now i need any other employees who totaled > 100K in 2019
      select '12/31/2019'::date, a.employee_number, a.employee_name
      from ppp.employees_wage_comparison a
      left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
        And check_year = 19
      group by a.employee_number, a.employee_name
      having sum(total_gross_pay) > 100000) cc
    group by employee_number, employee_name) dd on aa.employee_number = dd.employee_number;

end $$;

select * from wage_comparison order by store, employee_name;