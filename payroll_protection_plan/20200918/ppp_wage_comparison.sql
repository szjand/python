﻿/*
09/20/20 this entire script is to be run

09/30/20
  from jeri:
    On the Wage Comparison report—for the lookback period, will you change this to a cap of $25,000 
    for monthly and leave the semi-monthly at the $23076.92.  Thank you.

    Also, for the last 4 of the social in each of the reports, the zeros are being dropped.  

    Jeri


  

*/
drop table if exists ppp.employees_wage_comparison cascade;
create table ppp.employees_wage_comparison (
  employee_number citext not null primary key,
  store citext,
  employee_name citext,
  ssn citext,
  payroll_class citext,
  pay_period citext,
  hire_date date,
  term_date date);


insert into ppp.employees_wage_comparison  
select aa.employee_, 
  case
    when left(employee_, 1) = '1' then 'RY1'
    else 'RY2'
  end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
  select employee_, array_agg(distinct pay_period) as pay_period, array_agg(distinct payroll_class) as payroll_class
  from (
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      to_date(
        check_year::text || case when length(check_month::text) = 1 then '0'
        ||check_month::text else check_month::text end 
        ||case when length(check_day::text) = 1 then '0'
        ||check_day::text else check_day::text end, 'YYMMDD') as check_date  
    from arkona.ext_pyhshdta
    where to_date(
        check_year::text || case when length(check_month::text) = 1 then '0'
        ||check_month::text else check_month::text end 
        ||case when length(check_day::text) = 1 then '0'
        ||check_day::text else check_day::text end, 'YYMMDD')
      between '01/10/2020'::date and '03/31/2020'::date
    union all
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      to_date(
        check_year::text || case when length(check_month::text) = 1 then '0'
        ||check_month::text else check_month::text end 
        ||case when length(check_day::text) = 1 then '0'
        ||check_day::text else check_day::text end, 'YYMMDD') as check_date  
    from arkona.ext_pyhshdta
    where to_date(
        check_year::text || case when length(check_month::text) = 1 then '0'
        ||check_month::text else check_month::text end 
        ||case when length(check_day::text) = 1 then '0'
        ||check_day::text else check_day::text end, 'YYMMDD')
      between '04/15/2020'::date and '07/10/2020'::date    
     ) x group by employee_) aa
left join arkona.ext_pymast bb on aa.employee_ = bb.pymast_employee_number     
left join jon.ssn cc on aa.employee_ = cc.employee_number;
delete from ppp.employees_wage_comparison where employee_number = '1112425';  -- delete jim price




do $$
declare
  cp_from_date date := '04/15/2020';
  cp_thru_date date := '07/10/2020';
  lb_from_date date := '01/10/2020';
  lb_thru_date date := '03/31/2020';
  cp_weeks_B integer := 14;
begin
  drop table if exists wage_comparison;
  create temp table wage_comparison as
    
  select aa.*, 
    bb.lb_gross_pay, bb.lb_gross_pay_with_cap, bb.lb_annualized_salary, 
    bb.lb_annualized_salary_with_cap, bb.lb_total_hours, 
    bb.lb_avg_hourly_wage,
    cc.cp_gross_pay, cc.cp_gross_pay_with_cap, cc.cp_annualized_salary, 
    cc.cp_annualized_salary_with_cap, cc.cp_total_hours, 
    cc.cp_avg_hourly_wage,
    round(
      case
        when bb.lb_avg_hourly_wage is null or cc.cp_avg_hourly_wage is null then null
        else cc.cp_avg_hourly_wage / bb.lb_avg_hourly_wage
      end, 2) as wage_comparison,
    dd.check_date
  from ppp.employees_wage_comparison aa
  left join ( -- lookback, S: 25000 b: 23076.92
    select count(*),a.employee_number, 
      sum(b.total_gross_pay) as lb_gross_pay,  --G
      case
        when a.pay_period = 'B' then 
          case 
            when sum(b.total_gross_pay) > 23076.92 then 23076.92 
            else sum(b.total_gross_pay) 
          end
        when a.pay_period like '%S%' then
          case 
            when sum(b.total_gross_pay) > 25000 then 25000
            else sum(b.total_gross_pay) 
          end        
      end as lb_gross_pay_with_cap, --H
      sum(b.total_gross_pay) * 4 as lb_annualized_salary, -- I      
--       4 * (case when sum(b.total_gross_pay) > 23076.92 then 23076.92 else sum(b.total_gross_pay) end) as lb_annualized_salary_with_cap,  --J
      case
        when a.pay_period = 'B' then 
          case
            when sum(b.total_gross_pay) > 23076.92 then 4 * 23076.92
            else 4 * sum(b.total_gross_pay)
          end
        when a.pay_period like '%S%' then
          case
            when sum(b.total_gross_pay) > 25000 then 4 * 25000
            else 4 * sum(b.total_gross_pay)
          end
      end as lb_annualized_salary_with_cap,  --J
      round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as lb_total_hours, --K,
      
      round(
        case
          when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0
            then null
          else
            case
              when a.pay_period = 'B' then 
                (case when sum(b.total_gross_pay) > 23076.92 then 23076.92 else sum(b.total_gross_pay) end)
                /
                sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
              when a.pay_period like '%S%' then 
                (case when sum(b.total_gross_pay) > 25000 then 25000 else sum(b.total_gross_pay) end)
                /
                sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
            end              
        end, 2) as lb_avg_hourly_wage -- L  H/K  
    -- select sum(b.total_gross_pay) 
    from ppp.employees_wage_comparison a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      And (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between lb_from_date and lb_thru_date
    group by a.employee_number) bb on aa.employee_number = bb.employee_number
   left join ( -- covered period 
     select a.employee_number,
      sum(b.total_gross_pay) as cp_gross_pay, -- M
      case
        when a.pay_period = 'B' then
          case
            when sum(b.total_gross_pay) > (cp_weeks_B * 1923.08) then (cp_weeks_B * 1923.08)
            else sum(b.total_gross_pay)
          end
        when a.pay_period like '%S%' then
          case 
            when sum(b.total_gross_pay) > 25000 then 25000
            else sum(b.total_gross_pay)
          end
      end as cp_gross_pay_with_cap, -- N
      case
        when a.pay_period = 'B' then round(52 * (sum(b.total_gross_pay)/cp_weeks_B), 2) 
        when a.pay_period like '%S%' then round(12 * sum(b.total_gross_pay)/3, 2)
      end as cp_annualized_salary, -- O 
      case
        when 52 * (sum(b.total_gross_pay)/cp_weeks_B) > 100000 then 100000
        else round(52 * (sum(b.total_gross_pay)/cp_weeks_B), 2)
      end as cp_annualized_salary_with_cap,  -- P   
      round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as cp_total_hours, -- Q      
      round(
        case
          when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0
            then null
          else
            case 
              when a.pay_period = 'B' then
                (case when sum(b.total_gross_pay) > (cp_weeks_B * 1923.08) then (cp_weeks_B * 1923.08) else sum(b.total_gross_pay) end)
                /
                sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
              when a.pay_period like '%S%' then
                (case  when sum(b.total_gross_pay) > 25000 then 25000 else sum(b.total_gross_pay) end)
                /
                sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
            end
        end, 2) as cp_avg_hourly_wage -- R: N/Q  cp_gross_pay_with_cap/total_hours
    from ppp.employees_wage_comparison a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      And (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between cp_from_date and cp_thru_date
    group by a.employee_number) cc on aa.employee_number = cc.employee_number
  left join (  -- check dates annualized > 100000
    select employee_number, employee_name, min(check_date) as check_date
    from (-- yep, these are the folks who had at least one check that annualized for > 100K and the first date it happened
      select min(check_date) as check_date, employee_number, employee_name
      from (  -- all checks that annualize > 100K
        select distinct a.employee_number, a.employee_name,
        (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date
        from ppp.employees_wage_comparison a
        left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
          And check_year = 19
        where b.total_gross_pay * 26 > 100000) aa 
      group by employee_number, employee_name  
      union
      -- now i need any other employees who totaled > 100K in 2019
      select '12/31/2019'::date, a.employee_number, a.employee_name
      from ppp.employees_wage_comparison a
      left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
        And check_year = 19
      group by a.employee_number, a.employee_name
      having sum(total_gross_pay) > 100000) cc
    group by employee_number, employee_name) dd on aa.employee_number = dd.employee_number;

end $$;

select * from wage_comparison order by store, employee_name;

/*


do $$
declare
  cp_from_date date := '04/15/2020';
  cp_thru_date date := '07/10/2020';
  lb_from_date date := '01/10/2020';
  lb_thru_date date := '03/31/2020';
  cp_weeks_B integer := 14;
begin
  drop table if exists wage_comparison;
  create temp table wage_comparison as
    
  select aa.*, 
    bb.lb_gross_pay, bb.lb_gross_pay_with_cap, bb.lb_annualized_salary, 
    bb.lb_annualized_salary_with_cap, bb.lb_total_hours, 
    bb.lb_avg_hourly_wage,
    cc.cp_gross_pay, cc.cp_gross_pay_with_cap, cc.cp_annualized_salary, 
    cc.cp_annualized_salary_with_cap, cc.cp_total_hours, 
    cc.cp_avg_hourly_wage,
    round(
      case
        when bb.lb_avg_hourly_wage is null or cc.cp_avg_hourly_wage is null then null
        else cc.cp_avg_hourly_wage / bb.lb_avg_hourly_wage
      end, 2) as wage_comparison,
    dd.check_date
  from ppp.employees_wage_comparison aa
  left join ( -- lookback, replace 25000 with 23076.92
    select count(*),a.employee_number, 
      sum(b.total_gross_pay) as lb_gross_pay,  --G
      case when sum(b.total_gross_pay) > 23076.92 then 23076.92 else sum(b.total_gross_pay) end as lb_gross_pay_with_cap, --H
      sum(b.total_gross_pay) * 4 as lb_annualized_salary, -- I 
      4 * (case when sum(b.total_gross_pay) > 23076.92 then 23076.92 else sum(b.total_gross_pay) end) as lb_annualized_salary_with_cap,  --J
      round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as lb_total_hours, --K,
      round(
        case
          when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0
            then null
          else
            (case when sum(b.total_gross_pay) > 23076.92 then 23076.92 else sum(b.total_gross_pay) end)
            /
            sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
        end, 2) as lb_avg_hourly_wage -- L  H/K  
    -- select sum(b.total_gross_pay) 
    from ppp.employees_wage_comparison a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      And (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between lb_from_date and lb_thru_date
    group by a.employee_number) bb on aa.employee_number = bb.employee_number
   left join ( -- covered period 
     select a.employee_number,
      sum(b.total_gross_pay) as cp_gross_pay, -- M
      case
        when a.pay_period = 'B' then
          case
            when sum(b.total_gross_pay) > (cp_weeks_B * 1923.08) then (cp_weeks_B * 1923.08)
            else sum(b.total_gross_pay)
          end
        when a.pay_period like '%S%' then
          case 
            when sum(b.total_gross_pay) > 25000 then 25000
            else sum(b.total_gross_pay)
          end
      end as cp_gross_pay_with_cap, -- N
      case
        when a.pay_period = 'B' then round(52 * (sum(b.total_gross_pay)/cp_weeks_B), 2) 
        when a.pay_period like '%S%' then round(12 * sum(b.total_gross_pay)/3, 2)
      end as cp_annualized_salary, -- O 
      case
        when 52 * (sum(b.total_gross_pay)/cp_weeks_B) > 100000 then 100000
        else round(52 * (sum(b.total_gross_pay)/cp_weeks_B), 2)
      end as cp_annualized_salary_with_cap,  -- P   
      round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as cp_total_hours, -- Q      
      round(
        case
          when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0
            then null
          else
            case 
              when a.pay_period = 'B' then
                (case when sum(b.total_gross_pay) > (cp_weeks_B * 1923.08) then (cp_weeks_B * 1923.08) else sum(b.total_gross_pay) end)
                /
                sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
              when a.pay_period like '%S%' then
                (case  when sum(b.total_gross_pay) > 25000 then 25000 else sum(b.total_gross_pay) end)
                /
                sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
            end
        end, 2) as cp_avg_hourly_wage -- R: N/Q  cp_gross_pay_with_cap/total_hours
    from ppp.employees_wage_comparison a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      And (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between cp_from_date and cp_thru_date
    group by a.employee_number) cc on aa.employee_number = cc.employee_number
  left join (  -- check dates annualized > 100000
    select employee_number, employee_name, min(check_date) as check_date
    from (-- yep, these are the folks who had at least one check that annualized for > 100K and the first date it happened
      select min(check_date) as check_date, employee_number, employee_name
      from (  -- all checks that annualize > 100K
        select distinct a.employee_number, a.employee_name,
        (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date
        from ppp.employees_wage_comparison a
        left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
          And check_year = 19
        where b.total_gross_pay * 26 > 100000) aa 
      group by employee_number, employee_name  
      union
      -- now i need any other employees who totaled > 100K in 2019
      select '12/31/2019'::date, a.employee_number, a.employee_name
      from ppp.employees_wage_comparison a
      left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
        And check_year = 19
      group by a.employee_number, a.employee_name
      having sum(total_gross_pay) > 100000) cc
    group by employee_number, employee_name) dd on aa.employee_number = dd.employee_number;

end $$;

select * from wage_comparison order by store, employee_name;
*/

-- 
-- -- lookback columns G - L
-- select count(*),a.employee_number, 
--   sum(b.total_gross_pay) as lb_gross_pay,  --G
--   case when sum(b.total_gross_pay) > 25000 then 25000 else sum(b.total_gross_pay) end as lb_gross_pay_with_cap, --H
--   sum(b.total_gross_pay) * 4 as lb_annualized_salary, -- I 
--   4 * (case when sum(b.total_gross_pay) > 25000 then 25000 else sum(b.total_gross_pay) end) as lb_annualized_salary_with_cap,  --J
--   round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as lb_total_hours, --K,
--   round(
--     case
--       when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0
--         then null
--       else
--         (case when sum(b.total_gross_pay) > 25000 then 25000 else sum(b.total_gross_pay) end)
--         /
--         sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
--     end, 2) as lb_avg_hourly_wage -- L  H/K  ************* check with jeri, this disagrees with her doc H/J
-- -- select sum(b.total_gross_pay) 
-- from ppp.employees_wage_comparison a
-- left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--   and b.seq_void in ('01','00','02','0J')
--   And to_date(
--       check_year::text || case when length(check_month::text) = 1 then '0'
--       ||check_month::text else check_month::text end 
--       ||case when length(check_day::text) = 1 then '0'
--       ||check_day::text else check_day::text end, 'YYMMDD')
--     between '01/10/2020'::date and '03/31/2020'::date  
-- group by a.employee_number
-- 
-- -- covered period
-- -- is anyone paid monthly  nope
-- -- select pay_period, count(*) from arkona.ext_pymast group by pay_period
-- -- select 14 * 1923.08 = 26923.12
-- -- cp_total_hours does indeed aggree with fte_comparison
-- select count(*), a.employee_number,
--   sum(b.total_gross_pay) as cp_gross_pay, -- M
--   case
--     when sum(b.total_gross_pay) > (14 * 1923.08) then (14 * 1923.08)
--     else sum(b.total_gross_pay)
--   end as cp_gross_pay_with_cap, -- N
--   round(52 * (sum(b.total_gross_pay)/14), 2) as cp_annualized_salary, -- O 
--   case
--     when 52 * (sum(b.total_gross_pay)/14) > 100000 then 100000
--     else round(52 * (sum(b.total_gross_pay)/14), 2)
--   end as cp_annualized_salary_with_cap,  -- P
--   round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as cp_total_hours, -- Q
--   round(
--     case
--       when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0
--         then null
--       else
--         (case when sum(b.total_gross_pay) > (14 * 1923.08) then (14 * 1923.08) else sum(b.total_gross_pay) end)
--         /
--         sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)
--     end, 2) as cp_avg_hourly_wage -- R: N/Q  cp_gross_pay_with_cap/total_hours
-- -- select round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2)   
-- from ppp.employees_wage_comparison a
-- left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--   and b.seq_void in ('01','00','02','0J')
--   And to_date(
--       check_year::text || case when length(check_month::text) = 1 then '0'
--       ||check_month::text else check_month::text end 
--       ||case when length(check_day::text) = 1 then '0'
--       ||check_day::text else check_day::text end, 'YYMMDD')
--     between '04/15/2020'::date and '07/10/2020'::date  
-- group by a.employee_number
-- 
-- -----------------------------------------------------------------------------
-- -- need a separate piece identifying check dates that anuallize > 100,000
-- -- select 100000/26.0  = 3846.15
-- Over $100K in 2019*: 
--     For the wage comparison report, any employees who earned wages or salary over $100,000 
--     (on an annualized basis) during any single pay period in 2019 are excluded. This column shows the paydate when 
--     the employee went over the $100,000 annualized limit in 2019 or 12/31/2019 if the employee went over $100,00 
--     for the full year. The following periodic amounts are used to annualize to $100,000.																		
--     Weekly Limit - $1923.08, Bi-weekly Limit - $3846.15, Semi-monthly - $4166.67, Monthly - $8333.33																		
--     In addition, all employees with an annual Gross pay in excess of $100,000 at the end of 2019																		
-- 
-- 
-- 
-- -- these are the employees which have individual checks that annualize over 100K in 2019
-- -- 94 employees
-- drop table if exists employees;
-- create temp table employees as
--     select distinct a.employee_number, a.employee_name
--     from ppp.employees_wage_comparison a
--     left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--       and b.seq_void in ('01','00','02','0J')
--       And check_year = 19
--     where b.total_gross_pay * 26 > 100000;
-- -- -- this is nice but not what i am looking for
-- -- select aa.*, sum(total_gross_pay) over (partition by employee_name order by check_date asc) as running_total
-- -- from (
-- --   select a.* , b.total_gross_pay,
-- --   to_date(
-- --         b .check_year::text || case when length(b.check_month::text) = 1 then '0'
-- --         ||b.check_month::text else b.check_month::text end 
-- --         ||case when length(b.check_day::text) = 1 then '0'
-- --         ||b.check_day::text else b.check_day::text end, 'YYMMDD') as check_date
-- --   from employees a      
-- --   join arkona.ext_pyhshdta b on a.employee_number = b.employee_
-- --   where b.seq_void in ('01','00','02','0J')
-- --     And b.check_year = 19) aa
-- -- order by employee_name, check_date    
-- 
-- 
-- select employee_number, employee_name, min(check_date) as check_date
-- from (-- yep, these are the folks who had at least one check that annualized for > 100K and the first date it happened
--   select min(check_date) as check_date, employee_number, employee_name
--   from (  -- all checks that annualize > 100K
--     select distinct a.employee_number, a.employee_name,
--     to_date(
--       b .check_year::text || case when length(b.check_month::text) = 1 then '0'
--       ||b.check_month::text else b.check_month::text end 
--       ||case when length(b.check_day::text) = 1 then '0'
--       ||b.check_day::text else b.check_day::text end, 'YYMMDD') as check_date
--     from ppp.employees_wage_comparison a
--     left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--       and b.seq_void in ('01','00','02','0J')
--       And check_year = 19
--     where b.total_gross_pay * 26 > 100000) aa 
--   group by employee_number, employee_name  
--   union
--   -- now i need any other employees who totaled > 100K in 2019
--   select '12/31/2019'::date, a.employee_number, a.employee_name
--   from ppp.employees_wage_comparison a
--   left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--     and b.seq_void in ('01','00','02','0J')
--     And check_year = 19
--   group by a.employee_number, a.employee_name
--   having sum(total_gross_pay) > 100000) cc
-- group by employee_number, employee_name
-- order by check_date desc 
-- 
-- -- the above gives me the original 94 but it is telling me that there are no employees who totaled > 100K 
-- -- that did not have at least one check that annualized to > 100K
-- -- i think that make sense
-- biweekly check of 3846.15 is the value that annualizes to 100K
-- the assumption is, it would be impossbile to earn > 100K in the year unless at least one check annualized to > 100K
-- select 3846.14 * 26 = 99999.64
-- in my mind, that is confirmed
-- ------------------------------------------------
-- -- now need to divide column R by column L, so put this all together