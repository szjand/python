﻿/*
09/30/20
  from jeri:
    dropping leading 0's on SSN - fixed
    way too much COVID pay, wtf, on the join to arkona.ext_pyhscdta i neglected to join on employee_number, fixed
*/

drop table if exists ppp.employees_payroll_cost cascade;
create table ppp.employees_payroll_cost (
  employee_number citext not null primary key,
  store citext,
  employee_name citext,
  ssn citext,
  payroll_class citext,
  pay_period citext,
  hire_date date,
  term_date date);

insert into ppp.employees_payroll_cost  
select aa.employee_, 
  case
    when left(employee_, 1) = '1' then 'RY1'
    else 'RY2'
  end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
  select employee_, array_agg(distinct pay_period) as pay_period, array_agg(distinct payroll_class) as payroll_class
  from (
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
    from arkona.ext_pyhshdta
    where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date) x 
  group by employee_) aa
left join arkona.ext_pymast bb on aa.employee_ = bb.pymast_employee_number     
left join jon.ssn cc on aa.employee_ = cc.employee_number;
delete from ppp.employees_wage_comparison where employee_number = '1112425';  -- delete jim price

-- select * from ppp.employees_payroll_cost 
-- -- this was with no separation between biweekly and monthly
-- select aa.*, bb.gross_pay, bb.ffcra_excluded_earnings, -- H
--   case
--     when bb.gross_pay > (1923.08 * 14) then bb.gross_pay - (1923.08 * 14) 
--     else 0
--   end as gross_pay_in_exc_100K,  -- I
--   bb.gross_pay - bb.ffcra_excluded_earnings  
--     -   case
--           when bb.gross_pay > (1923.08 * 14) then bb.gross_pay - (1923.08 * 14) 
--           else 0
--         end as sba_gross_pay -- J
-- from ppp.employees_payroll_cost aa
-- left join (
--   select a.employee_number, sum(b.total_gross_pay) as gross_pay, coalesce(sum(c.amount), 0) as ffcra_excluded_earnings
--   from ppp.employees_payroll_cost a
--   left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--     and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date
--   left join arkona.ext_pyhscdta c on b.payroll_run_number = c.payroll_run_number
--     and c.code_id in ('COV','C0V','C19')  
--   group by a.employee_number) bb on aa.employee_number = bb.employee_number
-- order by aa.store, aa.employee_name; 



do $$
declare 
  from_date date := '04/15/2020';
  thru_date date := '07/10/2020';
  weeks integer := 14;
  months integer := 3;
begin
  drop table if exists payroll_cost;
  create temp table payroll_cost as
  select aa.*, bb.gross_pay, bb.ffcra_excluded_earnings, -- H
    case 
      when aa.pay_period = 'B' then
        case
          when bb.gross_pay > (1923.08 * weeks) then bb.gross_pay - (1923.08 * weeks) 
          else 0
        end
      when aa.pay_period like '%S%' then
        case when bb.gross_pay > (8333.33 * months) then bb.gross_pay -  (8333.33 * months) 
        else 0
      end
    end as gross_pay_in_exc_100K,  -- I
    bb.gross_pay - bb.ffcra_excluded_earnings  
      -  -- minus
      case 
        when aa.pay_period = 'B' then
          case
            when bb.gross_pay > (1923.08 * weeks) then bb.gross_pay - (1923.08 * weeks) 
            else 0
          end
        when aa.pay_period like '%S%' then
          case when bb.gross_pay > (8333.33 * months) then bb.gross_pay -  (8333.33 * months) 
          else 0
        end
      end as sba_gross_pay -- J          
  from ppp.employees_payroll_cost aa
  left join (
    select a.employee_number, sum(b.total_gross_pay) as gross_pay, coalesce(sum(c.amount), 0) as ffcra_excluded_earnings
    from ppp.employees_payroll_cost a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date
    left join arkona.ext_pyhscdta c on b.payroll_run_number = c.payroll_run_number
      and a.employee_number = c.employee_number
      and c.code_id in ('COV','C0V','C19')  
    group by a.employee_number) bb on aa.employee_number = bb.employee_number
  order by aa.store, aa.employee_name; 
end $$;

select * from payroll_cost order by store, employee_name;
-- 
-- -- fucking math for biweekly > 100000 does not make sense, these numbers are for shawn anderson
-- select * from payroll_cost;
-- 
-- select 37941.58/14   == 2710.11
-- 
-- select 2710.11 * 52   = 140925.72
-- 
-- select 1923.08 * 14  = 26923.12
-- 
-- select 1923.08 * 52   = 100000
-- 
-- select 37941.58 - 26923.12
-- 
-- -- agrees with the payroll reports i was sending to jeri
-- select (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date,
--   total_gross_pay
-- from arkona.ext_pyhshdta
-- where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date 
--   and employee_ = '13725'
-- order by (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date 
-- 
-- select (37941.58 * 52)/14 = 140925.87
-- 
-- select 40925.87/52 = 787.04
-- 
-- select 787.04 * 14  = 11018.56

