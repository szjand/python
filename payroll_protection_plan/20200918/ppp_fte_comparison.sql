﻿/*
09/30/20
  from jeri:
    dropping leading 0's on SSN - fixed
*/


drop table if exists ppp.employees_fte_comparison cascade;
create table ppp.employees_fte_comparison (
  employee_number citext not null primary key,
  store citext,
  employee_name citext,
  ssn citext,
  payroll_class citext,
  pay_period citext,
  hire_date date,
  term_date date);

insert into ppp.employees_fte_comparison  
select aa.employee_, 
  case
    when left(employee_, 1) = '1' then 'RY1'
    else 'RY2'
  end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
  select employee_, array_agg(distinct pay_period) as pay_period, array_agg(distinct payroll_class) as payroll_class
  from (
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
    from arkona.ext_pyhshdta
    where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date
    union all
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
    from arkona.ext_pyhshdta
    where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '02/15/2019'::date and '06/30/2019'::date
    union all
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
    from arkona.ext_pyhshdta
    where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date
     ) x group by employee_) aa
left join arkona.ext_pymast bb on aa.employee_ = bb.pymast_employee_number     
left join jon.ssn cc on aa.employee_ = cc.employee_number;
delete from ppp.employees_wage_comparison where employee_number = '1112425';  -- delete jim price

-- select * from ppp.employees_fte_comparison

do $$
declare
  cp_from_date date := '04/15/2020';
  cp_thru_date date := '07/10/2020';
  lb1_from_date date := '02/15/2019';
  lb1_thru_date date := '06/30/2019';
  lb2_from_date date := '01/10/2020';
  lb2_thru_date date := '02/29/2020';
  cp_denominator_B integer := 14;
  cp_denominator_S integer := 12;
  lb1_denominator integer := 20;
  lb2_denominator integer := 8;
begin
  drop table if exists fte;
  create temp table fte as
  select aa.*, 
    bb.cp_hours, bb.cp_fte_opt_1, bb.cp_fte_opt_2,
    cc.lb1_hours, cc.lb1_fte_opt_1, cc.lb1_fte_opt_2,
    dd.lb2_hours, dd.lb2_fte_opt_1, dd.lb2_fte_opt_2
  from ppp.employees_fte_comparison aa
  left join (-- covered_period B denominator = 14, S = 12
    select a.employee_number,
      round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as cp_hours, -- G
      case
        when a.pay_period like '%S%' then 
          case
            when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/cp_denominator_S/40 >= 1 then 1
            else round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/cp_denominator_S/40, 2)
          end
        when a.pay_period = 'B' then 
          case
            when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/cp_denominator_B/40 >= 1 then 1
            else round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/cp_denominator_B/40, 2)
          end        
      end as cp_fte_opt_1, -- H
      case
        when a.pay_period like '%S%' then       
          case
            when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/cp_denominator_S >= 40 then 1
            when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) is null then null
            when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0 then 0
            else .5
          end
        when a.pay_period = 'B' then  
          case
            when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/cp_denominator_B >= 40 then 1
            when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) is null then null
            when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0 then 0
            else .5
          end                   
      end as cp_fte_opt_2 -- I
    from ppp.employees_fte_comparison a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between cp_from_date and cp_thru_date
    group by a.employee_number) bb on aa.employee_number = bb.employee_number
  left join ( -- lb_period_1 denominator = 20
    select a.employee_number,
      round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as lb1_hours, -- G
      case
        when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/lb1_denominator/40 >= 1 then 1
        else round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/lb1_denominator/40, 2)
      end as lb1_fte_opt_1, -- H
      case
        when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/lb1_denominator >= 40 then 1
        when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) is null then null
        when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0 then 0      
        else .5
      end as lb1_fte_opt_2 -- I
    from ppp.employees_fte_comparison a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between lb1_from_date and lb1_thru_date
    group by a.employee_number) cc on aa.employee_number = cc.employee_number      
  left join ( -- lb_period_2 denominator = 10
    select a.employee_number,
      round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as lb2_hours, -- G
      case
        when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/lb2_denominator/40 >= 1 then 1
        else round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/lb2_denominator/40, 2)
      end as lb2_fte_opt_1, -- H
      case
        when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours)/lb2_denominator >= 40 then 1
        when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) is null then null
        when sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) = 0 then 0      
        else .5
      end as lb2_fte_opt_2 -- I
    from ppp.employees_fte_comparison a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between lb2_from_date and lb2_thru_date
    group by a.employee_number) dd on aa.employee_number = dd.employee_number
  order by aa.store, aa.employee_name;
end $$;

select * from fte;


-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------

-- i am so full of shit, this does not address semi monthly, they get paid on the 15th and ~30th for the calendar month
-- but kim plugs in 80 hours for each of them
-- so, i will go ahead with a denominator of 14, realizing that i may need to rerun all of these
-- 
-- 
-- select * 
-- from arkona.ext_pyhshdta
-- where employee_ = '17534'
--   and check_year = 20
-- 
--   
-- -- covered period: check dates 04/15/20 -> 07/10/20
-- -- look back period 1 check dates 02/15/2019 -> 06/30/2019
-- -- look back period 2 check dates 01/10/2020 -> 02/29/2020
-- -- 
-- -- select distinct check_month,check_year,check_day
-- -- from arkona.ext_pyhshdta
-- -- where to_date(
-- --     check_year::text || case when length(check_month::text) = 1 then '0'
-- --     ||check_month::text else check_month::text end 
-- --     ||case when length(check_day::text) = 1 then '0'
-- --     ||check_day::text else check_day::text end, 'YYMMDD')
-- --   between '04/15/2020'::date and '07/10/2020'::date
-- -- 
-- -- select ('07/10/2020'::date - '04/15/2020'::date)/7.0  -- 12.29
-- -- 
-- -- select ('06/30/2019'::date - '02/15/2019'::date)/7.0  -- 19.28
-- -- 
-- -- select ('02/29/2020'::date - '01/01/2020'::date)/7.0  -- 8.42
-- -- 
-- -- -- experiment with dates
-- -- -- 9/6 thru 9/26 is 3 weeks
-- -- -- to get 21 days need to expand the elements by 1 day
-- -- select ('09/26/2020'::date - '09/06/2020'::date) -- 20
-- -- select ('09/26/2020'::date - '09/05/2020'::date)  -- 21
-- -- select ('09/26/2020'::date - '09/06/2020'::date)/7.0  -- 2.857
-- -- select ('09/26/2020'::date - '09/05/2020'::date)/7.0  -- 3
-- -- 
-- -- --therefor
-- -- covered period: check dates 04/15/20 -> 07/10/20
-- -- look back period 1 check dates 02/15/2019 -> 06/30/2019
-- -- look back period 2 check dates 01/01/2020 -> 02/29/2020
-- -- select ('07/10/2020'::date - '04/14/2020'::date)/7.0  -- = 87 days , 12.43 weeks
-- -- select ('06/30/2019'::date - '02/14/2019'::date)/7.0  -- = 136 days, 19.43 weeks
-- -- select ('02/29/2020'::date - '12/31/2019'::date)/7.0  -- = 60 days, 8.57 weeks
-- -- 
-- -- these are the check dates, but these date ranges dont translate to 13, 19, 9 weeks
-- -- select * 
-- -- from arkona.ext_pyhshdta
-- -- where check_year = 20 and check_month = 7 and check_day = 10
-- -- limit 10
-- -- 
-- -- 
-- -- need to union all the periods to get the total employee lisst
-- -- create schema ppp;
-- -- -- comment on schema ppp is 'a place to consolidate tables for ppp, payroll protection plan, including fte for jeri';
-- 
-- 
-- select distinct biweekly_pay_period_start_date, biweekly_pay_period_end_date
-- from dds.dim_date
-- where the_Date between '04/15/2020' and '07/10/2020'
-- 
-- 
-- per conversation with jeri and greg on friday, the fact the covered period is in fact 14 weeks, that
-- is what should be used for the denominator
-- 
-- but, fuck me, its not 14 weeks, it is 12
-- 
-- -- ok, the payroll_end_date in pyhshdta matches with the actual biweekly payroll end dates
-- select *
-- from (
--   select distinct (('20'||payroll_ending_year)::text||'-'||payroll_ending_month::text||'-'||payroll_ending_day::text)::date as payroll_end_date
--   from arkona.ext_pyhshdta a
--   where check_year  in (19,20)) aa
-- where not exists (
--   select 1
--     from dds.dim_Date
--     where biweekly_pay_period_end_date <> aa.payroll_end_Date)
-- 
-- -- for the given check dates, here are the actual pay periods involved
-- -- covered period: check dates 04/15/20 -> 07/10/20
-- -- 6 pay periods: 12 weeks
-- select distinct biweekly_pay_period_start_date, biweekly_pay_period_end_date, array_to_string(array_agg(distinct b.check_date),',') as check_dates
-- from dds.dim_date a
-- join (
-- select distinct -- check_year, check_month, check_day, payroll_ending_year, payroll_ending_month, payroll_ending_day,
--   (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date,
--   (('20'||payroll_ending_year)::text||'-'||payroll_ending_month::text||'-'||payroll_ending_day::text)::date as payroll_end_date
-- from arkona.ext_pyhshdta
-- where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between  '04/15/2020'::date and '07/10/2020'::date) b on b.payroll_end_date between a.biweekly_pay_period_start_date and a.biweekly_pay_period_end_date
-- group by biweekly_pay_period_start_date, biweekly_pay_period_end_date  
-- order by biweekly_pay_period_start_date;     
-- 
-- -- look back period 1 check dates 02/15/2019 -> 06/30/2019
-- -- 10 pay periods 20 weeks
-- select distinct biweekly_pay_period_start_date, biweekly_pay_period_end_date, array_to_string(array_agg(distinct b.check_date),',') as check_dates
-- from dds.dim_date a
-- join (
-- select distinct -- check_year, check_month, check_day, payroll_ending_year, payroll_ending_month, payroll_ending_day,
--   (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date,
--   (('20'||payroll_ending_year)::text||'-'||payroll_ending_month::text||'-'||payroll_ending_day::text)::date as payroll_end_date
-- from arkona.ext_pyhshdta
-- where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between  '02/15/2019'::date and '06/30/2019'::date) b on b.payroll_end_date between a.biweekly_pay_period_start_date and a.biweekly_pay_period_end_date
-- group by biweekly_pay_period_start_date, biweekly_pay_period_end_date  
-- order by biweekly_pay_period_start_date;    
-- 
-- -- look back period 2 check dates 01/10/2020 -> 02/29/2020
-- -- 5 pay periods 10 weeks
-- select distinct biweekly_pay_period_start_date, biweekly_pay_period_end_date, array_to_string(array_agg(distinct b.check_date), ',') as check_dates
-- from dds.dim_date a
-- join (
-- select distinct -- check_year, check_month, check_day, payroll_ending_year, payroll_ending_month, payroll_ending_day,
--   (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date,
--   (('20'||payroll_ending_year)::text||'-'||payroll_ending_month::text||'-'||payroll_ending_day::text)::date as payroll_end_date
-- from arkona.ext_pyhshdta
-- where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between  '01/10/2020'::date and '02/29/2020'::date) b on b.payroll_end_date between a.biweekly_pay_period_start_date and a.biweekly_pay_period_end_date
-- group by biweekly_pay_period_start_date, biweekly_pay_period_end_date  
-- order by biweekly_pay_period_start_date      
-- 
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
-- 
-- -- covered_period
-- select count(distinct check_date), a.store, a.employee_number, a.employee_name, a.ssn,a.hire_date, a.term_date, a.pay_period, a.payroll_class, 
--   sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) as hours,
--   array_agg(distinct check_date order by check_date)
-- from ppp.employees_fte_comparison a
-- left join (
--   select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
--     reg_hours, overtime_hours, alt_pay_hours,
--     (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
--   from arkona.ext_pyhshdta
--   where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date
--   order by employee_ , (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) b on a.employee_number = b.employee_ 
-- group by a.employee_number, a.employee_name, a.pay_period, a.payroll_class,a.ssn,a.hire_date,a.term_date
-- order by count(*)
-- 
-- 
-- -- look back period 1
-- select employee_, pay_period, payroll_class, termination_date, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
--   reg_hours, overtime_hours, alt_pay_hours,
--   (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
-- from arkona.ext_pyhshdta
-- where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '02/15/2019'::date and '06/30/2019'::date
-- order by employee_ , (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date  
-- 
-- -- look back period 2
-- select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
--   reg_hours, overtime_hours, alt_pay_hours,
--   (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
-- from arkona.ext_pyhshdta
-- where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date
-- order by employee_ , (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date    
-- 
-- ---------------------------------------------------------------------------------------------------
