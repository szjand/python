﻿2 steps, no way at this time to avoid manual intervention for dealing with multiple rows
so
step 1, create the base_employees table and check it for dup employee numbers
instead of the "complex" insert query, do that in a function, then the dynamic sql just calls the function
eg  insert into the table select function(from_date date, thru_date date, the_range daterange)
https://dba.stackexchange.com/questions/5454/postgres-plpgsql-using-a-variable-inside-of-a-dynamic-create-statement

/**/
-- for cleaning up dups
do $$ 
declare
  _from_date date := '05/17/2020';
  _thru_date date := '05/23/2020';
  _the_range daterange := daterange(_from_date,_thru_date, '[]');
--   _table_name text := 'jeri.fte_2020-04-12_orig_v2';
begin
  drop table if exists wtf;
  create temp table wtf as
  select employeekey, storecode as store, employeenumber as employee_number, name, 
    hiredate as hire_date, termdate as term_date, distcode as dist_code,
    payrollclasscode as payroll_class, activecode, fullparttimecode, currentrow, 
    employeekeyfromdate as row_from_date, employeekeythrudate as row_thru_date,
    daterange (employeekeyfromdate, employeekeythrudate, '[]') as from_thru,
    daterange(hiredate, termdate, '[]') as hire_term,
    (select distinct sunday_to_saturday_week from dds.dim_date where the_Date = _from_date)
  from ads.ext_edw_employee_dim
  -- where employeenumber <> '135987' -- adam clark, sb ok on 4/19
  where employeenumber <> '1112425' -- exclude jim price
    and hiredate < _thru_date  -- before the end
    and termdate > _from_date -- after the start
    and daterange (employeekeyfromdate, employeekeythrudate, '[]') && _the_range;
end $$;    

select a.*
from wtf a
join (
  select employee_number
  from wtf
  group by employee_number
  having count(*) > 1) b on a.employee_number = b.employee_number 
order by a.employee_number  ;

select *
from wtf
where not currentrow
  and row_thru_date <= '04/18/2020';  

select * from wtf where employee_number = '1130426'

select * from ads.ext_edw_employee_dim where employeenumber = '123100' order by employeekey

select *
from wtf a
join 

/**/

/*
-- spreadsheets
select store,employee_number as "emp #",name,dist_Code,hire_date,term_date,
  payroll_class,full_part,clock_hours,fte 
from jeri.fte_04_19_orig where store = 'RY2'
union
select null,null,null,null,null,null,null,null,null,sum(fte) from jeri.fte_04_19_orig where store = 'RY2'
order by store, name nulls last;
*/

-- do $$ -- fte data using original rules
-- declare
--   _from_date date := '04/19/2020';
--   _thru_date date := '04/25/2020';
--   _the_range daterange := daterange(_from_date,_thru_date, '[]');
--   _table_name text := 'jeri.fte_04_19_orig';
-- begin
--   truncate jeri.fte_employees_tmp; -- replaces temp table base_employees
--   insert into jeri.fte_employees_tmp
--   select employeekey, storecode as store, employeenumber as employee_number, name, 
--     hiredate as hire_date, termdate as term_date, distcode as dist_code,
--     payrollclasscode as payroll_class, activecode, fullparttimecode, currentrow, 
--     employeekeyfromdate as row_from_date, employeekeythrudate as row_thru_date,
--     daterange (employeekeyfromdate, employeekeythrudate, '[]') as from_thru,
--     daterange(hiredate, termdate, '[]') as hire_term,
--     (select distinct sunday_to_saturday_week from dds.dim_date where the_Date = _from_date)
--   from ads.ext_edw_employee_dim
--   -- where employeenumber <> '135987' -- adam clark, sb ok on 4/19
--   where employeenumber <> '1112425' -- exclude jim price
--     and hiredate < _thru_date  -- before the end
--     and termdate > _from_date -- after the start
--     and daterange (employeekeyfromdate, employeekeythrudate, '[]') && _the_range;
-- 
-- ----------------------------------------------------------------------------------------------
-- --< if there are duplicate employee numbers, put the code to clean it up in here
-- ----------------------------------------------------------------------------------------------
-- -- delete
-- -- from jeri.fte_employees_tmp 
-- -- where not current_row
-- --   and row_thru_date <= '04/18/2020';  
-- ----------------------------------------------------------------------------------------------
-- --/> if there are duplicate employee numbers, put the code to clean it up in here
-- ----------------------------------------------------------------------------------------------
--   
--   drop table if exists dups;
--   create temp table dups as
--   select employee_number
--   from jeri.fte_employees_tmp
--   group by employee_number
--   having count(*) > 1;    
-- 
--   assert (
--     select count(*)
--     from dups) = 0, 'multiple employee number instances';  
-- 
--   -- replaces temp table clock_hours
--   truncate jeri.fte_clock_hours_tmp;
--   insert into jeri.fte_clock_hours_tmp
--   select a.store_code, a.employee_number, 
--     sum(a.clock_hours + a.vac_hours + a.pto_hours + a.hol_hours) as clock_hours
--   from arkona.xfm_pypclockin a
--   join jeri.fte_employees_tmp aa on a.employee_number = aa.employee_number
--   where a.the_date between _from_date and _thru_date
--   group by a.store_code, a.employee_number;  
-- 
-- -- select * from jeri.fte_employees_tmp 
-- -- select * from jeri.fte_clock_hours_tmp
-- 
--   -- call the function to populate jeri.fte_tmp;
--   perform jeri.get_fte_orig (_the_range);
-- 
--   execute 'drop table if exists ' || _table_name || ';';
--   execute '
--     create table ' || _table_name || '(
--     store citext not null,
--     employee_number citext primary key,
--     name citext not null,
--     dist_code citext not null,
--     hire_date date not null, 
--     term_date date,
--     payroll_class citext not null,
--     full_part citext not null,
--     clock_hours numeric (8,2) not null,
--     fte numeric(3,1) not null)';    
-- 
--   execute 'insert into ' || _table_name || ' select * from jeri.fte_tmp';
-- 
-- end $$;

*************************************************************************************************
*************************************************************************************************
-- the differences: 
--    _table_name
--    the name of the function called to generate the fte data

!!!!!!!!!!!!!!!!!!!!!! see function jon.weekly_fte() !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- spreadsheet
select store,employee_number as "emp #",name,dist_Code,hire_date,term_date,
  payroll_class,full_part,clock_hours,fte 
from jeri.fte_05_17_scenario_1 where store = 'RY2'
union
select null,null,null,null,null,null,null,null,null,sum(fte) from jeri.fte_05_17_scenario_1 where store = 'RY2'
order by store, name nulls last;

do $$ -- fte data using scenario 1
declare
  _from_date date := '05/17/2020';
  _thru_date date := '05/23/2020';
  _the_range daterange := daterange(_from_date,_thru_date, '[]');
  _table_name text := 'jeri.fte_05_17_scenario_1';
begin
  truncate jeri.fte_employees_tmp; -- replaces temp table base_employees
  insert into jeri.fte_employees_tmp
  select employeekey, storecode as store, employeenumber as employee_number, name, 
    hiredate as hire_date, termdate as term_date, distcode as dist_code,
    payrollclasscode as payroll_class, activecode, fullparttimecode, currentrow, 
    employeekeyfromdate as row_from_date, employeekeythrudate as row_thru_date,
    daterange (employeekeyfromdate, employeekeythrudate, '[]') as from_thru,
    daterange(hiredate, termdate, '[]') as hire_term,
    (select distinct sunday_to_saturday_week from dds.dim_date where the_Date = _from_date)
  from ads.ext_edw_employee_dim
  -- where employeenumber <> '135987' -- adam clark, sb ok on 4/19
  where employeenumber <> '1112425' -- exclude jim price
    and hiredate < _thru_date  -- before the end
    and termdate > _from_date -- after the start
    and daterange (employeekeyfromdate, employeekeythrudate, '[]') && _the_range;

----------------------------------------------------------------------------------------------
--< if there are duplicate employee numbers, put the code to clean it up in here
----------------------------------------------------------------------------------------------
-- --03/29
--   delete
--   from jeri.fte_employees_tmp 
--   where employeekey in (974,5110);

-- -- 04/05
-- delete 
-- from jeri.fte_employees_tmp
-- where  employeekey = 5113;

-- --04/12
-- delete 
-- from jeri.fte_employees_tmp
-- where employeekey in (4380,5043);

-- -- 04/26 8 dups, ok to just take the current row
-- delete 
-- -- select *
-- from jeri.fte_employees_tmp
-- where not current_row;

-- -- 05/10
-- delete
-- from jeri.fte_employees_tmp
-- where employeekey = 5138;

-- 05/17
delete from jeri.fte_employees_tmp where not current_row;

----------------------------------------------------------------------------------------------
--/> if there are duplicate employee numbers, put the code to clean it up in here
----------------------------------------------------------------------------------------------
  
  drop table if exists dups;
  create temp table dups as
  select employee_number
  from jeri.fte_employees_tmp
  group by employee_number
  having count(*) > 1;    

  assert (
    select count(*)
    from dups) = 0, 'multiple employee number instances';  

  -- replaces temp table clock_hours
  truncate jeri.fte_clock_hours_tmp;
  insert into jeri.fte_clock_hours_tmp
  select a.store_code, a.employee_number, 
    sum(a.clock_hours + a.vac_hours + a.pto_hours + a.hol_hours) as clock_hours
  from arkona.xfm_pypclockin a
  join jeri.fte_employees_tmp aa on a.employee_number = aa.employee_number
  where a.the_date between _from_date and _thru_date
  group by a.store_code, a.employee_number;  

-- select * from jeri.fte_employees_tmp 
-- select * from jeri.fte_clock_hours_tmp

  -- call the function to populate jeri.fte_tmp;
  perform jeri.get_fte_scenario_1 (_the_range);

  execute 'drop table if exists ' || _table_name || ';';
  execute '
    create table ' || _table_name || '(
    store citext not null,
    employee_number citext primary key,
    name citext not null,
    dist_code citext not null,
    hire_date date not null, 
    term_date date,
    payroll_class citext not null,
    full_part citext not null,
    clock_hours numeric (8,2) not null,
    fte numeric(3,1) not null)';    

  execute 'insert into ' || _table_name || ' select * from jeri.fte_tmp';
    
end $$;

*************************************************************************************************
*************************************************************************************************
-- the differences: 
--    _table_name
--    the name of the function called to generate the fte data

!!!!!!!!!!!!!!!!!!!!!! see function jon.weekly_fte() !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  if it fails due to dups, modify the line in the function that cleans up the dups for this week
-- spreadsheet
select store,employee_number as "emp #",name,dist_Code,hire_date,term_date,
  payroll_class,full_part,clock_hours,fte 
from jeri.fte_05_17_scenario_2 where store = 'RY2'
union
select null,null,null,null,null,null,null,null,null,sum(fte) from jeri.fte_05_17_scenario_2 where store = 'RY2'
order by store, name nulls last;

do $$ -- fte data using scenario 2
declare
  _from_date date := '05/17/2020';
  _thru_date date := '05/23/2020';
  _the_range daterange := daterange(_from_date,_thru_date, '[]');
  _table_name text := 'jeri.fte_05_17_scenario_2';
begin
  truncate jeri.fte_employees_tmp; -- replaces temp table base_employees
  insert into jeri.fte_employees_tmp
  select employeekey, storecode as store, employeenumber as employee_number, name, 
    hiredate as hire_date, termdate as term_date, distcode as dist_code,
    payrollclasscode as payroll_class, activecode, fullparttimecode, currentrow, 
    employeekeyfromdate as row_from_date, employeekeythrudate as row_thru_date,
    daterange (employeekeyfromdate, employeekeythrudate, '[]') as from_thru,
    daterange(hiredate, termdate, '[]') as hire_term,
    (select distinct sunday_to_saturday_week from dds.dim_date where the_Date = _from_date)
  from ads.ext_edw_employee_dim
  -- where employeenumber <> '135987' -- adam clark, sb ok on 4/19
  where employeenumber <> '1112425' -- exclude jim price
    and hiredate < _thru_date  -- before the end
    and termdate > _from_date -- after the start
    and daterange (employeekeyfromdate, employeekeythrudate, '[]') && _the_range;

----------------------------------------------------------------------------------------------
--< if there are duplicate employee numbers, put the code to clean it up in here
----------------------------------------------------------------------------------------------
-- --03/29
--   delete
--   from jeri.fte_employees_tmp 
--   where employeekey in (974,5110);

-- -- 04/05
-- delete 
-- from jeri.fte_employees_tmp
-- where  employeekey = 5113;

-- --04/12
-- delete 
-- from jeri.fte_employees_tmp
-- where employeekey in (4380,5043);

-- -- 04/26 8 dups, ok to just take the current row
-- delete 
-- -- select *
-- from jeri.fte_employees_tmp
-- where not current_row;

-- -- 05/10
-- delete
-- from jeri.fte_employees_tmp
-- where employeekey = 5138;

-- 05/17
delete from jeri.fte_employees_tmp where not current_row;

----------------------------------------------------------------------------------------------
--/> if there are duplicate employee numbers, put the code to clean it up in here
----------------------------------------------------------------------------------------------
  
  drop table if exists dups;
  create temp table dups as
  select employee_number
  from jeri.fte_employees_tmp
  group by employee_number
  having count(*) > 1;    

  assert (
    select count(*)
    from dups) = 0, 'multiple employee number instances';  

  -- replaces temp table clock_hours
  truncate jeri.fte_clock_hours_tmp;
  insert into jeri.fte_clock_hours_tmp
  select a.store_code, a.employee_number, 
    sum(a.clock_hours + a.vac_hours + a.pto_hours + a.hol_hours) as clock_hours
  from arkona.xfm_pypclockin a
  join jeri.fte_employees_tmp aa on a.employee_number = aa.employee_number
  where a.the_date between _from_date and _thru_date
  group by a.store_code, a.employee_number;  

  -- call the function to populate jeri.fte_tmp;
  perform jeri.get_fte_scenario_2 (_the_range);

  execute 'drop table if exists ' || _table_name || ';';
  execute '
    create table ' || _table_name || '(
    store citext not null,
    employee_number citext primary key,
    name citext not null,
    dist_code citext not null,
    hire_date date not null, 
    term_date date,
    payroll_class citext not null,
    full_part citext not null,
    clock_hours numeric (8,2) not null,
    fte numeric(3,1) not null)';    

  execute 'insert into ' || _table_name || ' select * from jeri.fte_tmp';
    
end $$;

*************************************************************************************************
*************************************************************************************************


-- replaces temp table base_employees
drop table if exists jeri.fte_employees_tmp;
create table jeri.fte_employees_tmp (
  employeekey integer not null,
  store citext not null,
  employee_number citext not null,
  name citext not null,
  hire_date date not null,
  term_date date not null,
  dist_code citext not null,
  payroll_class citext not null,
  active_code citext not null,
  full_part_time citext not null,
  current_row boolean not null,
  row_from_date date not null,
  row_thru_date date not null,
  from_thru daterange not null,
  hire_term daterange not null,
  sunday_to_saturday_week integer);
  
-- replaces temp table clock_hours
drop table if exists jeri.fte_clock_hours_tmp;
create table jeri.fte_clock_hours_tmp (
  store citext not null,
  employee_number citext primary key,
  clock_hours numeric(8,2) not null);

-- destination table for the function to populate  
-- same structure as the destination table(s)
drop table if exists jeri.fte_tmp;
create table jeri.fte_tmp (
  store citext not null,
  employee_number citext primary key,
  name citext not null,
  dist_code citext not null,
  hire_date date not null, 
  term_date date,
  payroll_class citext not null,
  full_part citext not null,
  clock_hours numeric (8,2) not null,
  fte numeric(3,1) not null);


 create or replace function jon.weekly_fte_spreadsheets()
returns table (week_id integer, from_date date, thru_date date,scenario_1_table_name text, scenario_2_table_name text) as
$BODY$
/*
select * from jon.weekly_fte_spreadsheets();
*/  
declare
  week_id integer = ( -- the previous week
    select distinct sunday_to_saturday_week -1 
    from dds.dim_date 
    where the_Date = current_date); 
  from_date date = (
    select min(the_date)
    from dds.dim_date
    where sunday_to_saturday_week = week_id);  
  thru_date date = (
    select max(the_date)
    from dds.dim_date
    where sunday_to_saturday_week = week_id); 
  scenario_1_table_name text = 'jon.fte_' || (select lpad(extract(month from from_date)::text, 2, '0')) 
      || '_' || (select lpad(extract(day from from_date)::text, 2, '0')) || '_scenario_1';
  scenario_2_table_name text = 'jon.fte_' || (select lpad(extract(month from from_date)::text, 2, '0')) 
      || '_' || (select lpad(extract(day from from_date)::text, 2, '0')) || '_scenario_2';

begin
  return query
  select week_id, from_date, thru_date, scenario_1_table_name, scenario_2_table_name;
end;
$BODY$
language plpgsql;


select * from jon.fte_05_17_scenario_1

select store,employee_number as "emp #",name,dist_Code,hire_date,term_date,
payroll_class,full_part,clock_hours,fte 
from jon.fte_05_17_scenario_1 where store = 'RY2'
union
select null,null,null,null,null,null,null,null,null,sum(fte) from jon.fte_05_17_scenario_1 where store = 'RY2'
order by store, name nulls last;



        pg_cur.execute(sql)
