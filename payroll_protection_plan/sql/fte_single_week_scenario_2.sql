﻿/*
do $$
declare
  from_date date := '12/29/2019';
  thru_date date := '01/04/2020';
  the_range daterange := daterange(from_date,thru_date, '[]');
begin
  drop table if exists the_test;
  create temp table the_test as
  select the_range;
end $$;

select * from the_test;

*/
-- have to go weekly
drop table if exists weeks;
create temp table weeks as
select daterange(min(the_date), max(the_date), '[]') as week_range, 
  min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week
from dds.dim_date 
where the_date between '04/12/2020' and '04/18/2020'
group by sunday_to_saturday_week
order by sunday_to_saturday_week;


drop table if exists base_employees;
create temp table base_employees as
select employeekey, storecode as store, employeenumber as employee_number, name, 
  hiredate as hire_date, termdate as term_date, distcode as dist_code,
  payrollclasscode as payroll_class, activecode, fullparttimecode, currentrow, 
  employeekeyfromdate as row_from_date, employeekeythrudate as row_thru_date,
  daterange (employeekeyfromdate, employeekeythrudate, '[]') as from_thru,
  daterange(hiredate, termdate, '[]') as hire_term,
  (select distinct sunday_to_saturday_week from dds.dim_date where the_Date = '04/12/2020')
from ads.ext_edw_employee_dim
-- where employeenumber <> '135987' -- adam clark, sb ok on 4/19
where employeenumber <> '1112425' -- exclude jim price
  and hiredate < '04/18/2020'  -- before the end
  and termdate > '04/12/2020' -- after the start
  and daterange (employeekeyfromdate, employeekeythrudate, '[]') && daterange ('04/12/2020', '04/18/2020', '[]')
order by name, employeekey  


---------------------------------------------------------
--< dup employee numbers
---------------------------------------------------------
select employee_number
from base_employees
group by employee_number
having count(*) > 1

select * 
from base_employees 
where employee_number in ('156988')

-- 3/29, only 1 dup, randy sattler
-- delete from base_employees where employeekey in (974, 5111)
-- 4/5 nope megan giesler switched from full to part time on 4/11
-- delete from base_employees where employeekey in (5113)

-- 4/12
stacey piche full to part time on 4/14
mitch rogers hourly to commission on 4/17
use current row for both of them
-- this gives me both non current rows
delete
-- select * 
from base_employees 
where not currentrow
  and row_thru_date <= '04/18/2020';
---------------------------------------------------------
--/> dup employee numbers
---------------------------------------------------------  

---------------------------------------------------------
--< clock hours
---------------------------------------------------------

drop table if exists clock_hours;
create temp table clock_hours as
select a.store_code, a.employee_number, b.from_date, b.thru_date, 
  sum(a.clock_hours + a.vac_hours + a.pto_hours + a.hol_hours) as clock_hours
from arkona.xfm_pypclockin a
join base_employees aa on a.employee_number = aa.employee_number
join weeks b on a.the_date between b.from_date and b.thru_date
group by a.store_code, a.employee_number, b.from_date, b.thru_date;   

---------------------------------------------------------
--/> clock hours
---------------------------------------------------------
drop table if exists jeri.weekly_fte_scenario_2_20200412;
create table jeri.weekly_fte_scenario_2_20200412 (
  store citext not null,
  employee_number citext primary key,
  name citext not null,
  dist_code citext not null,
  hire_date date not null, 
  term_date date,
  payroll_class citext not null,
  full_part citext not null,
  clock_hours numeric (8,2) not null,
  fte numeric(3,1) not null);
-- eliminate the employed field, if employed during this week, they are in the set
insert into jeri.weekly_fte_scenario_2_20200412
select aa.*,
  case --fte calculation
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and dist_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and dist_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when aa.clock_hours = 0 then 0
        when aa.clock_hours >= 40 then 1
        when aa.clock_hours < 40 then round(aa.clock_hours/40, 1)
      end    
  end as fte  
from (
  select a.store, a.employee_number, a.name, a.dist_code, a.hire_Date, 
    case
      when daterange ('04/12/2020', '04/18/2020', '[]') @> term_date then term_date
      when not daterange ('04/12/2020', '04/18/2020', '[]') @> term_date then null
    end as term_date,
    a.payroll_class, 
    case fullparttimecode
      when 'A' then 'full'
      when 'P' then 'part'
    end as full_part, coalesce(clock_hours, 0) as clock_hours
  from base_employees a
  left join clock_hours b on a.employee_number = b.employee_number) aa
-- order by store, payroll_class, dist_code, full_part, store, name  
order by store, name;

-- spreadsheets
select store,employee_number as "emp #",name,dist_Code,hire_date,term_date,
  payroll_class,full_part,clock_hours,fte 
from jeri.weekly_fte_scenario_2_20200412 where store = 'RY1'
union
select null,null,null,null,null,null,null,null,null,sum(fte) from jeri.weekly_fte_scenario_2_20200412 where store = 'RY1'
order by store, name nulls last;
