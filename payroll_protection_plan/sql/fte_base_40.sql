﻿----------------------------------------------------------------------------
-- 06/18/20 
----------------------------------------------------------------------------
-- this is jeri's new request
created function jeri.get_fte_base_40(daterange)

The original calculation for FTE appears like it may be the most correct given the limited guidance available right now.  
This is the report you were doing around the middle of April, but we are using 40 hours instead of the 30 that were started out with.  
Would you be able to rerun that report for the time periods below but change the hours to 40?  
I think I got the rules we were using included here.  

This information would be gathered by week for the following periods:

By week for Feb – June 2019
By week for 2020 beginning in Jan & ongoing

Separate tabs for GM/HN

Jim Price should always be FTE = 0 (code is 26) as we only give him active status to write 1 check/year.


For employees active at any point during the week:
•	If clocking hours (hourly and commission clocking hours)
o	If clock hours 40+, then FTE = 1.0
o	If clock hours <40, then FTE = weekly clock hours/40
•	If not clocking hours
o	Salary and full time, FTE = 1.0
o	Salary and part-time, FTE = 0.5
o	If commission (not clocking hours) and full-time, FTE = 1.0
o	Commission and part-time with following distribution codes, FTE = 0.5
	GM store:  SALM, SALE, TEAM, OWN, 25, AFTE, BSM, RAO, UCM, USED
	HN store:  MGR, SALE, TEAM, OWN


**clock hours are defined as Reg/OT/HOL/PTO
**active employees are non-termed employees

i need a flow chart of all the moving pieces
weekly_report.py
    1. select jon.weekly_fte()
        1a. truncates AND populates jeri.fte_employee_tmp with employee data for the week
        1b. check for dup employeenumber and handle as relevant to the specific week
        1c. truncates and populates jeri.fte_clock_hours_tmp with clock hours for the week
        1d. perform jeri.get_fte_scenario_1(_the_date_range)
            truncates and populates jeri.fte_tmp  with data from jeri.fte_employees_tmp and
                jeri.fte_clock_hours_tmp and fte based on scenario 1 rules
        1e. creates a table in the format of jon.fte_05_17_scenario_1 and populates it with
            data from jeri.fte_tmp
        1f. perform jeri.get_fte_scenario_2(_the_date_range)
            truncates and populates jeri.fte_tmp  with data from jeri.fte_employees_tmp and
                jeri.fte_clock_hours_tmp and fte based on scenario 2 rules      
        1g. creates a table in the format of jon.fte_05_17_scenario_2 and populates it with
            data from jeri.fte_tmp                      
    2. select * from jon.weekly_fte_spreadsheets()
        2a. returns a table of a single row: 858;2020-06-07;2020-06-13;jon.fte_06_07_scenario_1;jon.fte_06_07_scenario_2

    3. creates 2 excel workbooks: fte_0607_scenario_1.xlsx &  fte_0607_scenario_2.xlsx, each with a worksheet for RY1 & RY2
        populated with data from the tables created in 1e & 1g and saves them to E:\python projects\misc_sql\fte
        
----------------------------------------------------------------------------
-- 06/18/20 
----------------------------------------------------------------------------

-----------------------------------------------------------------------
--< orig_40 for all 47 weeks jeri requested
-- the objective here is to have a permanent record of ALL the data used
--    to generate fte reports
-----------------------------------------------------------------------
drop table if exists jeri.fte_weeks;
create table jeri.fte_weeks (
  week_id integer primary key,
  the_week daterange not null,
  from_date date not null,
  thru_date date not null);
create index on the_weeks using GIST(the_week);  
insert into jeri.fte_weeks
select sunday_to_saturday_week as week_id, daterange(min(the_date), max(the_date), '[]') as the_week,
  min(the_date) as from_date, max(the_date) as thru_date
from dds.dim_date
where sunday_to_saturday_week between 787 and 809
  or sunday_to_saturday_week between 835 and 858
group by sunday_to_saturday_week;
comment on table jeri.fte_weeks is 'week data for generating fte census data based on sunday to saturday weeks';

drop table if exists jeri.fte_census_tmp cascade;
create table jeri.fte_census_tmp (
  week_id integer not null,
  the_week daterange not null,
  from_date date not null,
  thru_date date not null,
  employee_key integer not null,
  store citext not null,
  employee_number citext not null,
  employee_name citext not null,
  hire_date date not null,
  term_date date not null,
  dist_code citext not null,
  payroll_class citext not null,
  active_code citext not null,
  full_part_code citext not null,
  current_row boolean not null,
  row_from_date date not null,
  row_thru_date date not null,
  primary key(week_id,employee_key));
create index on jeri.fte_census_tmp(from_date);
create index on jeri.fte_census_tmp(thru_date);
create index on jeri.fte_census_tmp(store);
create index on jeri.fte_census_tmp(employee_number);
create index on jeri.fte_census_tmp(week_id);
comment on table jeri.fte_census_tmp is 'staging table for fte census data, employee data for the relevant week(s)';

insert into jeri.fte_census_tmp
select a.*, employeekey, storecode as store, employeenumber as employee_number, name, 
    hiredate as hire_date, termdate as term_date, distcode as dist_code,
    payrollclasscode as payroll_class, activecode, fullparttimecode, currentrow, 
    employeekeyfromdate as row_from_date, employeekeythrudate as row_thru_date
from jeri.fte_weeks a
join ads.ext_edw_employee_dim b on true
where employeenumber <> '1112425' -- exclude jim price
  and hiredate < a.thru_date  -- before the end
  and termdate > a.from_date -- after the start
  and daterange (employeekeyfromdate, employeekeythrudate, '[]') && a.the_week;

drop table if exists jeri.fte_census_tmp_dups;
create table jeri.fte_census_tmp_dups(
  week_id integer not null,
  from_date date not null,
  thru_date date not null,
  employee_key integer not null,
  employee_number citext not null,
  employee_name citext not null,
  dist_code citext not null,
  payroll_class citext not null,
  active_code citext not null,
  full_part_code citext not null,
  current_row boolean not null,
  row_from_date date not null,
  row_thru_date date not null);
alter table jeri.fte_census_tmp_dups add primary key(week_id,employee_key);
comment on table jeri.fte_census_tmp_dups is 'permanent record of rows from jeri.fte_census_tmp where there are 
  multiple instances of an employee_number within a week'; 

insert into jeri.fte_census_tmp_dups
select a.week_id, a.from_Date, a.thru_date, a.employee_key, a.employee_number, 
  a.employee_name, a.dist_code, a.payroll_class, a.active_code, a.full_part_code, 
  a.current_row, a.row_From_Date, a.row_thru_date
from jeri.fte_census_tmp a
join (
  select week_id, employee_number  
  from jeri.fte_census_tmp
  group by week_id, employee_number
  having count(*) > 1) b on a.week_id = b.week_id
    and a.employee_number = b.employee_number
order by a.week_id, a.employee_number, a.employee_key;
 

-- this is the unavoidably manual part
select * from jeri.fte_census_tmp_dups;
-- have to manually scan through all the dups and 
-- manually add them to jeri.fte_census_tmp_dups_rows_to_delete
drop table if exists jeri.fte_census_tmp_dups_rows_to_delete;
create table jeri.fte_census_tmp_dups_rows_to_delete (
  week_id integer,
  employee_key integer,
  primary key(week_id,employee_key),
  foreign key(week_id,employee_key) references jeri.fte_census_tmp_dups(week_id,employee_key));

-- ok, this process looks ok
-- to get caught up with all the history
-- process a week at a time
-- the week_ids: between 787 and 809,  between 835 and 858

select * 
from jeri.fte_census_tmp_dups
where week_id = 858;
-- and add the appropriate rows to jeri.fte_census_tmp_dups_rows_to_delete
insert into jeri.fte_census_tmp_dups_rows_to_delete values 
(858,4742),
(858,5059),
(858,4811),
(855,5150),
(856,4780),
(857,4474),
(857,5057),
(857,5156),
(852,5025),
(852,3677),
(852,4925),
(852,5081),
(852,5126);
(808,4665),
(808,4716),
(808,4268),
(808,4634);  

-- and delete the superfluous rows
delete 
-- select *
from jeri.fte_census_tmp a
where exists (
  select 1
  from jeri.fte_census_tmp_dups_rows_to_delete
  where week_id = a.week_id
    and employee_key = a.employee_key)
returning *;  

-- looks good
select a.store, a.employee_number, week_id,
  sum(b.clock_hours + b.vac_hours + b.pto_hours + b.hol_hours) as clock_hours
from jeri.fte_census_tmp a
left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
  and b.the_Date between a.from_date and a.thru_date
group by a.store, a.employee_number, a.week_id


-- this is the new version of jeri.fte_tmp
-- and the final archival destination from fte census data, minus the fte, the fte is a decorator
-- to this data and is variable derived, eg, original, scenario 1, scenario 2, base 40 etc.
-- any new set of rules can be applied against this data to derive the desired fte
-- as the weeks pass, this data will be augmented with the census data for the new weeks
drop table if exists jeri.fte_census_data;
create table jeri.fte_census_data (
  week_id integer not null,
  the_week daterange  not null,
  from_date date not null,
  thru_date date not null,
  store citext  not null,
  employee_number citext not null,
  employee_name citext not null,
  dist_code citext not null,
  hire_date date not null,
  term_date date,
  payroll_class citext not null,
  full_part citext not null,
  clock_hours numeric (8,2) not null,
  primary key(week_id,employee_number));

insert into jeri.fte_census_data
select a.week_id, a.the_week, a.from_date, a.thru_date, a.store, 
  a.employee_number, a.employee_name, a.dist_code, a.hire_date, 
  case
    when a.the_week @> term_date then term_date
    when not a.the_week @> term_date then null
  end as term_date,
  a.payroll_class, 
  case a.full_part_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as full_part, coalesce(b.clock_hours, 0) as clock_hours
from jeri.fte_census_tmp a
join (  -- add the clock hours
  select a.store, a.employee_number, week_id,
    sum(b.clock_hours + b.vac_hours + b.pto_hours + b.hol_hours) as clock_hours
  from jeri.fte_census_tmp a
  left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
    and b.the_Date between a.from_date and a.thru_date
  group by a.store, a.employee_number, a.week_id) b on a.week_id = b.week_id 
    and a.employee_number = b.employee_number;

function jeri.fte_orig_40_get_spreadsheet_table(text, integer) populates jeri.fte_tmp 
from jeri.fte_census_data using the orig_40 rules

-----------------------------------------------------------------------
--/> orig_40 for all 47 weeks jeri requested
-----------------------------------------------------------------------

-- summary data, based on FUNCTION jeri.fte_orig_40_get_spreadsheet_table()
select store, from_date, sum(clock_hours) as clock_hours, sum(fte) as fte
from (
  select store, from_date, clock_hours, 
      case 
        when payroll_class = 'H' then -- all hourly
          case 
            when clock_hours = 0 then 0
            when clock_hours >= 40 then 1
            when clock_hours < 40 then round(clock_hours/40, 1)
          end
        when dist_code = '26' then 0 -- jim price
        when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
            (store = 'RY1' and dist_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
            or
            (store = 'ry2' and dist_code in ('mgr','sale','team','own'))) then 1
        when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
            (store = 'RY1' and dist_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
            or
            (store = 'ry2' and dist_code in ('mgr','sale','team','own'))) then .5    
        -- full/part doesn't matter, what matters is the number of hours
        when payroll_class = 'C' and (  -- commission with clock hours
            (store = 'RY1' and dist_code in ('BTEC','CWAS','STEC','WTEC'))
            or
            (store = 'ry2' and dist_code in ('DETA','TECH'))) then 
          case 
            when clock_hours = 0 then 0
            when clock_hours >= 40 then 1
            when clock_hours < 40 then round(clock_hours/40, 1)
          end 
        when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
        when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time            
      end as fte    
  from jeri.fte_census_data) a
group by store, from_date
order by store, from_date;