﻿-- for each new week
-- 1. add a row to jeri.fte_weeks
insert into jeri.fte_weeks
select sunday_to_saturday_week as week_id, daterange(min(the_date), max(the_date), '[]') as the_week,
  min(the_date) as from_date, max(the_date) as thru_date
from dds.dim_date
where sunday_to_saturday_week = (
  select sunday_to_saturday_week -1
  from dds.dim_date
  where the_date = current_date)
group by sunday_to_saturday_week;

-- select max(week_id) from jeri.fte_weeks

-- 2. truncate and populate jeri.fte_census_tmp with data for the new week
truncate jeri.fte_census_tmp;
insert into jeri.fte_census_tmp
select a.*, employeekey, storecode as store, employeenumber as employee_number, name, 
    hiredate as hire_date, termdate as term_date, distcode as dist_code,
    payrollclasscode as payroll_class, activecode, fullparttimecode, currentrow, 
    employeekeyfromdate as row_from_date, employeekeythrudate as row_thru_date
from jeri.fte_weeks a
join ads.ext_edw_employee_dim b on true
where employeenumber <> '1112425' -- exclude jim price
  and hiredate < a.thru_date  -- before the end
  and termdate > a.from_date -- after the start
  and daterange (employeekeyfromdate, employeekeythrudate, '[]') && a.the_week
  and a.week_id = 866; -- ******************************************************************************

-- 3. add any relevant rows from jeri.fte_census_tmp to jeri.fte_census_tmp_dups
-- DO NOT TRUNCATE
insert into jeri.fte_census_tmp_dups
select a.week_id, a.from_Date, a.thru_date, a.employee_key, a.employee_number, 
  a.employee_name, a.dist_code, a.payroll_class, a.active_code, a.full_part_code, 
  a.current_row, a.row_From_Date, a.row_thru_date
from jeri.fte_census_tmp a
join (
  select week_id, employee_number  
  from jeri.fte_census_tmp
  group by week_id, employee_number
  having count(*) > 1) b on a.week_id = b.week_id
    and a.employee_number = b.employee_number
order by a.week_id, a.employee_number, a.employee_key;

-- 4. using the above insert the rows to delete into jeri.fte_census_tmp_dups_rows_to_delete
select * from jeri.fte_census_tmp_dups where week_id = 866; -- *******************************************

insert into jeri.fte_census_tmp_dups_rows_to_delete values 
(866,5214),
(866,5185),
(866,5056);

-- 5. and delete the relevant rows from jeri.fte_census_tmp
delete 
-- select *
from jeri.fte_census_tmp a
where exists (
  select 1
  from jeri.fte_census_tmp_dups_rows_to_delete
  where week_id = a.week_id
    and employee_key = a.employee_key)
returning *;  


-- 6. add the new data to jeri.fte_census_data
insert into jeri.fte_census_data
select a.week_id, a.the_week, a.from_date, a.thru_date, a.store, 
  a.employee_number, a.employee_name, a.dist_code, a.hire_date, 
  case
    when a.the_week @> term_date then term_date
    when not a.the_week @> term_date then null
  end as term_date,
  a.payroll_class, 
  case a.full_part_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as full_part, coalesce(b.clock_hours, 0) as clock_hours
from jeri.fte_census_tmp a
join (  -- add the clock hours
  select a.store, a.employee_number, week_id,
    sum(b.clock_hours + b.vac_hours + b.pto_hours + b.hol_hours) as clock_hours
  from jeri.fte_census_tmp a
  left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
    and b.the_Date between a.from_date and a.thru_date
  group by a.store, a.employee_number, a.week_id) b on a.week_id = b.week_id 
    and a.employee_number = b.employee_number;

-- 7. fte - base_40_history.py, generates the spreadsheet(s) for the selected week(s)


select * from jeri.fte_census_data
  