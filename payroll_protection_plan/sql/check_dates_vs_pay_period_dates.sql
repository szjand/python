﻿-- The only things this question affects is the denominator used to determine FTE on FTE Comparison,
-- the denominator used in Wage Comparison for Annualized Salary, Gross Pay with Cap,
-- and the denominator used in Payroll Cost for Gross Pay in Excess of 100K.
-- 
-- Which means it easy to regenerate any of the reports with a substitution of the denominator.
-- 
-- The attached spreadsheet shows, for each of the periods in FTE Comparison, determined 
-- by check dates, the actual pay periods that encompass all checks with a check date in 
-- the defined period.
-- 
-- So, 
-- for the covered period of 4/15/20 thru 7/10/20, there were actual 16 checks all of which
-- were covered by 6 pay periods.
-- At 2 weeks per pay period, that is 12 weeks, not 13 or 14.
-- 
-- Etc.
-- 
-- The question that I would like to have answered, if possible, is, for Monday morning, what would you like the generated to use for a denominator?
-- 
-- I am only sending this to you Greg, leaving the matter of forwarding it to Jeri up to you.
-- 
-- Just in case I am completely full of shit and there is no good reason to bother her on the weekend.
-- 
-- You, of course, remain botherable at any time on any day.
-- 
-- Assuming you actually consider all of this
-- 
-- Thanks
-- 
-- jon

-- ok, the payroll_end_date in pyhshdta matches with the actual biweekly payroll end dates
select *
from (
  select distinct (('20'||payroll_ending_year)::text||'-'||payroll_ending_month::text||'-'||payroll_ending_day::text)::date as payroll_end_date
  from arkona.ext_pyhshdta a
  where check_year  in (19,20)) aa
where not exists (
  select 1
    from dds.dim_Date
    where biweekly_pay_period_end_date <> aa.payroll_end_Date)

-- for the given check dates, here are the actual pay periods involved
-- covered period: check dates 04/15/20 -> 07/10/20
-- 6 pay periods: 12 weeks
select distinct biweekly_pay_period_start_date, biweekly_pay_period_end_date, array_to_string(array_agg(distinct b.check_date),',') as check_dates
from dds.dim_date a
join (
select distinct -- check_year, check_month, check_day, payroll_ending_year, payroll_ending_month, payroll_ending_day,
  (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date,
  (('20'||payroll_ending_year)::text||'-'||payroll_ending_month::text||'-'||payroll_ending_day::text)::date as payroll_end_date
from arkona.ext_pyhshdta
where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between  '04/15/2020'::date and '07/10/2020'::date) b on b.payroll_end_date between a.biweekly_pay_period_start_date and a.biweekly_pay_period_end_date
group by biweekly_pay_period_start_date, biweekly_pay_period_end_date  
order by biweekly_pay_period_start_date;     

-- look back period 1 check dates 02/15/2019 -> 06/30/2019
-- 10 pay periods 20 weeks
select distinct biweekly_pay_period_start_date, biweekly_pay_period_end_date, array_to_string(array_agg(distinct b.check_date),',') as check_dates
from dds.dim_date a
join (
select distinct -- check_year, check_month, check_day, payroll_ending_year, payroll_ending_month, payroll_ending_day,
  (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date,
  (('20'||payroll_ending_year)::text||'-'||payroll_ending_month::text||'-'||payroll_ending_day::text)::date as payroll_end_date
from arkona.ext_pyhshdta
where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between  '02/15/2019'::date and '06/30/2019'::date) b on b.payroll_end_date between a.biweekly_pay_period_start_date and a.biweekly_pay_period_end_date
group by biweekly_pay_period_start_date, biweekly_pay_period_end_date  
order by biweekly_pay_period_start_date;    

-- look back period 2 check dates 01/10/2020 -> 02/29/2020
-- 5 pay periods 10 weeks
select distinct biweekly_pay_period_start_date, biweekly_pay_period_end_date, array_to_string(array_agg(distinct b.check_date), ',') as check_dates
from dds.dim_date a
join (
select distinct -- check_year, check_month, check_day, payroll_ending_year, payroll_ending_month, payroll_ending_day,
  (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date,
  (('20'||payroll_ending_year)::text||'-'||payroll_ending_month::text||'-'||payroll_ending_day::text)::date as payroll_end_date
from arkona.ext_pyhshdta
where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between  '01/10/2020'::date and '02/29/2020'::date) b on b.payroll_end_date between a.biweekly_pay_period_start_date and a.biweekly_pay_period_end_date
group by biweekly_pay_period_start_date, biweekly_pay_period_end_date  
order by biweekly_pay_period_start_date      


-- look at check count for covered period including period, 
-- looking for the difference between B and S
select count(distinct check_date), a.store, a.employee_number, a.employee_name, a.ssn,a.hire_date, a.term_date, a.pay_period, a.payroll_class, 
  sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) as hours,
  array_agg(distinct check_date order by check_date)
from ppp.employees_fte_comparison a
left join (
  select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
    reg_hours, overtime_hours, alt_pay_hours,
    (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
  from arkona.ext_pyhshdta
  where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date
  order by employee_ , (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) b on a.employee_number = b.employee_ 
group by a.employee_number, a.employee_name, a.pay_period, a.payroll_class,a.ssn,a.hire_date,a.term_date
order by count(*)

-- 04/15/2020 - 07/10/2020  S 6 checks majority
select count(distinct check_date), a.store, a.employee_number, a.employee_name, a.ssn,a.hire_date, a.term_date, a.pay_period, a.payroll_class, 
  sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) as hours,
  array_agg(distinct check_date order by check_date)
from ppp.employees_fte_comparison a
left join (
  select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
    reg_hours, overtime_hours, alt_pay_hours,
    (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
  from arkona.ext_pyhshdta
  where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date
  order by employee_ , (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) b on a.employee_number = b.employee_ 
where a.pay_period = 'S'  
group by a.employee_number, a.employee_name, a.pay_period, a.payroll_class,a.ssn,a.hire_date,a.term_date
order by count(*)


--  04/15/2020 - 07/10/2020  B 7 checks majority
select count(distinct check_date), a.store, a.employee_number, a.employee_name, a.ssn,a.hire_date, a.term_date, a.pay_period, a.payroll_class, 
  sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) as hours,
  array_agg(distinct check_date order by check_date)
from ppp.employees_fte_comparison a
left join (
  select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
    reg_hours, overtime_hours, alt_pay_hours,
    (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
  from arkona.ext_pyhshdta
  where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date
  order by employee_ , (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) b on a.employee_number = b.employee_ 
where a.pay_period = 'B'  
group by a.employee_number, a.employee_name, a.pay_period, a.payroll_class,a.ssn,a.hire_date,a.term_date
order by count(*)  


-- '02/15/2019' - '06/30/2019' B & S = 10 checks
select count(distinct check_date), a.store, a.employee_number, a.employee_name, a.ssn,a.hire_date, a.term_date, a.pay_period, a.payroll_class, 
  sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) as hours,
  array_agg(distinct check_date order by check_date)
from ppp.employees_fte_comparison a
left join (
  select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
    reg_hours, overtime_hours, alt_pay_hours,
    (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
  from arkona.ext_pyhshdta
  where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '02/15/2019'::date and '06/30/2019'::date
  order by employee_ , (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) b on a.employee_number = b.employee_ 
where a.pay_period = 'S'  
group by a.employee_number, a.employee_name, a.pay_period, a.payroll_class,a.ssn,a.hire_date,a.term_date
order by count(*)  

-- 01/10/2020 -  02/29/2020  B/S = 4 checks
select count(distinct check_date), a.store, a.employee_number, a.employee_name, a.ssn,a.hire_date, a.term_date, a.pay_period, a.payroll_class, 
  sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) as hours,
  array_agg(distinct check_date order by check_date)
from ppp.employees_fte_comparison a
left join (
  select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
    reg_hours, overtime_hours, alt_pay_hours,
    (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
  from arkona.ext_pyhshdta
  where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date
  order by employee_ , (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) b on a.employee_number = b.employee_ 
where a.pay_period = 'B'  
group by a.employee_number, a.employee_name, a.pay_period, a.payroll_class,a.ssn,a.hire_date,a.term_date
order by count(*)  


-- 01/10/2020 -  03/31/2020  B/S = 6 checks
select count(distinct check_date), a.store, a.employee_number, a.employee_name, a.ssn,a.hire_date, a.term_date, a.pay_period, a.payroll_class, 
  sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours) as hours,
  array_agg(distinct check_date order by check_date)
from ppp.employees_fte_comparison a
left join (
  select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
    reg_hours, overtime_hours, alt_pay_hours,
    (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date  
  from arkona.ext_pyhshdta
  where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '03/31/2020'::date
  order by employee_ , (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) b on a.employee_number = b.employee_ 
where a.pay_period = 'B'  
group by a.employee_number, a.employee_name, a.pay_period, a.payroll_class,a.ssn,a.hire_date,a.term_date
order by count(*)  




-- kim attributes 80 reg_hours for each check, this seems dodgy, but oh well
select * from  arkona.ext_pyhshdta where employee_name like 'AUBOL%' and check_year = 20 and check_month = 6

-- does jeri consider S to be monthly?