﻿-- have to go weekly
drop table if exists weeks;
create temp table weeks as
select daterange(min(the_date), max(the_date), '[]') as week_range, 
  min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week
from dds.dim_date 
where the_date between '03/29/2020' and '04/04/2020'
group by sunday_to_saturday_week
order by sunday_to_saturday_week;

select * from ads.ext_edw_employee_dim limit 100
-- start with a single week, '02/10/2019', '02/16/2019'
drop table if exists base_employees;
create temp table base_employees as
select employeekey, storecode as store, employeenumber as employee_number, name, 
  hiredate as hire_date, termdate as term_date, distcode as dist_code,
  payrollclasscode as payroll_class, activecode, fullparttimecode, currentrow, 
  employeekeyfromdate as row_from_date, employeekeythrudate as row_thru_date,
  daterange (employeekeyfromdate, employeekeythrudate, '[]') as from_thru,
  daterange(hiredate, termdate, '[]') as hire_term,
  (select distinct sunday_to_saturday_week from dds.dim_date where the_Date = '03/29/2020')
from ads.ext_edw_employee_dim
where hiredate < '04/04/2019'  -- before the end
  and termdate > '03/29/2019'; -- after the start

-- these are the rows where the row_from_date overlaps the defined date range
drop table if exists relevant_employees;
create temp table relevant_employees as
select *  -- 400
from base_employees
where from_thru && daterange('03/29/2020','04/04/2020', '[]')
order by store, name;

-- multiple rows for employee_number
select *
from relevant_employees a
join (
select employee_number
from relevant_employees
group by employee_number having count(*) > 1) b on a.employee_number = b.employee_number
order by a.employee_number, employeekey
-- pick the employee_number row i want to keep, basically, the one most relevant for the most of of the week
delete from relevant_employees
where employeekey in (974,5110)

-- multiple rows for name
select *
from relevant_employees a
join (
select name
from relevant_employees
group by name having count(*) > 1) b on a.name = b.name
order by a.name, employeekey
-- this one is messier, most of them are employees with an employee_number for both stores
-- so, in a separate window, look at the employees entire record in dim_employee and pick 
-- the one makes most sense for this week
-- select employeekey,store, employeenumber, name, termdate,employeekeyfromdate, employeekeythrudate,currentrow
-- from ads.ext_edw_employee_dim
-- where name = 'BEDNEY, TYLER'
-- order by employeekey
delete from relevant_employees
where employeekey in (4722,4694,4290,4658,4676,4504,4837);

drop table if exists jon.fte_employee_data;
create table jon.fte_employee_data (
  employee_key integer, 
  store citext, 
  employee_number citext,
  name citext,
  dist_code citext,
  hire_date date,
  term_date date, 
  payroll_class citext,
  full_part citext,
  week_id integer,
  primary key (employee_number,week_id));
create unique index on jon.fte_employee_data(employee_key,week_id)  ;
create unique index on jon.fte_employee_data(name,week_id)  ; 

insert into jon.fte_employee_data
select employeekey,store,employee_number,name,dist_code,hire_date,term_date, payroll_class,fullparttimecode,sunday_to_saturday_week
from relevant_employees;


select aa.*, coalesce(bb.clock_hours, 0)
from jon.fte_employee_data aa
left join (
  select a.store_code, a.employee_number, 
    sum(a.clock_hours + a.vac_hours + a.pto_hours + a.hol_hours) as clock_hours
  from arkona.xfm_pypclockin a
  join jon.fte_employee_data aa on a.employee_number = aa.employee_number
    and aa.week_id = 789
  join (
    select min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week
    from dds.dim_date 
    where sunday_to_saturday_week = 789
    group by sunday_to_saturday_week) b on a.the_date between b.from_date and b.thru_date
  group by a.store_code, a.employee_number, b.from_date, b.thru_date) bb on aa.employee_number = bb.employee_number 
where aa.week_id = 789
order by aa.store, aa.name



select min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week
from dds.dim_date 
where sunday_to_saturday_week = 789
group by sunday_to_saturday_week








/*

select employeekey,store, employeenumber, name, 
  activecode, fullparttimecode,
  termdate,employeekeyfromdate, employeekeythrudate,currentrow
-- select *
from ads.ext_edw_employee_dim
where name = 'ZEMAN, HUNTER'
order by employeekey

bedney ry1: from 10/19
barta ry1 thru 6/1/19
budeau all ry1
longoria ry1 thru 9/19, then ry2
thibert ry1 til 05/19
zeman ry1 til 09/19 then ry2

select *
from arkona.xfm_pymast
where pymast_employee_number = '211568'
order by pymast_key

*/


select *
from relevant_employees a
left join (
  select a.store_code, a.employee_number, b.from_date, b.thru_date, 
    coalesce(sum(a.clock_hours + a.vac_hours + a.pto_hours + a.hol_hours), 0) as clock_hours
  from arkona.xfm_pypclockin a
  join relevant_employees aa on a.employee_number = aa.employee_number
  join weeks b on a.the_date between b.from_date and b.thru_date
  group by a.store_code, a.employee_number, b.from_date, b.thru_date) b on a.employee_number = b.employee_number  
