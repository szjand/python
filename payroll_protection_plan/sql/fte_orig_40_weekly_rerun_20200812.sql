﻿/*
from jeri  8/11/20
I need to make sure that there have been no changes to this after the week’s report went out.  
Will you please resend me the reports from April 2020 to current by week so that I can finalized these?

*/
-- drop table if exists jeri.fte_census_tmp_rerun_20200812;
-- CREATE TABLE jeri.fte_census_tmp_rerun_20200812(
--   week_id integer NOT NULL,
--   the_week daterange NOT NULL,
--   from_date date NOT NULL,
--   thru_date date NOT NULL,
--   employee_key integer NOT NULL,
--   store citext NOT NULL,
--   employee_number citext NOT NULL,
--   employee_name citext NOT NULL,
--   hire_date date NOT NULL,
--   term_date date NOT NULL,
--   dist_code citext NOT NULL,
--   payroll_class citext NOT NULL,
--   active_code citext NOT NULL,
--   full_part_code citext NOT NULL,
--   current_row boolean NOT NULL,
--   row_from_date date NOT NULL,
--   row_thru_date date NOT NULL,
--   CONSTRAINT fte_census_tmp_rerun_20200812_pkey PRIMARY KEY (week_id, employee_key));
-- 
-- DROP TABLE if exists jeri.fte_census_data_rerun_20200812;
-- CREATE TABLE jeri.fte_census_data_rerun_20200812(
--   week_id integer NOT NULL,
--   the_week daterange NOT NULL,
--   from_date date NOT NULL,
--   thru_date date NOT NULL,
--   store citext NOT NULL,
--   employee_number citext NOT NULL,
--   employee_name citext NOT NULL,
--   dist_code citext NOT NULL,
--   hire_date date NOT NULL,
--   term_date date,
--   payroll_class citext NOT NULL,
--   full_part citext NOT NULL,
--   clock_hours numeric(8,2) NOT NULL,
--   CONSTRAINT fte_census_data_rerun_20200812_pkey PRIMARY KEY (week_id, employee_number));


do $$
declare
  _weekid integer := 866;
  
begin

  truncate jeri.fte_census_tmp_rerun_20200812;
  insert into jeri.fte_census_tmp_rerun_20200812
  select a.*, employeekey, storecode as store, employeenumber as employee_number, name, 
      hiredate as hire_date, termdate as term_date, distcode as dist_code,
      payrollclasscode as payroll_class, activecode, fullparttimecode, currentrow, 
      employeekeyfromdate as row_from_date, employeekeythrudate as row_thru_date
  from jeri.fte_weeks a
  join ads.ext_edw_employee_dim b on true
  where employeenumber <> '1112425' -- exclude jim price
    and hiredate < a.thru_date  -- before the end
    and termdate > a.from_date -- after the start
    and daterange (employeekeyfromdate, employeekeythrudate, '[]') && a.the_week
    and a.week_id = _weekid;

  delete 
  -- select *
  from jeri.fte_census_tmp_rerun_20200812 a
  where exists (
    select 1
    from jeri.fte_census_tmp_dups_rows_to_delete
    where week_id = a.week_id
      and employee_key = a.employee_key);  
--   truncate jeri.fte_census_data_rerun_20200812;
  insert into jeri.fte_census_data_rerun_20200812
  select a.week_id, a.the_week, a.from_date, a.thru_date, a.store, 
    a.employee_number, a.employee_name, a.dist_code, a.hire_date, 
    case
      when a.the_week @> term_date then term_date
      when not a.the_week @> term_date then null
    end as term_date,
    a.payroll_class, 
    case a.full_part_code
      when 'A' then 'full'
      when 'P' then 'part'
    end as full_part, coalesce(b.clock_hours, 0) as clock_hours
  from jeri.fte_census_tmp_rerun_20200812 a
  join (  -- add the clock hours
    select a.store, a.employee_number, week_id,
      sum(b.clock_hours + b.vac_hours + b.pto_hours + b.hol_hours) as clock_hours
    from jeri.fte_census_tmp_rerun_20200812 a
    left join arkona.xfm_pypclockin b on a.employee_number = b.employee_number
      and b.the_Date between a.from_date and a.thru_date
    group by a.store, a.employee_number, a.week_id) b on a.week_id = b.week_id 
      and a.employee_number = b.employee_number;    

  drop table if exists wtf;
  create temp table wtf as
  select store, week_id, 
    sum(
      case 
        when payroll_class = 'H' then -- all hourly
          case 
            when clock_hours = 0 then 0
            when clock_hours >= 40 then 1
            when clock_hours < 40 then round(clock_hours/40, 1)
          end
        when dist_code = '26' then 0 -- jim price
        when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
            (store = 'RY1' and dist_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
            or
            (store = 'ry2' and dist_code in ('mgr','sale','team','own'))) then 1
        when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
            (store = 'RY1' and dist_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
            or
            (store = 'ry2' and dist_code in ('mgr','sale','team','own'))) then .5    
        -- full/part doesn't matter, what matters is the number of hours
        when payroll_class = 'C' and (  -- commission with clock hours
            (store = 'RY1' and dist_code in ('BTEC','CWAS','STEC','WTEC'))
            or
            (store = 'ry2' and dist_code in ('DETA','TECH'))) then 
          case 
            when clock_hours = 0 then 0
            when clock_hours >= 40 then 1
            when clock_hours < 40 then round(clock_hours/40, 1)
          end 
        when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
        when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time            
      end) as fte    
  from jeri.fte_census_data_rerun_20200812 
  group by store, week_id;    
end $$;

-- compares the fte from the orig and rerun data
select *
from (
    select 'orig', store, week_id, 
      sum(
        case 
          when payroll_class = 'H' then -- all hourly
            case 
              when clock_hours = 0 then 0
              when clock_hours >= 40 then 1
              when clock_hours < 40 then round(clock_hours/40, 1)
            end
          when dist_code = '26' then 0 -- jim price
          when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
              (store = 'RY1' and dist_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
              or
              (store = 'ry2' and dist_code in ('mgr','sale','team','own'))) then 1
          when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
              (store = 'RY1' and dist_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
              or
              (store = 'ry2' and dist_code in ('mgr','sale','team','own'))) then .5    
          -- full/part doesn't matter, what matters is the number of hours
          when payroll_class = 'C' and (  -- commission with clock hours
              (store = 'RY1' and dist_code in ('BTEC','CWAS','STEC','WTEC'))
              or
              (store = 'ry2' and dist_code in ('DETA','TECH'))) then 
            case 
              when clock_hours = 0 then 0
              when clock_hours >= 40 then 1
              when clock_hours < 40 then round(clock_hours/40, 1)
            end 
          when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
          when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time            
        end) as fte    
    from jeri.fte_census_data 
  group by store, week_id) x
join wtf y on x.week_id = y.week_id and x.store = y.store  
order by x.week_id, x.store

-- fte has changed
select *
-- for jeri
-- select z.from_date as week_of, x.store, x.fte as orig, y.fte as rerun
from (
    select 'orig', store, week_id, 
      sum(
        case 
          when payroll_class = 'H' then -- all hourly
            case 
              when clock_hours = 0 then 0
              when clock_hours >= 40 then 1
              when clock_hours < 40 then round(clock_hours/40, 1)
            end
          when dist_code = '26' then 0 -- jim price
          when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
              (store = 'RY1' and dist_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
              or
              (store = 'ry2' and dist_code in ('mgr','sale','team','own'))) then 1
          when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
              (store = 'RY1' and dist_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
              or
              (store = 'ry2' and dist_code in ('mgr','sale','team','own'))) then .5    
          -- full/part doesn't matter, what matters is the number of hours
          when payroll_class = 'C' and (  -- commission with clock hours
              (store = 'RY1' and dist_code in ('BTEC','CWAS','STEC','WTEC'))
              or
              (store = 'ry2' and dist_code in ('DETA','TECH'))) then 
            case 
              when clock_hours = 0 then 0
              when clock_hours >= 40 then 1
              when clock_hours < 40 then round(clock_hours/40, 1)
            end 
          when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
          when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time            
        end) as fte    
    from jeri.fte_census_data 
  group by store, week_id) x
join wtf y on x.week_id = y.week_id and x.store = y.store  
join jeri.fte_weeks z on x.week_id = z.week_id
where x.fte <> y.fte
order by x.week_id, x.store


select * from (
select a.*, md5(a::text) as a_hash, b.*, md5(b::text) as b_hash
from jeri.fte_census_data a
full outer join jeri.fte_census_data_rerun_20200812  b on a.week_id = b.week_id
  and a.employee_number = b.employee_number 
where a.week_id = 859
  and a.store = 'RY1'
) x  where co <> b_hash

select a.employee_name, a.employee_number, a.payroll_class, a.full_part, a.clock_hours,
  b.employee_name, b.employee_number, b.payroll_class, b.full_part, b.clock_hours
from jeri.fte_census_data a
full outer join jeri.fte_census_data_rerun_20200812  b on a.week_id = b.week_id
  and a.employee_number = b.employee_number 
where a.week_id = 859
  and a.store = 'RY1'
  and a.clock_hours <> b.clock_hours
  