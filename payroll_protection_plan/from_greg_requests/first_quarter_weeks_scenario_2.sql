﻿

select a.store,a.employee_number, a.name, a.distrib_code, a.hire_date,  a.term_date, 
  a.payroll_class, a.full_part, 
    "12/29-1/4", b.employed, b.fte,
    "1/5-1/11", c.employed, c.fte,
    "1/12-1/18", d.employed, d.fte,
    "1/19-1/25", e.employed, e.fte,
    "1/26-2/1", f.employed, f.fte,
    "2/2-2/8", g.employed, g.fte,
    "2/9-2/15", h.employed, h.fte,
    "2/16-2/22", i.employed, i.fte,
    "2/23-2/29", j.employed, j.fte,
    "3/1-3/7", k.employed, k.fte,
    "3/8-3/14", l.employed, l.fte,
    "3/15-3/21", m.employed, m.fte,
    "3/22-3/28", n.employed, n.fte,
    "3/29-4/4", n.employed, o.fte
from jon.first_quarter_v2 a
left join jon.fq_scenario_2_week_1 b on a.employee_number = b.employee_number
left join jon.fq_scenario_2_week_2 c on a.employee_number = c.employee_number
left join jon.fq_scenario_2_week_3 d on a.employee_number = d.employee_number
left join jon.fq_scenario_2_week_4 e on a.employee_number = e.employee_number
left join jon.fq_scenario_2_week_5 f on a.employee_number = f.employee_number
left join jon.fq_scenario_2_week_6 g on a.employee_number = g.employee_number
left join jon.fq_scenario_2_week_7 h on a.employee_number = h.employee_number
left join jon.fq_scenario_2_week_8 i on a.employee_number = i.employee_number
left join jon.fq_scenario_2_week_9 j on a.employee_number = j.employee_number
left join jon.fq_scenario_2_week_10 k on a.employee_number = k.employee_number
left join jon.fq_scenario_2_week_11 l on a.employee_number = l.employee_number
left join jon.fq_scenario_2_week_12 m on a.employee_number = m.employee_number
left join jon.fq_scenario_2_week_13 n on a.employee_number = n.employee_number
left join jon.fq_scenario_2_week_14 o on a.employee_number = o.employee_number
where a.store = 'ry2'
union
select null,null,null,null, null::date,  null::date, '', '', 
    null::numeric, null::boolean,sum(b.fte),
    null::numeric, null::boolean,sum(c.fte),
    null::numeric, null::boolean,sum(d.fte),
    null::numeric, null::boolean,sum(e.fte),
    null::numeric, null::boolean,sum(f.fte),
    null::numeric, null::boolean,sum(g.fte),
    null::numeric, null::boolean,sum(h.fte),
    null::numeric, null::boolean,sum(i.fte),
    null::numeric, null::boolean,sum(j.fte),
    null::numeric, null::boolean,sum(k.fte),
    null::numeric, null::boolean,sum(l.fte),
    null::numeric, null::boolean,sum(m.fte),
    null::numeric, null::boolean,sum(n.fte),
    null::numeric, null::boolean,sum(o.fte)
from jon.first_quarter_v2 a
left join jon.fq_scenario_2_week_1 b on a.employee_number = b.employee_number
left join jon.fq_scenario_2_week_2 c on a.employee_number = c.employee_number
left join jon.fq_scenario_2_week_3 d on a.employee_number = d.employee_number
left join jon.fq_scenario_2_week_4 e on a.employee_number = e.employee_number
left join jon.fq_scenario_2_week_5 f on a.employee_number = f.employee_number
left join jon.fq_scenario_2_week_6 g on a.employee_number = g.employee_number
left join jon.fq_scenario_2_week_7 h on a.employee_number = h.employee_number
left join jon.fq_scenario_2_week_8 i on a.employee_number = i.employee_number
left join jon.fq_scenario_2_week_9 j on a.employee_number = j.employee_number
left join jon.fq_scenario_2_week_10 k on a.employee_number = k.employee_number
left join jon.fq_scenario_2_week_11 l on a.employee_number = l.employee_number
left join jon.fq_scenario_2_week_12 m on a.employee_number = m.employee_number
left join jon.fq_scenario_2_week_13 n on a.employee_number = n.employee_number
left join jon.fq_scenario_2_week_14 o on a.employee_number = o.employee_number
where a.store = 'ry2'
order by store, name




drop table if exists jon.fq_scenario_2_week_1;
create table jon.fq_scenario_2_week_1 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_1 
select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('12/29/2019','01/04/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('12/29/2019','01/04/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "12/29-1/4" = 0 then 0
        when "12/29-1/4" >= 40 then 1
        when "12/29-1/4" < 40 then round("12/29-1/4"/40, 1)
      end    
  end as fte, daterange('12/29/2019','01/04/2020', '[]')
from jon.first_quarter_v2 a;


create table jon.fq_scenario_2_week_2 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_2 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('01/05/2020','01/11/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('01/05/2020','01/11/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "1/5-1/11" = 0 then 0
        when "1/5-1/11" >= 40 then 1
        when "1/5-1/11" < 40 then round("1/5-1/11"/40, 1)
      end    
  end as fte, daterange('01/05/2020','01/11/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_2;

create table jon.fq_scenario_2_week_3 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_3 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('01/12/2020','01/18/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('01/12/2020','01/18/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "1/12-1/18" = 0 then 0
        when "1/12-1/18" >= 40 then 1
        when "1/12-1/18" < 40 then round("1/12-1/18"/40, 1)
      end    
  end as fte, daterange('01/12/2020','01/18/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_3;

create table jon.fq_scenario_2_week_4 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_4 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('01/19/2020','01/25/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('01/19/2020','01/25/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "1/19-1/25" = 0 then 0
        when "1/19-1/25" >= 40 then 1
        when "1/19-1/25" < 40 then round("1/19-1/25"/40, 1)
      end    
  end as fte, daterange('01/19/2020','01/25/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_4;

create table jon.fq_scenario_2_week_5 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_5 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('01/26/2020','02/01/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('01/26/2020','02/01/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "1/26-2/1" = 0 then 0
        when "1/26-2/1" >= 40 then 1
        when "1/26-2/1" < 40 then round("1/26-2/1"/40, 1)
      end    
  end as fte, daterange('01/26/2020','02/01/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_5;

create table jon.fq_scenario_2_week_6 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_6 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('02/02/2020','02/08/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('02/02/2020','02/08/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "2/2-2/8" = 0 then 0
        when "2/2-2/8" >= 40 then 1
        when "2/2-2/8" < 40 then round("2/2-2/8"/40, 1)
      end    
  end as fte, daterange('02/02/2020','02/08/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_6;

create table jon.fq_scenario_2_week_7 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_7 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('02/09/2020','02/15/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('02/09/2020','02/15/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "2/9-2/15" = 0 then 0
        when "2/9-2/15" >= 40 then 1
        when "2/9-2/15" < 40 then round("2/9-2/15"/40, 1)
      end    
  end as fte, daterange('02/09/2020','02/15/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_7;


create table jon.fq_scenario_2_week_8 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_8 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('02/16/2020','02/22/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('02/16/2020','02/22/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "2/16-2/22" = 0 then 0
        when "2/16-2/22" >= 40 then 1
        when "2/16-2/22" < 40 then round("2/16-2/22"/40, 1)
      end    
  end as fte, daterange('02/16/2020','02/22/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_8;


create table jon.fq_scenario_2_week_9 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_9 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('02/23/2020','02/29/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('02/23/2020','02/29/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "2/23-2/29" = 0 then 0
        when "2/23-2/29" >= 40 then 1
        when "2/23-2/29" < 40 then round("2/23-2/29"/40, 1)
      end    
  end as fte, daterange('02/23/2020','02/29/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_9;


create table jon.fq_scenario_2_week_10 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_10 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('03/01/2020','03/07/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('03/01/2020','03/07/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "3/1-3/7" = 0 then 0
        when "3/1-3/7" >= 40 then 1
        when "3/1-3/7" < 40 then round("3/1-3/7"/40, 1)
      end    
  end as fte, daterange('03/01/2020','03/07/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_10;

create table jon.fq_scenario_2_week_11 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_11 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('03/08/2020','03/14/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('03/08/2020','03/14/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "3/8-3/14" = 0 then 0
        when "3/8-3/14" >= 40 then 1
        when "3/8-3/14" < 40 then round("3/8-3/14"/40, 1)
      end    
  end as fte, daterange('03/08/2020','03/14/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_11;


create table jon.fq_scenario_2_week_12 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_12 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('03/15/2020','03/21/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('03/15/2020','03/21/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "3/15-3/21" = 0 then 0
        when "3/15-3/21" >= 40 then 1
        when "3/15-3/21" < 40 then round("3/15-3/21"/40, 1)
      end    
  end as fte, daterange('03/15/2020','03/21/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_12;


create table jon.fq_scenario_2_week_13 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_13 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('03/22/2020','03/28/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('03/22/2020','03/28/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "3/22-3/28" = 0 then 0
        when "3/22-3/28" >= 40 then 1
        when "3/22-3/28" < 40 then round("3/22-3/28"/40, 1)
      end    
  end as fte, daterange('03/22/2020','03/28/2020', '[]')
from jon.first_quarter_v2 a
order by store, name;  
select * from jon.fq_scenario_2_week_13;

drop table if exists jon.fq_scenario_2_week_14;
create table jon.fq_scenario_2_week_14 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_scenario_2_week_14 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('03/29/2020','04/04/2020', '[]') then true else false end as employed,
  case --fte calculation
    when not daterange(hire_date, term_date, '[]') && daterange('03/29/2020','04/04/2020', '[]') then 0  -- not employed
    when full_part = 'full' then 1 -- all full time
    when full_part = 'part' and ( -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
    else
      case 
        when "3/29-4/4" = 0 then 0
        when "3/29-4/4" >= 40 then 1
        when "3/29-4/4" < 40 then round("3/29-4/4"/40, 1)
      end    
  end as fte, daterange('03/29/2020','04/04/2020', '[]')
from jon.first_quarter_v2 a;