﻿-----------------------------------------------------------------------------------
--< this is the fix for 4/17
-----------------------------------------------------------------------------------
check date 04/17 3 batches 417200, 417201, 417202

select * from arkona.ext_pyhshdta where payroll_run_number = 417202

select payroll_run_number, count(*) from arkona.ext_pyhshdta where payroll_run_number in (417200,417201,417202) group by payroll_run_number

register
select 479536.98 + 1155.22 = 480692.20
jon
select 478450.48 + 1155.22 = 479605.70
diff
select 480692.20 - 479605.70 - 1086.5
2 missing employees in report riskey & tomlin

08/12 per jeri include the 0J check(s)
select 479536.98 + 1155.22 = 480692.20
select 479536.98 + 1155.22 -960 = 479732.20

-- drop table if exists wtf;
-- create temp table wtf as
select store, sum("Apr 17")
from (
select store, emp_number, employee_name, hire_date, term_Date, "B/S", 
  sum(total_gross_pay) filter (where check_date = '04/17/2020') as "Apr 17"
from (  
  select a.store, a.emp_number, a.employee_name, a.hire_Date, a.term_Date,
    c.selected_pay_per_ as "B/S", b.total_gross_pay, 
    (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
  from employees a
  left join arkona.ext_pyhshdta b on a.emp_number = b.employee_
    and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date >= '04/10/2020'
    and b.seq_void in ('00','01', '02', '0J')  
  left join arkona.ext_pyptbdta c on b.company_number = c.company_number and b.payroll_run_number = c.payroll_run_number) x
--   where b.payroll_run_number in (417200) ) x
group by store, emp_number, employee_name, hire_date, term_Date, "B/S" ) xx group by store 
-----------------------------------------------------------------------------------
--/> this is the fix for 4/17
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
--< this is the fix for 6/17
-----------------------------------------------------------------------------------
the fix is in all checks, it was not exposing the check from 6/17
select store, sum("Jun 17")
from (
select store, emp_number, employee_name, hire_date, term_Date, "B/S", 
--   sum(total_gross_pay) filter (where check_date = '04/15/2020') as "Apr 15",
  sum(total_gross_pay) filter (where check_date = '06/17/2020') as "Jun 17"
--   sum(total_gross_pay) filter (where check_date = '04/30/2020') as "Apr 30",
--   sum(total_gross_pay) filter (where check_date = '05/01/2020') as "May 01",
--   sum(total_gross_pay) filter (where check_date = '05/14/2020') as "May 14",
--   sum(total_gross_pay) filter (where check_date = '05/15/2020') as "May 15",
--   sum(total_gross_pay) filter (where check_date = '05/29/2020') as "May 29",
--   sum(total_gross_pay) filter (where check_date = '05/31/2020') as "May 31",
--   sum(total_gross_pay) filter (where check_date = '06/12/2020') as "Jun 12",
--   sum(total_gross_pay) filter (where check_date = '06/15/2020') as "Jun 15",
--   sum(total_gross_pay) filter (where check_date = '06/26/2020') as "Jun 26",
--   sum(total_gross_pay) filter (where check_date = '06/30/2020') as "Jun 30",
--   sum(total_gross_pay) filter (where check_date = '07/02/2020') as "Jul 02",
--   sum(total_gross_pay) filter (where check_date = '07/07/2020') as "Jul 07",
--   sum(total_gross_pay) filter (where check_date = '07/10/2020') as "Jul 10",
--   sum(total_gross_pay) filter (where check_date = '07/15/2020') as "Jul 15",
--   sum(total_gross_pay) filter (where check_date = '07/24/2020') as "Jul 24"
from (  
  select a.store, a.emp_number, a.employee_name, a.hire_Date, a.term_Date,
    c.selected_pay_per_ as "B/S", b.total_gross_pay, 
    (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
  from employees a
  left join arkona.ext_pyhshdta b on a.emp_number = b.employee_
    and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date >= '04/10/2020'
    and b.seq_void in ('01','00')  
  left join arkona.ext_pyptbdta c on b.company_number = c.company_number and b.payroll_run_number = c.payroll_run_number
  where b.payroll_run_number in (617201) ) x
group by store, emp_number, employee_name, hire_date, term_Date, "B/S" ) xx group by store 
-----------------------------------------------------------------------------------
--/> this is the fix for 6/17
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--> this is the fix for 6/12
-----------------------------------------------------------------------------------
        register     jon
select 532304.88 - 528138.38   4166.50
jon missing greg sorum 4166.50, he has 2 checks on the same batch

select * from arkona.ext_pyhshdta where reference_check_ = 93785
select * from arkona.ext_pyhshdta where seq_void = '02'
select seq_void, count(*) from arkona.ext_pyhshdta group by seq_void
select * from arkona.ext_pyhshdta where seq_void = '0J'

the fix was to add void_seq 02
                 
select store, emp_number, employee_name, hire_date, term_Date, "B/S", 
--   sum(total_gross_pay) filter (where check_date = '04/15/2020') as "Apr 15",
--   sum(total_gross_pay) filter (where check_date = '04/17/2020') as "Apr 17",
--   sum(total_gross_pay) filter (where check_date = '04/30/2020') as "Apr 30",
--   sum(total_gross_pay) filter (where check_date = '05/01/2020') as "May 01",
--   sum(total_gross_pay) filter (where check_date = '05/14/2020') as "May 14",
--   sum(total_gross_pay) filter (where check_date = '05/15/2020') as "May 15",
--   sum(total_gross_pay) filter (where check_date = '05/29/2020') as "May 29",
--   sum(total_gross_pay) filter (where check_date = '05/31/2020') as "May 31",
  sum(total_gross_pay) filter (where check_date = '06/12/2020') as "Jun 12"
--   sum(total_gross_pay) filter (where check_date = '06/15/2020') as "Jun 15",
--   sum(total_gross_pay) filter (where check_date = '06/26/2020') as "Jun 26",
--   sum(total_gross_pay) filter (where check_date = '06/30/2020') as "Jun 30",
--   sum(total_gross_pay) filter (where check_date = '07/02/2020') as "Jul 02",
--   sum(total_gross_pay) filter (where check_date = '07/07/2020') as "Jul 07",
--   sum(total_gross_pay) filter (where check_date = '07/10/2020') as "Jul 10",
--   sum(total_gross_pay) filter (where check_date = '07/15/2020') as "Jul 15",
--   sum(total_gross_pay) filter (where check_date = '07/24/2020') as "Jul 24"
from (  
  select a.store, a.emp_number, a.employee_name, a.hire_Date, a.term_Date,
    c.selected_pay_per_ as "B/S", b.total_gross_pay, 
    (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
  from employees a
  left join arkona.ext_pyhshdta b on a.emp_number = b.employee_
    and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date >= '04/10/2020'
    and b.seq_void in ('01','00', '02')  
  left join arkona.ext_pyptbdta c on b.company_number = c.company_number and b.payroll_run_number = c.payroll_run_number
  where b.payroll_run_number in (612200) and a.emp_number = '1130426') x
group by store, emp_number, employee_name, hire_date, term_Date, "B/S" 
order by employee_name
-----------------------------------------------------------------------------------
--/> this is the fix for 6/12
-----------------------------------------------------------------------------------