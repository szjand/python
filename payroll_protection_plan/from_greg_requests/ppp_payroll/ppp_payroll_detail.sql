﻿


----------------------------------------------------
--< 05/21
----------------------------------------------------
/*
jeri:
  Will you please add a column for houly/commission/salary, as well as the pay rate during each week?  
  There can be a column for week.  I have attached an updated spreadsheet. 
jon:
  No problem
  Pay rate for hourly folks only?
jeri:
  Yes, thank you.
jeri 6/3:
  Will you please send this report again now that the sales payroll is closed?  Thank you.
  1. add new check_date to wtf
  2. add the new column(s) to the spreadsheet for jeri

jeri:
I am missing some employees on RY1—for example, David Appling and Nicholas Anderson.
jon: 
This report is based on payroll since April
It is not a census.
Neither of those employees have received a check in that time period.
I am not saying that I can not include them, but as the report is currently configured, that is why they are not on it.
This one might be easier to talk about rather than type about
jeri: 
Ok, I would like to include them if possible since they are still on our active employee list and 
I have to answer to the 75%--it also makes my list line up much nicer and makes it quicker for me.  
I would estimate there are probably about 15 employees who haven’t received checks.  
We can either talk at 9:30 or can call my cell 701-256-4319.  
Internal calls don’t transfer to my cell for some reason. 

so i called her, the report should include any employee employed at any time in the time period

*/

-- select distinct biweekly_pay_period_Start_date, biweekly_pay_period_end_Date
-- from dds.dim_date
-- where the_Date between '04/01/2020' and current_Date
-- 
-- select * from arkona.ext_pyptbdta where check_Date = 20200417
-- so, the first check is on 4/15, that is for the sales draw for april
-- next is 4/17 that is for the pay period starting on 3/29,
-- so any employee employed at anytime since 03/29
-- which translates to any employee termed on or after 03/29
-- 
-- select termination_Date, arkona.db2_integer_to_date(termination_date) from arkona.xfm_pymast where pymast_employee_number = '168573'
-- select * from arkona.xfm_pymast where pymast_employee_number = '168573'
-- 
-- ok, getting h/c/s & b/x from dimEmployee,
-- so, just need pymast for the master list of employees along with hire_date, term_Date, store, emp# and name
-- can no longer depend on pyhshdta for those attributes, employees are to be listed regardless of any paycheck
-- and, of course, the wtf query needs to start with pymast as the base table, left join to all the others


-- -- if run on payroll day, need fresh cut of pyptbdta, pyhshdta & pyhscdta
-- -- 08/11 in the process of troubleshooting jeri's discrepancies, discovered that this query for employees
-- -- left out 2 employees termed before 03/28 but both were paid on checks with check date 04/17
-- drop table if exists employees;
-- create temp table employees as
create temp table wtf1 as
select pymast_company_number as store, pymast_employee_number as emp_number, employee_name, 
  arkona.db2_integer_to_date(hire_date) as hire_date, 
    case 
      when arkona.db2_integer_to_date(termination_date) > current_date then null 
      else arkona.db2_integer_to_date(termination_date) 
    end as term_date
from arkona.xfm_pymast
where current_row 
  and arkona.db2_integer_to_date(termination_date) > '03/28/2020'
  and pymast_company_number in ('RY1','RY2')
order by pymast_company_number, employee_name;  

select * from arkona.xfm_pymast where employee_last_name = 'monreal' and current_row

-- so it looks like i need to establish the employee list based on the checks issued
drop table if exists employees;
create temp table employees as
-- create temp table wtf2 as
select pymast_company_number as store, pymast_employee_number as emp_number, employee_name, 
  arkona.db2_integer_to_date(hire_date) as hire_date, 
    case 
      when arkona.db2_integer_to_date(termination_date) > current_date then null 
      else arkona.db2_integer_to_date(termination_date) 
    end as term_date
from arkona.xfm_pymast a
join (
  select distinct employee_ 
  from arkona.ext_pyhshdta b
  where (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date >= '03/28/2020'
    and b.company_number in ('RY1','RY2')) b on a.pymast_employee_number = b.employee_
--     and b.total_gross_pay <> 0) b on a.pymast_employee_number = b.employee_
where a.current_row 
order by a.pymast_company_number, a.employee_name;  

08/13/20
from jeri:
  Is there a way to get this to include everyone that was  on the payroll even if they did not receive checks?  They were on there prior to this update. 
  Thank you.
so what i did was combine pymast based on term date with 
pyhshdta based on check date (removed the pay <> 0 exclusion)
sent that larger list to jeri
have not heard back from her yet, that will determine the final solution
  

select * from wtf1 a
full outer join wtf2 b on a.store = b.store and  a.employee_name = b.employee_name

create temp table employees as
select * from wtf1
union
select * from wtf2
order by store, employee_name



-- -- this is the old query replaced on 8/11 as a result of ts jeri's anomalies
-- select  (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date, max(payroll_run_number) as batch, count(*)
-- from arkona.ext_pyhshdta a
-- where (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date >= '04/15/2020'
--   and a.company_number in ('RY1','RY2')
--   and a.total_gross_pay <> 0
-- group by (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date
-- order by (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date desc

-- this is all the checks
-- added payroll_run_number in the grouping, thereby including all checks
select  (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date, payroll_run_number as batch, count(*)
from arkona.ext_pyhshdta a
where (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date >= '04/10/2020'
  and a.company_number in ('RY1','RY2')
  and a.total_gross_pay <> 0
group by payroll_run_number, (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date
order by payroll_run_number, (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date desc


07/2
interesting, the check from 7/7 is gone now, go figure

07/13
again, today, a single check, this time on 07/07, who is it for?
select *
from arkona.ext_pyhshdta b 
where (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date = '07/07/2020'
  and b.seq_void in ('01','00')  
-- sky bob, commission
select * from arkona.ext_pymast where pymast_employee_number = '123538'  

-- new wtf using employees temp table as base
-- 6/15 this is no generating 2 rows for each employee?
-- one row with a b/s value, one with b/s null
-- needed a fresh cut of pyptbdta
-- ok, back to normal processing
-- add columns for the new check dates (6/12, 6/15)
-- 08/12 as a result of jeri's exceptions
--    for 6/12 the fix was to add seq_void 02
--    for 4/17 added seq_void 0J and based employees on checks rather than pymast & term date
--    for 6/17 fixed all checks query which now exposes check on 06/17
-- fuck now it is returning 2 rows for each employee (868 rows) ::  only on a recent check, missing pyptbdta.selected_pay_per_need, fresh cut of pyptbdta, that fixed it

-- 08/12 removed 07/07 check, it no longer shows in my all checks query nor in DT Payroll Register
drop table if exists wtf;
create temp table wtf as
select store, emp_number, employee_name, hire_date, term_Date, "B/S", 
  sum(total_gross_pay) filter (where check_date = '04/15/2020') as "Apr 15",
  sum(total_gross_pay) filter (where check_date = '04/17/2020') as "Apr 17",
  sum(total_gross_pay) filter (where check_date = '04/30/2020') as "Apr 30",
  sum(total_gross_pay) filter (where check_date = '05/01/2020') as "May 01",
  sum(total_gross_pay) filter (where check_date = '05/14/2020') as "May 14",
  sum(total_gross_pay) filter (where check_date = '05/15/2020') as "May 15",
  sum(total_gross_pay) filter (where check_date = '05/29/2020') as "May 29",
  sum(total_gross_pay) filter (where check_date = '05/31/2020') as "May 31",
  sum(total_gross_pay) filter (where check_date = '06/12/2020') as "Jun 12",
  sum(total_gross_pay) filter (where check_date = '06/15/2020') as "Jun 15",
  sum(total_gross_pay) filter (where check_date = '06/17/2020') as "Jun 17",  
  sum(total_gross_pay) filter (where check_date = '06/26/2020') as "Jun 26",
  sum(total_gross_pay) filter (where check_date = '06/30/2020') as "Jun 30",
  sum(total_gross_pay) filter (where check_date = '07/02/2020') as "Jul 02",
--   sum(total_gross_pay) filter (where check_date = '07/07/2020') as "Jul 07",
  sum(total_gross_pay) filter (where check_date = '07/10/2020') as "Jul 10",
  sum(total_gross_pay) filter (where check_date = '07/15/2020') as "Jul 15",
  sum(total_gross_pay) filter (where check_date = '07/24/2020') as "Jul 24",
  sum(total_gross_pay) filter (where check_date = '07/31/2020') as "Jul 31",
  sum(total_gross_pay) filter (where check_date = '08/05/2020') as "Aug 05",
  sum(total_gross_pay) filter (where check_date = '08/07/2020') as "Aug 07",
  sum(total_gross_pay) filter (where check_date = '08/14/2020') as "Aug 14"
from (  
  select a.store, a.emp_number, a.employee_name, a.hire_Date, a.term_Date,
    c.selected_pay_per_ as "B/S", b.total_gross_pay, 
    (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
  from employees a
  left join arkona.ext_pyhshdta b on a.emp_number = b.employee_
    and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date >= '04/10/2020'
    and b.seq_void in ('01','00', '02', '0J')  
  left join arkona.ext_pyptbdta c on b.company_number = c.company_number and b.payroll_run_number = c.payroll_run_number) x
group by store, emp_number, employee_name, hire_date, term_Date, "B/S"  
order by store, employee_name; 

select * from wtf

-- spreadsheet for jeri
-- named, eg, payroll_breakdown_06-03-20.xlsx
-- add a sub select and date attribute for the new biweekly chec date, add the date attribute for the new semi monthly check date
select a.store, a.emp_number, a.employee_name, a.hire_date, a.term_date, 
  b.payrollclasscode as "H/C/S",
  (select payperiodcode
    from ads.ext_edw_employee_dim
    where employeenumber = b.employeenumber
      and currentrow) as "B/S",
  a."Apr 15",
  (select hourlyrate 
    from ads.ext_edw_employee_dim 
    where employeenumber = b.employeenumber 
      and payrollclasscode = 'H' 
        and '04/17/2020' between  employeekeyfromdate and employeekeythrudate) as hr_0417, "Apr 17",
  "Apr 30",
  (select hourlyrate 
    from ads.ext_edw_employee_dim 
    where employeenumber = b.employeenumber 
      and payrollclasscode = 'H' 
        and '05/01/2020' between  employeekeyfromdate and employeekeythrudate) as hr_0501, "May 01",
  "May 14",
  (select hourlyrate 
    from ads.ext_edw_employee_dim 
    where employeenumber = b.employeenumber 
      and payrollclasscode = 'H' 
        and '05/15/2020' between  employeekeyfromdate and employeekeythrudate) as hr_0515, "May 15",    
  (select hourlyrate 
    from ads.ext_edw_employee_dim 
    where employeenumber = b.employeenumber 
      and payrollclasscode = 'H' 
        and '05/29/2020' between  employeekeyfromdate and employeekeythrudate) as hr_0515, "May 29", 
  "May 31",
  (select hourlyrate 
    from ads.ext_edw_employee_dim 
    where employeenumber = b.employeenumber 
      and payrollclasscode = 'H' 
        and '06/12/2020' between  employeekeyfromdate and employeekeythrudate) as hr_0612, "Jun 12", 
  "Jun 15", "Jun 17",
  (select hourlyrate 
    from ads.ext_edw_employee_dim 
    where employeenumber = b.employeenumber 
      and payrollclasscode = 'H' 
        and '06/26/2020' between  employeekeyfromdate and employeekeythrudate) as hr_0617, "Jun 26",
  "Jun 30", "Jul 02", -- "Jul 07",
  (select hourlyrate 
    from ads.ext_edw_employee_dim 
    where employeenumber = b.employeenumber 
      and payrollclasscode = 'H' 
        and '07/10/2020' between  employeekeyfromdate and employeekeythrudate) as hr_0710, "Jul 10",
  "Jul 15",
  (select hourlyrate 
    from ads.ext_edw_employee_dim 
    where employeenumber = b.employeenumber 
      and payrollclasscode = 'H' 
        and '07/24/2020' between  employeekeyfromdate and employeekeythrudate) as hr_0724, "Jul 24",
  "Jul 31", "Aug 05",
  (select hourlyrate 
  from ads.ext_edw_employee_dim 
  where employeenumber = b.employeenumber 
    and payrollclasscode = 'H' 
      and '08/07/2020' between  employeekeyfromdate and employeekeythrudate) as hr_0807, "Aug 07",
  "Aug 14"            
-- select count(*)                           
from wtf a
join ads.ext_edw_employee_dim b on a.emp_number = b.employeenumber
  and b.currentrow
order by a.store, a.employee_name
----------------------------------------------------
--/> 05/21
----------------------------------------------------


---------------------------------------------------------------------------------
--< 05/18
---------------------------------------------------------------------------------
/*
jeri
05/18/20

Correct all employees.  The total gross pay for employees for each check date should tie out to the total gross payroll for the store for that date.  Separate tabs for each store, please.


So, you want all employees, not just hourly?  That will include check dates of 4/30 and 5/14.


I think I proved that I really really suck at clear communication and specs….

------------------------------------------------------------

Updated to include Biweekly or Semimonthly designation

------------------------------------------------------------
(after i asked her for a sample spreadsheet showing just a couple lines of what she wants to see)

Here’s the first piece.  We may not even need my second request—waiting to visit with Greg.

This is really just a presentation difference with the same info from the report you sent.

		Gross Pay (by check date)							
                                  17-Apr	    1-May	      15-May	     29-May				
Employee Number	Employee Name								
12345	          Tom Smith	        $2,500.00 	 $3,000.00 	 $1,400.00 	 $3,000.00 				
56789         	Bob Johnson	      $1,400.00 	 $3,200.00 	 $1,600.00 	 $1,400.00 				

----------------------------------------------------------------------
The info is there, but can I get the employee names on the horizontal lines and the pay periods listed in the columns?

Also, is it possible for the biweekly payroll period paid out on 5/1—
I need to get the hours worked on 4/12 and 4/13 at GM store by employee and 
4/12-4/14 at Honda store by employee.  Pay Rate as well please.  This is for hourly only people.
---------------------------------------------------------
*/
1954
select * from arkona.xfm_pymast limit 10

select * from arkona.ext_pyhshdta limit 10

select payroll_class, pay_period, count(*) from arkona.xfm_pymast where current_row and active_code <> 'T' and pymast_company_number <> 'RY5' group by payroll_class, pay_period

select d.payroll_class, b.selected_pay_per_,
  (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date as check_date, count(*)
from arkona.ext_pyhshdta a
join arkona.ext_pyptbdta b on a.company_number = b.company_number and a.payroll_run_number  = b.payroll_run_number
left join arkona.xfm_pymast d on a.employee_ = d.pymast_employee_number
  and d.current_row  
where  (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date >= '04/17/2020'
  and a.seq_void in ('01','00')  
--   and d.payroll_class = 'H'
  and a.company_number in ('RY1','RY2')
group by d.payroll_class, b.selected_pay_per_,
  (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date  
  

  
4/15, 4/17, 4/30, 5/1,5/14,/5/15

drop table if exists wtf;
create temp table wtf as

select store, emp_number, employee_name, hire_date, term_Date, "B/S", 
  sum(total_gross_pay) filter (where check_date = '04/15/2020') as "Apr 15",
  sum(total_gross_pay) filter (where check_date = '04/17/2020') as "Apr 17",
  sum(total_gross_pay) filter (where check_date = '04/30/2020') as "Apr 30",
  sum(total_gross_pay) filter (where check_date = '05/01/2020') as "May 01",
  sum(total_gross_pay) filter (where check_date = '05/14/2020') as "May 14",
  sum(total_gross_pay) filter (where check_date = '05/15/2020') as "May 15"
from (   
  select a.company_number as store, a.employee_ as emp_number, a.employee_name,
    arkona.db2_integer_to_date(d.hire_date) as hire_date, 
    case when arkona.db2_integer_to_date(d.termination_date) > current_date then null else arkona.db2_integer_to_date(d.termination_date) end as term_date,
    b.selected_pay_per_ as "B/S", a.total_gross_pay, 
    (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date as check_date
  from arkona.ext_pyhshdta a
  join arkona.ext_pyptbdta b on a.company_number = b.company_number and a.payroll_run_number  = b.payroll_run_number
  left join arkona.xfm_pymast d on a.employee_ = d.pymast_employee_number
    and d.current_row  
  where  (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date >= '04/10/2020'
    and a.seq_void in ('01','00')  
    and a.company_number in ('RY1','RY2')) x
group by store, emp_number, employee_name, hire_date, term_Date, "B/S"  
order by store, employee_name  



---------------------------------------------------------------------------------
--/> 05/18
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
--< 05/09
---------------------------------------------------------------------------------
/*
jeri
05/08/20
1.	For the PPP loan, I need to deduct the gross wages on the last payroll that were attributed to 4/12-4/13 for the GM store and 4/12-4/14 for HGF.  My first thought is as follows:

Hourly only:  Hours worked in time clock * pay rate
Hourly+Commission and Salary:  average pay per day based on last payroll’s total gross pay * number of day’s needed.  (for these individuals, Sunday would not count.  We would use a total of 10 days for average.  For GM, would be average day * 1 and Honda average day *2).

This is only for biweekly—looking for ideas on monthly payroll as they received a draw on 4/15.  

2.	Dealertrack has no good downloadable Excel report for payroll periods—monthly is the best I can get.  This was fine last week as it was the first in the month.  However, next week’s will be a bit trickier.  Is it possible for you to do a report as follows:

Pay Period A         Pay Period B   Pay Period C                    Total Gross Pay
Employee #                     Employee Name                 Gross Pay

Thoughts?

jon
05/09/20
Not exactly clear on what is needed.

The attached spreadsheet is all payroll detail for 2020, I’m thinking from this kind of detail we can probably derive whatever it is that is needed.

The following distribution codes are paid monthly (even though the pay period is S) : 25,AFTE,BSM,MGR,OWN,PTEC,RAO,SALE,SALM,TEAM,USED

The last 2 columns are just the calendar dates for the actual biweekly pay periods, they are matched to the payroll data based on the payroll start date.
Thus they are mostly blank on folks paid monthly as their “pay periods” start on the first and the fifteenth.

Again, this is not meant to be the answer, just a start, hopefully to generating an answer
*/


select company_number, payroll_run_number, employee_, employee_name, distrib_code, seq_void,
  payroll_ending_year, payroll_ending_month, payroll_ending_day, 
  check_year, check_month, check_day, total_gross_pay
from arkona.ext_pyhshdta
where payroll_cen_year = 120
and payroll_run_number = 515200
 limit 10

select company_number, payroll_run_number, payroll_Start_date, payroll_end_date, check_date, description
from arkona.ext_pyptbdta
where payroll_cen_year = 120

select seq_void, count(*)
from arkona.ext_pyhshdta
group by seq_void

select *
from arkona.ext_pyhshdta
where seq_void in ('0K','02','01')
  and payroll_cen_year = 120

drop table if exists wtf;
create temp table wtf as
select a.company_number as store, a.payroll_run_number as payroll_run, a.total_gross_pay, a.employee_ as "emp #", 
  a.employee_name as name, a.distrib_code, b.selected_pay_per_ as pay_period, b.description as check_description,
  (a.check_month::text ||'-' || a.check_day::text ||'-' || a.check_year::text)::Date as check_date,
  arkona.db2_integer_to_date(b.payroll_start_date) as payroll_start_date,
  (a.payroll_ending_month::text ||'-' || a.payroll_ending_day::text ||'-' || a.payroll_ending_year::text)::Date as payroll_end_date,
  c.*
from arkona.ext_pyhshdta a
join arkona.ext_pyptbdta b on a.company_number = b.company_number and a.payroll_run_number  = b.payroll_run_number
left join (
  select biweekly_pay_period_start_date as biweekly_from_date, biweekly_pay_period_end_date as biweekly_thru_date
  from dds.dim_date
  where the_date between '03/01/2020' and '12/31/2020'
  group by biweekly_pay_period_start_date, biweekly_pay_period_end_date) c on  arkona.db2_integer_to_date(b.payroll_start_date) = c.biweekly_from_date 
where a.payroll_cen_year = 120  
  and a.check_month > 2 -- added 5/18 to limit the ouptut 
  and a.seq_void in ('01','00')  
--   and c.from_date is null
--   and a.total_Gross_pay <> 0
order by employee_name, arkona.db2_integer_to_date(b.payroll_start_date)


select * from wtf

select distinct check_date from wtf order by check_date

-- i believe this is all the paid monthly

select distinct pay_period, distrib_code
from wtf
where biweekly_from_Date is null

select string_agg(distinct distrib_code,',')
from wtf
where biweekly_from_Date is null


select a.*
from wtf a
join (
select distinct "emp #", pay_period, distrib_code
from wtf
where biweekly_from_Date is null) b on a."emp #" = b."emp #"


--  ahh, what about the hourly folks

select a.* --452
from wtf a
where a.payroll_start_date > '03/31/2020'
  and a.total_gross_pay <> 0
order by a.store, a.payroll_run, a.distrib_code, a.name


select *
from ads.ext_edw_employee_dim
where employeenumber = '1120120'

select a.*, b.payrollclass, b.employeekey
from wtf a
left join ads.ext_edw_employee_dim b on a."emp #" = b.employeenumber
  and a.payroll_end_date between b.employeekeyfromdate and b.employeekeythrudate
where a.payroll_start_date > '03/31/2020'
  and a.total_gross_pay <> 0
order by a.store, a.payroll_run, a.distrib_code, name


