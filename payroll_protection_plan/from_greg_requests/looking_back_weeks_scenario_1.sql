﻿-- looking_back_scenario_1 spreadsheet
select a.store,a.employee_number, a.name, a.distrib_code, a.hire_date,  a.term_date, 
  a.payroll_class, a.full_part, 
    "2/10-2/16", b.employed, b.fte,
    "2/17-2/23", c.employed, c.fte,
    "2/24-3/2", d.employed, d.fte,
    "3/3-3/9", e.employed, e.fte,
    "3/10-3/16", f.employed, f.fte,
    "3/17-3/23", g.employed, g.fte,
    "3/24-3/30", h.employed, h.fte,  -- this was g
    "3/31-4/6", i.employed, i.fte,
    "4/7-4/13", j.employed, j.fte,
    "4/14-4/20", k.employed, k.fte,
    "4/21-4/27", l.employed, l.fte,
    "4/28-5/4", m.employed, m.fte,
    "5/5-5/11", n.employed, n.fte,
    "5/12-5/18", o.employed, o.fte,
    "5/19-5/25", p.employed, p.fte,
    "5/26-6/1", q.employed, q.fte,
    "6/2-6/8",  r.employed, r.fte,
    "6/9-6/15", s.employed, s.fte,
    "6/16-6/22", t.employed, t.fte,
    "6/23-6/29", u.employed, u.fte,
    "6/30-7/6", v.employed, v.fte
from jon.looking_back a
left join jon.lb_scenario_1_week_1 b on a.employee_number = b.employee_number
left join jon.lb_scenario_1_week_2 c on a.employee_number = c.employee_number
left join jon.lb_scenario_1_week_3 d on a.employee_number = d.employee_number
left join jon.lb_scenario_1_week_4 e on a.employee_number = e.employee_number
left join jon.lb_scenario_1_week_5 f on a.employee_number = f.employee_number
left join jon.lb_scenario_1_week_6 g on a.employee_number = g.employee_number
left join jon.lb_scenario_1_week_7 h on a.employee_number = h.employee_number
left join jon.lb_scenario_1_week_8 i on a.employee_number = i.employee_number
left join jon.lb_scenario_1_week_9 j on a.employee_number = j.employee_number
left join jon.lb_scenario_1_week_10 k on a.employee_number = k.employee_number
left join jon.lb_scenario_1_week_11 l on a.employee_number = l.employee_number
left join jon.lb_scenario_1_week_12 m on a.employee_number = m.employee_number
left join jon.lb_scenario_1_week_13 n on a.employee_number = n.employee_number
left join jon.lb_scenario_1_week_14 o on a.employee_number = o.employee_number
left join jon.lb_scenario_1_week_15 p on a.employee_number = p.employee_number
left join jon.lb_scenario_1_week_16 q on a.employee_number = q.employee_number
left join jon.lb_scenario_1_week_17 r on a.employee_number = r.employee_number
left join jon.lb_scenario_1_week_18 s on a.employee_number = s.employee_number
left join jon.lb_scenario_1_week_19 t on a.employee_number = t.employee_number
left join jon.lb_scenario_1_week_20 u on a.employee_number = u.employee_number
left join jon.lb_scenario_1_week_21 v on a.employee_number = v.employee_number
where a.store = 'ry1'
union
select null,null,null,null, null::date,  null::date, '', '', 
    null::numeric, null::boolean,sum(b.fte),
    null::numeric, null::boolean,sum(c.fte),
    null::numeric, null::boolean,sum(d.fte),
    null::numeric, null::boolean,sum(e.fte),
    null::numeric, null::boolean,sum(f.fte),
    null::numeric, null::boolean,sum(g.fte),
    null::numeric, null::boolean,sum(h.fte),
    null::numeric, null::boolean,sum(i.fte),
    null::numeric, null::boolean,sum(j.fte),
    null::numeric, null::boolean,sum(k.fte),
    null::numeric, null::boolean,sum(l.fte),
    null::numeric, null::boolean,sum(m.fte),
    null::numeric, null::boolean,sum(n.fte),
    null::numeric, null::boolean,sum(o.fte),
    null::numeric, null::boolean,sum(p.fte),
    null::numeric, null::boolean,sum(q.fte),
    null::numeric, null::boolean,sum(r.fte),
    null::numeric, null::boolean,sum(s.fte),
    null::numeric, null::boolean,sum(t.fte),
    null::numeric, null::boolean,sum(u.fte),
    null::numeric, null::boolean,sum(v.fte)
from jon.looking_back a
left join jon.lb_scenario_1_week_1 b on a.employee_number = b.employee_number
left join jon.lb_scenario_1_week_2 c on a.employee_number = c.employee_number
left join jon.lb_scenario_1_week_3 d on a.employee_number = d.employee_number
left join jon.lb_scenario_1_week_4 e on a.employee_number = e.employee_number
left join jon.lb_scenario_1_week_5 f on a.employee_number = f.employee_number
left join jon.lb_scenario_1_week_6 g on a.employee_number = g.employee_number
left join jon.lb_scenario_1_week_7 h on a.employee_number = h.employee_number
left join jon.lb_scenario_1_week_8 i on a.employee_number = i.employee_number
left join jon.lb_scenario_1_week_9 j on a.employee_number = j.employee_number
left join jon.lb_scenario_1_week_10 k on a.employee_number = k.employee_number
left join jon.lb_scenario_1_week_11 l on a.employee_number = l.employee_number
left join jon.lb_scenario_1_week_12 m on a.employee_number = m.employee_number
left join jon.lb_scenario_1_week_13 n on a.employee_number = n.employee_number
left join jon.lb_scenario_1_week_14 o on a.employee_number = o.employee_number
left join jon.lb_scenario_1_week_15 p on a.employee_number = p.employee_number
left join jon.lb_scenario_1_week_16 q on a.employee_number = q.employee_number
left join jon.lb_scenario_1_week_17 r on a.employee_number = r.employee_number
left join jon.lb_scenario_1_week_18 s on a.employee_number = s.employee_number
left join jon.lb_scenario_1_week_19 t on a.employee_number = t.employee_number
left join jon.lb_scenario_1_week_20 u on a.employee_number = u.employee_number
left join jon.lb_scenario_1_week_21 v on a.employee_number = v.employee_number
where a.store = 'ry1'
order by store, name



-- drop table if exists jon.lb_scenario_1_week_1;
-- create table jon.lb_scenario_1_week_1 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_1 
-- select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/10/2019','02/16/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('02/10/2019','02/16/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "2/10-2/16" = 0 then 0
--         when "2/10-2/16" >= 30 then 1
--         when "2/10-2/16" < 30 then round("2/10-2/16"/30, 1)
--       end    
--   end as fte, daterange('02/10/2019','02/16/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_2;
-- create table jon.lb_scenario_1_week_2 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_2 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/17/2019','02/23/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('02/17/2019','02/23/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "2/17-2/23" = 0 then 0
--         when "2/17-2/23" >= 30 then 1
--         when "2/17-2/23" < 30 then round("2/17-2/23"/30, 1)
--       end    
--   end as fte, daterange('02/17/2019','02/23/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_3;
-- create table jon.lb_scenario_1_week_3 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_3 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/24/2019','03/02/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('02/24/2019','03/02/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "2/24-3/2" = 0 then 0
--         when "2/24-3/2" >= 30 then 1
--         when "2/24-3/2" < 30 then round("2/24-3/2"/30, 1)
--       end    
--   end as fte, daterange('02/24/2019','03/02/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_4;
-- create table jon.lb_scenario_1_week_4 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_4 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/03/2019','03/09/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('03/03/2019','03/09/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "3/3-3/9" = 0 then 0
--         when "3/3-3/9" >= 30 then 1
--         when "3/3-3/9" < 30 then round("3/3-3/9"/30, 1)
--       end    
--   end as fte, daterange('03/03/2019','03/09/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_5;
-- create table jon.lb_scenario_1_week_5 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_5 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/10/2019','03/16/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('03/10/2019','03/16/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "3/10-3/16" = 0 then 0
--         when "3/10-3/16" >= 30 then 1
--         when "3/10-3/16" < 30 then round("3/10-3/16"/30, 1)
--       end    
--   end as fte, daterange('03/10/2019','03/16/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_6;
-- create table jon.lb_scenario_1_week_6 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_6 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/17/2019','03/23/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('03/17/2019','03/23/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "3/17-3/23" = 0 then 0
--         when "3/17-3/23" >= 30 then 1
--         when "3/17-3/23" < 30 then round("3/17-3/23"/30, 1)
--       end    
--   end as fte, daterange('03/17/2019','03/23/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_7;
-- create table jon.lb_scenario_1_week_7 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_7 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/24/2019','03/30/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('03/24/2019','03/30/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "3/24-3/30" = 0 then 0
--         when "3/24-3/30" >= 30 then 1
--         when "3/24-3/30" < 30 then round("3/24-3/30"/30, 1)
--       end    
--   end as fte, daterange('03/24/2019','03/30/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_8;
-- create table jon.lb_scenario_1_week_8 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_8 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/31/2019','04/06/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('03/31/2019','04/06/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "3/31-4/6" = 0 then 0
--         when "3/31-4/6" >= 30 then 1
--         when "3/31-4/6" < 30 then round("3/31-4/6"/30, 1)
--       end    
--   end as fte, daterange('03/31/2019','04/06/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_9;
-- create table jon.lb_scenario_1_week_9 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_9 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('04/07/2019','04/13/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('04/07/2019','04/13/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "4/7-4/13" = 0 then 0
--         when "4/7-4/13" >= 30 then 1
--         when "4/7-4/13" < 30 then round("4/7-4/13"/30, 1)
--       end    
--   end as fte, daterange('04/07/2019','04/13/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_10;
-- create table jon.lb_scenario_1_week_10 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_10 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('04/14/2019','04/20/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('04/14/2019','04/20/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "4/14-4/20" = 0 then 0
--         when "4/14-4/20" >= 30 then 1
--         when "4/14-4/20" < 30 then round("4/14-4/20"/30, 1)
--       end    
--   end as fte, daterange('04/14/2019','04/20/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_11;
-- create table jon.lb_scenario_1_week_11 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_11 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('04/21/2019','04/27/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('04/21/2019','04/27/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "4/21-4/27" = 0 then 0
--         when "4/21-4/27" >= 30 then 1
--         when "4/21-4/27" < 30 then round("4/21-4/27"/30, 1)
--       end    
--   end as fte, daterange('04/21/2019','04/27/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_12;
-- create table jon.lb_scenario_1_week_12 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_12 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('04/28/2019','05/04/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('04/28/2019','05/04/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "4/28-5/4" = 0 then 0
--         when "4/28-5/4" >= 30 then 1
--         when "4/28-5/4" < 30 then round("4/28-5/4"/30, 1)
--       end    
--   end as fte, daterange('04/28/2019','05/04/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_13;
-- create table jon.lb_scenario_1_week_13 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_13 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('05/05/2019','05/11/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('05/05/2019','05/11/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "5/5-5/11" = 0 then 0
--         when "5/5-5/11" >= 30 then 1
--         when "5/5-5/11" < 30 then round("5/5-5/11"/30, 1)
--       end    
--   end as fte, daterange('05/05/2019','05/11/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_14;
-- create table jon.lb_scenario_1_week_14 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_14 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('05/12/2019','05/18/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('05/12/2019','05/18/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "5/12-5/18" = 0 then 0
--         when "5/12-5/18" >= 30 then 1
--         when "5/12-5/18" < 30 then round("5/12-5/18"/30, 1)
--       end    
--   end as fte, daterange('05/12/2019','05/18/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_15;
-- create table jon.lb_scenario_1_week_15 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_15 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('05/19/2019','05/25/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('05/19/2019','05/25/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "5/19-5/25" = 0 then 0
--         when "5/19-5/25" >= 30 then 1
--         when "5/19-5/25" < 30 then round("5/19-5/25"/30, 1)
--       end    
--   end as fte, daterange('05/19/2019','05/25/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_16;
-- create table jon.lb_scenario_1_week_16 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_16 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('05/26/2019','06/01/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('05/26/2019','06/01/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "5/26-6/1" = 0 then 0
--         when "5/26-6/1" >= 30 then 1
--         when "5/26-6/1" < 30 then round("5/26-6/1"/30, 1)
--       end    
--   end as fte, daterange('05/26/2019','06/01/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_17;
-- create table jon.lb_scenario_1_week_17 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_17 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/02/2019','06/08/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('06/02/2019','06/08/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "6/2-6/8" = 0 then 0
--         when "6/2-6/8" >= 30 then 1
--         when "6/2-6/8" < 30 then round("6/2-6/8"/30, 1)
--       end    
--   end as fte, daterange('06/02/2019','06/08/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_18;
-- create table jon.lb_scenario_1_week_18 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_18 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/09/2019','06/15/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('06/09/2019','06/15/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "6/9-6/15" = 0 then 0
--         when "6/9-6/15" >= 30 then 1
--         when "6/9-6/15" < 30 then round("6/9-6/15"/30, 1)
--       end    
--   end as fte, daterange('06/09/2019','06/15/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_19;
-- create table jon.lb_scenario_1_week_19 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_19 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/16/2019','06/22/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('06/16/2019','06/22/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "6/16-6/22" = 0 then 0
--         when "6/16-6/22" >= 30 then 1
--         when "6/16-6/22" < 30 then round("6/16-6/22"/30, 1)
--       end    
--   end as fte, daterange('06/16/2019','06/22/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_20;
-- create table jon.lb_scenario_1_week_20 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_20 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/23/2019','06/29/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('06/23/2019','06/29/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "6/23-6/29" = 0 then 0
--         when "6/23-6/29" >= 30 then 1
--         when "6/23-6/29" < 30 then round("6/23-6/29"/30, 1)
--       end    
--   end as fte, daterange('06/23/2019','06/29/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- drop table if exists jon.lb_scenario_1_week_21;
-- create table jon.lb_scenario_1_week_21 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.lb_scenario_1_week_21 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/30/2019','07/06/2019', '[]') then true else false end as employed,
--   case --fte calculation
--     when not daterange(hire_date, term_date, '[]') && daterange('06/30/2019','07/06/2019', '[]') then 0  -- not employed
--     when full_part = 'full' then 1 -- all full time
--     when full_part = 'part' and ( -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 0.5      
--     else
--       case 
--         when "6/30-7/6" = 0 then 0
--         when "6/30-7/6" >= 30 then 1
--         when "6/30-7/6" < 30 then round("6/30-7/6"/30, 1)
--       end    
--   end as fte, daterange('06/30/2019','07/06/2019', '[]')
-- from jon.looking_back a
-- order by store, name;