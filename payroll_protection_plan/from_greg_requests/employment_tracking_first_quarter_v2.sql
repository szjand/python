﻿doing first quarter (employment_tracking.sql in the style of employment_Tracking_looking_back.sql)
04/23 redoing it with full first and last weeks for scenarios aa & 2

drop table if exists weeks;
create temp table weeks as
select daterange(min(the_date), max(the_date), '[]') as week_range, 
  min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week
from dds.dim_date 
where the_date between '12/29/2019' and '04/04/2020'
group by sunday_to_saturday_week
order by sunday_to_saturday_week;



-- employed at anytime
drop table if exists base_employees;
create temp table base_employees as
select pymast_company_number as store, pymast_employee_number employee_number, employee_name as name, distrib_code,
  arkona.db2_integer_to_date(hire_date) as hire_date,
  arkona.db2_integer_to_date(termination_date) as term_date,
  payroll_class, active_code, current_row, pymast_key, row_From_date, row_thru_date,
  daterange (row_from_date, row_thru_date, '[]') as from_thru,
  daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
from arkona.xfm_pymast a
where arkona.db2_integer_to_date(hire_date) < '04/04/2020'
  and arkona.db2_integer_to_date(termination_date) > '12/29/2019'
  and pymast_employee_number <> '1112425'  -- jim price
  and not exists (
    select 1
    from arkona.xfm_pymast
    where pymast_employee_number = a.pymast_employee_number
      and current_row
      and arkona.db2_integer_to_date(termination_date) < '01/01/2020')
order by store, name

jonathon pachl termed on 12/31/2019, so his non termed rows show up
but should not
adding just current row doesnt feel right
added:
  and not exists (
    select 1
    from arkona.xfm_pymast
    where pymast_employee_number = a.pymast_employee_number
      and arkona.db2_integer_to_date(termination_date) < '01/01/2020')

now james svenson is missing   145972   
select * from arkona.xfm_pymast where pymast_employee_number = '145972' order by pymast_key
its becuase he is a rehire hired 4/11/18 termed 8/23/19, hired 12/23/19 termed 2/25/20
select * from base_employees where employee_number = '145972'
added current row to the not exists, only want to exclude employees whose current row has a termdate < 1/1/20

select * from base_employees order by store, name
well, at least now, no pachl or steve flaat, but we have svenson

-- multiple payroll_class
drop table if exists multiple_payroll_class;
create temp table multiple_payroll_class as
select employee_number              
from (
select employee_number, payroll_class
from base_employees
where row_from_date between '12/29/2019' and '04/04/2020'
  or row_thru_date between '12/29/2019' and '04/04/2020'
group by employee_number, payroll_class) a
group by employee_number having count(*) > 1;

-- multiple full_part
drop table if exists multiple_full_part;
create temp table multiple_full_part as
select employee_number              
from (
select employee_number , active_code
from base_employees
where active_code in ('A','P')
and ( row_from_date between '12/29/2019' and '04/04/2020'
  or row_thru_date between  '12/29/2019' and '04/04/2020')
group by employee_number, active_code) a
group by employee_number having count(*) > 1;

drop table if exists employee_numbers;
create temp table employee_numbers as
  select distinct employee_number              
  from base_employees
  where from_thru && daterange('12/29/2019','04/04/2020', '[]')
    and hire_term && daterange('12/29/2019','04/04/2020', '[]')
--     and active_code <> 'T'  
  group by store, employee_number, name, term_date, payroll_class, active_code;

drop table if exists clock_hours;
create temp table clock_hours as
select a.store_code, a.employee_number, b.from_date, b.thru_date, 
  sum(a.clock_hours) as clock_hours, sum(a.vac_hours + a.pto_hours) as pto_hours , sum(a.hol_hours) as holiday_hours
from arkona.xfm_pypclockin a
join employee_numbers aa on a.employee_number = aa.employee_number
join weeks b on a.the_date between b.from_date and b.thru_date
group by a.store_code, a.employee_number, b.from_date, b.thru_date;   


drop table if exists final_employees;
create temp table final_employees as
-- single row employees
select aa.*
from (
  select store, employee_number, name, max(distrib_code) as distrib_code, hire_date, term_date, payroll_Class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as full_part
  from base_employees a
  join (
    select distinct pymast_key
    from (
      select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
        payroll_class, active_code,
        daterange (row_from_date, row_thru_date, '[]') as from_thru,
        daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
      from arkona.xfm_pymast a
      where a.pymast_company_number in ('RY1','RY2')
        and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
    where from_thru && daterange('12/29/2019','04/04/2020', '[]')
      and hire_term && daterange('12/29/2019','04/04/2020', '[]')) b on a.pymast_key = b.pymast_key
  group by store, employee_number, name, hire_date, term_date, payroll_Class, active_code) aa
left join multiple_full_part bb on aa.employee_number = bb.employee_number   
left join multiple_payroll_class cc on aa.employee_number = cc.employee_number
where bb.employee_number is null
  and cc.employee_number is null
  and not exists (
    select 1
    from base_employees
    where name = aa.name
      and active_code = 'T')
union      
-- multiple class/active
select aa.store, aa.employee_number, aa.name, distrib_code, hire_date, term_date, 
--   array_agg(distinct payroll_class), array_agg(distinct active_code),
--   cardinality(array_agg(distinct payroll_class)), cardinality(array_agg(distinct active_code)),
  case
    when cardinality(array_agg(distinct payroll_class)) > 1 then '*'
    else (array_agg(distinct payroll_class))[1]
  end as payroll_class,
  case
    when cardinality(array_agg(distinct active_code)) > 1 then '*'
    when cardinality(array_agg(distinct active_code)) = 1 and (array_agg(distinct active_code))[1] = 'A' then 'full'
    when cardinality(array_agg(distinct active_code)) = 1 and (array_agg(distinct active_code))[1] = 'P' then 'part'
  end as full_part
from (
  select store, employee_number, name, max(distrib_code) as distrib_code, hire_date, term_date, payroll_Class, active_code
  from base_employees a
  join (
    select distinct pymast_key
    from (
      select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
        payroll_class, active_code,
        daterange (row_from_date, row_thru_date, '[]') as from_thru,
        daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
      from arkona.xfm_pymast a
      where a.pymast_company_number in ('RY1','RY2')
        and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
    where from_thru && daterange('12/29/2019','04/04/2020', '[]')
      and hire_term && daterange('12/29/2019','04/04/2020', '[]')) b on a.pymast_key = b.pymast_key
  group by store, employee_number, name, hire_date, term_date, payroll_Class, active_code) aa
left join multiple_full_part bb on aa.employee_number = bb.employee_number   
left join multiple_payroll_class cc on aa.employee_number = cc.employee_number
where (bb.employee_number is not null or cc.employee_number is not null)
--   and not exists (
--     select 1
--     from base_employees
--     where name = aa.name
--       and active_code = 'T')
group by aa.store, aa.employee_number, aa.name, distrib_code, hire_Date, term_date    
union
-- terms
select aa.store, aa.employee_number, name, distrib_code, hire_date, min(term_date), payroll_class, max(full_part)
from (
  select store, employee_number, name, max(distrib_code) as distrib_code, hire_date, term_date, payroll_class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as full_part
  from base_employees a
  join (
    select distinct pymast_key
    from (
      select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
        payroll_class, active_code,
        daterange (row_from_date, row_thru_date, '[]') as from_thru,
        daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
      from arkona.xfm_pymast a
      where a.pymast_company_number in ('RY1','RY2')
        and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
    where from_thru && daterange('12/29/2019','04/04/2020', '[]')
      and hire_term && daterange('12/29/2019','04/04/2020', '[]')) b on a.pymast_key = b.pymast_key
  group by store, employee_number, name, hire_date, term_date, payroll_Class, active_code) aa
left join multiple_full_part bb on aa.employee_number = bb.employee_number   
left join multiple_payroll_class cc on aa.employee_number = cc.employee_number
where bb.employee_number is null
  and cc.employee_number is null
  and exists (
    select 1
    from base_employees
    where name = aa.name
      and active_code = 'T')  
group by aa.store, aa.employee_number, name, distrib_code, payroll_class, hire_date;  

--multiple rows
select *
from final_employees a
join (
  select employee_number
  from final_employees
  group by employee_number
  having count(*) > 1) b on a.employee_number = b.employee_number
  order by a.employee_number;

-- select pymast_employee_number, employee_name, distrib_code, active_code, payroll_class, row_from_date, row_thru_date
-- from arkona.xfm_pymast
-- where pymast_employee_number = '167932'
-- order by pymast_key
delete from final_employees where employee_number = '108345' and distrib_code = 'OPE'; 
delete from final_employees where employee_number = '1150920' and distrib_code = 'PTDR'; 
delete from final_employees where employee_number = '143769' and distrib_code = '16'; 
delete from final_employees where employee_number = '147583' and full_part = '*'; 
delete from final_employees where employee_number = '167932' and distrib_code = 'PTDR'; 
delete from final_employees where employee_number = '184620' and distrib_code = 'WTEC'; 
delete from final_employees where name = 'HALL, MICHAEL';
delete from final_employees where name = 'SUMMERS, SAMANTHA';

select *
from final_employees a
join (
  select name
  from final_employees
  group by name
  having count(*) > 1) b on a.name = b.name
  order by a.name;

delete from final_employees where employee_number = '269843'; 
delete from final_employees where employee_number = '247583'; 

-- have to get rid of the astrices
select *
from final_employees
where payroll_class = '*'
  or full_part = '*'
-- select pymast_employee_number, employee_name, distrib_code, active_code, payroll_class, row_from_date, row_thru_date
-- from arkona.xfm_pymast
-- where pymast_employee_number = '1122342'
-- order by pymast_key
update final_employees set payroll_class = 'C' where employee_number in('159863','184614','163278','175393','1122342');
update final_employees set payroll_class = 'H' where employee_number in('15648','174384','1117901');
update final_employees set distrib_code= 'PTEC', payroll_class = 'H' where employee_number = '187564';
update final_employees set full_part = 'part' where employee_number in('294387','267531','1130690','152964'); 
update final_employees set payroll_class = 'S' where employee_number in('256845','1111325');
update final_employees set full_part = 'full' where employee_number in('167834');  


-- compare term date to employee_dim
select *
from (
select *
from final_employees
where term_Date < current_date) a
full outer join (
select distinct storecode, employeenumber, name ,termdate 
from ads.ext_edw_employee_dim where termdate between '01/01/2020' and '03/31/2020') b on a.employee_number = b.employeenumber;


update final_employees z
set term_date = x.termdate 
from (
  select b.*
  from (
    select *
    from final_employees
    where term_Date < current_date) a
  full outer join (
    select distinct storecode, employeenumber, name ,termdate 
    from ads.ext_edw_employee_dim where 
    termdate between '12/29/2019' and '04/04/2020') b on a.employee_number = b.employeenumber
  where a.employee_number is null) x 
where z.employee_number = x.employeenumber;


select * from weeks

drop table if exists the_data;
create temp table the_data as
select aa.store, aa.employee_number, aa.name, distrib_code, hire_date, 
  case
    when term_date = '12/31/9999' then null
    else term_date
  end as term_date, aa.payroll_class, aa.full_part,
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-12-29'), 0) as "12/29-1/4",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-01-05'), 0) as "1/5-1/11",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-01-12'), 0) as "1/12-1/18",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-01-19'), 0) as "1/19-1/25",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-01-26'), 0) as "1/26-2/1",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-02-02'), 0) as "2/2-2/8",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-02-09'), 0) as "2/9-2/15",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-02-16'), 0) as "2/16-2/22",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-02-23'), 0) as "2/23-2/29",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-03-01'), 0) as "3/1-3/7",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-03-08'), 0) as "3/8-3/14",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-03-15'), 0) as "3/15-3/21",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-03-22'), 0) as "3/22-3/28",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2020-03-29'), 0) as "3/29-4/4"
-- select * 
from final_employees aa
left join clock_hours bb on aa.employee_number = bb.employee_number
group by aa.store, aa.employee_number, aa.name, distrib_code, aa.hire_date, aa.term_date, aa.payroll_class, aa.full_part
order by aa.store, aa.name;



SELECT string_agg('"' || attname || '"', ',')
FROM   pg_attribute
WHERE  attrelid = 'the_data'::regclass
AND    attnum > 0
AND    NOT attisdropped

"store","employee_number","name","distrib_code","hire_date","term_date","payroll_class","full_part","1/1-1/4","1/5-1/11","1/12-1/18","1/19-1/25","1/26-2/1","2/2-2/8","2/9-2/15","2/16-2/22","2/23-2/29","3/1-3/7","3/8-3/14","3/15-3/21","3/22-3/28","3/29-3/31"

select * from the_data where employee_number = '168573'

drop table if exists jon.first_quarter_v2;
create table jon.first_quarter_v2 (
  store citext,
  employee_number citext primary key,
  name citext,
  distrib_code citext,
  hire_date date,
  term_date date,
  payroll_class citext,
  full_part citext,
  "12/29-1/4" numeric,
  "1/5-1/11" numeric,
  "1/12-1/18"  numeric,
  "1/19-1/25" numeric,
  "1/26-2/1" numeric,
  "2/2-2/8" numeric,
  "2/9-2/15" numeric,
  "2/16-2/22" numeric,
  "2/23-2/29" numeric,
  "3/1-3/7" numeric,
  "3/8-3/14" numeric,
  "3/15-3/21" numeric,
  "3/22-3/28" numeric,
  "3/29-4/4" numeric);

 insert into jon.first_quarter_v2
 select * from the_data;


select * 
from jon.first_quarter_v2
order by store, name
----------------------------------------------------------------------------------------
-- first_quartter_with_fte spreadsheet

select a.store,a.employee_number, a.name, a.distrib_code, a.hire_date,  a.term_date, 
  a.payroll_class, a.full_part, 
    "1/1-1/4", b.employed, b.fte,
    "1/5-1/11", c.employed, c.fte,
    "1/12-1/18", d.employed, d.fte,
    "1/19-1/25", e.employed, e.fte,
    "1/26-2/1", f.employed, f.fte,
    "2/2-2/8", g.employed, g.fte,
    "2/9-2/15", h.employed, h.fte,
    "2/16-2/22", i.employed, i.fte,
    "2/23-2/29", j.employed, j.fte,
    "3/1-3/7", k.employed, k.fte,
    "3/8-3/14", l.employed, l.fte,
    "3/15-3/21", m.employed, m.fte,
    "3/22-3/28", n.employed, n.fte,
    "3/29-3/31", n.employed, o.fte
from jon.first_quarter a
left join jon.fq_week_1 b on a.employee_number = b.employee_number
left join jon.fq_week_2 c on a.employee_number = c.employee_number
left join jon.fq_week_3 d on a.employee_number = d.employee_number
left join jon.fq_week_4 e on a.employee_number = e.employee_number
left join jon.fq_week_5 f on a.employee_number = f.employee_number
left join jon.fq_week_6 g on a.employee_number = g.employee_number
left join jon.fq_week_7 h on a.employee_number = h.employee_number
left join jon.fq_week_8 i on a.employee_number = i.employee_number
left join jon.fq_week_9 j on a.employee_number = j.employee_number
left join jon.fq_week_10 k on a.employee_number = k.employee_number
left join jon.fq_week_11 l on a.employee_number = l.employee_number
left join jon.fq_week_12 m on a.employee_number = m.employee_number
left join jon.fq_week_13 n on a.employee_number = n.employee_number
left join jon.fq_week_14 o on a.employee_number = o.employee_number
where a.store = 'ry2'
union

select 'z','z', '', '', null::date,  null::date, '', '', 
    null::numeric, null::boolean,sum(b.fte),
    null::numeric, null::boolean,sum(c.fte),
    null::numeric, null::boolean,sum(d.fte),
    null::numeric, null::boolean,sum(e.fte),
    null::numeric, null::boolean,sum(f.fte),
    null::numeric, null::boolean,sum(g.fte),
    null::numeric, null::boolean,sum(h.fte),
    null::numeric, null::boolean,sum(i.fte),
    null::numeric, null::boolean,sum(j.fte),
    null::numeric, null::boolean,sum(k.fte),
    null::numeric, null::boolean,sum(l.fte),
    null::numeric, null::boolean,sum(m.fte),
    null::numeric, null::boolean,sum(n.fte),
    null::numeric, null::boolean,sum(o.fte)
from jon.first_quarter a
left join jon.fq_week_1 b on a.employee_number = b.employee_number
left join jon.fq_week_2 c on a.employee_number = c.employee_number
left join jon.fq_week_3 d on a.employee_number = d.employee_number
left join jon.fq_week_4 e on a.employee_number = e.employee_number
left join jon.fq_week_5 f on a.employee_number = f.employee_number
left join jon.fq_week_6 g on a.employee_number = g.employee_number
left join jon.fq_week_7 h on a.employee_number = h.employee_number
left join jon.fq_week_8 i on a.employee_number = i.employee_number
left join jon.fq_week_9 j on a.employee_number = j.employee_number
left join jon.fq_week_10 k on a.employee_number = k.employee_number
left join jon.fq_week_11 l on a.employee_number = l.employee_number
left join jon.fq_week_12 m on a.employee_number = m.employee_number
left join jon.fq_week_13 n on a.employee_number = n.employee_number
left join jon.fq_week_14 o on a.employee_number = o.employee_number
where a.store = 'ry2'
order by store, name




-- 
-- -- first week is 4 days, so rather than 30 hours, use 20
-- 
-- create table jon.fq_week_1 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_1 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('01/01/2020','01/04/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('01/01/2020','01/04/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "1/1-1/4" = 0 then 0
--         when "1/1-1/4" >= 20 then 1
--         when "1/1-1/4" < 20 then round("1/1-1/4"/20, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "1/1-1/4" = 0 then 0
--         when "1/1-1/4" >= 20 then 1
--         when "1/1-1/4" < 20 then round("1/1-1/4"/20, 1)
--       end     
--   end as fte, daterange('01/01/2020','01/04/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_1;
-- 
-- 
create table jon.fq_week_2 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
insert into jon.fq_week_2 select store, employee_number,
  case when daterange(hire_date, term_date, '[]') && daterange('01/05/2020','01/11/2020', '[]') then true else false end as employed,
  case 
    when not daterange(hire_date, term_date, '[]') && daterange('01/05/2020','01/11/2020', '[]') then 0  -- not employed
    when payroll_class = 'H' then -- all hourly
      case 
        when "1/5-1/11" = 0 then 0
        when "1/5-1/11" >= 30 then 1
        when "1/5-1/11" < 30 then round("1/5-1/11"/30, 1)
      end
    when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
    when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
        (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
        or
        (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
    when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
    when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
    -- full/part doesn't matter, what matters is the number of hours
    when payroll_class = 'C' and (  -- commission with clock hours
        (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
        or
        (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
      case 
        when "1/5-1/11" = 0 then 0
        when "1/5-1/11" >= 30 then 1
        when "1/5-1/11" < 30 then round("1/5-1/11"/30, 1)
      end     
  end as fte, daterange('01/05/2020','01/11/2020', '[]')
from jon.first_quarter a
order by store, name;  
select * from jon.fq_week_2;
-- 
-- create table jon.fq_week_3 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_3 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('01/12/2020','01/18/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('01/12/2020','01/18/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "1/12-1/18" = 0 then 0
--         when "1/12-1/18" >= 30 then 1
--         when "1/12-1/18" < 30 then round("1/12-1/18"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "1/12-1/18" = 0 then 0
--         when "1/12-1/18" >= 30 then 1
--         when "1/12-1/18" < 30 then round("1/12-1/18"/30, 1)
--       end     
--   end as fte, daterange('01/12/2020','01/18/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_3;
-- 
-- create table jon.fq_week_4 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_4 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('01/19/2020','01/25/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('01/19/2020','01/25/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "1/19-1/25" = 0 then 0
--         when "1/19-1/25" >= 30 then 1
--         when "1/19-1/25" < 30 then round("1/19-1/25"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "1/19-1/25" = 0 then 0
--         when "1/19-1/25" >= 30 then 1
--         when "1/19-1/25" < 30 then round("1/19-1/25"/30, 1)
--       end     
--   end as fte, daterange('01/19/2020','01/25/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_4;
-- 
-- create table jon.fq_week_5 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_5 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('01/26/2020','02/01/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('01/26/2020','02/01/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "1/26-2/1" = 0 then 0
--         when "1/26-2/1" >= 30 then 1
--         when "1/26-2/1" < 30 then round("1/26-2/1"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "1/26-2/1" = 0 then 0
--         when "1/26-2/1" >= 30 then 1
--         when "1/26-2/1" < 30 then round("1/26-2/1"/30, 1)
--       end     
--   end as fte, daterange('01/26/2020','02/01/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_5;
-- 
-- create table jon.fq_week_6 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_6 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/02/2020','02/08/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('02/02/2020','02/08/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "2/2-2/8" = 0 then 0
--         when "2/2-2/8" >= 30 then 1
--         when "2/2-2/8" < 30 then round("2/2-2/8"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "2/2-2/8" = 0 then 0
--         when "2/2-2/8" >= 30 then 1
--         when "2/2-2/8" < 30 then round("2/2-2/8"/30, 1)
--       end     
--   end as fte, daterange('02/02/2020','02/08/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_6;
-- 
-- create table jon.fq_week_7 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_7 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/09/2020','02/15/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('02/09/2020','02/15/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "2/9-2/15" = 0 then 0
--         when "2/9-2/15" >= 30 then 1
--         when "2/9-2/15" < 30 then round("2/9-2/15"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "2/9-2/15" = 0 then 0
--         when "2/9-2/15" >= 30 then 1
--         when "2/9-2/15" < 30 then round("2/9-2/15"/30, 1)
--       end     
--   end as fte, daterange('02/09/2020','02/15/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_7;
-- 
-- 
-- create table jon.fq_week_8 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_8 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/16/2020','02/22/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('02/16/2020','02/22/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "2/16-2/22" = 0 then 0
--         when "2/16-2/22" >= 30 then 1
--         when "2/16-2/22" < 30 then round("2/16-2/22"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "2/16-2/22" = 0 then 0
--         when "2/16-2/22" >= 30 then 1
--         when "2/16-2/22" < 30 then round("2/16-2/22"/30, 1)
--       end     
--   end as fte, daterange('02/16/2020','02/22/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_8;
-- 
-- 
-- create table jon.fq_week_9 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_9 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/23/2020','02/29/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('02/23/2020','02/29/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "2/23-2/29" = 0 then 0
--         when "2/23-2/29" >= 30 then 1
--         when "2/23-2/29" < 30 then round("2/23-2/29"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "2/23-2/29" = 0 then 0
--         when "2/23-2/29" >= 30 then 1
--         when "2/23-2/29" < 30 then round("2/23-2/29"/30, 1)
--       end     
--   end as fte, daterange('02/23/2020','02/29/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_9;
-- 
-- 
-- create table jon.fq_week_10 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_10 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/01/2020','03/07/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/01/2020','03/07/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/1-3/7" = 0 then 0
--         when "3/1-3/7" >= 30 then 1
--         when "3/1-3/7" < 30 then round("3/1-3/7"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/1-3/7" = 0 then 0
--         when "3/1-3/7" >= 30 then 1
--         when "3/1-3/7" < 30 then round("3/1-3/7"/30, 1)
--       end     
--   end as fte, daterange('03/01/2020','03/07/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_10;
-- 
-- create table jon.fq_week_11 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_11 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/08/2020','03/14/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/08/2020','03/14/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/8-3/14" = 0 then 0
--         when "3/8-3/14" >= 30 then 1
--         when "3/8-3/14" < 30 then round("3/8-3/14"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/8-3/14" = 0 then 0
--         when "3/8-3/14" >= 30 then 1
--         when "3/8-3/14" < 30 then round("3/8-3/14"/30, 1)
--       end     
--   end as fte, daterange('03/08/2020','03/14/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_11;
-- 
-- 
-- create table jon.fq_week_12 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_12 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/15/2020','03/21/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/15/2020','03/21/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/15-3/21" = 0 then 0
--         when "3/15-3/21" >= 30 then 1
--         when "3/15-3/21" < 30 then round("3/15-3/21"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/15-3/21" = 0 then 0
--         when "3/15-3/21" >= 30 then 1
--         when "3/15-3/21" < 30 then round("3/15-3/21"/30, 1)
--       end     
--   end as fte, daterange('03/15/2020','03/21/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_12;
-- 
-- 
-- create table jon.fq_week_13 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_13 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/22/2020','03/28/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/22/2020','03/28/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/22-3/28" = 0 then 0
--         when "3/22-3/28" >= 30 then 1
--         when "3/22-3/28" < 30 then round("3/22-3/28"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/22-3/28" = 0 then 0
--         when "3/22-3/28" >= 30 then 1
--         when "3/22-3/28" < 30 then round("3/22-3/28"/30, 1)
--       end     
--   end as fte, daterange('03/22/2020','03/28/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_13;
-- 
-- -- week is 3 days, use 15 instead of 30
-- create table jon.fq_week_14 (store citext not null,employee_number citext primary key,employed boolean not null,fte numeric not null,the_week daterange not null); 
-- insert into jon.fq_week_14 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/29/2020','03/31/2020', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/29/2020','03/31/2020', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/29-3/31" = 0 then 0
--         when "3/29-3/31" >= 15 then 1
--         when "3/29-3/31" < 15 then round("3/29-3/31"/15, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/29-3/31" = 0 then 0
--         when "3/29-3/31" >= 15 then 1
--         when "3/29-3/31" < 15 then round("3/29-3/31"/15, 1)
--       end     
--   end as fte, daterange('03/29/2020','03/31/2020', '[]')
-- from jon.first_quarter a
-- order by store, name;  
-- select * from jon.fq_week_14;