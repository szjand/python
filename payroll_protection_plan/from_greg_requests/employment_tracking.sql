﻿/*
Employment Tracking
Need a report for the GM store and the Honda store
Row for every employee that was on our payroll since Oct 1
Columns
  Emp #
  Name
  Commission/Salary/Full Time/Part Time
  Repeating Columns of Clock Hours for each week
This is a start for folks to review and request possible additional columns

4/13
jeri:
Are termed employees included on this sheet?
jon:
If you are referring to the spreadsheet I think you are (employment_tracking_v1), it is based on employees hired before 10/01/2019 employed on 4/12/2020
jeri:
For purposes of calculating FTE, do you all agree that we need those not still employed as well? 
greg:
Yes, if they were working in the time period covered by the spreadsheet. 
jon:
V2 will be laid out the same, with the same columns, but will include anyone employed at any time between 10/01/2019 and today. 
*/


-- assume only employees currently employed
select pymast_company_number, pymast_employee_number, employee_name, payroll_class, active_code
from arkona.xfm_pymast a
where a.pymast_company_number in ('RY1','RY2')
  and arkona.db2_integer_to_date(hire_date) < '10/01/2019'
  and arkona.db2_integer_to_date(termination_date) > '10/01/2019'
  and active_code <> 'T'
  and current_row



select * 
from arkona.xfm_pypclockin
limit 100

select a.store_code, a.employee_number, b.from_date, b.thru_date, sum(a.clock_hours) as clock_hours 
from arkona.xfm_pypclockin a
join (
  select min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week, max(the_date) - min(the_date)
  from dds.dim_date 
  where the_date between '09/29/2019' and current_date
  group by sunday_to_saturday_week) b on a.the_date between b.from_date and b.thru_date
group by a.store_code, a.employee_number, b.from_date, b.thru_date  



select aa.store, aa."emp#", aa.name, aa.payroll_class, aa."full/part",
  coalesce(max(bb.clock_hours) filter (where from_date = '09/29/2019'), 0) as "9/29-10/5",
  coalesce(max(bb.clock_hours) filter (where from_date = '10/06/2019'), 0) as "10/6/10/12",
  coalesce(max(bb.clock_hours) filter (where from_date = '10/13/2019'), 0) as "10/13-10/19",
  coalesce(max(bb.clock_hours) filter (where from_date = '10/20/2019'), 0) as "10/20-10/26",
  coalesce(max(bb.clock_hours) filter (where from_date = '10/27/2019'), 0) as "10/26-11/2",
  coalesce(max(bb.clock_hours) filter (where from_date = '11/03/2019'), 0) as "11/3-/11/9",
  coalesce(max(bb.clock_hours) filter (where from_date = '11/10/2019'), 0) as "11/10-11/16",
  coalesce(max(bb.clock_hours) filter (where from_date = '11/17/2019'), 0) as "11/17-11/23",
  coalesce(max(bb.clock_hours) filter (where from_date = '11/24/2019'), 0) as "11/24-/11/30",
  coalesce(max(bb.clock_hours) filter (where from_date = '12/01/2019'), 0) as "12/1-12/7",
  coalesce(max(bb.clock_hours) filter (where from_date = '12/08/2019'), 0) as "12/8-12/14",
  coalesce(max(bb.clock_hours) filter (where from_date = '12/15/2019'), 0) as "12/15-12/21",
  coalesce(max(bb.clock_hours) filter (where from_date = '12/22/2019'), 0) as "12/22-12/28",
  coalesce(max(bb.clock_hours) filter (where from_date = '12/29/2019'), 0) as "12/29-1/4",
  coalesce(max(bb.clock_hours) filter (where from_date = '01/05/2020'), 0) as "1/5-1/11",
  coalesce(max(bb.clock_hours) filter (where from_date = '01/12/2020'), 0) as "1/12-1/18",
  coalesce(max(bb.clock_hours) filter (where from_date = '01/19/2020'), 0) as "1/19-1/25",
  coalesce(max(bb.clock_hours) filter (where from_date = '01/26/2020'), 0) as "1/26-2/1",
  coalesce(max(bb.clock_hours) filter (where from_date = '02/02/2020'), 0) as "2/2-2/8",
  coalesce(max(bb.clock_hours) filter (where from_date = '02/09/2020'), 0) as "2/9-2-15",
  coalesce(max(bb.clock_hours) filter (where from_date = '02/16/2020'), 0) as "2/16-2/22",
  coalesce(max(bb.clock_hours) filter (where from_date = '02/23/2020'), 0) as "2/23-2/29",
  coalesce(max(bb.clock_hours) filter (where from_date = '03/01/2020'), 0) as "3/1-3/7",
  coalesce(max(bb.clock_hours) filter (where from_date = '03/08/2020'), 0) as "3/8-3/14",
  coalesce(max(bb.clock_hours) filter (where from_date = '03/15/2020'), 0) as "3/15-3/21",
  coalesce(max(bb.clock_hours) filter (where from_date = '03/22/2020'), 0) as "3/22-3/28",
  coalesce(max(bb.clock_hours) filter (where from_date = '03/29/2020'), 0) as "3/29-4/4",
  coalesce(max(bb.clock_hours) filter (where from_date = '04/05/2020'), 0) as "4/5-4/11"
-- select *  
from (
select pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
  payroll_class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as "full/part"
from arkona.xfm_pymast a
where a.pymast_company_number in ('RY1','RY2')
  and arkona.db2_integer_to_date(hire_date) < '10/01/2019'
  and arkona.db2_integer_to_date(termination_date) > '10/01/2019'
  and active_code <> 'T'
  and current_row) aa
left join (
  select a.store_code, a.employee_number, b.from_date, b.thru_date, sum(a.clock_hours) as clock_hours 
  from arkona.xfm_pypclockin a
  join (
    select min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week, max(the_date) - min(the_date)
    from dds.dim_date 
    where the_date between '09/29/2019' and current_date
    group by sunday_to_saturday_week) b on a.the_date between b.from_date and b.thru_date
  group by a.store_code, a.employee_number, b.from_date, b.thru_date)  bb on aa."emp#" = bb.employee_number
group by aa.store, aa."emp#", aa.name, aa.payroll_class, aa."full/part"
order by aa.store, aa.name


--------------------------------------------------------------------------------
--< v2
--------------------------------------------------------------------------------
-- currently employed
select pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
  payroll_class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as "full/part", 
  arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date) 
from arkona.xfm_pymast a
where a.pymast_company_number in ('RY1','RY2')
  and arkona.db2_integer_to_date(hire_date) < '10/01/2019'
  and arkona.db2_integer_to_date(termination_date) > '10/01/2019'
  and active_code <> 'T'
  and current_row

-- termed
select pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
  payroll_class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as "full/part", 
  arkona.db2_integer_to_date(hire_date) as hire_date, arkona.db2_integer_to_date(termination_date) as term_date, active_code,
  row_from_date, row_thru_date
from arkona.xfm_pymast a
where a.pymast_company_number in ('RY1','RY2')
  and arkona.db2_integer_to_date(hire_date) < '10/01/2019'
  and arkona.db2_integer_to_date(termination_date) between '10/02/2019' and current_date
order by pymast_employee_number, row_from_date


select store, "emp#", name, "full/part"
from (
select pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
  payroll_class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as "full/part", 
  arkona.db2_integer_to_date(hire_date) as hire_date, arkona.db2_integer_to_date(termination_date) as term_date, active_code,
  row_from_date, row_thru_date
from arkona.xfm_pymast a
where a.pymast_company_number in ('RY1','RY2')
  and arkona.db2_integer_to_date(hire_date) < '10/01/2019'
  and exists (
    select 1
    from arkona.xfm_pymast
    where pymast_employee_number = a.pymast_employee_number
      and arkona.db2_integer_to_date(termination_date) between '10/02/2019' and current_date)) aa
where "full/part" is not null   
group by store, "emp#", name, "full/part"   



select pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
  payroll_class, active_code,
--   case active_code
--     when 'A' then 'full'
--     when 'P' then 'part'
--   end as "full/part", 
  arkona.db2_integer_to_date(hire_date) as hire_date, arkona.db2_integer_to_date(termination_date) as term_date, 
  row_from_date, row_thru_date
from arkona.xfm_pymast a
where a.pymast_company_number in ('RY1','RY2')  
  and a.employee_name like 'A%'
  and arkona.db2_integer_to_date(hire_date) < '09/29/2019'
  and arkona.db2_integer_to_date(termination_date) > '10/05/2019'
--   and a.row_thru_date < '10/05/2019'
order by a.pymast_employee_number, a.row_from_date  

these 3 return multiple rows
axtman 18005, anderson 187632 alsleben 12725


  select min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week, max(the_date) - min(the_date)
  from dds.dim_date 
  where the_date between '09/29/2019' and '10/15/2019'
  group by sunday_to_saturday_week

select * from arkona.xfm_pymast where pymast_employee_number = '135872' 


-- this looks good
-- need to generalize it, for the larger date range 9/29 - 4/11
-- leaning with, at this level, going with the pymast_key, unambiguous single row identifier
select distinct pymast_key
from (
  select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
    payroll_class, active_code,
    daterange (row_from_date, row_thru_date, '[]') as from_thru,
    daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
  from arkona.xfm_pymast a
  where a.pymast_company_number in ('RY1','RY2')
    and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
where from_thru && daterange('09/29/2019', '10/05/2019', '[]')
  and hire_term && daterange('09/29/2019', '10/05/2019', '[]')
  and active_code <> 'T'

**************************************************************************************************
-- then i had the conversation with greg and jeri
**************************************************************************************************

drop table if exists weeks;
create temp table weeks as
select daterange(min(the_date), max(the_date), '[]') as week_range, 
  min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week
from dds.dim_date 
where the_date between '01/01/2020' and '03/31/2020'
group by sunday_to_saturday_week
order by sunday_to_saturday_week;


-- employed at anytime
drop table if exists base_employees;
create temp table base_employees as
select pymast_company_number as store, pymast_employee_number employee_number, employee_name as name, 
  arkona.db2_integer_to_date(hire_date) as hire_date,
  arkona.db2_integer_to_date(termination_date) as term_date,
  payroll_class, active_code, current_row, pymast_key, row_From_date, row_thru_date,
  daterange (row_from_date, row_thru_date, '[]') as from_thru,
  daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
from arkona.xfm_pymast 
where arkona.db2_integer_to_date(hire_date) < '03/31/2020'
  and arkona.db2_integer_to_date(termination_date) > '01/01/2020';
delete from base_employees where employee_number = '268573';

select * from base_employees order by store, name


-- multiple payroll_class
drop table if exists multiple_payroll_class;
create temp table multiple_payroll_class as
select employee_number              
from (
select employee_number, payroll_class
from base_employees
where row_from_date between '01/01/2020' and '03/31/2020'
  or row_thru_date between  '01/01/2020' and '03/31/2020'
group by employee_number, payroll_class) a
group by employee_number having count(*) > 1;


-- multiple full_part
drop table if exists multiple_full_part;
create temp table multiple_full_part as
select employee_number              
from (
select employee_number , active_code
from base_employees
where active_code in ('A','P')
and ( row_from_date between '01/01/2020' and '03/31/2020'
  or row_thru_date between  '01/01/2020' and '03/31/2020')
group by employee_number, active_code) a
group by employee_number having count(*) > 1;


-- no employees with multiple employee_numbers in this period
select name
from (
select employee_number, name
from base_employees
where row_from_date between '01/01/2020' and '03/31/2020'
  or row_thru_date between  '01/01/2020' and '03/31/2020'
group by employee_number, name) a
group by name having count(*) > 1

select * from base_employees where name in ('BOOKER, JEREMIAH','VOELKER, JUSTIN') order by name ,employee_number, pymast_key


select pymast_employee_number, employee_name, current_row, hire_Date, termination_date, row_From_date, row_thru_date, pymast_key 
from arkona.xfm_pymast
where employee_name in ('BOOKER, JEREMIAH','VOELKER, JUSTIN') 
order by employee_name, pymast_employee_number, pymast_key


select * 
from arkona.xfm_pymast
where pymast_employee_number = '1100820'


SELECT NAME from (
select * 
from base_employees
where from_thru && daterange('01/01/2020', '03/31/2020', '[]')
  and hire_term && daterange('01/01/2020', '03/31/2020', '[]')
  and active_code <> 'T'  
) x group by name having count(*) > 1


select * from base_employees where name = 'AGUILAR, ANNA' order by pymast_key

select *
from base_employees aa
join (
  SELECT NAME from (
    select store, employee_number , name, term_date, payroll_class, active_code 
    from base_employees
    where from_thru && daterange('01/01/2020', '03/31/2020', '[]')
      and hire_term && daterange('01/01/2020', '03/31/2020', '[]')
      and active_code <> 'T'  
    group by store, employee_number, name, term_date, payroll_class, active_code) x group by name having count(*) > 1) bb on aa.name = bb.name
order by aa.name, pymast_key

drop table if exists employee_numbers;
create temp table employee_numbers as
  select distinct employee_number              
  from base_employees
  where from_thru && daterange('01/01/2020', '03/31/2020', '[]')
    and hire_term && daterange('01/01/2020', '03/31/2020', '[]')
--     and active_code <> 'T'  
  group by store, employee_number, name, term_date, payroll_class, active_code;

select * from arkona.xfm_pypclockin limit 10

drop table if exists clock_hours;
create temp table clock_hours as
select a.store_code, a.employee_number, b.from_date, b.thru_date, 
  sum(a.clock_hours) as clock_hours, sum(a.vac_hours + a.pto_hours) as pto_hours , sum(a.hol_hours) as holiday_hours
from arkona.xfm_pypclockin a
join employee_numbers aa on a.employee_number = aa.employee_number
join weeks b on a.the_date between b.from_date and b.thru_date
group by a.store_code, a.employee_number, b.from_date, b.thru_date;  




select distinct pymast_key
from (
  select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
    payroll_class, active_code,
    daterange (row_from_date, row_thru_date, '[]') as from_thru,
    daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
  from arkona.xfm_pymast a
  where a.pymast_company_number in ('RY1','RY2')
    and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
where from_thru && daterange('01/01/2020', '03/31/2020', '[]')
  and hire_term && daterange('01/01/2020', '03/31/2020', '[]')
  and active_code <> 'T'

dammit this shows zacha 1150920 with multiple payroll class in the period
that might be good
nope, it shows NO term dates
select *
from base_employees a
join (
  select distinct pymast_key
  from (
    select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
      payroll_class, active_code,
      daterange (row_from_date, row_thru_date, '[]') as from_thru,
      daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
    from arkona.xfm_pymast a
    where a.pymast_company_number in ('RY1','RY2')
      and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
  where from_thru && daterange('01/01/2020', '03/31/2020', '[]')
    and hire_term && daterange('01/01/2020', '03/31/2020', '[]')
    /*and active_code <> 'T'*/) b on a.pymast_key = b.pymast_key
  order by name, a.pymast_key   

select * from base_employees where employee_number = '1150920' order by pymast_key


drop table if exists final_employees;
create temp table final_employees as
-- single row employees
select aa.*
from (
  select store, employee_number, name, hire_date, term_date, payroll_Class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as full_part
  from base_employees a
  join (
    select distinct pymast_key
    from (
      select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
        payroll_class, active_code,
        daterange (row_from_date, row_thru_date, '[]') as from_thru,
        daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
      from arkona.xfm_pymast a
      where a.pymast_company_number in ('RY1','RY2')
        and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
    where from_thru && daterange('01/01/2020', '03/31/2020', '[]')
      and hire_term && daterange('01/01/2020', '03/31/2020', '[]')) b on a.pymast_key = b.pymast_key
  group by store, employee_number, name, hire_date, term_date, payroll_Class, active_code) aa
left join multiple_full_part bb on aa.employee_number = bb.employee_number   
left join multiple_payroll_class cc on aa.employee_number = cc.employee_number
where bb.employee_number is null
  and cc.employee_number is null
  and not exists (
    select 1
    from base_employees
    where name = aa.name
      and active_code = 'T')
union      
-- multiple class/active
select aa.store, aa.employee_number, aa.name, hire_date, term_date, 
--   array_agg(distinct payroll_class), array_agg(distinct active_code),
--   cardinality(array_agg(distinct payroll_class)), cardinality(array_agg(distinct active_code)),
  case
    when cardinality(array_agg(distinct payroll_class)) = 2 then '*'
    else (array_agg(distinct payroll_class))[1]
  end as payroll_class,
  case
    when cardinality(array_agg(distinct active_code)) = 2 then '*'
    when cardinality(array_agg(distinct active_code)) = 1 and (array_agg(distinct active_code))[1] = 'A' then 'full'
    when cardinality(array_agg(distinct active_code)) = 1 and (array_agg(distinct active_code))[1] = 'P' then 'part'
  end as full_part
from (
  select store, employee_number, name, hire_date, term_date, payroll_Class, active_code
  from base_employees a
  join (
    select distinct pymast_key
    from (
      select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
        payroll_class, active_code,
        daterange (row_from_date, row_thru_date, '[]') as from_thru,
        daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
      from arkona.xfm_pymast a
      where a.pymast_company_number in ('RY1','RY2')
        and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
    where from_thru && daterange('01/01/2020', '03/31/2020', '[]')
      and hire_term && daterange('01/01/2020', '03/31/2020', '[]')) b on a.pymast_key = b.pymast_key
  group by store, employee_number, name, hire_date, term_date, payroll_Class, active_code) aa
left join multiple_full_part bb on aa.employee_number = bb.employee_number   
left join multiple_payroll_class cc on aa.employee_number = cc.employee_number
where (bb.employee_number is not null or cc.employee_number is not null)
  and not exists (
    select 1
    from base_employees
    where name = aa.name
      and active_code = 'T')
group by aa.store, aa.employee_number, aa.name, hire_Date, term_date    
union
-- terms
select aa.store, aa.employee_number, name, hire_date, min(term_date), payroll_class, max(full_part)
from (
  select store, employee_number, name, hire_date, term_date, payroll_class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as full_part
  from base_employees a
  join (
    select distinct pymast_key
    from (
      select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
        payroll_class, active_code,
        daterange (row_from_date, row_thru_date, '[]') as from_thru,
        daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
      from arkona.xfm_pymast a
      where a.pymast_company_number in ('RY1','RY2')
        and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
    where from_thru && daterange('01/01/2020', '03/31/2020', '[]')
      and hire_term && daterange('01/01/2020', '03/31/2020', '[]')) b on a.pymast_key = b.pymast_key
  group by store, employee_number, name, hire_date, term_date, payroll_Class, active_code) aa
left join multiple_full_part bb on aa.employee_number = bb.employee_number   
left join multiple_payroll_class cc on aa.employee_number = cc.employee_number
where bb.employee_number is null
  and cc.employee_number is null
  and exists (
    select 1
    from base_employees
    where name = aa.name
      and active_code = 'T')  
group by aa.store, aa.employee_number, name, payroll_class, hire_date;

select aa.store, aa.employee_number, aa.name, hire_date, 
  case
    when term_date = '12/31/9999' then null
    else term_date
  end as term_date, aa.payroll_class, aa.full_part,
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '01/01/2020'), 0) as "1/1-1/4",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '01/05/2020'), 0) as "1/5-1/11",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '01/12/2020'), 0) as "1/12-1/18",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '01/19/2020'), 0) as "1/19-1/25",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '01/26/2020'), 0) as "1/26-2/1",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '02/02/2020'), 0) as "2/2-2/8",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '02/09/2020'), 0) as "2/9-2-15",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '02/16/2020'), 0) as "2/16-2/22",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '02/23/2020'), 0) as "2/23-2/29",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '03/01/2020'), 0) as "3/1-3/7",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '03/08/2020'), 0) as "3/8-3/14",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '03/15/2020'), 0) as "3/15-3/21",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '03/22/2020'), 0) as "3/22-3/28",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '03/29/2020'), 0) as "3/29-3/31"
-- select * 
from final_employees aa
left join clock_hours bb on aa.employee_number = bb.employee_number
group by aa.store, aa.employee_number, aa.name, aa.hire_date, aa.term_date, aa.payroll_class, aa.full_part
order by aa.store, aa.name




-- double employee_number
select *
from final_employees a
join (
select employee_number
from final_employees
group by employee_number
having count(*) > 1) b on a.employee_number = b.employee_number
order by a.employee_number

delete from final_employees where name in ('SUNDBY, AMANDA','HALL, MICHAEL','SUMMERS, SAMANTHA')


-- missing terms
select *
from final_employees
where term_date < current_Date
order by name

update final_employees -- catlin
set term_Date = '03/18/2020'
-- select * from final_employees
where employee_number = '185340';

update final_employees  -- hall
set term_Date = '03/09/2020'
-- select * from final_employees
where employee_number = '198521';

update final_employees --lougee
set term_Date = '03/26/2020'
-- select * from final_employees
where employee_number = '249315';

update final_employees --morgan
set term_Date = '03/26/2020'
-- select * from final_employees
where employee_number = '173286';

update final_employees --nelp
set term_Date = '03/19/2020'
-- select * from final_employees
where employee_number = '158997';

update final_employees --quinlan 
set term_Date = '03/19/2020'
-- select * from final_employees
where employee_number = '132856';

update final_employees --suda 
set term_Date = '03/11/2020'
-- select * from final_employees
where employee_number = '169853';

update final_employees --troemner 
set term_Date = '03/12/2020'
-- select * from final_employees
where employee_number = '297324';

update final_employees --winkler 
set term_Date = '03/20/2020'
-- select * from final_employees
where employee_number = '1149300';


---------------------------------------------------------------------------------------------------
-- look back
---------------------------------------------------------------------------------------------------