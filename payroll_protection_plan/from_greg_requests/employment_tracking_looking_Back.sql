﻿drop table if exists weeks;
drop table if exists weeks;
create temp table weeks as
select daterange(min(the_date), max(the_date), '[]') as week_range, 
  min(the_date) as from_date, max(the_date) as thru_date, sunday_to_saturday_week
from dds.dim_date 
where the_date between '02/10/2019' and '07/06/2019'
group by sunday_to_saturday_week
order by sunday_to_saturday_week;

select * from weeks;

-- employed at anytime
drop table if exists base_employees;
create temp table base_employees as
select pymast_company_number as store, pymast_employee_number employee_number, employee_name as name, distrib_code,
  arkona.db2_integer_to_date(hire_date) as hire_date,
  arkona.db2_integer_to_date(termination_date) as term_date,
  payroll_class, active_code, current_row, pymast_key, row_From_date, row_thru_date,
  daterange (row_from_date, row_thru_date, '[]') as from_thru,
  daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
from arkona.xfm_pymast a
where arkona.db2_integer_to_date(hire_date) < '07/06/2019'
  and arkona.db2_integer_to_date(termination_date) > '02/10/2019'
  and pymast_employee_number <> '1112425'  -- jim price
  and not exists (
    select 1
    from arkona.xfm_pymast
    where pymast_employee_number = a.pymast_employee_number
      and current_row
      and arkona.db2_integer_to_date(termination_date) < '02/10/2019')
order by store, name

-- multiple payroll_class
drop table if exists multiple_payroll_class;
create temp table multiple_payroll_class as
select employee_number              
from (
select employee_number, payroll_class
from base_employees
where row_from_date between  '02/10/2019' and '07/06/2019'
  or row_thru_date between   '02/10/2019' and '07/06/2019'
group by employee_number, payroll_class) a
group by employee_number having count(*) > 1;

select * 
from base_employees a
join (
select employee_number              
from (
select employee_number, distrib_code
from base_employees
where row_from_date between  '02/10/2019' and '07/06/2019'
  or row_thru_date between   '02/10/2019' and '07/06/2019'
group by employee_number, distrib_code) a
group by employee_number having count(*) > 1) b on a.employee_number = b.employee_number
order by a.employee_number, row_from_Date


-- multiple full_part
drop table if exists multiple_full_part;
create temp table multiple_full_part as
select employee_number              
from (
select employee_number , active_code
from base_employees
where active_code in ('A','P')
and ( row_from_date between '02/10/2019' and '07/06/2019'
  or row_thru_date between  '02/10/2019' and '07/06/2019')
group by employee_number, active_code) a
group by employee_number having count(*) > 1;

-- several employees changed stores
select name
from (
select employee_number, name
from base_employees
where row_from_date between '02/10/2019' and '07/06/2019'
  or row_thru_date between  '02/10/2019' and '07/06/2019'
group by employee_number, name) a
group by name having count(*) > 1

select *
from base_employees a
join (
select name
from (
select employee_number, name
from base_employees
where row_from_date between '02/10/2019' and '07/06/2019'
  or row_thru_date between  '02/10/2019' and '07/06/2019'
group by employee_number, name) a
group by name having count(*) > 1) b on a.name = b.name
order by a.name, store, pymast_key

drop table if exists employee_numbers;
create temp table employee_numbers as
  select distinct employee_number              
  from base_employees
  where from_thru && daterange('02/10/2019','07/06/2019', '[]')
    and hire_term && daterange('02/10/2019','07/06/2019', '[]')
--     and active_code <> 'T'  
  group by store, employee_number, name, term_date, payroll_class, active_code;

drop table if exists clock_hours;
create temp table clock_hours as
select a.store_code, a.employee_number, b.from_date, b.thru_date, 
  sum(a.clock_hours) as clock_hours, sum(a.vac_hours + a.pto_hours) as pto_hours , sum(a.hol_hours) as holiday_hours
from arkona.xfm_pypclockin a
join employee_numbers aa on a.employee_number = aa.employee_number
join weeks b on a.the_date between b.from_date and b.thru_date
group by a.store_code, a.employee_number, b.from_date, b.thru_date;   


drop table if exists final_employees;
create temp table final_employees as
-- single row employees
select aa.*
from (
  select store, employee_number, name, max(distrib_code) as distrib_code, hire_date, term_date, payroll_Class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as full_part
  from base_employees a
  join (
    select distinct pymast_key
    from (
      select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
        payroll_class, active_code,
        daterange (row_from_date, row_thru_date, '[]') as from_thru,
        daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
      from arkona.xfm_pymast a
      where a.pymast_company_number in ('RY1','RY2')
        and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
    where from_thru && daterange('02/10/2019','07/06/2019', '[]')
      and hire_term && daterange('02/10/2019','07/06/2019', '[]')) b on a.pymast_key = b.pymast_key
  group by store, employee_number, name, hire_date, term_date, payroll_Class, active_code) aa
left join multiple_full_part bb on aa.employee_number = bb.employee_number   
left join multiple_payroll_class cc on aa.employee_number = cc.employee_number
where bb.employee_number is null
  and cc.employee_number is null
  and not exists (
    select 1
    from base_employees
    where name = aa.name
      and active_code = 'T')
union      
-- multiple class/active
select aa.store, aa.employee_number, aa.name, distrib_code, hire_date, term_date, 
--   array_agg(distinct payroll_class), array_agg(distinct active_code),
--   cardinality(array_agg(distinct payroll_class)), cardinality(array_agg(distinct active_code)),
  case
    when cardinality(array_agg(distinct payroll_class)) > 1 then '*'
    else (array_agg(distinct payroll_class))[1]
  end as payroll_class,
  case
    when cardinality(array_agg(distinct active_code)) > 1 then '*'
    when cardinality(array_agg(distinct active_code)) = 1 and (array_agg(distinct active_code))[1] = 'A' then 'full'
    when cardinality(array_agg(distinct active_code)) = 1 and (array_agg(distinct active_code))[1] = 'P' then 'part'
  end as full_part
from (
  select store, employee_number, name, max(distrib_code) as distrib_code, hire_date, term_date, payroll_Class, active_code
  from base_employees a
  join (
    select distinct pymast_key
    from (
      select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
        payroll_class, active_code,
        daterange (row_from_date, row_thru_date, '[]') as from_thru,
        daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
      from arkona.xfm_pymast a
      where a.pymast_company_number in ('RY1','RY2')
        and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
    where from_thru && daterange('02/10/2019','07/06/2019', '[]')
      and hire_term && daterange('02/10/2019','07/06/2019', '[]')) b on a.pymast_key = b.pymast_key
  group by store, employee_number, name, hire_date, term_date, payroll_Class, active_code) aa
left join multiple_full_part bb on aa.employee_number = bb.employee_number   
left join multiple_payroll_class cc on aa.employee_number = cc.employee_number
where (bb.employee_number is not null or cc.employee_number is not null)
--   and not exists (
--     select 1
--     from base_employees
--     where name = aa.name
--       and active_code = 'T')
group by aa.store, aa.employee_number, aa.name, distrib_code, hire_Date, term_date    
union
-- terms
select aa.store, aa.employee_number, name, distrib_code, hire_date, min(term_date), payroll_class, max(full_part)
from (
  select store, employee_number, name, max(distrib_code) as distrib_code, hire_date, term_date, payroll_class, 
  case active_code
    when 'A' then 'full'
    when 'P' then 'part'
  end as full_part
  from base_employees a
  join (
    select distinct pymast_key
    from (
      select pymast_key, pymast_company_number as store, pymast_employee_number as "emp#", employee_name as name, 
        payroll_class, active_code,
        daterange (row_from_date, row_thru_date, '[]') as from_thru,
        daterange(arkona.db2_integer_to_date(hire_date), arkona.db2_integer_to_date(termination_date), '[]') as hire_term
      from arkona.xfm_pymast a
      where a.pymast_company_number in ('RY1','RY2')
        and arkona.db2_integer_to_date(hire_date) <= arkona.db2_integer_to_date(termination_date)) x
    where from_thru && daterange('02/10/2019','07/06/2019', '[]')
      and hire_term && daterange('02/10/2019','07/06/2019', '[]')) b on a.pymast_key = b.pymast_key
  group by store, employee_number, name, hire_date, term_date, payroll_Class, active_code) aa
left join multiple_full_part bb on aa.employee_number = bb.employee_number   
left join multiple_payroll_class cc on aa.employee_number = cc.employee_number
where bb.employee_number is null
  and cc.employee_number is null
  and exists (
    select 1
    from base_employees
    where name = aa.name
      and active_code = 'T')  
group by aa.store, aa.employee_number, name, distrib_code, payroll_class, hire_date;


   
--multiple rows
select *
from final_employees a
join (
  select employee_number
  from final_employees
  group by employee_number
  having count(*) > 1) b on a.employee_number = b.employee_number
order by a.employee_number

-- select pymast_employee_number, employee_name, distrib_code, active_code, payroll_class, row_from_date, row_thru_date
-- from arkona.xfm_pymast
-- where pymast_employee_number = '167932'
-- order by pymast_key
delete from final_employees where employee_number = '1121250' and distrib_code = 'CWAS';
delete from final_employees where employee_number = '113011' and distrib_code = 'SRVM';
delete from final_employees where employee_number = '123896' and distrib_code = 'LOT';
update final_employees set full_part = 'full' where employee_number = '123896';
delete from final_employees where employee_number = '151702' and distrib_code = 'BTEA';
delete from final_employees where employee_number = '154783' and hire_Date = '05/03/2019';
delete from final_employees where employee_number = '156988' and distrib_code = '14';
delete from final_employees where employee_number = '167934' and hire_Date = '2019-01-01';
delete from final_employees where employee_number = '196341' and hire_Date = '2008-08-18';
delete from final_employees where name = 'SCHADT, MICAHEL';
delete from final_employees where employee_number = '211232' and hire_Date = '2017-03-27';
delete from final_employees where employee_number = '2114160' and hire_Date = '1999-11-02';
delete from final_employees where employee_number = '2135031' and hire_Date = '2016-02-01';
delete from final_employees where employee_number = '223654' and hire_Date = '2018-08-13';
delete from final_employees where employee_number = '242539' and hire_Date = '2019-06-03';
delete from final_employees where employee_number = '243180' and hire_Date = '2017-10-09';
delete from final_employees where employee_number = '254976' and payroll_class in ('*','H');
delete from final_employees where employee_number = '269852' and payroll_class = 'C';
update final_employees  set payroll_class = 'C' where employee_number = '269852';


select *
from final_employees a
join (
  select name
  from final_employees
  group by name
  having count(*) > 1) b on a.name = b.name
  order by a.name;

delete from final_employees where employee_number = '265875';  
delete from final_employees where employee_number = '111232'; 
delete from final_employees where employee_number = '112589'; 
delete from final_employees where employee_number = '110123'; 
delete from final_employees where employee_number = '154976'; 
delete from final_employees where employee_number = '175873'; 
delete from final_employees where employee_number = '175873'; 

select * from final_employees where payroll_class = '*'
update final_employees set payroll_class = 'H' where employee_number = '15648';
update final_employees set payroll_class = 'C' where employee_number = '186531';
-- update final_employees set payroll_class = 'H' where employee_number = '151702';
update final_employees set payroll_class = 'H' where employee_number = '1112411';
update final_employees set payroll_class = 'C' where employee_number = '1121250';
update final_employees set payroll_class = 'C' where employee_number = '169542';
update final_employees set payroll_class = 'S' where employee_number = '256845';
update final_employees set payroll_class = 'H' where employee_number = '1566882';

select * from final_employees where full_part = '*'
update final_employees set full_part = 'part' where employee_number = '165783';
-- update final_employees set full_part = 'part' where employee_number = '156988';
-- update final_employees set full_part = 'part' where employee_number = '113011';
update final_employees set full_part = 'part' where employee_number = '1100820';
update final_employees set full_part = 'part' where employee_number = '143895';
update final_employees set full_part = 'full' where employee_number = '1135710';



  
select *
from (
select *
from final_employees
where term_Date < current_date) a
full outer join (
select distinct storecode, employeenumber, name ,termdate 
from ads.ext_edw_employee_dim where termdate between '02/10/2019' and '07/06/2019') b on a.employee_number = b.employeenumber


update final_employees z
set term_date = x.termdate 
from (
  select b.*
  from (
    select *
    from final_employees
    where term_Date < current_date) a
  full outer join (
    select distinct storecode, employeenumber, name ,termdate 
    from ads.ext_edw_employee_dim where 
    termdate between '02/10/2019' and '07/06/2019') b on a.employee_number = b.employeenumber
  where a.employee_number is null) x 
where z.employee_number = x.employeenumber  

select * from weeks

drop table if exists the_data;
create temp table the_data as
select aa.store, aa.employee_number, aa.name, distrib_code, hire_date, 
  case
    when term_date = '12/31/9999' then null
    else term_date
  end as term_date, aa.payroll_class, aa.full_part,
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-02-10'), 0) as "2/10-2/16",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-02-17'), 0) as "2/17-2/23",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-02-24'), 0) as "2/24-3/2",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-03-03'), 0) as "3/3-3/9",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-03-10'), 0) as "3/10-3/16",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-03-17'), 0) as "3/17-3/23",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-03-24'), 0) as "3/24-3/30",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-03-31'), 0) as "3/31-4/6",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-04-07'), 0) as "4/7-4/13",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-04-14'), 0) as "4/14-4/20",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-04-21'), 0) as "4/21/4/27",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-04-28'), 0) as "4/28-5/4",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-05-05'), 0) as "5/5-5/11",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-05-12'), 0) as "5/12-5/18",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-05-19'), 0) as "5/19-5/25",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-05-26'), 0) as "5/26-6/1",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-06-02'), 0) as "6/2-6/8",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-06-09'), 0) as "6/9-6/15",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-06-16'), 0) as "6/16-6/22",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-06-23'), 0) as "6/23-6/29",
  coalesce(sum(bb.clock_hours + bb.pto_hours + bb.holiday_hours) filter (where from_date = '2019-06-30'), 0) as "6/30-7/6"
-- select * 
from final_employees aa
left join clock_hours bb on aa.employee_number = bb.employee_number
group by aa.store, aa.employee_number, aa.name, distrib_code, aa.hire_date, aa.term_date, aa.payroll_class, aa.full_part
order by aa.store, aa.name;

select *
from the_data
where payroll_class = '*'
  or full_part = '*'
-- 
-- -- multiple_payroll_class
-- select store, a.employee_number, name, term_date, payroll_class, active_code, min(row_from_Date) as row_from_date 
-- from base_employees a 
-- join multiple_payroll_class b on a.employee_number = b.employee_number 
-- where from_thru && daterange('02/10/2019','07/06/2019', '[]') 
-- group by store, a.employee_number, name, term_date, payroll_class, active_code 
-- order by name, row_from_date
-- 
-- -- multiple_full_part
-- select store, a.employee_number, name, term_date, payroll_class, active_code, min(row_from_Date) as row_from_date 
-- from base_employees a 
-- join multiple_full_part b on a.employee_number = b.employee_number 
-- where from_thru && daterange('02/10/2019','07/06/2019', '[]') 
-- group by store, a.employee_number, name, term_date, payroll_class, active_code 
-- order by name, row_from_date




SELECT string_agg('"' || attname || '"', ',')
FROM   pg_attribute
WHERE  attrelid = 'the_data'::regclass
AND    attnum > 0
AND    NOT attisdropped




drop table if exists jon.looking_back;
create table jon.looking_back (
  store citext,
  employee_number citext primary key,
  name citext,
  distrib_code citext,
  hire_date date,
  term_date date,
  payroll_class citext,
  full_part citext,
  "2/10-2/16" numeric,
  "2/17-2/23" numeric,
  "2/24-3/2" numeric,
  "3/3-3/9" numeric,
  "3/10-3/16" numeric,
  "3/17-3/23" numeric,
  "3/24-3/30" numeric,
  "3/31-4/6" numeric,
  "4/7-4/13" numeric,
  "4/14-4/20" numeric,
  "4/21/4/27" numeric,
  "4/28-5/4" numeric,
  "5/5-5/11" numeric,
  "5/12-5/18" numeric,
  "5/19-5/25" numeric,
  "5/26-6/1" numeric,
  "6/2-6/8" numeric,
  "6/9-6/15" numeric,
  "6/16-6/22" numeric,
  "6/23-6/29" numeric,
  "6/30-7/6" numeric);


-- 4/22 
with lots more changes, important to retain what has been produced
the base data is jon.looking_back



the fte calcualtions go into the week tables
renamed the original tables because we now have to generate fte calculations with an additional 2 set of rules


-- alter table jon.week_1
-- rename to looking_back_original_week_1;
-- alter table jon.week_2
-- rename to looking_back_original_week_2;
-- alter table jon.week_3
-- rename to looking_back_original_week_3;
-- alter table jon.week_4
-- rename to looking_back_original_week_4;
-- alter table jon.week_5
-- rename to looking_back_original_week_5;
-- alter table jon.week_6
-- rename to looking_back_original_week_6;
-- alter table jon.week_7
-- rename to looking_back_original_week_7;
-- alter table jon.week_8
-- rename to looking_back_original_week_8;
-- alter table jon.week_9
-- rename to looking_back_original_week_9;
-- alter table jon.week_10
-- rename to looking_back_original_week_10;
-- alter table jon.week_11
-- rename to looking_back_original_week_11;
-- alter table jon.week_12
-- rename to looking_back_original_week_12;
-- alter table jon.week_13
-- rename to looking_back_original_week_13;
-- alter table jon.week_14
-- rename to looking_back_original_week_14;
-- alter table jon.week_15
-- rename to looking_back_original_week_15;
-- alter table jon.week_16
-- rename to looking_back_original_week_16;
-- alter table jon.week_17
-- rename to looking_back_original_week_17;
-- alter table jon.week_18
-- rename to looking_back_original_week_18;
-- alter table jon.week_19
-- rename to looking_back_original_week_19;
-- alter table jon.week_20
-- rename to looking_back_original_week_20;
-- alter table jon.week_21
-- rename to looking_back_original_week_21;


select * from (
select the_week, store, sum(fte) from jon.week_1 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_2 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_3 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_4 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_5 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_6 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_7 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_8 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_9 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_10 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_11 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_12 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_13 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_14 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_15 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_16 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_17 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_18 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_19 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_20 group by the_week, store
union all
select the_week, store, sum(fte) from jon.week_21 group by the_week, store
order by the_week, store
) x where store = 'ry2'

select a.store, a.name, a.employee_number, a.payroll_class, full_part, "3/10-3/16"
from jon.looking_back a
join jon.week_5 b on a.employee_number = b.employee_number
where b.fte = 0 and b.employed

select * from jon.week_5 where fte = 0 and employed

select * from jon.week_5 where fte <> 0 and not employed

----------------------------------------------------------------------------------------
-- looking_back_with_fte spreadsheet
select a.store,a.employee_number, a.name, a.distrib_code, a.hire_date,  a.term_date, 
  a.payroll_class, a.full_part, 
    "2/10-2/16", b.employed, b.fte,
    "2/17-2/23", c.employed, c.fte,
    "2/24-3/2", d.employed, d.fte,
    "3/3-3/9", e.employed, e.fte,
    "3/10-3/16", f.employed, f.fte,
    "3/17-3/23", g.employed, g.fte,
    "3/24-3/30", h.employed, h.fte,  -- this was g
    "3/31-4/6", i.employed, i.fte,
    "4/7-4/13", j.employed, j.fte,
    "4/14-4/20", k.employed, k.fte,
    "4/21-4/27", l.employed, l.fte,
    "4/28-5/4", m.employed, m.fte,
    "5/5-5/11", n.employed, n.fte,
    "5/12-5/18", o.employed, o.fte,
    "5/19-5/25", p.employed, p.fte,
    "5/26-6/1", q.employed, q.fte,
    "6/2-6/8",  r.employed, r.fte,
    "6/9-6/15", s.employed, s.fte,
    "6/16-6/22", t.employed, t.fte,
    "6/23-6/29", u.employed, u.fte,
    "6/30-7/6", v.employed, v.fte
from jon.looking_back a
left join jon.week_1 b on a.employee_number = b.employee_number
left join jon.week_2 c on a.employee_number = c.employee_number
left join jon.week_3 d on a.employee_number = d.employee_number
left join jon.week_4 e on a.employee_number = e.employee_number
left join jon.week_5 f on a.employee_number = f.employee_number
left join jon.week_6 g on a.employee_number = g.employee_number
left join jon.week_7 h on a.employee_number = h.employee_number
left join jon.week_8 i on a.employee_number = i.employee_number
left join jon.week_9 j on a.employee_number = j.employee_number
left join jon.week_10 k on a.employee_number = k.employee_number
left join jon.week_11 l on a.employee_number = l.employee_number
left join jon.week_12 m on a.employee_number = m.employee_number
left join jon.week_13 n on a.employee_number = n.employee_number
left join jon.week_14 o on a.employee_number = o.employee_number
left join jon.week_15 p on a.employee_number = p.employee_number
left join jon.week_16 q on a.employee_number = q.employee_number
left join jon.week_17 r on a.employee_number = r.employee_number
left join jon.week_18 s on a.employee_number = s.employee_number
left join jon.week_19 t on a.employee_number = t.employee_number
left join jon.week_20 u on a.employee_number = u.employee_number
left join jon.week_21 v on a.employee_number = v.employee_number
where a.store = 'ry2'
union
select 'z','z', '', '', null::date,  null::date, '', '', 
    null::numeric, null::boolean,sum(b.fte),
    null::numeric, null::boolean,sum(c.fte),
    null::numeric, null::boolean,sum(d.fte),
    null::numeric, null::boolean,sum(e.fte),
    null::numeric, null::boolean,sum(f.fte),
    null::numeric, null::boolean,sum(g.fte),
    null::numeric, null::boolean,sum(h.fte),
    null::numeric, null::boolean,sum(i.fte),
    null::numeric, null::boolean,sum(j.fte),
    null::numeric, null::boolean,sum(k.fte),
    null::numeric, null::boolean,sum(l.fte),
    null::numeric, null::boolean,sum(m.fte),
    null::numeric, null::boolean,sum(n.fte),
    null::numeric, null::boolean,sum(o.fte),
    null::numeric, null::boolean,sum(p.fte),
    null::numeric, null::boolean,sum(q.fte),
    null::numeric, null::boolean,sum(r.fte),
    null::numeric, null::boolean,sum(s.fte),
    null::numeric, null::boolean,sum(t.fte),
    null::numeric, null::boolean,sum(u.fte),
    null::numeric, null::boolean,sum(v.fte)
from jon.looking_back a
left join jon.week_1 b on a.employee_number = b.employee_number
left join jon.week_2 c on a.employee_number = c.employee_number
left join jon.week_3 d on a.employee_number = d.employee_number
left join jon.week_4 e on a.employee_number = e.employee_number
left join jon.week_5 f on a.employee_number = f.employee_number
left join jon.week_6 g on a.employee_number = g.employee_number
left join jon.week_7 h on a.employee_number = h.employee_number
left join jon.week_8 i on a.employee_number = i.employee_number
left join jon.week_9 j on a.employee_number = j.employee_number
left join jon.week_10 k on a.employee_number = k.employee_number
left join jon.week_11 l on a.employee_number = l.employee_number
left join jon.week_12 m on a.employee_number = m.employee_number
left join jon.week_13 n on a.employee_number = n.employee_number
left join jon.week_14 o on a.employee_number = o.employee_number
left join jon.week_15 p on a.employee_number = p.employee_number
left join jon.week_16 q on a.employee_number = q.employee_number
left join jon.week_17 r on a.employee_number = r.employee_number
left join jon.week_18 s on a.employee_number = s.employee_number
left join jon.week_19 t on a.employee_number = t.employee_number
left join jon.week_20 u on a.employee_number = u.employee_number
left join jon.week_21 v on a.employee_number = v.employee_number
where a.store = 'ry2'
order by store, name


these are the fte calculations using the original rules

-- truncate jon.week_1;
-- insert into jon.week_1 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/10/2019','02/16/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('02/10/2019','02/16/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "2/10-2/16" = 0 then 0
--         when "2/10-2/16" >= 30 then 1
--         when "2/10-2/16" < 30 then round("2/10-2/16"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "2/10-2/16" = 0 then 0
--         when "2/10-2/16" >= 30 then 1
--         when "2/10-2/16" < 30 then round("2/10-2/16"/30, 1)
--       end        
--   end as fte, daterange('02/10/2019','02/16/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- truncate jon.week_2;
-- insert into jon.week_2 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/17/2019','02/23/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('02/17/2019','02/23/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "2/17-2/23" = 0 then 0
--         when "2/17-2/23" >= 30 then 1
--         when "2/17-2/23" < 30 then round("2/17-2/23"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "2/17-2/23" = 0 then 0 
--         when "2/17-2/23" >= 30 then 1
--         when "2/17-2/23" < 30 then round("2/17-2/23"/30, 1)
--       end        
--   end as fte, daterange('02/17/2019','02/23/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- truncate jon.week_3;
-- insert into jon.week_3 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('02/24/2019','03/02/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('02/24/2019','03/02/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "2/24-3/2" = 0 then 0
--         when "2/24-3/2" >= 30 then 1
--         when "2/24-3/2" < 30 then round("2/24-3/2"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "2/24-3/2" = 0 then 0
--         when "2/24-3/2" >= 30 then 1
--         when "2/24-3/2" < 30 then round("2/24-3/2"/30, 1)
--       end        
--   end as fte, daterange('02/24/2019','03/02/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_4;
-- insert into jon.week_4 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/03/2019','03/09/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/03/2019','03/09/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/3-3/9" = 0 then 0
--         when "3/3-3/9" >= 30 then 1
--         when "3/3-3/9" < 30 then round("3/3-3/9"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/3-3/9" = 0 then 0
--         when "3/3-3/9" >= 30 then 1
--         when "3/3-3/9" < 30 then round("3/3-3/9"/30, 1)
--       end        
--   end as fte, daterange('03/03/2019','03/09/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_5;
-- insert into jon.week_5 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/10/2019','03/16/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/10/2019','03/16/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/10-3/16" = 0 then 0
--         when "3/10-3/16" >= 30 then 1
--         when "3/10-3/16" < 30 then round("3/10-3/16"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/10-3/16" = 0 then 0
--         when "3/10-3/16" >= 30 then 1
--         when "3/10-3/16" < 30 then round("3/10-3/16"/30, 1)
--       end        
--   end as fte, daterange('03/10/2019','03/16/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_6;
-- insert into jon.week_6 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/17/2019','03/23/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/17/2019','03/23/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/17-3/23" = 0 then 0
--         when "3/17-3/23" >= 30 then 1
--         when "3/17-3/23" < 30 then round("3/17-3/23"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/17-3/23" = 0 then 0
--         when "3/17-3/23" >= 30 then 1
--         when "3/17-3/23" < 30 then round("3/17-3/23"/30, 1)
--       end        
--   end as fte, daterange('03/17/2019','03/23/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_7;
-- insert into jon.week_7 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/24/2019','03/30/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/24/2019','03/30/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/24-3/30" = 0 then 0
--         when "3/24-3/30" >= 30 then 1
--         when "3/24-3/30" < 30 then round("3/24-3/30"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/24-3/30" = 0 then 0
--         when "3/24-3/30" >= 30 then 1
--         when "3/24-3/30" < 30 then round("3/24-3/30"/30, 1)
--       end        
--   end as fte, daterange('03/24/2019','03/30/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- truncate jon.week_8;
-- insert into jon.week_8 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('03/31/2019','04/06/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('03/31/2019','04/06/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "3/31-4/6" = 0 then 0
--         when "3/31-4/6" >= 30 then 1
--         when "3/31-4/6" < 30 then round("3/31-4/6"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "3/31-4/6" = 0 then 0
--         when "3/31-4/6" >= 30 then 1
--         when "3/31-4/6" < 30 then round("3/31-4/6"/30, 1)
--       end        
--   end as fte, daterange('03/31/2019','04/06/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_9;
-- insert into jon.week_9 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('04/07/2019','04/13/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('04/07/2019','04/13/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "4/7-4/13" = 0 then 0
--         when "4/7-4/13" >= 30 then 1
--         when "4/7-4/13" < 30 then round("4/7-4/13"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "4/7-4/13" = 0 then 0
--         when "4/7-4/13" >= 30 then 1
--         when "4/7-4/13" < 30 then round("4/7-4/13"/30, 1)
--       end        
--   end as fte, daterange('04/07/2019','04/13/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_10;
-- insert into jon.week_10 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('04/14/2019','04/20/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('04/14/2019','04/20/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "4/14-4/20" = 0 then 0
--         when "4/14-4/20" >= 30 then 1
--         when "4/14-4/20" < 30 then round("4/14-4/20"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "4/14-4/20" = 0 then 0
--         when "4/14-4/20" >= 30 then 1
--         when "4/14-4/20" < 30 then round("4/14-4/20"/30, 1)
--       end        
--   end as fte, daterange('04/14/2019','04/20/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_11;
-- insert into jon.week_11 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('04/21/2019','04/27/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('04/21/2019','04/27/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "4/21-4/27" = 0 then 0
--         when "4/21-4/27" >= 30 then 1
--         when "4/21-4/27" < 30 then round("4/21-4/27"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "4/21-4/27" = 0 then 0
--         when "4/21-4/27" >= 30 then 1
--         when "4/21-4/27" < 30 then round("4/21-4/27"/30, 1)
--       end        
--   end as fte, daterange('04/21/2019','04/27/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_12;
-- insert into jon.week_12 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('04/28/2019','05/04/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('04/28/2019','05/04/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "4/28-5/4" = 0 then 0
--         when "4/28-5/4" >= 30 then 1
--         when "4/28-5/4" < 30 then round("4/28-5/4"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "4/28-5/4" = 0 then 0
--         when "4/28-5/4" >= 30 then 1
--         when "4/28-5/4" < 30 then round("4/28-5/4"/30, 1)
--       end        
--   end as fte, daterange('04/28/2019','05/04/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_13;
-- insert into jon.week_13 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('05/05/2019','05/11/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('05/05/2019','05/11/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "5/5-5/11" = 0 then 0
--         when "5/5-5/11" >= 30 then 1
--         when "5/5-5/11" < 30 then round("5/5-5/11"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "5/5-5/11" = 0 then 0
--         when "5/5-5/11" >= 30 then 1
--         when "5/5-5/11" < 30 then round("5/5-5/11"/30, 1)
--       end        
--   end as fte, daterange('05/05/2019','05/11/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_14;
-- insert into jon.week_14 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('05/12/2019','05/18/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('05/12/2019','05/18/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "5/12-5/18" = 0 then 0
--         when "5/12-5/18" >= 30 then 1
--         when "5/12-5/18" < 30 then round("5/12-5/18"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "5/12-5/18" = 0 then 0
--         when "5/12-5/18" >= 30 then 1
--         when "5/12-5/18" < 30 then round("5/12-5/18"/30, 1)
--       end        
--   end as fte, daterange('05/12/2019','05/18/2019', '[]')
-- from jon.looking_back a
-- order by store, name;-- 
-- 
-- truncate jon.week_15;
-- insert into jon.week_15 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('05/19/2019','05/25/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('05/19/2019','05/25/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "5/19-5/25" = 0 then 0
--         when "5/19-5/25" >= 30 then 1
--         when "5/19-5/25" < 30 then round("5/19-5/25"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "5/19-5/25" = 0 then 0
--         when "5/19-5/25" >= 30 then 1
--         when "5/19-5/25" < 30 then round("5/19-5/25"/30, 1)
--       end        
--   end as fte, daterange('05/19/2019','05/25/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_16;
-- insert into jon.week_16 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('05/26/2019','06/01/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('05/26/2019','06/01/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "5/26-6/1" = 0 then 0
--         when "5/26-6/1" >= 30 then 1
--         when "5/26-6/1" < 30 then round("5/26-6/1"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "5/26-6/1" = 0 then 0
--         when "5/26-6/1" >= 30 then 1
--         when "5/26-6/1" < 30 then round("5/26-6/1"/30, 1)
--       end        
--   end as fte, daterange('05/26/2019','06/01/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_17;
-- insert into jon.week_17 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/02/2019','06/08/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('06/02/2019','06/08/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "6/2-6/8" = 0 then 0
--         when "6/2-6/8" >= 30 then 1
--         when "6/2-6/8" < 30 then round("6/2-6/8"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "6/2-6/8" = 0 then 0
--         when "6/2-6/8" >= 30 then 1
--         when "6/2-6/8" < 30 then round("6/2-6/8"/30, 1)
--       end        
--   end as fte, daterange('06/02/2019','06/08/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- truncate jon.week_18;
-- insert into jon.week_18 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/09/2019','06/15/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('06/09/2019','06/15/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "6/9-6/15" = 0 then 0
--         when "6/9-6/15" >= 30 then 1
--         when "6/9-6/15" < 30 then round("6/9-6/15"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "6/9-6/15" = 0 then 0
--         when "6/9-6/15" >= 30 then 1
--         when "6/9-6/15" < 30 then round("6/9-6/15"/30, 1)
--       end        
--   end as fte, daterange('06/09/2019','06/15/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_19;
-- insert into jon.week_19 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/16/2019','06/22/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('06/16/2019','06/22/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "6/16-6/22" = 0 then 0
--         when "6/16-6/22" >= 30 then 1
--         when "6/16-6/22" < 30 then round("6/16-6/22"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "6/16-6/22" = 0 then 0
--         when "6/16-6/22" >= 30 then 1
--         when "6/16-6/22" < 30 then round("6/16-6/22"/30, 1)
--       end        
--   end as fte, daterange('06/16/2019','06/22/2019', '[]')
-- from jon.looking_back a
-- order by store, name;

-- truncate jon.week_20;
-- insert into jon.week_20 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/23/2019','06/29/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('06/23/2019','06/29/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "6/23-6/29" = 0 then 0
--         when "6/23-6/29" >= 30 then 1
--         when "6/23-6/29" < 30 then round("6/23-6/29"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "6/23-6/29" = 0 then 0
--         when "6/23-6/29" >= 30 then 1
--         when "6/23-6/29" < 30 then round("6/23-6/29"/30, 1)
--       end        
--   end as fte, daterange('06/23/2019','06/29/2019', '[]')
-- from jon.looking_back a
-- order by store, name;
-- 
-- truncate jon.week_21;
-- insert into jon.week_21 select store, employee_number,
--   case when daterange(hire_date, term_date, '[]') && daterange('06/30/2019','07/06/2019', '[]') then true else false end as employed,
--   case 
--     when not daterange(hire_date, term_date, '[]') && daterange('06/30/2019','07/06/2019', '[]') then 0  -- not employed
--     when payroll_class = 'H' then -- all hourly
--       case 
--         when "6/30-7/6" = 0 then 0
--         when "6/30-7/6" >= 30 then 1
--         when "6/30-7/6" < 30 then round("6/30-7/6"/30, 1)
--       end
--     when payroll_class = 'C' and full_part = 'full' and (  -- commission only full time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1
--     when payroll_class = 'C' and full_part = 'part' and (  -- commission only part time
--         (store = 'RY1' and distrib_code in ('salm','sale','team','own','25','mgr','bsm','26','afte','rao','ucm','used'))
--         or
--         (store = 'ry2' and distrib_code in ('mgr','sale','team','own'))) then 1     
--     when payroll_class = 'S' and full_part = 'full' then 1  -- salaried full time
--     when payroll_class = 'S' and full_part = 'part' then .5 -- salaried part time
--     -- full/part doesn't matter, what matters is the number of hours
--     when payroll_class = 'C' and (  -- commission with clock hours
--         (store = 'RY1' and distrib_code in ('BTEC','CWAS','STEC','WTEC'))
--         or
--         (store = 'ry2' and distrib_code in ('DETA','TECH'))) then 
--       case 
--         when "6/30-7/6" = 0 then 0
--         when "6/30-7/6" >= 30 then 1
--         when "6/30-7/6" < 30 then round("6/30-7/6"/30, 1)
--       end        
--   end as fte, daterange('06/30/2019','07/06/2019', '[]')
-- from jon.looking_back a
-- order by store, name;