# encoding=utf-8
import db_cnx
from openpyxl import Workbook
"""
line 49 in jon.weekly_fte() has to be updated every week
it first needs to be run in a manner to expose dups
"""
# create the 2 new fte tables
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        sql = "select jon.weekly_fte();"
        pg_cur.execute(sql)
        pg_con.commit()
        
        sql = "select * from jon.weekly_fte_spreadsheets();"
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            scenario_1_table_name = row[3]
            scenario_2_table_name = row[4]
        # variables for worksheet names
        ry1_scen_1_ws = scenario_1_table_name[4:] + '_ry1'
        ry1_scen_2_ws = scenario_2_table_name[4:] + '_ry1'
        ry2_scen_1_ws = scenario_1_table_name[4:] + '_ry2'
        ry2_scen_2_ws = scenario_2_table_name[4:] + '_ry2'
        # variables for spreadsheet (workbook) names
        wb_1_file_name = scenario_1_table_name[4:] + '.xlsx'
        wb_2_file_name = scenario_2_table_name[4:] + '.xlsx'

        # scenario 1
        wb_1 = Workbook()
        ws1 = wb_1.active
        ws1.title = ry1_scen_1_ws

        # ry1 scenario 1
        store = 'RY1'
        ws1.append(["store", "emp #", "name", "dist code", "hire date", "term date",
                    "payroll class", "full/part", "clock_hours", "fte"])
        sql = """
            select store, employee_number, name, dist_Code, hire_date, term_date,
                payroll_class, full_part, clock_hours, fte
            from {0} where store = '{1}'  
        """.format(scenario_1_table_name, store)
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            ws1.append(row)
        sql = """
            select null,null,null,null,null,null,null,null,sum(clock_hours),sum(fte)
            from {0} where store = '{1}'
        """.format(scenario_1_table_name, store)
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            ws1.append(row)

        ws2 = wb_1.create_sheet(title=ry2_scen_1_ws)

        # ry2 scenario 1
        store = 'RY2'
        ws2.append(["store", "emp #", "name", "dist code", "hire date", "term date",
                    "payroll class", "full/part", "clock_hours", "fte"])
        sql = """
            select store, employee_number, name, dist_Code, hire_date, term_date,
                payroll_class, full_part, clock_hours, fte
            from {0} where store = '{1}'  
        """.format(scenario_1_table_name, store)
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            ws2.append(row)
        sql = """
            select null,null,null,null,null,null,null,null,sum(clock_hours),sum(fte)
            from {0} where store = '{1}'      
        """.format(scenario_1_table_name, store)
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            ws2.append(row)

        wb_1.save(filename=wb_1_file_name)

        # scenario 2
        wb_2 = Workbook()
        ws1 = wb_2.active
        ws1.title = ry1_scen_2_ws

        # ry1 scenario 2
        store = 'RY1'
        ws1.append(["store", "emp #", "name", "dist code", "hire date", "term date",
                    "payroll class", "full/part", "clock_hours", "fte"])
        sql = """
            select store, employee_number, name, dist_Code, hire_date, term_date,
                payroll_class, full_part, clock_hours, fte
            from {0} where store = '{1}'  
        """.format(scenario_2_table_name, store)
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            ws1.append(row)
        sql = """
            select null,null,null,null,null,null,null,null,sum(clock_hours),sum(fte)
            from {0} where store = '{1}'
        """.format(scenario_2_table_name, store)
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            ws1.append(row)

        ws2 = wb_2.create_sheet(title=ry2_scen_2_ws)

        # ry2 scenario 2
        store = 'RY2'
        ws2.append(["store", "emp #", "name", "dist code", "hire date", "term date",
                    "payroll class", "full/part", "clock_hours", "fte"])
        sql = """
            select store, employee_number, name, dist_Code, hire_date, term_date,
                payroll_class, full_part, clock_hours, fte
            from {0} where store = '{1}'  
        """.format(scenario_2_table_name, store)
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            ws2.append(row)
        sql = """
            select null,null,null,null,null,null,null,null,sum(clock_hours),sum(fte)
            from {0} where store = '{1}'      
        """.format(scenario_2_table_name, store)
        pg_cur.execute(sql)
        for row in pg_cur.fetchall():
            ws2.append(row)

        wb_2.save(filename=wb_2_file_name)
