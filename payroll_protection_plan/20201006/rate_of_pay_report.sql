﻿drop table if exists ppp.rate_of_pay_employees cascade;
create table ppp.rate_of_pay_employees (
  employee_number citext not null primary key,
  store citext,
  employee_name citext,
  ssn citext,re
  payroll_class citext,
  pay_period citext,
  full_part citext,
  hire_date date,
  term_date date);
comment on table ppp.gross_payroll_2019_report_employees is 'all employees employed and or receiving a paycheck at anytime 
  between 4/14/20 and 7/13/20 for GM and 4/15/20 and 7/14/20 for HN';

 
insert into ppp.rate_of_pay_employees  
select aa.employee_, 
  case
    when left(employee_, 1) = '1' then 'RY1'
    else 'RY2'
  end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  array_to_string(aa.full_part, ',') as full_part,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
  select employee_, array_agg(distinct pay_period) as pay_period, array_agg(distinct payroll_class) as payroll_class, array_agg(distinct full_part) as full_part
  from (
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date, 
      case
        when b.fullparttime = 'Full' then 'F'
        when b.fullparttime = 'Part' then 'P'
      end as full_part
    from arkona.ext_pyhshdta a
    left join ads.ext_edw_employee_dim b on a.employee_ = b.employeenumber
      and (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date between b.employeekeyfromdate and b.employeekeythrudate    
    where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date
     ) x group by employee_) aa
left join arkona.ext_pymast bb on aa.employee_ = bb.pymast_employee_number     
left join jon.ssn cc on aa.employee_ = cc.employee_number;    
delete from ppp.rate_of_pay_employees where employee_number = '1112425';  -- delete jim price


-- -- check dates
select row_number() over (), (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date, sum(total_gross_pay), count(*)
from ppp.gross_payroll_2019_report_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/01/2020'::date and '07/14/2020'::date
group by (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date  
having sum(total_gross_pay)  <> 0
order by (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date;  




-- this looks good for rate of pay
select a.employee_number,
  (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date,
  case
    when b.payroll_class = 'S' then b.base_salary
    when b.payroll_class = 'H' then b.base_hrly_rate
    when b.payroll_class = 'C' and c.employee_number is not null then c.flat_rate
    when b.payroll_class = 'C' and d.employeenumber is not null then round(d.techtfrrate, 2)
    when a.employee_number = '1110425' then 20.4
  end as rate_of_pay
from ppp.rate_of_pay_employees a
left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2020'::date and '07/14/2020'::date
left join hs.main_shop_flat_rate_techs c on a.employee_number = c.employee_number
  and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::date between c.from_date and c.thru_date
left join ads.ext_tp_data d on a.employee_number = d.employeenumber  
  and d.thedate = (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::date - 7
order by (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date, employee_number

-- and here are the gross numbers
select a.employee_number, sum(b.total_gross_pay) as gross_q1_2020, sum(c.total_gross_pay) as cp_gross
from ppp.rate_of_pay_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2020'::date and '03/31/2020'::date
join arkona.ext_pyhshdta c on a.employee_number = c.employee_
  and (c.check_month::text ||'-' || c.check_day::text ||'-' || c.check_year::text)::Date between '04/15/2020'::date and '07/10/2020'::date  
group by a.employee_number

-- Q1 gross
select a.employee_number, sum(b.total_gross_pay) as gross_q1_2020
from ppp.rate_of_pay_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2020'::date and '03/31/2020'::date
group by a.employee_number

-- CP gross
select a.employee_number, sum(c.total_gross_pay) as cp_gross
from ppp.rate_of_pay_employees a
join arkona.ext_pyhshdta c on a.employee_number = c.employee_
  and (c.check_month::text ||'-' || c.check_day::text ||'-' || c.check_year::text)::Date between '04/15/2020'::date and '07/10/2020'::date  
group by a.employee_number



-- now just need a query to get the individual check dates as columns

-- generate the select and aliases
select 'max(case when check_date =  '|| '''' || extract(month from check_date) || '/'||extract(day from check_date)||'/'||extract(year from check_date)||''''||'::DATE'
  ||' then rate_of_pay END) AS '||'"'||extract(month from check_date)||'/'|| extract(day from check_date)||'/'||extract(year from check_date)||'"'||','
from (
  select row_number() over (), (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date
  from ppp.gross_payroll_2019_report_employees a
  join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/01/2020'::date and '07/14/2020'::date
  group by (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) aa
order by (extract(month from check_date)||'/'|| extract(day from check_date)||'/'||extract(year from check_date))::date 


drop table if exists ppp.rate_of_pay_report;
create table ppp.rate_of_pay_report as
select aa.*, bb.gross_q1_2020, cc.cp_gross, dd.*
from ppp.rate_of_pay_employees aa
left join ( -- Q1 gross
  select a.employee_number, sum(b.total_gross_pay) as gross_q1_2020
  from ppp.rate_of_pay_employees a
  join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2020'::date and '03/31/2020'::date
  group by a.employee_number) bb on aa.employee_number = bb.employee_number
left join ( -- CP gross
  select a.employee_number, sum(c.total_gross_pay) as cp_gross
  from ppp.rate_of_pay_employees a
  join arkona.ext_pyhshdta c on a.employee_number = c.employee_
    and (c.check_month::text ||'-' || c.check_day::text ||'-' || c.check_year::text)::Date between '04/15/2020'::date and '07/10/2020'::date  
  group by a.employee_number) cc on aa.employee_number = cc.employee_number  
left join (
  select employee_number as emp,
    max(case when check_date =  '1/10/2020'::DATE then rate_of_pay END) AS "1/10/2020",
    max(case when check_date =  '1/15/2020'::DATE then rate_of_pay END) AS "1/15/2020",
    max(case when check_date =  '1/24/2020'::DATE then rate_of_pay END) AS "1/24/2020",
    max(case when check_date =  '1/31/2020'::DATE then rate_of_pay END) AS "1/31/2020",
    max(case when check_date =  '2/7/2020'::DATE then rate_of_pay END) AS "2/7/2020",
    max(case when check_date =  '2/14/2020'::DATE then rate_of_pay END) AS "2/14/2020",
    max(case when check_date =  '2/21/2020'::DATE then rate_of_pay END) AS "2/21/2020",
    max(case when check_date =  '2/29/2020'::DATE then rate_of_pay END) AS "2/29/2020",
    max(case when check_date =  '3/6/2020'::DATE then rate_of_pay END) AS "3/6/2020",
    max(case when check_date =  '3/15/2020'::DATE then rate_of_pay END) AS "3/15/2020",
    max(case when check_date =  '3/20/2020'::DATE then rate_of_pay END) AS "3/20/2020",
    max(case when check_date =  '3/31/2020'::DATE then rate_of_pay END) AS "3/31/2020",
    max(case when check_date =  '4/3/2020'::DATE then rate_of_pay END) AS "4/3/2020",
    max(case when check_date =  '4/6/2020'::DATE then rate_of_pay END) AS "4/6/2020",
    max(case when check_date =  '4/15/2020'::DATE then rate_of_pay END) AS "4/15/2020",
    max(case when check_date =  '4/17/2020'::DATE then rate_of_pay END) AS "4/17/2020",
    max(case when check_date =  '4/30/2020'::DATE then rate_of_pay END) AS "4/30/2020",
    max(case when check_date =  '5/1/2020'::DATE then rate_of_pay END) AS "5/1/2020",
    max(case when check_date =  '5/14/2020'::DATE then rate_of_pay END) AS "5/14/2020",
    max(case when check_date =  '5/15/2020'::DATE then rate_of_pay END) AS "5/15/2020",
    max(case when check_date =  '5/29/2020'::DATE then rate_of_pay END) AS "5/29/2020",
    max(case when check_date =  '5/31/2020'::DATE then rate_of_pay END) AS "5/31/2020",
    max(case when check_date =  '6/12/2020'::DATE then rate_of_pay END) AS "6/12/2020",
    max(case when check_date =  '6/15/2020'::DATE then rate_of_pay END) AS "6/15/2020",
    max(case when check_date =  '6/17/2020'::DATE then rate_of_pay END) AS "6/17/2020",
    max(case when check_date =  '6/19/2020'::DATE then rate_of_pay END) AS "6/19/2020",
    max(case when check_date =  '6/26/2020'::DATE then rate_of_pay END) AS "6/26/2020",
    max(case when check_date =  '6/30/2020'::DATE then rate_of_pay END) AS "6/30/2020",
    max(case when check_date =  '7/2/2020'::DATE then rate_of_pay END) AS "7/2/2020",
    max(case when check_date =  '7/10/2020'::DATE then rate_of_pay END) AS "7/10/2020"
  from (
    select a.employee_number,
      (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date,
      case
        when b.payroll_class = 'S' then b.base_salary
        when b.payroll_class = 'H' then b.base_hrly_rate
        when b.payroll_class = 'C' and c.employee_number is not null then c.flat_rate
        when b.payroll_class = 'C' and d.employeenumber is not null then round(d.techtfrrate, 2)
        when a.employee_number = '1110425' then 20.4
      end as rate_of_pay
    from ppp.rate_of_pay_employees a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2020'::date and '07/14/2020'::date
    left join hs.main_shop_flat_rate_techs c on a.employee_number = c.employee_number
      and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::date between c.from_date and c.thru_date
    left join ads.ext_tp_data d on a.employee_number = d.employeenumber  
      and d.thedate = (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::date - 7
    order by (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date, employee_number) e
  group by employee_number) dd on aa.employee_number = dd.emp
order by aa.store, aa.employee_name;

select * from ppp.rate_of_pay_report;