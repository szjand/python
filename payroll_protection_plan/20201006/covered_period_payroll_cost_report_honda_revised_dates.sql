﻿/*
Jon,  
We are running into an issue for HGF, and need a different period for reports.  
Will you run the covered period report using 4/15/20 to 7/28/20 as the dates?  
Also, for the covered period hours and the lookback period hours, will you please add columns to state Regular, OT, Vac/PTO/Holiday?
11/05/20
Jeri Schmiess Penas, CPA
*/

drop table if exists ppp.covered_period_payroll_cost_employees_honda_revised cascade;
create table ppp.covered_period_payroll_cost_employees_honda_revised (
  employee_number citext not null primary key,
  store citext,
  employee_name citext,
  ssn citext,
  payroll_class citext,
  pay_period citext,
  full_part citext,
  hire_date date,
  term_date date);
comment on table ppp.covered_period_payroll_cost_employees_honda_revised is 'all employees employed and or receiving a paycheck at anytime 
  between 4/15/20 and 7/28/20 for HN ONLY per jeri''s request 11/05/20';

-- paycheck employees  
insert into ppp.covered_period_payroll_cost_employees_honda_revised  
select aa.employee_, 
  case
    when left(employee_, 1) = '1' then 'RY1'
    else 'RY2'
  end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  array_to_string(aa.full_part, ',') as full_part,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
  select employee_, array_agg(distinct pay_period) as pay_period, array_agg(distinct payroll_class) as payroll_class, array_agg(distinct full_part) as full_part
  from (
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date, 
      case
        when b.fullparttime = 'Full' then 'F'
        when b.fullparttime = 'Part' then 'P'
      end as full_part
    from arkona.ext_pyhshdta a
    left join ads.ext_edw_employee_dim b on a.employee_ = b.employeenumber
      and (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date between b.employeekeyfromdate and b.employeekeythrudate    
    where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/28/2020'::date
      and a.company_number = 'RY2'
     ) x group by employee_) aa
left join arkona.ext_pymast bb on aa.employee_ = bb.pymast_employee_number     
left join jon.ssn cc on aa.employee_ = cc.employee_number order by employee_name;   
-- not reqd for honda 
-- delete from ppp.covered_period_payroll_cost_employees where employee_number = '1112425';  -- delete jim price

--select * from ppp.covered_period_payroll_cost_employees_honda_revised  order by employee_name

-- added an additional 22 employees in script oops_left_out_employed_but_not_check.sql 
-- not doing that for this honda only revision, instead, will include here all relevant processing from that query
-- so, here are those employed but no check for the new date range
-- the new dates now include 3 employees hired on 07/27/20: OSOWSKI, DENISE,DURAND, TIMOTHY,BEYER, LESTAT
--   exclude brittany rose, looks like she xfrd from gm to honda on 10/1 ish
-- hintz and brunk transferred to honda after 07/28/20, so they are no longer included
insert into ppp.covered_period_payroll_cost_employees_honda_revised  
select aa.employeenumber, 
  'RY2' as store,
--   case
--     when left(employeenumber, 1) = '1' then 'RY1'
--     else 'RY2'
--   end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  case
    when array_to_string(aa.full_part, ',') = 'Full' then 'F'
    when array_to_string(aa.full_part, ',') = 'Part' then 'P'
    else array_to_string(aa.full_part, ',') 
  end as full_part,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
  select aa.employeenumber, array_agg(distinct payperiodcode) as pay_period, array_agg(distinct payrollclasscode) as payroll_class, array_agg(distinct aa.fullparttime) as full_part
  from ads.ext_edw_employee_dim aa
  join (
    select distinct  a.pymast_employee_number -- 26
    from arkona.ext_pymast a
    where dds.db2_integer_to_date(hire_date) <= '07/28/2020'
      and dds.db2_integer_to_date(termination_date) >= '04/15/2020'
      and not exists (
        select 1
        from ppp.fte_reduction_employees
        where employee_number = a.pymast_employee_number)) bb on aa.employeenumber = bb.pymast_employee_number
  where aa.storecode = 'RY2'
    and ((aa.employeekeyfromdate <= '07/28/2020' and aa.employeekeythrudate >= '04/15/2020'))
    or (aa.employeenumber = '263095' and employeekeythrudate = '12/31/9999')
  group by aa.employeenumber) aa
left join arkona.ext_pymast bb on aa.employeenumber = bb.pymast_employee_number     
left join jon.ssn cc on aa.employeenumber = cc.employee_number
where not exists (
  select 1
  from ppp.covered_period_payroll_cost_employees_honda_revised 
  where employee_number = aa.employeenumber)
and employeenumber <> '259875';  


-- sent this to jeri as covered_period_payroll_cost_report_honda_date_revision_extract_v1.xlsx
drop table if exists ppp.covered_period_payroll_cost_report_honda_date_revision_v1;
create table ppp.covered_period_payroll_cost_report_honda_date_revision_v1 as
select aa.*, bb.cp_gross, "Regular", "OT", "Vac/PTO/Holiday"
from ppp.covered_period_payroll_cost_employees_honda_revised aa
left join (
  select a.employee_number, sum(b.total_gross_pay) as cp_gross,
--     round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as cp_hours
    sum(b.reg_hours) as "Regular", sum(overtime_hours) as "OT",
    round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken), 2) as "Vac/PTO/Holiday"
  from ppp.covered_period_payroll_cost_employees a
  left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/28/2020'::date
  group by a.employee_number) bb on aa.employee_number = bb.employee_number
order by store, employee_name;