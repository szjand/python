﻿/*
10/13/20
V2
I messed up….on the FTE lookback report spec, 
I had said that I wanted the lookback period hours for only those employees 
employed during the covered period, and that is not right.  
I actually want all employees that were employed during the lookback period.  
I’m sorry—I’m struggling to keep all of the necessary info straight.  

Jeri Schmiess Penas
*/

/*
11/05/20
v3
Also, for the covered period hours and the lookback period hours, will you please add columns to state Regular, OT, Vac/PTO/Holiday?
just going to regenerate the spreadsheet from the existing table, but break out the clockhours, i think
*/
drop table if exists ppp.fte_reduction_employees cascade;
create table ppp.fte_reduction_employees (
  employee_number citext not null primary key,
  store citext,
  employee_name citext,
  ssn citext,
  payroll_class citext,
  pay_period citext,
  full_part citext,
  hire_date date,
  term_date date);
comment on table ppp.fte_reduction_employees is 'all employees employed and or receiving a paycheck at anytime 
  between 1/1/20 and 2/29/20 ';

-- drop table if exists employee_base;
-- create temp table employee_base as
-- select distinct employee_
-- from arkona.ext_pyhshdta a
-- left join ads.ext_edw_employee_dim b on a.employee_ = b.employeenumber
--   and (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date between b.employeekeyfromdate and b.employeekeythrudate    
-- where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/01/2020'::date and '02/29/2020'::date
-- union
-- select distinct  pymast_employee_number 
-- from arkona.ext_pymast 
-- where dds.db2_integer_to_date(hire_date) <= '02/29/2020'
--   and dds.db2_integer_to_date(termination_date) >= '01/01/2020';

-- the paycheck employees 
insert into ppp.fte_reduction_employees  
select aa.employee_, 
  case
    when left(employee_, 1) = '1' then 'RY1'
    else 'RY2'
  end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  array_to_string(aa.full_part, ',') as full_part,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
  select employee_, array_agg(distinct pay_period) as pay_period, array_agg(distinct payroll_class) as payroll_class, array_agg(distinct full_part) as full_part
  from (
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date, 
      case
        when b.fullparttime = 'Full' then 'F'
        when b.fullparttime = 'Part' then 'P'
      end as full_part
    from arkona.ext_pyhshdta a
    left join ads.ext_edw_employee_dim b on a.employee_ = b.employeenumber
      and (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date between b.employeekeyfromdate and b.employeekeythrudate    
    where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/01/2020'::date and '02/29/2020'::date ) x 
  group by employee_) aa
left join arkona.ext_pymast bb on aa.employee_ = bb.pymast_employee_number     
left join jon.ssn cc on aa.employee_ = cc.employee_number;    
delete from ppp.fte_reduction_employees where employee_number = '1112425';  -- delete jim price





-- employed but no check
insert into ppp.fte_reduction_employees  
select aa.employeenumber, 
  case
    when left(employeenumber, 1) = '1' then 'RY1'
    else 'RY2'
  end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  array_to_string(aa.full_part, ',') as full_part,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
select aa.employeenumber, array_agg(distinct payperiodcode) as pay_period, array_agg(distinct payrollclasscode) as payroll_class, array_agg(distinct aa.fullparttime) as full_part
from ads.ext_edw_employee_dim aa
join (
  select distinct  a.pymast_employee_number -- 26
  from arkona.ext_pymast a
  where dds.db2_integer_to_date(hire_date) <= '02/29/2020'
    and dds.db2_integer_to_date(termination_date) >= '01/01/2020'
    and not exists (
      select 1
      from ppp.fte_reduction_employees
      where employee_number = a.pymast_employee_number)) bb on aa.employeenumber = bb.pymast_employee_number
where ((aa.employeekeyfromdate <= '02/29/2020'
  and aa.employeekeythrudate >= '01/01/2020')
  or aa.employeenumber in ('258763'))
  or (aa.employeenumber = '263095' and employeekeythrudate = '12/31/9999')
  and aa.employeenumber not in ('115883')
group by aa.employeenumber) aa
left join arkona.ext_pymast bb on aa.employeenumber = bb.pymast_employee_number     
left join jon.ssn cc on aa.employeenumber = cc.employee_number;  

create unique index on ppp.fte_reduction_employees(employee_number);

select * from ppp.fte_reduction_employees aa


--------------------------------------------------------------------------------------------------

drop table if exists ppp.fte_reduction_report cascade;
create table ppp.fte_reduction_report as
select aa.*, bb.lb_hours
from ppp.fte_reduction_employees aa
left join (
  select a.employee_number, round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as lb_hours
  from ppp.fte_reduction_employees a
  left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date  
  group by a.employee_number) bb on aa.employee_number = bb.employee_number
order by store, employee_name;
create unique index on ppp.fte_reduction_report(employee_number);

select * from ppp.fte_reduction_report;

create unique index on ppp.fte_reduction_report(employee_number);


-- 10/28/2020, discrepancy picked up by jeri
-- some how aaron robinson, 122332, changed from 341.51 (the value in table ppp.fte_reduction_report) to 325.51 (the value this query now produces)
-- 
select * from arkona.ext_pymast where employee_last_name = 'robinson' and employee_first_name = 'aaron'

  select a.employee_number, (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date,
    round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as lb_hours
  from ppp.fte_reduction_employees a
  join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date  
    and b.employee_ = '122332'
  group by a.employee_number

-- 11/05/20 V3,  break out the clock hour
-- just use the above query, no need to generate a new ppp.fte_reduction_report table
-- Also, for the covered period hours and the lookback period hours, will you please add columns to state Regular, OT, Vac/PTO/Holiday?
-- i am assuming just for honda
-- sent to jeri as fte_reductions_report_honda_only_with_hours_breakout_v1
select aa.*, bb.reg, bb.ot, bb.pto, bb.reg+bb.ot+bb.pto as total
from ppp.fte_reduction_employees aa
left join (
  select a.employee_number, round(sum(b.reg_hours), 2) as reg, round(sum(b.overtime_hours), 2) as ot,
  round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken), 2) as pto
  from ppp.fte_reduction_employees a
  left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date  
  group by a.employee_number) bb on aa.employee_number = bb.employee_number
where aa.store = 'RY2'  
order by store, employee_name; 

-- from jeri
-- Will you take a look at Pat Barta?  For some reason, it looks like he is short his 16 hours for holiday in the lookback period.  
-- exists in dt ui for the check on the jan 10th, but does not exist in db2 (assuming the correct field is holiday_taken)
-- in db2 looks like lots of other folks got 16 hours in that field on then 1/10 check
-- told jeri to add it back in
  select a.employee_number, (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date,
    b.vacation_taken,b.holiday_taken,b.sick_leave_taken,b.reg_hours,b.overtime_hours,b.alt_pay_hours
  from ppp.fte_reduction_employees a
  left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date  
where a.employee_number = '210123'    
order by (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date  



11/11/2020
same shit, looks like the underliying data has changed
what has changed is the disappearance of some holiday_hours from pyhshdta on 1/10/20