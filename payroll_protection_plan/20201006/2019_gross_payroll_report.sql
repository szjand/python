﻿drop table if exists ppp.gross_payroll_2019_report_employees cascade;
create table ppp.gross_payroll_2019_report_employees (
  employee_number citext not null primary key,
  store citext,
  employee_name citext,
  ssn citext,
  payroll_class citext,
  pay_period citext,
  full_part citext,
  hire_date date,
  term_date date);
comment on table ppp.gross_payroll_2019_report_employees is 'all employees employed and or receiving a paycheck at anytime 
  between 4/14/20 and 7/13/20 for GM and 4/15/20 and 7/14/20 for HN';

 
insert into ppp.gross_payroll_2019_report_employees  
select aa.employee_, 
  case
    when left(employee_, 1) = '1' then 'RY1'
    else 'RY2'
  end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  array_to_string(aa.full_part, ',') as full_part,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
  select employee_, array_agg(distinct pay_period) as pay_period, array_agg(distinct payroll_class) as payroll_class, array_agg(distinct full_part) as full_part
  from (
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date, 
      case
        when b.fullparttime = 'Full' then 'F'
        when b.fullparttime = 'Part' then 'P'
      end as full_part
    from arkona.ext_pyhshdta a
    left join ads.ext_edw_employee_dim b on a.employee_ = b.employeenumber
      and (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date between b.employeekeyfromdate and b.employeekeythrudate    
    where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date
     ) x group by employee_) aa
left join arkona.ext_pymast bb on aa.employee_ = bb.pymast_employee_number     
left join jon.ssn cc on aa.employee_ = cc.employee_number;    
delete from ppp.gross_payroll_2019_report_employees where employee_number = '1112425';  -- delete jim price


-- -- check dates
select row_number() over (), (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date, sum(total_gross_pay), count(*)
from ppp.gross_payroll_2019_report_employees a
join arkona.ext_pyhshdta b on a.employee_number = b.employee_
  and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/01/2019'::date and '12/31/2019'::date
group by (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date  
order by (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date;  


-- thinking i don't need to do a sum to get the gross for a check date,
-- i have already grouped by employee_number and check_date in sub query d
-- this works for biweekly
-- inner join on pyhshdta to limit to B only  331 rows of the total 449 employees, 
-- **************** remember 67 of the employees have no checks ********************************
  select employee_number, max(pay_period) as pay_period,
    max(case when check_date = '01/11/2019' then gross end) As "01/11/2019" ,
    max(case when check_date = '01/11/2019' and pay_period = 'B' and gross > 3846.15 then '*' end),
    max(case when check_date = '07/12/2019' then gross end) As "07/12/2019" ,
    max(case when check_date = '07/12/2019' and pay_period = 'B' and gross > 3846.15 then '*' end)    
  from (  -- grouped by emp & check_date for sum of total gross  
    select employee_number, sum(total_gross_pay) as gross, check_date, max(pay_period) as pay_period
    from ( -- individual checks where pay period = B there are some mult check per emp per check_date
      select a.employee_number, b.total_gross_pay, b.pay_period,
        (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
      from ppp.gross_payroll_2019_report_employees a
      join arkona.ext_pyhshdta b on a.employee_number = b.employee_
        and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date
        and b.pay_period = 'B') c
    group by employee_number, check_date) d
--   where employee_number in ('13725','16425')
  group by employee_number

-- looks good for monthly
select aa.*, case when bb.gross >= 8333.33 then '*' end
from (  -- grouped by emp & check_date for sum of total gross  
  select employee_number, check_date, sum(total_gross_pay) as gross
  from (-- individual checks where pay period = S, there are some mult check per emp per check_date
    select a.employee_number, b.total_gross_pay, 
      (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
    from ppp.gross_payroll_2019_report_employees a
    join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date
    where b.pay_period = 'S') c
  group by employee_number, check_date ) aa
left join (  -- for salaried i need to total by month to get total monthly pay
  select a.employee_number, b.check_month, sum(b.total_gross_pay) as gross, 
    max((b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date) as max_check_date
  from ppp.gross_payroll_2019_report_employees a
  join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date
  where b.pay_period = 'S'
  group by a.employee_number, b.check_month) bb  on aa.employee_number = bb.employee_number
    and aa.check_date = bb.max_check_date
order by aa.employee_number, aa.check_date    


-- checked and fixed overlapping B and S, 4 employees, see below

now i can union them
but first i need to generate the select clause
it needss to llok like
max(case when check_date = '01/11/2019' then gross end) as "01/11/2019", ann_100k,

-- generate the select and aliases
select 'max(case when check_date =  '|| '''' || extract(month from check_date) || '/'||extract(day from check_date)||'/'||extract(year from check_date)||''''||'::DATE'
  ||' then gross END) AS '||'"'||extract(month from check_date)||'/'|| extract(day from check_date)||'/'||extract(year from check_date)||'"'||','
from (
  select row_number() over (), (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date
  from ppp.gross_payroll_2019_report_employees a
  join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/01/2019'::date and '12/31/2019'::date
  group by (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) aa
order by (extract(month from check_date)||'/'|| extract(day from check_date)||'/'||extract(year from check_date))::date 


drop table if exists ppp.gross_payroll_2019_report cascade;
create table ppp.gross_payroll_2019_report as
select *
from ppp.gross_payroll_2019_report_employees x
left join (
  select employee_number as emp, 
    max(case when check_date =  '1/11/2019'::DATE then gross END) AS "1/11/2019",
    max(case when check_date =  '1/15/2019'::DATE then gross END) AS "1/15/2019",
    max(case when check_date =  '1/25/2019'::DATE then gross END) AS "1/25/2019",
    max(case when check_date =  '1/31/2019'::DATE then gross END) AS "1/31/2019",
    max(case when check_date =  '2/8/2019'::DATE then gross END) AS "2/8/2019",
    max(case when check_date =  '2/15/2019'::DATE then gross END) AS "2/15/2019",
    max(case when check_date =  '2/22/2019'::DATE then gross END) AS "2/22/2019",
    max(case when check_date =  '2/28/2019'::DATE then gross END) AS "2/28/2019",
    max(case when check_date =  '3/8/2019'::DATE then gross END) AS "3/8/2019",
    max(case when check_date =  '3/15/2019'::DATE then gross END) AS "3/15/2019",
    max(case when check_date =  '3/22/2019'::DATE then gross END) AS "3/22/2019",
    max(case when check_date =  '3/31/2019'::DATE then gross END) AS "3/31/2019",
    max(case when check_date =  '4/5/2019'::DATE then gross END) AS "4/5/2019",
    max(case when check_date =  '4/15/2019'::DATE then gross END) AS "4/15/2019",
    max(case when check_date =  '4/19/2019'::DATE then gross END) AS "4/19/2019",
    max(case when check_date =  '4/30/2019'::DATE then gross END) AS "4/30/2019",
    max(case when check_date =  '5/3/2019'::DATE then gross END) AS "5/3/2019",
    max(case when check_date =  '5/15/2019'::DATE then gross END) AS "5/15/2019",
    max(case when check_date =  '5/17/2019'::DATE then gross END) AS "5/17/2019",
    max(case when check_date =  '5/31/2019'::DATE then gross END) AS "5/31/2019",
    max(case when check_date =  '6/14/2019'::DATE then gross END) AS "6/14/2019",
    max(case when check_date =  '6/28/2019'::DATE then gross END) AS "6/28/2019",
    max(case when check_date =  '6/30/2019'::DATE then gross END) AS "6/30/2019",
    max(case when check_date =  '7/12/2019'::DATE then gross END) AS "7/12/2019",
    max(case when check_date =  '7/15/2019'::DATE then gross END) AS "7/15/2019",
    max(case when check_date =  '7/26/2019'::DATE then gross END) AS "7/26/2019",
    max(case when check_date =  '7/31/2019'::DATE then gross END) AS "7/31/2019",
    max(case when check_date =  '8/9/2019'::DATE then gross END) AS "8/9/2019",
    max(case when check_date =  '8/15/2019'::DATE then gross END) AS "8/15/2019",
    max(case when check_date =  '8/23/2019'::DATE then gross END) AS "8/23/2019",
    max(case when check_date =  '8/30/2019'::DATE then gross END) AS "8/30/2019",
    max(case when check_date =  '8/31/2019'::DATE then gross END) AS "8/31/2019",
    max(case when check_date =  '9/6/2019'::DATE then gross END) AS "9/6/2019",
    max(case when check_date =  '9/10/2019'::DATE then gross END) AS "9/10/2019",
    max(case when check_date =  '9/16/2019'::DATE then gross END) AS "9/16/2019",
    max(case when check_date =  '9/20/2019'::DATE then gross END) AS "9/20/2019",
    max(case when check_date =  '9/30/2019'::DATE then gross END) AS "9/30/2019",
    max(case when check_date =  '10/4/2019'::DATE then gross END) AS "10/4/2019",
    max(case when check_date =  '10/15/2019'::DATE then gross END) AS "10/15/2019",
    max(case when check_date =  '10/18/2019'::DATE then gross END) AS "10/18/2019",
    max(case when check_date =  '10/22/2019'::DATE then gross END) AS "10/22/2019",
    max(case when check_date =  '10/31/2019'::DATE then gross END) AS "10/31/2019",
    max(case when check_date =  '11/1/2019'::DATE then gross END) AS "11/1/2019",
    max(case when check_date =  '11/15/2019'::DATE then gross END) AS "11/15/2019",
    max(case when check_date =  '11/29/2019'::DATE then gross END) AS "11/29/2019",
    max(case when check_date =  '11/30/2019'::DATE then gross END) AS "11/30/2019",
    max(case when check_date =  '12/13/2019'::DATE then gross END) AS "12/13/2019",
    max(case when check_date =  '12/16/2019'::DATE then gross END) AS "12/16/2019",
    max(case when check_date =  '12/17/2019'::DATE then gross END) AS "12/17/2019",
    max(case when check_date =  '12/18/2019'::DATE then gross END) AS "12/18/2019",
    max(case when check_date =  '12/19/2019'::DATE then gross END) AS "12/19/2019",
    max(case when check_date =  '12/21/2019'::DATE then gross END) AS "12/21/2019",
    max(case when check_date =  '12/24/2019'::DATE then gross END) AS "12/24/2019",
    max(case when check_date =  '12/27/2019'::DATE then gross END) AS "12/27/2019",
    max(case when check_date =  '12/31/2019'::DATE then gross END) AS "12/31/2019",
    sum(replace(gross,'*','')::NUMERIC) as "2010 Total"
  from (
    select employee_number, check_date, sum(total_gross_pay)::text || coalesce(case when sum(total_gross_pay) > 3846.15 then '*' end, '') as gross 
    from ( -- individual checks where pay period = B there are some mult check per emp per check_date
      select a.employee_number, b.total_gross_pay, b.pay_period,
        (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
      from ppp.gross_payroll_2019_report_employees a
      join arkona.ext_pyhshdta b on a.employee_number = b.employee_
        and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date
        and b.pay_period = 'B'
      and a.employee_number not in ('1151450','167834')) c
    group by employee_number, check_date
    union
    select aa.employee_number, aa.check_date,  aa.gross::text || coalesce(case when bb.gross >= 8333.33 then '*' end, '')
    from (  -- grouped by emp & check_date for sum of total gross  
      select employee_number, check_date, sum(total_gross_pay) as gross
      from (-- individual checks where pay period = S, there are some mult check per emp per check_date
        select a.employee_number, b.total_gross_pay, 
          (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
        from ppp.gross_payroll_2019_report_employees a
        join arkona.ext_pyhshdta b on a.employee_number = b.employee_
          and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date
        where b.pay_period = 'S'
        and a.employee_number not in ('175642','1566882')) c
      group by employee_number, check_date ) aa
    left join (  -- for salaried i need to total by month to get total monthly pay
      select a.employee_number, b.check_month, sum(b.total_gross_pay) as gross, 
        max((b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date) as max_check_date
      from ppp.gross_payroll_2019_report_employees a
      join arkona.ext_pyhshdta b on a.employee_number = b.employee_
        and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date
      where b.pay_period = 'S'
      group by a.employee_number, b.check_month) bb  on aa.employee_number = bb.employee_number
        and aa.check_date = bb.max_check_date) cc
  group by employee_number) y on x.employee_number = y.emp
order by x.employee_number::integer;

create unique index on ppp.gross_payroll_2019_report(employee_number);

-- added an additional 22 employees in script oops_left_out_employed_but_not_check.sql, they exist in the table ppp.first_cut;

select * from ppp.gross_payroll_2019_report order by store, employee_name













-- -- ok good all appears to work
-- -- now do a union on B data and S data to form the base data set
-- -- then generate the select
-- -- first lets check for any overlapping emp#
-- -- 4 employees overlap
-- -- so, exclude 175642 & 1566882 from monthly, 1151450 & 167834 from biweekly
-- -- that fixes that
-- select * 
-- from ppp.gross_payroll_2019_report_employees
-- where employee_number in (
-- select distinct aa.employee_number
-- from ( -- biweekly
--   select employee_number, sum(total_gross_pay) as gross, check_date, 
--     case when sum(total_gross_pay) > 3846.15 then '*' end
--   from ( -- individual checks where pay period = B there are some mult check per emp per check_date
--     select a.employee_number, b.total_gross_pay, b.pay_period,
--       (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
--     from ppp.gross_payroll_2019_report_employees a
--     join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--       and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date
--       and b.pay_period = 'B'
--     and a.employee_number not in ('1151450','167834')) c
--   group by employee_number, check_date) aa
-- join ( -- monthly
--   select aa.*, case when bb.gross >= 8333.33 then '*' end
--   from (  -- grouped by emp & check_date for sum of total gross  
--     select employee_number, check_date, sum(total_gross_pay) as gross
--     from (-- individual checks where pay period = S, there are some mult check per emp per check_date
--       select a.employee_number, b.total_gross_pay, 
--         (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
--       from ppp.gross_payroll_2019_report_employees a
--       join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--         and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date
--       where b.pay_period = 'S'
--       and a.employee_number not in ('175642','1566882')) c
--     group by employee_number, check_date ) aa
--   left join (  -- for salaried i need to total by month to get total monthly pay
--     select a.employee_number, b.check_month, sum(b.total_gross_pay) as gross, 
--       max((b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date) as max_check_date
--     from ppp.gross_payroll_2019_report_employees a
--     join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--       and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date
--     where b.pay_period = 'S'
--     group by a.employee_number, b.check_month) bb  on aa.employee_number = bb.employee_number
--       and aa.check_date = bb.max_check_date
--   order by aa.employee_number, aa.check_date) bb on aa.employee_number = bb.employee_number)



-- -- this query based on what i did in ...ppp/from_greg_requests/ppp_payroll_details.sql
-- -- turned out to not use this
-- 
-- -- generate the select and aliases
-- select 'sum(gross) filter (where check_date =  '|| '''' || extract(month from check_date) || '/'||extract(day from check_date)||'/'||extract(year from check_date)||''''||'::DATE'
--   ||') AS '||'"'||extract(month from check_date)||'/'|| extract(day from check_date)||'/'||extract(year from check_date)||'"'||','
-- from (
--   select row_number() over (), (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date
--   from ppp.gross_payroll_2019_report_employees a
--   join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--     and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/01/2019'::date and '12/31/2019'::date
--   group by (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date) aa;  
-- 
--   
-- select *
-- from ppp.gross_payroll_2019_report_employees aa
-- join (-- this query based on what i did in ...ppp/from_greg_requests/ppp_payroll_details.sql
--   select employee_number, 
--     sum(gross) filter (where check_date =  '1/11/2019'::DATE) AS "1/11/2019",' ',
--     sum(gross) filter (where check_date =  '1/15/2019'::DATE) AS "1/15/2019",' ',
--     sum(gross) filter (where check_date =  '1/25/2019'::DATE) AS "1/25/2019",
--     sum(gross) filter (where check_date =  '1/31/2019'::DATE) AS "1/31/2019",
--     sum(gross) filter (where check_date =  '2/8/2019'::DATE) AS "2/8/2019",
--     sum(gross) filter (where check_date =  '2/15/2019'::DATE) AS "2/15/2019",
--     sum(gross) filter (where check_date =  '2/22/2019'::DATE) AS "2/22/2019",
--     sum(gross) filter (where check_date =  '2/28/2019'::DATE) AS "2/28/2019",
--     sum(gross) filter (where check_date =  '3/8/2019'::DATE) AS "3/8/2019",
--     sum(gross) filter (where check_date =  '3/15/2019'::DATE) AS "3/15/2019",
--     sum(gross) filter (where check_date =  '3/22/2019'::DATE) AS "3/22/2019",
--     sum(gross) filter (where check_date =  '3/31/2019'::DATE) AS "3/31/2019",
--     sum(gross) filter (where check_date =  '4/5/2019'::DATE) AS "4/5/2019",
--     sum(gross) filter (where check_date =  '4/15/2019'::DATE) AS "4/15/2019",
--     sum(gross) filter (where check_date =  '4/19/2019'::DATE) AS "4/19/2019",
--     sum(gross) filter (where check_date =  '4/30/2019'::DATE) AS "4/30/2019",
--     sum(gross) filter (where check_date =  '5/3/2019'::DATE) AS "5/3/2019",
--     sum(gross) filter (where check_date =  '5/15/2019'::DATE) AS "5/15/2019",
--     sum(gross) filter (where check_date =  '5/17/2019'::DATE) AS "5/17/2019",
--     sum(gross) filter (where check_date =  '5/31/2019'::DATE) AS "5/31/2019",
--     sum(gross) filter (where check_date =  '6/14/2019'::DATE) AS "6/14/2019",
--     sum(gross) filter (where check_date =  '6/28/2019'::DATE) AS "6/28/2019",
--     sum(gross) filter (where check_date =  '6/30/2019'::DATE) AS "6/30/2019",
--     sum(gross) filter (where check_date =  '7/12/2019'::DATE) AS "7/12/2019",
--     sum(gross) filter (where check_date =  '7/15/2019'::DATE) AS "7/15/2019",
--     sum(gross) filter (where check_date =  '7/26/2019'::DATE) AS "7/26/2019",
--     sum(gross) filter (where check_date =  '7/31/2019'::DATE) AS "7/31/2019",
--     sum(gross) filter (where check_date =  '8/9/2019'::DATE) AS "8/9/2019",
--     sum(gross) filter (where check_date =  '8/15/2019'::DATE) AS "8/15/2019",
--     sum(gross) filter (where check_date =  '8/23/2019'::DATE) AS "8/23/2019",
--     sum(gross) filter (where check_date =  '8/30/2019'::DATE) AS "8/30/2019",
--     sum(gross) filter (where check_date =  '8/31/2019'::DATE) AS "8/31/2019",
--     sum(gross) filter (where check_date =  '9/6/2019'::DATE) AS "9/6/2019",
--     sum(gross) filter (where check_date =  '9/10/2019'::DATE) AS "9/10/2019",
--     sum(gross) filter (where check_date =  '9/16/2019'::DATE) AS "9/16/2019",
--     sum(gross) filter (where check_date =  '9/20/2019'::DATE) AS "9/20/2019",
--     sum(gross) filter (where check_date =  '9/30/2019'::DATE) AS "9/30/2019",
--     sum(gross) filter (where check_date =  '10/4/2019'::DATE) AS "10/4/2019",
--     sum(gross) filter (where check_date =  '10/15/2019'::DATE) AS "10/15/2019",
--     sum(gross) filter (where check_date =  '10/18/2019'::DATE) AS "10/18/2019",
--     sum(gross) filter (where check_date =  '10/22/2019'::DATE) AS "10/22/2019",
--     sum(gross) filter (where check_date =  '10/31/2019'::DATE) AS "10/31/2019",
--     sum(gross) filter (where check_date =  '11/1/2019'::DATE) AS "11/1/2019",
--     sum(gross) filter (where check_date =  '11/15/2019'::DATE) AS "11/15/2019",
--     sum(gross) filter (where check_date =  '11/29/2019'::DATE) AS "11/29/2019",
--     sum(gross) filter (where check_date =  '11/30/2019'::DATE) AS "11/30/2019",
--     sum(gross) filter (where check_date =  '12/13/2019'::DATE) AS "12/13/2019",
--     sum(gross) filter (where check_date =  '12/16/2019'::DATE) AS "12/16/2019",
--     sum(gross) filter (where check_date =  '12/17/2019'::DATE) AS "12/17/2019",
--     sum(gross) filter (where check_date =  '12/18/2019'::DATE) AS "12/18/2019",
--     sum(gross) filter (where check_date =  '12/19/2019'::DATE) AS "12/19/2019",
--     sum(gross) filter (where check_date =  '12/21/2019'::DATE) AS "12/21/2019",
--     sum(gross) filter (where check_date =  '12/24/2019'::DATE) AS "12/24/2019",
--     sum(gross) filter (where check_date =  '12/27/2019'::DATE) AS "12/27/2019",
--     sum(gross) filter (where check_date =  '12/31/2019'::DATE) AS "12/31/2019",
--     sum(gross) as "2019 Total"
--   from (
--     select employee_number, sum(total_gross_pay) as gross, check_date
--     from ( -- 8152 emp# gross & date
--       select a.employee_number, b.total_gross_pay, 
--         (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date as check_date
--       from ppp.gross_payroll_2019_report_employees a
--       left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
--         and (b.check_month::text ||'-' || b.check_day::text ||'-' || b.check_year::text)::Date between '01/01/2019'::date and '12/31/2019'::date) c
--     group by employee_number, check_date) d
--   group by employee_number) bb on aa.employee_number = bb.employee_number  
-- order by aa.store, aa.employee_name;
