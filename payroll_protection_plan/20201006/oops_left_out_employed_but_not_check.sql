﻿select employee_, min((('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date), 
  max((('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date)
from arkona.ext_pyhshdta a
where employee_ in (  
select aa.pymast_employee_number
from (
select pymast_employee_number, employee_name, 
  dds.db2_integer_to_date(hire_date) as hire_date, 
  case
    when termination_date = 0 then null
    else dds.db2_integer_to_date(termination_date)
  end as term_date
from arkona.ext_pymast 
where dds.db2_integer_to_date(hire_date) <= '07/14/2020'
  and dds.db2_integer_to_date(termination_date) >= '04/14/2020') aa
full outer join ppp.gross_payroll_2019_report_employees bb on aa.pymast_employee_number = bb.employee_number  
where bb.employee_number  is null )
group by a.employee_

select * from arkona.xfm_pymast where pymast_employee_number = '1100710'


select employee_, (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date, 
  (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date
from arkona.ext_pyhshdta a
where employee_ = '165873'
order by (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date


-- these are the employees employed betwen the covered period but did not receive a check in that period
-- 22, 20/2
select aa.pymast_employee_number
from (
select pymast_employee_number, employee_name, 
  dds.db2_integer_to_date(hire_date) as hire_date, 
  case
    when termination_date = 0 then null
    else dds.db2_integer_to_date(termination_date)
  end as term_date
from arkona.ext_pymast 
where dds.db2_integer_to_date(hire_date) <= '07/14/2020'
  and dds.db2_integer_to_date(termination_date) >= '04/14/2020') aa
full outer join ppp.gross_payroll_2019_report_employees bb on aa.pymast_employee_number = bb.employee_number  
where bb.employee_number  is null

-- unfortunately, these need to be added to the existing spreadsheets to comply with jeri's request
-- so
-- first ppp.gross_payroll_2019_report_employees  

drop table if exists first_cut cascade;
create temp table first_cut as
select pymast_employee_number, pymast_company_number, employee_name, 
  array_to_string(array_agg(distinct pay_period),',') as pay_period, 
  array_to_string(array_agg(distinct payroll_class),',') as payroll_class,
--   case
--     when pymast_employee_number in ('159823','1135710','164202') then 'P'
--     when pymast_employee_number in ('165873') then 'F'
--     when active_code = 'A' then 'F'
--     when active_code = 'P' then 'P'
--   end as full_part,
  array_to_string(array_agg( distinct
    case
      when active_code = 'A' then 'F'
      when active_code = 'P' then 'P'
    end),',') as full_part,
  array_to_string(array_agg( distinct dds.db2_integer_to_date(hire_date)),',') as hire_date,  
  array_to_string(array_agg( distinct dds.db2_integer_to_date(termination_date)),',') as term_date
--     case
--       when termination_date = 0 then null
--       else dds.db2_integer_to_date(termination_date)
--     end) as term_date
from arkona.xfm_pymast 
where pymast_employee_number in (
  select aa.pymast_employee_number
  from (
    select pymast_employee_number, employee_name, 
      dds.db2_integer_to_date(hire_date) as hire_date, 
      case
        when termination_date = 0 then null
        else dds.db2_integer_to_date(termination_date)
      end as term_date
    from arkona.ext_pymast 
    where dds.db2_integer_to_date(hire_date) <= '07/14/2020'
      and dds.db2_integer_to_date(termination_date) >= '04/14/2020'
      and pymast_employee_number not in ('129874')) aa  -- MCCOY, KYLER termed for covered period
  full outer join ppp.gross_payroll_2019_report_employees bb on aa.pymast_employee_number = bb.employee_number  
  where bb.employee_number  is null)
 group by pymast_employee_number, pymast_company_number, employee_name;


select * 
from first_cut

update first_cut
set full_part = 'P', term_date = '2020-06-16'
where pymast_employee_number = '1135710'; 

update first_cut
set pay_period = 'S'
where pymast_employee_number = '115883';

update first_cut
set term_date = '2020-04-27'
where pymast_employee_number = '159823';

update first_cut
set term_date = '2020-09-04'
where pymast_employee_number = '164202';

update first_cut
set term_date = '9999-12-31', hire_date = '2020-07-06'
where pymast_employee_number = '165873';


create table ppp.first_cut as
select *
from first_cut;

insert into ppp.gross_payroll_2019_report_employees
select a.pymast_employee_number, a.pymast_company_number, a.employee_name,
  '="' ||right(b.ssn, 4)|| '"' as ssn, a.payroll_class, a.pay_period,
  a.full_part, a.hire_date::date, 
  case
    when a.term_date = '9999-12-31' then null
    else a.term_date::date
  end
from ppp.first_cut a
left join jon.ssn b on a.pymast_employee_number = b.employee_number;

-- moved this data to the honda data revision query
insert into ppp.covered_period_payroll_cost_employees
select a.pymast_employee_number, a.pymast_company_number, a.employee_name,
  '="' ||right(b.ssn, 4)|| '"' as ssn, a.payroll_class, a.pay_period,
  a.full_part, a.hire_date::date, 
  case
    when a.term_date = '9999-12-31' then null
    else a.term_date::date
  end
from ppp.first_cut a
left join jon.ssn b on a.pymast_employee_number = b.employee_number;

-- -- 10/13/20
-- -- had to redo fte_reduction_report, included this stuff in that script
-- insert into ppp.fte_reduction_employees
-- select a.pymast_employee_number, a.pymast_company_number, a.employee_name,
--   '="' ||right(b.ssn, 4)|| '"' as ssn, a.payroll_class, a.pay_period,
--   a.full_part, a.hire_date::date, 
--   case
--     when a.term_date = '9999-12-31' then null
--     else a.term_date::date
--   end
-- from ppp.first_cut a
-- left join jon.ssn b on a.pymast_employee_number = b.employee_number;

insert into ppp.rate_of_pay_employees
select a.pymast_employee_number, a.pymast_company_number, a.employee_name,
  '="' ||right(b.ssn, 4)|| '"' as ssn, a.payroll_class, a.pay_period,
  a.full_part, a.hire_date::date, 
  case
    when a.term_date = '9999-12-31' then null
    else a.term_date::date
  end
from ppp.first_cut a
left join jon.ssn b on a.pymast_employee_number = b.employee_number;