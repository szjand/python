﻿drop table if exists ppp.fte_reduction_employees cascade;
create table ppp.fte_reduction_employees (
  employee_number citext not null primary key,
  store citext,
  employee_name citext,
  ssn citext,
  payroll_class citext,
  pay_period citext,
  full_part citext,
  hire_date date,
  term_date date);
comment on table ppp.fte_reduction_employees is 'all employees employed and or receiving a paycheck at anytime 
  between 4/14/20 and 7/13/20 for GM and 4/15/20 and 7/14/20 for HN';

-- 10/06, consistent with the other reports, base employees are from 4/20 -> 7/20  
-- added an additional 22 employees in script oops_left_out_employed_but_not_check.sql 
insert into ppp.fte_reduction_employees  
select aa.employee_, 
  case
    when left(employee_, 1) = '1' then 'RY1'
    else 'RY2'
  end as store, 
  bb.employee_name, 
  '="' ||right(cc.ssn, 4)|| '"' as ssn, -- 09/30/20 this fixes the ssn leading zero problem
  array_to_string(aa.payroll_class, ',') as payroll_class, 
  array_to_string(aa.pay_period, ',') as pay_period,
  array_to_string(aa.full_part, ',') as full_part,
  dds.db2_integer_to_date(bb.hire_date) as hire_date, 
  case
    when bb.termination_date = 0 then null
    else dds.db2_integer_to_date(bb.termination_date)
  end as term_date
from (
  select employee_, array_agg(distinct pay_period) as pay_period, array_agg(distinct payroll_class) as payroll_class, array_agg(distinct full_part) as full_part
  from (
    select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
      reg_hours, overtime_hours, alt_pay_hours,
      (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date, 
      case
        when b.fullparttime = 'Full' then 'F'
        when b.fullparttime = 'Part' then 'P'
      end as full_part
    from arkona.ext_pyhshdta a
    left join ads.ext_edw_employee_dim b on a.employee_ = b.employeenumber
      and (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date between b.employeekeyfromdate and b.employeekeythrudate    
    where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '04/15/2020'::date and '07/10/2020'::date ) x group by employee_) aa
--     union all
--     select employee_, pay_period, payroll_class, hire_date, employee_name, vacation_taken, holiday_taken, sick_leave_taken,
--       reg_hours, overtime_hours, alt_pay_hours,
--       (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date as check_date,
--       case
--         when b.fullparttime = 'Full' then 'F'
--         when b.fullparttime = 'Part' then 'P'
--       end as full_part         
--     from arkona.ext_pyhshdta a
--     left join ads.ext_edw_employee_dim b on a.employee_ = b.employeenumber
--       and (('20'||a.check_year)::text||'-'||a.check_month::text||'-'||a.check_day::text)::date between b.employeekeyfromdate and b.employeekeythrudate      
--     where (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date    
--      ) x group by employee_) aa
left join arkona.ext_pymast bb on aa.employee_ = bb.pymast_employee_number     
left join jon.ssn cc on aa.employee_ = cc.employee_number;    
delete from ppp.fte_reduction_employees where employee_number = '1112425';  -- delete jim price

drop table if exists ppp.fte_reduction_report;
create table ppp.fte_reduction_report as
select aa.*, bb.lb_hours
from ppp.fte_reduction_employees aa
left join (
  select a.employee_number, round(sum(b.vacation_taken+b.holiday_taken+b.sick_leave_taken+b.reg_hours+b.overtime_hours+b.alt_pay_hours), 2) as lb_hours
  from ppp.fte_reduction_employees a
  left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
    and (('20'||check_year)::text||'-'||check_month::text||'-'||check_day::text)::date between '01/10/2020'::date and '02/29/2020'::date  
  group by a.employee_number) bb on aa.employee_number = bb.employee_number
order by store, employee_name;

