# encoding=utf-8
import db_cnx
import ops
import csv
import datetime
import string

# 1/13/16: limit to 45 days of data

pg_con = None
db2_con = None
run_id = None
task = 'ext_glptrns_cdc'
file_name = 'files/ext_glptrns_cdc.csv'
# today
# table_name = ('test.ext_glptrns_' + str(datetime.date.today().strftime("%m")) +
#               str(datetime.date.today().strftime("%d")))
# yesterday: for running script past midnight, but data if for previous day
# to be sure and get all of michelle's west coast/EOM postings
# eg, run at 04/22/16 2:30 AM, table name is 0421
table_name = ('test.ext_glptrns_' + str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%m")) +
              str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%d")))

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  gtco#, gttrn#, gtseq#, gtdtyp, gttype, gtpost, gtrsts, gtadjust, gtpsel,
                  gtjrnl, gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl#), trim(gtdoc#),
                  trim(gtrdoc#), gtrdtyp, trim(gtodoc#), trim(gtref#), trim(gtvnd#),
                  trim(gtdesc), gttamt, gtcost, gtctlo,gtoco#, rrn(rydedata.glptrns)
                FROM  rydedata.glptrns
                where gtdate between current_date - 45 day and current_date
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.postgres_ubuntu() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
            CREATE TABLE """ + table_name + """ (
              gtco_ citext, -- GTCO# : Company Number
              gttrn_ bigint, -- GTTRN# : Trans Number
              gtseq_ integer, -- GTSEQ# : Tran Seq Number
              gtdtyp citext, -- GTDTYP : Doc Type
              gttype citext, -- GTTYPE : Record Type
              gtpost citext, -- GTPOST : Post Status
              gtrsts citext, -- GTRSTS : Reconcile Status
              gtadjust citext, -- GTADJUST : Adjustment Tran
              gtpsel citext, -- GTPSEL : Select to Pay
              gtjrnl citext, -- GTJRNL : Journal
              gtdate date, -- GTDATE : Transaction Date
              gtrdate date, -- GTRDATE : Reconciled Date
              gtsdate date, -- GTSDATE : A/R Statemnt Date
              gtacct citext, -- GTACCT : Account Number
              gtctl_ citext, -- GTCTL# : Control Number
              gtdoc_ citext, -- GTDOC# : Document Number
              gtrdoc_ citext, -- GTRDOC# : Reconciled Doc#
              gtrdtyp citext, -- GTRDTYP : Recon Doc Type
              gtodoc_ citext, -- GTODOC# : Outside Doc#
              gtref_ citext, -- GTREF# : Reference Number
              gtvnd_ citext, -- GTVND# : Vendor Number
              gtdesc citext, -- GTDESC : Description
              gttamt numeric(11,2), -- GTTAMT : Transaction Amt
              gtcost numeric(11,2), -- GTCOST : Transaction Cost
              gtctlo citext, -- GTCTLO : Control Overide
              gtoco_ citext, -- GTOCO# : Originating Co#
              rrn bigint)
            WITH (
              OIDS=FALSE);
            """
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """
                    from stdin with csv encoding 'latin-1'""", io)
    ops.log_pass(run_id)
    print 'Passssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
