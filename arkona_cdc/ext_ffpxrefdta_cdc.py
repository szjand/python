# encoding=utf-8
import db_cnx
import ops
import csv
import datetime
import string

pg_con = None
db2_con = None
run_id = None
task = 'ext_ffpxrefdta_cdc'
file_name = 'files/ext_ffpxrefdta_cdc.csv'
# today
# table_name = ('test.ext_glptrns_' + str(datetime.date.today().strftime("%m")) +
#               str(datetime.date.today().strftime("%d")))
# yesterday: for running script past midnight, but data if for previous day
# to be sure and get all of michelle's west coast/EOM postings
# eg, run at 04/22/16 2:30 AM, table name is 0421
table_name = ('test.ext_ffpxrefdta_' + str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%m")) +
              str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%d")))

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        # print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select  TRIM(COMPANY_NUMBER),TRIM(CONSOLIDATION_GRP),TRIM(G_L_COMPANY_),TRIM(FACTORY_CODE),
                    FACTORY_FINANCIAL_YEAR,TRIM(G_L_ACCT_NUMBER),TRIM(FACTORY_ACCOUNT),TRIM(FACTORY_ACCOUNT2),
                    TRIM(FACTORY_ACCOUNT3),TRIM(FACTORY_ACCOUNT4),TRIM(FACTORY_ACCOUNT5),TRIM(FACTORY_ACCOUNT6),
                    TRIM(FACTORY_ACCOUNT7),TRIM(FACTORY_ACCOUNT8),TRIM(FACTORY_ACCOUNT9),TRIM(FACTORY_ACCOUNT10),
                    FACT_ACCOUNT_,FACT_ACCOUNT__FXFP02,FACT_ACCOUNT__FXFP03,FACT_ACCOUNT__FXFP04,FACT_ACCOUNT__FXFP05,
                    FACT_ACCOUNT__FXFP06,FACT_ACCOUNT__FXFP07,FACT_ACCOUNT__FXFP08,FACT_ACCOUNT__FXFP09,FACT_ACCOUNT__FXFP10
                from rydedata.ffpxrefdta
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.postgres_ubuntu() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                CREATE TABLE """ + table_name + """ (
                    COMPANY_NUMBER CITEXT,
                    CONSOLIDATION_GRP CITEXT,
                    G_L_COMPANY_ CITEXT,
                    FACTORY_CODE CITEXT,
                    FACTORY_FINANCIAL_YEAR INTEGER,
                    G_L_ACCT_NUMBER CITEXT,
                    FACTORY_ACCOUNT CITEXT,
                    FACTORY_ACCOUNT2 CITEXT,
                    FACTORY_ACCOUNT3 CITEXT,
                    FACTORY_ACCOUNT4 CITEXT,
                    FACTORY_ACCOUNT5 CITEXT,
                    FACTORY_ACCOUNT6 CITEXT,
                    FACTORY_ACCOUNT7 CITEXT,
                    FACTORY_ACCOUNT8 CITEXT,
                    FACTORY_ACCOUNT9 CITEXT,
                    FACTORY_ACCOUNT10 CITEXT,
                    FACT_ACCOUNT_ NUMERIC (3,2),
                    FACT_ACCOUNT__FXFP02 NUMERIC (3,2),
                    FACT_ACCOUNT__FXFP03 NUMERIC (3,2),
                    FACT_ACCOUNT__FXFP04 NUMERIC (3,2),
                    FACT_ACCOUNT__FXFP05 NUMERIC (3,2),
                    FACT_ACCOUNT__FXFP06 NUMERIC (3,2),
                    FACT_ACCOUNT__FXFP07 NUMERIC (3,2),
                    FACT_ACCOUNT__FXFP08 NUMERIC (3,2),
                    FACT_ACCOUNT__FXFP09 NUMERIC (3,2),
                    FACT_ACCOUNT__FXFP10 NUMERIC (3,2))
                WITH (OIDS=FALSE);
            """
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """
                    from stdin with csv encoding 'latin-1'""", io)
    ops.log_pass(run_id)
    # print 'Passssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception as error:
    print(error)
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
