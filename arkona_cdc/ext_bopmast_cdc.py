# encoding=utf-8
import db_cnx
import ops
import csv
import datetime
import string

pg_con = None
db2_con = None
run_id = None
task = 'ext_bopmast_cdc'
file_name = 'files/ext_bopmast_cdc.csv'
# today
# table_name = ('test.ext_bopomast_' + str(datetime.date.today().strftime("%m")) +
#               str(datetime.date.today().strftime("%d")))
# yesterday: for running script past midnight, but data if for previous day
# to be sure and get all of michelle's west coast/EOM postings
# eg, run at 04/22/16 2:30 AM, table name is 0421
table_name = ('test.ext_bopmast_' + str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%m")) +
              str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%d")))

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  trim(BOPMAST_COMPANY_NUMBER),RECORD_KEY,trim(RECORD_STATUS),trim(RECORD_TYPE),trim(VEHICLE_TYPE),
                  trim(FRANCHISE_CODE),trim(SALE_TYPE),trim(BOPMAST_STOCK_NUMBER),trim(BOPMAST_VIN),BUYER_NUMBER,
                  CO_BUYER_NUMBER,trim(BOPMAST_SEARCH_NAME),RETAIL_PRICE,DOWN_PAYMENT,TRADE_ALLOWANCE,TRADE_ACV,
                  TRADE_PAYOFF,APR,TERM,INSURANCE_TERM,DAYS_TO_1ST_PAYMT,trim(PAYMENT_FREQUENCY),PAYMENT,
                  AMOUNT_FINANCED,ORIGINATION_DATE,DATE_1ST_PAYMENT,DATE_LAST_PAYMENT,DATE_STICKER_EXP,TAX_GROUP,
                  LENDING_SOURCE,LEASING_SOURCE,INSURANCE_SOURCE,GAP_SOURCE,SERV_CONT_COMPANY,SERV_CONT_AMOUNT,
                  SERV_CONT_DEDUCT,SERV_CONT_MILES,SERV_CONT_MONTHS,SERV_CONT_COST,SERV_CONT_STR_DAT,SERV_CONT_EXP_DAT,
                  trim(ADD_SERV_TO_CAP_C),CREDIT_LIFE_PREMIUM,LEVEL_CL_PREMIUM,A_H_PREMIUM,GAP_PREMIUM,GAP_COST,
                  trim(CREDIT_LIFE_CODE),trim(A_H_CODE),trim(GAP_CODE),TAX_1,TAX_2,TAX_3,TAX_4,TAX_5,BUY_RATE,
                  RESERVE_HOLDBACK_,NUMBER_OF_TRADES,trim(TEMPORARYSTICKER_),trim(SERV_CONT_NUMBER),
                  trim(CRED_LIFE_CERT_),trim(A_H_CERT_),trim(GAP_CERT_),trim(FLOORING_VENDOR),trim(PD_INSURANCE_CO),
                  trim(PD_INSUR_CO_ALIAS),trim(PD_POLICY_NUMBER),PD_POLICY_EXP_DAT,trim(PD_AGENT_NAME),trim(PD_AGENT_ADDRESS),
                  trim(PD_AGENT_CITY),trim(PD_AGENT_STATE),PD_AGENT_ZIP,PD_AGENT_PHONE_NO,trim(PD_COLL_COVERAGE),
                  trim(PD_COMP_COVERAGE),trim(PD_FIRE_THEFT_COV),PD_COLL_DEDUCT,PD_COMP_DEDUCT,PD_FIRE_THEFT_DED,
                  trim(PD_VEHICLE_USE),PD_PREMIUM,AMO_TOTAL,FEE_TOTAL,TOTAL_OF_PAYMENTS,INTEREST_CHARGED,VEHICLE_COST,
                  INTERNAL_COST,ADDS_TO_COST_TOT,HOLD_BACK,PACK_AMOUNT,COMM_GROSS,INCENTIVE,REBATE,ODOMETER_AT_SALE,
                  PICKUP_AMOUNT,PICKUP_BEG_DATE,trim(PICKUP_FREQ),PICKUP_NO_PAY,LIFE_RESERVE,A_H_RESERVE,GAP_RESERVE,
                  PDI_RESERVE,AMO_RESERVE,SERV_RESERVE,FINANCE_RESERVE,TOTAL_F_I_RESERVE,trim(PRIMARY_SALESPERS),
                  trim(SECONDARY_SLSPERS2),DEF_DOWN_PAYMENT,DEF_DOWN_DUE_DATE,LOAN_ORIG_FEE,trim(LOF_OVERRIDE),
                  EFFECTIVE_APR,MSRP,LEASE_PRICE,CAPITALIZED_COST,CAP_COST_REDUCT,APR_BMLAPR,LEASE_FACTOR,LEASE_TERM,
                  RESIDUAL_,RESIDUAL_AMT,NET_RESIDUAL,TOT_DEPRECIATION,MILES_PER_YEAR,MILES_YEAR_ALLOW,EXCESS_MILE_RATE,
                  EXCESS_MILE_RATE2,EXCESS_MILE_CHG,ACQUISITION_FEE,LEASE_PAYMENT,LEASE_PAYMT_TAX,LEASE_SALES_TAX,
                  MO_LEASE_CHARGE,CAP_REDUCTION_TAX,CAP_REDUCT_TAX_1,CAP_REDUCT_TAX_2,CAP_REDUCT_TAX_3,CAP_REDUCT_TAX_4,
                  CAP_REDUCT_TAX_6,SECURITY_DEPOSIT,DEPOSIT_RECEIVED,CASH_RECEIVED,FACTOR_BUY_RATE,TOTAL_INITIAL_CHG,
                  trim(CAP_LEASE_SLS_TAX),trim(SECUR_DEPOS_ORIDE),trim(ACQUI_FEE_ORIDE),trim(FET_OPTIONS),
                  trim(CAPITALIZE_SLS_TX),trim(ADV_PAYMENT_LEASE),ORIDE_TAX_RATE_1,ORIDE_TAX_RATE_2,ORIDE_TAX_RATE_3,
                  ORIDE_TAX_RATE_4,ORIDE_TAX_RATE_6,trim(ORIDE_EXC_TRADE_1),trim(ORIDE_EXC_TRADE_2),trim(ORIDE_EXC_TRADE_3),
                  trim(ORIDE_EXC_TRADE_4),trim(TAX_OVERRIDE_1),trim(TAX_OVERRIDE_2),trim(TAX_OVERRIDE_3),trim(TAX_OVERRIDE_4),
                  trim(TAX_OVERRIDE_6),trim(FIN_RESERV_ORIDE),trim(COST_OVERRIDE),trim(HOLDBACK_OVERRIDE),trim(TAX_OVERRIDE),
                  trim(REBATE_DOWN_PAY),trim(CONTRACT_SELECTED),trim(CHG_INT_ON_1_PYMT),trim(SALE_ACCOUNT),trim(UNWIND_FLAG),
                  trim(DOCUMENT_NUMBER),BUYER_AGE,CO_BUYER_AGE,LIFE_COVERAGE,A_H_COVERAGE,INSUR_TERM_DATE,LEASE_TAX_GROUP,
                  CO_BUYER_LIFE_PREM,DATE_SAVED,DATE_TRADE_PAYOFF,trim(CAPITALIZE_CR_TAX),REBATE_AS_DOWN,TOTAL_DOWN,
                  TOT_CAP_REDUCTION,GROSS_CAP_COST,DEALER_INCENTIVE,trim(FORMS_PRINTED),trim(NEW_VEH_DELIV_RPT),DATE_APPROVED,
                  DATE_CAPPED,EXCESS_MILEAGE_CHARGE_BALL,PENALTY_EXCESS_MILEAGE_CHARG,PREPAID_EXCESS_MILEAGE_CHARG,
                  MILES_PER_YEAR_BALLOON,MILES_PER_YEAR_ALLOWED_BAL,NET_RESIDUAL_BALLOON,RESIDUAL_AMOUNT_BALLOON,
                  RESIDUAL_PERCENT_BALLOON,AFTER_MARKET_TAX,EXEMPT_PAYMENTS,FULL_PAYMENTS,trim(FORD_PLAN_CODE),A_H_TERM,
                  LEASE_PAYMENT_TAX,trim(TRADE_FROM_LEASE),ORIG_NET_TRADE,NTAX_TRADE_EQUITY,trim(PD_POLICY_NUMBER_B9PDP#),
                  PART_EXEMPT_PYMT,PART_EXEMPT_TAX,SERVICE_CONT_TAX,TOTAL_A_H_COVERAG,TAX_BASE_AMOUNT,WORK_IN_PROCESS,
                  DATE_LAST_LEASE_PAYMENT,AFTERMARKET_TAX_RATE5,SERVICE_CONTRACT_TAX_RATE7,ODD_LAST_PAYMENT,
                  ORIGINAL_CONTRACT_DATE,trim(LEVEL_LIFE_OPTIONAL),REG_FEE_TAX_1,REG_FEE_TAX_2,PYMT_EXCLUDING_INS,
                  PAYBACK_RATE,trim(BUYER_RED_RT_IND),trim(CO_BUYER_RED_RT_IND),trim(BUYER_GENDER),trim(CO_BUYER_GENDER),
                  trim(CRIT_ILL_CODE),CRIT_ILL_PREMIUM,trim(LOSS_OF_EMPLOYMENT_CODE),LOSS_OF_EMPLOYMENT_PREMIUM,
                  UPFRONT_FEE_TAX_1,UPFRONT_FEE_TAX_2,trim(RESIDUAL_INSURANCE),trim(DISABILITY_ELIG),trim(REM_INS_CODE),
                  PREMIUM_PERCENT,GAP_TAX_RATE8,DELIVERY_DATE,INITIAL_MILES,INITIAL_MILE_RATE,INITIAL_MILE_CHG,
                  LEASE_PMTS_REMAINING,F_I_GROSS,HOUSE_GROSS,MONTHLY_GAP_PREMIUM,trim(INITIAL_MILES_CHG_FLAG),
                  INITIAL_MILES_THRESHOLD,GAP_TAX,PRO_RATA_AMOUNT,PRO_RATA_TAX_1,PRO_RATA_TAX_2,trim(OPENTRACK_VENDOR),
                  trim(F_I_MANAGER),trim(SALES_MANAGER),TRADE_UPFRONT,TRADE_CAPITALIZED,REBATE_UPFRONT,REBATE_CAPITALIZED,
                  CASH_UPFRONT,CASH_CAPITALIZED,SIGN_AND_DRIVE,trim(ROLL_PRICE_FLAG),SAVED_PRICE,CRITICAL_ILLNESS_TERM,
                  LOSS_OF_EMPLOYMENT_TERM,CREDIT_LIFE_SOURCE,A_H_SOURCE,CRITICAL_ILLNESS_SOURCE,LOSS_OF_EMPLOYMENT_SOURCE,
                  A_H_COVERAGE_BMAHCOV,LEVEL_CI_PREMIUM,trim(CRITICAL_ILLNES_CERT_),trim(LOSS_OF_EMPLOYMENT_CERT_),
                  CRITICAL_ILLNESS_RESERVE,LOSS_OF_EMPLOYMENT_RESERVE,VW_EFFECTIVE_APR,UNREALIZED_REBATE,
                  A_H_INSURANCE_TERM_DATE,CI_INSURANCE_TERM_DATE,LE_INSURANCE_TERM_DATE,trim(FIRST_PAYMENT_WAIVER)
                FROM  rydedata.bopmast
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.postgres_ubuntu() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                CREATE TABLE """ + table_name + """ (
                  bopmast_company_number citext NOT NULL, -- BMCO# : Company Number
                  record_key integer NOT NULL, -- BMKEY : Record Key
                  record_status citext, -- BMSTAT : Record Status
                  record_type citext, -- BMTYPE : Record Type
                  vehicle_type citext, -- BMVTYP : Vehicle Type
                  franchise_code citext, -- BMFRAN : Franchise Code
                  sale_type citext, -- BMWHSL : Sale Type
                  bopmast_stock_number citext, -- BMSTK# : Stock Number
                  bopmast_vin citext, -- BMVIN : VIN
                  buyer_number integer, -- BMBUYR : Buyer Number
                  co_buyer_number integer, -- BMCBYR : Co-Buyer Number
                  bopmast_search_name citext, -- BMSNAM : Search Name
                  retail_price numeric(9,2), -- BMPRIC : Retail Price
                  down_payment numeric(9,2), -- BMDOWN : Down Payment
                  trade_allowance numeric(9,2), -- BMTRAD : Trade Allowance
                  trade_acv numeric(9,2), -- BMTACV : Trade ACV
                  trade_payoff numeric(9,2), -- BMTDPO : Trade Payoff
                  apr numeric(5,3), -- BMAPR : APR
                  term integer, -- BMTERM : Term
                  insurance_term integer, -- BMITRM : Insurance Term
                  days_to_1st_paymt integer, -- BMDYFP : Days to 1st Paymt
                  payment_frequency citext, -- BMPFRQ : Payment Frequency
                  payment numeric(9,2), -- BMPYMT : Payment
                  amount_financed numeric(9,2), -- BMAMTF : Amount Financed
                  origination_date integer, -- BMDTOR : Origination Date
                  date_1st_payment integer, -- BMDTFP : Date 1st Payment
                  date_last_payment integer, -- BMDTLP : Date Last Payment
                  date_sticker_exp integer, -- BMDTSE : Date Sticker Exp
                  tax_group integer, -- BMTAXG : Tax Group
                  lending_source integer, -- BMLSRC : Lending Source
                  leasing_source integer, -- BMSSRC : Leasing Source
                  insurance_source integer, -- BMISRC : Insurance Source
                  gap_source integer, -- BMGSRC : GAP Source
                  serv_cont_company integer, -- BMSRVC : Serv Cont Company
                  serv_cont_amount numeric(7,2), -- BMSVCA : Serv Cont Amount
                  serv_cont_deduct numeric(7,2), -- BMSDED : Serv Cont Deduct
                  serv_cont_miles integer, -- BMSMIL : Serv Cont Miles
                  serv_cont_months integer, -- BMSMOS : Serv Cont Months
                  serv_cont_cost numeric(7,2), -- BMSCST : Serv Cont Cost
                  serv_cont_str_dat integer, -- BMSCSD : Serv Cont Str Dat
                  serv_cont_exp_dat integer, -- BMSCED : Serv Cont Exp Dat
                  add_serv_to_cap_c citext, -- BMSVAC : Add Serv to Cap C
                  credit_life_premium numeric(7,2), -- BMCLPM : Credit Life Premium
                  level_cl_premium numeric(7,2), -- BMLVPM : Level CL Premium
                  a_h_premium numeric(7,2), -- BMAHPM : A&H Premium
                  gap_premium numeric(7,2), -- BMGAPP : GAP Premium
                  gap_cost numeric(7,2), -- BMGCST : GAP Cost
                  credit_life_code citext, -- BMCLCD : Credit Life Code
                  a_h_code citext, -- BMAHCD : A&H Code
                  gap_code citext, -- BMGPCD : GAP Code
                  tax_1 numeric(7,2), -- BMTAX1 : Tax 1
                  tax_2 numeric(7,2), -- BMTAX2 : Tax 2
                  tax_3 numeric(7,2), -- BMTAX3 : Tax 3
                  tax_4 numeric(7,2), -- BMTAX4 : Tax 4
                  tax_5 numeric(7,2), -- BMTAX5 : Tax 5
                  buy_rate numeric(5,3), -- BMBRAT : Buy Rate
                  reserve_holdback_ numeric(4,2), -- BMRHBP : Reserve Holdback%
                  number_of_trades integer, -- BMTRD# : Number of Trades
                  temporarysticker_ citext, -- BMTSK# : TemporarySticker#
                  serv_cont_number citext, -- BMSVC# : Serv Cont Number
                  cred_life_cert_ citext, -- BMCLC# : Cred Life Cert #
                  a_h_cert_ citext, -- BMAHC# : A&H Cert #
                  gap_cert_ citext, -- BMGAP# : GAP Cert #
                  flooring_vendor citext, -- BMFVND : Flooring Vendor
                  pd_insurance_co citext, -- BMPDIC : PD Insurance Co
                  pd_insur_co_alias citext, -- BMPDAL : PD Insur Co Alias
                  pd_policy_number citext, -- BMPDP# : PD Policy Number
                  pd_policy_exp_dat integer, -- BMPDED : PD Policy Exp Dat
                  pd_agent_name citext, -- BMPDAN : PD Agent Name
                  pd_agent_address citext, -- BMPDAA : PD Agent Address
                  pd_agent_city citext, -- BMPDAC : PD Agent City
                  pd_agent_state citext, -- BMPDAS : PD Agent State
                  pd_agent_zip integer, -- BMPDAZ : PD Agent Zip
                  pd_agent_phone_no bigint, -- BMPDPN : PD Agent Phone No
                  pd_coll_coverage citext, -- BMPDCL : PD Coll Coverage
                  pd_comp_coverage citext, -- BMPDCP : PD Comp Coverage
                  pd_fire_theft_cov citext, -- BMPDFT : PD Fire/Theft Cov
                  pd_coll_deduct integer, -- BMPDCD : PD Coll Deduct
                  pd_comp_deduct integer, -- BMPDPD : PD Comp Deduct
                  pd_fire_theft_ded integer, -- BMPDFD : PD Fire/Theft Ded
                  pd_vehicle_use citext, -- BMPDVU : PD Vehicle Use
                  pd_premium numeric(7,2), -- BMPDPM : PD Premium
                  amo_total numeric(9,2), -- BMATOT : AMO Total
                  fee_total numeric(7,2), -- BMFTOT : Fee Total
                  total_of_payments numeric(9,2), -- BMTPAY : Total of Payments
                  interest_charged numeric(9,2), -- BMICHG : Interest Charged
                  vehicle_cost numeric(9,2), -- BMVCST : Vehicle Cost
                  internal_cost numeric(7,2), -- BMICST : Internal Cost
                  adds_to_cost_tot numeric(7,2), -- BMADDC : Adds to Cost Tot
                  hold_back numeric(7,2), -- BMHLDB : Hold Back
                  pack_amount numeric(7,2), -- BMPACK : Pack Amount
                  comm_gross numeric(9,2), -- BMCGRS : Comm Gross
                  incentive numeric(7,2), -- BMINCT : Incentive
                  rebate numeric(7,2), -- BMRBTE : Rebate
                  odometer_at_sale integer, -- BMODOM : Odometer at Sale
                  pickup_amount numeric(7,2), -- BMPAMT : Pickup Amount
                  pickup_beg_date integer, -- BMPBDT : Pickup Beg Date
                  pickup_freq citext, -- BMPCKF : Pickup Freq
                  pickup_no_pay integer, -- BMNPPY : Pickup No. Pay
                  life_reserve numeric(7,2), -- BMLRES : Life Reserve
                  a_h_reserve numeric(7,2), -- BMARES : A&H  Reserve
                  gap_reserve numeric(7,2), -- BMGRES : GAP  Reserve
                  pdi_reserve numeric(7,2), -- BMPRES : PDI  Reserve
                  amo_reserve numeric(9,2), -- BMMRES : AMO  Reserve
                  serv_reserve numeric(7,2), -- BMSRES : Serv Reserve
                  finance_reserve numeric(7,2), -- BMFRES : Finance Reserve
                  total_f_i_reserve numeric(9,2), -- BMTRES : Total F&I Reserve
                  primary_salespers citext, -- BMPSLP : Primary Salespers
                  secondary_slspers2 citext, -- BMSLP2 : Secondary Slspers
                  def_down_payment numeric(7,2), -- BMDDWN : Def Down Payment
                  def_down_due_date integer, -- BMDDDD : Def Down Due Date
                  loan_orig_fee numeric(7,2), -- BMLOF : Loan Orig Fee
                  lof_override citext, -- BMLOFO : LOF Override
                  effective_apr numeric(5,3), -- BMEAPR : Effective APR
                  msrp numeric(9,2), -- BMMSRP : MSRP
                  lease_price numeric(9,2), -- BMLPRC : Lease Price
                  capitalized_cost numeric(9,2), -- BMCAPC : Capitalized Cost
                  cap_cost_reduct numeric(9,2), -- BMCAPR : Cap Cost Reduct
                  apr_bmlapr numeric(5,3), -- BMLAPR : APR
                  lease_factor numeric(7,7), -- BMLFAC : Lease Factor
                  lease_term integer, -- BMLTRM : Lease Term
                  residual_ numeric(4,2), -- BMRESP : Residual %
                  residual_amt numeric(9,2), -- BMRESA : Residual Amt
                  net_residual numeric(9,2), -- BMNETR : Net Residual
                  tot_depreciation numeric(9,2), -- BMTDEP : Tot Depreciation
                  miles_per_year integer, -- BMMIPY : Miles Per Year
                  miles_year_allow integer, -- BMMPYA : Miles/Year Allow
                  excess_mile_rate integer, -- BMEMRT : Excess Mile Rate
                  excess_mile_rate2 integer, -- BMEMR2 : Excess Mile Rate2
                  excess_mile_chg numeric(7,2), -- BMEMCG : Excess Mile Chg
                  acquisition_fee numeric(7,2), -- BMACQF : Acquisition Fee
                  lease_payment numeric(7,2), -- BMLPAY : Lease Payment
                  lease_paymt_tax numeric(7,2), -- BMLPYM : Lease Paymt & Tax
                  lease_sales_tax numeric(7,2), -- BMLSTX : Lease Sales Tax
                  mo_lease_charge numeric(7,2), -- BMLCHG : Mo. Lease Charge
                  cap_reduction_tax numeric(7,2), -- BMCRTX : Cap Reduction Tax
                  cap_reduct_tax_1 numeric(7,2), -- BMCRT1 : Cap Reduct Tax 1
                  cap_reduct_tax_2 numeric(7,2), -- BMCRT2 : Cap Reduct Tax 2
                  cap_reduct_tax_3 numeric(7,2), -- BMCRT3 : Cap Reduct Tax 3
                  cap_reduct_tax_4 numeric(7,2), -- BMCRT4 : Cap Reduct Tax 4
                  cap_reduct_tax_6 numeric(7,2), -- BMCRT6 : Cap Reduct Tax 6
                  security_deposit numeric(7,2), -- BMSECD : Security Deposit
                  deposit_received numeric(7,2), -- BMDPOS : Deposit Received
                  cash_received numeric(8,2), -- BMCSHR : Cash Received
                  factor_buy_rate numeric(5,5), -- BMLFBR : Factor Buy Rate
                  total_initial_chg numeric(8,2), -- BMITOT : Total Initial Chg
                  cap_lease_sls_tax citext, -- BMCTAX : Cap Lease Sls Tax
                  secur_depos_oride citext, -- BMSDOR : Secur Depos Oride
                  acqui_fee_oride citext, -- BMAFOR : Acqui Fee Oride
                  fet_options citext, -- BMFETO : FET Options
                  capitalize_sls_tx citext, -- BMCSTX : Capitalize Sls Tx
                  adv_payment_lease citext, -- BMAPYL : Adv Payment Lease
                  oride_tax_rate_1 numeric(7,5), -- BMRAT1 : Oride Tax Rate 1
                  oride_tax_rate_2 numeric(7,5), -- BMRAT2 : Oride Tax Rate 2
                  oride_tax_rate_3 numeric(7,5), -- BMRAT3 : Oride Tax Rate 3
                  oride_tax_rate_4 numeric(7,5), -- BMRAT4 : Oride Tax Rate 4
                  oride_tax_rate_6 numeric(7,5), -- BMRAT6 : Oride Tax Rate 6
                  oride_exc_trade_1 citext, -- BMEXT1 : Oride Exc Trade 1
                  oride_exc_trade_2 citext, -- BMEXT2 : Oride Exc Trade 2
                  oride_exc_trade_3 citext, -- BMEXT3 : Oride Exc Trade 3
                  oride_exc_trade_4 citext, -- BMEXT4 : Oride Exc Trade 4
                  tax_override_1 citext, -- BMTOR1 : Tax Override 1
                  tax_override_2 citext, -- BMTOR2 : Tax Override 2
                  tax_override_3 citext, -- BMTOR3 : Tax Override 3
                  tax_override_4 citext, -- BMTOR4 : Tax Override 4
                  tax_override_6 citext, -- BMTOR6 : Tax Override 6
                  fin_reserv_oride citext, -- BMFROR : Fin Reserv Oride
                  cost_override citext, -- BMCOVR : Cost Override
                  holdback_override citext, -- BMHOVR : Holdback Override
                  tax_override citext, -- BMTOVR : Tax Override
                  rebate_down_pay citext, -- BMRBDP : Rebate/Down Pay
                  contract_selected citext, -- BMCSEL : Contract Selected
                  chg_int_on_1_pymt citext, -- BMCIOP : Chg Int on 1 Pymt
                  sale_account citext, -- BMSACT : Sale Account
                  unwind_flag citext, -- BMUNWD : Unwind Flag
                  document_number citext, -- BMDOC# : Document Number
                  buyer_age integer, -- BMBAGE : Buyer Age
                  co_buyer_age integer, -- BMCBAG : Co-Buyer Age
                  life_coverage numeric(9,2), -- BMLCOV : Life Coverage
                  a_h_coverage numeric(9,2), -- BMHCOV : A&H Coverage
                  insur_term_date integer, -- BMITDT : Insur Term Date
                  lease_tax_group integer, -- BMLTXG : Lease Tax Group
                  co_buyer_life_prem numeric(7,2), -- BMCBLP : Co-Buyer Life Prem
                  date_saved integer, -- BMDTSV : Date Saved
                  date_trade_payoff integer, -- BMDTTP : Date Trade Payoff
                  capitalize_cr_tax citext, -- BMCCRT : Capitalize CR Tax
                  rebate_as_down numeric(9,2), -- BMRDWN : Rebate as Down
                  total_down numeric(9,2), -- BMTDWN : Total Down
                  tot_cap_reduction numeric(9,2), -- BMTCRD : Tot Cap Reduction
                  gross_cap_cost numeric(9,2), -- BMGCAP : Gross Cap Cost
                  dealer_incentive numeric(7,2), -- BMDINCT : Dealer Incentive
                  forms_printed citext, -- BMPRNT : Forms Printed
                  new_veh_deliv_rpt citext, -- BMNVDR : New Veh Deliv Rpt
                  date_approved date, -- BMDTAPRV : Date Approved
                  date_capped date, -- BMDTCAP : Date Capped
                  excess_mileage_charge_ball numeric(7,2), -- BMEMCB : Excess mileage charge - ball
                  penalty_excess_mileage_charg integer, -- BMEMPB : Penalty excess mileage charg
                  prepaid_excess_mileage_charg integer, -- BMEMRB : Prepaid excess mileage charg
                  miles_per_year_balloon integer, -- BMMPYB : Miles per year - balloon
                  miles_per_year_allowed_bal integer, -- BMMYAB : Miles per year allowed - bal
                  net_residual_balloon numeric(9,2), -- BMNTRB : Net residual - balloon
                  residual_amount_balloon numeric(9,2), -- BMRSAB : Residual amount - balloon
                  residual_percent_balloon integer, -- BMRSPB : Residual percent - balloon
                  after_market_tax numeric(7,2), -- B9AMTAX : After Market Tax
                  exempt_payments integer, -- B9EPAY : Exempt Payments
                  full_payments integer, -- B9FPAY : Full Payments
                  ford_plan_code citext, -- B9FPCODE : Ford Plan Code
                  a_h_term integer, -- B9HTERM : A&H Term
                  lease_payment_tax numeric(7,2), -- B9LPTAX : Lease Payment Tax
                  trade_from_lease citext, -- B9LTRAD : Trade From Lease
                  orig_net_trade numeric(9,2), -- B9ONETTR : Orig Net Trade
                  ntax_trade_equity numeric(9,2), -- B9ONTEQ : NTax Trade Equity
                  pd_policy_number_b9pdp_ citext, -- B9PDP# : PD Policy Number
                  part_exempt_pymt numeric(7,2), -- B9PEAMT : Part Exempt Pymt
                  part_exempt_tax numeric(7,2), -- B9PETAX : Part Exempt Tax
                  service_cont_tax numeric(7,2), -- B9SCTAX : Service Cont Tax
                  total_a_h_coverag integer, -- B9THCOV : Total A&H Coverag
                  tax_base_amount numeric(9,2), -- B9TXBASE : Tax Base Amount
                  work_in_process numeric(7,2), -- B9WKIP : Work In Process
                  date_last_lease_payment integer, -- BMLDTLP : Date Last Lease Payment
                  aftermarket_tax_rate5 numeric(7,5), -- BMRAT5 : Aftermarket Tax Rate
                  service_contract_tax_rate7 numeric(7,5), -- BMRAT7 : Service Contract Tax Rate
                  odd_last_payment numeric(9,2), -- BMOLPM : Odd Last Payment
                  original_contract_date integer, -- BMOCDT : Original Contract Date
                  level_life_optional citext, -- BMLLOPT : Level Life Optional
                  reg_fee_tax_1 numeric(7,2), -- BMRFTX1 : Reg Fee Tax 1
                  reg_fee_tax_2 numeric(7,2), -- BMRFTX2 : Reg Fee Tax 2
                  pymt_excluding_ins numeric(9,2), -- BMPYXINS : Pymt excluding Ins
                  payback_rate numeric(5,3), -- BMPBKR : Payback Rate
                  buyer_red_rt_ind citext, -- BMBRRI : Buyer Red Rt Ind
                  co_buyer_red_rt_ind citext, -- BMCRRI : Co-Buyer Red Rt Ind
                  buyer_gender citext, -- BMBGEN : Buyer Gender
                  co_buyer_gender citext, -- BMCGEN : Co-Buyer Gender
                  crit_ill_code citext, -- BMCICD : Crit Ill Code
                  crit_ill_premium numeric(7,2), -- BMCIPM : Crit Ill Premium
                  loss_of_employment_code citext, -- BMLECD : Loss of Employment Code
                  loss_of_employment_premium numeric(7,2), -- BMLEPM : Loss of Employment Premium
                  upfront_fee_tax_1 numeric(7,2), -- BMUFTX1 : Upfront Fee Tax 1
                  upfront_fee_tax_2 numeric(7,2), -- BMUFTX2 : Upfront Fee Tax 2
                  residual_insurance citext, -- BMRESIN : Residual Insurance
                  disability_elig citext, -- BMDISEL : Disability Elig
                  rem_ins_code citext, -- BMREMIC : REM Ins Code
                  premium_percent numeric(4,4), -- BMPRMPCT : Premium Percent
                  gap_tax_rate8 numeric(7,5), -- BMRAT8 : GAP Tax Rate
                  delivery_date date, -- BMDELVDT : Delivery Date
                  initial_miles integer, -- BMIMILE : Initial Miles
                  initial_mile_rate integer, -- BMIMRT : Initial Mile Rate
                  initial_mile_chg numeric(7,2), -- BMIMCG : Initial Mile Chg
                  lease_pmts_remaining numeric(7,2), -- BMLPYREM : Lease Pmts Remaining
                  f_i_gross numeric(9,2), -- BMFIGRS : F&I Gross
                  house_gross numeric(9,2), -- BMHGRS : House Gross
                  monthly_gap_premium numeric(9,2), -- BMMTHGAPP : Monthly GAP Premium
                  initial_miles_chg_flag citext, -- BMIMFLAG : Initial Miles Chg Flag
                  initial_miles_threshold integer, -- BMIMTHLD : Initial Miles Threshold
                  gap_tax numeric(7,2), -- BMGAPTAX : Gap Tax
                  pro_rata_amount numeric(7,2), -- BMPRORATA : Pro-Rata Amount
                  pro_rata_tax_1 numeric(7,2), -- BMPRTAX1 : Pro-Rata Tax 1
                  pro_rata_tax_2 numeric(7,2), -- BMPRTAX2 : Pro-Rata Tax 2
                  opentrack_vendor citext, -- BMOTVEND : OpenTrack Vendor
                  f_i_manager citext, -- BMFIMGR : F&I Manager
                  sales_manager citext, -- BMSLMGR : Sales Manager
                  trade_upfront numeric(9,2), -- BMTRUF : Trade Upfront
                  trade_capitalized numeric(9,2), -- BMTRCP : Trade Capitalized
                  rebate_upfront numeric(7,2), -- BMRBUF : Rebate Upfront
                  rebate_capitalized numeric(7,2), -- BMRBCP : Rebate Capitalized
                  cash_upfront numeric(9,2), -- BMCSUF : Cash Upfront
                  cash_capitalized numeric(9,2), -- BMCSCP : Cash Capitalized
                  sign_and_drive numeric(7,2), -- BMSGDA : Sign And Drive
                  roll_price_flag citext, -- BMRPFL : Roll Price Flag
                  saved_price numeric(9,2), -- BMSPRC : Saved Price
                  critical_illness_term integer, -- BMCITRM : Critical Illness Term
                  loss_of_employment_term integer, -- BMLETRM : Loss of Employment Term
                  credit_life_source integer, -- BMCLSRC : Credit Life Source
                  a_h_source integer, -- BMAHSRC : A&H Source
                  critical_illness_source integer, -- BMCISRC : Critical Illness Source
                  loss_of_employment_source integer, -- BMLESRC : Loss of Employment Source
                  a_h_coverage_bmahcov integer, -- BMAHCOV : A&H Coverage
                  level_ci_premium numeric(7,2), -- BMLVCIPM : Level CI Premium
                  critical_illnes_cert_ citext, -- BMCIC# : Critical Illnes Cert #
                  loss_of_employment_cert_ citext, -- BMLEC# : Loss of Employment Cert #
                  critical_illness_reserve numeric(7,2), -- BMCIRES : Critical Illness Reserve
                  loss_of_employment_reserve numeric(7,2), -- BMLERES : Loss of Employment Reserve
                  vw_effective_apr numeric(5,3), -- BMVWAPR : VW Effective APR
                  unrealized_rebate numeric(7,2), -- BMUNRB : Unrealized Rebate
                  a_h_insurance_term_date integer, -- BMAHTDT : A&H Insurance Term Date
                  ci_insurance_term_date integer, -- BMCITDT : CI Insurance Term Date
                  le_insurance_term_date integer, -- BMLETDT : LE Insurance Term Date
                  first_payment_waiver citext) -- BMFPWV : First Payment Waiver
            """
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """
                    from stdin with csv encoding 'latin-1'""", io)
    ops.log_pass(run_id)
    print 'Passssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
