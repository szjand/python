# encoding=utf-8
import db_cnx
import ops
import csv
import datetime
import string

pg_con = None
db2_con = None
run_id = None
task = 'ext_sypffxmst_cdc'
file_name = 'files/ext_sypffxmst_cdc.csv'
# today
# table_name = ('test.ext_glptrns_' + str(datetime.date.today().strftime("%m")) +
#               str(datetime.date.today().strftime("%d")))
# yesterday: for running script past midnight, but data if for previous day
# to be sure and get all of michelle's west coast/EOM postings
# eg, run at 04/22/16 2:30 AM, table name is 0421
table_name = ('test.ext_sypffxmst_' + str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%m")) +
              str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%d")))
try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(FXMCDE), FXMCYY, TRIM(FXMACT), FXMPGE, FXMLNE, FXMCOL, FXMSTR
                from eisglobal.sypffxmst
                where trim(FXMCDE) = 'GM'
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.postgres_ubuntu() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                CREATE TABLE """ + table_name + """ (
                    FXMCDE CITEXT,
                    FXMCYY INTEGER,
                    FXMACT CITEXT,
                    FXMPGE INTEGER,
                    FXMLNE NUMERIC(6,1),
                    FXMCOL INTEGER,
                    FXMSTR INTEGER)
                WITH (OIDS=FALSE);
            """
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """
                    from stdin with csv encoding 'latin-1'""", io)
    ops.log_pass(run_id)
    print 'Passssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
