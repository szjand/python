﻿create schema jon;
comment on schema jon is 'jon''s sandbox';

-- drop table jon.pto_compli_users;
create TABLE jon.pto_compli_users(
    user_id citext NOT NULL,
    first_name citext,
    last_name citext,
    email citext,
    supervisor_id citext,
    supervisor_name citext,
    location_id citext,
    location_name citext,
    date_of_hire date,
    title citext,
    status citext,
    policy_assigned citext,
    policy_signed citext,
    training_assigned citext,
    training_completed citext,
    forms_assigned citext,
    forms_completed citext,
    forms_in_notification citext,
    exclusionary citext,
    PRIMARY KEY (user_id)) WITH (OIDS=FALSE);

drop table jon.pto_tmp_compli_users;
create TABLE jon.pto_tmp_compli_users(
    user_id citext NOT NULL,
    first_name citext,
    last_name citext,
    email citext,
    supervisor_id citext,
    supervisor_name citext,
    location_id citext,
    location_name citext,
    date_of_hire date,
    title citext,
    status citext,
    policy_assigned citext,
    policy_signed citext,
    training_assigned citext,
    training_completed citext,
    forms_assigned citext,
    forms_completed citext,
    forms_in_notification citext,
    exclusionary citext,
    drivers_license_state citext,
    drivers_license_id citext,   
    PRIMARY KEY (user_id)) WITH (OIDS=FALSE);

    


truncate jon.pto_tmp_compli_users

select * from jon.pto_tmp_compli_users

select user_id, md5((a.*)::text)
from jon.pto_compli_users a

select *
from (
  select user_id, md5((first_name,last_name,email,
      supervisor_id,supervisor_name,location_id,location_name,
      date_of_hire,title,status):: text) as hash 
  from jon.pto_compli_users a) b
inner join (
  select user_id, md5((first_name,last_name,email,
      supervisor_id,supervisor_name,location_id,location_name,
      date_of_hire,title,status):: text) as hash
  from jon.pto_tmp_compli_users a) c on b.user_id = c.user_id 
    and b.hash <> c.hash
    
-- new records
select *
from jon.pto_tmp_compli_users a
where not exists (
  select 1
  from jon.pto_compli_users
  where user_id = a.user_id);

-- deleted records
select *
from jon.pto_compli_users a
where not exists (
  select 1
  from jon.pto_tmp_compli_users
  where user_id = a.user_id);


-- changed records
select user_id, first_name,last_name,email,
  supervisor_id,supervisor_name,location_id,location_name,
  date_of_hire,title,status, 'USERS' as source
from jon.pto_compli_users 
where user_id in (
  select b.user_id
  from (
    select user_id, md5((first_name,last_name,email,
        supervisor_id,supervisor_name,location_id,location_name,
        date_of_hire,title,status):: text) as hash 
    from jon.pto_compli_users a) b
  inner join (
    select user_id, md5((first_name,last_name,email,
        supervisor_id,supervisor_name,location_id,location_name,
        date_of_hire,title,status):: text) as hash
    from jon.pto_tmp_compli_users a) c on b.user_id = c.user_id 
      and b.hash <> c.hash)
union all
select user_id, first_name,last_name,email,
  supervisor_id,supervisor_name,location_id,location_name,
  date_of_hire,title,status, 'TMP' as source
from jon.pto_tmp_compli_users
where user_id in (
  select b.user_id
  from (
    select user_id, md5((first_name,last_name,email,
        supervisor_id,supervisor_name,location_id,location_name,
        date_of_hire,title,status):: text) as hash 
    from jon.pto_compli_users a) b
  inner join (
    select user_id, md5((first_name,last_name,email,
        supervisor_id,supervisor_name,location_id,location_name,
        date_of_hire,title,status):: text) as hash
    from jon.pto_tmp_compli_users a) c on b.user_id = c.user_id 
      and b.hash <> c.hash)     
order by user_id, source      