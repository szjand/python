# initial load of data into jon.pto_compli_users from scotest
# it was necessary to create the files folder first
import adsdb
import psycopg2
import csv
pgCon = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
pgCursor = pgCon.cursor()
adsCon = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\advantage\\scotest\\sco.add',
                       userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
                       TrimTrailingSpaces='TRUE')
schema = 'jon'
adsCursor = adsCon.cursor()

adsCursor.execute("select trim(name) from system.tables where name = 'pto_compli_users'")
numrows = adsCursor.rowcount
out_file = 'files/table_data.csv'
# schema = 'dds'
for x in xrange(0, numrows):
    row = adsCursor.fetchone()
    tableName = row[0]
    tableCursor = adsCon.cursor()
    tableCursor.execute("select * from " + tableName)
    with open(out_file, 'wb') as f:
        csv.writer(f).writerows(tableCursor.fetchall())
    tableCursor.close()
    f.close()
    pgCursor = pgCon.cursor()
    pgCursor.execute("truncate " + schema + "." + tableName)
    pgCon.commit()
    io = open(out_file, 'r')
    pgCursor.copy_expert("""copy """ + schema + """.""" + tableName + """ from stdin with(format csv)""", io)
    pgCon.commit()
    io.close()
adsCursor.close()
adsCon.close()
pgCursor.close()
pgCon.close()