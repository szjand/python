# encoding=utf-8
"""
2/26/19: while looking at doind this same thing for honda, discovered that i have been generating
         flag hours for both stores and representing it as RY1, because i left out the
         where storecode = 'RY1'
3/10/20:
    honda scheduler has been shut down, replaced with DealerFX, no appointment info available
"""
import db_cnx
import smtplib
from email.mime.text import MIMEText
import datetime

body = ''
# format date as Thursday 01/04/18
subject = "Honda Nissan final checkout data for " + (datetime.date.today() - datetime.timedelta(days=1)).strftime("%A %x")
# with db_cnx.ads_service_scheduler_honda() as sched_con:
#     with sched_con.cursor() as sched_cur:
#         sql = """
#             select
#               (SELECT curdate() FROM system.iota),
#               (SELECT curtime() FROM system.iota),
#               1, 'Appointments', count(*)
#             from Appointments a
#             left outer join Advisors adv on a.AdvisorID = adv.ShortName
#             left outer join Teams t on adv.TeamID = t.TeamID
#             left outer join Departments d on t.DepartmentID = d.DepartmentID
#             where Cast(StartTime as SQL_DATE) = curdate() -1
#               and d.Description = 'Main Shop - Day'
#               and a.Status <> 'No Show' and a.Status <> 'Canceled'
#             union
#             select
#               (SELECT curdate() FROM system.iota),
#               (SELECT curtime() FROM system.iota),
#               7, 'Walk ins', count(*)
#             from Appointments a
#             left outer join Advisors adv on a.AdvisorID = adv.ShortName
#             left outer join Teams t on adv.TeamID = t.TeamID
#             left outer join Departments d on t.DepartmentID = d.DepartmentID
#             WHERE Cast(StartTime as SQL_DATE) = curdate() -1
#               and d.Description = 'Main Shop - Day'
#               AND a.Status <> 'No Show' and a.Status <> 'Canceled'
#               and a.Requisite = 'WalkIn/TowIn'
#             UNION
#             select
#               (SELECT curdate() FROM system.iota),
#               (SELECT curtime() FROM system.iota),
#               8, 'No Shows', count(*)
#             from Appointments a
#             left outer join Advisors adv on a.AdvisorID = adv.ShortName
#             left outer JOIN Teams t on adv.TeamID = t.TeamID
#             left outer join Departments d on t.DepartmentID = d.DepartmentID
#             where Cast(StartTime as SQL_DATE) = curdate() -1
#               and d.Description = 'Main Shop - Day'
#               AND a.Status = 'No Show'
#         """
#         sched_cur.execute(sql)
#         for row in sched_cur.fetchall():
#             with db_cnx.ads_dds() as save_con:
#                 with save_con.cursor() as save_cur:
#                     insert_str = ("""insert into andrew_checkout_RY2 values ('""" + str(row[0]) + "','" +
#                                   str(row[1]) + "'," + str(row[2]) + ",'" + str(row[3]) + "'," + str(row[4]) + ")")
#                     save_cur.execute(insert_str)
with db_cnx.ads_dds() as dds_con:
    with dds_con.cursor() as dds_cur:
        sql = """
            SELECT -- clockhours yesterday
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              2, 'Tech Clock Hours', SUM(clockhours) AS clockhours
            FROM edwClockHoursFact a
            INNER JOIN (
              SELECT b.employeekey
              FROM dimtech a
              inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
                AND b.active = 'active'
                AND b.currentrow = true
              WHERE a.active = true
                AND a.flagdeptcode in ('MR','QL')
                AND a.currentrow = true
                AND a.employeenumber <> 'NA'
                AND a.storecode = 'RY2'
                AND a.employeenumber <> '298543') aa on a.employeekey = aa.employeekey
            INNER JOIN day bb on a.datekey = bb.datekey
              AND bb.thedate = curdate() - 1
            UNION
            SELECT -- flaghours yesterday
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              CASE d.paymenttype
                WHEN 'customer pay' THEN 3
                WHEN 'internal' THEN 4
                WHEN 'warranty' THEN 5
                WHEN 'service contract' THEN 6
              END AS seq,
              trim (d.paymenttype) + ' Flag Hours', round(SUM(flaghours), 1)
            FROM factrepairorder a
            INNER JOIN day b on a.closedatekey = b.datekey
              AND b.datetype = 'DATE'
              AND b.thedate = curdate() -1
            INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
              AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
            INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
            where a.storecode = 'RY2'  
            GROUP by d.paymenttype
        """
        dds_cur.execute(sql)
        for row in dds_cur.fetchall():
            with db_cnx.ads_dds() as save_con:
                with save_con.cursor() as save_cur:
                    insert_str = ("""insert into andrew_checkout_RY2 values ('""" + str(row[0]) + "','" +
                                  str(row[1]) + "'," + str(row[2]) + ",'" + str(row[3]) + "'," + str(row[4]) + ")")
                    save_cur.execute(insert_str)
with db_cnx.ads_dds() as report_con:
    with report_con.cursor() as report_cur:
        sql = """
            SELECT metric, value
            FROM andrew_checkout_RY2
            WHERE the_date = curdate()
              and hour(the_time) < 12
            ORDER BY seq
        """
        report_cur.execute(sql)
        for t in report_cur.fetchall():
            body += t[0] + ': ' + str(t[1]) + '\n'
        body += '\n'

try:
    COMMASPACE = ', '
    sender = 'jandrews@cartiva.com'
    # recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
    recipients = ['jandrews@cartiva.com', 'aneumann@rydellcars.com', 'jdangerfield@gfhonda.com']
    outer = MIMEText(body)
    outer['Subject'] = subject
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    # this works with Python2
    e = smtplib.SMTP('mail.cartiva.com')
    try:
        e.sendmail(sender, recipients, outer.as_string())
    except smtplib.SMTPException:
        print "Error: unable to send email"
    e.quit()
except Exception, error:
    print error
