-- today ----------------------------------------------------------------------
-- 184/3/0
select 
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  1, 'Appointments', count(*)
-- SELECT a.advisorid,created,createdbyid,a.status,ronumber,vin,techstatus
from Appointments a
left outer join Advisors adv on a.AdvisorID = adv.ShortName
left outer join Teams t on adv.TeamID = t.TeamID
left outer join Departments d on t.DepartmentID = d.DepartmentID
where Cast(StartTime as SQL_DATE) = curdate()
  and d.Description = 'Main Shop - Day' 
  and a.Status <> 'No Show' and a.Status <> 'Canceled'
union
select 
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  7, 'Walk ins', count(*)
-- SELECT *
from Appointments a
left outer join Advisors adv on a.AdvisorID = adv.ShortName
left outer join Teams t on adv.TeamID = t.TeamID
left outer join Departments d on t.DepartmentID = d.DepartmentID
WHERE Cast(StartTime as SQL_DATE) = curdate()
  and d.Description = 'Main Shop - Day' 
  AND a.Status <> 'No Show' and a.Status <> 'Canceled'
  and a.Requisite = 'WalkIn'
UNION 
select 
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  8, 'No Shows', count(*) 
from Appointments a
left outer join Advisors adv on a.AdvisorID = adv.ShortName
left outer JOIN Teams t on adv.TeamID = t.TeamID
left outer join Departments d on t.DepartmentID = d.DepartmentID
where Cast(StartTime as SQL_DATE) = curdate()
  and d.Description = 'Main Shop - Day' 
  AND a.Status = 'No Show'

-- yesterday ----------------------------------------------------------------------
-- 142/9/0
select 
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  1, 'Appointments', count(*)
from Appointments a
left outer join Advisors adv on a.AdvisorID = adv.ShortName
left outer join Teams t on adv.TeamID = t.TeamID
left outer join Departments d on t.DepartmentID = d.DepartmentID
where Cast(StartTime as SQL_DATE) = curdate() -1
  and d.Description = 'Main Shop - Day' 
  and a.Status <> 'No Show' and a.Status <> 'Canceled'
union
select 
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  7, 'Walk ins', count(*)
from Appointments a
left outer join Advisors adv on a.AdvisorID = adv.ShortName
left outer join Teams t on adv.TeamID = t.TeamID
left outer join Departments d on t.DepartmentID = d.DepartmentID
WHERE Cast(StartTime as SQL_DATE) = curdate() -1
  and d.Description = 'Main Shop - Day' 
  AND a.Status <> 'No Show' and a.Status <> 'Canceled'
  and a.Requisite = 'WalkIn'
UNION 
select 
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  8, 'No Shows', count(*) 
from Appointments a
left outer join Advisors adv on a.AdvisorID = adv.ShortName
left outer JOIN Teams t on adv.TeamID = t.TeamID
left outer join Departments d on t.DepartmentID = d.DepartmentID
where Cast(StartTime as SQL_DATE) = curdate() -1
  and d.Description = 'Main Shop - Day' 
  AND a.Status = 'No Show'
    