drop TABLE andrew_checkout;
CREATE TABLE andrew_checkout (
  the_date date,
  the_time time,
  seq integer,
  metric cichar(30),
  value numeric(12,2));