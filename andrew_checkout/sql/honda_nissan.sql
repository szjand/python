            SELECT -- clockhours yesterday
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              2, 'Tech Clock Hours', SUM(clockhours) AS clockhours
            FROM edwClockHoursFact a
            INNER JOIN (
              SELECT b.employeekey
              FROM dimtech a
              inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
                AND b.active = 'active'
                AND b.currentrow = true
              WHERE a.active = true
                AND a.flagdeptcode = 'mr'
                AND a.currentrow = true
                AND a.employeenumber <> 'NA'
                AND a.storecode = 'RY2'
                AND a.employeenumber <> '1117960') aa on a.employeekey = aa.employeekey
            INNER JOIN day bb on a.datekey = bb.datekey
              AND bb.thedate = curdate() - 1
            UNION
            SELECT -- flaghours yesterday
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              CASE d.paymenttype
                WHEN 'customer pay' THEN 3
                WHEN 'internal' THEN 4
                WHEN 'warranty' THEN 5
                WHEN 'service contract' THEN 6
              END AS seq,
              trim (d.paymenttype) + ' Flag Hours', round(SUM(flaghours), 1)
            FROM factrepairorder a
            INNER JOIN day b on a.closedatekey = b.datekey
              AND b.datetype = 'DATE'
              AND b.thedate = curdate() -1
            INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
              AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
            INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
            where a.storecode = 'RY2'  
            GROUP by d.paymenttype
-- NOT sure which techs to include for clock hours, sent andrew a request for a list		
-- andrew sent me a screen shop of the flat rate roster	
-- this gives me that list
            SELECT -- clockhours yesterday
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              2, 'Tech Clock Hours', SUM(clockhours) AS clockhours, aa.name, aa.employeenumber
-- SELECT SUM(clockhours)              
            FROM edwClockHoursFact a
            INNER JOIN (
              SELECT b.employeekey, b.name, b.employeenumber
              FROM dimtech a
              inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
                AND b.active = 'active'
                AND b.currentrow = true
              WHERE a.active = true
                -- AND a.flagdeptcode = 'mr'
                AND a.currentrow = true
                AND a.employeenumber <> 'NA'
                AND a.storecode = 'RY2'
				AND b.employeenumber <> '298543') aa on a.employeekey = aa.employeekey
            INNER JOIN day bb on a.datekey = bb.datekey
              AND bb.thedate = curdate() - 1			
GROUP BY aa.name, aa.employeenumber			  


----------------------------------------
-- scheduler
SELECT requisite, COUNT(*)
FROM appointments
GROUP BY requisite			  
change WalkIn to WalkIn/TowIn

SELECT status, COUNT(*)
FROM appointments
GROUP BY status	

SELECT * FROM Departments
----------------------------------------
            select
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              1, 'Appointments', count(*)
            from Appointments a
            left outer join Advisors adv on a.AdvisorID = adv.ShortName
            left outer join Teams t on adv.TeamID = t.TeamID
            left outer join Departments d on t.DepartmentID = d.DepartmentID
            where Cast(StartTime as SQL_DATE) = curdate() -1
              and d.Description = 'Main Shop - Day'
              and a.Status <> 'No Show' and a.Status <> 'Canceled'
            union
            select
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              7, 'Walk ins', count(*)
            from Appointments a
            left outer join Advisors adv on a.AdvisorID = adv.ShortName
            left outer join Teams t on adv.TeamID = t.TeamID
            left outer join Departments d on t.DepartmentID = d.DepartmentID
            WHERE Cast(StartTime as SQL_DATE) = curdate() -1
              and d.Description = 'Main Shop - Day'
              AND a.Status <> 'No Show' and a.Status <> 'Canceled'
              and a.Requisite = 'WalkIn/TowIn'
            UNION
            select
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              8, 'No Shows', count(*)
            from Appointments a
            left outer join Advisors adv on a.AdvisorID = adv.ShortName
            left outer JOIN Teams t on adv.TeamID = t.TeamID
            left outer join Departments d on t.DepartmentID = d.DepartmentID
            where Cast(StartTime as SQL_DATE) = curdate() -1
              and d.Description = 'Main Shop - Day'
              AND a.Status = 'No Show'
			  
			  
select a.*, hour(the_time) 
FROM andrew_checkout_ry2 a
WHERE the_date = curdate()
and hour(the_time) < 12


            SELECT metric, value
            FROM andrew_checkout_RY2
            WHERE the_date = curdate()
              and hour(the_time) < 12
            ORDER BY seq
			
delete FROM andrew_checkout_RY2			



            SELECT -- flaghours today
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              CASE d.paymenttype
                WHEN 'customer pay' THEN 3
                WHEN 'internal' THEN 4
                WHEN 'warranty' THEN 5
                WHEN 'service contract' THEN 6
              END AS seq,
              trim (d.paymenttype) + ' Flag Hours', round(SUM(flaghours), 1)
            FROM todayfactrepairorder a
            INNER JOIN day b on a.closedatekey = b.datekey
              AND b.datetype = 'DATE'
              AND b.thedate = curdate()
            INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
              AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
            INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
            where a.storecode = 'RY2'
            GROUP by d.paymenttype