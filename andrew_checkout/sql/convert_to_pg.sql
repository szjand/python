﻿truncate dds.andrew_checkout;
-- both are right on

                    insert into dds.andrew_checkout
                    SELECT -- clockhours yesterday
                      current_date, current_time,
                      2 as seq, 'Tech Clock Hours', SUM(clock_hours) AS clock_hours
                    FROM arkona.xfm_pypclockin a
                    INNER JOIN (
                      SELECT b.pymast_employee_number
                      FROM dds.dim_tech a
                      inner JOIN arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
                        AND b.active_code <> 'T'
                      WHERE a.active 
                        AND a.flag_department_code = 'mr'
                        AND a.current_row
                        AND a.store_code = 'RY1'
                        AND a.employee_number <> '1117960') aa on a.employee_number = aa.pymast_employee_number
                    INNER JOIN dds.dim_Date bb on a.the_date = bb.the_date
                      AND bb.the_date = current_date - 1
                    union
                    SELECT -- flaghours yesterday
                      current_date,
                      current_time,
                      CASE d.payment_type
                        WHEN 'customer pay' THEN 3
                        WHEN 'internal' THEN 4
                        WHEN 'warranty' THEN 5
                        WHEN 'service contract' THEN 6
                      END AS seq,
                      trim (d.payment_type) || ' Flag Hours', round(SUM(flag_hours), 1)
                    FROM dds.fact_repair_order a
                    INNER JOIN dds.dim_date b on a.close_date = b.the_date
                      AND b.date_type = 'DATE'
                      AND b.the_date = current_Date -1
                    INNER JOIN dds.dim_Service_type c on a.service_type_key = c.service_type_key
                      AND c.service_type_code IN ('AM','CM','EM','MN','MR','SV')
                    INNER JOIN dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
                      AND d.payment_type_key IN (2,3,4,5)
                    where a.store_code = 'RY1'  
                    GROUP by d.payment_type   


            SELECT metric, value
            FROM dds.andrew_checkout
            WHERE the_date = current_date
              and extract(hour from the_time) > 12
            ORDER BY seq                    