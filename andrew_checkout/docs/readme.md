combines data from the service scheduler (\10.130.196.83:2001\advantage\rydellservice\rydellservice.add)
with data from dds (\\67.135.158.12:6363\advantage\DDS\DDS.add)

Appointments: 162.00
Tech Clock Hours: 140.45
Customer Pay Flag Hours: 25.10
Internal Flag Hours: 1.20
Warranty Flag Hours: 8.00
Service Contract Flag Hours: 25.10
Walk ins: 1.00
No Shows: 0.00

which is sent to Andrew Neumann (service director) twice a day.
at 5PM with current day data
at 5AM with totals from the previous day

extracted data is stored in a table dds.andrew_checkout.