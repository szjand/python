﻿select 'fact', a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
  d.thedate, a.control, a.doc, a.ref, e.account, a.amount
from fin.fact_gl a 
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.day d on a.date_key = d.datekey
inner join fin.dim_account e on a.account_key = e.account_key
where  trans = 3478713 and seq = 4
union
select 'glptrns', gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, 
  gtdate, gtctl_, gtdoc_, gtref_, gtacct, gttamt
from dds.ext_glptrns_2016 
where gttrn_ = 3478713 and gtseq_ = 4
order by trans, seq


-- same thing for new_fact_gl and daily cdc
select 'fact', a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
  d.thedate, a.control, a.doc, a.ref, e.account, a.amount
from fin.new_fact_gl a 
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.day d on a.date_key = d.datekey
inner join fin.dim_account e on a.account_key = e.account_key
where  trans = 3478713 and seq = 4
union
select 'glptrns', gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, 
  gtdate, gtctl_, gtdoc_, gtref_, gtacct, gttamt
from _1210
where gttrn_ = 3478713 and gtseq_ = 4
order by trans, seq


select *
from fin.dim_journal

update fin.dim_doc_type
set doc_type_code = upper(doc_type_code)


select * from dds.ext_glptrns_2016 where gtpost = ' ' limit 100

select * from fin.fact_gl a where post_status = '' limit 100

select * from dds.ext_glptrns_2016 where gtpost = 'V' and gtacct <> '*VOID'

select gtacct, count(*) from dds.ext_glptrns_2016 where gtacct like '*%' group by gtacct

select * from dds.ext_glptrns_2016 where gtacct like '*E*'

select gtacct, count(*) from dds.ext_glptrns_2016 group by gtacct order by gtacct

select * from dds.ext_glptrns_2016 where gtacct is null

select * from fin.dim_Account where account = '3319900'

-- account not in glpmast
select *
from dds.ext_glptrns_2016 a
where gtacct not like '*%'
and not exists (
  select 1
  from fin.dim_account
  where account = a.gtacct)

-- wtf they all look like crookston accounts
-- all with no balances, looks like correcting offsetting entries
select gtacct, gtdoc_, sum(gttamt)  
from dds.ext_glptrns_2016 a
where gtacct not like '*%'
and not exists (
  select 1
  from fin.dim_account
  where account = a.gtacct)
group by gtacct, gtdoc_


select count(*) -- 37,1493 (of 13,563,990 rows)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where b.account = 'none'

insert into fin.dim_account(account,account_type_code,account_type,description,store_code,
  store,department_code,department,typical_balance,current_row,row_from_date,
  row_thru_date,row_reason)
select '*SPORD',account_type_code,account_type,description,store_code,
  store,department_code,department,typical_balance,current_row,row_from_date,
  row_thru_date,row_reason
from fin.dim_account 
where account = 'none'    

select a.*
from fin.fact_gl a
inner join (
select trans, seq, post_status
from fin.fact_gl
group by trans, seq, post_status
having count(*) > 1) b on a.trans = b.trans and a.seq = b.seq and a.post_status = b.post_status

select *
from dds.ext_glptrns_2016 a
inner join (
select gttrn_, gtseq_, gtpost
from dds.ext_glptrns_2016
group by gttrn_, gtseq_, gtpost
having count(*) > 1) b on a.gttrn_ = b.gttrn_ and a.gtseq_ = b.gtseq_ and a.gtpost = b.gtpost

-- bingo, group on the only fields i really need, and trans/seq/gtpost become unique
select gttrn_, gtseq_, gtpost
from (
  select gttrn_, gtseq_, gtpost, gtjrnl, gtdate, gtacct, gtctl_, gtdoc_, gtref_, gttamt
  from dds.ext_glptrns_2016
  group by gttrn_, gtseq_, gtpost, gtjrnl, gtdate, gtacct, gtctl_, gtdoc_, gtref_, gttamt) a
group by gttrn_, gtseq_, gtpost 
having count(*) > 1

    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount
    from fin.new_fact_gl a 
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
where trans = 3511473 and seq = 1  

select * from fin.new_fact_gl limit 100

select * from fin.dim_account order by account

select * from dds.ext_glptrns_2016 a
left join fin.dim_account b on a.gtacct = b.account
 where gttrn_ = 3511473

