﻿drop table if exists fin.dim_fs_org cascade;
create table fin.dim_fs_org (
  fs_org_key serial primary key,
  market citext not null,
  store citext not null,
  area citext not null,
  department citext not null,
  sub_department citext not null default 'none',
constraint dim_fs_org_nk unique ( market,store,area,department,sub_department)); 
insert into fin.dim_fs_org (market,store,area,department,sub_department) values
  ('grand forks','ry1','variable','sales','new'),  
  ('grand forks','ry1','variable','sales','used'),
  ('grand forks','ry1','variable','finance','new'),
  ('grand forks','ry1','variable','finance','used'),
  ('grand forks','ry1','fixed','body shop','none'),
  ('grand forks','ry1','fixed','parts','none'),
  ('grand forks','ry1','fixed','service','mechanical'),
  ('grand forks','ry1','fixed','service','detail'),
  ('grand forks','ry1','fixed','service','pdq'),
  ('grand forks','ry1','fixed','service','car wash'),
  ('grand forks','ry2','variable','sales','new'),  
  ('grand forks','ry2','variable','sales','used'),
  ('grand forks','ry2','variable','finance','new'),
  ('grand forks','ry2','variable','finance','used'),
  ('grand forks','ry2','fixed','parts','none'),
  ('grand forks','ry2','fixed','service','mechanical'),
  ('grand forks','ry2','fixed','service','pdq'),
  ('none','none','none','none','none');