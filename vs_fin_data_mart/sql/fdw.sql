﻿/*
12/31/16
  not sure the schema pg_cartiva is necessary
  ok, in pgadmin file->options->browser-display, selected Foreign Data Wrappers, Foreign Servers,
    User Mappings and Foreign Tables
  now, the the table shows up under foreign_tables in schema from_pg_cartiva
  i like this separation, foreign tables are in a specific schema.
  this is good
*/  
create extension postgres_fdw;

create server pg_cartiva 
  foreign data wrapper postgres_fdw 
  options (host '10.130.196.173', port '5432', dbname 'cartiva');

create user mapping for postgres
  server pg_cartiva
  options (password '1rydell$');

create schema from_pg_cartiva;  

import foreign schema dds limit to (day)
  from server pg_cartiva into from_pg_cartiva;

-- drop user mapping for postgres server pg_cartiva  

insert into dds.day
select * from from_pg_cartiva.day


import foreign schema arkona limit to (ext_glpdept, ext_glpdept_tmp, ext_glpjrnd, ext_glpjrnd_tmp,
  ext_glpmast, ext_glptrns_tmp, xfm_glpmast, xfm_glptrns)
  from server pg_cartiva into from_pg_cartiva;

insert into arkona.ext_glpdept
select * from from_pg_cartiva.ext_glpdept;
insert into arkona.ext_glpdept_tmp
select * from from_pg_cartiva.ext_glpdept_tmp;
insert into arkona.ext_glpjrnd
select * from from_pg_cartiva.ext_glpjrnd;
insert into arkona.ext_glpjrnd_tmp
select * from from_pg_cartiva.ext_glpjrnd_tmp;
insert into arkona.ext_glpmast
select * from from_pg_cartiva.ext_glpmast;
insert into arkona.ext_glptrns_tmp
select * from from_pg_cartiva.ext_glptrns_tmp;
insert into arkona.xfm_glpmast
select * from from_pg_cartiva.xfm_glpmast;
insert into arkona.xfm_glptrns
select * from from_pg_cartiva.xfm_glptrns;


import foreign schema fin limit to (dim_account, dim_doc_type, dim_gl_description, 
  dim_journal, fact_gl)
  from server pg_cartiva into from_pg_cartiva;

insert into fin.dim_account
select * from from_pg_cartiva.dim_account;  

insert into fin.dim_doc_type
select * from from_pg_cartiva.dim_doc_type; 

insert into fin.dim_gl_description
select * from from_pg_cartiva.dim_gl_description; 

insert into fin.dim_journal
select * from from_pg_cartiva.dim_journal; 

insert into fin.fact_gl
select a.* 
from from_pg_cartiva.fact_gl a
inner join from_pg_cartiva.day b on a.date_key = b.datekey
  and b.thedate > '10/31/2016';






