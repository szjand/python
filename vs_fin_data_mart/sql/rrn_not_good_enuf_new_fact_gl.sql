﻿/*
-- drop rrn
-- unique index on trans/seq/post_status
drop table fin.fact_gl cascade;
CREATE TABLE fin.fact_gl
(
  trans bigint NOT NULL,
  seq integer NOT NULL,
  doc_type_key integer NOT NULL,
  post_status citext NOT NULL,
  journal_key integer NOT NULL,
  date_key integer NOT NULL,
  account_key integer NOT NULL,
  control citext NOT NULL,
  doc citext NOT NULL,
  ref citext NOT NULL,
  amount numeric(12,2) NOT NULL,
  CONSTRAINT fact_gl_account_key_fkey FOREIGN KEY (account_key)
      REFERENCES fin.dim_account (account_key) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fact_gl_date_key_fkey FOREIGN KEY (date_key)
      REFERENCES dds.day (datekey) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fact_gl_doc_type_key_fkey FOREIGN KEY (doc_type_key)
      REFERENCES fin.dim_doc_type (doc_type_key) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fact_gl_journal_key_fkey FOREIGN KEY (journal_key)
      REFERENCES fin.dim_journal (journal_key) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fact_gl_pkey primary key (trans,seq,post_status)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fin.fact_gl
  OWNER TO rydell;

-- Index: fin.fact_gl_account_key_idx

-- DROP INDEX fin.fact_gl_account_key_idx;

CREATE INDEX fact_gl_account_key_idx
  ON fin.fact_gl
  USING btree
  (account_key);

-- Index: fin.fact_gl_control_idx

-- DROP INDEX fin.fact_gl_control_idx;

CREATE INDEX fact_gl_control_idx
  ON fin.fact_gl
  USING btree
  (control COLLATE pg_catalog."default");

-- Index: fin.fact_gl_date_key_idx

-- DROP INDEX fin.fact_gl_date_key_idx;

CREATE INDEX fact_gl_date_key_idx
  ON fin.fact_gl
  USING btree
  (date_key);

-- Index: fin.fact_gl_doc_type_key_idx

-- DROP INDEX fin.fact_gl_doc_type_key_idx;

CREATE INDEX fact_gl_doc_type_key_idx
  ON fin.fact_gl
  USING btree
  (doc_type_key);

-- Index: fin.fact_gl_journal_key_idx

-- DROP INDEX fin.fact_gl_journal_key_idx;

CREATE INDEX fact_gl_journal_key_idx
  ON fin.fact_gl
  USING btree
  (journal_key);

-- Index: fin.fact_gl_post_status_idx

-- DROP INDEX fin.fact_gl_post_status_idx;

CREATE INDEX fact_gl_post_status_idx
  ON fin.fact_gl
  USING btree
  (post_status COLLATE pg_catalog."default");
*/

-- 12/17/16 add dim_gl_description, gl_description_key
1. add the attribute to fact_gl
2. update fact_gl with gl_description_key
3. add not null, FK constraints to fact_gl
4. add index to fact_gl

1. 
alter table fin.fact_gl
add column gl_description_key integer;
2. 
see dim_gl_description.sql
3. 
select b.thedate, a.* from fin.fact_gl a inner join dds.day b on a.date_key = b.datekey where gl_description_key is null
ALTER TABLE fin.fact_gl ALTER COLUMN gl_description_key SET NOT NULL;
ALTER TABLE fin.fact_gl ADD FOREIGN KEY (gl_description_key) REFERENCES fin.dim_gl_description;
4.
create index on fin.fact_gl(gl_description_key);

-- exclude not posted, crookston accounts, gtco <> RY1
-- ok, there are some (12 ish) dups for each year, fuck it, i am going to exclude them
-- truncate fin.fact_gl;
insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
  account_key,control,doc,ref,amount)
-- 2011: 2160354 3:03
-- 2012: 2099202 3:09
-- 2013: 2182840 3:24
-- 2014: 2235834 3:34
-- 2015: 2364148: 3:46
select e.gttrn_, e.gtseq_, coalesce(f.doc_type_key, ff.doc_type_key),
  coalesce(e.gtpost, ''),
  coalesce(g.journal_key, gg.journal_key),
  h.datekey,
  coalesce(i.account_key, j.account_key) as account_key,
  e.gtctl_, e.gtdoc_,
  coalesce(e.gtref_, ''),
  e.gttamt
from (
  select gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, gtacct, gtctl_, gtdoc_, gtref_, gttamt
--   from dds.z_unused_ext_glptrns_2011
--   from dds.z_unused_ext_glptrns_2012
--   from dds.z_unused_ext_glptrns_2013
--   from dds.z_unused_ext_glptrns_2014
--   from dds.z_unused_ext_glptrns_2015
  from dds.ext_glptrns_2016
  where gtco_ = 'RY1'
    and gtpost in ('V','Y')
    and left(gtacct, 1) <> '3'
  group by gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, gtacct, gtctl_, gtdoc_, gtref_, gttamt) e
left join (
  select gttrn_, gtseq_, gtpost
  from (
    select gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, gtacct, gtctl_, gtdoc_, gtref_, gttamt
    from dds.ext_glptrns_2016
    where gtco_ = 'RY1'
      and gtpost in ('V','Y')
      and left(gtacct, 1) <> '3'
   group by gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, gtacct, gtctl_, gtdoc_, gtref_, gttamt) x
  group by gttrn_, gtseq_, gtpost
  having count(*) > 1) ee on e.gttrn_ = ee.gttrn_ and e.gtseq_ = ee.gtseq_ and e.gtpost = ee.gtpost  
left join fin.dim_doc_type f on e.gtdtyp = f.doc_type_code
  and e.gtdate between f.row_from_date and f.row_thru_date
left join fin.dim_doc_type ff on 1 = 1
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.gtjrnl = g.journal_code
  and e.gtdate between g.row_from_date and g.row_thru_date
left join fin.dim_journal gg on 1 = 1
  and gg.journal_code = 'none'
left join dds.day h on e.gtdate = h.thedate
left join fin.dim_account i on e.gtacct = i.account
  and e.gtdate between i.row_from_date and i.row_thru_date
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
where ee.gttrn_ is null;  

-- -- --exclude non RY1, non posted, 3... accounts, coalesce journal to none
-- -- drop table if exists _1210;
-- -- create temp table _1210 as
-- -- -- Query returned successfully: 305374 rows affected, 829 msec execution time
-- -- select gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none') as gtjrnl, gtdate, gtacct, 
-- --   gtctl_, gtdoc_, gtref_, gttamt::numeric(12,2)
-- -- from fin.ext_glptrns_1210
-- -- where gtco_ = 'RY1'
-- --   and gtpost in ('Y','V')
-- --   and left(gtacct, 1) <> '3'
-- -- group by gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none'), gtdate, gtacct, gtctl_, gtdoc_, gtref_, gttamt;
-- -- 
-- -- select * from _1210 limit 100
-- -- 
-- -- select a.*, md5(a::text) as hash
-- -- from (
-- --   select gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none') as gtjrnl, gtdate, gtacct, 
-- --     gtctl_, gtdoc_, gtref_, gttamt::numeric(12,2)
-- --   from fin.ext_glptrns_1210
-- --   where gtco_ = 'RY1'
-- --     and gtpost in ('Y','V')
-- --     and left(gtacct, 1) <> '3'
-- --   group by gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none'), gtdate, gtacct, gtctl_, gtdoc_, gtref_, gttamt) a 

-- match field names to fact_gl joined to dimensions

create table arkona.xfm_glptrns (
  trans bigint not null,
  seq integer not null, 
  doc_type_code citext not null,
  post_status citext not null,
  journal_code citext not null,
  the_date date not null,
  account citext not null,
  control citext not null,
  doc citext not null,
  ref citext not null,
  amount numeric(12,2),
  hash text not null,
  constraint xfm_glptrns_pk primary key (trans,seq,post_status));

truncate arkona.xfm_glptrns;
insert into arkona.xfm_glptrns
select a.*, md5(a::text) as hash
from (
  select gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none') as gtjrnl, gtdate, gtacct, 
    gtctl_, gtdoc_, gtref_, gttamt::numeric(12,2)
  from arkona.ext_glptrns_tmp
  where gtco_ = 'RY1'
    and gtpost in ('Y','V')
    and left(gtacct, 1) <> '3'
  group by gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none'), gtdate, gtacct, gtctl_, gtdoc_, gtref_, gttamt) a;
    
-- new rows
select *
from arkona.xfm_glptrns a
left join fin.fact_gl b on a.trans = b.trans
  and a.seq = b.seq
  and a.post_status = b.post_status
where b.trans is null


insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
  account_key,control,doc,ref,amount)
select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
  e.post_status,
  coalesce(g.journal_key, gg.journal_key),
  h.datekey,
  coalesce(i.account_key, j.account_key) as account_key,
  e.control, e.doc,
  coalesce(e.ref, ''),
  e.amount
from arkona.xfm_glptrns e 
left join fin.fact_gl ee on e.trans = ee.trans
  and e.seq = ee.seq
  and e.post_status = ee.post_status
left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
  and e.the_date between f.row_from_date and f.row_thru_date
left join fin.dim_doc_type ff on 1 = 1
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.journal_code = g.journal_code
  and e.the_date between g.row_from_date and g.row_thru_date
left join fin.dim_journal gg on 1 = 1
  and gg.journal_code = 'none'
left join dds.day h on e.the_date = h.thedate
left join fin.dim_account i on e.account = i.account
  and e.the_date between i.row_from_date and i.row_thru_date
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
where ee.trans is null;  

-- changed rows
select *
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount
    from fin.fact_gl a 
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key) a) f on e.trans = f.trans 
      and e.seq = f.seq 
      and e.post_status = f.post_status
      and e.hash <> f.hash;

insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
  account_key,control,doc,ref,amount,gl_description_key)
-- something different in this fact, instead of joining to dimensions "none" row and
-- coalescing, the raw data, in this case arkona.xfm_glptrns has already done (journal & description)
-- the coalescing
select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
  e.post_status, g.journal_key, h.datekey,
  coalesce(i.account_key, j.account_key) as account_key,
  e.control, e.doc,
  coalesce(e.ref, ''),
  e.amount, m.gl_description_key
from arkona.xfm_glptrns e 
left join fin.fact_gl ee on e.trans = ee.trans
  and e.seq = ee.seq
  and e.post_status = ee.post_status
left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
  and e.the_date between f.row_from_date and f.row_thru_date
left join fin.dim_doc_type ff on 1 = 1
  and ff.doc_type_code = 'none'
left join fin.dim_journal g on e.journal_code = g.journal_code
  and e.the_date between g.row_from_date and g.row_thru_date
left join fin.dim_journal gg on 1 = 1
  and gg.journal_code = 'none'
left join dds.day h on e.the_date = h.thedate
left join fin.dim_account i on e.account = i.account
  and e.the_date between i.row_from_date and i.row_thru_date
left join fin.dim_account j on 1 = 1
  and j.account = 'none'
left join fin.xfm_gl_description k on e.trans = k.trans
  and e.seq = k.seq
  and e.post_status = k.post_status 
left join fin.dim_gl_description m on k.description = m.description   
where ee.trans is null;  

-- 12/19 changed rows
-- need to include description
-- EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
select *
select count(*)
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from fin.fact_gl a 
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans and a.seq = aa.seq and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f on e.trans = f.trans 
      and e.seq = f.seq 
      and e.post_status = f.post_status
      and e.hash <> f.hash
order by e.trans      

-- 12/19, ok got some differences
but it is only case on description
eg; 'Invoice for PO 04 SATURN' vs 'INVOICE FOR PO 04 SATURN'
which generates different md5
i do not care about this, so upper the description
do it in the generation of in.xfm_gl_description


select *
from arkona.ext_glptrns_tmp
where gttrn_ = 3547630 a

update fin.dim_gl_description
set description = upper(description)

select * from fin.fact_gl where row_from_date = current_date

      
-- deleted rows ????
-- ok, i will go out on a limb, since i can't fucking figure out how to detect
-- deleted rows, i will assume there are none



