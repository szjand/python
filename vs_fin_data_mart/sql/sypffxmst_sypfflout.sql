﻿2/1/17
-- sypffxmst ------------------------------------------------------------------
select *
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst d
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp e)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp f
    except
    select *
    from arkona.ext_eisglobal_sypffxmst g)) x

select 'archive' as source, a.*
from arkona.ext_eisglobal_sypffxmst a
where fxmcyy = 2017
  and fxmpge = 16
  and fxmlne in (3,6)
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypffxmst_tmp a
where fxmcyy = 2017
  and fxmpge = 16
  and fxmlne in (3,6)
order by fxmlne, fxmcol 

looks like line 3 col 2 & 4, line 6 col 2 & 4 get deleted

select *
from arkona.ext_eisglobal_sypffxmst a
left join arkona.ext_eisglobal_sypffxmst_tmp b on a.fxmcyy = b.fxmcyy
  and a.fxmpge = b.fxmpge
  and a.fxmlne = b.fxmlne
  and a.fxmcol = b.fxmcol
where b.fxmpge is null

delete 
-- select *
from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2017 
  AND fxmpge = 16 
  and fxmlne in (3.0, 6.0)
  and fxmcol in (2,4)
  
-- sypfflout ------------------------------------------------------------------  
select *
from ((
    select *
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select *
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select *
    from arkona.ext_eisglobal_sypfflout f
    except
    select *
    from arkona.ext_eisglobal_sypfflout_tmp g)) x

|MEMO RECON PER CAR   RETAIL CERT    |#1    ||#2      |  |#3      |  |#4   | GM CERT  |#6      |   |OTHER |   |#8   |  
|MEMO RECON PER CAR   RETAIL CERT    |CERTIFY|#2      |  | OTHER  |  |#4   | GM CERT  |#6      |   |OTHER |   |#8   |                  

#1=1.647A.Y.>TCRY #2=2.<TCR/$1$1.M #3=3.647B.Y.>TRCY #4=4.<TRC/$2$1.M #6=6.<TCRY/$1$5.Y #8=8.<TRCY/$2$5.Y
#2=2.647A.Y.>TCRY #4=4.647B.Y.>TRCY #6=6.<TCRY/$1$5.Y #8=8.<TRCY/$2$5.Y


select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and flpage = 16
  and flflne in (3,6)
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and flpage = 16
  and flflne in (3,6)
order by flflne

-- data not currently being used for 2017 page 16 lines 3 & 6 changed
update arkona.ext_eisglobal_sypfflout a
set fldata = x.fldata,
    flcont = x.flcont
from (
  select *
  from arkona.ext_eisglobal_sypfflout_tmp
  where flcyy = 2017
    and flpage = 16
    and flflne in (3,6)) x
where a.flcyy = x.flcyy
  and a.flpage = x.flpage
  and a.flflne = x.flflne    

  
      