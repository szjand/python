# encoding=utf-8
import db_cnx
file_name = 'files/REPORT_12501.csv'
with db_cnx.pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        pg_cur.execute("truncate fin.tmp_12501  ")
        with open(file_name, 'r') as io:
            pg_cur.copy_expert("""copy fin.tmp_12501   from stdin with csv encoding 'latin-1 '""", io)