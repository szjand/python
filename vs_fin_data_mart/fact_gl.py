# encoding=utf-8
import db_cnx
import ops
import string

task = 'fact_gl'
pg_con = None
run_id = None
file_name = 'files/ext_glptrns_tmp.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()

    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:

            """
                insert new rows into fin.fact_gl
                1/7/17 joins on trans, seq and post_status, so, if a trans/seq

                struggling with voids, (still)
                trans 3555016, 3535418: arkona.xfm_glptrns matches arkona
                looks like instead of updating rows in fin.fact_gl, rows are being added
                so the assumption is that one day the row exists as post_status = Y
                subsequently, the post_status changes to V and a new row gets added to fin.fact_gl
                yep, looking at cdc for 3555016
                on 12/24 seq 1-4 all Y
                on 12/26 seq 1-4 all V
                so, change the new row query to join only on trans/seq
                deal with changes below
                ********** don't fucking forget, version of these files in luigi *********
                also, start a nightly fs account balance check
            """
            sql = """
                insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
                  account_key,control,doc,ref,amount,gl_description_key)
                select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
                  e.post_status, g.journal_key, h.datekey,
                  coalesce(i.account_key, j.account_key) as account_key,
                  e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
                from arkona.xfm_glptrns e
                left join fin.fact_gl ee on e.trans = ee.trans
                  and e.seq = ee.seq
                  -- and e.post_status = ee.post_status
                left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
                  and e.the_date between f.row_from_date and f.row_thru_date
                left join fin.dim_doc_type ff on 1 = 1
                  and ff.doc_type_code = 'none'
                left join fin.dim_journal g on e.journal_code = g.journal_code
                  and e.the_date between g.row_from_date and g.row_thru_date
                left join dds.day h on e.the_date = h.thedate
                left join fin.dim_account i on e.account = i.account
                  and e.the_date between i.row_from_date and i.row_thru_date
                left join fin.dim_account j on 1 = 1
                  and j.account = 'none'
                left join fin.xfm_gl_description k on e.trans = k.trans
                  and e.seq = k.seq
                  and e.post_status = k.post_status
                left join fin.dim_gl_description m on k.description = m.description
                where ee.trans is null;
            """
            pg_cur.execute(sql)
            # pg_con.commit()

            """
               check for changed rows, if there are any, send an email
               1/7/17 i think i am doing voids wrong, a row can change from Y to V and this will
               not pick it up because of the join on post_status
               remove that join, the change will be picked up by the hash
            """
            sql = """
                select count(*)
                from arkona.xfm_glptrns e
                inner join (
                  select 'fact', trans, seq, post_status, md5(a::text) as hash
                  from (
                    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
                      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
                    from fin.fact_gl a
                    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
                      and a.seq = aa.seq and a.post_status = aa.post_status
                    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
                    inner join fin.dim_journal c on a.journal_key = c.journal_key
                    inner join dds.day d on a.date_key = d.datekey
                    inner join fin.dim_account e on a.account_key = e.account_key
                    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
                      on e.trans = f.trans
                        and e.seq = f.seq
                        -- and e.post_status = f.post_status
                        and e.hash <> f.hash
            """
            pg_cur.execute(sql)
            the_count = pg_cur.fetchone()[0]
            if the_count != 0:
                raise Exception('fact_gl has some changed rows')
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    ops.email_error(task, run_id, error)
    print error
finally:
    if pg_con:
        pg_con.close()
