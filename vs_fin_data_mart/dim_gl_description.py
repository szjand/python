# encoding=utf-8
import db_cnx
import ops
import string

task = 'dim_gl_description'
pg_con = None
run_id = None

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate fin.xfm_gl_description")
            sql = """
                insert into fin.xfm_gl_description
                select trans, seq, post_status, description
                from arkona.xfm_glptrns
            """
            pg_cur.execute(sql)
            pg_con.commit()
            sql = """
                insert into fin.dim_gl_description (description, row_from_date, current_row, row_reason)
                select distinct description, current_date, true, 'added from nightly etl'
                from fin.xfm_gl_description a
                where not exists (
                  select 1
                  from fin.dim_gl_description
                  where description = a.description);
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    ops.email_error(task, run_id, error)
    print error
finally:
    if pg_con:
        pg_con.close()
