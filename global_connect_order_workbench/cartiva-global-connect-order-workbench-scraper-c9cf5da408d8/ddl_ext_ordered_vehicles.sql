
CREATE TABLE gmgl.ext_ordered_vehicles (
	order_number public."citext" NULL,
	field_action citext,
	trade public."citext" NULL,
	assigned public."citext" NULL,
	gm_config_id public."citext" NULL,
	order_type public."citext" NULL,
	msrp_dfc public."citext" NULL,
	dan public."citext" NULL,
	model public."citext" NULL,
	tpw public."citext" NULL,
	est_delivery_date public."citext" NULL,
	vin public."citext" NULL,
	age_of_inventory public."citext" NULL,
	primary_color public."citext" NULL,
	trim_value public."citext" NULL,
	changed public."citext" NULL,
	stock_number public."citext" NULL,
	peg public."citext" NULL,
	alloc_group public."citext" NULL,
	ship_to_bac public."citext" NULL,
	ship_to_bfc public."citext" NULL,
	current_event public."citext" NULL,
	current_event_date public."citext" NULL,
	ordered_options public."citext" NULL,
	customer_name public."citext" NULL,
	gm_stored_config_description public."citext" null,
	model_year citext, 
	divison citext,
	distribution_entity citext,
	deliver_vehicle citext, 
	lookup_incentives_by_vin citext
);
