
CREATE OR REPLACE FUNCTION gmgl.ext_orders(_make citext)
 RETURNS void
 LANGUAGE sql
AS $body$
/*
12/22/2020
  *a*: gmgl.ext_ordered_vehicles.est_delivery_date can now have a value of DELAYED, if that is the case, replace it with null
02/17/21
    added MP to possible nullable values of est_delivery_date, 9/29 added 'XH' *** note needs to be done in 2 places
05/2023
	Order workbench has been completely redone and the data we download has changed slightly. 
*/

-----------ORDERS------------
insert into gmgl.vehicle_orders
select order_number, order_type, 
  case when msrp_dfc = 'Â ' then null else msrp_dfc end, 
  case when tpw = 'Â ' then null else tpw::date end, 
  case when vin = 'Â ' then null else vin end, 
  case when age_of_inventory = 'Â ' then null else age_of_inventory::integer end, 
  model_year::integer, 
  alloc_group, peg, primary_color, case when _make = 'CHEVROLET ALL' then 'CHEVROLET' else _make end, model, trade, 
  case when assigned = 'Â ' then null else assigned end, 
  case when  gm_config_id = 'Â ' then null else  gm_config_id end, dan, 
  -- *a*
  case when est_delivery_date in ( 'Â ','DELAYED','MP','XH') then null else est_delivery_date::date end,
  null as secondary_color, -- SECONDARY COLOR not PROVIDED ANYMORE
  case when  stock_number = 'Â ' then null else  stock_number end, peg, 
  null as primary_fan, -- PRIMARY FAN NOT PROVIDED ANYMORE,
  null as end_user_fan, -- END USER FAN NOT PROVIDED ANYMORE,
  null as po_number, -- PO NUMBER NOT PROVIDED ANYMORE , 
  ship_to_bac, ship_to_bfc, 
  null as request_id, -- REQUEST ID NOT PROVIDED ANYMORE, 
  case when  customer_name = 'Â ' then null else  customer_name end, gm_stored_config_description
from gmgl.ext_ordered_vehicles
where order_number not in (select order_number from gmgl.vehicle_orders);

update gmgl.vehicle_orders a
set model_code = model,
  vin = case when b.vin = 'Â ' then null else b.vin end,
  color = primary_color, 
  alloc_group = b.alloc_group,
  model_year = b.model_year::integer,
  msrp_dfc = b.msrp_dfc,
  tpw = case when b.tpw = 'Â ' then null else b.tpw::date end,
  age_of_inventory = case when b.age_of_inventory = 'Â ' then null else b.age_of_inventory::integer end,
  peg = b.peg, 
  trade = b.trade, 
  assigned = case when b.assigned = 'Â ' then null else b.assigned end,
  gm_config_id = case when  b.gm_config_id = 'Â ' then null else  b.gm_config_id end, 
  dan = b.dan, 
  -- *a*
  est_delivery_date = case when b.est_delivery_date in ('Â ', 'DELAYED', 'MP', 'XH') then null else b.est_delivery_date::date end,
  stock_number =case when  b.stock_number = 'Â ' then null else  b.stock_number end, 
  ship_to_bac = b.ship_to_bac, 
  ship_to_bfc = b.ship_to_bfc,
  customer_name = case when  b.customer_name = 'Â ' then null else  b.customer_name end
from gmgl.ext_ordered_vehicles b
where a.order_number = b.order_number;


-----------CODES-------------
update gmgl.vehicle_order_options a
set thru_ts = now()
from (select a.order_number, option_code
from gmgl.vehicle_order_options a
inner join gmgl.ext_ordered_vehicles b on a.order_number = b.order_number
where not exists (
select unnest(concat('{',ordered_options,'}')::citext[]) as ordered_option
from  gmgl.ext_ordered_vehicles b
where a.order_number = b.order_number
and a.option_code in (select  unnest(concat('{',ordered_options,'}')::citext[]))
)) b
where b.order_number = a.order_number
and a.option_code = b.option_code;

insert into gmgl.vehicle_order_options
select a.order_number, a.ordered_option, now(), '12/31/9999'
from (
select order_number, unnest(concat('{',ordered_options,'}')::citext[]) as ordered_option
from gmgl.ext_ordered_vehicles a ) a
left join gmgl.vehicle_order_options b on a.order_number = b.order_number and a.ordered_option = b.option_code
where b.option_code is null;

-----------EVENTS-------------
-- when the current event in vehicle_order_events <> to the
-- current event in ext_ordered_vehicles, 
-- update vehicle_order_events.thru_date to current_date
update gmgl.vehicle_order_events a
set thru_date = current_date
from gmgl.ext_ordered_vehicles b
where a.order_number = b.order_number 
and a.event_code <> b.current_event
and a.thru_date = '12/31/9999' ;

-- update vehicle_order_events.thru_date to current_date
-- when the order number exists in vehicle_orders 
-- and the order number does not exist in ext_ordered_vehicles
-- and the vehicle_order_events.event code = 5000 and vehicle_order_events.thru_date = 12/31/9999
update gmgl.vehicle_order_events a
set thru_date = current_date 
from  gmgl.vehicle_orders b 
where a.order_number = b.order_number 
  and make = case when _make = 'CHEVROLET ALL' then 'CHEVROLET' else _make end
  and a.order_number not in (
    select order_number
    from gmgl.ext_ordered_vehicles)
  and event_code = '5000'
  and thru_date = '12/31/9999';

/*06/28 problem brought up by jon.
A vehicle can be in a status multiple times. */

insert into gmgl.vehicle_order_events 
select order_number, current_event, current_event_date::Date, '12/31/9999'
from gmgl.ext_ordered_vehicles b
where not exists (
  select * 
  from gmgl.vehicle_order_events aa
  where aa.order_number = b.order_number 
  and aa.event_code = b.current_event
  and aa.thru_date = '12/31/9999'  );

insert into gmgl.vehicles_no_longer_in_global
select a.order_number, a.vin, a.event_code, _make
from (
  select b.vin, a.event_code,a.order_number
  from gmgl.vehicle_order_events a
  inner join gmgl.vehicle_orders b on a.order_number = b.order_number
  inner join gmgl.vehicle_event_codes c on a.event_code = c.code
  where make::citext = case when _make = 'CHEVROLET ALL' then 'CHEVROLET' else _make end
    and a.thru_date > now()) A
left join gmgl.ext_ordered_vehicles b on a.order_number = b.order_number
where b.order_number is null
  and not exists (
    select 1
    from gmgl.vehicles_no_longer_in_global
    where order_number = a.order_number
      and vin = a.vin
      and event_code = a.event_code
      and make::citext = case when _make = 'CHEVROLET ALL' then 'CHEVROLET' else _make end);

$body$
;