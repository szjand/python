# encoding=utf-8
# import utilities
# import luigi
import datetime
import csv
from selenium import webdriver
import db_cnx
import time
import os
from selenium.webdriver.chrome.options import Options
# from bs4 import BeautifulSoup
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import requests
pipeline = 'global_connect'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())
user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None
pipeline = pipeline
# divisions = ["11,**,004", "12,**,006", "13,**,001", "48,**,012"]
# 5/17 looks like these are the values for divisions
divisions = ["BUICK", "CADILLAC", "CHEVROLET", "GMC"]
# this assignment satisfies the linters complaint: local variable 'csv_dir' might be referenced before assignment
csv_dir = None
local_or_production = 'local'
if local_or_production == 'local':
    #  TODO CHANGE FOR AFTON LOCAL
    download_dir = 'C:\\Users\\abrug\\OneDrive - Cartiva\\Projects\\Global Connect Scraping' \
                   '\\global-connect-order-workbench-scraper'
    # csv_dir = '/home/jon/projects/luigi_1804_36/global_connect/orders_csv/'
    csv_file = download_dir + '\\view-inventory-and-orders-all.csv'  # does not includes column Field Action
elif local_or_production == 'production':
    download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/orders_xls/'
    csv_dir = '/home/rydell/projects/luigi_1804_36/global_connect/orders_csv/'
    csv_file_field = csv_dir + 'ext_orders_field.csv'  # includes column Field Action
    csv_file = csv_dir + 'ext_orders.csv'  # does not includes column Field Action


def get_secret(category, platform):
    modified_platform = platform.replace(' ', '+')
    url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
           (modified_platform, category))
    response = requests.get(url, auth=(user, passcode))
    return response.json()


def setup_driver():
    # # Chromedriver path
    # TODO AFTON CHANGE
    # chromedriver_path = 'C:\\Users\\abrug\\OneDrive - CARTIVA INC\\python'
    # options = webdriver.ChromeOptions()
    # options.add_experimental_option("prefs", profile)
    # options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
    #                      '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
    # driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
    options = Options()
    # options.add_argument('--headless')
    options.add_argument('--disable-gpu')  # Last I checked this was necessary.
    options.add_argument("--start-fullscreen")  # ****** FULL SCREEN MUST HAVE *****
    profile = {"download.default_directory": download_dir}
    options.add_experimental_option("prefs", profile)
    # Path to your chrome driver
    chromedriver_path = 'C:\\Users\\abrug\\OneDrive - Cartiva\\Desktop\\chromedriver\\chromedriver.exe'
    # Set up driver
    driver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=options)

    return driver


def login(driver):
    credentials = get_secret('Cartiva', 'Global Connect')
    resp_dict = credentials
    # username_box = driver.find_element_by_id('IDToken1')
    # password_box = driver.find_element_by_id('IDToken2')
    username_box = driver.find_element(By.ID, 'IDToken1')
    password_box = driver.find_element(By.ID, 'IDToken2')
    # login_btn = driver.find_element_by_name('Login.Submit')
    login_btn = driver.find_element(By.NAME, 'Login.Submit')
    username_box.send_keys(resp_dict.get('username'))
    password_box.send_keys(resp_dict.get('secret'))
    login_btn.click()
    # WebDriverWait(driver, 20).until(
    #     ec.presence_of_element_located(('id', 'selModelYear')))
    WebDriverWait(driver, 40).until(
        EC.presence_of_element_located(('id', 'search-criteria-title')))


def select_years(driver):
    time.sleep(5)
    # year_dropdown = driver.find_element_by_xpath('//*[@id="selModelYear"]/option[2]')

    # YEARS
    year_dropdown = driver.find_element(By.ID, 'model-year-dropdown')
    year_dropdown.click()
    select_all_years = WebDriverWait(driver, 40).until(
        EC.presence_of_element_located((By.XPATH, "/html/body/div/div[1]/div[1]/div[2]")))
    select_all_years.click()


def select_make(division, driver):

    time.sleep(5)
    # DIVISIONS
    division_dropdown = driver.find_element(By.ID, 'division-dropdown')
    division_dropdown.click()
    select_division = WebDriverWait(driver, 40).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'li[aria-label="%s"]' % division)))
    select_division.click()

    time.sleep(5)

    # ALLOCATION GROUPS
    allocation_dropdown = driver.find_element(By.ID, 'allocation-group-dropdown')
    allocation_dropdown.click()
    select_all_allocations = WebDriverWait(driver, 40).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="allocation-group-dropdown"]/div/div[4]/div[1]/div[1]/div[2]')))
    select_all_allocations.click()

    # SEARCH BUTTON
    driver.find_element(By.ID, 'search-button').click()
    return division


def download_file(driver):
    # Move to the all tab (will default to preliminary)
    all_tab = WebDriverWait(driver, 40).until(
        EC.element_to_be_clickable((By.ID, 'all-tab-subheader')))
    all_tab.click()
    # Have to make sure you are in the all-tab panel
    tab_panel = WebDriverWait(driver, 40).until(
        EC.presence_of_element_located((By.ID, 'all-tab')))
    print('found all tab')
    # Wait for the download button to visible. Once it is clicked you will need to select CSV in the dropdown
    button = WebDriverWait(tab_panel, 40).until(
        EC.element_to_be_clickable((By.XPATH, '/html/body/app-root/div/div/app-view-inventory/div/gm-tab-view/div/'
                                              'div[2]/gm-tab-panel[8]/div/app-all-tab/gm-sticky-table/div/p-table/div/'
                                              'div[1]/p-toolbar/div/div[1]/p-button/button')))
    button.click()
    print('clicking button')
    # Select CSV in dropdown
    csv_download = WebDriverWait(tab_panel, 40).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="download-menu-item-csv"]')))
    csv_download.click()
    print('clicking csv')


def create_data(division_name):
    # OLD version was a nasty xlsx file with HTML in it, ew
    # NEW version is a csv. Simple copy into pg
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate gmgl.ext_ordered_vehicles")
            pg_con.commit()
            with open(csv_file, 'r') as io:
                pg_cur.copy_expert("""copy gmgl.ext_ordered_vehicles from stdin
                                     with csv header encoding 'latin-1 '""", io)
            sql = """select gmgl.ext_orders('{}')""".format(division_name)
            pg_cur.execute(sql)
    os.remove(csv_file)


def next_division(driver):
    # Where the filters are located sometimes collapse. This must be done to ensure the filters are visible
    driver.find_element(By.ID, 'search-criteria-title').click()

# OLD VERSION FOR REFERENCE
# def create_data(division_name):
#     with utilities.pg(pg_server) as database:
#         with database.cursor() as pg_cursor:
#             pg_cursor.execute("truncate gmgl.ext_ordered_vehicles")
#             pg_cursor.execute("truncate gmgl.ext_ordered_vehicles_field")
#             database.commit()
#             soup = BeautifulSoup(open(download_dir + os.listdir(download_dir)[0]), 'lxml')
#             rows = []
#             for row in soup.find_all('tr'):
#                 rows.append([val.text.strip() for val in row.find_all('td')])
#             if 'Field Action' in (item for sublist in rows for item in sublist):
#                 with open(csv_file_field, 'a') as f:
#                     writer = csv.writer(f)
#                     writer.writerows(row for row in rows if len(row) > 1)
#                 with open(csv_file_field, 'r') as io:
#                     pg_cursor.copy_expert("""copy gmgl.ext_ordered_vehicles_field from stdin
#                         with csv encoding 'latin-1 '""", io)
#                 database.commit()
#             else:
#                 with open(csv_file, 'a') as f:
#                     writer = csv.writer(f)
#                     writer.writerows(row for row in rows if len(row) > 1)
#                 with open(csv_file, 'r') as io:
#                     pg_cursor.copy_expert("""copy gmgl.ext_ordered_vehicles from stdin
#                         with csv encoding 'latin-1 '""", io)
#                 database.commit()
#             sql = """select gmgl.ext_orders('{}')""".format(division_name)
#             pg_cursor.execute(sql)


def main():
    driver = setup_driver()
    try:
        # for filename in os.listdir(csv_dir):
        #     os.unlink(csv_dir + filename)
        # driver.get('https://www.autopartners.net/apps/naowb/naowb/manageinventory/mi_02.do?modName=mi'
        #            '&VIEWTYPE=ALL&USER_ACTION=mi_02_body&METHOD=doTileInit')
        # 5/17  i believe this is the new link
        driver.get('https://naowb.autopartners.net/ui/manage-inventory/view-inventory')
        login(driver)
        for i, division in enumerate(divisions):
            if i == 0:
                # For the first division (i == 0) you need to click ALL years - persisted through all divisions after
                select_years(driver)
            division_name = select_make(division, driver)
            download_file(driver)
            # Wait for the file to download.
            time.sleep(5)
            create_data(division_name)
            next_division(driver)

    finally:
        print('done')
        driver.quit()


if __name__ == '__main__':
    main()
