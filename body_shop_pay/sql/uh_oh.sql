﻿select distinct trans
from fin.fact_gl
where control = '18052583'

select *
from fin.fact_gl
where control = '18052583'




select e.the_date, a.control, b.account, b.department, b.description, sum(a.amount) as amount
-- select e.the_date, a.control, b.account, b.department, b.description, amount
-- select sum(amount)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
  and b.current_row = true
inner join dds.dim_date e on a.date_key = e.date_key
--   and e.biweekly_pay_period_sequence = (
--     select biweekly_pay_period_sequence
--     from dds.dim_date
--     where the_date = current_date -1)
where a.post_status = 'Y'
  and control = '18052583'
group by a.control, b.account, e.the_date, b.description, b.department;

select *
from fin.dim_account
where account = '147700'

select *
from fin.dim_account
where department = 'parts'
and account_type = 'sale'
  and store_code = 'ry1'
order by account

select *
from bspp.pay_period_transactions
where control = '18052583'


select *
from bspp.writer_sales
where ro = '18052583'

select (2*8891.26) + 7946.34

select *
from bspp.sales
where ro = '18052583'


oh shit, new writer, no attribution in stored procs for the opcode


select *
from ads.ext_dim_opcode 
where opcodekey in ('26302','26303','26304','26305','26306','26307')

select *
from ads.ext_dim_opcode 
where opcode like 'bs%'


select *
from bspp.personnel


select *
from ads.ext_fact_repair_order a
inner join ads.ext_dim_Service_writer b on a.servicewriterkey = b.servicewriterkey
where writernumber = '318'


select sum(total_sales)
from bspp.writer_sales
where the_date between '08/06/2017' and '08/19/2017'

select sum(total_sales)
from bspp.sales
where the_date between '08/06/2017' and '08/19/2017'


-- bspp.update_pay_period_transactions()
create temp table pay_period_transactions as
select e.the_date, a.control, b.account, b.department, b.description, sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
  and b.current_row = true
inner join dds.dim_date e on a.date_key = e.date_key
  and e.the_date between '08/06/2017' and '08/19/2017'
--   and e.biweekly_pay_period_sequence = (
--     select biweekly_pay_period_sequence
--     from dds.dim_date
--     where the_date = current_date -1)
where a.post_status = 'Y'
group by a.control, b.account, e.the_date, b.description, b.department;


-- class BsSales  bspp.bs_ros

create temp table bs_ros as
SELECT ro, writer_id,
  coalesce((
    select
      case opcodekey
        when 26302 then '312'
        when 26303 then '311'
        when 26304 then '303'
        when 26305 then '317'
        when 26306 then '712'
        when 26307 then '403'
      end as alt_writer_id
    from ads.ext_fact_repair_order
    where ro = d.ro
      and opcodekey between 26302 and 26307), 'none') as alt_writer_id
FROM (
  SELECT a.ro, c.writernumber AS writer_id
  FROM ads.ext_fact_repair_order a
  INNER JOIN (
    select a.control
    from fin.fact_gl a
    inner join fin.dim_account b on a.account_key = b.account_key
    inner join bspp.sale_accounts c on b.account = c.account
    inner join dds.dim_date e on a.date_key = e.date_key
      and e.the_date between '08/06/2017' and '08/19/2017'
--       and e.biweekly_pay_period_sequence = (
--         select biweekly_pay_period_sequence
--         from dds.dim_date
--         where the_date = current_date -1)
    where a.post_status = 'Y'
    group by a.control) b on a.ro = b.control
  INNER JOIN ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
  GROUP BY a.ro, c.writernumber) d


-- bspp.update_writer_sales();

select writer_id, alt_writer_id, the_date, ro, gm_parts, non_gm_parts,
  gm_parts + non_gm_parts as total_parts, labor, 
  gm_parts + non_gm_parts + labor as total_sales, 'pipeline' as source
from (  
  select writer_id, alt_writer_id, the_date, ro,
    coalesce(round(sum(case when category = 'parts' and multiplier = .5 then -1 * amount * multiplier end), 2), 0) as gm_parts,
    coalesce(round(sum(case when category = 'parts' and multiplier = 1 then -1 * amount * multiplier end), 2), 0) as non_gm_parts,
    coalesce(round(sum(case when category = 'labor' then -1 * amount * multiplier end), 2), 0) as labor
  from ( -- n: bs estimator is the writer: 1 row per ro/account/date
    select d.ro, d.writer_id, alt_writer_id,
      e.account, e.the_date, sum(e.amount) as amount,
      e.description, e.department, 
      case 
        when f.category is not null then f.category
        when e.department = 'parts' then 'parts'
        else 'labor'
      end as category, 
      case 
        when f.multiplier is not null then f.multiplier
        when e.department = 'parts' and e.description not like '%NON-GM%' then 0.5
        else 1.0
      end as multiplier
    from (
      select m.ro, m.writernumber as writer_id, coalesce(n.alt_writer_id, 'none') as alt_writer_id
      from ( -- writer and alt_writer where bs est is writer: 1 row per ro
          select a.ro, c.writernumber
          from ads.ext_fact_repair_order  a
          inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey 
            and c.writernumber in ('312','311','303','317','712','403')
          left join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
            and d.opcodekey BETWEEN 26302 AND 26307
          group by a.ro, c.writernumber) m
        left join ( -- alt_writer where bs est is writer : 1 ro per ro
          select a.ro, 
            case a.opcodekey
              when 26302 then '312'
              when 26303 then '311'
              when 26304 then '303'
              when 26305 then '317'
              when 26306 then '712'
              when 26307 then '403'
              else 'none'
            end as alt_writer_id
          from ads.ext_fact_repair_order  a
          inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
            and c.opcodekey between 26302 and 26307
          where a.servicewriterkey in (
            select servicewriterkey
            from ads.ext_dim_service_Writer
--             where  writernumber in ('312','311','303','317','712','403'))
            where  writernumber in (
              select writer_id
              from bspp.personnel b 
              inner join bspp.personnel_role c on b.user_name = c.user_name
                and c.the_role in ('vda','csr')))
          and ro <> '18049420'  
          group by a.ro, a.opcodekey) n on m.ro = n.ro) d
--     inner join bspp.pay_period_transactions e on d.ro = e.control
    inner join pay_period_transactions e on d.ro = e.control
    left join bspp.sale_accounts f on e.account = f.account
    group by d.ro, d.writer_id, d.alt_writer_id, e.account, e.the_date, e.department, 
      f.category, e.description, f.multiplier) n
where not exists ( -- exclude manual source rows
  select 1
  from bspp.writer_sales
  where ro = n.ro
    and the_date = n.the_date
    and source = 'manual');


select *
from bspp.personnel

dayton writer number 318


SELECT a.ro, c.writernumber AS writer_id
FROM ads.ext_fact_repair_order a
INNER JOIN (
  select a.control
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.the_date between '08/06/2017' and '08/29/2017'
  where a.post_status = 'Y'
  group by a.control) b on a.ro = b.control
INNER JOIN ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
  and c.writernumber = '318'
GROUP BY a.ro, c.writernumber


insert into bspp.writer_sales
-- for 8/6 -> 8/19
select -- s.writer_id, s.alt_writer_id, 
  '318',
  case control
    when '18053357' then '303'
    else '403'
  end as alt_writer, 
  r.the_date, r.control, r.gm_parts, r.non_gm_parts,
  r.gm_parts + r.non_gm_parts as total_parts, r.labor,
  r.gm_parts + r.non_gm_parts + r.labor as total_sales,
  'manual'
from ( -- r: date, ro, amounts; 1 row per ro/date; 147700(GM Parts): multiplier = .5
  select e.the_date, a.control,
    round(sum(case when c.category = 'parts' and b.account = '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
    round(sum(case when c.category = 'parts' and b.account <> '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,
    round(
      sum(
        case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
  from fin.fact_gl a -- same query that generates bs_ro_numbers.csv
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.the_date between '08/06/2017' and '08/19/2017'
  where a.post_status = 'Y'
  group by a.control, e.the_date) r
-- inner join bspp.bs_ros s on r.control = s.ro
where r.control in ('18053357','18053406','18053586','18053612');

insert into bspp.writer_sales
-- for 8/6 -> 8/19
select -- s.writer_id, s.alt_writer_id, 
  '318',
  case control
    when '18053357' then '303'
    else '403'
  end as alt_writer, 
  r.the_date, r.control, r.gm_parts, r.non_gm_parts,
  r.gm_parts + r.non_gm_parts as total_parts, r.labor,
  r.gm_parts + r.non_gm_parts + r.labor as total_sales,
  'manual'
from ( -- r: date, ro, amounts; 1 row per ro/date; 147700(GM Parts): multiplier = .5
  select e.the_date, a.control,
    round(sum(case when c.category = 'parts' and b.account = '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
    round(sum(case when c.category = 'parts' and b.account <> '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,
    round(
      sum(
        case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
  from fin.fact_gl a -- same query that generates bs_ro_numbers.csv
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.the_date between '08/06/2017' and '08/19/2017'
  where a.post_status = 'Y'
  group by a.control, e.the_date) r
-- inner join bspp.bs_ros s on r.control = s.ro
where r.control in ('18053357','18053406','18053586','18053612');


insert into bspp.writer_sales
-- for 8/19 -> 8/22
select -- s.writer_id, s.alt_writer_id, 
  '318',
  case control
    when '18053607' then '303'
    else '403'
  end as alt_writer, 
  r.the_date, r.control, r.gm_parts, r.non_gm_parts,
  r.gm_parts + r.non_gm_parts as total_parts, r.labor,
  r.gm_parts + r.non_gm_parts + r.labor as total_sales,
  'manual'
from ( -- r: date, ro, amounts; 1 row per ro/date; 147700(GM Parts): multiplier = .5
  select e.the_date, a.control,
    round(sum(case when c.category = 'parts' and b.account = '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
    round(sum(case when c.category = 'parts' and b.account <> '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,
    round(
      sum(
        case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
  from fin.fact_gl a -- same query that generates bs_ro_numbers.csv
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.the_date between '08/20/2017' and current_date
  where a.post_status = 'Y'
  group by a.control, e.the_date) r
-- inner join bspp.bs_ros s on r.control = s.ro
where r.control in ('18053544','18053588','18053607');


-- 8/22/17
had neglected to add dayton ro number to function bspp.update_writer_sales
still need opcode from randy for dayton as alt writer

these are the ros that need to be added manually: the above 4 for payroll for 8-6 -> 8-19
SELECT a.ro, c.writernumber AS writer_id
FROM ads.ext_fact_repair_order a
INNER JOIN (
  select a.control
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.the_date > '08/19/2017' 
  where a.post_status = 'Y'
  group by a.control) b on a.ro = b.control
INNER JOIN ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
  and c.writernumber = '318'
GROUP BY a.ro, c.writernumber

select *
from ads.ext_dim_service_writer
where name like 'mar%'