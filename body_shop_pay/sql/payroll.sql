﻿do 
$$
declare _from_date date := '02/18/2018';
declare _thru_date date := '03/03/2018';

begin

/*
select * from bspp.get_estimator_pay_summary('myem@rydellcars.com', -1)
          coalesce(round((.5 * c.overtime_hours * 
            (a.commission_perc * d.team_parts_and_labor_sales))/(c.regular_clock_hours + c.overtime_hours), 2), 0) as ot_variance
3/17: need to add parts      
10/28/17:  added guarantee per Randy   
*/            
drop table if exists bs_payroll;
create temp table bs_payroll as

select z.*,
  case
    when "last name" = 'marek' and "total pay" < 1925 then '1925.00' 
    else 'N/A'
  end as Guarantee
from (
  select x.*, coalesce("reg pay", 0) + coalesce("ot pay", 0) + coalesce("pto/holiday pay", 0) 
    + coalesce("ot variance", 0) + coalesce("commission pay", 0) as "total pay"
  from (
    select a.last_name as "last name", a.first_name as "first name", a.employee_number as "emp #", round(100 * commission_perc, 2) as "commission %", 
      -- per ben c, shop_foreman is ATO
      case when aa.the_role <> 'shop_foreman' then a.pto_rate end as "pto/holiday rate", 
      case when aa.the_role <> 'shop_foreman' then aa.base_pay_value end as "hourly rate",
      case when aa.the_role <> 'shop_foreman' then 1.5 * aa.base_pay_value end as "ot rate",
      case when aa.the_role = 'shop_foreman' then aa.base_pay_value end as "monthly salary",
      b.reg_hours, b.ot_hours, b.pto_hours, b.hol_hours, 
  --     coalesce(c.total_sales, d.total_sales) as "total sales",

      case 
        when aa.the_role in ('vda','csr') then c.total_sales
        when aa.the_role in ('shop_foreman', 'parts') then d.total_sales
      end as "total sales",

          
      case 
        when aa.the_role <> 'shop_foreman' then round(b.reg_hours * aa.base_pay_value, 2) 
        else aa.base_pay_value 
      end as "reg pay", 
      case when aa.the_role <> 'shop_foreman' then round(b.ot_hours * 1.5 * aa.base_pay_value, 2) end as "ot pay",
      case when aa.the_role <> 'shop_foreman' then round(a.pto_rate * (b.pto_hours + b.hol_hours), 2) end as "pto/holiday pay",
      case 
        when b.reg_hours + b.ot_hours = 0 then 0
        when aa.the_role in ('vda','csr') then round((.5 * b.ot_hours * 
                (a.commission_perc * c.total_sales))/(b.reg_hours + b.ot_hours), 2)
        when aa.the_role = 'parts' then round((.5 * b.ot_hours * 
                (a.commission_perc * d.total_sales))/(b.reg_hours + b.ot_hours), 2)
      end as "ot variance",  
      case 
        when aa.the_role in ('vda','csr') then round(commission_perc *  c.total_sales, 2)
        when aa.the_role in ('shop_foreman', 'parts') then round(commission_perc *  d.total_sales, 2)
      end as "commission pay" 
          
    from bspp.personnel a
    inner join bspp.personnel_role aa on a.user_name = aa.user_name
    left join ( -- clock hours for estimators only
      select q.user_name, sum(q.reg_hours) as reg_hours, sum(q.ot_hours) as ot_hours,
        sum(q.pto_hours) as pto_hours, sum(q.hol_hours) as hol_hours
      from bspp.clock_hours q 
      inner join bspp.personnel r on q.user_name = r.user_name
      inner join bspp.personnel_role s on r.user_name = s.user_name
        and s.the_role <> 'shop_foreman'
      where q.the_date between _from_date and _thru_date
      group by q.user_name) b on a.user_name = b.user_name
    left join ( -- advisor bs sales
      select b.employee_number, sum(total_sales) total_sales
  ----------------------------------------------------------------------------------------------------------------    
  --     from bspp.sales a
      from bspp.writer_sales a
  ----------------------------------------------------------------------------------------------------------------    
      inner join bspp.personnel b on (a.writer_id = b.writer_id
        or a.alt_writer_id = b.writer_id)
      where the_date between _from_date and _thru_date
      group by b.employee_number) c on  a.employee_number = c.employee_number   
    left join (-- shop foreman all bs sales
      select sum(total_sales) as total_sales
      from bspp.sales a
      where the_date between _from_date and _thru_date) d on 1 = 1) x) z;
--   where a.last_name <> 'blumhagen') x;



end
$$;

select * from bs_payroll order by "last name";


/*
for accrual

select "last name", "first name", "emp #", "reg pay", "ot pay", "ot variance", "commission pay", 
  coalesce("ot variance", 0) + coalesce("commission pay", 0) as total
from bs_payroll order by "last name";

6/1/17, the accrual period for may is 5/14 -> 5/31, (the last pay period ended 5/27)
so, i am going to add 30% to the total (3 working days in addition to the full pay period)

select a.*, round(.3 * total, 2), round(.3 * total + total, 2) 
from (
select "last name", "first name", "emp #", "reg pay", "ot pay", "ot variance", "commission pay", 
  coalesce("ot variance", 0) + coalesce("commission pay", 0) as total
from bs_payroll order by "last name") a;

*/