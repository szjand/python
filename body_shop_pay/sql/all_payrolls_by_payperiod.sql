﻿-- this looks good for sales
select c.biweekly_pay_period_start_date, c.biweekly_pay_period_end_date, b.employee_number, sum(total_sales) total_sales
from bspp.writer_sales a
inner join bspp.personnel b on (a.writer_id = b.writer_id
  or a.alt_writer_id = b.writer_id)
  and b.last_name not in ('blumhagen','shereck')
inner join dds.dim_date c on a.the_date = c.the_date        
where a.the_date between '03/05/2017' and '11/11/2017'
group by c.biweekly_pay_period_start_date, c.biweekly_pay_period_end_date,b.employee_number
order by c.biweekly_pay_period_start_date


-- clock hours for estimators only
select t.biweekly_pay_period_start_date, t.biweekly_pay_period_end_date, q.user_name, 
  sum(q.reg_hours) as reg_hours, sum(q.ot_hours) as ot_hours,
  sum(q.pto_hours) as pto_hours, sum(q.hol_hours) as hol_hours
from bspp.clock_hours q 
inner join bspp.personnel r on q.user_name = r.user_name
inner join bspp.personnel_role s on r.user_name = s.user_name
  and s.the_role not in ( 'shop_foreman','parts')
inner join dds.dim_date t on q.the_date = t.the_date  
where q.the_date between '03/05/2017' and '11/11/2017'
group by t.biweekly_pay_period_start_date, t.biweekly_pay_period_end_date,q.user_name
order by t.biweekly_pay_period_start_date


select x.*, coalesce("reg pay", 0) + coalesce("ot pay", 0) + coalesce("pto/holiday pay", 0) 
  + coalesce("ot variance", 0) + coalesce("commission pay", 0) as "total pay"
from (
  select b.biweekly_pay_period_start_date, b.biweekly_pay_period_end_date,
    a.last_name as "last name", a.first_name as "first name", a.employee_number as "emp #", round(100 * commission_perc, 2) as "commission %", 
    case when aa.the_role <> 'shop_foreman' then a.pto_rate end as "pto/holiday rate", 
    case when aa.the_role <> 'shop_foreman' then aa.base_pay_value end as "hourly rate",
    case when aa.the_role <> 'shop_foreman' then 1.5 * aa.base_pay_value end as "ot rate",
    case when aa.the_role = 'shop_foreman' then aa.base_pay_value end as "monthly salary",
    b.reg_hours, b.ot_hours, b.pto_hours, b.hol_hours, 
    case 
      when aa.the_role in ('vda','csr') then c.total_sales
    end as "total sales",    
    case 
      when aa.the_role <> 'shop_foreman' then round(b.reg_hours * aa.base_pay_value, 2) 
      else aa.base_pay_value 
    end as "reg pay", 
    case when aa.the_role <> 'shop_foreman' then round(b.ot_hours * 1.5 * aa.base_pay_value, 2) end as "ot pay",
    case when aa.the_role <> 'shop_foreman' then round(a.pto_rate * (b.pto_hours + b.hol_hours), 2) end as "pto/holiday pay",
    case 
      when b.reg_hours + b.ot_hours = 0 then 0
      when aa.the_role in ('vda','csr') then round((.5 * b.ot_hours * 
              (a.commission_perc * c.total_sales))/(b.reg_hours + b.ot_hours), 2)
    end as "ot variance",  
    case 
      when aa.the_role in ('vda','csr') then round(commission_perc *  c.total_sales, 2)
    end as "commission pay"           
  -- select *
  from bspp.personnel a
  inner join bspp.personnel_role aa on a.user_name = aa.user_name
  left join (
    select t.biweekly_pay_period_start_date, t.biweekly_pay_period_end_date, q.user_name, 
      sum(q.reg_hours) as reg_hours, sum(q.ot_hours) as ot_hours,
      sum(q.pto_hours) as pto_hours, sum(q.hol_hours) as hol_hours
    from bspp.clock_hours q 
    inner join bspp.personnel r on q.user_name = r.user_name
    inner join bspp.personnel_role s on r.user_name = s.user_name
      and s.the_role not in ( 'shop_foreman','parts')
    inner join dds.dim_date t on q.the_date = t.the_date  
    where q.the_date between '03/05/2017' and '11/11/2017'
    group by t.biweekly_pay_period_start_date, t.biweekly_pay_period_end_date,q.user_name
    order by t.biweekly_pay_period_start_date) b on a.user_name = b.user_name
  left join (
    select c.biweekly_pay_period_start_date, c.biweekly_pay_period_end_date, b.employee_number, sum(total_sales) total_sales
    from bspp.writer_sales a
    inner join bspp.personnel b on (a.writer_id = b.writer_id
      or a.alt_writer_id = b.writer_id)
      and b.last_name not in ('blumhagen','shereck')
    inner join dds.dim_date c on a.the_date = c.the_date        
    where a.the_date between '03/05/2017' and '11/11/2017'
    group by c.biweekly_pay_period_start_date, c.biweekly_pay_period_end_date,b.employee_number
    order by c.biweekly_pay_period_start_date) c on a.employee_number = c.employee_number and b.biweekly_pay_period_start_date = c.biweekly_pay_period_start_date
  where aa.the_role not in ('parts','shop_foreman')) x
order by "last name", biweekly_pay_period_start_date