﻿-- drop table bspp.personnel;
create temp table writers as
select a.firstname, a.lastname, a.employeenumber, a.employeekey, a.distcode, writernumber
-- select *
from ads.ext_dds_edwemployeedim a
left join ads.ext_dim_service_writer b on a.employeenumber = b.employeenumber
  and b.currentrow = true
where a.currentrow = true
  and a.active = 'active'
  and a.lastname in ('hill','steinke','evavold','blumhagen', 'lueker','ostlund', 'yem','shereck') 

select * from ads.ext_dds_edwemployeedim where firstname = 'loren'

select b.the_date, c.*, a.*
from ads.ext_dds_edwclockhoursfact a
inner join dds.dim_date b on a.datekey = b.date_key
inner join writers c on a.employeekey = c.employeekey
where b.the_year = 2017
where b.the_year = 2017
  and a.overtimehours <> 0
order by b.the_date, c.lastname


select *
from ads.ext_dds_factrepairorder
order by rocreatedts desc 
limit 100


select *
from ads.ext_dim_service_writer
where name like 'evavold%'

-- hmmm
go from accounting via ro to factrepairorder to get writer number
awfully fucking circuitous
at some point factrepairorder has to get into postgres and then not by importing from ads

select count(*) from ads.ext_dds_factrepairorder

select b.the_date
from ads.ext_dds_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
order by b.the_date desc
limit 100

select current_date - 90

alter table ads.ext_dds_fact_repair_order
rename to ext_fact_repair_order


select count(*)
from ads.ext_fact_repair_order_tmp

delete 
from ads.ext_dds_fact_repair_order
where ro in (
  select ro
  from ads.ext_fact_repair_order_tmp);



create temp table writer_ros as
select a.ro, b.writernumber
from ads.ext_fact_repair_order a
inner join ads.ext_dim_service_writer b on a.servicewriterkey = b.servicewriterkey
group by a.ro, b.writernumber;


select *
from fin.dim_account
where account in ('16105P', '167906')

-- need to categorize accounts as labor or parts, do it in accounts query
select d.the_date, a.control, b.account, 
  case 
    when b.account = '14770' then round(.5 * a.amount, 2)
    else a.amount
  end as amount, c.description, e.writernumber, f.*
-- select sum(amount)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join bspp.sale_accounts c on b.account = c.account
inner join dds.dim_date d on a.date_key = d.date_key
  and d.the_date between '01/22/2017' and '02/04/2017' -- selected payperiod
left join writer_ros e on a.control = e.ro
left join writers f on e.writernumber = f.writernumber  
order by control
-- sales by writer number
select f.lastname, f.firstname,
  sum(
    case 
      when b.account = '14770' then round(.5 * a.amount, 2)
      else a.amount
    end) as amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join bspp.sale_accounts c on b.account = c.account
inner join dds.dim_date d on a.date_key = d.date_key
  and d.the_date between '02/05/2017' and '02/10/2017' -- selected payperiod
left join writer_ros e on a.control = e.ro
left join writers f on e.writernumber = f.writernumber  
group by f.lastname, f.firstname

-- more than one accounting date per ro?
-- YES
select control, min(the_date), max(the_date) from (
select a.control, b.the_date 
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_year = 2017
inner join fin.dim_account c on a.account_key = c.account_key
inner join bspp.sale_accounts d on c.account = d.account
group by a.control, b.the_date 
) x group by control having count(*) > 1

select a.trans, a.seq, a.control, a.amount, b.the_date, c.description, e.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
inner join fin.dim_Account d on a.account_key = d.account_key
inner join bspp.sale_accounts e on d.account = e.account
where a.control in (
  select control from (
  select a.control, b.the_date 
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.the_year = 2017
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join bspp.sale_accounts d on c.account = d.account
  group by a.control, b.the_date 
  ) x group by control having count(*) > 1)
order by a.control, a.trans, a.seq

-- clock hours
select *
from writers a
left join ads.ext_dds_edwemployeedim b n a.

select d.lastname, d.firstname, d.writernumber, sum(regularhours) as regularhours,
  sum(overtimehours) as overtimehours, sum(vacationhours) as vacationhours, 
  sum(ptohours) as ptohours, sum(holidayhours) as holidayhours
from ads.ext_dds_edwclockhoursfact a
inner join dds.dim_date b on a.datekey = b.date_key
  and b.the_date between '01/22/2017' and '02/04/2017' -- selected payperiod
inner join ads.ext_dds_edwemployeedim c on a.employeekey = c.employeekey
inner join writers d on c.employeenumber = d.employeenumber
group by d.lastname, d.firstname, d.writernumber



select b.the_date, a.*
from ads.ext_dds_edwclockhoursfact a
inner join dds.dim_date b on a.datekey = b.date_key
order by b.the_Date desc 
limit 100


-- 1/13/17 
-- fact_gl done
-- build and populate non-nightly tables
--
drop table bspp.personnel cascade;
create table bspp.personnel (
  user_name citext primary key,
  employee_number citext not null,
  last_name citext not null,
  first_name citext not null,
  writer_id citext not null,
  commission_perc numeric(4,2) not null,
  pto_rate numeric(6,2) not null);
create unique index on bspp.personnel(employee_number);
create unique index on bspp.personnel(writer_id);
drop table bspp.roles cascade;
create table bspp.roles (
  the_role citext primary key,
  base_pay_type citext not null);

drop table bspp.personnel_role cascade;
create table bspp.personnel_role (
  user_name citext not null references bspp.personnel(user_name),
  the_role citext not null references bspp.roles(the_role),
  base_pay_value numeric(6,2) not null,
  constraint personnel_role_pkey primary key(user_name,the_role));

drop table bspp.teams cascade;
create table bspp.teams (
  team_name citext not null,
  vda_user_name citext not null references bspp.personnel(user_name) check (vda_user_name <> csr_user_name),
  csr_user_name citext not null references bspp.personnel(user_name),
  constraint teams_pkey primary key (vda_user_name,csr_user_name));

delete from bspp.teams;
insert into bspp.teams (team_name,vda_user_name,csr_user_name) values
('team_1','bhill@rydellcars.com','aostlund@rydellcars.com'),
('team_2','msteinke@rydellcars.com','myem@rydellcars.com'),
('team_3','devavold@rydellcars.com','dlueker@rydellcars.com');

insert into bspp.roles (the_role, base_pay_type) values
  ('vda','hourly'),
  ('csr','hourly'),
  ('shop_foreman','salary');

insert into bspp.personnel(user_name,employee_number,last_name,first_name,writer_id,commission_perc,pto_rate)
select c.username, a.employeenumber, a.lastname, a.firstname, coalesce(writernumber, 'XXX'), .05, 25
-- select *
from ads.ext_dds_edwemployeedim a
left join ads.ext_dim_service_writer b on a.employeenumber = b.employeenumber
  and b.currentrow = true
left join ads.ext_sco_tpemployees c on a.employeenumber = c.employeenumber  
where a.currentrow = true
  and a.active = 'active'
  and a.lastname in ('hill','steinke','evavold','blumhagen', 'lueker','ostlund', 'yem','shereck');

insert into bspp.personnel_role(user_name,the_role,base_pay_value)
select user_name, 'vda', 10
from bspp.personnel
where last_name in ('hill','steinke','lueker');

insert into bspp.personnel_role(user_name,the_role,base_pay_value)
select user_name, 'csr', 10
-- select *
from bspp.personnel
where last_name in ('blumhagen','ostlund');

insert into bspp.personnel_role(user_name,the_role,base_pay_value)
select user_name, 'csr', 8.5
-- select *
from bspp.personnel
where last_name in ('evavold');

insert into bspp.personnel_role(user_name,the_role,base_pay_value)
select user_name, 'shop_foreman', 1000
-- select *
from bspp.personnel
where last_name in ('shereck');



drop table bspp.clock_hours cascade;
create table bspp.clock_hours (
  the_date date not null,
  user_name citext not null references bspp.personnel(user_name),
  reg_hours numeric(6,2),
  ot_hours numeric(6,2),
  pto_hours numeric(6,2),
  hol_hours numeric(6,2),
  constraint clock_hours_pkey primary key(the_date,user_name));  
-- this is a one time deal 
-- nightly clock_hours.py is the real deal
insert into bspp.clock_hours(the_date,user_name,reg_hours,ot_hours,pto_hours,hol_hours)
select b.the_date, d.user_name, sum(regularhours) as regularhours,
  sum(overtimehours) as overtimehours, sum(vacationhours + ptohours) as ptohours, 
  sum(holidayhours) as holidayhours
from ads.ext_dds_edwclockhoursfact a
inner join dds.dim_date b on a.datekey = b.date_key
  and b.the_date between '02/05/2017' and current_date - 1 -- selected payperiod
inner join ads.ext_dds_edwemployeedim c on a.employeekey = c.employeekey
inner join bspp.personnel d on c.employeenumber = d.employee_number
group by b.the_date, d.user_name
order by b.the_date;




-- insert into bspp.clock_hours(the_date,user_name,reg_hours,ot_hours,pto_hours,hol_hours)
select a.the_date, b.user_name, sum(a.regularhours), sum(a.overtimehours),
  sum(a.ptohours + a.vacationhours) , sum(a.holidayhours)
from bspp.xfm_fact_clock_hours a
inner join bspp.personnel b on a.employee_number = b.employee_number
group by a.the_date, b.user_name;













                

create index on ads.ext_fact_repair_order(ro);
create index on ads.ext_fact_repair_order(opendatekey);
create index on ads.ext_fact_repair_order(servicewriterkey);
create index on ads.ext_dim_service_writer(servicewriterkey);
create index on ads.ext_dim_service_writer(writernumber);

-- ro_writers
drop table bspp.ro_writers;
create table bspp.ro_writers (
  RO citext primary key,
  writer_id citext not null);

-- initial load
insert into bspp.ro_writers(ro, writer_id)
select a.ro, b.writernumber
from ads.ext_fact_repair_order_tmp a
inner join ads.ext_dim_service_writer b on a.servicewriterkey = b.servicewriterkey
-- inner join dds.dim_date c on a.opendatekey = c.date_key
--   and c.the_date > '01/01/2016'
group by a.ro, b.writernumber

drop table bspp.sales;
create table bspp.sales (
  writer_id citext not null,
  the_date date not null,
  ro citext not null,
  parts numeric(8,2) not null default '0',
  labor numeric(8,2) not null default '0',
  constraint sales_pkey primary key(the_date,ro));

-- getting too blurry on sales.py
-- take it up in the morning

insert into bspp.sales(writer_id,the_date,ro,parts,labor)  
select d.writer_id, e.the_date, a.control, 
  sum(case when c.category = 'parts' then -1 * a.amount else 0 end) as parts,
  sum(case when c.category = 'labor' then -1 * a.amount else 0 end) as labor
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join bspp.sale_accounts c on b.account = c.account
inner join bspp.ro_writers d on a.control = d.ro
inner join dds.dim_date e on a.date_key = e.date_key
  and e.the_year = 2017
where a.post_status = 'Y'  
group by e.the_date, d.writer_id, a.control;

-- the issue is how can i conclusively extract both all new and changed rows from
--    factRepairOrder on a daily basis
--    this is a table of 1,325,330 rows
would i not be beter off starting with fact_gs and working backwards,
all i really need from factRepairOrder is the writer/ro number
how often if at all does the writer on an ro change
all i really care about are the ros for the open pay period
fact_gl is the driver



select *
from bspp.sales a
left join bspp.personnel b on a.writer_id = b.writer_id

select first_name, last_name, the_date, sum(parts  + labor) as amount
from bspp.sales a
left join bspp.personnel b on a.writer_id = b.writer_id
group by first_name, last_name, the_date
order by last_name, the_Date

select first_name, last_name, sum(parts  + labor) as amount
from bspp.sales a
left join bspp.personnel b on a.writer_id = b.writer_id
group by first_name, last_name
order by last_name

select *
from bspp.sale_accounts

select a.control, a.amount, b.*
from fin.fact_gl a
inner join bspp.sale_accounts b on a.account_key = b.account_key
where a.control = '18048106'


-- 2/18/17
here are the real teams and commission percentages per Ben C

Brian (10/hr, 2.35%) / Anne (10/hr, 1.25%)
Mark (10/hr, 2.35%) / Mike (10/hr, 1.25%) 
Dan (8.75/hr, 2%) / Dave (10/hr, 1.5%)

need pto rates for mark steinke, all other using alt_hrly_rate from arkona

-- uh oh, for most of the year mark did not clock in and out
-- 
select a.check_month, sum(a.total_gross_pay) --84129
from dds.ext_pyhshdta a
where a.employee_ = '1132218'
and a.payroll_ending_year = 16
  and a.company_number = 'RY1'
  and a.check_month between 10 and 12
group by check_month  
union
select c.year_month, sum(clockhours)
from ads.ext_dds_edwclockhoursfact a
inner join ads.ext_dds_edwemployeedim b on a.employeekey = b.employeekey
  and b.employeenumber = '1132218'
inner join dds.dim_date c on a.datekey = c.date_key
  and c.year_month between 201610 and 201612
group by c.year_month

-- here are the teams
select a.last_name, a.first_name, a.employee_number, a.writer_id, a.commission_perc, 
  b.base_pay_value, a.pto_rate,
  c.team_name, c.vda_user_name, c.csr_user_name
-- select *
from bspp.personnel a
inner join bspp.personnel_role b on a.user_name = b.user_name
left join bspp.teams c on a.user_name = c.vda_user_name or a.user_name = c.csr_user_name
-- left join bspp.teams d on c.team_name = d.team_name 
-- where b.the_role <> 'shop_foreman'
order by team_name, vda_user_name, a.last_name


-- and a little clean up
drop function if exists bspp.test(citext,integer);

alter function bspp.get_estimator_pay_break_down(citext,integer)
rename to zzUnused_get_estimator_pay_break_down

select * from bspp.get_estimator_pay_summary('aostlund@rydellcars.com',0)

select * from bspp.get_shop_foreman_pay_summary('lshereck@rydellcars.com', -1)

  

select b.last_name, the_date, sum(parts) as parts, sum(labor) as labor
from bspp.sales a
inner join bspp.personnel b on a.writer_id = b.writer_id
where a.the_date > '02/18/2017'
group by b.last_name, the_date
order by b.last_name, the_date



