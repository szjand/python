﻿create temp table accounts as 
select distinct d.gl_account, 'labor'
-- select sum(a.amount) -- total bs sales P6L43
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page =16
  and b.line between 21 and 34
   and b.col in (1,2)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.account_type = 'sale'
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
  and f.department = 'service'
  and f.sub_department = 'mechanical'  
union
select distinct d.gl_account, 'parts'
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
  and f.department = 'service'
  and f.sub_department = 'mechanical'
inner join fin.dim_account e on d.gl_account = e.account  
  and e.account_type = 'sale'


select * 
from bspp.sales
order by ro


/* ads
SELECT b.thedate, c.name, a.*
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimservicewriter c on a.servicewriterkey = c.servicewriterkey
WHERE b.thedate BETWEEN '01/01/2017' AND curdate()
  AND c.writernumber IN ('312','311','403','712','303','317')
  AND ro IN (
SELECT gtctl#
FROM stgArkonaGLPTRNS
WHERE gtacct IN (
'145404',
'146000',
'146001',
'146004',
'146007',
'146100',
'146101',
'146201',
'146202',
'146204',
'146301',
'146304',
'146305',
'146320',
'146401',
'146402',
'146404',
'146604',
'146700',
'146800')
AND gtdate BETWEEN '01/01/2017' AND curdate()
GROUP BY gtctl#)
*/

select *
from bspp.sales
where ro = '18050536'

select b.account, b.account_type, b.description, sum(a.amount)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where a.control = '18050536'
group by b.account, b.account_type, b.description

select min(the_date)
from bspp.sales


-- ros by bs writers or have the "bs writer" op codethat are not in bspp.sales
select * 
from bspp.sales d
left join (
  select ro
  from ads.ext_Fact_repair_order a
  inner join dds.dim_date b on a.closedatekey = b.date_key
    and b.the_date > '02/19/2017'
  inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
    and c.writernumber in ('312','311','403','712','303','317')
  group by ro) e on d.ro = e.ro
where e.ro is null  

16270620: writer travis bursinger, evavold added via opcode

are there any ros with a body shop writer or alt_writer that do not have an accounting entry against a bs sale account?

select b.*, a.*
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where control = '16270620'
order by b.account



SELECT *
FROM ads.ext_Fact_repair_order  
where ro in (                  

select g.ro
from (
  select ro, b.the_date, sum(flaghours)
  from ads.ext_Fact_repair_order a
  inner join dds.dim_date b on a.closedatekey = b.date_key
    and b.the_date between '02/19/2017' and current_date   
  left join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
    and c.writernumber in ('312','311','403','712','303','317')
  INNER JOIN ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
    AND d.opcodekey BETWEEN 26302 AND 26307  
  where c.writernumber is not null 
    or d.opcodekey is not null 
  group by ro, b.the_date) g
left join bspp.sales h on g.ro = h.ro
where h.ro is null)


select *
from bspp.sales
where ro = '18049872'

select *
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where control = '18049872'
  and account_type = 'sale'



-- 4/13 -----------------------------------------------------------------------------------------
/*
fuck
pay period is coming to an end and i still don't know what to do
randy is asking about
    RO #16271531

    Anne and Dan shorted $530.33 labor. 

    Line #1 – DIA labor of $37.50. 

    Line #4—alignment labor of $74.95. 

    Line #5—mechanical labor of $427.88.

asked ben c for feedback, here is what i proposed:
    A body shop estimator is paid on the sale of ALL parts and labor on ANY ro on 
    which the estimator is the writer, or on which the estimator is added via an opcode.

that seems to me to be a reasonable approach    
*/

/*
current data lineage (all done in body_shop_pay.sales.py)
  fact_gl generates ro numbers based on account in bspp.sale_accounts
  those ro numbers are used to extract data from ads::dds.factRepairOrder
  the query populating bspp.sales, limits sales based on bspp.sale_accounts
  
so, altering that query should be good enough based on the assumption that 
an ro with a bs estimator as writer or alt_writer will have at least sales against bspp.sale_accounts
i can and should test that
*/  

-- testing the assumption
-- bs estimator is writer or alt_writer
select a.ro, c.writernumber, d.opcodekey
-- select b.the_date, a.*, c.*, d.*
from ads.ext_fact_repair_order  a
inner join dds.dim_date b  on a.closedatekey = b.date_key
  and b.the_date between '02/19/2017' and current_date
left join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey 
left join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
where c.writernumber in ('312','311','303','317','712','403')
  or d.opcodekey between 26302 and 26307
group by a.ro, c.writernumber, d.opcodekey 

-- -- alt_writer, but writer is not bs estimator
-- select a.ro, c.writernumber, d.opcodekey
-- -- select b.the_date, a.*, c.*, d.*
-- from ads.ext_fact_repair_order  a
-- inner join dds.dim_date b  on a.closedatekey = b.date_key
--   and b.the_date between '02/19/2017' and current_date
-- left join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey 
-- left join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
-- where d.opcodekey between 26302 and 26307
--  and c.writernumber not in ('312','311','303','317','712','403')
-- group by a.ro, c.writernumber, d.opcodekey 


-- bs estimator is writer or alt_writer ro not in bspp.sales
select *
from (-- bs estimator is writer or alt_writer
  select a.ro, c.writernumber, d.opcodekey
  -- select b.the_date, a.*, c.*, d.*
  from ads.ext_fact_repair_order  a
  inner join dds.dim_date b  on a.closedatekey = b.date_key
    and b.the_date between '02/19/2017' and current_date
  left join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey 
  left join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
  where c.writernumber in ('312','311','303','317','712','403')
    or d.opcodekey between 26302 and 26307
  group by a.ro, c.writernumber, d.opcodekey) x 
where not exists (
   select 1
   from bspp.sales
   where ro = x.ro)  



select control, sum(amount)
from (-- bs estimator is writer or alt_writer ro not in bspp.sales
  select *
  from (
    select a.ro, c.writernumber, string_agg(distinct d.opcodekey::text, ',')
    -- select b.the_date, a.*, c.*, d.*
    from ads.ext_fact_repair_order  a
    inner join dds.dim_date b  on a.closedatekey = b.date_key
      and b.the_date between '02/19/2017' and current_date
    left join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey 
    left join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
    where c.writernumber in ('312','311','303','317','712','403')
      or d.opcodekey between 26302 and 26307
    group by a.ro, c.writernumber) x 
  where not exists (
     select 1
     from bspp.sales
     where ro = x.ro)) e 
left join fin.fact_gl f on e.ro = f.control     
-- where control = '18050072'  
group by control


select sum(amount)
from fin.fact_gl
where control = '18050072';







-- after i figure that out, here's what i came up with for modifying the generation of bspp.sales
-- 148104 148105: non-gm




drop table if exists sale_accounts_expanded;
create temp table sale_accounts_expanded as
select distinct g.account, g.account_key, g.description, b.page, b.line, g.department,
--   case
--     when g.account in ('147000','147100','147102','147200','147300','147400','147600') then 'labor'
--     when g.account in ('147700','147701','147900','147901') then 'parts'
--     else 'WTF'
--   end as category, 
--   g.department, 
  case
    when g.account in ('147700','147701','147900','147901') then 'parts'
    else 
      case
        when g.department = 'parts' then 'parts'
        else 'labor'
      end
  end as category
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 16
  and b.line between 21 and 59
   and b.col in (1,2)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
inner join fin.dim_account g on d.gl_account = g.account  

select * from sale_accounts_expanded where department = 'body shop' and category = 'parts'

create temp table old_way as
select s.writer_id, s.alt_writer_id, r.the_date, r.control, r.gm_parts, r.non_gm_parts,
  r.gm_parts + r.non_gm_parts as total_parts, r.labor,
  r.gm_parts + r.non_gm_parts + r.labor as total_sales
from ( -- r: date, ro, amounts; 1 row per ro/date; 147700(GM Parts): multiplier = .5
  select e.the_date, a.control,
    round(sum(case when c.category = 'parts' and b.account = '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
    round(sum(case when c.category = 'parts' and b.account <> '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,
    round(sum(case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
  from fin.fact_gl a -- same query that generates bs_ro_numbers.csv
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.biweekly_pay_period_sequence = (
      select biweekly_pay_period_sequence
      from dds.dim_date
      where the_date = current_date - 1)
  where a.post_status = 'Y'
  group by a.control, e.the_date) r
inner join bspp.bs_ros s on r.control = s.ro


drop table if exists new_way;
create temp table new_way as
select s.writer_id, s.alt_writer_id, r.the_date, r.control, r.gm_parts, r.non_gm_parts,
  r.gm_parts + r.non_gm_parts as total_parts, r.labor,
  r.gm_parts + r.non_gm_parts + r.labor as total_sales
from ( -- r: date, ro, amounts; 1 row per ro/date; 147700(GM Parts): multiplier = .5
  select e.the_date, a.control,
    round(sum(case when c.category = 'parts' and b.account = '147700'
--       then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
      then -1 * a.amount * .5 else 0 end), 2) as gm_parts,
    round(sum(case when c.category = 'parts' and b.account <> '147700'
--       then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,
      then -1 * a.amount else 0 end), 2) as non_gm_parts,      
    round(sum(case when c.category = 'labor' then -1 * a.amount /** c.multiplier*/ else 0 end), 2) as labor
  from fin.fact_gl a -- same query that generates bs_ro_numbers.csv
  inner join fin.dim_account b on a.account_key = b.account_key
--   inner join bspp.sale_accounts c on b.account = c.account
  inner join sale_accounts_expanded c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.biweekly_pay_period_sequence = (
      select biweekly_pay_period_sequence
      from dds.dim_date
      where the_date = current_date - 1)
  where a.post_status = 'Y'
  group by a.control, e.the_date) r
inner join bspp.bs_ros s on r.control = s.ro



select a.writer_id, a.alt_writer_id, a.the_date, a.control, a.total_parts, a.labor, a.total_sales,
  b.total_parts, b.labor,  b.total_sales
-- select *  
from old_way a
left join new_way b on a.control = b.control

-- 18049905, why does new_way have 1006.87 labor (549.00) old way



select amount, department, description, account
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where control = '18049905'
  and account_type = 'sale'
order by department, description


select *
from  sale_accounts_expanded 
where account in ('148104','148105')

select * from fin.dim_account where account_type = 'sale' and description like '%non-gm%'
  
