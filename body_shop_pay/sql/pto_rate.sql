﻿select employee_name, employee_,
  sum(commissions) as commission,
  sum(holiday_pay) as holiday_pay,
  sum(pto_pay_out) as pto_pay_out,
  sum(draw) as draw,
  sum(sales_vacation) as sales_vacation,
  sum(spiff_pay_out) as spiff_pay_out,
  sum(lease_program) as lease_program,
  round(sum(commissions + holiday_pay + pto_pay_out + draw + sales_vacation + spiff_pay_out + lease_program), 0) as total_gross
from (  
  select a.employee_name, a.employee_, a.check_month, 100 * (2000 + a.check_year) + check_month as year_month,
    sum(case when description = 'COMMISSIONS' then amount else 0 end) as commissions,
    sum(case when description = 'HOLIDAY PAY' then amount else 0 end) as holiday_pay,
    sum(case when description = 'PAY OUT PTO' then amount else 0 end) as pto_pay_out,
    sum(case when description = 'DRAWS' then amount else 0 end) as draw,
    sum(case when description = 'SALES VACATION' then amount else 0 end) as sales_vacation,
    sum(case when description = 'SPIFF PAY OUTS' then amount else 0 end) as spiff_pay_out,
    sum(case when description = 'LEASE PROGRAM' then amount else 0 end) as lease_program
  --   b.description, sum(b.amount) as amount, b.code_id
  --select distinct c.description
  from dds.ext_pyhshdta a
  inner join dds.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
    and a.company_number = b.company_number
    and a.employee_ = b.employee_number
    and b.code_type in ('0','1') -- 0,1: income, 2: deduction
  where trim(a.distrib_code) = 'SALE'
  --       and a.payroll_cen_year in (116, 117)
  --       and a.payroll_ending_month in (9,10,12,1)
  and (
    (a.payroll_ending_year = 16 and a.payroll_ending_month in (9,10,12))
    or
    (a.payroll_ending_year = 17 and a.payroll_ending_month = 1))
    and a.company_number = 'RY1'
  group by a.employee_name, a.employee_, a.check_month, a.check_year) x
group by employee_name, employee_
order by employee_name

-- here's all 24 codes for ry1 2016
select a.company_number, a.code_id, a.description
-- select *
from dds.ext_pyhscdta a
where a.payroll_cen_year = 116
  and a.code_type in ('0','1')
  and company_number = 'RY1'
  limit 100
group by company_number, code_id, description

-- here are the 13 that apply to bspp.personnel
TECH XTRA BONUS 70
PTO PAY         PTO
TECH HOURS PAY  87
PAY OUT PTO     400
SALARY          38
VAC/PTO PAY     85
Training        66
HOLIDAY PAY     86
OVERTIME PAY    OVT
VACATION PAY    VAC
HOLIDAY PAY     HOL
LEASE PROGRAM   500
COMMISSIONS     79


select a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
  b.employee_number, a.base_pay, a.total_gross_pay, 
  sum(case when c.code_id = '70' then c.amount else 0 end) as "TECH XTRA BONUS",
  sum(case when c.code_id = 'PTO' then c.amount else 0 end) as "PTO PAY",
  sum(case when c.code_id = '87' then c.amount else 0 end) as "TECH HOURS PAY",
  sum(case when c.code_id = '400' then c.amount else 0 end) as "PAY OUT PTO",
  sum(case when c.code_id = '38' then c.amount else 0 end) as "SALARY",
  sum(case when c.code_id = '85' then c.amount else 0 end) as "VAC/PTO PAY",
  sum(case when c.code_id = '66' then c.amount else 0 end) as "TRAINING",
  sum(case when c.code_id = '86' then c.amount else 0 end) as "HOLIDAY PAY",
  sum(case when c.code_id = 'OVT' then c.amount else 0 end) as "OVERTIME PAY",
  sum(case when c.code_id = 'HOL' then c.amount else 0 end) as "HOLIDAY PAY",
  sum(case when c.code_id = 'VAC' then c.amount else 0 end) as "VACATION PAY",
  sum(case when c.code_id = '500' then c.amount else 0 end) as "LEASE PROGRAM",
  sum(case when c.code_id = '79' then c.amount else 0 end) as "COMMISSIONS"
from arkona.ext_pyhshdta a
inner join bspp.personnel b on a.employee_ = b.employee_number
left join arkona.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
  and a.company_number = c.company_number
  and a.employee_ = c.employee_number
  and c.code_type in ('0','1') -- 0,1: income, 2: deduction
where a.payroll_ending_year = 16 -- leaves out second january check
group by a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
  b.employee_number, a.base_pay, a.total_gross_pay
order by b.last_name, a.check_month

drop table if exists pay_2016; 
create temp table pay_2016 as 
select last_name,first_name, employee_number, sum(total_gross_pay) as total_gros, sum(pto) as pto, sum(total_gross_pay - pto) as net
from (
  select a.check_month, b.last_name, b.first_name, 
    b.employee_number, a.total_gross_pay, 
    sum(case when c.code_id in ('PTO','400','85','86','VAC') then c.amount else 0 end) as pto
  from arkona.ext_pyhshdta a
  inner join bspp.personnel b on a.employee_ = b.employee_number
  left join arkona.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
    and a.company_number = c.company_number
    and a.employee_ = c.employee_number
    and c.code_type in ('0','1') -- 0,1: income, 2: deduction
  where a.payroll_ending_year = 16 -- leaves out second january check
  group by a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
    b.employee_number, a.base_pay, a.total_gross_pay) x
group by last_name,first_name, employee_number;

-- now clock hours
drop table if exists hours_2016;
create temp table hours_2016 as
select d.employee_number, d.last_name, d.first_name, sum(clockhours) as clock_hours
from ads.ext_edw_clock_hours_fact a
inner join dds.dim_date b on a.datekey = b.date_key
  and b.the_year = 2016
inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
inner join bspp.personnel d on c.employeenumber = d.employee_number  
group by d.employee_number, d.last_name, d.first_name
order by last_name

-- when did loren work  -- 8 mnonths
select b.year_month, d.employee_number, d.last_name, d.first_name, sum(clockhours) as clock_hours
from ads.ext_dds_edwclockhoursfact a
inner join dds.dim_date b on a.datekey = b.date_key
  and b.the_year = 2016
inner join ads.ext_dds_edwemployeedim c on a.employeekey = c.employeekey
--   and c.currentrow = true
  and c.employeenumber = '1126040'
inner join bspp.personnel d on c.employeenumber = d.employee_number  
group by b.year_month, d.employee_number, d.last_name, d.first_name

-- this is the pto_rate for rollout 201702
select a.last_name, a.first_name, a.employee_number, a.net, b.clock_hours,
  case
    when a.employee_number = '1126040' then round(net/(173.33*8), 2)
    when b.clock_hours < 1000 then round(net/2080, 2)
    else round(net/b.clock_hours, 2)
  end as rate
from pay_2016 a
inner join hours_2016 b on a.employee_number = b.employee_number



select * from pay_2016