﻿--3/3/17 its a whole new ball game
-- see \docs\body shop.docx

1. 
  fix the parts deal, gm parts sales are 50%, i like the idea of a multiplier in the bspp.sales_account table

alter table bspp.sale_accounts 
add column multiplier numeric(2,1);
update bspp.sale_accounts
set multiplier = 1;
update bspp.sale_accounts
set multiplier = .5
where account = '147700';

2. create a table as a target for ro/writer info from ads factrepairorder
create table bspp.bs_ros (
  ro citext primary key,
  writer_id citext not null,
  alt_writer_id citext not null);

2.
  going forward, teams have nothing to do with the pay plan, sales on an ro will be attributed to 
  the writer of the ro and at most one other writer as indicated by the presence of the alt_writer
  to get thru this first pay period, when there is no alt_writer, fill that data with the team member

parts split sales: 147701: GM bs gets 1/2, 147700: non-gm body shop gets all

                select n.writernumber, m.the_date, m.control, m.parts, m.labor
                from ( -- m
                  select (
                    select distinct servicewriterkey
                    from ads.ext_fact_repair_order
                    where ro = a.control),
                    e.the_date, a.control,
                    sum(case when c.category = 'parts' then -1 * a.amount else 0 end) as parts,
                    sum(case when c.category = 'labor' then -1 * a.amount else 0 end) as labor
                  from fin.fact_gl a
                  inner join fin.dim_account b on a.account_key = b.account_key
                  inner join bspp.sale_accounts c on b.account = c.account
                  inner join dds.dim_date e on a.date_key = e.date_key
                    and e.biweekly_pay_period_sequence = (
                      select biweekly_pay_period_sequence
                      from dds.dim_date
                      where the_date = current_date)
                  where a.post_status = 'Y'
                  group by servicewriterkey, e.the_date, a.control) m
                left join ads.ext_dim_service_writer n on m.servicewriterkey = n.servicewriterkey
                where m.control <> '18019019';

select *
from bspp.sales    
where the_date between '02/19/2017' and current_date

-- looks like no more than ~300 ros per pay period
select year_month, count(*)
from (
  select control, year_month
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.year_month between 201601 and 201702
  where a.post_status = 'Y'
  group by control, year_month) x
group by year_month



select a.control
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join bspp.sale_accounts c on b.account = c.account
inner join dds.dim_date e on a.date_key = e.date_key
  and e.biweekly_pay_period_sequence = (
    select biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date - 1) -- -1 if run after midnight
where a.post_status = 'Y'
group by a.control


select *
from ops.task_dependencies




-- for sales from fact_gl: date, ro, amounts
select e.the_date, a.control, 
  round(sum(case when c.category = 'parts' then -1 * a.amount * c.multiplier else 0 end), 2) as parts,
  round(sum(case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join bspp.sale_accounts c on b.account = c.account
inner join dds.dim_date e on a.date_key = e.date_key
  and e.biweekly_pay_period_sequence = (
    select biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date)
where a.post_status = 'Y' and a.control = '18049350'
group by a.control, e.the_date
order by a.control

-- for sales from factrepairorder: ro, writers
select * from bspp.bs_ros

drop table if exists bspp.sales_new;
create table bspp.sales_new (
  writer_id citext not null,
  alt_writer_id citext not null default 'none',
  the_date date not null,
  ro citext not null,
  gm_parts numeric(8,2) not null default '0',
  non_gm_parts numeric(8,2) not null default  '0',
  total_parts numeric(8,2) not null default '0',
  labor numeric(8,2) not null default '0',
  total_sales numeric(8,2) not null default '0',
  source citext not null default 'pipeline',
  check (writer_id <> alt_writer_id),
  constraint sales_new_pkey primary key (the_date, ro));


insert into bspp.sales_new (writer_id,alt_writer_id,the_date,ro,gm_parts,
  non_gm_parts,total_parts,labor,total_sales)
select s.writer_id, s.alt_writer_id, r.the_date, r.control, r.gm_parts, r.non_gm_parts,
  r.gm_parts + r.non_gm_parts as total_parts, r.labor,
  r.gm_parts + r.non_gm_parts + r.labor as total_sales
from ( -- r: date, ro, amounts; 1 row per ro/date
  select e.the_date, a.control, 
    round(sum(case when c.category = 'parts' and b.account = '147700' 
      then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
    round(sum(case when c.category = 'parts' and b.account <> '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,      
    round(sum(case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.biweekly_pay_period_sequence = (
      select biweekly_pay_period_sequence
      from dds.dim_date
      where the_date = current_date)
  where a.post_status = 'Y'
  group by a.control, e.the_date) r  
inner join bspp.bs_ros s on r.control = s.ro  

select *
from bspp.sales_new
order by ro


need teammate for this first pay period

-- update alt_writer_id to be the teammate where there isn't already an alt_writer
-- only for the first pay period
update bspp.sales x
set alt_writer_id = z.alt_writer_id
from (
  select a.ro, e.writer_id as alt_writer_id
  from bspp.sales a
  inner join bspp.personnel b on a.writer_id = b.writer_id
  inner join bspp.personnel_role c on b.user_name = c.user_name
  inner join bspp.teams d on b.user_name = 
    case 
      when c.the_role = 'csr' then d.csr_user_name
      when c.the_role = 'vda' then d.vda_user_name
    end
  inner join bspp.personnel e on e.user_name =
    case
      when c.the_role = 'csr' then d.vda_user_name
      when c.the_role = 'vda' then d.csr_user_name
    end
  where a.alt_writer_id = 'none'
    and the_date between '02/19/2017' and '03/04/2017') z
where x.ro = z.ro;  


-- maintenance, manual row, more than 2 writers in arkona
-- 18049420, per randy, mark and dan get paid
insert into bspp.sales (writer_id,alt_writer_id,the_date,ro,gm_parts,
  non_gm_parts,total_parts,labor,total_sales,source)
select
  (select writer_id from bspp.personnel where last_name = 'steinke'), 
  (select writer_id from bspp.personnel where last_name = 'evavold'), 
  r.the_date, r.control, r.gm_parts, r.non_gm_parts,
  r.gm_parts + r.non_gm_parts as total_parts, r.labor,
  r.gm_parts + r.non_gm_parts + r.labor as total_sales,
  'manual' as source
from ( -- r: date, ro, amounts; 1 row per ro/date
  select e.the_date, a.control, 
    round(sum(case when c.category = 'parts' and b.account = '147700' 
      then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
    round(sum(case when c.category = 'parts' and b.account <> '147700'
      then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,      
    round(sum(case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.biweekly_pay_period_sequence = (
      select biweekly_pay_period_sequence
      from dds.dim_date
      where the_date = current_date)
  where a.post_status = 'Y'
    and a.control = '18049420'
  group by a.control, e.the_date) r; 


-- code for nightly deleting current pay period rows from bspp.sales
-- where source <> manual
delete
select *
from bspp.sales
where source <> 'manual'
  and the_date between 
    (select biweekly_pay_period_start_date from dds.dim_date where the_date = current_date - 1)
    and
    (select biweekly_pay_period_end_date from dds.dim_date where the_date = current_date - 1)


select * from bspp.sales where source = 'manual'


select * from bspp.sales where alt_writer_id = 'none'

select 'anne writer', sum(total_sales) 
from bspp.sales 
where writer_id = '312'
union
select 'anne alt', sum(total_sales) 
from bspp.sales 
where alt_writer_id = '312'
union
select 'brian writer', sum(total_sales) 
from bspp.sales 
where writer_id = '311'
union
select 'brian alt', sum(total_sales) 
from bspp.sales 
where alt_writer_id = '311'


select 'brian is alt', sum(total_sales)
from bspp.sales 
where writer_id = '312'
  and alt_writer_id = '311'
union  
select 'brian is not alt', sum(total_sales)
from bspp.sales 
where writer_id = '312'
  and alt_writer_id <> '311'  



select *
from bspp.sales
where ro = '18049420'


-- 3/7/17 ah shit i fucked up, some of the ros i changed the date to 3/20 are still like that
re fucking do everything:  fix_my_fuck_up_3_17.py

select * from bspp.sales where source = 'manual'

select *
from bspp.sales order by the_date
ok, i think we are ok now
------------------------------------------------------------------------------
-- 3/7/17
-- looks like the process has not kicked in yet
-- 3/6: 22 ros, initially no alt_writer, updated 8 ros to have an alt writer
select a.*, b.last_name, c.last_name
from bspp.sales a
left join bspp.personnel b on a.writer_id = b.writer_id
left join bspp.personnel c on a.alt_writer_id = c.writer_id
where a.the_date = '03/06/2017'
  and b.writer_id is not null
  and a.alt_writer_id = 'none'
order by ro  

-- sent this list to randy,
select b.last_name as writer, c.last_name as other_writer, a.ro, a.the_date,a.total_sales
from bspp.sales a
left join bspp.personnel b on a.writer_id = b.writer_id
left join bspp.personnel c on a.alt_writer_id = c.writer_id
where a.the_date > '03/04/2017'
  and b.writer_id is not null

he returned it:
writer	other_writer	ro	the_date	total_sales
Ostlund	Brian	        18049506	3/6/2017	2329.92
Steinke	Dan	          18049586	3/6/2017	1636.42
Lueker	Dan	          18049604	3/6/2017	1214.88
Ostlund	Dan	          18049627	3/6/2017	1199.39
Steinke	Dan	          18049640	3/6/2017	3689.77
Steinke	Dan	          18049693	3/6/2017	1421.17
Steinke	Dan	          18049729	3/6/2017	2150.03
Ostlund	Mike        	18049844	3/6/2017	1079.67
Lueker	none	        18049217	3/6/2017	806.91
Lueker	none	        18049653	3/6/2017	110
Lueker	none	        18049778	3/6/2017	1130
Yem	    none	        18049845	3/6/2017	217.09
Lueker	none	        18049865	3/6/2017	203.49
Ostlund	none	        18049971	3/6/2017	140
Ostlund	none	        18049985	3/6/2017	294.96
Steinke	none	        18050020	3/6/2017	122
Lueker	none	        18050030	3/6/2017	249.24
Lueker	none	        18050031	3/6/2017	29.38
Ostlund	none	        18050057	3/6/2017	615.69
Yem	    none	        18050092	3/6/2017	55
Lueker	none	        18050094	3/6/2017	223.8
Yem	    none	        18050123	3/6/2017	100

-- update with alt_writer, and set source = manual so it doesn't get whacked in the nightly
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'hill'),
      source = 'manual'
where ro = '18049506';

update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'evavold'),
      source = 'manual'
where ro in ('18049586','18049604','18049627','18049640','18049693','18049729');

update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'yem'),
      source = 'manual'
where ro = '18049844';

-- 3/8
select b.last_name as writer, c.last_name as other_writer, a.ro, a.the_date,a.total_sales
from bspp.sales a
left join bspp.personnel b on a.writer_id = b.writer_id
left join bspp.personnel c on a.alt_writer_id = c.writer_id
where a.the_date > '03/04/2017'
  and b.writer_id is not null
order by the_date, ro  

-- 3/8, only one correction from randy
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'yem'),
      source = 'manual'
where ro = '18050077';

-- 3/9 fuck 9 of the 14 were wrong
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'evavold'),
      source = 'manual'
where ro in ('18049524','18049525','18050081','18049708');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'ostlund'),
      source = 'manual'
where ro in ('18049779');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'steinke'),
      source = 'manual'
where ro in ('18050088');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'hill'),
      source = 'manual'
where ro in ('18050093');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'yem'),
      source = 'manual'
where ro in ('18050110','18050167');

-- Need you to please fix  couple …….RO#18049022--------RO#18049979----both RO’s have Brian Hill as the second person, both should be Dan Evavold on as the second person.
update bspp.sales 
set alt_writer_id = '403',
    source = 'manual'
-- select * from bspp.sales
where ro in ('18049022','18049979')

-- 3/10 4 of the 12 were wrong: all without an alt_writer should have had an alt_writer
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'evavold'),
      source = 'manual'
where ro in ('18049682','18050121');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'steinke'),
      source = 'manual'
where ro in ('18049973');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'yem'),
      source = 'manual'
where ro in ('18050098');

-- 3/13,14,15 no corrections required

-- 3/15,16:
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'hill'),
      source = 'manual'
where ro in ('18050023','18049940','18050347');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'evavold'),
      source = 'manual'
where ro in ('18050310','18049410','18049654');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'lueker'),
      source = 'manual'
where ro in ('18050280');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'yem'),
      source = 'manual'
where ro in ('18050369');
-- 3-22
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'ostlund'),
      source = 'manual'
where ro in ('18050402');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'evavold'),
      source = 'manual'
where ro in ('18050433');

-- 3/27:
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'ostlund'),
      source = 'manual'
where ro in ('18049291','18050329');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'evavold'),
      source = 'manual'
where ro in ('18049655','18050511','18049736');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'hill'),
      source = 'manual'
where ro in ('18050445');

-- 3/28:
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'yem'),
      source = 'manual'
where ro in ('18050181');

-- 3/29:
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'evavold'),
      source = 'manual'
where ro in ('18049681','18049911','18050221','18050425');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'hill'),
      source = 'manual'
where ro in ('18050270');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'yem'),
      source = 'manual'
where ro in ('18050119');

-- 3/30:
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'steinke'),
      source = 'manual'
where ro in ('18050535');

-- 3/31:
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'evavold'),
      source = 'manual'
where ro in ('18049482','18049520','18049676','18049695','18049733');
update bspp.sales
  set alt_writer_id = (select writer_id from bspp.personnel where last_name = 'yem'),
      source = 'manual'
where ro in ('18050018','18050593');

-- for randy
select b.last_name as writer, c.last_name as other_writer, a.ro, a.the_date,a.total_sales
from bspp.sales a
left join bspp.personnel b on a.writer_id = b.writer_id
left join bspp.personnel c on a.alt_writer_id = c.writer_id
where a.the_date = '04/07/2017' --between '04/01/2017' and '04/03/2017'
  and b.writer_id is not null
order by the_date, ro  

-- pay period to date sales total by estimator
select last_name, sum(total_sales)
from (
  select b.last_name, a.total_sales 
  from bspp.sales a
  left join bspp.personnel b on a.writer_id = b.writer_id
  where a.the_date between '03/05/2017' and current_date
  union all
  select c.last_name, a.total_sales 
  from bspp.sales a
  left join bspp.personnel c on a.alt_writer_id = c.writer_id
  where a.the_date between '03/05/2017' and current_date) d
group by last_name



