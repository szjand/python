﻿do i want to zap and reload bspp.sales every day for the current pay period
not that much data, less than a thousand rows per pay period
why NO, not bspp.sales, but sales_tmp, that will be zapped & reloaded daily



drop table bspp.sales_tmp;
create table bspp.sales_tmp (
  writer_id citext not null,
  the_date date not null,
  ro citext not null,
  parts numeric(8,2) default '0',
  labor numeric(8,2) default '0',
  constraint sales_tmp_pkey primary key (the_date, ro));

-- sales for current pay period
insert into bspp.sales_tmp
select n.writernumber, m.the_date, m.control, m.parts, m.labor
from ( -- m
  select (
    select distinct servicewriterkey
    from ads.ext_fact_repair_order
    where ro = a.control), 
    e.the_date, a.control,
    sum(case when c.category = 'parts' then -1 * a.amount else 0 end) as parts,
    sum(case when c.category = 'labor' then -1 * a.amount else 0 end) as labor
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join bspp.sale_accounts c on b.account = c.account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.biweekly_pay_period_sequence = (
      select biweekly_pay_period_sequence
      from dds.dim_date
      where the_date = current_date)
  where a.post_status = 'Y'
  group by servicewriterkey, e.the_date, a.control) m
left join ads.ext_dim_service_writer n on m.servicewriterkey = n.servicewriterkey;

-- new rows
insert into bspp.sales
select writer_id,the_date,ro,parts,labor
from bspp.sales_tmp a
where not exists (
  select 1
  from bspp.sales
  where ro = a.ro
    and the_date = a.the_date);

select * from bspp.sales where ro = '18019358'    

-- update changed writer, parts and/or labor
update bspp.sales z
set writer_id = x.writer_id,
    parts = x.parts,
    labor = x.labor
from (    
  select a.*
  from bspp.sales_tmp a
  inner join bspp.sales b on a.ro = b.ro
    and a.the_date = b.the_Date
    and (
      a.writer_id <> b.writer_id
      or
      a.parts <> b.parts
      or 
      a.labor <> b.labor)) x
where z.ro = x.ro
  and x.the_date = x.the_date;    







