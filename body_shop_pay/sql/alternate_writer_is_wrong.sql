﻿ALL THAT NEEDS TO BE DONE IS TO UPDATE bspp.writer_sales.source and bspp.sales.source to manual
AND THE MANUALLY CORRECTED WRITER/ALT_WRITER WILL PERSIST

10/14/17
but, to get the fucked up ro into the sales/writer_sales tables if they have not yet been added, need to 

1. edit ads.ext_fact_repair_order, set the opcodekey for the incorrect alt writer to 1 (opcode N/A)
or to the correct alt writer if known

when 26302 then '312' - ostlund
when 26303 then '311' - hill
when 26304 then '303' - steinke
when 26305 then '317' - yem
when 26306 then '712' - lueker
when 26307 then '403' - evavold
when 26857 then '318' - marek
                            
2. delete the luigi output file FactGl.txt

3. re-run RunAll

4. update source in tables sales & writer_sales to manual

select *
from ads.ext_fact_repair_order
where ro = '18054670'


update ads.ext_fact_repair_order
set opcodekey = 1
where ro = '18054670'
  and line = 3

select *
from bspp.sales
where ro = '18054460'  

select *
from bspp.writer_sales
where ro = '18054460' 

update bspp.sales
set source = 'manual'
where ro = '18054460';


update bspp.sales
set source = 'manual'
where ro = '18054670';

update bspp.writer_sales
set source = 'manual'
where ro = '18054460';


update bspp.writer_sales
set source = 'manual'
where ro = '18054670';

-- 10/21  18054976, writer & alt writer = 311 s/b 311/312
select *
from ads.ext_fact_Repair_order
where ro = '18054976'

update ads.ext_fact_repair_order
set opcodekey = 26302
where ro = '18054976'
  and line = 2;

update bspp.sales
-- select * from bspp.sales
-- set source = 'manual'
where ro = '18054976';

update bspp.writer_sales
-- select * from bspp.writer_sales
-- set source = 'manual'
where ro = '18054976';

-- 10/28  18055126, writer & alt writer = 403 s/b 403/712
select *
from ads.ext_fact_Repair_order
where ro = '18055126'

update ads.ext_fact_repair_order
set opcodekey = 26306
where ro = '18055126'
  and line = 2;

update bspp.sales
-- select * from bspp.sales where ro = '18055126'
set source = 'manual'
where ro = '18055126';

update bspp.writer_sales
-- select * from bspp.writer_sales where ro = '18055126'
set source = 'manual'
where ro = '18055126';

-----------------------------------------------------------------------------------------------------------------------------------------------------
11/13/17 from gayla
I was away last week for training and I have quite a few RO’s that were closed for end of payroll that did not get the 2nd estimator put on the RO

Please adjust for me

Mark 
18055539

Brian 
18055538
18055537
18055532
18055533

Dan
18055525
Dan 
18055512

when 26302 then '312' - ostlund
when 26303 then '311' - hill
when 26304 then '303' - steinke
when 26305 then '317' - yem
when 26306 then '712' - lueker
when 26307 then '403' - evavold
when 26857 then '318' - marek

hmmm, can i just update writer_sales.alter_writer_id, and .source?

select * from bspp.writer_sales where ro = '18055539'
--steinke
update bspp.writer_sales
set alt_writer_id = '303',
    source = 'manual'
where ro = '18055539';
update bspp.sales
set alt_writer_id = '303',
    source = 'manual'
where ro = '18055539';
--hill
update bspp.writer_sales
set alt_writer_id = '311',
    source = 'manual'
where ro = '18055538';
update bspp.sales
set alt_writer_id = '311',
    source = 'manual'
where ro = '18055538';
update bspp.writer_sales
set alt_writer_id = '311',
    source = 'manual'
where ro = '18055537';
update bspp.sales
set alt_writer_id = '311',
    source = 'manual'
where ro = '18055537';
update bspp.writer_sales
set alt_writer_id = '311',
    source = 'manual'
where ro = '18055532';
update bspp.sales
set alt_writer_id = '311',
    source = 'manual'
where ro = '18055532';
update bspp.writer_sales
set alt_writer_id = '311',
    source = 'manual'
where ro = '18055533';
update bspp.sales
set alt_writer_id = '311',
    source = 'manual'
where ro = '18055533';
-- evavold
update bspp.writer_sales
set alt_writer_id = '403',
    source = 'manual'
where ro = '18055512';
update bspp.sales
set alt_writer_id = '403',
    source = 'manual'
where ro = '18055512';
update bspp.writer_sales
set alt_writer_id = '403',
    source = 'manual'
where ro = '18055525';
update bspp.sales
set alt_writer_id = '403',
    source = 'manual'
where ro = '18055525';
-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------
11/27/17 from gayla
Please add Dan to ro 18055757 – this one is all my fault, was too much in a hurry to get out of here on Wednesday night

when 26302 then '312' - ostlund
when 26303 then '311' - hill
when 26304 then '303' - steinke
when 26305 then '317' - yem
when 26306 then '712' - lueker
when 26307 then '403' - evavold
when 26857 then '318' - marek

hmmm, can i just update writer_sales.alter_writer_id, and .source?

select * from bspp.writer_sales where ro = '18055757'

update bspp.writer_sales
set alt_writer_id = '403',
    source = 'manual'
where ro = '18055757';
update bspp.sales
set alt_writer_id = '403',
    source = 'manual'
where ro = '18055757';

-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------
1/20/18
ro 18056952 has steinke (303) as writer & alt_writer
1.fix factrepairorder

select *
from ads.ext_fact_repair_order
where ro = '18056952'

select * 
from ads.ext_dim_opcode
where opcodekey in (26304,8430,8431)

update ads.ext_fact_repair_order
set opcodekey = 1
where ro = '18056952'
  and line = 3

2. delete the luigi output file FactGl.txt

3. re-run RunAll

4. update source in tables sales & writer_sales to manual

update bspp.sales
set source = 'manual'
-- select * from bspp.sales
where ro = '18056952';

update bspp.writer_sales
set source = 'manual'
-- select * from bspp.writer_sales
where ro = '18056952';

-- turns out dan is the alt writer
update bspp.writer_sales
set alt_writer_id = '403'
-- select * from bspp.writer_sales 
where ro = '18056952';

update bspp.sales
set alt_writer_id = '403'
-- select * from bspp.sales
where ro = '18056952';

-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------
02/16/18 from gayla
18057496
Please add Mark Steinke as 2nd estimator

when 26302 then '312' - ostlund
when 26303 then '311' - hill
when 26304 then '303' - steinke
when 26305 then '317' - yem
when 26306 then '712' - lueker
when 26307 then '403' - evavold
when 26857 then '318' - marek

hmmm, can i just update writer_sales.alter_writer_id, and .source?

select * from bspp.writer_sales where ro = '18057496'
select * from bspp.sales where ro = '18057496'

update bspp.writer_sales
set alt_writer_id = '303',
    source = 'manual'
where ro = '18057496';
update bspp.sales
set alt_writer_id = '303',
    source = 'manual'
where ro = '18057496';

-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------
18057595: lueker, 712, as both writers

1. edit ads.ext_fact_repair_order, set the opcodekey for the incorrect alt writer to 1 (opcode N/A)
or to the correct alt writer if known

when 26302 then '312' - ostlund
when 26303 then '311' - hill
when 26304 then '303' - steinke
when 26305 then '317' - yem
when 26306 then '712' - lueker
when 26307 then '403' - evavold
when 26857 then '318' - marek
                            
2. delete the luigi output file FactGl.txt

3. re-run RunAll

4. update source in tables sales & writer_sales to manual

update ads.ext_fact_repair_order
set opcodekey = 1
where ro = '18057595'
  and line = 3;

update bspp.writer_sales
set source = 'manual'
where ro = '18057595';


update bspp.sales
set source = 'manual'
where ro = '18057595';


