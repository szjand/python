﻿-- gm vs non gm parts sales
select e.year_month,
  round(sum(case when c.category = 'parts' and b.account = '147700'
    then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
  round(sum(case when c.category = 'parts' and b.account <> '147700'
    then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,
  round(
    sum(
      case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
from fin.fact_gl a -- same query that generates bs_ro_numbers.csv
inner join fin.dim_account b on a.account_key = b.account_key
inner join bspp.sale_accounts c on b.account = c.account
inner join dds.dim_date e on a.date_key = e.date_key
  and e.the_date between '01/01/2016' and '10/31/2017'
group by e.year_month
order by e.year_month


-- bs ros with internal work
select * from ads.ext_fact_repair_order limit 100
select * from ads.ext_dim_payment_type
select * from ads.ext_dim_customer limit 100

-- select b.the_date, e.fullname, a.ro, c.paymenttype, sum(a.flaghours)
select b.year_month, sum(a.flaghours) as flag_hours
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
inner join ads.ext_dim_payment_type c on a.paymenttypekey = c.paymenttypekey
inner join ads.ext_dim_service_type d on a.servicetypekey = d.servicetypekey
inner join ads.ext_dim_customer e on a.customerkey = e.customerkey
where b.year_month between 201601 and 201710
--   and c.paymenttype = 'Internal' -- honda inventory shows as customer pay
  and d.servicetypecode = 'BS'
  and e.fullname = 'INVENTORY'
group by b.year_month
