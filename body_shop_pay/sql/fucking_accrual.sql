﻿select b.last_name, b.first_name, b.user_name, b.employee_number, b.writer_id, b.commission_perc, 
  c.the_role, c.base_pay_value, b.pto_rate, a.ro, a.parts, a.labor, d.*
from bspp.sales a
left join bspp.personnel b on a.writer_id = b.writer_id
left join bspp.personnel_role c on b.user_name = c.user_name
left join bspp.teams d on b.user_name = d.vda_user_name or b.user_name = d.csr_user_name
where a.the_date between '02/19/2017' and current_date

-- individual stats
create temp table step_1 as
select a.user_name, a.first_name, a.last_name, a.employee_number, a.writer_id,
  c.the_role, a.commission_perc, b.base_pay_value, d.team_name,
  e.reg_hours, e.ot_hours, e.pto_hours,
  f.parts, f.labor, f.total_sales
from bspp.personnel a
inner join bspp.personnel_role b on a.user_name = b.user_name
inner join bspp.roles c on b.the_role = c.the_role
  and c.the_role in ('vda','csr')
inner join bspp.teams d on a.user_name = d.vda_user_name or a.user_name = d.csr_user_name
inner join (
  select user_name, sum(ot_hours) as ot_hours, sum(reg_hours) as reg_hours, 
    sum(pto_hours) as pto_hours
  from bspp.clock_hours
  where the_date between '02/19/2017' and current_date
  group by user_name) e on a.user_name = e.user_name
inner join (
  select writer_id, sum(parts) as parts, sum(labor) as labor, 
    sum(parts + labor) as total_sales
  from bspp.sales
  where the_date between '02/19/2017' and current_date
  group by writer_id) f on a.writer_id = f.writer_id
order by d.team_name, c.the_role;

what have they made so far this pay period vs what their ave check for the last months was
                                       from vision                          total    reg+ot+pto  avg   .7(avg - clock)
select * from bspp.get_estimator_pay_summary('aostlund@rydellcars.com', 0): 1862.13     795       2782    1391
select * from bspp.get_estimator_pay_summary('bhill@rydellcars.com', 0):    2905.52    1020       3658    1847
select * from bspp.get_estimator_pay_summary('msteinke@rydellcars.com', 0): 2879.06    1795       2782     691
select * from bspp.get_estimator_pay_summary('myem@rydellcars.com', 0):     1173.19     592       2465    1311
select * from bspp.get_estimator_pay_summary('dlueker@rydellcars.com', 0):  1957.27     689       3041    1646
select * from bspp.get_estimator_pay_summary('devavold@rydellcars.com', 0): 3169.50    1561       3079    1063

so, the hourly accrual will handle the reg+ot+pto
so, add to accrual_commissions .7 (avg - (reg+ot+pto)) that is the estimated commission + ot var for the period of 2/19 -> 2/28

no, wait, take the average and do the ratio of days

accrual days 2/19 - 2/28 = 7 days



-- from \body_shop_pay\sql\pto_rate
select a.check_month, b.last_name, b.first_name, 
  b.employee_number, a.total_gross_pay, 
  sum(case when c.code_id in ('PTO','400','85','86','VAC') then c.amount else 0 end) as pto
from dds.ext_pyhshdta a
inner join bspp.personnel b on a.employee_ = b.employee_number
left join dds.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
  and a.company_number = c.company_number
  and a.employee_ = c.employee_number
  and c.code_type in ('0','1') -- 0,1: income, 2: deduction
where a.payroll_ending_year = 16 -- leaves out second january check
group by a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
  b.employee_number, a.base_pay, a.total_gross_pay
order by last_name, check_month

-- limit it to the relevant folks 
-- don't care about pto, just total_gross
select a.check_month, b.last_name, b.first_name, 
  b.employee_number, a.total_gross_pay
from dds.ext_pyhshdta a
inner join bspp.personnel b on a.employee_ = b.employee_number
inner join bspp.teams bb on b.user_name = bb.vda_user_name or b.user_name =bb.csr_user_name
left join dds.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
  and a.company_number = c.company_number
  and a.employee_ = c.employee_number
  and c.code_type in ('0','1') -- 0,1: income, 2: deduction
where a.payroll_ending_year = 16 -- leaves out second january check
group by a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
  b.employee_number, a.base_pay, a.total_gross_pay
order by last_name, check_month

-- monthly total
-- leave out december, 3 checks
select a.check_month, b.last_name, b.first_name, 
  b.employee_number, sum(a.total_gross_pay) as total_gross
from dds.ext_pyhshdta a
inner join bspp.personnel b on a.employee_ = b.employee_number
inner join bspp.teams bb on b.user_name = bb.vda_user_name or b.user_name =bb.csr_user_name
where a.payroll_ending_year = 16 
  and a.check_month between 9 and 11
group by a.check_month, b.last_name, b.first_name, b.employee_number
order by last_name, check_month

-- per paycheck
select b.last_name, b.first_name, 
  b.employee_number, round(sum(a.total_gross_pay)/6, 0) as total_gross
from dds.ext_pyhshdta a
inner join bspp.personnel b on a.employee_ = b.employee_number
inner join bspp.teams bb on b.user_name = bb.vda_user_name or b.user_name =bb.csr_user_name
where a.payroll_ending_year = 16 
  and a.check_month between 9 and 11
group by b.last_name, b.first_name, b.employee_number
order by last_name