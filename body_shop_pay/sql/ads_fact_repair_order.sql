need to deal with >2 writers,

ok, this excludes 18049420, which has 2 alt writers
SELECT ro, writer_id,
  coalesce((
    select
      case opcodekey
        when 26302 then '312'
        when 26303 then '311'
        when 26304 then '303'
        when 26305 then '317'
        when 26306 then '712'
        when 26307 then '403'
      end as alt_writer_id
    from factrepairorder
    where ro = d.ro
      and opcodekey between 26302 and 26307), 'none') as alt_writer_id
FROM (
  SELECT a.ro, c.writernumber AS writer_id
  FROM factrepairorder a
  INNER JOIN  tmp_body_shop_ros b on a.ro = b.ro
  INNER JOIN dimservicewriter c on a.servicewriterkey = c.servicewriterkey
  WHERE a.ro NOT IN ( -- exclude ros with > 2 writers
    SELECT a.ro
    FROM factrepairorder a
    INNER JOIN  tmp_body_shop_ros b on a.ro = b.ro
    INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
      AND c.opcodekey BETWEEN 26302 AND 26307
    GROUP BY a.ro 
    HAVING COUNT(*) > 1)
  GROUP BY a.ro, c.writernumber) d
 
	  
    SELECT COUNT(*)
    FROM factrepairorder a
    INNER JOIN  tmp_body_shop_ros b on a.ro = b.ro
    INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
      AND c.opcodekey BETWEEN 26302 AND 26307
    GROUP BY a.ro 
    HAVING COUNT(*) > 1