﻿
select * from arkona.ext_eisglobal_sypffxmst limit 10

drop table if exists accounts_lines cascade;
create temp table accounts_lines as
-- from sales_consultant_payroll.py AccountsRoutes
-- accounts/page/line/store/year
select distinct a.fxmcyy as the_year,
  case coalesce(b.consolidation_grp, '1') when '2' then 'RY2' else 'RY1' end::citext as store_code,
  a.fxmpge as page, a.fxmlne::integer as line, trim(b.g_l_acct_number) as gl_account
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy between 2016 and 2017
  and trim(a.fxmcde) = 'GM'
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
  and b.company_number = 'RY1'
  and b.g_l_acct_number <> ''
  and (
    (a.fxmpge = 4 and a.fxmlne between 8 and 16 and a.fxmcol = 11) or
    (a.fxmpge = 16 and a.fxmlne between 35 and 42));
create unique index on accounts_lines(the_year, store_code, gl_account);   
create index on accounts_lines(the_year);
create index on accounts_lines(gl_account);

drop table if exists accounting_detail cascade;
create temp table accounting_detail as
select a.trans, a.seq, c.the_date, c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
  a.amount
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join accounts_lines d on b.account = d.gl_account
  and c.the_year = d.the_year
where post_status = 'Y'
  and c.year_month between 201601 and 201710;

-- use this as base for left joins
drop table if exists pages_lines cascade;
create temp table pages_lines as
select a.the_year * 100 + the_month as year_month, store_code, page, line
from (  
  select the_year, store_code, page, line
  from accounts_lines
  group by the_year, store_code, page, line) a
cross join (
  select * from generate_series(1,12,1) as the_month) b
group by year_month,store_code, page,line;  
create unique index on pages_lines(year_month,store_code,page,line);   




select bb.line_label, cc.*
from (
  select *
  from (
    select page, line::integer, line_label
    from fin.dim_fs
    where year_month = 201710
      and page = 16
      and line between 35 and 43
    union
    select page, line::integer, line_label
    from fin.dim_fs
    where year_month = 201710
      and page = 4
      and line between 8 and 17) aa
  group by page, line, line_label) bb 
inner join (
  select case when page = 16 then 6 else page end, line, 
    sum(case when year_month = 201601 then case when page = 16 then -amount else amount end end)::integer as "201601",
    sum(case when year_month = 201602 then case when page = 16 then -amount else amount end end)::integer as "201602",
    sum(case when year_month = 201603 then case when page = 16 then -amount else amount end end)::integer as "201603",
    sum(case when year_month = 201604 then case when page = 16 then -amount else amount end end)::integer as "201604",
    sum(case when year_month = 201605 then case when page = 16 then -amount else amount end end)::integer as "201605",
    sum(case when year_month = 201606 then case when page = 16 then -amount else amount end end)::integer as "201606",
    sum(case when year_month = 201607 then case when page = 16 then -amount else amount end end)::integer as "201607",
    sum(case when year_month = 201608 then case when page = 16 then -amount else amount end end)::integer as "201608",
    sum(case when year_month = 201609 then case when page = 16 then -amount else amount end end)::integer as "201609",
    sum(case when year_month = 201610 then case when page = 16 then -amount else amount end end)::integer as "201610",
    sum(case when year_month = 201611 then case when page = 16 then -amount else amount end end)::integer as "201611",
    sum(case when year_month = 201612 then case when page = 16 then -amount else amount end end)::integer as "201612",
    sum(case when year_month = 201701 then case when page = 16 then -amount else amount end end)::integer as "201701",
    sum(case when year_month = 201703 then case when page = 16 then -amount else amount end end)::integer as "201703",
    sum(case when year_month = 201704 then case when page = 16 then -amount else amount end end)::integer as "201704",
    sum(case when year_month = 201705 then case when page = 16 then -amount else amount end end)::integer as "201705",
    sum(case when year_month = 201706 then case when page = 16 then -amount else amount end end)::integer as "201706",
    sum(case when year_month = 201707 then case when page = 16 then -amount else amount end end)::integer as "201707",
    sum(case when year_month = 201708 then case when page = 16 then -amount else amount end end)::integer as "201708",
    sum(case when year_month = 201709 then case when page = 16 then -amount else amount end end)::integer as "201709",
    sum(case when year_month = 201710 then case when page = 16 then -amount else amount end end)::integer as "201710" 
  from accounting_Detail
  where store_code = 'ry1'
  group by page, line
  union
  select case when page = 16 then 6 else page end, case when page = 4 then 17 else 43 end,
    sum(case when year_month = 201601 then case when page = 16 then -amount else amount end end)::integer as "201601",
    sum(case when year_month = 201602 then case when page = 16 then -amount else amount end end)::integer as "201602",
    sum(case when year_month = 201603 then case when page = 16 then -amount else amount end end)::integer as "201603",
    sum(case when year_month = 201604 then case when page = 16 then -amount else amount end end)::integer as "201604",
    sum(case when year_month = 201605 then case when page = 16 then -amount else amount end end)::integer as "201605",
    sum(case when year_month = 201606 then case when page = 16 then -amount else amount end end)::integer as "201606",
    sum(case when year_month = 201607 then case when page = 16 then -amount else amount end end)::integer as "201607",
    sum(case when year_month = 201608 then case when page = 16 then -amount else amount end end)::integer as "201608",
    sum(case when year_month = 201609 then case when page = 16 then -amount else amount end end)::integer as "201609",
    sum(case when year_month = 201610 then case when page = 16 then -amount else amount end end)::integer as "201610",
    sum(case when year_month = 201611 then case when page = 16 then -amount else amount end end)::integer as "201611",
    sum(case when year_month = 201612 then case when page = 16 then -amount else amount end end)::integer as "201612",
    sum(case when year_month = 201701 then case when page = 16 then -amount else amount end end)::integer as "201701",
    sum(case when year_month = 201703 then case when page = 16 then -amount else amount end end)::integer as "201703",
    sum(case when year_month = 201704 then case when page = 16 then -amount else amount end end)::integer as "201704",
    sum(case when year_month = 201705 then case when page = 16 then -amount else amount end end)::integer as "201705",
    sum(case when year_month = 201706 then case when page = 16 then -amount else amount end end)::integer as "201706",
    sum(case when year_month = 201707 then case when page = 16 then -amount else amount end end)::integer as "201707",
    sum(case when year_month = 201708 then case when page = 16 then -amount else amount end end)::integer as "201708",
    sum(case when year_month = 201709 then case when page = 16 then -amount else amount end end)::integer as "201709",
    sum(case when year_month = 201710 then case when page = 16 then -amount else amount end end)::integer as "201710" 
  from accounting_Detail
  where store_code = 'ry1'
  group by page) cc on bb.page = case when cc.page = 6 then 16 else cc.page end and bb.line = cc.line
order by bb.page, bb.line   

need to look at comp by employee
select * from bspp.personnel
select * from ads.ext_edw_employee_dim where employeenumber = '1122320'

select year_month, control, case when control = '1122320' then 'Sattler' else b.last_name end, sum(amount)
from accounting_detail a
left join bspp.personnel b on a.control = b.employee_number
where page = 4
  and line = 11
  and gl_account = '12305B'
group by year_month, control, case when control = '1122320' then 'Sattler' else b.last_name end


select last_name, control,
  sum(case when year_month = 201601 then amount end)::integer as "201601",
  sum(case when year_month = 201602 then amount end)::integer as "201602",
  sum(case when year_month = 201603 then amount end)::integer as "201603",
  sum(case when year_month = 201604 then amount end)::integer as "201604",
  sum(case when year_month = 201605 then amount end)::integer as "201605",
  sum(case when year_month = 201606 then amount end)::integer as "201606",
  sum(case when year_month = 201607 then amount end)::integer as "201607",
  sum(case when year_month = 201608 then amount end)::integer as "201608",
  sum(case when year_month = 201609 then amount end)::integer as "201609",
  sum(case when year_month = 201610 then amount end)::integer as "201610",
  sum(case when year_month = 201611 then amount end)::integer as "201611",
  sum(case when year_month = 201612 then amount end)::integer as "201612",
  sum(case when year_month = 201701 then amount end)::integer as "201701",
  sum(case when year_month = 201703 then amount end)::integer as "201703",
  sum(case when year_month = 201704 then amount end)::integer as "201704",
  sum(case when year_month = 201705 then amount end)::integer as "201705",
  sum(case when year_month = 201706 then amount end)::integer as "201706",
  sum(case when year_month = 201707 then amount end)::integer as "201707",
  sum(case when year_month = 201708 then amount end)::integer as "201708",
  sum(case when year_month = 201709 then amount end)::integer as "201709",
  sum(case when year_month = 201710 then amount end)::integer as "201710" 
from (
  select year_month, control, case when control = '1122320' then 'Sattler' else b.last_name end, sum(amount) as amount
  from accounting_detail a
  left join bspp.personnel b on a.control = b.employee_number
  where page = 4
    and line = 11
    and gl_account = '12305B'
  group by year_month, control, case when control = '1122320' then 'Sattler' else b.last_name end) a
group by last_name, control



-- hmm, forgot about the FILTER clause
select last_name, control,
  sum(case when year_month = 201601 then amount end)::integer as "201601",
  sum(amount) filter (where year_month = 201601)::integer as "201601-b",
  sum(case when year_month = 201602 then amount end)::integer as "201602",
  sum(case when year_month = 201603 then amount end)::integer as "201603",
  sum(case when year_month = 201604 then amount end)::integer as "201604",
  sum(case when year_month = 201605 then amount end)::integer as "201605",
  sum(case when year_month = 201606 then amount end)::integer as "201606",
  sum(case when year_month = 201607 then amount end)::integer as "201607",
  sum(case when year_month = 201608 then amount end)::integer as "201608",
  sum(case when year_month = 201609 then amount end)::integer as "201609",
  sum(case when year_month = 201610 then amount end)::integer as "201610",
  sum(case when year_month = 201611 then amount end)::integer as "201611",
  sum(case when year_month = 201612 then amount end)::integer as "201612",
  sum(case when year_month = 201701 then amount end)::integer as "201701",
  sum(case when year_month = 201703 then amount end)::integer as "201703",
  sum(case when year_month = 201704 then amount end)::integer as "201704",
  sum(case when year_month = 201705 then amount end)::integer as "201705",
  sum(case when year_month = 201706 then amount end)::integer as "201706",
  sum(case when year_month = 201707 then amount end)::integer as "201707",
  sum(case when year_month = 201708 then amount end)::integer as "201708",
  sum(case when year_month = 201709 then amount end)::integer as "201709",
  sum(case when year_month = 201710 then amount end)::integer as "201710" 
from (
  select year_month, control, case when control = '1122320' then 'Sattler' else b.last_name end, sum(amount) as amount
  from accounting_detail a
  left join bspp.personnel b on a.control = b.employee_number
  where page = 4
    and line = 11
    and gl_account = '12305B'
  group by year_month, control, case when control = '1122320' then 'Sattler' else b.last_name end) a
group by last_name, control



-- do total sales by pay period?