﻿create temp table accounts as 
select distinct d.gl_account
-- select sum(a.amount) -- total bs sales P6L43
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page =16
  and b.line between 35 and 43
   and b.col in (1,2)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
union
-- parts split sales: 147700: GM bs gets 1/2, 147701: non-gm body shop gets all
select distinct d.gl_account
-- select sum(a.amount) -- total bs sales P6L43
-- select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
  and f.department = 'body shop'
inner join fin.dim_account e on d.gl_account = e.account  
  and e.account_type = 'sale'
  
select * from accounts

select b.the_date, a.*, d.*, c.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b. year_month = 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts d on c.account = d.gl_account
order by control

select the_date, count(*)
from (
  select b.the_date, a.control
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b. year_month = 201701
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts d on c.account = d.gl_account
  group by b.the_date, a.control) x
group by the_date
order by the_date


-- all sales accounts
drop table bspp.sale_accounts cascade;
create table bspp.sale_accounts (
  account citext primary key,
  account_key integer not null references fin.dim_account(account_key),
  description citext not null,
  page integer not null,
  line numeric(4,1) not null,
  category citext not null);
create index on bspp.sale_accounts(category);
create unique index on bspp.sale_accounts(account_key);
  
insert into bspp.sale_accounts (account,account_key,description,page,line,category)
select distinct g.account, g.account_key, g.description, b.page, b.line,
  case
    when g.account in ('147000','147100','147102','147200','147300','147400','147600') then 'labor'
    when g.account in ('147700','147701','147900','147901') then 'parts'
    else 'WTF'
  end as category
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 16
  and b.line between 35 and 43
   and b.col in (1,2)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
inner join fin.dim_account g on d.gl_account = g.account  
union -- parts accounts
select distinct e.account, e.account_key, e.description, b.page, b.line,
  case
    when e.account in ('147000','147100','147102','147200','147300','147400','147600') then 'labor'
    when e.account in ('147700','147701','147900','147901') then 'parts'
    else 'WTF'
  end as category
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
  and f.department = 'body shop'
inner join fin.dim_account e on d.gl_account = e.account  
  and e.account_type = 'sale';


  