﻿-- -- select *
-- select employee_ as employee_number, employee_name, payroll_run_number, reference_check_ as payroll_check_number,
--   (check_month::text || '-' || check_day::text || '-' || (2000 + check_year)::text):: date as check_date, 
--   reg_hours as clock_hours, total_gross_pay 
-- from dds.ext_pyhshdta
-- where employee_ = '222300'
-- -- and payroll_run_number = 116170
--   and check_century = 1
--   and (
--     (check_year = 16 and check_month between 11 and 12) or
--     (check_year = 17 and check_month between 1 and 2))

select *
from fin.fact_fs_all
where year_month = 201702
  and page = 16
  and line = 38


select a.*, d.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.the_date between '03/19/2017' and '04/01/2017'
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '147300'  
left join bspp.sales d on a.control = d.ro
order by a.control


  
