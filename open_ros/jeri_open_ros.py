# encoding=utf-8
import csv
import db_cnx
import ops
import time

this is been replaced by luigi.ext_arkona.OpenRos()
pg_con = None
db2_con = None
run_id = None
file_name = 'files/jeri_open_ros.csv'

try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select curdate(), aa.company_number as store, trim(aa.document_number) as ro, 
                  trim(aa.cust_name) as customer,
                  trim(dd.id) as writer, 
                  trim(
                    case aa.ro_status
                      when 'L' then 'G/L Error'
                      when '2' then 'In Process'
                      when '3' then 'Approved by Parts'
                      when '4' then 'Cashier'
                      when '5' then 'Cashier, Delayed Close'
                      when '6' then 'Pre-Invoice'
                      when '7' then 'Odom Required'
                      when '8' then 'Waiting for Parts'
                      when '9' then 'Parts Approval Reqd'
                      else 'Unknown'
                     end) as ro_status, aa.service_type, aa.payment_type, aa.flag_hours, 
                  aa.labor_gross, aa.parts_gross,
                  days(curdate()) - days(cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2)
                    ||'-'||right(trim(open_tran_date),2) as date)) as days_open,
				  0 as days_in_cashier
                from (
                    select a.company_number, a.document_number, a.open_tran_date, a.cust_name, 
                      a.service_writer_id,
                      a.ro_status, 
                      max(case when b.ptltyp = 'A' then ptlpym end) as payment_type,
                      max(case when b.ptltyp = 'A' then ptsvctyp end) as service_type,  
                      coalesce(sum(case when b.ptcode = 'TT' then b.ptlhrs end), 0) as flag_hours,
                      coalesce(sum(case when b.ptcode = 'TT' then b.ptnet - b.ptcost end), 0) as labor_gross,
                      coalesce(sum(case when b.ptltyp = 'P' and ptcode in ('CP','SC', 'WS', 'IS') then 
                        b.ptqty * (b.ptnet - b.ptcost) end), 0) as parts_gross
                    from RYDEDATA.PDPPHDR a
                    left join rydedata.pdppdet b on a.pending_key = b.ptpkey
                    where a.document_type = 'RO'
                      and a.document_number <> ''
                      and a.company_number in ('RY1','RY2')
                      and a.customer_key <> 0
                      and b.ptline < 900
                    group by a.company_number, a.document_number, a.open_tran_date, a.cust_name, a.service_writer_id, 
                        a.ro_status) aa
                left join rydedata.sdpsvct cc on aa.company_number = cc.company_number
                  and aa.service_type = cc.service_type
                left join rydedata.sdpswtr dd on aa.company_number = dd.company_number
                  and aa.service_writer_id = dd.id
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy jeri.open_ros from stdin with csv encoding 'latin-1 '""", io)
            #
            sql = """select jeri.update_open_ro_writers()"""
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error('jeri open ros', time.strftime("%d/%m/%Y"), error)
    print error
finally:
    # required syntax for pypyodbc
    if db2_con.connected:
        db2_con.close()
    if pg_con:
        pg_con.close()
