# encoding=utf-8
import csv
import db_cnx
import openpyxl as xl
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
# from email.mime.text import MIMEText
from email import encoders
import os
import ops
import time

ads_con = None
file_name = 'files/open_ros.csv'

try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                DELETE FROM open_ros_report;
                INSERT INTO open_ros_report
                SELECT c.thedate AS open_date, c.storecode, c.ro, d.fullname AS customer,
                  f.name AS writer,
                  CASE -- handlw XX censusdept
                    WHEN f.name IN ('BEAR, JEFFERY L','LONGORIA, BEVERLEY A','WOINAROWICZ, EMILEE M') THEN 'MR'
                    WHEN f.name = 'POWELL, GAYLA R' THEN 'BS'
                    ELSE e.censusdept
                  END AS censusdept, g.rostatus, curdate() - c.thedate AS age,
                  CASE
                    WHEN h.ro IS NOT NULL THEN true
                    ELSE false
                  END AS warranty
                FROM (
                  SELECT c.thedate, a.storecode, a.ro, a.customerkey, a.servicewriterkey,
                    a.rostatuskey
                  FROM factrepairorder a
                  INNER JOIN day b on a.finalclosedatekey = b.datekey
                    AND b.thedate > curdate()
                  INNER JOIN day c on a.opendatekey = c.datekey
                  GROUP BY c.thedate, a.storecode, a.ro, a.customerkey, a.servicewriterkey,
                    a.rostatuskey) c
                INNER JOIN dimcustomer d on c.customerkey = d.customerkey
                INNER JOIN dimservicewriter e on c.servicewriterkey = e.servicewriterkey
                INNER JOIN edwEmployeeDim f on e.employeenumber = f.employeenumber
                  AND f.currentrow = true
                INNER JOIN dimrostatus g on c.rostatuskey = g.rostatuskey
                LEFT JOIN (--warranty
                  SELECT d.thedate, a.ro
                  FROM factrepairorder a
                  INNER JOIN day b on a.finalclosedatekey = b.datekey
                    AND b.thedate > curdate()
                  INNER JOIN day d on a.opendatekey = d.datekey
                    AND d.thedate > '12/31/2015'
                  INNER JOIN dimpaymenttype c on a.paymenttypekey = c.paymenttypekey
                    AND c.paymenttype = 'Warranty'
                  GROUP BY ro, d.thedate) h on c.ro = h.ro
                WHERE c.thedate > '12/31/2015'
                  AND c.ro <> '19226040'
            """
            ads_cur.execute(sql)
            ads_con.commit()
            sql = """
                SELECT 1 AS seq, 'GM CASHIER STATUS' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 2, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus = 'Cashier'
                  AND store = 'ry1'
                -- ORDER BY seq, age DESC
                UNION
                SELECT 3 AS seq, 'GM SERVICE: RED > 30 days' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 4, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept IN ('MR','AM')
                  AND store = 'ry1'
                  AND age > 30
                UNION
                SELECT 5 AS seq, 'GM SERVICE: YELLOW 14 - 30 days' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 6, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept IN ('MR','AM')
                  AND store = 'ry1'
                  AND age BETWEEN 14 AND 30
                UNION
                SELECT 7 AS seq, 'GM BS RED > 60 days' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 8, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept IN ('BS')
                  AND store = 'ry1'
                  AND age > 60
                UNION
                SELECT 9 AS seq, 'GM BS YELLOW > between 30 and 60' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 10, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept IN ('BS')
                  AND store = 'ry1'
                  AND age BETWEEN 30 AND 60
                UNION
                SELECT 11 AS seq, 'GM PDQ RED > 1 day' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 12, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept = 'QL'
                  AND store = 'ry1'
                  AND age > 1
                  AND warranty = false
                UNION
                SELECT 13 AS seq, 'GM PDQ WARRANTY RED > 7 day' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 14, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept = 'QL'
                  AND store = 'ry1'
                  AND age > 7
                  AND warranty = true
                UNION
                SELECT 15 AS seq, 'HONDA CASHIER STATUS' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 16, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus = 'Cashier'
                  AND store = 'ry2'
                UNION
                SELECT 17 AS seq, 'HONDA SERVICE RED > 30 days' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 18, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept = 'MR'
                  AND store = 'ry2'
                  AND age > 30
                UNION
                SELECT 19 AS seq, 'HONDA SERVICE YELLOW BETWEEN 14 and 30 days' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 20, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept = 'MR'
                  AND store = 'ry2'
                  AND age BETWEEN 14 AND 30
                UNION
                SELECT 21 AS seq, 'HONDA PDQ RED > 1 day' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 22, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept = 'QL'
                  AND store = 'ry2'
                  AND age > 1
                  AND warranty = false
                UNION
                SELECT 23 AS seq, 'HONDA PDQ WARRANTY RED > 7 days' AS category, '' AS store, '' AS ro,
                  CAST(NULL AS sql_date) AS open_date,
                  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
                FROM system.iota
                UNION
                select 24, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
                FROM open_ros_report a
                WHERE rostatus <> 'Cashier'
                  AND censusdept = 'QL'
                  AND store = 'ry2'
                  AND age > 7
                  AND warranty = true
                ORDER BY seq, age DESC
            """
            ads_cur.execute(sql)
            # with open(file_name, 'w', newline='') as f:
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(ads_cur)
    wb = xl.Workbook()
    ws = wb.active
    with open(file_name) as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            ws.append(row)
        f.close()
    wb.save('files/open_ros.xlsx')
    """
        after AFUCKINGLOT of tweaking, this is good enuf, fucking email with attachment to multiple recipients
        the only bug is, the first recipient in the list of recipients, does not get the attachment
        as long as that is me, fuck it, good eunf
        After the first email, won't need a body, can revert to
        outer = MIMEMultipart()
        and no outer.attach(MIMEText(body))
        in which case, all recipients receive the attachment
    """
    COMMASPACE = ', '
    # body = 'Oops, here''s the list'
    sender = 'jandrews@cartiva.com'
    recipients = ['jandrews@cartiva.com', 'test@cartiva.com',  'test@gfhonda.com']
    # recipients = (['jandrews@cartiva.com', 'bcahalan@rydellcars.com', 'aneumann@rydellcars.com',
    #                'blongoria@rydellcars.com', 'jschmiess@rydellcars.com', 'dstinar@rydellcars.com',
    #                'gpowell@rydellcars.com', 'rsattler@rydellcars.com', 'gsorum@cartiva.com',
    #                'kespellund@rydellcars.com', 'jdangerfield@gfhonda.com', 'nneumann@rydellcars.com',
    #                'apreston@rydellcars.com', 'test@cartiva.com'])
    # outer = MIMEMultipart('alternative')
    outer = MIMEMultipart()
    outer['Subject'] = 'Open ROs'
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    # outer.attach(MIMEText(body))
    attachments = ['files/open_ros.xlsx']
    # Add the attachments to the message
    for file in attachments:
        try:
            with open(file, 'rb') as fp:
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read())
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(file))
            outer.attach(msg)
        except:
            # print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
            raise
    composed = outer.as_string()
    # this works with Python3, but not with Python2
    # Error: SMTP instance has no attribute '__exit__'
    # context manager was not added to smtplib until python 3.3
    # try:
    #     with smtplib.SMTP('mail.cartiva.com') as s:
    #         s.sendmail(sender, recipients, composed)
    #         s.close()
    #     print("Email sent!")
    # except:
    #     # print("Unable to send the email. Error: ", sys.exc_info()[0])
    #     raise

    # this works with Python2
    e = smtplib.SMTP('mail.cartiva.com')
    try:
        e.sendmail(sender, recipients, composed)
        print "Successfully sent email"
    except smtplib.SMTPException:
        print "Error: unable to send email"
    e.quit()

except Exception as error:
    ops.email_error('Open RO Report', time.strftime("%d/%m/%Y"), error)
    print(error)
finally:
    if ads_con:
        ads_con.close()
