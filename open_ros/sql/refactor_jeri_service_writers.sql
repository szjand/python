﻿

/*
having added dealer fx writers, it now becomes necessary to add the store to jeri.service_writers
change structure of the table
change the function FUNCTION jeri.update_open_ros() - join on jeri.service_writers needs to include store
*/

------------------------------------------------------------------
--< original setup 
------------------------------------------------------------------
drop table if exists jeri.service_writers;
create table jeri.service_writers (
  writer_name citext not null,
  employee_number citext not null,
  writer_id citext not null,
  primary key(writer_name));
create index on jeri.service_writers(writer_id);
create index on jeri.service_writers(employee_number);


insert into jeri.service_writers  
select max(name) as name, employeenumber, max(writernumber) as writernumber
from (
  select a.employeenumber, a.name, a.writernumber as writernumber, 
    case
      when a.name in ('BEAR, JEFF', 'DANGERFIELD, JOEL', 'LONGORIA,BEVERLEY ANNE')then 'MR'
      when a.name in ('POWELL,GAYLA R') then 'BS'
      else a.censusdept
    end as censusdept
  from ads.ext_dim_service_Writer a
  left join ads.ext_edw_employee_dim b on a.employeenumber = b.employeenumber
    and b.currentrow = true
  where a.currentrow = true
    and a.active 
    and a.censusdept <> 'HS'
    and a.name <> 'unknown'
    and left(a.employeenumber, 1) in ('1','2')
    and b.termdate > current_date
    and a.employeenumber not in ('1150510','1103050')) aa
where censusdept <> 'XX' 
group by employeenumber;

------------------------------------------------------------------
--/> original setup 
------------------------------------------------------------------


------------------------------------------------------------------
--< new structure
------------------------------------------------------------------
drop table if exists jeri.service_writers;
create table jeri.service_writers (
  writer_name citext not null,
  employee_number citext not null,
  writer_id citext not null,
  store_code citext,
  primary key(writer_name,store_code));
create index on jeri.service_writers(writer_id);
create index on jeri.service_writers(employee_number);

-- yikes i fucked up, fortunately i have the data in tem.writers
-- jeez jon, this query depends on jeri.service_writers which i just dropped

drop table if exists tem.writers;
create table tem.writers as
select writer_name, employee_number, writer_id, store_code
from (
  select a.writer_name, a.employee_number, a.writer_id,
    case
      when writer_id = '224' then 'RY2'
      when left(a.employee_number, 1) = '1' then 'RY1'
      when left(a.employee_number, 1) = '2' then 'RY2'
      when a.writer_name = 'detail department' then 'RY1'
      when writer_number = 'ca' and b.store_code = 'RY1' then 'RY1'
      when writer_number = 'ca' and b.store_code = 'RY2' then 'RY2'
      else b.store_code
    end as store_code
  --   b.store_code
  from jeri.service_writers a
  left join (
    select store_code, writer_name, employee_number, writer_number
    from dds.dim_service_writer
    where active
      and writer_name is not null
      and employee_number is not null
      and current_Row
    group by store_code, writer_name, employee_number, writer_number) b on a.writer_name = b.writer_name 
      and a.writer_id = b.writer_number
  where writer_id not in ('647','797')) aa
group by writer_name, employee_number, writer_id, store_code    
order by writer_name;

insert into jeri.service_writers
select * from tem.writers;
------------------------------------------------------------------
--/> new structure
------------------------------------------------------------------
