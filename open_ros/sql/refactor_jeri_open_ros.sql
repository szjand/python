﻿drop table if exists jeri.open_ro_headers;
create table jeri.open_ro_headers (
  store_code citext not null,
  ro citext primary key,
  ptpkey integer not null,
  customer citext not null,
  service_writer_id citext not null,
  ro_status citext not null,
  open_date date not null,
  days_open integer not null,
  days_in_cashier_status integer not null);
  
alter table jeri.open_ro_headers
add column run_date date;
update jeri.open_ro_headers
set run_date = current_date - 1 ;

alter table jeri.open_ro_headers
drop constraint open_ro_headers_pkey;
alter table jeri.open_ro_headers
add primary key (run_date,ro);

drop table if exists jeri.open_ro_details;
create table jeri.open_ro_details (
  ro citext not null,
  ptpkey integer not null,
  service_type citext not null,
  payment_type citext not null,
  flag_hours numeric(8,2) not null,
  labor_sales numeric(8,2) not null,
  labor_cost numeric(8,2) not null,
  labor_gross numeric(8,2) not null,
  parts_gross numeric(8,2) not null,
  primary key (ro,service_type,payment_type))

alter table jeri.open_ro_details
add column  run_date date;
update jeri.open_ro_details
set run_date = current_date - 1;

alter table jeri.open_ro_details
drop constraint open_ro_details_pkey;
alter table jeri.open_ro_details
add primary key (run_date,ro,service_type,payment_type);


-- once i get it down to populate jeri.open_ros from headers & detail, the function should work just the same
-- lets look at todays data, open_ros should be very close to the same as headers/details

-- so, somehow tease out from header/details those with more than one row
-- and replace those ros in open_ros, i think that all i have to maintain from open_ros is days in cashier status


-- ok, the transition should be a one time deal
-- i am only updating current day data
-- statuses are not affected
-- single ro ros are not affected
-- hoping tto replace rows in jeri.open ros with multiple row ros 


-- in fact, need to think this one out, no need to keep history of header/details
nightly scrape header/detail -> insert into open_ros which is a permanent store of all dates

-- should i exclude ros/lines with 0 flag hours and 0 gross?
-- first issue, just lines or entrire ros?
-- the ros are open, but of what interest are they?
-- per greg, they are of interest.

select * -- 535 ros with single rows
from jeri.open_ro_headers a
where ro in (
  select ro
  from jeri.open_ro_details
  group by ro 
  having count(*) = 1)

select * -- 13 ros with no detail row
from jeri.open_ro_headers a
where ro not in (
  select ro
  from jeri.open_ro_details
  group by ro ) 

select * -- 222 ros with multiple rows
from jeri.open_ro_headers a
where ro in (
  select ro
  from jeri.open_ro_details
  group by ro 
  having count(*) > 1)

select a.ro, a.ro_status, a.days_in_cashier_status, b.status, b.days_in_cashier_status
from jeri.open_ro_headers a
left join jeri.open_ros b on a.ro = b.ro
  and b.the_date = current_date
where a.ro in (
  select ro
  from jeri.open_ro_details
  group by ro 
  having count(*) > 1)
order by ro_status


-- ok, this has updated jeri.open_ro_headers.days_in_cashier_status
update jeri.open_ro_headers z
set days_in_cashier_status = y.days_in_cashier_status
from (
  select ro, days_in_cashier_status
  from jeri.open_ros
  where the_date = current_date
    and ro in (
      select ro
      from jeri.open_ro_details
      group by ro 
      having count(*) > 1)) y
where z.ro = y.ro


-- DO THIS ON LOCAL FIRST, looks like it works fine, had to change PK on open_ros
-- store ro_status_days in temp table
CREATE TABLE jeri.tmp_ro_status_days (
  ro citext NOT NULL,
  status citext NOT NULL,
  from_date date NOT NULL,
  thru_date date NOT NULL DEFAULT '9999-12-31'::date);
  
insert into jeri.tmp_ro_status_days
select * from jeri.ro_status_days; 
 
-- drop ro_status_days (due to constraints)
DROP TABLE jeri.ro_status_days;

-- delete all rows from jeri.open_ros where the_date = current_date and ro is in details with multiple rows
delete 
from jeri.open_ros 
where the_date = current_date 
  and ro in (
    select ro
    from jeri.open_ro_details
    group by ro 
    having count(*) > 1);

-- need to change PK on open_ros
alter table jeri.open_ros
drop constraint open_ros_pkey;
alter table jeri.open_ros
add primary key (the_date,ro,service_type,payment_type);
   
-- insert rows from headers/details
insert into jeri.open_ros
select a.run_date, a.store_code, a.ro, a.customer, c.writer_name, a.ro_status,
  b.service_type, b.payment_type, b.flag_hours, b.labor_gross, b.parts_gross, 
  a.days_open, a.days_in_cashier_status
-- select count(distinct a.ro)
from jeri.open_ro_headers a
inner join jeri.open_ro_details b on a.ro = b.ro
left join jeri.service_writers c on a.service_writer_id = c.writer_id
where b.ro in (
  select ro
  from jeri.open_ro_details
  group by ro 
  having count(*) > 1);

-- restore ro_status_days
CREATE TABLE jeri.ro_status_days(
  ro citext NOT NULL,
  status citext NOT NULL,
  from_date date NOT NULL,
  thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  CONSTRAINT ro_status_days_pkey PRIMARY KEY (ro, thru_date),
  CONSTRAINT date_check CHECK (thru_date >= from_date),
  CONSTRAINT ro_status_days_ro_check CHECK (jeri.ro_exists(ro)));

-- repopulate ro_status_days
insert into jeri.ro_status_days
select * 
from jeri.tmp_ro_status_days;

-- get rid of the tmp table
drop table jeri.tmp_ro_status_days cascade;


select *
from jeri.open_ros
where the_date = current_date
  order by ro

==============================================================================================
drop table if exists wtf;
create temp table wtf as
select *
from jeri.ro_status_days;

DROP TABLE jeri.ro_status_days;

CREATE TABLE jeri.ro_status_days
(
  ro citext NOT NULL,
  status citext NOT NULL,
  from_date date NOT NULL,
  thru_date date NOT NULL DEFAULT '9999-12-31'::date,
  CONSTRAINT ro_status_days_pkey PRIMARY KEY (ro, thru_date),
  CONSTRAINT date_check CHECK (thru_date >= from_date),
  CONSTRAINT ro_status_days_ro_check CHECK (jeri.ro_exists(ro))
)
WITH (
  OIDS=FALSE
);
ALTER TABLE jeri.ro_status_days
  OWNER TO rydell;

insert into jeri.ro_status_days
select * from wtf;  



drop table if exists ros;
create temp table ros as
select *
from jeri.open_ros;  