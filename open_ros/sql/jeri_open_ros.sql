﻿

select c.*
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
left join arkona.ext_sdphist c on a.ro = c.repair_order_number
where a.ro = '16314916'
order by updated_Time

limit 1001
offset 5000

select *
from arkona.ext_sdphist
limit 100

create table ads.ext_dim_ro_status (
  ro_status_key integer primary key,
  ro_status_code citext not null,
  ro_status citext not null);

select *
from arkona.ext_pdptdet
where ptinv_ = '16314916'


select a.ro, a.line,
  case a.linestatuskey
    when 2 then 'closed'
    else 'open'
  end as line_status,
  c.the_date as open_date, d.the_date as close_date, b.the_date as final_close_date,
  e.ro_status, f.servicetype, g.paymenttype, h.fullname, a.flaghours, a.roflaghours
-- select a.*  
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
inner join dds.dim_date c on a.opendatekey = c.date_key
inner join dds.dim_date d on a.closedatekey = d.date_key
inner join ads.ext_dim_ro_status e on a.rostatuskey = e.ro_status_key
  and e.ro_status_key = 5 -- cashier
inner join ads.ext_dim_service_type f on a.servicetypekey = f.servicetypekey
inner join ads.ext_dim_payment_type g on a.paymenttypekey = g.paymenttypekey
inner join ads.ext_dim_customer h on a.customerkey = h.customerkey
order by c.the_date
-- where a.ro = '16314916'  


select max(open_date)
from arkona.ext_sdprhdr
where ro_number = '16314916'

select * from ads.ext_dim_ro_status

select *
from arkona.ext_pdptdet
where ptinv_ = '16315901'

select a.ro, a.line,
  case a.linestatuskey
    when 2 then 'closed'
    else 'open'
  end as line_status,
  c.the_date as open_date, d.the_date as close_date, b.the_date as final_close_date,
  e.ro_status, f.servicetype, g.paymenttype, h.fullname, a.flaghours, j.the_date, i.amount, k.account, k.account_type, k.description, k.department
-- select a.*  
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
inner join dds.dim_date c on a.opendatekey = c.date_key
  and c.year_month in (201806, 201807)
inner join dds.dim_date d on a.closedatekey = d.date_key
inner join ads.ext_dim_ro_status e on a.rostatuskey = e.ro_status_key
  and e.ro_status_key = 5 -- cashier
inner join ads.ext_dim_service_type f on a.servicetypekey = f.servicetypekey
inner join ads.ext_dim_payment_type g on a.paymenttypekey = g.paymenttypekey
inner join ads.ext_dim_customer h on a.customerkey = h.customerkey
left join fin.fact_gl i on a.ro = i.control
  and i.post_status = 'Y'
left join dds.dim_date j on i.date_key = j.date_key
left join fin.dim_Account k on i.account_key = k.account_key
--   and k.account_type = 'sale'
-- where a.ro = '16315901'
where h.fullname not in ('shop time','inventory')
order by c.the_date




SELECT c.the_date, a.storecode, a.ro, a.customerkey, a.servicewriterkey,
  a.rostatuskey
FROM ads.ext_fact_repair_order a
INNER JOIN dds.dim_date b on a.finalclosedatekey = b.date_key
  AND b.the_date > current_Date
INNER JOIN dds.dim_date c on a.opendatekey = c.date_key
GROUP BY c.the_date, a.storecode, a.ro, a.customerkey, a.servicewriterkey,
  a.rostatuskey


delete
FROM ads.ext_fact_repair_order
where ro in (
select ro_number
from arkona.ext_sdprhdr
where cust_name = '*VOIDED REPAIR ORDER*')


select max(open_date) from arkona.ext_sdprhdr limit 10


-- 7/9 a little clarity in the 9:30 today, only open lines (ie, not yet in accounting) are relevant

-- need tech
-- ro/line unique
select a.ro, a.line, linestatuskey,
  c.the_date as open_date, d.the_date as close_date, b.the_date as final_close_date,
  e.ro_status, f.servicetype, g.paymenttype, h.fullname, -- sum(a.flaghours) as flaghours
  a.flaghours, i.laborcost, laborsales
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.the_date > current_date
inner join dds.dim_date c on a.opendatekey = c.date_key
inner join dds.dim_date d on a.closedatekey = d.date_key
inner join ads.ext_dim_ro_status e on a.rostatuskey = e.ro_status_key
inner join ads.ext_dim_service_type f on a.servicetypekey = f.servicetypekey
inner join ads.ext_dim_payment_type g on a.paymenttypekey = g.paymenttypekey
inner join ads.ext_dim_customer h on a.customerkey = h.customerkey
left join ads.ext_dim_tech i on a.techkey = i.techkey
where h.fullname not in ('shop time','inventory')
  and a.linestatuskey = 1
order by ro, line
group by a.ro, a.line,
  c.the_date, d.the_date, b.the_date,
  e.ro_status, f.servicetype, g.paymenttype, h.fullname

-- shows void in accounting, still open in pdpphdr and ui
select * from ads.ext_Fact_repair_order where ro = '2776342'
select * from fin.fact_gl where control = '2776342'
  
select * from ads.ext_dim_ro_status

select * from open_ro a
left join arkona.ext_sdphist b on a.ro = b.repair_order_number


select * from arkona.ext_sdphist limit 100


select * from ads.ext_fact_repair_order where ro = '16315572'



-- menu priced service
select a.*
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.the_date between '06/01/2018' and current_date
inner join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetype = 'menu priced service'
inner join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
  and d.opcode = 'ALIGN'



sdpsvct: service types: accounting
sdpprice: labor rates

select * from ads.ext_dim_ro_status
select * from ads.ext_dim_service_type


select * from arkona.ext_pdptdet where ptinv_ = '16313888'
-- mechanical, can't seem to match parts (pdptdet) with ro
select * from ads.ext_fact_repair_order
where ro in (
select a.ro
from ads.ext_Fact_repair_order a
inner join dds.dim_date b on a.finalclosedatekey = b.date_key
  and b.the_date between '06/01/2018' and current_date
inner join ads.ext_dim_service_type c on a.servicetypekey = c.servicetypekey
  and c.servicetype = 'mechanical'
where roStatuskey = 1
  and storecode = 'ry1'
  and ropartssales > 100)
order by ro, line
  

-- what is the deal with pt 19162203 on line 999 ~ $600 ?!? not on ro
-- looking at part history, it was removed, ahh, ptcode = DP, ok, stick to lines, may be ok
select * from arkona.ext_pdptdet where ptinv_ = '16298324'
--ok, we are good now
select ptline, sum(ptqty * ptnet)
from arkona.ext_pdptdet where ptinv_ = '16298324'
group by ptline


-- 7/12/18 fuck it, going with a direct scrape of PDPPHDR/PDPPDET
drop table if exists jeri.open_ros cascade;
create table jeri.open_ros (
  the_date date not null,
  store_code citext not null,
  ro citext not null,
  customer citext not null,
  writer citext not null,
  status citext not null,
  service_type citext not null,
  payment_type citext not null,
  est_labor_hours_sold numeric(6,2) not null default 0,
  est_labor_gross numeric(6,2) not null default 0,
  est_parts_gross numeric(6,2) not null default 0,
  days_open integer not null default 0,
  days_in_cashier_status integer not null default 0,
  primary key (the_date,ro));
comment on column jeri.open_ros.the_date is 'date the row was generated';  
comment on table jeri.open_ros is 'direct daily scrape from arkona, PDPPHDR/PDPPDET';
create index on jeri.open_ros(store_code);
create index on jeri.open_ros(writer);
create index on jeri.open_ros(service_type);
create index on jeri.open_ros(the_date);



select * from jeri.open_ros

select ro, the_date
from jeri.open_ros
group by ro, the_date
having count(*) > 1

select nrv.test_get_ros(false, '["AM","BS"]','["All"]','["All"]','["All"]')


select the_date, store_code, service_type, payment_type, sum(est_labor_hours_sold) as flag_hours,
  sum(est_labor_gross) as labor_gross, sum(est_parts_gross) as parts_gross
from jeri.open_ros 
group by the_date, store_code, service_type, payment_type 
order by store_code, service_type, payment_type , the_date

select the_date, store_code, service_type, sum(est_labor_hours_sold) as flag_hours,
  sum(est_labor_gross) as labor_gross, sum(est_parts_gross) as parts_gross,
  sum(est_labor_gross + est_parts_gross) as total_gross,
  count(distinct ro) as ros
from jeri.open_ros 
group by the_date, store_code, service_type
order by store_code, service_type, the_date



-- ro statuses
select distinct status
from jeri.open_ros;
-- writers
select writer_name
from jeri.service_writers;
-- payment types
select payment_type_code
from jeri.service_payment_types;


drop table if exists jeri.service_payment_types;
create table jeri.service_payment_types (
  payment_type_code citext primary key,
  description citext not null);
insert into jeri.service_payment_types values
('I', 'Internal'),
('C', 'Customer Pay'),
('W', 'Warranty'),
('S', 'Service Contract');
  
drop table if exists jeri.service_writers;
create table jeri.service_writers (
  writer_name citext not null,
  employee_number citext not null,
  writer_id citext not null,
  primary key(writer_name));
create index on jeri.service_writers(writer_id);
create index on jeri.service_writers(employee_number);

insert into jeri.service_writers  
select max(name) as name, employeenumber, max(writernumber) as writernumber
from (
  select a.employeenumber, a.name, a.writernumber as writernumber, 
    case
      when a.name in ('BEAR, JEFF', 'DANGERFIELD, JOEL', 'LONGORIA,BEVERLEY ANNE')then 'MR'
      when a.name in ('POWELL,GAYLA R') then 'BS'
      else a.censusdept
    end as censusdept
  from ads.ext_dim_service_Writer a
  left join ads.ext_edw_employee_dim b on a.employeenumber = b.employeenumber
    and b.currentrow = true
  where a.currentrow = true
    and a.active 
    and a.censusdept <> 'HS'
    and a.name <> 'unknown'
    and left(a.employeenumber, 1) in ('1','2')
    and b.termdate > current_date
    and a.employeenumber not in ('1150510','1103050')) aa
where censusdept <> 'XX' 
group by employeenumber;


update jeri.open_ros x
set writer = y.id
from (
select a.ro, a.writer, b.*
from jeri.open_ros a
left join arkona.ext_sdpswtr b on a.writer = b.name
  and a.store_code = b.company_number
  and case when b.name = 'Ned Euliss' then b.id = '471' else 1 = 1 end
  and active = 'Y') y
where x.writer = y.name

select writer
from (
select writer, id
from (
select a.ro, a.writer, b.*
from jeri.open_ros a
left join arkona.ext_sdpswtr b on a.writer = b.name
  and a.store_code = b.company_number
  and case when b.name = 'Ned Euliss' then b.id = '471' else 1 = 1 end) c
group by writer, id) d
group by writer having count(*) > 1 

select *
from jeri.open_ros

update jeri.open_ros
set store_code = trim(store_code),
    ro = trim(ro),
    customer = trim(customer),
    writer = trim(writer),
    status = trim(status),
    service_type = trim(service_type),
    payment_type = trim(payment_type)

delete from jeri.open_ros where the_date = current_date


select * from jeri.service_Writers


select *
from jeri.open_ros a
left join jeri.service_Writers b on a.writer = b.writer_id


select *
from arkona.ext_sdpswtr a
where 
  (a.company_number = 'RY1' and id = '224')
  or
  (a.company_number = 'RY2' and id = '527')
  or
  (a.company_number = 'RY1' and id = '999')


insert into jeri.service_writers values 
('NEUMANN, ANDREW', '1102195', '224'),
('FRASSER, BRADEN','247000','527'),
('Arkokna Support', 'N/A','999');


  
update jeri.open_ros a
  set writer = (
    select writer_name
    from jeri.service_Writers
    where writer_id = a.writer)
where the_date = current_date     


select *
from jeri.open_ros
where est_labor_gross + est_parts_gross = 0

    
select writernumber from ads.ext_dim_service_writer where currentrow = true group by writernumber having count(*) > 1


select *
from ads.ext_dim_service_writer
where writernumber = '131'


delete from jeri.open_ros where the_date = current_date

select distinct the_Date from jeri.open_ros

insert into jeri.service_writers values ('Arkokna Support','N/A','999')

-- ok, this is good
create or replace function jeri.update_open_ro_writers()
  returns void as
$BODY$
Begin
/*
select jeri.update_open_ro_writers()
*/
update jeri.open_ros a
  set writer = (
    select writer_name
    from jeri.service_writers
    where writer_id = a.writer)
where the_date = current_date;

end
$BODY$
language plpgsql;
comment on function jeri.update_open_ro_writers() is 'updates jeri.open_ros.writer from the writer id extracted from arkona to the writers name as stored in jeri.service_writers. 
if the writer_id does not exist, function fails and the entire script fails.'

on weekend when afton not fucking with it
need to do the update function

-- 7/14
-- come up with a way to track ro status
select *
from jeri.open_Ros
where the_date = current_date
  and status = 'cashier'
  and days_in_cashier_status = 0;




CREATE OR REPLACE FUNCTION jeri.ro_exists(_ro citext)
  RETURNS boolean AS
$BODY$
select exists (
  select 1
  from jeri.open_ros
  where ro = _ro);
$BODY$
  LANGUAGE sql;
COMMENT ON FUNCTION nc.stock_number_exists(citext) IS 'verify the existence of an ro in jeri.open_ros, 
used in check constraint in table jeri.ro_status_days';


create table jeri.ro_status_days (
  ro citext not null check (jeri.ro_exists(ro)),
  status citext not null,
  from_date date not null,
  thru_date date not null default '12/31/9999'::date,
  primary key (ro,thru_date));
  
alter table jeri.ro_status_days add constraint date_check check(thru_date >= from_date);

-- need to backfill current data in preparation for processing new data
-- ros that changed status 7/13 - 7/14, only 18, but it is a start
insert into jeri.ro_status_days(ro,status,from_date,thru_date)
select b.ro, b.status, '07/14/2018', '12/31/9999'
from (
  select ro, status
  from jeri.open_Ros
  where the_date = current_date - 1) a
inner join (
  select ro, status
  from jeri.open_Ros
  where the_date = current_date) b on a.ro = b.ro  and a.status <> b.status

insert into jeri.ro_status_days(ro,status,from_date,thru_date)
select ro, status, current_date - days_in_cashier_status, '12/31/9999'
from jeri.open_ros a
where the_date = current_date
  and status = 'cashier'
  and days_in_cashier_status <> 0
  and not exists (
    select 1 
    from jeri.ro_status_days
    where ro = a.ro
      and thru_date > current_date);
      
-- these days look ok
select *
from jeri.open_ros a
left join jeri.ro_status_days b on a.ro = b.ro
where the_date = current_date  
order by days_in_cashier_status desc

-- cashier status, not in ro_status_days yet, latest sdphist date
insert into jeri.ro_status_days(ro,status,from_date,thru_date)
select x.ro, x.status, x.updated_time::date, '12/31/9999'
from (
  select a.*, d.updated_time, d.transaction_code
  from jeri.open_ros a
  left join jeri.ro_status_days b on a.ro = b.ro
  left join (
    select a.*, c.transaction_code
    from (
      select a.repair_order_number, max(updated_time) as updated_time
      from arkona.ext_sdphist a
      inner join jeri.open_ros b on a.repair_order_number = b.ro
      group by a.repair_order_number) a
    inner join arkona.ext_sdphist c on a.repair_order_number = c.repair_order_number  
      and a.updated_time = c.updated_time) d on a.ro = d.repair_order_number
  where a.the_date = current_date  
    and a.status = 'cashier'
    and b.ro is null) x;
    
-- and the few (79, mostly 0 days)
insert into jeri.ro_status_days(ro,status,from_date,thru_date)
select a.ro, a.status, current_date - days_open, '12/31/9999'
from jeri.open_ros a
left join jeri.ro_status_days b on a.ro = b.ro
where a.status = 'cashier'
  and b.ro is null;

-- ok, all ros in cashier status should now have a row in jeri.ro_status_days
select *
from jeri.open_ros a
left join jeri.ro_status_days b on a.ro = b.ro
where a.status = 'cashier' 
  and b.ro is null

select *
from jeri.open_ros a
inner join jeri.ro_status_days b on a.ro = b.ro
where a.the_date = current_date -1

-- open ros from 7/13 not in 7/14 which have rows in ro_status_days, close status
update jeri.ro_status_days
set thru_date = current_date 
where ro in (
  select a.ro
  -- select a.*, c.*
  from (
    select *
    from jeri.open_ros 
    where the_date = current_date - 1) a
  left join (
    select *
    from jeri.open_ros
    where the_date = current_date) b on a.ro = b.ro
  inner join jeri.ro_status_days c on a.ro = c.ro  
  where b.ro is null)
  
-- ros with no row in ro_status_days
-- let's get last event log date for cashier delayed close
insert into jeri.ro_status_days(ro,status,from_date,thru_date)
select x.ro, x.status, x.updated_time::date, '12/31/9999'
from (
  select a.*, d.*
  from jeri.open_ros a
  left join jeri.ro_status_days b on a.ro = b.ro
  left join (
      select a.*, c.transaction_code
      from (
        select a.repair_order_number, max(updated_time) as updated_time
        from arkona.ext_sdphist a
        inner join jeri.open_ros b on a.repair_order_number = b.ro
        group by a.repair_order_number) a
      inner join arkona.ext_sdphist c on a.repair_order_number = c.repair_order_number  
        and a.updated_time = c.updated_time) d on a.ro = d.repair_order_number
  where a.the_date = current_date
    and a.status = 'Cashier, Delayed Close'
    and b.ro is null) x

 -- ros with no ro in ro_status_days   
 -- for all the rest of these, create a row in ro_status_days based on days open
insert into jeri.ro_status_days(ro,status,from_date,thru_date)
select a.ro, a.status, current_date - days_open, '12/31/9999'
from jeri.open_ros a
left join jeri.ro_status_days b on a.ro = b.ro
where a.the_date = current_date
  and b.ro is null


-- days_in_cashier_status when ro is not in cashier status
-- none, good
select * 
from jeri.open_ros a
left join jeri.ro_status_days b on a.ro = b.ro
where the_date = current_date
  and a.status <> 'cashier'
  and days_in_cashier_status <> 0

-- for a given date, ros with no row in status_days
-- as it should be, none
select * 
from jeri.open_ros a
left join jeri.ro_status_days b on a.ro = b.ro
where the_date = current_date - 1
  and b.ro is null



-- when the ro disappears from jeri.open_ros, close the last open status
-- need to get rid of the sdphist derived cashier days in the python script

!!!!!!!!!!!!! BEFORE I RUN THE SCRIPT ON SUNDAY, REMOVE THE SDPHIST DATE ESTIMATING
so each day, 
1. new rows
2. changed rows
3. removed rows

-- 7/15
thinking all the new/change/removed is about status_days, once that is done, update current row
open_ros with days in cashier status

ok, this is where i need to be careful, 
even though the run date is today (early in the am), the ro was opened yesterday, so the status started yesterday


-- insert the new row
wait a minute, when an ro changes status
the status on the 14th was in process
the status on the 15th is cashier, delayed
the new status actually started on the 14th because of the early morning scrape
therefore the old status ended on the 13th, current_Date - 2


-- i believe this is a serious issue
thinking of doing a raw scrape, no processing, earlier and separate from  main script
that would help for some situations where it was the processing that caused the error (eg writer is null)

so what happens if the script fails for any reason, and i dont get it figured out until after ro work has been done
that would seem to undermine the whole "previous day" paradigm, ie i will now have ros that have changed on current_Date

move the scrape to luigi.ext_arkona

-- ok, processing has been moved into luigi, separate extract and update, that will have to be good enuf for now
-- now it's time to change jeri.update_open_ro_writers to jeri.update_open_ros with all the daily processing
-- working on localhost

DROP FUNCTION jeri.update_open_ro_writers();

CREATE OR REPLACE FUNCTION jeri.update_open_ros()
  RETURNS void AS
$BODY$
Begin
/*
select jeri.update_open_ros()

update jeri.open_ros a
set writer = (
  select writer_id
  from jeri.service_writers
  where writer_name = a.writer)
where the_date = current_date;

select * from jeri.open_ros where the_date = current_date;

*/
-- update writer id to writer name
update jeri.open_ros a
  set writer = (
    select writer_name
    from jeri.service_writers
    where writer_id = a.writer)
where the_date = current_date;

-- new ros
insert into jeri.ro_status_days (ro,status,from_date)
select a.ro, a.status, current_date - 1
from jeri.open_ros a
where a.the_date = current_date
  and not exists (
    select 1
    from jeri.open_ros
    where the_date = current_date - 1
      and ro = a.ro);
      
-- closed ros
update jeri.ro_status_days
set thru_date = current_date - 1
-- select * from jeri.ro_status_days
where thru_date > current_date 
  and ro in (
    select a.ro
    from jeri.open_ros a
    where a.the_date = current_date - 1
      and not exists (
        select 1
        from jeri.open_ros
        where the_date = current_date
          and ro = a.ro));

-- changed rows
-- update the old row
update jeri.ro_status_days
set thru_date = 
  case
    when from_date = '07/14/2018' then '07/14/2018'
    else current_date - 2
  end
where thru_date > current_date
  and ro in (
    select a.ro -- , a.status as previous_status, b.status as current_status
    from jeri.open_ros a
    inner join jeri.open_ros b on a.ro = b.ro
      and b.the_date = current_date
    where a.the_date = current_date - 1
      and a.status <> b.status);  
-- insert the new row
insert into jeri.ro_status_days(ro,status,from_date)
select a.ro, b.status as current_status, current_date -1
from jeri.open_ros a
inner join jeri.open_ros b on a.ro = b.ro
  and b.the_date = current_date
where a.the_date = current_date - 1
  and a.status <> b.status;    

-- update days in cashier for current date
update jeri.open_ros x
set days_in_cashier_status = y.days
from (
  select b.ro, current_date - b.from_date as days
  from jeri.open_ros a
  left join jeri.ro_status_days b on a.ro = b.ro
    and b.status = 'cashier'
    and b.thru_date > current_date
  where a.status = 'cashier'
    and the_date = current_date) y
where x.ro = y.ro
  and x.status = 'cashier'
  and x.the_date = current_date;
      
end
$BODY$
  LANGUAGE plpgsql;
COMMENT ON FUNCTION jeri.update_open_ros() IS 'updates jeri.open_ros.writer from the writer id extracted from arkona to the writers name as stored in jeri.service_writers. 
if the writer_id does not exist, function fails and the entire script fails. in addition it update ro statuses via table jeri.ro_status_days';



-- ros with multiple statuses
select * 
from jeri.open_ros a
left join jeri.ro_status_days b on a.ro = b.ro
where a.the_date = current_date 
  and a.ro in (
    select ro
    from (
    select ro, status
    from jeri.ro_Status_days
    group by ro, status) a
    group by ro 
    having count(*) > 1)
order by a.status, a.ro, b.from_date


select *
from jeri.open_ros







.


select *
from jeri.service_writers a

insert into jeri.service_writers
select a.writer_name, 'dealerfx', a.writer_number
from dds.dim_service_writer a
left join jeri.service_writers b on a.writer_number = b.writer_id
where a.active
  and a.writer_name like 'dfx%' or a.writer_name like 'satur%'











.