﻿select the_date, store_code, service_type, payment_type, sum(est_labor_hours_sold) as flag_hours,
  sum(est_labor_gross) as labor_gross, sum(est_parts_gross) as parts_gross
from jeri.open_ros
group by the_date, store_code, service_type, payment_type
order by the_date


select c.day_name, a.the_date, a.store_code, b.department, 
  sum(est_labor_hours_sold) as flag_hours,
    sum(est_labor_gross) as labor_gross, sum(est_parts_gross) as parts_gross
from jeri.open_ros a
left join jeri.service_types b on a.service_type = b.service_type
left join dds.dim_date c on a.the_date = c.the_date
group by c.day_name, a.the_date, a.store_code, b.department
order by a.store_code, b.department, the_date