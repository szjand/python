# coding=utf-8
'''
5/2/18
    Bev asked to have tech access to tool removed if they don't need it
    Midas needs it, no one else, she is getting bugged by techs looking in
    the tool and asking her when recon work is going to be released to
    the shop
    Anyway
    i have not been doing due maintenance, this is a good opportunity to do some
    clean up
    using the script from pto terms for removing tool access
'''

import db_cnx

ads_con = None
try:
    with db_cnx.ads_dpsvseries() as ads_con:
        with ads_con.cursor() as user_cur:
            # access never used
            sql = """
                SELECT a.username
                FROM users a
                WHERE a.active = true
                  and username in ('cferry','zwinkler')
            """
            user_cur.execute(sql)
            for row in user_cur.fetchall():
                print row[0]
                with ads_con.cursor() as cleanup_cur:
                    sql = """
                        execute procedure user_cleanup('{0}')
                    """.format(row[0])
                    cleanup_cur.execute(sql)
except Exception, error:
    print error
finally:
    if ads_con:
        ads_con.close()
