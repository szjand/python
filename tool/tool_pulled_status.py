# coding=utf-8
'''
run as a scheduled task on 10.130.196.22

8/27/17
instead of scraping all the base tables, simply scrpae the destination table: greg.tool_used_priorities
12/12/17
    extract(hour from the_time) = 21
    well, if, as i discovered, the time may be 20:59:59, retrun set was null
    change from = 21 to between 20 and 22

02/02/2020
    discontinued per ben cahalan
'''
############################################################
############################################################
# uncomment mail list before deploying
############################################################
############################################################

import db_cnx
import csv
import ops
import openpyxl as xl
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os

pg_con = None
ads_con = None
pg_server = '173'

#  tool_used_priorities
task = 'tool_used_priorities'
try:
    file_name = 'files/tool_used_priorities.csv'
    table_name = 'greg.tool_used_priorities'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                select a.stocknumber, a.currentpriority, c.yearmodel, c.make, c.model, c.exteriorcolor,
                (SELECT
                  CASE status
                    WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
                    WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
                    WHEN 'MechanicalReconProcess_NotStarted' THEN --'Not Started'
                      CASE
                        WHEN EXISTS (
                          SELECT 1
                          FROM vehicleinventoryitemstatuses
                          WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
                          AND category = 'MechanicalReconDispatched'
                          AND ThruTS IS NULL) THEN 'Dispatched'
                        ELSE 'Not Started'
                      END
                  END
                  FROM vehicleinventoryitemstatuses
                  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
                  AND category = 'MechanicalReconProcess'
                  AND ThruTS IS NULL) as mechanical_status,
                (SELECT
                  CASE status
                    WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
                    WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
                    WHEN 'BodyReconProcess_NotStarted' THEN --'Not Started'
                      CASE
                        WHEN EXISTS (
                          SELECT 1
                          FROM vehicleinventoryitemstatuses
                          WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
                          AND category = 'BodyReconDispatched'
                          AND ThruTS IS NULL) THEN 'Dispatched'
                        ELSE 'Not Started'
                      END
                  END
                  FROM vehicleinventoryitemstatuses
                  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
                  AND category = 'BodyReconProcess'
                  AND ThruTS IS NULL) as body_status,
                (SELECT
                  CASE status
                    WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
                    WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
                    WHEN 'AppearanceReconProcess_NotStarted' THEN --'Not Started'
                      CASE
                        WHEN EXISTS (
                          SELECT 1
                          FROM vehicleinventoryitemstatuses
                          WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
                          AND category = 'AppearanceReconDispatched'
                          AND ThruTS IS NULL) THEN 'Dispatched'
                        ELSE 'Not Started'
                      END
                  END
                  FROM vehicleinventoryitemstatuses
                  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
                  AND category = 'AppearanceReconProcess'
                  AND ThruTS IS NULL) as appearance_status,
                  curdate(),
                  current_time()
                -- select COUNT(*)
                from vehicleinventoryitems a
                inner join vehicleinventoryitemstatuses b
                    on a.vehicleinventoryitemid = b.vehicleinventoryitemid
                  and b.category = 'RMFlagPulled'
                  and b.thruts IS NULL
                inner join vehicleitems c on a.vehicleitemid = c.vehicleitemid
                where a.owninglocationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
except Exception, error:
    print error
    ops.email_error(task, '', error)

#  spreadsheet
task = 'spreadsheet'
file_name = 'files/status.csv'
try:
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select '0  stock #' as stock_number, 'year','make', 'model', 'color', -- header row for spreadsheet
                  case -- three days ago
                    when extract(dow from current_date) in (1,2,3) then to_char(current_date -4, 'Dy Mon DD')
                        || ': 9 PM Priority'
                    else to_char(current_date -3, 'Dy Mon DD') || ': 9 PM Priority'
                  end,
                  case -- 2 days ago
                    when extract(dow from current_date) in (1, 2) then to_char(current_date -3, 'Dy Mon DD')
                        || ': 9 PM Priority'
                    else to_char(current_date -2, 'Dy Mon DD') || ': 9 PM Priority'
                  end,
                  case -- 1 day ago
                    when extract(dow from current_date) = 1 then to_char(current_date -2, 'Dy Mon DD')
                        || ': 9 PM Priority'
                    else to_char(current_date -1, 'Dy Mon DD') || ': 9 PM Priority'
                  end,
                  to_char(current_date, 'Dy Mon DD') || ': 7 AM Priority',
                  to_char(current_date, 'Dy Mon DD') || ': 2 PM Priority',
                  to_char(current_date, 'Dy Mon DD') || ': 9 PM Priority',
                  case -- three days ago
                    when extract(dow from current_date) in (1,2,3) then to_char(current_date -4, 'Dy Mon DD')
                        || ': 9 PM Status'
                    else to_char(current_date -3, 'Dy Mon DD') || ': 9 PM Status'
                  end,
                  case -- 2 days ago
                    when extract(dow from current_date) in (1, 2) then to_char(current_date -3, 'Dy Mon DD')
                        || ': 9 PM Status'
                    else to_char(current_date -2, 'Dy Mon DD') || ': 9 PM Status'
                  end,
                  case -- 1 ay ago
                    when extract(dow from current_date) = 1 then to_char(current_date -2, 'Dy Mon DD')
                        || ': 9 PM Status'
                    else to_char(current_date -1, 'Dy Mon DD') || ': 9 PM Status'
                  end,
                  to_char(current_date, 'Dy Mon DD') || ': 7 AM Status',
                  to_char(current_date, 'Dy Mon DD') || ': 2 PM Status',
                  to_char(current_date, 'Dy Mon DD') || ': 9 PM Status'
                union
                select stock_number, model_year, make, model, color,
                  max(case when seq = 6 then current_priority end)::text as "3 days ago priority",
                  max(case when seq = 1 then current_priority end)::text as "2 days ago priority",
                  max(case when seq = 2 then current_priority end)::text as "yesterday priority",
                  max(case when seq = 3 then current_priority end)::text as "7 priority",
                  max(case when seq = 4 then current_priority end)::text as "2 priority",
                  max(case when seq = 5 then current_priority end)::text as "9 priority",
                  max(case when seq = 6 then status end) as "3 days ago status",
                  max(case when seq = 1 then status end) as "2 days ago status",
                  max(case when seq = 2 then status end) as "yesterday status",
                  max(case when seq = 3 then status end) as "2 status",
                  max(case when seq = 4 then status end) as "7 status",
                  max(case when seq = 5 then status end) as "9 status"
                from (
                  select 6 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_date =
                    case
                      when extract(dow from current_date) in (1, 2, 3) then current_date - 4
                      else current_date - 3
                    end
                    and extract(hour from the_time) between 20 and 22
                  union
                  select 1 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_date =
                    case
                      when extract(dow from current_date) in (1, 2) then current_date - 3
                      else current_date - 2
                    end
                    and extract(hour from the_time) between 20 and 22
                  union
                  select 2 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_date =
                    case
                      when extract(dow from current_date) = 1 then current_date - 2
                      else current_date - 1
                    end
                    and extract(hour from the_time) between 20 and 22
                  union
                  select 3 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_Date = current_date
                    and extract(hour from the_time) between 6 and 8
                  union
                  select 4 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_Date = current_date
                    and extract(hour from the_time) between 13 and 15
                  union
                  select 5 as seq, stock_number, current_priority, model_year, make, model, color,
                    case
                      when mech_status = 'no open items' and body_status = 'no open items'
                            and app_status = 'no open items' then 'All Recon Done'
                    else
                      'M:'||case mech_status when 'no open items' then 'Done' when 'dispatched' then 'Disp'
                            when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'B:'||case body_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                      || ' - ' || 'A:'||case app_status when 'no open items' then 'Done'
                            when 'dispatched' then 'Disp' when 'Not Started' then 'Open' when 'WIP' then 'WIP ' end
                    end as status
                  from greg.tool_used_priorities
                  where the_Date = current_date
                    and extract(hour from the_time) between 20 and 22) x
                group by stock_number, model_year, make, model, color
                order by stock_number;
            """
            pg_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(pg_cur)
    wb = xl.Workbook()
    ws = wb.active
    with open(file_name) as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            ws.append(row)
        f.close()
    wb.save('files/pulled_statuses.xlsx')
except Exception, error:
    print error
    ops.email_error(task, '', error)

#  email
task = 'email'
try:
    COMMASPACE = ', '
    sender = 'jandrews@cartiva.com'
    recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
    # recipients = ['jonfandrews@gmail.com', 'jandrews@cartiva.com', 'bcahalan@rydellcars.com','rsattler@rydellcars.com',
    #               'jgardner@rydellcars.com', 'aneumann@rydellcars.com','blongoria@rydellcars.com','bknudson@rydellcars.com']
    outer = MIMEMultipart()
    outer['Subject'] = 'Pulled Vehicle Statuses'
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    # outer.attach(MIMEText(body))
    attachments = ['files/pulled_statuses.xlsx']
    for file in attachments:
        try:
            with open(file, 'rb') as fp:
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read())
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(file))
            outer.attach(msg)
        except:
            # print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
            raise
    composed = outer.as_string()
    # this works with Python2
    e = smtplib.SMTP('mail.cartiva.com')
    try:
        e.sendmail(sender, recipients, composed)
        # print "Successfully sent email"
    except smtplib.SMTPException:
        print "Error: unable to send email"
    e.quit()
except Exception, error:
    print error
    ops.email_error(task, '', error)
