﻿variable gross includes doc fees P3L67
fixed gross includes parts split

select *
from fin.dim_fs_org
order by store, area, department, sub_department

select *
from fin.dim_fs
where year_month = 202011

select *
from fin.dim_fs_account
limit 100


/*
THE PURPOSE OF THESE INITIAL QUERIES IS SIMPLY TO MATCH THE FLEX BUDGET SPREADSHEET
*/

-- fixed expenses, from the statement, the heading subtotals (lines 17, 40, 56)
-- matches flex and statement
select b.year_month, c.department, 
  case
    when c.sub_department like '%wash' then  'carwash'
    else c.sub_department
  end,
  sum(a.amount) filter (where b.line between 8 and 16) as personnel,
  sum(a.amount) filter (where b.line between 18 and 39) as semi_fixed,
  sum(a.amount) filter (where b.line between 41 and 54) as fixed
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201911 and 202011
  and b.page = 4
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'RY1' 
  and c.area = 'fixed'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
  and d.current_row
group by b.year_month, c.department, 
  case
    when c.sub_department like '%wash' then  'carwash'
    else c.sub_department
  end
order by b.year_month, c.department;  

-- fixed gross, includes parts split, total  = P4L2 + P4L59
-- gross problems
-- parts parts split not included in parts gross  - 60427
-- acct 147701 routes to body shop gross instead of parts gross  17294

-- P16L49
-- account 147701, is configured as body shop, but on the flex budget goes to parts
-- jon: It appears that account 147701 is routed to parts gross rather than body shop gross, Is that the way it should be?
-- jeri: It is, but it’s weird GM accounting with the split. It goes to parts and then I assign the split percentage to body shop. 100% is assigned. 

-- the solution is figure gross for the parts department separately from the rest of fixed

-- fixed (without parts) gross, includes parts split, total  = P4L2 + P4L59
select b.year_month,  c.department, 
  case
    when c.sub_department like '%wash' then  'carwash'
    else c.sub_department
  end as sub_department,
  sum(-amount) as total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201911 and 202011
  and (
    (b.page = 16 and b.line between 20 and 43)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'RY1'
  and c.area = 'fixed'
  and c.department <> 'parts'
group by b.year_month, c.store, c.department, 
  case
    when c.sub_department like '%wash' then  'carwash'
    else c.sub_department
  end
order by b.year_month, c.department

-- parts gross
-- control the fs lines by page and line, can't control by department as parts split would be left out
select b.year_month, 
  sum(case when b.page = 4 then a.amount else - a.amount end) as total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201911 and 202011
  and (
    (b.page = 16 and b.line between 45 and 61)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'RY1'
group by b.year_month

-- variable expenses
select b.year_month, c.department, 
  sum(a.amount) filter (where b.line between 4 and 6) as variable,
  sum(a.amount) filter (where b.line between 8 and 16) as personnel,
  sum(a.amount) filter (where b.line between 18 and 39) as semi_fixed,
  sum(a.amount) filter (where b.line between 41 and 54) as fixed
from fin.fact_fs a
join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201911 and 202011
  and b.page = 3
join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'RY1' 
  and c.area = 'variable'
join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
  and d.current_row
group by b.year_month, c.department
order by b.year_month

-- variable gross
select b.year_month, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201911 and 202011 -- = 202011
  and (
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20) -- f/i
    or
    (b.page = 3 and b.line = 67)) -- doc fees
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
group by c.store, b.year_month
order by c.store, b.year_month;
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
drop table if exists tem.flex_budget_actual cascade;
create table tem.flex_budget_actual (
  department citext not null, 
  year_month integer not null,
  gross integer,
  variable_expense integer,
  personnel_expense integer,
  semi_fixed_expense integer,
  fixed_expense integer,
  net_income integer,
  primary key (department,year_month),
  check (department in ('variable','main shop','pdq','detail','carwash','body shop','parts','store')));
create index on tem.flex_budget_actual(year_month);


-- variable gross
insert into tem.flex_budget_actual (department, year_month, gross)
select 'variable', b.year_month, sum(-amount)::integer
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201811 and 202011 -- = 202011
  and (
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20) -- f/i
    or
    (b.page = 3 and b.line = 67)) -- doc fees
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
group by c.store, b.year_month;

-- variable expenses
update tem.flex_budget_actual x
set variable_expense = y.variable,
    personnel_expense = y.personnel,
    semi_fixed_expense = y.semi_fixed,
    fixed_expense = y.fixed
from (    
  select b.year_month, c.department, 
    sum(a.amount) filter (where b.line between 4 and 6) as variable,
    sum(a.amount) filter (where b.line between 8 and 16) as personnel,
    sum(a.amount) filter (where b.line between 18 and 39) as semi_fixed,
    sum(a.amount) filter (where b.line between 41 and 54) as fixed
  from fin.fact_fs a
  join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201811 and 202011
    and b.page = 3
  join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
    and c.store = 'RY1' 
    and c.area = 'variable'
  join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
    and d.current_row
  group by b.year_month, c.department) y
where x.department = 'variable'
  and x.year_month = y.year_month;  

-- fixed  gross (exc parts)
insert into tem.flex_budget_actual (department, year_month, gross)
select 
  case 
    when sub_department = 'mechanical' then 'main shop'
    when sub_department = 'pdq' then 'pdq'
    when sub_department = 'detail' then 'detail'
    when sub_department = 'carwash' then 'carwash'
    when department = 'body shop' then 'body shop'
  end as department, year_month, total as gross
from (  
  select b.year_month,  c.department, 
    case
      when c.sub_department like '%wash' then  'carwash'
      else c.sub_department
    end as sub_department,
    sum(-amount) as total
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month between 201811 and 202011
    and (
      (b.page = 16 and b.line between 20 and 43)
      or
      (b.page = 4 and b.line = 59)) -- parts split
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.store = 'RY1'
    and c.area = 'fixed'
    and c.department <> 'parts'
  group by b.year_month, c.store, c.department, 
    case
      when c.sub_department like '%wash' then  'carwash'
      else c.sub_department
    end) aa;
        
-- parts gross
insert into tem.flex_budget_actual (department, year_month, gross)
select 'parts', b.year_month, 
  sum(case when b.page = 4 then a.amount else - a.amount end) as total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201811 and 202011
  and (
    (b.page = 16 and b.line between 45 and 61)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'RY1'
group by b.year_month;
   
-- fixed expenses
  update tem.flex_budget_actual x
  set personnel_expense = y.personnel,
      semi_fixed_expense = y.semi_fixed,
      fixed_expense = y.fixed
  from (   
    select 
      case 
        when sub_department = 'mechanical' then 'main shop'
        when sub_department = 'pdq' then 'pdq'
        when sub_department = 'detail' then 'detail'
        when sub_department = 'carwash' then 'carwash'
        else department -- body shop & parts
      end as department, year_month, personnel, semi_fixed, fixed
    from (       
      select b.year_month, c.department, 
        case
          when c.sub_department like '%wash' then  'carwash'
          else c.sub_department
        end,
        sum(a.amount) filter (where b.line between 8 and 16) as personnel,
        sum(a.amount) filter (where b.line between 18 and 39) as semi_fixed,
        sum(a.amount) filter (where b.line between 41 and 54) as fixed
      from fin.fact_fs a
      join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month between 201811 and 202011
        and b.page = 4
      join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
        and c.store = 'RY1' 
        and c.area = 'fixed'
      join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
        and d.current_row
      group by b.year_month, c.department, 
        case
          when c.sub_department like '%wash' then  'carwash'
          else c.sub_department
        end) aa) y
  where x.department = y.department
    and x.year_month = y.year_month;  

-- net income
update tem.flex_budget_actual
set net_income = gross - coalesce(variable_expense, 0) - personnel_expense - semi_fixed_expense - fixed_expense;   

-- store
insert into tem.flex_budget_actual(department,year_month,gross,variable_expense,
  personnel_expense,semi_fixed_expense,fixed_expense,net_income)
select 'store', year_month, sum(gross) as gross, sum(coalesce(variable_expense, 0)), sum(coalesce(personnel_expense, 0)),
  sum(coalesce(semi_fixed_expense, 0)), sum(coalesce(fixed_expense, 0)),sum(coalesce(net_income, 0))
from  tem.flex_budget_actual
group by year_month
order by year_month;


select * from  tem.flex_budget_actual order by year_month, department



