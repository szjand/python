﻿-- 01/14/2020, new journal: DEP
                select count (*)
                from (
                    select * from arkona.ext_glpjrnd
                    except
                    select * from arkona.ext_glpjrnd_tmp
                union all (
                    select * from arkona.ext_glpjrnd_tmp
                    except
                    select * from arkona.ext_glpjrnd)) x

select * from arkona.ext_glpjrnd order by journal_code          

-- add row to arkona.ext_glpjrnd
insert into arkona.ext_glpjrnd
select * from arkona.ext_glpjrnd_tmp
except
select * from arkona.ext_glpjrnd         

-- add row to fin.dim_journal
-- yikes, RY5 !?!?!
select *
from arkona.ext_glpjrnd a
where not exists (
  select 1
  from fin.dim_journal
  where journal_code = a.journal_code)

-- add row to fin.dim_journal
insert into fin.dim_journal(journal_code,journal, row_from_date,current_row,row_reason)
select journal_code, journal_desc, current_date - 1, true, 'new journal created by controller'
from arkona.ext_glpjrnd
where journal_code = 'DEP'
