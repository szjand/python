﻿drop table if exists acct;
create temp table acct as
select c.store_code, b.the_date, 
  a.trans, a.seq, a.control, a.doc, a.ref, a.amount, 
  c.account, c.account_type, c.department, c.description as acct_desc,
  d.journal_code, d.journal, 
  e.description
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
  and b.the_date between c.row_from_date and c.row_thru_date
inner join fin.dim_journal d on a.journal_key = d.journal_key
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
where a.post_status = 'Y'
  and b.the_date > current_date - 180

-- 
-- select count(*) from fin.dim_gl_description -- 1,049,190
-- select count(*) from fin.fact_gl -- 15,923,656
-- 

select current_date - 365

select *
from fin.dim_account
where description like '%hold%'



drop table if exists board_data cascade;
create temp table board_data as
select a.board_id, a.boarded_ts::date as board_date, d.arkona_store_key, c.last_name as boarded_by, b.board_type, b.board_sub_type,
  e.vehicle_type, a.deal_number, a.stock_number, a.vin, 
  case when a.is_deleted then 'DELETED' else null end as deleted, 
  case when a.is_backed_on then 'BACK_ON' else null end as back_on, 
  e.sale_code, vehicle_make
-- select *
from board.sales_board a
left join board.board_types b on a.board_type_key = b.board_type_key
left join nrv.users c on a.boarded_by = c.user_key
left join onedc.stores d on a.store_key = d.store_key
left join (
  select vehicle_type, board_id, vehicle_make, customer_name, sale_code
  from board.daily_board 
  group by vehicle_type, board_id, vehicle_make, customer_name, sale_code) e on a.board_id = e.board_id; 
create index on board_data(stock_number);
create index on board_data(vin);
create index on board_data(board_type);


-- acquired dealer trades:
select stock_number 
from board_data 
where board_type = 'addition' 
  and board_sub_type = 'dealer trade'

select *
from acct
where control in (
select max(stock_number) as stock_number
from sls.deals_by_month
where stock_number in (
select stock_number from board_data where board_type = 'addition' and board_sub_type = 'dealer trade')
group by make)
order by control, journal_code, account

