﻿-- source (209) is the remote postgres server from where the tables are accessed by the destination database server as foreign tables.
-- destination (173) is another postgres server where the foreign tables are created which is referring tables in source database server.

create schema test_fdw;
comment on schema test_fdw is 'testing import foreign schema, destination schema for importing schema fin from pg_standby_fdw,
  which is a restored backup of 173';

create server pg_standby_fdw
foreign data wrapper postgres_fdw
options(dbname 'cartiva', host '10.130.196.209', port '5432')

create user mapping for current_user
server pg_standby_fdw
options(user 'rydell', password 'cartiva');

import foreign schema fin from server pg_standby_fdw into test_fdw;



-- ok, looks like nothing got fucked up beyond the deletion of 202101 when trying
-- to test an old version of fact_fs_monthly_update
select * 
from (
  select b.page, b.line, b.line_label, sum(a.amount) as amount
  from fin.fact_fs a
  join fin.dim_Fs b on a.fs_key = b.fs_key
  join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  where b.year_month = 202012
  group by b.page, b.line, b.line_label) aa
left join (
  select b.page, b.line, b.line_label, sum(a.amount) as amount
  from test_fdw.fact_fs a
  join test_fdw.dim_Fs b on a.fs_key = b.fs_key
  join test_fdw.dim_fs_org c on a.fs_org_key = c.fs_org_key
  join test_fdw.dim_fs_account d on a.fs_account_key = d.fs_account_key
  where b.year_month = 202012
  group by b.page, b.line, b.line_label) bb on aa.page = bb.page and aa.line = bb.line 
where aa.amount <> bb.amount


select  * -- 1318 rows
from test_fdw.fact_fs a
where not exists (
  select 1
  from fin.fact_fs
  where fs_key = a.fs_key
    and fs_org_key = a.fs_org_key
    and fs_account_key = a.fs_account_key);

select * -- 1318
from test_fdw.fact_fs a
join test_fdw.dim_fs b on a.fs_key = b.fs_key    
where b.year_month = 202101

-- check against the fs
select c.store, b.page, b.line, b.line_label, b.col, sum(a.amount) as amount
from test_fdw.fact_fs a
join test_fdw.dim_Fs b on a.fs_key = b.fs_key
join test_fdw.dim_fs_org c on a.fs_org_key = c.fs_org_key
join test_fdw.dim_fs_account d on a.fs_account_key = d.fs_account_key
where b.year_month = 202101 
group by c.store, b.page, b.line, b.col, b.line_label
order by c.store, b.page, b.line, b.col

-- total expenses
select c.store, b.page, b.col, sum(a.amount) as amount
from test_fdw.fact_fs a
join test_fdw.dim_Fs b on a.fs_key = b.fs_key
join test_fdw.dim_fs_org c on a.fs_org_key = c.fs_org_key
join test_fdw.dim_fs_account d on a.fs_account_key = d.fs_account_key
where b.year_month = 202101 and page in (16) and line between 21 and 62
group by c.store, b.page, b.col
order by c.store, b.page, b.col



select c.store, b.page, b.line, b.line_label, 
  sum(a.amount) filter (where b.col = 1) as col_1,
  sum(a.amount) filter (where b.col = 2) as col_2,
  sum(a.amount) filter (where b.col = 3) as col_3
from test_fdw.fact_fs a
join test_fdw.dim_Fs b on a.fs_key = b.fs_key
join test_fdw.dim_fs_org c on a.fs_org_key = c.fs_org_key
join test_fdw.dim_fs_account d on a.fs_account_key = d.fs_account_key
where b.year_month = 202101 and page in (16) and line between 21 and 63
group by c.store, b.page, b.line, b.line_label--, b.col
order by c.store, b.page, b.line, b.line_label--, b.col



