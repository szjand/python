﻿/*
does this save the right stuff
12/14/16
fixed: 201611, 201610

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FEB 2017: new accounts to be added to dim_fs_account
  164501

3/7/17 working on page 2, went back to 201608, 201609, starting to get some separation
          ran this script for those months, bang back on the money
3/23/17
  *a* 
    tried to restore 201702 (while killing myself over page 2) and it failed, generating
    rows with null fs_account_key, changed to inner join between arkona.ext_eisglobal_sypffxmst  and arkona.ext_ffpxrefdta  
    was generating rows for gm accounts for which there is no routing in 2017

7/4/17
  *b*
  new department in fin.dim_account: AO:Auto Outlet, goes to used cars
  *c*
  P6L30 adjusted cost of labor way off: accounts 166504 & 166504A had a type 2 update
  in june (automatically done in luig script) changing the account type, need to 
  specifiy current row - or the appropriate row !!!
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
*/

/*
2/26/21
  in the recovery phase after losing fact_fs_monthly_update_including_page_2.sql, 
  lets see if this generates correctly for 202101
  oh fuck me, i may have just fucked up the dimensions running this with all the fin dimensions
  into a tem.fact_fs table
  and fuck of course, deleted from fin.fact_fs where year_month  = 202101

  time to see if i can restore a table from a backup

  ok, that worked ok, but to play with this stuff i have created a schema tem_fs with the fin tables
  
*/
do
$$
declare 
   _year integer := 2021;
   _year_month integer := 202101;
begin

delete from fin.fact_fs
where fs_key in (
  select fs_key
  from fin.dim_fs 
  where year_month = _year_month);
  
delete from fin.dim_fs where year_month = _year_month;

insert into fin.dim_fs (the_year,year_month,page,line,col,line_label, row_from_date, current_row)  
select _year, _year_month, flpage, flflne, coalesce(c.fxmcol, -1) as col,
  case -- multi valued labels with large internal block of spaces
    when line_label = 'ASSETS                 AMOUNT' 
      then 'ASSETS'
    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
      then 'EXPENSES'
    else line_label
  end as line_label, current_date, true
from ( -- year, page, line, line_label
  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
  from (
    select flcyy, flpage, flflne,       
    case
      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
      else fldata
    end as line_label
    from arkona.ext_eisglobal_sypfflout
--     where flcode = 'GM'
--       and flcyy > 2010 
    where flflsq = 0 
      and flflne > 0
      and flpage < 18) a
  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b
left join ( -- columns
  select fxmcyy, fxmpge, fxmlne, fxmcol
  from arkona.ext_eisglobal_sypffxmst
--   where fxmcyy > 2010
  group by fxmcyy, fxmpge, fxmlne, fxmcol) c  on b.flcyy = c.fxmcyy and b.flpage = c.fxmpge and b.flflne = c.fxmlne
where b.flcyy = _year; 

truncate fin.xfm_fs_1;  
insert into fin.xfm_fs_1  
select _year, _year_month,
  case -- 147701/167701 100% body shop, no parts split
    when b.g_l_acct_number in ('147701', '167701') then 16
    else a.fxmpge
  end as page, 
  case 
    when b.g_l_acct_number in ('147701', '167701') then 49
    else a.fxmlne
  end as line, 
  case 
    when b.g_l_acct_number = '147701' then 2
    when b.g_l_acct_number = '167701' then 3
    else a.fxmcol
  end as col, a.fxmact, 
  coalesce(
    case 
      when b.g_l_acct_number like '%SPLT%' then replace(b.g_l_acct_number,'SPLT','')::citext
      else b.g_l_acct_number
    end, 'none') as gl_account
from arkona.ext_eisglobal_sypffxmst a
-- *a*
inner join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
where a.fxmpge < 18
  and a.fxmcyy = _year;

-- parts split
-- drop table if exists parts_split;
truncate fin.parts_split;
-- create temp table parts_split as  
insert into fin.parts_split
select a.the_year, a.year_month, 4 as page, 59.0 as line, 
  case 
    when a.gm_account in ('477s','677s') then 11 -- body shop
    else 1
  end as col, 
  a.gm_account, replace(a.gl_account,'SPLT','')::citext as gl_account
-- select *  
from fin.xfm_fs_1 a
where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s'))
  or (a.gl_account in ('246700D','246701D'))) order by gl_Account;  

delete from fin.xfm_fs_1
where page = 4 
  and line = 59;

insert into fin.xfm_fs_1 (the_year,year_month,page,line,col,gm_account,gl_account) 
select *
from fin.parts_split;  

insert into fin.fact_fs
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  round( -- this is the parts split
    sum(
      case 
        when b.page = 4 and b.line = 59 and c.gl_account not in ('147701','167701') then round(coalesce(f.amount, 0) * .5, 0) 
        else coalesce(f.amount, 0)
      end), 0) as amount  
from fin.xfm_fs_1 a
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.gm_account = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
-- *c*
  and d.current_row = true
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case -- parts split
      when b.page = 4 and b.line = 59 then 
        case
          when c.gm_account in ('467S','667S', '468S','668S' ) then e.sub_department = 'mechanical'
          when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
          when c.gm_account in ('477s','677s') then e.department = 'body shop'
          when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
          when c.gl_account = '246701D' then e.sub_department = 'pdq'
        end 
      else   
        case 
          when d.department = 'Body Shop' then e.department = 'body shop'
          when d.department = 'Car Wash' then e.sub_Department = 'car wash'
          when d.department = 'Detail' then e.sub_department = 'detail'
          when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
          -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
          when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
          when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
          when d.department = 'Parts' then e.department = 'parts'
          when d.department = 'Quick Lane' then e.sub_department = 'pdq'
          when d.department = 'Service' then e.sub_department = 'mechanical'
          when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
--*b*          
          when d.department = 'Auto Outlet' then e.department = 'sales' and e.sub_department = 'used'
        end
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = _year_month
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key); 

end
$$;

/*
-- 
-- 201606 parts split ok
select b.year_month, c.store, b.col, sum(a.amount) june
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
group by b.year_month, c.store, b.col
having sum(a.amount) <> 0
-- 
--variable expenses
select store, 
  sum(case when b.col = 1 and b.line between 4 and 6 then amount end) as "New Variable",
  sum(case when b.col = 5 and b.line between 4 and 6 then amount end) as "Used Variable",
  sum(case when b.col = 1 and b.line between 8 and 16 then amount end) as "New Personnel",
  sum(case when b.col = 5 and b.line between 8 and 16 then amount end) as "Used Personnel",  
  sum(case when b.col = 1 and b.line between 18 and 39 then amount end) as "New Semi-Fixed",
  sum(case when b.col = 5 and b.line between 18 and 39 then amount end) as "Used Semi-Fixed",  
  sum(case when b.col = 1 and b.line between 41 and 54 then amount end) as "New Fixed",
  sum(case when b.col = 5 and b.line between 41 and 54 then amount end) as "Used Fixed",
  sum(case when b.col = 1 then amount end) as "New Total",
  sum(case when b.col = 5 then amount end) as "Used Total"
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and b.page = 3
--   and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.area = 'variable' 
group by store
having sum(amount) <> 0  
order by store

-- variable expenses by line
select store, line, line_label, 
  sum(case when col = 1 then amount else 0 end) as New,
  sum(case when col = 5 then amount else 0 end) as Used
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and b.page = 3
--   and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.area = 'variable' 
group by store, line, line_label  
order by store, line
-- 
-- 
----------------------------------------------------------------------------------------------------------------------------------------
-- ry1 variable personnel accounts/amounts by line
create temp table fs as
select line, line_label, d.gl_account, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and b.page = 3
  and b.line between 8 and 17
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.area = 'variable' 
  and c.store = 'ry1'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
group by line, line_label,d.gl_account 
order by line

-- ry1 variable personnel accounts/amounts/person by line

select * 
from fs a
inner join  join 

drop table personnel_detail;
create temp table personnel_detail as 
select m.*, n.name, n.employeenumber
-- select sum(amount)
from (
  select g.control, f.account, b.line, b.line_label, sum(g.amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201701
    and b.page = 3
    and b.line between 8 and 17
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
    and c.area = 'variable' 
    and c.store = 'ry1'
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
  inner join dds.dim_date e on b.year_month = e.year_month
  inner join fin.dim_account f on d.gl_account = f.account
  inner join fin.fact_gl g on f.account_key = g.account_key
    and e.date_key = g.date_key
    and g.post_status = 'Y'
  group by g.control, b.line, b.line_label, f.account) m
left join ads.ext_dds_edwemployeedim n on m.control = n.employeenumber
  and n.currentrow = true
order by m.control

select name, control, line, account, sum(amount) as amount
-- select sum(amount)
from personnel_detail
group by name, control, line, account
order by line

select * 
from personnel_detail

----------------------------------------------------------------------------------------------------------------------------------------------

-- 
-- -- expenses - sub_departments
select year_month, store, department, sub_department,
  sum(case when line between 8 and 17 then amount end) as personnel,
  sum(case when line between 18 and 40 then amount end) as semifixed,
  sum(case when line between 41 and 56 then amount end) fixed,
  sum(case when line between 8 and 56 then amount end) total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 4
  and b.line < 58 -- expenses only, exclude parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
group by year_month, store, department, sub_department
having sum(amount) <> 0  
order by year_month, store, department, sub_department

-- -- expenses - departments P4
select year_month, store, department, 
  sum(case when line between 8 and 17 then amount end) as personnel,
  sum(case when line between 18 and 40 then amount end) as semifixed,
  sum(case when line between 41 and 56 then amount end) fixed,
  sum(case when line between 8 and 56 then amount end) total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and b.page = 4
  and b.line < 58 -- expenses only, exclude parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
group by year_month, store, department
having sum(amount) <> 0  
order by year_month, store, department

-- 
-- -- variable P2L2,  P6L18
-- GOOD
select c.store, b.year_month, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and (
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20))-- f/i
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, b.year_month
having sum(amount) <> 0
order by c.store, b.year_month;

-- budget gross
-- fixed
-- all except parts
select b.year_month, c.store, c.department, c.sub_department, 
  sum(case when b.page = 16 then amount end) as gross,
  sum(case when b.page = 4 then amount end) as parts_split,
  sum(amount) as total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and (
    (b.page = 16 and b.line between 20 and 42)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
group by b.year_month, c.store, c.department, c.sub_department
having sum(amount) <> 0
order by c.store, c.department, c.sub_department, b.year_month;   

-- budget gross
-- fixed 2 categories: body shop and service (group by  department)
--   for comparing to fs
-- all except parts
-- P4L2
select b.year_month, c.store, c.department, 
  sum(case when b.page = 16 then amount end) as gross,
  sum(case when b.page = 4 then amount end) as parts_split,
  sum(amount) as total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and (
    (b.page = 16 and b.line between 20 and 42)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
group by b.year_month, c.store, c.department
having sum(amount) <> 0
order by c.store, c.department, b.year_month;

-- parts P6-L61
select b.year_month, c.store, 
  sum(case when b.page = 16 then a.amount end) as gross, 
  sum(case when b.page = 4 then a.amount end) as parts_split, 
  sum(case when b.page = 4 then -1 * a.amount else a.amount end) as total
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and (
    (b.page = 16 and b.line between 45 and 61)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by b.year_month, c.store
having sum(amount) <> 0
order by c.store, b.year_month
-- 


--New Sales/COGS/Gross
-- P3L2
select c.store, b.year_month, 
  sum(case when col = 1 then -amount end) as sales,
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201705
  and b.page between 5 and 15 -- new cars
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, b.year_month
having sum(amount) <> 0
order by c.store, b.year_month;

--New Sales/COGS/Gross
-- P3L2 by page, line
select c.store, b.year_month, b.page, b.line, d.gl_account,
  sum(case when col = 1 then -amount end) as sales,
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201706
  and b.page between 5 and 15 -- new cars
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
where page = 5 and line = 31
group by c.store, b.year_month, b.page, b.line, d.gl_account
having sum(amount) <> 0
order by c.store, b.year_month, b.page, b.line, d.gl_account

-- Used Sales/COGS/Gross
-- P3L2
select c.store, b.year_month, 
  sum(case when col = 1 then -amount end) as sales,
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201705
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, b.year_month
having sum(amount) <> 0
order by c.store, b.year_month;

-- Used Sales 
-- P6L1-14
select c.store, b.year_month, b.line,
  sum(case when col = 1 then -amount end) as sales,
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201705
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
--   and c.store = 'ry1'
group by c.store, b.year_month, line
having sum(amount) <> 0
order by c.store, b.line;

-- New F&I
-- P7L10
select c.store, b.year_month, 
  sum(case when col = 1 then -amount end) as sales,
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201704
  and b.page = 17
  and b.line between 1 and 10
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, b.year_month
having sum(amount) <> 0
order by c.store, b.year_month;

-- Used F&I
-- P7L20
select c.store, b.year_month, 
  sum(case when col = 1 then -amount end) as sales,
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201704
  and b.page = 17
  and b.line between 11 and 20
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, b.year_month
having sum(amount) <> 0
order by c.store, b.year_month;

*/



/*
-- used car gross accounts
-- ry1 by line, sales only
select d.gl_account, b.line, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
  and e.account_type_code = '4'
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'  
  

-- what are the new car gross accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page between 5 and 15 -- new cars
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
union
-- used car gross accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
union
-- f/i accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 17
  and b.line between 1 and 20
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account

-- new car expense accounts
select distinct f.store, b.line, b.line_label, d.gl_account, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 3
  and b.col = 1
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, b.line, d.gl_account


-- new car semi-fixed accounts
select distinct f.store, b.line, b.col, b.line_label, d.gl_account, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 3
  and b.line between 18 and 40
  and b.col = 1
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, b.line, d.gl_account

-- used car semi-fixed accounts
select distinct f.store, b.line, b.col, b.line_label, d.gl_account, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 3
  and b.line between 18 and 40
  and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, b.line, d.gl_account

-- fixed semi-fixed accounts
select distinct f.store, b.line, b.col, b.line_label, d.gl_account, e.description, f.*
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 4
  and b.line between 18 and 40
--   and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, f.department, f.sub_department, b.line, d.gl_account


-- semi-fixed accounts
select distinct f.department, f.sub_department, f.store, b.line, b.col, b.line_label, d.gl_account, e.description 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page in (3,4)
  and b.line between 18 and 40
--   and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, f.department, f.sub_department, b.line, d.gl_account



-- just the sales accounts for body shop page 6
select distinct d.gl_account
-- select sum(a.amount) -- total bs sales P6L43
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page =16
  and b.line between 35 and 43
   and b.col in (1,2)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
union
-- parts split sales: 147701: GM bs gets 1/2, 147700: non-gm body shop gets all
select distinct d.gl_account
-- select sum(a.amount) -- total bs sales P6L43
-- select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store = 'ry1'
  and f.department = 'body shop'
inner join fin.dim_account e on d.gl_account = e.account  
  and e.account_type = 'sale'

-- body shop sales
select b.page, b.line, b.line_label,-- d.gl_account, d.gm_account,
  sum(case when e.account_type = 'Sale' then amount else 0 end) as sales,
  sum(case when e.account_type in ('cogs','expense') then amount else 0 end) as cogs
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
  and (
    (b.page = 16 and b.line between 20 and 42)
    or
    (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.department = 'body shop'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
inner join fin.dim_account e on d.gl_account = e.account
group by b.page, b.line, b.line_label
order by b.page, b.line

-- bs sales totals (without parts)
select 
  sum(case when e.account_type = 'Sale' then amount else 0 end) as sales,
  sum(case when e.account_type in ('cogs','expense') then amount else 0 end) as cogs
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and (
    (b.page = 16 and b.line between 20 and 42))
--     or
--     (b.page = 4 and b.line = 59)) -- parts split
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.department = 'body shop'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
inner join fin.dim_account e on d.gl_account = e.account

-- unit counts -----------------------------------------------------------------------------------------------------------------
drop table if exists step_1;
create temp table step_1 as
include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201603
  inner join fin.dim_account c on a.account_key = c.account_key
add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201703
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
        or
        (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line;


ok, count is still good
select store, page, line, line_label, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page, line, line_label
order by store, page, line, line_label

by store, page
select store, page, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page
order by store, page
  
*/

