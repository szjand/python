﻿-- 7/18 --- thinking a factless fact table: fs_layout
-- 9/8/16 did some clean up with trim and multiple lables per line
drop table if exists fin.fact_fs_layout;
create table fin.fact_fs_layout (
  the_year integer not null,
  page integer not null,
  line integer not null,
  line_label citext,
constraint fact_fs_layout_pkey primary key (the_year,page,line));
insert into fin.fact_fs_layout  
select flcyy, flpage, flflne, left(line_label, position('|' in line_label) - 1) as line_label
from (
  select flcyy, flpage, flflne,       
  case
    when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
    else fldata
  end as line_label
  from dds.ext_eisglobal_sypfflout
  where flcode = 'GM'
    and flcyy > 2010 
    and flflsq = 0 
    and flflne > 0
    and flpage < 18) x
group by flcyy, flpage, flflne, left(line_label, position('|' in line_label) - 1);

insert into ops.tasks values('fact_fs_layout','Daily');

update fin.fact_fs_layout
set line_label = trim(line_label);

select * from fin.fact_fs_layout where line_label like '%      %' order by line_label

update fin.fact_fs_layout 
set line_label = 'ASSETS'
where line_label = 'ASSETS                 AMOUNT';

update fin.fact_fs_layout 
set line_label = 'EXPENSES'
where line_label = 'EXPENSES                                                                                                 NEW SALES/PN';

leave the page 17 ones, they do not matter for now


select count (1)
from ((
    select *
    from fin.fact_fs_layout d
    except
    select *
    from dds.xfm_eisglobal_sypfflout e)
  union (
    select *
    from dds.xfm_eisglobal_sypfflout f
    except
    select *
    from fin.fact_fs_layout g)) x