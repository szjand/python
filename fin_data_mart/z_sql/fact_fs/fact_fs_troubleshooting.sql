﻿this is all about reconciling to the budget

-- ry1 2016 pdq total expenses
select year_month, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and b.page = 4
--   and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.store = 'ry1' 
  and c.sub_department = 'pdq'
group by year_month
having sum(amount) <> 0  
order by year_month

all ry1 pdq total expenses low 1k - 2k
-- fixed limit lines to expense lines, do not include parts split
select year_month, line, line_label, col, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and b.page = 4
  and b.line < 58
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.store = 'ry1' 
  and c.sub_department = 'pdq'
group by year_month, line, line_label, col
having sum(amount) <> 0  
order by year_month, line

-- ry2 2016 pdq total expenses
-- off every month 
-- what's missing is fixed expenses
-- accounts 29003, 28203 departments wrong in COA
select year_month, line_label, line, col, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and b.page = 4
  and b.line < 58
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.store = 'ry2' 
  and c.sub_department = 'pdq'
group by year_month, line_label, line, col
having sum(amount) <> 0  
order by year_month, line, col

select *
from fin.dim_account
where account in ('29003','28203')

select distinct department_code, department from fin.dim_Account

update fin.dim_account
set department_code = 'QL',
    department = 'Quick Lane'
where account in ('29003','28203') 

select *
from fin.fact_fs a
inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
where b.gl_account in ('29003','28203') 

update fin.fact_fs
set fs_org_key = (
  select fs_org_key
  from fin.dim_fs_org
  where store = 'ry2'
    and sub_department = 'pdq')
-- select * from fin.fact_fs
where fs_account_key in (
  select fs_account_key
  from fin.dim_fs_account
  where gl_account in ('29003','28203'));

-----------------------------------------------------------------------------------------------------  
-----------------------------------------------------------------------------------------------------
-- gross --------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- variable
select c.store, b.page, sum(a.amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201605
  and b.page between 5 and 15
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, b.page  
order by c.store, b.page

page 7 store none with an amount ?!?!?

ok, the problem is account 2657001
select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201605
  and b.page = 7
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key 
where a.amount <> 0

update fin.dim_account
set department_code = 'SD',
    department = 'Service'
-- select * from fin.dim_Account
where account = '2657001';

update fin.fact_fs
set fs_org_key = (
  select fs_org_key
  from fin.dim_fs_org
  where store = 'ry2'
    and sub_department = 'mechanical')
-- select * from fin.fact_fs
where fs_account_key in (
  select fs_account_key
  from fin.dim_fs_account
  where gl_account = '2657001');

-- fixed 
-- 
-- body shop
-- on the fs, gross is P6/L43

select b.year_month, c.store, c.department, c.sub_department, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month between 201601 and 201606
  and b.page = 16
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
group by b.year_month, c.store, c.department, c.sub_department
having sum(amount) <> 0
order by c.store, c.department, c.sub_department, b.year_month;    

select b.year_month, b.page, b.line, b.col, amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201605
  and b.page = 16 and line between 35 and 43
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
order by b.line, b.col  

select b.year_month, b.page, b.line, sum(amount)
-- select sum(amount) -- this matches budget, which includes parts split
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201605
  and (
    (b.page = 16 and line between 35 and 43)
    or
    (b.page = 4 and b.line = 59))
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
  and c.department = 'body shop'
group by b.year_month, b.page, b.line 


select b.year_month, b.page, b.line, sum(amount)
-- select sum(amount) -- this matches budget, which includes parts split
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201605
  and (
    (b.page = 16) -- and line between 35 and 43)
    or
    (b.page = 4 and b.line = 59))
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
  and c.department = 'body shop'
group by b.year_month, b.page, b.line 
order by b.year_month, b.page, b.line 

select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
where b.year_month = 201605
  and b.page = 16
  and b.line = 49

because of parts splits, need to include line ranges on page 6  

--< 10/8/16 -------------------------------------------------------------------------<
201607
car wash semi-fixed is low
look at line by line detail

select year_month, store, department, sub_department,
  line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201607
  and b.page = 4
  and b.line between 18 and 40 
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
where sub_department = 'car wash'
group by year_month, store, department, sub_department, line
having sum(amount) <> 0  
order by year_month, store, department, sub_department, line


-- difference is line 25 - policy work
-- check doc; account 16727

7-28 16241783 819206 236.14, not in query
select *
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201607
inner join fin.dim_Account c on a.account_key = c.account_key
  and c.account = '16727'  
where a.post_status = 'Y'  

select b.thedate, a.*
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
where control = '819206'

select * from arkona.ext_glptrns where gtctl_ = '819206'

select * from arkona.ext_glptrns where rrn = 15633195

select * from arkona.ext_glptrns where gttrn_ = 3393330

this is ugly, somehow the transaction got missed
in arkona
gttrn 3393330 has 10 seq lines including the one with control 819206 (rrn 15633195)
in ext_glptrns it aint there and i do not know why, suspect using rrn is not a good idea
but am UNSURE

--/> 10/8/16 --------------------------------------------------------------------------

--< 10/8/16 ---main shop gross ----------------------------------------------------------<
gross in quer 357 low
look at detail - compare to fs

select b.line_label, b.line, sum(amount)
from fin.fact_fs a
inner join fin.dim_Fs b on a.fs_key = b.fs_key
  and b.year_month = 201607
  and b.page = 16
  and b.line between 21 and 34
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry1'
  and c.department = 'service' and c.sub_department = 'mechanical'
group by b.line_label, b.line
order by b.line

line 27 warranty claim labor off by 341
let it go for now, lets see how subsequent months look
--< 10/10/16 --- 201608 ry2 parts gross ----------------------------------------------------------<
-- $172 hi
-- line 45 $119
-- line 52 $55
select b.line, sum(a.amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201608
  and b.page = 16 and b.line between 45 and 61
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry2'
group by b.line
having sum(amount) <> 0
order by b.line

line 45 warrant
248000 Honda
248010 Nissan

select c.account, sum(a.amount)
-- select b.thedate, c.account, a.*
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201608
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('248000','248010')
where a.post_status = 'Y'  
group by c.account   

select b.thedate, c.account, a.doc, a.control, a.amount
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201608
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '248000'
where a.post_status = 'Y'    
order by a.doc

2740112 in twice

select b.thedate, c.account, a.*
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth in (201608, 201609)
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '248000'
where doc = '2740112'

-- fuck me looks like trn 3426206 seq 1 gets deleted
-- also, looking at daily scrapes, do no see where seq 1.post_Status was ever 'Y'
-- in it is in the extracted data

select * from arkona.ext_glptrns where gttrn_ = 3426206

select rrn(a), '26' as run, a.*
from test.ext_Glptrns_0826 a where gtctl_ = '2740112'
  and gtacct = '248000'
union  all
select rrn(a), '29', a.*
from test.ext_Glptrns_0829 a where gtctl_ = '2740112'
  and gtacct = '248000'  
union  all
select rrn(a), '906', a.*
from test.ext_Glptrns_0906 a where gtctl_ = '2740112'
  and gtacct = '248000' 
union  all
select rrn(a), '930', a.*
from test.ext_Glptrns_0930 a where gtctl_ = '2740112'
  and gtacct = '248000'         
order by run, gtdate, gttrn_ , gtseq_ 

-- shit looked extra goofy because the fucking account number was changed
select rrn(a), '26' as run, a.*
from test.ext_Glptrns_0826 a where gttrn_ = 3426206
union  all
select rrn(a), '29', a.*
from test.ext_Glptrns_0829 a where gttrn_ = 3426206  
union  all
select rrn(a), '906', a.*
from test.ext_Glptrns_0906 a where gttrn_ = 3426206

select *
from fin.fact_gl a 
inner join dds.day b on a.date_key = b.datekey
where not exists (
  select 1
  from arkona.ext_glptrns_tmp 
  where rrn = a.rrn)
and b.thedate > (
  select min(gtdate)
  from arkona.ext_glptrns_tmp)

-- yikes, not deleted, changed (seq 1)
select * from arkona.ext_glptrns_tmp  where gttrn_ = 3426206

select * from fin.fact_gl a left join fin.dim_account b on a.account_key = b.account_key where trans in( 3426206, 3427601) order by trans, seq

10/10/16  i am so fucking confused
ok, here is the problem trans 3426206 seq 1 rrn 15874126 was modified on 9/6 (with a reconcile date of 8/29)
changes: account,amount,journal,gtrdate, gtpost gttype &gtrsts (null to Y),,gtrdoc#, description  
i am not processing changed data
i process new rows and only transactions that become void

what i need to look at?
  1. gtdate <> gtrdate
  2. blank gtpost
  3. md5 on account, journal


select gtpost || '|  wtf  |', length(gtpost)
from arkona.ext_glptrns_tmp a
where gtpost = ' 'not in ('Y','V')
limit 100

select a.gtdesc,b.gtdesc,a.gtpost,b.gtpost,a.gtacct,b.gtacct,a.gtjrnl,b.gtjrnl,a.gttype,b.gttype,a.gtrdoc_,b.gtrdoc_
from arkona.ext_glptrns_tmp a
inner join arkona.ext_glptrns b on a.rrn = b.rrn
  and a.gttrn_ = b.gttrn_
  and a.gtseq_ = b.gtseq_
  and (a.gtdesc <> b.gtdesc or a.gtpost <> b.gtpost or a.gtacct <> b.gtacct or a.gtjrnl <> b.gtjrnl or a.gttype <> b.gttype or a.gtrdoc_ <> b.gtrdoc_)

select post_status, count(*) from fin.fact_gl group by post_status

update arkona.ext_glptrns_tmp
set gtpost = ''
-- select * from arkona.ext_glptrns_tmp
where gtpost = ' '
      
--/> 10/10/16 --- 201608 ry2 parts gross ---------------------------------------------------------/>


--< 10/11/16 --- 201609 ry2 main shop gross ----------------------------------------------------------<
fact_fs: 106942
fs: 107990  

select e.account, e.description, sum(a.amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201609
  and b.page = 16 and b.line between 20 and 42
  and b.col <3
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
  and c.store = 'ry2'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account 
  and e.current_row = true
group by e.account, e.description
having sum(amount) <> 0
order by e.account, e.description

looks like account 246000 (sls cust labor honda) is the problem
in the doc: 40485
in fact_fs: 39306

select b.thedate, a.doc, a.amount
select sum(amount)
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201609
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '246000'
order by a.doc  

the diff is ro 2742243 for 1179

select * from arkona.ext_glptrns where gtctl_ = '2742243'

select * from arkona.ext_glptrns where rrn between 15925735 and 15925738

select * from arkona.ext_glptrns where gtdesc like 'ERTMAN%'

select * from arkona.ext_glptrns where gtdate = '09/06/2016' order by gtctl_

select * from arkona.ext_glptrns_tmp where rrn between 15925735 and 15925738

-- transaction disappears, rrn gets reused

select * from test.ext_glptrns_0902 where gttrn_ = 3434462 -- exists with rrn 15925735 and doc 16247897 and gtpost ''

select * from test.ext_glptrns_0906 where gttrn_ = 3434462 -- transaction no longer exists

select * from test.ext_glptrns_0906 where rrn = 15925735 -- now exists for transaction 3435152, doc 2742243

select * from test.ext_glptrns_0906 where gtdoc_ = '16247897' -- transaction 3435184 & 3435192, all new rrn

s--/> 10/11/16 --- 201609 ry2 main shop gross ---------------------------------------------------------/>



--< 10/8/16 ---main shop gross ----------------------------------------------------------<
    
--/> 10/8/16 ---main shop gross ---------------------------------------------------------/>
--< 10/8/16 ---main shop gross ----------------------------------------------------------<
    
--/> 10/8/16 ---main shop gross ---------------------------------------------------------/>
--< 10/8/16 ---main shop gross ----------------------------------------------------------<
    
--/> 10/8/16 ---main shop gross ---------------------------------------------------------/>
--< 10/8/16 ---main shop gross ----------------------------------------------------------<
    
--/> 10/8/16 ---main shop gross ---------------------------------------------------------/>
--< 10/8/16 ---main shop gross ----------------------------------------------------------<
    
--/> 10/8/16 ---main shop gross ---------------------------------------------------------/>
--< 10/8/16 ---main shop gross ----------------------------------------------------------<
    
--/> 10/8/16 ---main shop gross ---------------------------------------------------------/>

--< 201610 Mechanical Semi Fixed --------------------------------------------------------------<
FS: 110,272
fact_fs: 110,011

select year_month, store, department, line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201610
  and b.page = 4
  and b.line between 18 and 40
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.department = 'service'
  and c.store = 'ry1'
group by year_month, store, department, line
having sum(amount) <> 0  
order by line

the problem is line 25


select page, line, d.gl_account, a.amount
-- select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201610
  and b.page = 4
  and b.line = 25
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'RY1'
  and c.department = 'service'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key    
order by gl_account

the problem is account 16704C (advisor error)
fs: 1034
fact_fs: 773

select a.doc, a.control, a.amount
from fin.fact_gl a
inner join fin.dim_Account b on a.account_key = b.account_key
  and b.account = '16704C'
inner join dds.day c on a.date_key = c.datekey
  and c.yearmonth = 201610 
order by doc  

fact_gl missing ro 16248989

select a.*, b.account
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where control = '16248989'
order by trans, seq

select *
from fin.fact_gl
where trans = 3486875

select b.thedate, a.*
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
where rrn between 16087120 and 16087125

here we go again, rrn 16087124 exists but has no post status, so subsequent runs 
do not update
and the rrn gets reused bu the new record does not get included
that is why the posting for 16704C does not show up

-- select gtdate, gttrn_, gtseq_, gtpost, gtctl_, gtdoc_, gttamt
-- from test.ext_glptrns_0928
-- where rrn = 16087124
-- union all
-- select gtdate, gttrn_, gtseq_, gtpost, gtctl_, gtdoc_, gttamt
-- from test.ext_glptrns_0929
-- where rrn = 16087124
-- union all
-- select gtdate, gttrn_, gtseq_, gtpost, gtctl_, gtdoc_, gttamt
-- from test.ext_glptrns_0930
-- where rrn = 16087124
-- union all
-- select gtdate, gttrn_, gtseq_, gtpost, gtctl_, gtdoc_, gttamt
-- from test.ext_glptrns_1020
-- where rrn = 16087124
-- union all
-- select gtdate, gttrn_, gtseq_, gtpost, gtctl_, gtdoc_, gttamt
-- from test.ext_glptrns_1021
-- where rrn = 16087124
-- 
-- 
-- select gtdate, gttrn_, gtseq_, gtpost, gtctl_, gtdoc_, gttamt
-- from test.ext_glptrns_1020
-- where gttrn_ = 3460711 and gtseq_ = 16
-- 
-- 
-- 
-- select gtdate, gttrn_, gtseq_, gtpost, gtctl_, gtdoc_, gttamt
-- from test.ext_glptrns_1020 a
-- where not exists (
--   select 1
--   from test.ext_glptrns_1021
--   where gttrn_ = a.gttrn_)
-- 
-- select *
-- from (
-- select gtdate, gttrn_, gtseq_, gtpost, gtctl_, gtdoc_, gttamt
-- from test.ext_glptrns_1020) a
-- left join (
-- select gtdate, gttrn_, gtseq_, gtpost, gtctl_, gtdoc_, gttamt
-- from test.ext_glptrns_1021) b on a.gtdate = b.gtdate
--   and a.gttrn_ = b.gttrn_
-- where b.gttrn_ is null   
--/> 201610 Service SemiFixed ------------------------------------------------------------/>

--< 201610 Service Gross --------------------------------------------------------------<
Line 34
FS: 527,549
fact_fs: 527,633

select b.line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201610
  and b.page = 16 and b.line between 21 and 31
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.department = 'service'
  and c.store = 'ry1'
group by b.line
having sum(amount) <> 0
order by b.line;

problem is line 27
FS: 90,316
fact_fs: 90,395

select page, line, col, d.gl_account, a.amount
-- select col, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201610
  and b.page = 16
  and b.line = 27
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'RY1'
--   and c.department = 'service'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key   
--   and d.gm_account = '462' 
group by col
order by gl_account

select * 
from fin.dim_fs_account
where gm_account = '462'
order by gl_account

select * 
from fin.dim_fs
where year_month = 201610
  and page = 16 and line = 27

i am fucking striking out, unable to find a doc that matches the statement
it is not big enough, betting on the same sort of problem as above, unposted entries
fucking things up (in a small way)

--/> 201610 Service Gross ------------------------------------------------------------/>

--< 201610 Parts Gross --------------------------------------------------------------<
FS: 393,114
fact_fs: 392,952

select b.line, sum(amount)
-- select sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201610
  and b.page = 16 and b.line between 45 and 56
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.department = 'parts'
  and c.store = 'ry1'
group by b.line
having sum(amount) <> 0
order by b.line;

--/> 201610 Service Gross ------------------------------------------------------------/>

--< 201611 RY1 Variable Gross ------------------------------------------------------<
--fs: 315963, fact_fs: 315620
select sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201611
  and (
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20))-- f/i
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
-- new ok, used and finance are off
-- used fs: 326421, fact_fs: 327256
-- finance: fs: 233737, fact_fs: 232564
select 
  sum(case when b.page between 5 and 15 then amount end) as new,
  sum(case when b.page = 16 and b.line between 1 and 14 then amount end) as used,
  sum(case when b.page = 17 and b.line between 1 and 20 then amount end) as finance
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201611
  and (
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20))-- f/i
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'  

-- used c/s fs: 4227297  fact_fs: 4226461 
select b.col, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201611
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'  
group by b.col   
order by col  

-- line 5  fs: 1527387, fact_fs: 1526553
select b.line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201611
  and b.page = 16
  and b.line between 1 and 14
  and b.col = 3
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'  
group by b.line
order by b.line

select sum(amount) -- sales: ok 1685544
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201611  
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '145001'
where a.post_status = 'y'  

-- recon is the problem: doc: 121086, fact_gs: 120251
select doc, control, amount
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201611
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ('165101')
where a.post_status = 'y' 
  and doc = '16252372'
order by doc

select sum(amount)
from fin.fact_Gl a
inner join fin.dim_Account b on a.account_key = b.account_key
-- where rrn = 16191593
where doc = '16252372'


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
same thing, deleted unposted record with reused rrn
fuck it , stop here until i get that resolved

--/> 201611 RY1 Variable Gross -----------------------------------------------------/>

-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! --

12/14/16, looks like the fix to fact_gl has taken care of all these unresolved differences

-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! --

--1/6/17  after a shit load of problems due to bad handling of voids, deleted and repopulated
--        fact_gl for december
--        now all kinds of shit is fucked up
RY1 P3L57 New FS:221084 Fact:229462


select store, 
  sum(case when b.col = 1 and b.line between 4 and 6 then amount end) as "New Variable",
  sum(case when b.col = 5 and b.line between 4 and 6 then amount end) as "Used Variable",
  sum(case when b.col = 1 and b.line between 8 and 16 then amount end) as "New Personnel",
  sum(case when b.col = 5 and b.line between 8 and 16 then amount end) as "Used Personnel",  
  sum(case when b.col = 1 and b.line between 18 and 39 then amount end) as "New Semi-Fixed",
  sum(case when b.col = 5 and b.line between 18 and 39 then amount end) as "Used Semi-Fixed",  
  sum(case when b.col = 1 and b.line between 41 and 54 then amount end) as "New Fixed",
  sum(case when b.col = 5 and b.line between 41 and 54 then amount end) as "Used Fixed",
  sum(case when b.col = 1 then amount end) as "New Total",
  sum(case when b.col = 5 then amount end) as "Used Total"
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 3
--   and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.area = 'variable' 
group by store
having sum(amount) <> 0  
order by store

-- both stores, personnel and semi-fixed are off
-- personnel
select line, 
  sum(case when c.store = 'RY1' and col = 1 then amount end) as "GM New",
  sum(case when c.store = 'RY1' and col = 5 then amount end) as "GM Used",
  sum(case when c.store = 'RY2' and col = 1 then amount end) as "Honda New",
  sum(case when c.store = 'RY2' and col = 5 then amount end) as "Honda Used"  
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 3
--   and b.col = 1
  and b.line between 8 and 16
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
--   and c.store = 'ry1' 
  and c.area = 'variable' 
group by line
having sum(amount) <> 0  
order by line

RY1 line 14 and 15 are off

RY2 14, 15, 16

RY1 L14 acct 12501   
RY1 L15 acct 12701 

select b.thedate, c.journal_code, a.doc, a.control, a.amount, d.description
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201612
inner join fin.dim_journal c on a.journal_key = c.journal_key 
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key 
inner join fin.dim_account e on a.account_key = e.account_key
  and e.account = '12501'

select gtdate, gtjrnl, gtdoc_, gtctl_, gttamt, gtdesc
select sum(gttamt)
from arkona.ext_glptrns_tmp
where gtdate between '12/01/2016' and '12/31/2016' 
  and gtacct = '12501'
  and gtdoc_ <> 'RGJ31316'

create table fin.tmp_12501 (
  journal_code citext,
  doc citext,
  control citext,
  amount numeric(12,2),
  description citext);

select sum(amount) from fin.tmp_12501

-- these are the 5 rows that are different
select *
from (
select a.control, sum(amount) as amount
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201612
inner join fin.dim_journal c on a.journal_key = c.journal_key 
inner join fin.dim_gl_description d on a.gl_description_key = d.gl_description_key 
inner join fin.dim_account e on a.account_key = e.account_key
  and e.account = '12501'
group by a.control) x
left join (
select control, sum(amount) as amount
from fin.tmp_12501
group by control) y on x.control = y.control
where coalesce(x.amount, 0) <> coalesce(y.amount, 0)
  
-- semi-fixed
select line, 
  sum(case when c.store = 'RY1' and col = 1 then amount end) as "GM New",
  sum(case when c.store = 'RY1' and col = 5 then amount end) as "GM Used",
  sum(case when c.store = 'RY2' and col = 1 then amount end) as "Honda New",
  sum(case when c.store = 'RY2' and col = 5 then amount end) as "Honda Used"  
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 3
--   and b.col = 1
  and b.line between 18 and 39
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
--   and c.store = 'ry1' 
  and c.area = 'variable' 
group by line
having sum(amount) <> 0  
order by line

RY1 line 22, 27

RY2 line 27, 




select distinct doc
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201612
where doc like '%1316'


--< 201708 RY1 parts split mech ------------------------------------------------------<
--fs: 27,427, fact_fs: 27787

select b.year_month, c.store, d.gl_account, sum(a.amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201708
  and b.page = 4
  and b.line = 59
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key 
  and c.store = 'ry1'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
group by b.year_month, c.store, d.gl_account
having sum(a.amount) <> 0

-- missing account 146700D, yep, more auto outlet
--/> 201708 RY1 parts split mech ----------------------------------------------------/>
