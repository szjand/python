﻿select *
from fin.dim_Fs_account
where gm_Account in ('902', '903', '905','909','910','952','953','955')
order by gm_account, gl_Account


select *
from arkona.ext_eisglobal_sypffxmst
where fxmcyy between 2015 and 2017
  and fxmact  in ('902','905','909','910','952','953','955')
order by fxmcyy, fxmpge, fxmlne  


select *
from arkona.ext_eisglobal_sypffxmst a
-- -- *a*
left join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
where a.fxmcyy = 2017
  and fxmpge = 2
  and fxmlne between 63 and 68
order by store_code, fxmpge, fxmlne, fxmcol

select *

select *
from arkona.ext_eisglobal_sypfflout 
where flcyy = 2017
  and flpage = 2
order by flflne  

select *
from arkona.ext_eisglobal_sypffxmst
where fxmcyy between 2015 and 2017
  and fxmpge = 3
  and fxmlne > 62
order by fxmcyy, fxmpge, fxmlne  

select *
from arkona.ext_eisglobal_sypffxmst
where fxmcyy between 2015 and 2017
  and fxmpge = 2
  and fxmlne > 57
order by fxmcyy, fxmpge, fxmlne  

select *
from fin.fact_fs_all
where year_month = 201702
  and page = 3
  and line > 62


select *
from fin.dim_fs_account
where gm_account like '<%'



select *
from fin.xfm_fs_1  



select store_code, gm_account, sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201702
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_fs_account d on c.account = d.gl_account
  and d.gm_account in ('902', '903', '905','909','910','952','953','955')   
where a.post_status = 'Y'  
group by store_code, gm_account




select store_code, sum(amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201702
inner join fin.dim_account c on a.account_key = c.account_key
inner join fin.dim_fs_account d on c.account = d.gl_account
  and d.gm_account in ('902', '903', '905','909','910','952','953','955')   
where a.post_status = 'Y'  
group by store_code, gm_account



select line, sum(amount)
from fin.fact_fs_all
where year_month = 201702
  and page = 2
  and store = 'ry1'
group by line
order  by line


select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where year_month = 201701
  and page = 2
  and store = 'ry1'
order by line    