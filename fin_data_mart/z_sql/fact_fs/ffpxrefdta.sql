﻿
-------------------------------------------------------------------------------------
--< base queries
-------------------------------------------------------------------------------------
-- new, removed or changed rows
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
order by g_l_acct_number  

-- changed rows
select *
from (
  select factory_financial_year, g_l_acct_number, md5(a::text) as hash
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, md5(b::text) as hash
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.hash <> d.hash 

-- changed routing
select c.*, d.new_factory
from (
  select factory_financial_year, g_l_acct_number, factory_account as old_factory
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, factory_account as new_factory
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.old_factory <> d.new_factory
    
-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2019
  and g_l_acct_number in ('222002','127703','1625001','261300','166424')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2019
  and g_l_acct_number in ('222002','127703','1625001','261300','166424')
order by g_l_acct_number, source           

-- rows missing from dim_fs_account
select a.account
from fin.dim_account a
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account) 

-------------------------------------------------------------------------------------
--/> base queries
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 02/10/21: 3 changed routing, reverse what was done a week ago
-------------------------------------------------------------------------------------
-- changed routing
select c.*, d.new_factory
from (
  select factory_financial_year, g_l_acct_number, factory_account as old_factory
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, factory_account as new_factory
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.old_factory <> d.new_factory
    

-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '433A'
where g_l_acct_number = '1433001'
  and factory_account = '427A'
  and factory_financial_year = 2021; 

update arkona.ext_ffpxrefdta
set factory_account = '635A'
where g_l_acct_number = '1635001'
  and factory_account = '634A'
  and factory_financial_year = 2021; 

update arkona.ext_ffpxrefdta
set factory_account = '633A'
where g_l_acct_number = '1633001'
  and factory_account = '627A'
  and factory_financial_year = 2021; 
     
-- update fin.dim_fs_account

update fin.dim_fs_account
set gm_account = '433A'
where gl_account = '1433001'
  and gm_account = '427A';   

update fin.dim_fs_account
set gm_account = '636A'
where gl_account = '1635001'
  and gm_account = '634A';    

update fin.dim_fs_account
set gm_account = '633A'
where gl_account = '1633001'
  and gm_account = '627A';   
-------------------------------------------------------------------------------------
--/> 02/10/21: 6 changed routing
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< 02/04/21: 6 changed routing, looks like the sort of temporary change jeri needs 
-- to do to accommodate dealertrack
-------------------------------------------------------------------------------------
-- changed routing
select c.*, d.new_factory
from (
  select factory_financial_year, g_l_acct_number, factory_account as old_factory
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, factory_account as new_factory
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.old_factory <> d.new_factory
    

-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '657A'
where g_l_acct_number = '164501'
  and factory_financial_year = 2021;     

update arkona.ext_ffpxrefdta
set factory_account = '427A'
where g_l_acct_number = '1433001'
  and factory_financial_year = 2021; 

update arkona.ext_ffpxrefdta
set factory_account = '457A'
where g_l_acct_number = '144500'
  and factory_financial_year = 2021; 

update arkona.ext_ffpxrefdta
set factory_account = '634A'
where g_l_acct_number = '1635001'
  and factory_financial_year = 2021; 

update arkona.ext_ffpxrefdta
set factory_account = '657A'
where g_l_acct_number = '164500'
  and factory_financial_year = 2021; 

update arkona.ext_ffpxrefdta
set factory_account = '627A'
where g_l_acct_number = '1633001'
  and factory_financial_year = 2021; 
     
-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '657A'
where gl_account = '164501'
  and gm_account = '645J';    

update fin.dim_fs_account
set gm_account = '427A'
where gl_account = '1433001'
  and gm_account = '433A';   

update fin.dim_fs_account
set gm_account = '457A'
where gl_account = '144500'
  and gm_account = '445J';   

update fin.dim_fs_account
set gm_account = '634A'
where gl_account = '1635001'
  and gm_account = '635A';   

update fin.dim_fs_account
set gm_account = '657A'
where gl_account = '164500'
  and gm_account = '645J';    

update fin.dim_fs_account
set gm_account = '627A'
-- select * from fin.dim_fs_account
where gl_account = '1633001'
  and gm_account = '633A';   
-------------------------------------------------------------------------------------
--/> 02/04/21: 6 changed routing
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 02/02/21: 3150 new rows: 2021 factory_financial_year
-------------------------------------------------------------------------------------
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x

-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;     


-------------------------------------------------------------------------------------
--< 01/15/21: 1 new accounts
-------------------------------------------------------------------------------------
select * 
from fin.dim_account a
where a.account not in ('999999','21','none','*E*','*VOID','*SPORD','12101W','120502')
  and not exists (
    select 1 
    from arkona.ext_ffpxrefdta
    where g_l_acct_number = a.account) ;

-- add to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);   

-- add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '12/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where a.current_row
  and account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);
-------------------------------------------------------------------------------------
--/> 01/15/21: 1 new accounts
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--< 12/02/20: 11 new accounts
-------------------------------------------------------------------------------------

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
-- this is generating duplicdate rows for 4 of these accounts - CURRENT ROW
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '12/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where a.current_row
  and account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);
    
-------------------------------------------------------------------------------------
--/> 12/02/20: 11 new accounts
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--< 11/03/20: 1 new accounts for gateway carwash: 17028
-------------------------------------------------------------------------------------

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '09/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-------------------------------------------------------------------------------------
--/> 11/03/20: 1 new accounts for gateway carwash
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< 10/02/20: 1 new accounts for gateway carwash
-------------------------------------------------------------------------------------

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '09/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-------------------------------------------------------------------------------------
--/> 10/02/20: 1 new accounts for gateway carwash
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< 10/01/20: 8 new accounts for gateway carwash
-------------------------------------------------------------------------------------

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '09/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-------------------------------------------------------------------------------------
--/> 10/01/20: 8 new accounts for gateway carwash
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< 09/10/20: 14 accounts with changed routing
-------------------------------------------------------------------------------------

-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2020
  and g_l_acct_number in ('133200','133201','133202','133204','133206')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2020
  and g_l_acct_number in ('133200','133201','133202','133204','133206')
order by g_l_acct_number, source    

-- changed routing
select c.*, d.new_factory
from (
  select factory_financial_year, g_l_acct_number, factory_account as old_factory
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, factory_account as new_factory
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.old_factory <> d.new_factory
    

-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '331'
where g_l_acct_number = '133200'
  and factory_financial_year = 2020;     
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '322'
where g_l_acct_number = '133201'
  and factory_financial_year = 2020;    
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '322'
where g_l_acct_number = '133202'
  and factory_financial_year = 2020;     
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '322'
where g_l_acct_number = '133204'
  and factory_financial_year = 2020;    
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '331'
where g_l_acct_number = '133206'
  and factory_financial_year = 2020;     
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '322'
where g_l_acct_number = '133212'
  and factory_financial_year = 2020;    
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '220CD'
where g_l_acct_number = '222002'
  and factory_financial_year = 2020;     
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '331'
where g_l_acct_number = '232200'
  and factory_financial_year = 2020;    
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '331'
where g_l_acct_number = '233200'
  and factory_financial_year = 2020;     
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '322'
where g_l_acct_number = '233201'
  and factory_financial_year = 2020;    
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '322'
where g_l_acct_number = '233202'
  and factory_financial_year = 2020;     
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '322'
where g_l_acct_number = '233203'
  and factory_financial_year = 2020;    
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '331'
where g_l_acct_number = '233206'
  and factory_financial_year = 2020;     
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '322'
where g_l_acct_number = '233212'
  and factory_financial_year = 2020;    





-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '331'
where gl_account = '133200';     
-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '322'
where gl_account = '133201';    
-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '322'
where gl_account = '133202';     
-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '322'
where gl_account = '133204';    
-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '331'
where gl_account = '133206';     
-- update fin.dim_fs_account  
update fin.dim_fs_account
set gm_account = '322'
where gl_account = '133212';    
-- update fin.dim_fs_account  
update fin.dim_fs_account
set gm_account = '220CD'
where gl_account = '222002';     
-- update fin.dim_fs_account  
update fin.dim_fs_account
set gm_account = '331'
where gl_account = '232200';    
-- update fin.dim_fs_account  
update fin.dim_fs_account
set gm_account = '331'
where gl_account = '233200';     
-- update fin.dim_fs_account  
update fin.dim_fs_account
set gm_account = '322'
where gl_account = '233201';    
-- update fin.dim_fs_account  
update fin.dim_fs_account
set gm_account = '322'
where gl_account = '233202';     
-- update fin.dim_fs_account  
update fin.dim_fs_account
set gm_account = '322'
where gl_account = '233203';    
-- update fin.dim_fs_account  
update fin.dim_fs_account
set gm_account = '331'
where gl_account = '233206';     
-- update fin.dim_fs_account  
update fin.dim_fs_account
set gm_account = '322'
where gl_account = '233212';    











-------------------------------------------------------------------------------------
--/> 09/10/20: 11 new accounts for gateway carwash
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 09/01/20: 11 new accounts for gateway carwash
-------------------------------------------------------------------------------------

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '07/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-------------------------------------------------------------------------------------
--/> 09/01/20: 11 new accounts for gateway carwash
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< 08/04/20: 1 new accounts 16128
-------------------------------------------------------------------------------------
-- select * from fin.dim_account where account in ('232413','232415')
-- insert new accounts
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '07/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-------------------------------------------------------------------------------------
--/> 08/04/20: 1 new accounts 16128
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 06/30/20: 2 new accounts 232413, 232415
-------------------------------------------------------------------------------------
-- select * from fin.dim_account where account in ('232413','232415')
-- insert new accounts
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '06/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-------------------------------------------------------------------------------------
--/> 06/30/20: 2 new accounts 232413, 232415
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 06/03/20: changed routing on 1430006: 430E->431E/1630006: 630E->631E
-------------------------------------------------------------------------------------

-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1430006','1630006')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1430006','1630006')
order by g_l_acct_number, source 

-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '431E'
where g_l_acct_number = '1430006'
  and factory_financial_year = 2020;     
-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '631E'
where g_l_acct_number = '1630006'
  and factory_financial_year = 2020;    
  
-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '431E'
where gl_account = '1430006'
  and gm_account = '430E';
update fin.dim_fs_account
set gm_account = '631E'
where gl_account = '1630006'
  and gm_account = '630E';  

-------------------------------------------------------------------------------------
--<06/03/20
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--< 05/07/20: reverse what was done on 4/30 1 account w/changed routing 1625001  629A -> 625A 
-------------------------------------------------------------------------------------

-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1625001')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1625001')
order by g_l_acct_number, source 

-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '625A'
where g_l_acct_number = '1625001'
  and factory_financial_year = 2020;     

-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '625A'
where gl_account = '1625001'
  and gm_account = '629A';

-------------------------------------------------------------------------------------
--< 05/07/20: reverse what was done on 4/30 1 account w/changed routing 1625001  629A -> 625A 
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 04/03/20: 1 account w/changed routing 1625001 625A -> 629A
-------------------------------------------------------------------------------------

-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1625001')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1625001')
order by g_l_acct_number, source    

-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '629A'
where g_l_acct_number = '1625001'
  and factory_financial_year = 2020;     

-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '629A'
where gl_account = '1625001'
  and gm_account = '625A';

-------------------------------------------------------------------------------------
--/> 04/03/20: 1 account w/changed routing 1625001 625A -> 629A
-------------------------------------------------------------------------------------



-------------------------------------------------------------------------------------
--< 04/25/20: 3 new accounts 133601, 220203, 233601
-------------------------------------------------------------------------------------

-- insert new accounts
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '04/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-------------------------------------------------------------------------------------
--/> 04/25/20: 3 new accounts 133601, 220203, 233601
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 04/04/20: 2 accounts with changed routing: 1625001 629A -> 625A, 1628006 627E -> 628E
          --  reversing what was done yesterday
-------------------------------------------------------------------------------------
-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1625001','1628006')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1625001','1628006')
order by g_l_acct_number, source    

-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '625A'
where g_l_acct_number = '1625001'
  and factory_financial_year = 2020;     
update arkona.ext_ffpxrefdta
set factory_account = '628E'
where g_l_acct_number = '1628006'
  and factory_financial_year = 2020;     

-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '625A'
where gl_account = '1625001'
  and gm_account = '629A';
update fin.dim_fs_account
set gm_account = '628E'
where gl_account = '1628006'
  and gm_account = '627E';    


-------------------------------------------------------------------------------------
--/> 04/04/2020
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 04/03/20: 2 new accounts 148199, 168199: printer toner
--            2 accounts with changed routing: 1625001 625A -> 629A, 1628006 628E -> 627E
-------------------------------------------------------------------------------------
-- insert new accounts
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '03/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1625001','1628006')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2020
  and g_l_acct_number in ('1625001','1628006')
order by g_l_acct_number, source    

-- update ext_ffpxrefdta  
update arkona.ext_ffpxrefdta
set factory_account = '629A'
where g_l_acct_number = '1625001'
  and factory_financial_year = 2020;     
update arkona.ext_ffpxrefdta
set factory_account = '627E'
where g_l_acct_number = '1628006'
  and factory_financial_year = 2020;     

-- update fin.dim_fs_account
update fin.dim_fs_account
set gm_account = '629A'
where gl_account = '1625001'
  and gm_account = '625A';
update fin.dim_fs_account
set gm_account = '627E'
where gl_account = '1628006'
  and gm_account = '628E';  
-------------------------------------------------------------------------------------
--/> 04/03/2020
-------------------------------------------------------------------------------------



-------------------------------------------------------------------------------------
--< 04/02/20: 4 new accounts 1411006,1413006,1611006,1611006: cad CT4 & CT5
-------------------------------------------------------------------------------------
-- insert new accounts
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '03/01/2020', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-------------------------------------------------------------------------------------
--/> 03/18/20: 2 new accounts 124299, 130310
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 03/18/20: 2 new accounts 124299, 130310
-------------------------------------------------------------------------------------
-- new account
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);

-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '11/01/2019', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-------------------------------------------------------------------------------------
--/> 03/18/20: 2 new accounts 124299, 130310
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 02/04/20: ffpxrefdta failed 
-- 3100 rows: 2000 data
-------------------------------------------------------------------------------------
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x

-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;     


-------------------------------------------------------------------------------------
--/> 02/04/20: ffpxrefdta failed 
-- 3100 rows: 200 data
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 12/20/19  new account 233600, changed account 133600
-------------------------------------------------------------------------------------

-- new account
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);


-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '11/01/2019', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-- changed account
-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2019
  and g_l_acct_number in ('133600')
union
select 'new' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2019
  and g_l_acct_number in ('133600')   

update arkona.ext_ffpxrefdta
set factory_account = '336'
where g_l_acct_number = '133600'
  and factory_financial_year = 2019;    
-------------------------------------------------------------------------------------
--/> 12/20/19  new account 233600, changed account 133600
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 12/18/19  new account 128501
-------------------------------------------------------------------------------------

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);


-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '11/01/2019', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

    
-------------------------------------------------------------------------------------
--/> 12/18/19  new account 128501
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 12/03/19  new account 29024
-------------------------------------------------------------------------------------

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);


-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '11/01/2019', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

    
-------------------------------------------------------------------------------------
--/> 12/03/19  new account 29024
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 11/02/19  1625001
-------------------------------------------------------------------------------------
-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2019
  and g_l_acct_number in ('1625001')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2019
  and g_l_acct_number in ('1625001')
order by g_l_acct_number, source    
   
1625001: 629A -> 625A


update arkona.ext_ffpxrefdta
set factory_account = '625A'
-- select * from arkona.ext_ffpxrefdta
where g_l_acct_number = '1625001'
  and factory_financial_year = 2019; 


-- not sure i understand this, gm_account is already 625A ?!?!?
-- update fin.dim_fs_account  
-- set gm_account = '220'  
select * from fin.dim_fs_account 
where gl_account = '1625001';  
-------------------------------------------------------------------------------------
--/> 11/02/19  1625001
-------------------------------------------------------------------------------------  
-------------------------------------------------------------------------------------
--< 10/4/19  1640006 changed
-------------------------------------------------------------------------------------
select * from fin.dim_account where account = '1640006'

-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2019
  and g_l_acct_number in ('1640006')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2019
  and g_l_acct_number in ('1640006')
order by g_l_acct_number, source    

update arkona.ext_ffpxrefdta
set factory_account = '640E'
where g_l_acct_number = '1640006'
  and factory_financial_year = 2019; 

-------------------------------------------------------------------------------------
--/> 10/4/19  1640006 changed
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< 10/2/19  3 new accounts: 132309, 132310, 132311
-------------------------------------------------------------------------------------
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number);


-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '09/01/2019', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

    
-------------------------------------------------------------------------------------
--/> 10/2/19  3 new accounts: 132309, 132310, 132311
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 9/5/19  127703, 1625001, 166424, 261300
--  mostly fucking reverse what she did a week ago
-------------------------------------------------------------------------------------
-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2019
  and g_l_acct_number in ('127703','1625001','261300','166424')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2019
  and g_l_acct_number in ('127703','1625001','261300','166424')
order by g_l_acct_number, source   

127703: 285 -> 277E 
1625001: 625A -> 629A 
166424: 664 -> 662 
261300: 613C ->613J 

update arkona.ext_ffpxrefdta
set factory_account = '277E'
where g_l_acct_number = '127703'
  and factory_financial_year = 2019; 

update arkona.ext_ffpxrefdta
set factory_account = '662'
where g_l_acct_number = '166424'
  and factory_financial_year = 2019; 

update arkona.ext_ffpxrefdta
set factory_account = '629A'
where g_l_acct_number = '1625001'
  and factory_financial_year = 2019; 

update arkona.ext_ffpxrefdta
set factory_account = '613J'
where g_l_acct_number = '261300'
  and factory_financial_year = 2019;  
-------------------------------------------------------------------------------------
--/> 9/5/19  127703, 1625001, 166424, 222002, 261300
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< 8/31/19  127703, 1625001, 166424, 222002, 261300
-------------------------------------------------------------------------------------
-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2019
  and g_l_acct_number in ('222002','127703','1625001','261300','166424')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2019
  and g_l_acct_number in ('222002','127703','1625001','261300','166424')
order by g_l_acct_number, source    
   
127703: 277E -> 285
1625001: 629A -> 625A
166424: 662 -> 664
222002: 220CD -> 220
261300: 613J -> 613C

update arkona.ext_ffpxrefdta
set factory_account = '220'
where g_l_acct_number = '222002'
  and factory_financial_year = 2019; 

update arkona.ext_ffpxrefdta
set factory_account = '285'
where g_l_acct_number = '127703'
  and factory_financial_year = 2019; 

update arkona.ext_ffpxrefdta
set factory_account = '664'
where g_l_acct_number = '166424'
  and factory_financial_year = 2019; 

update arkona.ext_ffpxrefdta
set factory_account = '625A'
where g_l_acct_number = '1625001'
  and factory_financial_year = 2019; 

update arkona.ext_ffpxrefdta
set factory_account = '613C'
where g_l_acct_number = '261300'
  and factory_financial_year = 2019;         

-- new account 132308 / 323

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number)
  
-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '08/01/2019', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

select * from fin.dim_fs_account where gl_account = '222002'

update fin.dim_fs_account  
set gm_account = '220'  
-- select * from fin.dim_fs_account 
where gl_account = '222002';
    
-------------------------------------------------------------------------------------
--/> 8/31/19  127703, 1625001, 166424, 222002, 261300
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 7/9/19  222002, 231404
-------------------------------------------------------------------------------------

select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2019
  and g_l_acct_number in ('222002','231404')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2019
  and g_l_acct_number in ('222002','231404')
order by g_l_acct_number, source  

222002: 220 -> 220CD
231404: 335 -> 338

update arkona.ext_ffpxrefdta
set factory_account = '220CD'
where g_l_acct_number = '222002'
  and factory_financial_year = 2019; 

  update arkona.ext_ffpxrefdta
set factory_account = '338'
where g_l_acct_number = '231404'
  and factory_financial_year = 2019; 

select * from fin.dim_fs_account where gl_account in('222002','231404') 

update fin.dim_fs_account
set gm_account = '220CD'
where fs_account_key = 1964;  

update fin.dim_fs_account
set gm_account = '338'
where fs_account_key = 96429;
-------------------------------------------------------------------------------------
--/> 7/9/19  
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< 7/3/19  
-------------------------------------------------------------------------------------

select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2019
  and g_l_acct_number in ('166424')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2019
  and g_l_acct_number in ('166424')
order by g_l_acct_number, source   

update arkona.ext_ffpxrefdta
set factory_account = '662'
where g_l_acct_number = '166424'
  and factory_financial_year = 2019; 

select * from fin.dim_fs_account where gl_account = '166424'   

update fin.dim_fs_account
set gm_account = '662'
where fs_account_key = 3214;


-------------------------------------------------------------------------------------
--/> 7/3/19  
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 7/2/19  
-------------------------------------------------------------------------------------
-- 3 new accounts 1428006, 1628006, 231404
-- add them to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number)
-- add them to  fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '05/01/2019', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

-- changed routing: 1620001

-- compare changed rows
select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2019
  and g_l_acct_number in ('1620001')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2019
  and g_l_acct_number in ('1620001')
order by g_l_acct_number, source      

update arkona.ext_ffpxrefdta
set factory_account = '620A'
where g_l_acct_number = '1620001'
  and factory_financial_year = 2019; 

select * from fin.dim_fs_account where gl_account = '1620001'

update fin.dim_fs_account
set gm_account = '620A'
where fs_account_key = 2994;
  
-------------------------------------------------------------------------------------
--/> 7/2/19  
-------------------------------------------------------------------------------------
      
-------------------------------------------------------------------------------------
--< 6/4/19  2 new accounts 167101, 167301
-------------------------------------------------------------------------------------
select * From fin.dim_account where account in ('167101','167301')

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number)

-- dont yet exists in fin.dim_fs_account
select * from fin.dim_Fs_account where gl_account in ('167101','167301')

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '05/01/2019', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);
-------------------------------------------------------------------------------------
--< 6/4/19
-------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------
--< 5/3/19 2 accounts with changed routing: 127703:285->277E, 261300:613C->613J
-------------------------------------------------------------------------------------
select c.*, d.new_factory
from (
  select factory_financial_year, g_l_acct_number, factory_account as old_factory
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, factory_account as new_factory
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.old_factory <> d.new_factory

update arkona.ext_ffpxrefdta
set factory_account = '277E'
where g_l_acct_number = '127703'
  and factory_financial_year = 2019;

update arkona.ext_ffpxrefdta
set factory_account = '613J'
where g_l_acct_number = '261300'
  and factory_financial_year = 2019; 

after doing this change and then ffpxrefdta passes,
fs failed  because dim_fs_account still shows the old routing of 261300 -> 613C
so change it, the 127703 apparently had no effect

update fin.dim_fs_account
set gm_account = '613J'
where gl_account = '261300'
  and gm_account = '613C';
-------------------------------------------------------------------------------------
--< 5/3/19 2 accounts with changed routing: 127703:285->277E, 261300:613C->613J
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--< 4/3/19 3 accounts with changed routing: 1628001:629A->628A, 1605001:604A->605A, 1631006:627E->631E
-------------------------------------------------------------------------------------
select c.*, d.new_factory
from (
  select factory_financial_year, g_l_acct_number, factory_account as old_factory
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, factory_account as new_factory
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.old_factory <> d.new_factory

update arkona.ext_ffpxrefdta
set factory_account = '628A'
where g_l_acct_number = '1628001'
  and factory_financial_year = 2019;

update arkona.ext_ffpxrefdta
set factory_account = '605A'
where g_l_acct_number = '1605001'
  and factory_financial_year = 2019;  

update arkona.ext_ffpxrefdta
set factory_account = '631E'
where g_l_acct_number = '1631006'
  and factory_financial_year = 2019;  
  
-- select * from fin.dim_fs_Account where gl_account in ('1628001','1605001','1631006') order by gl_account
update fin.dim_fs_account
set gm_account = '628A'
where gl_account = '1628001'
  and gm_account = '629A';

update fin.dim_fs_account
set gm_account = '605A'
where gl_account = '1605001'
  and gm_account = '604A';

update fin.dim_fs_account
set gm_account = '631E'
where gl_account = '1631006'
  and gm_account = '627E';
-------------------------------------------------------------------------------------
--/>  4/3/19 3 accounts with changed routing: 1628001:629A->628A, 1605001:604A->605A, 1631006:627E->631E
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--< 4/2/19 12 new accounts, mostly ry2 detail
-------------------------------------------------------------------------------------
-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;     


-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select b.factory_account, b.g_l_acct_number, '03/01/2019', true, 'new gl_account'
from fin.dim_account a
join arkona.ext_ffpxrefdta b on a.account = b.g_l_acct_number
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account);

    

-------------------------------------------------------------------------------------
--/>  4/2/19 bunch of new accounts, mostly ry2 detail
-------------------------------------------------------------------------------------

-- 3/5/19 routing for 1625001 changed from 625A to 629A
select c.*, d.new_factory
from (
  select factory_financial_year, g_l_acct_number, factory_account as old_factory
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, factory_account as new_factory
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.old_factory <> d.new_factory

update arkona.ext_ffpxrefdta
set factory_account = '629A'
where g_l_acct_number = '1625001'
  and factory_financial_year = 2019;
    
-- 3/3/19 routing for 1626006 changed back to 626E from 627E
-- changed routing
select c.*, d.new_factory
from (
  select factory_financial_year, g_l_acct_number, factory_account as old_factory
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, factory_account as new_factory
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.old_factory <> d.new_factory

select * from arkona.ext_ffpxrefdta where g_l_acct_number = '1626006'

update arkona.ext_ffpxrefdta
set factory_account = '626E'
where g_l_acct_number = '1626006'
  and factory_financial_year = 2019;


      
-- 2/6/19 monthly failed
failed due to no fin.dim_fs_account row for gl account 232406
-- rows missing from dim_fs_account
select a.* -- a.account
from fin.dim_account a
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account) 

select * from fin.dim_fs_Account limit 10
select * from arkona.ext_ffpxrefdta where g_l_acct_number = '232406'    

insert into fin.dim_fs_account(gm_account,gl_account,row_from_Date,current_row,row_reason)
select factory_account,g_l_acct_number,'01/01/2019',true, 'New Row'
from arkona.ext_ffpxrefdta where g_l_acct_number = '232406'    


 

    
        
-- 2/6/19 -----------------------------------------------
-- 5 accounts routing change
-- changed routing
-- suspect these were temporary changes to facilitate submitting the statement

select c.*, d.new_factory
from (
  select factory_financial_year, g_l_acct_number, factory_account as old_factory
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, factory_account as new_factory
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.old_factory <> d.new_factory;

-- change arkona.ext_ffpxrefdta
update arkona.ext_ffpxrefdta
set factory_account = '621A'
where g_l_acct_number = '1620001'
  and factory_financial_year = 2019;
update arkona.ext_ffpxrefdta
set factory_account = '629A'
where g_l_acct_number = '1628001'
  and factory_financial_year = 2019;
update arkona.ext_ffpxrefdta
set factory_account = '627E'
where g_l_acct_number = '1626006'
  and factory_financial_year = 2019;
update arkona.ext_ffpxrefdta
set factory_account = '604A'
where g_l_acct_number = '1605001'
  and factory_financial_year = 2019;
update arkona.ext_ffpxrefdta
set factory_account = '627E'
where g_l_acct_number = '1631006'
  and factory_financial_year = 2019;      


select * from fin.dim_fs_Account where gl_Account in  ('1620001','1628001','1626006','1605001','1631006') and left(gm_account, 1) = '6'

update fin.dim_fs_account
set gm_account = '621A'
where gl_account = '1620001'
  and fs_Account_key = 2994;
update fin.dim_fs_account
set gm_account = '629A'
where gl_account = '1628001'
  and fs_Account_key = 1569;
update fin.dim_fs_account
set gm_account = '627E'
where gl_account = '1626006'
  and fs_Account_key = 96407;  
update fin.dim_fs_account
set gm_account = '604A'
where gl_account = '1605001'
  and fs_Account_key = 3283;    
update fin.dim_fs_account
set gm_account = '627E'
where gl_account = '1631006'
  and fs_Account_key = 4067;   

select factory_financial_year, g_l_acct_number, factory_account as old_factory
from  arkona.ext_ffpxrefdta  
where g_l_acct_number in ('1620001','1628001','1626006','1605001','1631006')
  and factory_financial_year in (2018, 2019)
order by g_l_acct_number,factory_financial_year;

  
-- change fin.dim_fs_account
 

-- 2/2/19: ffpxrefdta failed ----------------------------------------------------------------------------------
-- 3076 rows: 2019 data

select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x

-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;     


-------------------------------------------------------------------------

1/11/19

selecT * from arkona.ext_ffpxrefdta where g_l_acct_number = '127703' -- 2018: 277E

selecT * from arkona.ext_ffpxrefdta_tmp where g_l_acct_number = '127703' -- 2018 : 285

select factory_financial_year, count(*) from arkona.ext_ffpxrefdta_tmp group by factory_financial_year

dont know the side effects yet, but to get luigi ffpxrefdta to pass

update arkona.ext_ffpxrefdta
set factory_account = '285'
where g_l_acct_number = '127703'
  and factory_financial_year = 2018;

-- didn't do this, and the monthly update failed on it
update fin.dim_fs_account
set gm_account = '285'
where gl_account = '127703';

12/5

select *
from arkona.ext_ffpxrefdta d
except
select *
from arkona.ext_ffpxrefdta_tmp e

select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta g   

                     
routing for account 1640006 changed from 640E to 640D

select * from fin.dim_Account where account = '1640006'
select * from c where g_l_acct_number = '1640006'

-- 12/5 caused fs script to fail, changed it back to 640E, check with jeri
-- Jeri: No, it will change.  I just needed to adjust to submit the statement.  Likely will stick through December and change in January.
update arkona.ext_ffpxrefdta
set factory_account = '640D'
where g_l_acct_number = '1640006'
  and factory_financial_year = 2018;

select * from fin.dim_fs_account where gm_account in ('640e','640d')

10/3
new accounts: '1658001','1626006','1426006','2658001'
one account, changed routing 146424 from 464 to 462 -- checking with jeri on this one

-- 1. add new accounts to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '09/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in ('1658001','1626006','1426006','2658001');


select *
from arkona.ext_ffpxrefdta_tmp 
where g_l_acct_number = '146424'
  and factory_financial_year = 2018
union all
select *
from arkona.ext_ffpxrefdta
where g_l_acct_number = '146424'
  and factory_financial_year = 2018

whats weird, is that in ext_ffpxrefdta, there are 2 rows for 146424, one to 464 and one to 462
and apparently it is the only account like that:
select factory_code, factory_financial_year, g_l_acct_number
from arkona.ext_ffpxrefdta
group by factory_code, factory_financial_year, g_l_acct_number
having count(*) > 1

anyway, to get luigi to pass until i hear from jeri, this is the fix

delete
-- select *
from arkona.ext_ffpxrefdta
where g_l_acct_number = '146424'
  and factory_financial_year = 2018
  and factory_account = '464'

-- 9/5/18
new accounts: 244502, 230402, 264502, 264502A, 144502, 164502, 164502A

-- 1. add to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '09/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in ('244502', '230402', '264502', '264502A', '144502', '164502', '164502A');


-- 9/2/18
new account: 130402

select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta

-- 1. add to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '09/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in ('130402');

--8/2/18
new accounts: 122001A, 222001A

select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta

-- 1. add to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '08/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in ('122001A', '222001A');



-- 7/3/18

select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta

select * from arkona.ext_ffpxrefdta where g_l_acct_number = '266500a'

new account, 266500a

select * from arkona.ext_ffpxrefdta_tmp where g_l_acct_number = '266500a'
select * from fin.dim_account where account = '266500a'
select * from fin.dim_fs_account where gl_account = '266500a'

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '07/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '266500A';


--5/31/18
select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta

select * from arkona.ext_ffpxrefdta where g_l_acct_number = '15724'

new account, 15724

select * from arkona.ext_ffpxrefdta_tmp where g_l_acct_number = '15724'
select * from fin.dim_account where account = '15724'
select * from fin.dim_fs_account where gl_account = '15724'

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '05/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '15724';




-- 5/2/18
select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta

select * from arkona.ext_ffpxrefdta where g_l_acct_number = '190504'

new account, 190504

select * from fin.dim_account where account = '190504'

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '04/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '190504';


/*
4/3/18
reversed order, most recent now on top
*/

-- 4-3-18 
-- 2 accounts added to routing
-- 132407
-- 232407

select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '03/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '132407';
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '03/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '232407';




select * from dds.ext_ffpxrefdta -- 144850

select * from dds.ext_ffpxrefdta  -- 16975
where coalesce(consolidation_grp, '1')  <> '3'
  and factory_code = 'GM'
  and factory_account <> '331A'
  and factory_financial_year > 2010
  and g_l_acct_number is not null limit 100


select factory_financial_year as the_year, 
  CASE coalesce(consolidation_grp, '1') 
    when '1' then 'RY1'::citext
    when '2' then 'RY2'::citext
  end as store_code,
  g_l_acct_number as gl_account, factory_account as gm_account,
  fact_account_ as multiplier
from dds.ext_ffpxrefdta 
where coalesce(consolidation_grp, '1')  <> '3' -- ry1/ry2 only
  and factory_code = 'GM'
  and factory_account <> '331A' -- don't remember why, but this is necessary
  and factory_financial_year > 2010
  and g_l_acct_number is not null -- eliminates the XFCOPY... rows



select the_year, gl_account, gm_account from (
select factory_financial_year as the_year, 
  CASE coalesce(consolidation_grp, '1') 
    when '1' then 'RY1'::citext
    when '2' then 'RY2'::citext
  end as store_code,
  g_l_acct_number as gl_account, factory_account as gm_account,
  fact_account_ as multiplier
from dds.ext_ffpxrefdta 
where coalesce(consolidation_grp, '1')  <> '3' -- ry1/ry2 only
  and factory_code = 'GM'
  and factory_account <> '331A' -- don't remember why, but this is necessary
  and factory_financial_year > 2010
  and g_l_acct_number is not null -- eliminates the XFCOPY... rows
) x group by the_year, gl_account,gm_account having count(*) > 1  

drop table if exists dds.xfm_ffpxrefdta cascade;
create table dds.xfm_ffpxrefdta (
  the_year integer not null,
  gl_account citext not null,
  gm_account citext not null,
  store_code citext not null,
  multiplier numeric(2,1) not null default 1,
  constraint xfm_ffpxrefdta_pk primary key (the_year,gl_account,gm_account));

insert into dds.xfm_ffpxrefdta
select factory_financial_year as the_year, 
  g_l_acct_number as gl_account, factory_account as gm_account,
  CASE coalesce(consolidation_grp, '1') 
    when '1' then 'RY1'::citext
    when '2' then 'RY2'::citext
  end as store_code,
  fact_account_ as multiplier
from dds.ext_ffpxrefdta 
where coalesce(consolidation_grp, '1')  <> '3' -- ry1/ry2 only
  and factory_code = 'GM'
  and factory_account <> '331A' -- don't remember why, but this is necessary
  and factory_financial_year > 2010
  and g_l_acct_number is not null; -- eliminates the XFCOPY... rows  

  
create table dds.xfm_ffpxrefdta_tmp (
  the_year integer not null,
  gl_account citext not null,
  gm_account citext not null,
  store_code citext not null,
  multiplier numeric(2,1) not null default 1,
  constraint xfm_ffpxrefdta_tmp_pk primary key (the_year,gl_account,gm_account));  



insert into ops.tasks values ('ffpxrefdta','Daily');

select * from dds.xfm_ffpxrefdta 


                select *
                from ((
                    select *
                    from dds.xfm_ffpxrefdta d
                    except
                    select *
                    from dds.xfm_ffpxrefdta_tmp e)
                  union (
                    select *
                    from dds.xfm_ffpxrefdta_tmp f
                    except
                    select *
                    from dds.xfm_ffpxrefdta g)) x

-- 8/23/16 
-- routing for the new marketing holding accounts (12315,12415,12515,12715,12915)    
insert into dds.xfm_ffpxrefdta (the_year,gl_account,gm_account,store_code,multiplier)
select *
from dds.xfm_ffpxrefdta_tmp f
except
select *
from dds.xfm_ffpxrefdta g


-- 10/6/16
move to arkona schema     

select *
from arkona.xfm_ffpxrefdta_tmp f
except
select *
from arkona.xfm_ffpxrefdta g

-- get out of dds
alter table dds.ext_ffpxrefdta
rename to z_unused_ext_ffpxrefdta;
alter table dds.xfm_ffpxrefdta
rename to z_unused_xfm_ffpxrefdta;
alter table dds.xfm_ffpxrefdta_tmp
rename to z_unused_xfm_ffpxrefdta_tmp;

in september, jeri added one new account: 16603 (pdq contributions)
add that to ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta g

-- -- changes in ffpxrefdta:
--   update arkona.ext_ffpxrefdta
--   update fin.dim_fs

select * from arkona.ext_Ffpxrefdta where g_l_acct_number = '16603'

select * from arkona.ext_eisglobal_sypffxmst where fxmact = '066d'

select * from ops.task_dependencies where successor = 'sypffxmst' or predecessor = 'sypffxmst'

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '09/12/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '16603'


-- 1/3/17
-- 3 new accounts added for December
-- 230700
-- 285003
-- 130700
                select *
                from ((
                    select *
                    from arkona.ext_ffpxrefdta d
                    except
                    select *
                    from arkona.ext_ffpxrefdta_tmp e)
                  union (
                    select *
                    from arkona.ext_ffpxrefdta_tmp f
                    except
                    select *
                    from arkona.ext_ffpxrefdta g)) x

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '230700';
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '130700';
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '285003';


-- 1/13/17
-- 2 new accounts added in January, this time, jeri added the routing immediately after adding the new accounts in glpmast
-- 180601
-- 180801
                select *
                from ((
                    select *
                    from arkona.ext_ffpxrefdta d
                    except
                    select *
                    from arkona.ext_ffpxrefdta_tmp e)
                  union (
                    select *
                    from arkona.ext_ffpxrefdta_tmp f
                    except
                    select *
                    from arkona.ext_ffpxrefdta g)) x

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '01/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in ('180601','180801');

-- 2/2/17
-- 2017 added to ffpxrefdta
select max(factory_financial_year) from arkona.ext_ffpxrefdta
select max(factory_financial_year) from arkona.ext_ffpxrefdta_tmp

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta 


-- 3/3/17
-- new rows - added in february
-- need to be added arkona.ext_ffpxrefdta and to fin.dim_fs_account

select g_l_acct_number, factory_financial_year
from arkona.ext_ffpxrefdta
group by g_l_acct_number, factory_financial_year
having count(*) > 1

-- new rows in ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number)
  
-- 1. add to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

-- rows missing from dim_fs_account
select a.account
from fin.dim_account a
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account)  

-- 
-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '02/02/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
      where gl_Account = a.account)); 




-- removed rows
select * 
from arkona.ext_ffpxrefdta a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta_tmp
  where g_l_acct_number = a.g_l_acct_number)

-- 5/4/17
-- On May 4, 2017, at 7:24 AM, Jon Andrews <jandrews@cartiva.com> wrote:
-- Why was account 145400 (SLS USED GM PROT PLANS) rerouted to 455 (Other Protection Plans) from 454
--  
-- And account 165400 (C/S USED GM PROT PLANS) rerouted to 655 (Other Protection Plans) from 654
-- 
-- Because only activity this month was an unwound deal and gm wouldn't let me show a negative count. It will be changed back now that statement is submitted. I can't get away with anything :) 
-- but that leaves me with 11301F

select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
  

-- changed rows
select *
from (
  select factory_financial_year, g_l_acct_number, md5(a::text) as hash
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, md5(b::text) as hash
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.hash <> d.hash


select 'xfm' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
-- select *
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2017
  and g_l_acct_number in ('11301F')
union
select 'ext', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2017
  and g_l_acct_number in ('11301F')  
order by g_l_acct_number, source
  

select company_number, count(*) from arkona.ext_ffpxrefdta group by company_number

select company_number, count(*) from arkona.ext_ffpxrefdta_tmp group by company_number

can not figure out in 5250 where this goofy one off of 11301F / 202 with company number RY2 comes from
so just exclude it


-- 6/30/17
-- new rows - added in june: 16902W, 16102W, 16802W, 146901,166901, 246901, 266901
-- need to be added arkona.ext_ffpxrefdta and to fin.dim_fs_account

-- new rows in ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number)
  
-- 1. add to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

-- rows missing from dim_fs_account
select a.account
from fin.dim_account a
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account)  

-- !!!!!!!!!!!!!!!!!!! 6/30 - 7/2, done
-- jeri has also added 146901,166901, 246901, 266901 but not routed them yet
-- hold off on  updating until all have been routed
-- 
-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '06/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
      where gl_Account = a.account)); 

-- 3. 7/4/17, page 2 failing accounts missing from fin.dim_fs_account for gm_account <EX061Z, <EX061V

select *
from fin.dim_fs_account
where gm_account in ('<EX061Z','<EX061V')

insert into fin.dim_fs_account (gm_account,gl_account,row_from_date,current_row,row_reason)values
('<G&AV','16102W',current_date,true,'Page 2'),
('<G&AV','16802W',current_date,true,'Page 2'),
('<G&AV','16902W',current_date,true,'Page 2');
-- ('P2L57C1','16102W',current_date,true,'Page 2'),
-- ('P2L57C1','16802W',current_date,true,'Page 2'),
-- ('P2L57C1','16902W',current_date,true,'Page 2'),
-- ('P2L57C7','16102W',current_date,true,'Page 2'),
-- ('P2L57C7','16802W',current_date,true,'Page 2'),
-- ('P2L57C7','16902W',current_date,true,'Page 2');
-- ('P2L56C1','16102W',current_date,true,'Page 2'),
-- ('P2L56C1','16802W',current_date,true,'Page 2'),
-- ('P2L56C1','16902W',current_date,true,'Page 2'),
-- ('P2L56C7','16102W',current_date,true,'Page 2'),
-- ('P2L56C7','16802W',current_date,true,'Page 2'),
-- ('P2L56C7','16902W',current_date,true,'Page 2');
-- ('P2L40C1','16102W',current_date,true,'Page 2'),
-- ('P2L40C1','16802W',current_date,true,'Page 2'),
-- ('P2L40C1','16902W',current_date,true,'Page 2');
-- ('P2L40C7','16102W',current_date,true,'Page 2'),
-- ('P2L40C7','16802W',current_date,true,'Page 2'),
-- ('P2L40C7','16902W',current_date,true,'Page 2');
-- ('<EX068V','16802W',current_date,true,'Page 2'),
-- ('<EX069Z','16902W',current_date,true,'Page 2'),
-- ('<EX068Z','16802W',current_date,true,'Page 2'),
-- ('<EX069V','16902W',current_date,true,'Page 2');
-- ('<EX061Z','16102W',current_date,true,'Page 2'),
-- ('<EX061v','16102W',current_date,true,'Page 2');


-- 8/1/17: ffpxrefdta failed ----------------------------------------------------------------------------------
-- looks like 46 new auto outlet accounts
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
  

-- changed rows
select *
from (
  select factory_financial_year, g_l_acct_number, md5(a::text) as hash
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, md5(b::text) as hash
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.hash <> d.hash


-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;    

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '07/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
   where gl_Account = a.account)); 



select * from  fin.dim_fs_account where row_from_date = '07/01/2017'

select * from fin.dim_fs limit 100

-- 8/2 FS page 2 fucked up, generation fails 

-- 9/1/17: ffpxrefdta failed ----------------------------------------------------------------------------------
-- looks like 10 new auto outlet accounts
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
  

-- changed rows
select *
from (
  select factory_financial_year, g_l_acct_number, md5(a::text) as hash
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, md5(b::text) as hash
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.hash <> d.hash


-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;    

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '08/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
   where gl_Account = a.account)); 

select *
from fin.dim_fs_account
where gl_account like '146700%'

select *
from fin.dim_account
where account like '146700%'


select 'tmp', a.* from arkona.ext_ffpxrefdta_tmp a where factory_financial_year = 2017 and g_l_acct_number = 'SPLT146700' 
union 
select '', a.* from arkona.ext_ffpxrefdta a where factory_financial_year = 2017 and g_l_acct_number = 'SPLT146700'  

-- 9/7/17 damnit, still not as clear as i should be on this, but, current reality is that on db2, there is only one row for SPLT146700
--          and it's value for factory_account10 is 146700D, so, i am going to delete the "extraneous" row in ext_ffpxrefdta
delete
from arkona.ext_ffpxrefdta
where factory_financial_year = 2017
  and g_l_acct_number = 'SPLT146700'
  and factory_Account10 = '146700'

-- reran ffpxrefdta scrape, good  

-- and now there are 2 more new auto outlet accounts: 191002, 180803

-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta; 

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '08/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
   where gl_Account = a.account)); 



-- 10/3/17: ffpxrefdta failed ----------------------------------------------------------------------------------
-- looks like 1 new auto outlet accounts 19202W
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
  
-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta; 

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '09/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
   where gl_Account = a.account)); 



-- 11/3/17: ffpxrefdta failed ----------------------------------------------------------------------------------
-- looks like 1 new auto outlet accounts 11302W
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
  
-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta; 

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '10/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
   where gl_Account = a.account)); 

-- 12/4/17: ffpxrefdta failed ----------------------------------------------------------------------------------
-- sales/cogs for honda clarity
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
  
-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta; 

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '11/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
   where gl_Account = a.account)); 

-- 1/3/17: ffpxrefdta failed ----------------------------------------------------------------------------------
-- the lacrosse account 1406004
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
  
-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta; 

-- get rid of the bogus row for 2017
delete 
-- select *
from arkona.ext_ffpxrefdta
where factory_financial_year = 2017
  and g_l_acct_number = '1406004'
  and factory_account = '425D'
  
-- may need to remove row from fin.dim_fs_account, don't know yet
-- the incorrect routing (1406004 -> 425D) remains in rydedata.ffpxrefdta for 2016
select * from arkona.ext_Ffpxrefdta where g_l_acct_number in ( '1406004','1406104') order by g_l_acct_number, factory_financial_year

select * from fin.dim_fs_account where gl_account in  ( '1406004','1406104') 



select max(factory_financial_year) from arkona.ext_Ffpxrefdta

select * from arkona.ext_Ffpxrefdta where g_l_acct_number in ( '1606004','1606104') order by g_l_acct_number, factory_financial_year

select * from fin.dim_fs_account where gl_account in  ( '1606004','1606104') 




-- 1/4/17: ffpxrefdta failed ----------------------------------------------------------------------------------
-- the lacrosse account, now the cogs 1606004, plus new accounts for bolt, outlet meals
select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x

-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta; 

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
   where gl_Account = a.account)); 

-- get rid of the bogus row for 2017 for the lacrosse account
delete 
-- select *
from arkona.ext_ffpxrefdta
where factory_financial_year = 2017
  and g_l_acct_number = '1606004'
  and factory_account = '625D'   


-- 1/17/18: ffpxrefdta failed ----------------------------------------------------------------------------------
-- 3055 rows: 2018 data

select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x

-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;     


-- 2. add to fin.dim_fs_account
-- no new accounts to add
-- insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
   where gl_Account = a.account));


-- 2/2/18: ffpxrefdta failed ----------------------------------------------------------------------------------
-- 1 new account: 126106

select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x

-- 1. add new rows to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;        

-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '01/01/2018', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
   where gl_Account = a.account));


-- 2/5/18: ffpxrefdta failed ----------------------------------------------------------------------------------
-- 5 changed accounts

select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
order by g_l_acct_number    
    
-- changed rows
select *
from (
  select factory_financial_year, g_l_acct_number, md5(a::text) as hash
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, md5(b::text) as hash
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.hash <> d.hash

select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
-- select *
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number in ('1627006','1626004','1640012','1631012','146424')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2018
  and g_l_acct_number in ('1627006','1626004','1640012','1631012','146424')  
order by g_l_acct_number, source    

146424: this is a valid correction, changed from 462 to 464, 
COA: SLS NEW VEH PREP WASH
462: p16 l27: warranty claim labor
464: p16 l29: new veh insp lbr
-- has been wrong since 2016
select amount, gl_account, gm_account, year_month, page, line, col, line_label
from fin.fact_fs a
inner join fin.dim_fs_account b on a.fs_account_key = b.fs_Account_key   
inner join fin.dim_fs c on a.fs_key = c.fs_key
-- where b.gm_account = '462'
where gl_account = '146424'
order by year_month

-- change it for 2018 
update arkona.ext_ffpxrefdta
set factory_account = '464'
-- select * from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number = '146424';

The other 4 are all new vehicle cogs accounts, fin statement bombs if there are cogs against an account
for which there are no sales, so jeri has to temporarily reroute to a different account, and then,
next month will route them back to the correct accounts

so, i will change them for 2018, knowing that i will be changing them back later

update arkona.ext_ffpxrefdta
set factory_account = '627D'
-- select * from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number = '1626004';
  
update arkona.ext_ffpxrefdta
set factory_account = '631E'
-- select * from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number = '1627006';

update arkona.ext_ffpxrefdta
set factory_account = '632F'
-- select * from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number = '1631012';

update arkona.ext_ffpxrefdta
set factory_account = '640A'
-- select * from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number = '1640012';


select * 
from fin.dim_fs_account 
where gl_account in ('1627006','1626004','1640012','1631012')  
order by gl_account, row_from_date

need temporary rows in dim_fs_account


insert into fin.dim_fs_account (gm_Account, gl_account, row_from_date, current_row, row_reason)
values
('627D','1626004',current_date,true,'temp routing'),
('631E','1627006',current_date,true,'temp routing'),
('632F','1631012',current_date,true,'temp routing'),
('640A','1640012',current_date,true,'temp routing');


-- 3/2/18: ffpxrefdta failed ----------------------------------------------------------------------------------
-- 4 changed accounts
-- these are the 4 new cogs accounts jeri had to temporarily change last month
-- hmm not sure what i need to do

select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
order by g_l_acct_number    
    
-- changed rows
select *
from (
  select factory_financial_year, g_l_acct_number, md5(a::text) as hash
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, md5(b::text) as hash
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.hash <> d.hash

select 'old' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
-- select *
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number in ('1627006','1626004','1640012','1631012')
union
select 'new', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2018
  and g_l_acct_number in ('1627006','1626004','1640012','1631012')  
order by g_l_acct_number, source   

-- i think all i need to do is update arkona.ext_ffpxrefdta for 2018 with the correct factory_account
-- for these 4 gl_accounts.
-- that will keep the script from failing
-- dim_fs_account already has the correct routing combination (in addition to the bogus temp routing)

update arkona.ext_ffpxrefdta
set factory_account = '626D'
-- select * from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number = '1626004';
  
update arkona.ext_ffpxrefdta
set factory_account = '627E'
-- select * from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number = '1627006';

update arkona.ext_ffpxrefdta
set factory_account = '631F'
-- select * from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number = '1631012';

update arkona.ext_ffpxrefdta
set factory_account = '640F'
-- select * from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2018
  and g_l_acct_number = '1640012';
