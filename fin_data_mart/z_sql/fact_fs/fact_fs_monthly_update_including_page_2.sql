﻿-- this will also expose the r_units_missing_cost from above, these require no action
do
$$
begin
assert (
  select count(*)
  from (
    select * 
    from nc.vehicle_acquisitions a
    where thru_date > current_date
      and not exists (
        select 1
        from fin.fact_gl
        where control = a.stock_number
          and post_status = 'Y'))x ) = 0, 'possible void dealer trade';
end
$$;

-- 8/16/19 i believe this excludes the r_units_missing_cost which generate a false positive
do
$$
begin
assert (
  select count(*)
  from (
    select * 
    from nc.vehicle_acquisitions a
    where thru_date > current_date
      and not exists (
        select 1
        from fin.fact_gl
        where control = a.stock_number
          and post_status = 'Y')
      and not exists (
        select 1
        from (
          select *
          from nc.get_r_units_missing_cost())aa
        where a.stock_number = aa.stock_number))x ) = 0, 'possible void dealer trade';      
end
$$;
