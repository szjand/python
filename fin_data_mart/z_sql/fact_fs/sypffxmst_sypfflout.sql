﻿-- eisglobal_sypffxmst goto the bottom
3/6/20
-- failed rows
select *
from ((
    select *
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select *
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select *
    from arkona.ext_eisglobal_sypfflout f
    except
    select *
    from arkona.ext_eisglobal_sypfflout_tmp g)) x
-- the difference is in fldata, OTHER is spelled with a 0 in the old version of the row
select 'tmp', a.* from arkona.ext_eisglobal_sypfflout_tmp a where flcyy = 2020 and flpage = 5 and flseq = 43 and flline = 49.0
union
select 'base', a.* from arkona.ext_eisglobal_sypfflout a where flcyy = 2020 and flpage = 5 and flseq = 43 and flline = 49.0

-- and the fix
update arkona.ext_eisglobal_sypfflout z
set fldata = x.fldata,
    flcont = x.flcont
from ( -- short hand changed rows
  select *
  from arkona.ext_eisglobal_sypfflout_tmp 
  except
  select *
  from arkona.ext_eisglobal_sypfflout) x   
where z.flcode = x.flcode
  and z.flcyy = x.flcyy
  and z.flpage = x.flpage
  and z.flseq = x.flseq
  and z.flflne = x.flflne
  and z.flflsq = x.flflsq

-- 01/08/21
-- rows for the new year
-- insert new rows
insert into arkona.ext_eisglobal_sypfflout
select *
from arkona.ext_eisglobal_sypfflout_tmp a
where not exists (
  select 1
  from arkona.ext_eisglobal_sypfflout
  where flcode = a.flcode
    and flcyy = a.flcyy
    and flpage = a.flpage
    and flseq = a.flseq
    and flflne = a.flflne
    and flflsq = a.flflsq);


1/8/20
-- 1 failed row
select *
from ((
    select *
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select *
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select *
    from arkona.ext_eisglobal_sypfflout f
    except
    select *
    from arkona.ext_eisglobal_sypfflout_tmp g)) x
-- the difference is in fldata, OTHER is spelled with a 0 in the old version of the row
select 'tmp', a.* from arkona.ext_eisglobal_sypfflout_tmp a where flcyy = 2020 and flpage = 14 and flseq = 1 and flline = 6
union
select 'base', a.* from arkona.ext_eisglobal_sypfflout a where flcyy = 2020 and flpage = 14 and flseq = 1 and flline = 6

-- and the fix
update arkona.ext_eisglobal_sypfflout z
set fldata = x.fldata,
    flcont = x.flcont
from ( -- short hand changed rows
  select *
  from arkona.ext_eisglobal_sypfflout_tmp 
  except
  select *
  from arkona.ext_eisglobal_sypfflout) x   
where z.flcode = x.flcode
  and z.flcyy = x.flcyy
  and z.flpage = x.flpage
  and z.flseq = x.flseq
  and z.flflne = x.flflne
  and z.flflsq = x.flflsq

1/3/20
rows for the new year

select flcyy, count(*) from arkona.ext_eisglobal_sypfflout group by flcyy order by flcyy

-- insert new rows
insert into arkona.ext_eisglobal_sypfflout
select *
from arkona.ext_eisglobal_sypfflout_tmp a
where not exists (
  select 1
  from arkona.ext_eisglobal_sypfflout
  where flcode = a.flcode
    and flcyy = a.flcyy
    and flpage = a.flpage
    and flseq = a.flseq
    and flflne = a.flflne
    and flflsq = a.flflsq);


-- 6/13/18
-- need definitive queries to expose changed rows, and a query for new rows
-- new rows
select *
from arkona.ext_eisglobal_sypfflout_tmp a
where not exists (
  select 1
  from arkona.ext_eisglobal_sypfflout
  where flcode = a.flcode
    and flcyy = a.flcyy
    and flpage = a.flpage
    and flseq = a.flseq
    and flflne = a.flflne
    and flflsq = a.flflsq)

-- insert new rows
insert into arkona.ext_eisglobal_sypfflout
select *
from arkona.ext_eisglobal_sypfflout_tmp a
where not exists (
  select 1
  from arkona.ext_eisglobal_sypfflout
  where flcode = a.flcode
    and flcyy = a.flcyy
    and flpage = a.flpage
    and flseq = a.flseq
    and flflne = a.flflne
    and flflsq = a.flflsq);
    
-- changed rows
select *
from (-- hash on permanent table 
  select a.*,
    (
      select md5(z::text) as hash
      from (
        select flcode, flcyy, flpage, flseq, flline, flflne, flflsq, fldata, flcont
        from arkona.ext_eisglobal_sypfflout
          where flcode = a.flcode
            and flcyy = a.flcyy
            and flpage = a.flpage
            and flseq = a.flseq
            and flflne = a.flflne
            and flflsq = a.flflsq) z)
  from arkona.ext_eisglobal_sypfflout a) e
inner join ( -- hash on tmp table 
  select a.*,
    (
      select md5(x::text) as hash
      from (
        select  flcode, flcyy, flpage, flseq, flline, flflne, flflsq, fldata, flcont
        from arkona.ext_eisglobal_sypfflout_tmp
          where flcode = a.flcode
            and flcyy = a.flcyy
            and flpage = a.flpage
            and flseq = a.flseq
            and flflne = a.flflne
            and flflsq = a.flflsq) x)
  from arkona.ext_eisglobal_sypfflout_tmp a) f on e.flcode = f.flcode
      and e.flcyy = f.flcyy
      and e.flpage = f.flpage
      and e.flseq = f.flseq
      and e.flflne = f.flflne
      and e.flflsq = f.flflsq
      and e.hash <> f.hash

-- and the fix
update arkona.ext_eisglobal_sypfflout z
set fldata = x.fldata,
    flcont = x.flcont
from ( -- short hand changed rows
  select *
  from arkona.ext_eisglobal_sypfflout_tmp 
  except
  select *
  from arkona.ext_eisglobal_sypfflout) x   
where z.flcode = x.flcode
  and z.flcyy = x.flcyy
  and z.flpage = x.flpage
  and z.flseq = x.flseq
  and z.flflne = x.flflne
  and z.flflsq = x.flflsq

  
          
-- 5/27 another theory eats shit and dies
-- sypfflout failed last night, on a 2017 line

select *
from ((
    select *
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select *
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select *
    from arkona.ext_eisglobal_sypfflout f
    except
    select *
    from arkona.ext_eisglobal_sypfflout_tmp g)) x order by flflne


select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and flpage = 5
  and flflne in (37)
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and flpage = 5
  and flflne in (37)
order by fxmlne, fxmcol, source 

-- just fix it
update arkona.ext_eisglobal_sypfflout
set fldata = '|TRAX                                |#1  |  |#2      |  |#3      |  |#4   |  |#5  |  |#6      |  |#7      |  |#8   |'
where flcyy = 2017
  and flpage = 5
  and flflne in (37)

    
-- -- 5/26/18 have put off dealing with the changes from 5/17
-- today i realize i am not doing type2, so primary keys, hashes update vs insert/delete are all irrelevant
-- which leads me to think that when there are changes, simply overwrite
-- looking at the fact_fs_monthly_update script, data is generated directly from the ext_eisglobal tables
-- which also supports just overwriting, i believe the changes are only ever for current year

delete 
from arkona.ext_eisglobal_sypffxmst 
where fxmcyy = 2018;
insert into arkona.ext_eisglobal_sypffxmst 
select *
from arkona.ext_eisglobal_sypffxmst_tmp 
where fxmcyy = 2018;

delete 
from arkona.ext_eisglobal_sypfflout 
where flcyy = 2018;
insert into arkona.ext_eisglobal_sypfflout 
select *
from arkona.ext_eisglobal_sypfflout_tmp 
where flcyy = 2018;

    
--5/17/18 bunch of changes page 17 in both, but am mind fucking on understanding new row vs changed row 
-- and on changed row type 1 or type 2
-- except gives me diffs, including new or removed rows

-- ok, we have a candidate key
select fxmcyy, fxmact, fxmpge, fxmlne from arkona.ext_eisglobal_sypffxmst_tmp group by fxmcyy, fxmact, fxmpge, fxmlne having count(*) > 1

alter table arkona.ext_eisglobal_sypffxmst 
add primary key (fxmcyy, fxmact, fxmpge, fxmlne);

alter table arkona.ext_eisglobal_sypffxmst_tmp 
add primary key (fxmcyy, fxmact, fxmpge, fxmlne);

-- and now we have primary keys
-- added rows:
select *
from arkona.ext_eisglobal_sypffxmst_tmp a
where not exists (
  select 1
  from arkona.ext_eisglobal_sypffxmst 
  where fxmcyy = a.fxmcyy
    and fxmact = a.fxmact
    and a.fxmpge = a.fxmpge 
    and fxmlne = a.fxmlne)

-- removed rows -- eek, or changed PK, which is what is happening 5/17
select *
from arkona.ext_eisglobal_sypffxmst a
where not exists (
  select 1
  from arkona.ext_eisglobal_sypffxmst_tmp 
  where fxmcyy = a.fxmcyy
    and fxmact = a.fxmact
    and a.fxmpge = a.fxmpge 
    and fxmlne = a.fxmlne)    

-- hash on permanent table    
select a.*,
  (
    select md5(z::text) as hash
    from (
      select fxmcde,fxmcyy,fxmact,fxmpge,fxmlne,fxmcol,fxmstr
      from arkona.ext_eisglobal_sypffxmst
      where fxmcyy = a.fxmcyy
        and fxmact = a.fxmact
        and fxmpge = a.fxmpge 
        and fxmlne = a.fxmlne) z)
from arkona.ext_eisglobal_sypffxmst a

-- hash on temp table
select a.*,
  (
    select md5(z::text) as hash
    from (
      select fxmcde,fxmcyy,fxmact,fxmpge,fxmlne,fxmcol,fxmstr
      from arkona.ext_eisglobal_sypffxmst_tmp
      where fxmcyy = a.fxmcyy
        and fxmact = a.fxmact
        and fxmpge = a.fxmpge 
        and fxmlne = a.fxmlne) z)
from arkona.ext_eisglobal_sypffxmst_tmp a    

-- changed rows
select *
from (
  select a.*,
    (
      select md5(z::text) as hash
      from (
        select fxmcde,fxmcyy,fxmact,fxmpge,fxmlne,fxmcol,fxmstr
        from arkona.ext_eisglobal_sypffxmst
        where fxmcyy = a.fxmcyy
          and fxmact = a.fxmact
          and fxmpge = a.fxmpge 
          and fxmlne = a.fxmlne) z)
  from arkona.ext_eisglobal_sypffxmst a) e
inner join (  
  select a.*,
    (
      select md5(z::text) as hash
      from (
        select fxmcde,fxmcyy,fxmact,fxmpge,fxmlne,fxmcol,fxmstr
        from arkona.ext_eisglobal_sypffxmst_tmp
        where fxmcyy = a.fxmcyy
          and fxmact = a.fxmact
          and a.fxmpge = a.fxmpge 
          and fxmlne = a.fxmlne) z)
  from arkona.ext_eisglobal_sypffxmst_tmp a) f on e.fxmcyy = f.fxmcyy
            and e.fxmact = f.fxmact
            and e.fxmpge = f.fxmpge 
            and e.fxmlne = f.fxmlne
            and e.hash <> f.hash


-- 2/12/19 fix changed row

select 'archive', fxmcde,fxmcyy,fxmact,fxmpge,fxmlne,fxmcol,fxmstr
from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2019
  and fxmact = '470'
  and fxmpge = 16
  and fxmlne = 35.0
union 
select 'scrape', fxmcde,fxmcyy,fxmact,fxmpge,fxmlne,fxmcol,fxmstr
from arkona.ext_eisglobal_sypffxmst_tmp
where fxmcyy = 2019
  and fxmact = '470'
  and fxmpge = 16
  and fxmlne = 35.0

update  arkona.ext_eisglobal_sypffxmst
set fxmcol = 1,
    fxmstr = 38
where fxmcyy = 2019
  and fxmact = '470'
  and fxmpge = 16
  and fxmlne = 35.0

---------------------------------------------------------------
--< 2/14/19 sypffxmst
---------------------------------------------------------------
select count (*)
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x

-- count = 1
-- think this means a row has been removed
select *
from arkona.ext_eisglobal_sypffxmst
except
select *
from arkona.ext_eisglobal_sypffxmst_tmp

select * -- no rows
from arkona.ext_eisglobal_sypffxmst_tmp
where fxmcyy = 2019
  and fxmact = '<TFG'

select * -- yep, row no longer exists
from arkona.ext_eisglobal_sypffxmst_tmp
where fxmcyy = 2019
  and fxmpge = 3
  and fxmlne = 12.0
  
delete from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2019
  and fxmact = '<TFG'
  and fxmpge = 3 
  and fxmlne = 12.0;
---------------------------------------------------------------
--/> 2/14/19 sypffxmst
---------------------------------------------------------------

---------------------------------------------------------------
--< 6/11/19 sypffxmst
---------------------------------------------------------------
select count (*)
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x

-- fxmact <M220CD fxmlne change from 57.0 to 55.0
select 'new', *
from arkona.ext_eisglobal_sypffxmst_tmp
where fxmact = '<M220CD'
and fxmcyy = 2019
union
select 'old', *
from arkona.ext_eisglobal_sypffxmst
where fxmact = '<M220CD'
and fxmcyy = 2019

update arkona.ext_eisglobal_sypffxmst
set fxmlne = 55.0
where fxmact = '<M220CD'
  and fxmcyy = 2019;
---------------------------------------------------------------
--/> 6/11/19 sypffxmst
---------------------------------------------------------------







            
1/16/2018
flipped the order, most recent now on top
what we are looking at is all the 2018 data for sypffxmst and sypfflout
1/16/19 ha, same day, new year, insert the 2019 data

-- sypffxmst ------------------------------------------------------------------
select *
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst 
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp 
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x

insert into arkona.ext_eisglobal_sypffxmst
select *
from arkona.ext_eisglobal_sypffxmst_tmp 
except
select *
from arkona.ext_eisglobal_sypffxmst;

-- sypfflout ------------------------------------------------------------------  

select *
from ((
    select *
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select *
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select *
    from arkona.ext_eisglobal_sypfflout f
    except
    select *
    from arkona.ext_eisglobal_sypfflout_tmp g)) x order by flflne

insert into arkona.ext_eisglobal_sypfflout
select *
from arkona.ext_eisglobal_sypfflout_tmp d
except
select *
from arkona.ext_eisglobal_sypfflout e;


    
2/1/17
-- sypffxmst ------------------------------------------------------------------
select *
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst 
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp 
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x

select 'archive' as source, a.*
from arkona.ext_eisglobal_sypffxmst a
where fxmcyy = 2017
  and fxmpge = 16
  and fxmlne in (3,6)
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypffxmst_tmp a
where fxmcyy = 2017
  and fxmpge = 16
  and fxmlne in (3,6)
order by fxmlne, fxmcol, source 

2/7/17
looks like page 16 line 3/6, col 2/4 get added back
select * from arkona.ext_eisglobal_sypffxmst where fxmpge = 16 and fxmlne in( 3.0, 6.0) and fxmcol in (2,4) and fxmcyy = 2017

insert into arkona.ext_eisglobal_sypffxmst
select * from arkona.ext_eisglobal_sypffxmst_tmp where fxmpge = 16 and fxmlne in( 3.0, 6.0) and fxmcol in (2,4) and fxmcyy = 2017 

looks like line 3 col 2 & 4, line 6 col 2 & 4 get deleted

select *
from arkona.ext_eisglobal_sypffxmst a
left join arkona.ext_eisglobal_sypffxmst_tmp b on a.fxmcyy = b.fxmcyy
  and a.fxmpge = b.fxmpge
  and a.fxmlne = b.fxmlne
  and a.fxmcol = b.fxmcol
where b.fxmpge is null

delete 
-- select *
from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2017 
  AND fxmpge = 16 
  and fxmlne in (3.0, 6.0)
  and fxmcol in (2,4)

update arkona.ext_eisglobal_sypffxmst a
set fxmstr = x.fxmstr
from (
  select *
  from arkona.ext_eisglobal_sypffxmst_tmp a
  where fxmcyy = 2017
    and fxmpge = 16
    and fxmlne = 3)  x
where a.fxmcyy = x.fxmcyy
  and a.fxmpge = x.fxmpge
  and a.fxmlne = x.fxmlne
  and a.fxmcol = x.fxmcol
      
--2/2/17

select 'archive' as source, a.*
from arkona.ext_eisglobal_sypffxmst a
where fxmcyy = 2017
  and fxmpge = 3
  and fxmlne in (52)
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypffxmst_tmp a
where fxmcyy = 2017
  and fxmpge = 3
  and fxmlne in (52)
order by fxmlne, fxmcol   

delete 
-- select * 
from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2017
  and fxmpge = 3
  and fxmlne = 52.0
  and fxmcol = 2
-- sypfflout ------------------------------------------------------------------  

select *
from ((
    select *
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select *
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select *
    from arkona.ext_eisglobal_sypfflout f
    except
    select *
    from arkona.ext_eisglobal_sypfflout_tmp g)) x


select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and flpage = 16
  and flflne in (3,6)
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and flpage = 16
  and flflne in (3,6)
order by flflne

-- data not currently being used for 2017 page 16 lines 3 & 6 changed
update arkona.ext_eisglobal_sypfflout a
set fldata = x.fldata,
    flcont = x.flcont
from (
  select *
  from arkona.ext_eisglobal_sypfflout_tmp
  where flcyy = 2017
    and flpage = 16
    and flflne in (3, 6)) x
where a.flcyy = x.flcyy
  and a.flpage = x.flpage
  and a.flflne = x.flflne    

-- 2/2/17 
select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and flpage = 3
  and flflne in (52)
--   and flflsq = 0
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and flpage = 3
  and flflne in (52)
--   and flflsq = 0
order by flflne 
-- flcont change (i don't currently use this data) page 3, 
update arkona.ext_eisglobal_sypfflout a
set fldata = x.fldata,
    flcont = x.flcont
from (
  select *
  from arkona.ext_eisglobal_sypfflout_tmp
  where flcyy = 2017
    and flpage = 3
    and flflne = 52) x
where a.flcyy = x.flcyy
  and a.flpage = x.flpage
  and a.flflne = x.flflne   
  and a.flflsq = x.flflsq
   

-- 4/29/17

select *
from ((
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout f
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp g)) x
order by flpage    


select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and flpage in (7,8,9,10,11,12,13,14)
  and flflne = 52
  and flflsq = 0
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and flpage in (7,8,9,10,11,12,13,14)
  and flflne = 52
  and flflsq = 0
order by flpage, flflne, flflsq, source, flflsq

-- trivial unused information from far right in fldata has changed
-- go ahead and update it
update arkona.ext_eisglobal_sypfflout a
set fldata = x.fldata,
    flcont = x.flcont
from (
  select *
  from arkona.ext_eisglobal_sypfflout_tmp
  where flcyy = 2017
    and flpage in (7,8,9,10,11,12,13,14)
    and flflne = 52
    and flflsq = 0) x
where a.flcyy = x.flcyy
  and a.flpage = x.flpage
  and a.flflne = x.flflne   
  and a.flflsq = x.flflsq;

-- 5/11/17
select *
from ((
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout f
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp g)) x
order by flpage, flseq  


select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and (
    (flpage = 3 and flflne = 2 and flflsq = 1)
    or
    (flpage = 16 and flflne = 3 and flflsq = 0)
    or
    (flpage = 16 and flflne = 6 and flflsq = 0)
    or
    (flpage = 16 and flflne = 9 and flflsq = 0))
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and (
    (flpage = 3 and flflne = 2 and flflsq = 1)
    or
    (flpage = 16 and flflne = 3 and flflsq = 0)
    or
    (flpage = 16 and flflne = 6 and flflsq = 0)
    or
    (flpage = 16 and flflne = 9 and flflsq = 0))
order by flpage, flflne, flflsq, source, flflsq


update arkona.ext_eisglobal_sypfflout a
set fldata = x.fldata,
    flcont = x.flcont
from (
  select *
  from arkona.ext_eisglobal_sypfflout_tmp
  where flcyy = 2017
    and (
      (flpage = 3 and flflne = 2 and flflsq = 1)
      or
      (flpage = 16 and flflne = 3 and flflsq = 0)
      or
      (flpage = 16 and flflne = 6 and flflsq = 0)
      or
      (flpage = 16 and flflne = 9 and flflsq = 0))) x
where a.flcyy = x.flcyy
  and a.flpage = x.flpage
  and a.flflne = x.flflne   
  and a.flflsq = x.flflsq;


-- 2/2/18 sypfflout -------------------------------------
select *
from ((
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout f
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp g)) x
order by flpage, flseq  

-- it is hard to see what the differences are
-- it all about the | dividers in fldata, no real content changes, no changes in flcont
select a.flpage, a.flflne, a.fldata, b.fldata
from arkona.ext_eisglobal_sypfflout a
inner join arkona.ext_eisglobal_sypfflout_tmp b on a.flcode = b.flcode
  and a.flcyy = b.flcyy 
  and a.flpage = b.flpage
  and a.flseq = b.flseq
  and a.flflne = b.flflne
  and a.flflsq = b.flflsq
  and a.fldata <> b.fldata

-- pk
select flcode, flcyy, flpage, flseq, flflne, flflsq
from arkona.ext_eisglobal_sypfflout_tmp
group by flcode, flcyy, flpage, flseq, flflne, flflsq
having count(*) > 1  

update arkona.ext_eisglobal_sypfflout x
set fldata = z.fldata
from (
  select a.flcode, a.flcyy, a.flpage, a.flseq, a.flflne, a.flflsq, b.fldata
  from arkona.ext_eisglobal_sypfflout a
  inner join arkona.ext_eisglobal_sypfflout_tmp b on a.flcode = b.flcode
    and a.flcyy = b.flcyy 
    and a.flpage = b.flpage
    and a.flseq = b.flseq
    and a.flflne = b.flflne
    and a.flflsq = b.flflsq
    and a.fldata <> b.fldata) z
where x.flcode = z.flcode
    and x.flcyy = z.flcyy 
    and x.flpage = z.flpage
    and x.flseq = z.flseq
    and x.flflne = z.flflne
    and x.flflsq = z.flflsq
   
-- 2/2/18 sypffxmst -------------------------------------
-- one new row
select *
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst 
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp 
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x

insert into arkona.ext_eisglobal_sypffxmst
select *
from arkona.ext_eisglobal_sypffxmst_tmp 
except
select *
from arkona.ext_eisglobal_sypffxmst;


select 'base', *
from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2019
  and fxmpge = 17
  and fxmact in ( '<M220CB','<M220CD')
union all
select 'temp', *
from arkona.ext_eisglobal_sypffxmst_tmp
where fxmcyy = 2019
  and fxmpge = 17
  and fxmact in ( '<M220CB','<M220CD')

delete from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2019
  and fxmpge = 17
  and fxmact in ( '<M220CB','<M220CD')
  and fxmlne = 55.0


-- 1/3/2020  new year new rows
-- 1/8/2021  new year new rows
select fxmcyy, count(*) from arkona.ext_eisglobal_sypffxmst group by fxmcyy order by fxmcyy

insert into arkona.ext_eisglobal_sypffxmst
select *
from arkona.ext_eisglobal_sypffxmst_tmp a
where not exists (
  select 1
  from arkona.ext_eisglobal_sypffxmst 
  where fxmcyy = a.fxmcyy
    and fxmact = a.fxmact
    and a.fxmpge = a.fxmpge 
    and fxmlne = a.fxmlne)
  