﻿/*
run this script, then see below on how to use the results
*/

do
$$
declare 
   _year integer := 2021; --------------------------------------------------------------
   _year_month integer := 202101; ------------------------------------------------------
begin

delete from fin.fact_fs
where fs_key in (
  select fs_key
  from fin.dim_fs 
  where year_month = _year_month);
  
delete from fin.dim_fs where year_month = _year_month;

insert into fin.dim_fs (the_year,year_month,page,line,col,line_label, row_from_date, current_row)  
select _year, _year_month, flpage, flflne, coalesce(c.fxmcol, -1) as col,
  case -- multi valued labels with large internal block of spaces
    when line_label = 'ASSETS                 AMOUNT' 
      then 'ASSETS'
    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
      then 'EXPENSES'
    else line_label
  end as line_label, current_date, true
from ( -- year, page, line, line_label
  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
  from (
    select flcyy, flpage, flflne,       
    case
      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
      else fldata
    end as line_label
    from arkona.ext_eisglobal_sypfflout
    where flflsq = 0 
      and flflne > 0
      and flpage < 18) a
  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b
left join ( -- columns
  select fxmcyy, fxmpge, fxmlne, fxmcol
  from arkona.ext_eisglobal_sypffxmst
--   where fxmcyy > 2010
  group by fxmcyy, fxmpge, fxmlne, fxmcol) c  on b.flcyy = c.fxmcyy and b.flpage = c.fxmpge and b.flflne = c.fxmlne
where b.flcyy = _year; 

-- drop table if exists fin.xfm_fs_1 cascade;
-- create table if not exists fin.xfm_fs_1 (
--   the_year integer not null,
--   year_month integer not null,
--   page integer not null,
--   line numeric (4,1) not null,
--   col integer not null,
--   gm_account citext not null,
--   gl_account citext not null,
--   constraint xfm_fs_1_nk unique(year_month,gm_account,gl_account,page,line,col));
truncate fin.xfm_fs_1;  
insert into fin.xfm_fs_1  
-- *d*
select x._year,x._year_month, x.page, x.line, x.col, x.fxmact, x.gl_account
from (
  select _year, _year_month,
    case -- 147701/167701 100% body shop, no parts split
      when b.g_l_acct_number in ('147701', '167701') then 16
      else a.fxmpge
    end as page, 
    case 
      when b.g_l_acct_number in ('147701', '167701') then 49
      else a.fxmlne
    end as line, 
    case 
      when b.g_l_acct_number = '147701' then 2
      when b.g_l_acct_number = '167701' then 3
      else a.fxmcol
    end as col, a.fxmact, 
    coalesce(
      case 
        when b.g_l_acct_number like '%SPLT%' then replace(b.g_l_acct_number,'SPLT','')::citext
        else b.g_l_acct_number
      end, 'none') as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  -- *a*
  inner join (
    select factory_financial_year, 
    case
      when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
      when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
      else 'XXX'::citext
    end as store_code, g_l_acct_number, factory_account, fact_account_
    from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
  where a.fxmpge < 18
    and a.fxmcyy = _year) x
group by x._year,x._year_month, x.page, x.line, x.col, x.fxmact, x.gl_account;

-- parts split
-- drop table if exists parts_split;
truncate fin.parts_split;
-- create temp table parts_split as  
insert into fin.parts_split
select a.the_year, a.year_month, 4 as page, 59.0 as line, 
  case 
    when a.gm_account in ('477s','677s') then 11 -- body shop
    else 1
  end as col, 
  a.gm_account, replace(a.gl_account,'SPLT','')::citext as gl_account
-- select *  
from fin.xfm_fs_1 a
-- *e*
where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s'))
  or (a.gl_account in ('146700D','246700D','246701D'))) order by gl_Account;  

delete from fin.xfm_fs_1
where page = 4 
  and line = 59;


insert into fin.xfm_fs_1 (the_year,year_month,page,line,col,gm_account,gl_account) 
select *
from fin.parts_split;  

drop table if exists wtf;
create temp table wtf as 
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  round( -- this is the parts split
    sum(
      case 
        when b.page = 4 and b.line = 59 and c.gl_account not in ('147701','167701') 
          then round(coalesce(f.amount, 0) * .5, 0) 
        else coalesce(f.amount, 0)
      end), 0) as amount  
from fin.xfm_fs_1 a
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.gm_account = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
-- *c*
  and d.current_row = true
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case -- parts split
      when b.page = 4 and b.line = 59 then 
        case
          when c.gm_account in ('467S','667S', '468S','668S' ) then e.sub_department = 'mechanical'
          when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
          when c.gm_account in ('477s','677s') then e.department = 'body shop'
          when c.gl_account = '146700D' then e.sub_department = 'mechanical'
          when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
          when c.gl_account = '246701D' then e.sub_department = 'pdq'
        end 
      else   
        case 
          when d.department = 'Body Shop' then e.department = 'body shop'
          when d.department = 'Car Wash' then e.sub_Department = 'car wash'
          when d.department = 'Detail' then e.sub_department = 'detail'
          when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
          -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
          when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
          when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
          when d.department = 'Parts' then e.department = 'parts'
          when d.department = 'Quick Lane' then e.sub_department = 'pdq'
          when d.department = 'Service' then e.sub_department = 'mechanical'
          when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
-- *b*          
          when d.department = 'Auto Outlet' then e.department = 'sales' and e.sub_department = 'used'
-- *e*
          when d.department = 'National' then e.department = 'national'
          when d.department = 'General' then e.area = 'general'
        end
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
-- *f*
--     and aa.current_row = true
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = _year_month
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key) having sum(amount) <> 0; 

end 
$$;

select * from wtf
where fs_account_key is null;


--------------------------------------------------------------------------
--< 1/8/200
--------------------------------------------------------------------------

883364  18  -5225416

select * 
from fin.dim_fs
where fs_key in (883364)

select * 
from fin.xfm_fs_1 a
left join fin.dim_fs_account b on a.gm_account = b.gm_account and a.gl_Account = b.gl_account
where page = 1 and line = 35 and col = 6

select * from fin.dim_fs_account where gl_account in ('133600')

select * from arkona.ext_ffpxrefdta where g_l_acct_number in ('133600')

-- dim_fs_account does not match ext_ffpxrefdta
update fin.dim_fs_account
set gm_account = '336'
where gl_account = '133600'
  and gm_account = '340';

--------------------------------------------------------------------------
--< 9/5/19
--------------------------------------------------------------------------
867162;18;;-172254

select * 
from fin.dim_fs
where fs_key in (867162)

select * 
from fin.xfm_fs_1 a
left join fin.dim_fs_account b on a.gm_account = b.gm_account and a.gl_Account = b.gl_account
where page = 17 and line = 55 and col = 4

same as before, updated ext_ffpxrefdta, but forgot to update fin.dim_fs_Account
--------------------------------------------------------------------------
--/> 9/5/19
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--< 7/3/19
--------------------------------------------------------------------------
853793;18;;18176

select * 
from fin.dim_fs
where fs_key in (855807)

select * 
from fin.xfm_fs_1 a
left join fin.dim_fs_account b on a.gm_account = b.gm_account and a.gl_Account = b.gl_account
where page = 5 and line = 22 and col = 3

same as before, updated ext_ffpxrefdta, but forgot to update fin.dim_fs_Account
--------------------------------------------------------------------------
--/> 7/3/19
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--< 5/3/19
--------------------------------------------------------------------------

834505;18;;45472

select * 
from fin.dim_fs
where fs_key in (834505)

select * 
from fin.xfm_fs_1 a
left join fin.dim_fs_account b on a.gm_account = b.gm_account and a.gl_Account = b.gl_account
where page = 14 and line = 14 and col = 3

select * from fin.dim_fs_account where gl_account = '261300'

after doing the ffpxrefdta filx and then ffpxrefdta passes,
fs failed  because dim_fs_account still shows the old routing of 261300 -> 613C

update fin.dim_fs_account
set gm_account = '613J'
where gl_account = '261300'
  and gm_account = '613C';

--------------------------------------------------------------------------
--/> 5/3/19
--------------------------------------------------------------------------  
--------------------------------------------------------------------------
--< 4/4/19
--------------------------------------------------------------------------
825410;18;;-469
825487;18;;535
826663;18;;-188
826664;18;;15
826677;18;;2150
826678;18;;-4657

select * 
from fin.dim_fs
where fs_key in (825410,825487,826663,826664,826677,826678)

select * 
from fin.xfm_fs_1 a
left join fin.dim_fs_account b on a.gm_account = b.gm_account and a.gl_Account = b.gl_account
where page = 4 and line = 20 and col = 1

select * from fin.dim_fs_account where gl_account = '26124C'

select * from fin.dim_fs_account where gm_account = gl_account

yikes, i fucked up on 4/2 and assigned the gl_account to both gm & gl account 

select a.*, b.g_l_acct_number, b.factory_account
from fin.dim_fs_account a
left join arkona.ext_ffpxrefdta b on a.gm_account = b.g_l_acct_number
  and b.factory_financial_year = 2019
where a.gm_account = a.gl_account

update fin.dim_fs_account a
  set gm_account = (
    select factory_account
    from arkona.ext_ffpxrefdta
    where g_l_acct_number = a.gm_account
      and factory_financial_year = 2019)
-- select * from fin.dim_fs_account a
where a.gm_account = a.gl_account

--------------------------------------------------------------------------
--/> 4/4/19
--------------------------------------------------------------------------

/*
fs_key  = 798747, fs_org_key = 18, amount = 45

select * from fin.dim_Fs where fs_key = 806808

select * 
from fin.xfm_fs_1 a
left join fin.dim_fs_account b on a.gm_account = b.gm_account and a.gl_Account = b.gl_account
where page = 1 and line = 23 and col = 6

select * from fin.dim_fs_account where gl_Account = '232406'

*/

3/6/19 2 lines in wtf

fs_key    fs_org_key  fs_Account_key  amount
815544        18                        -280
817931        18                       41260

select * from fin.dim_Fs where fs_key = 815544

select * 
from fin.xfm_fs_1 a
left join fin.dim_fs_account b on a.gm_account = b.gm_account and a.gl_Account = b.gl_account
where page = 5 and line = 31 and col = 3

-- this is the shit jeri changed, rerouting 1625001 from 625A to 629A
-- so, i will temporarily change fin.dim_fs to get the statement to run, then change it back
select * from fin.dim_fs_account where gl_Account = '1625001'

update fin.dim_fs_account
set gm_account = '629A'
where fs_account_key = 1056;

statement run, change it back
update fin.dim_fs_account
set gm_account = '625A'
where fs_account_key = 1056;

select * from fin.dim_Fs where fs_key = 817931

select * 
from fin.xfm_fs_1 a
left join fin.dim_fs_account b on a.gm_account = b.gm_account and a.gl_Account = b.gl_account
where page = 9 and line = 28 and col = 3

select * from fin.dim_fs_account where gl_Account = '1626006'
this one is from last month
update fin.dim_fs_account
set gm_account = '626E'
where fs_account_key = 96407;
