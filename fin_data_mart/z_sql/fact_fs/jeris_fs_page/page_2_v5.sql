﻿drop table if exists P2_page_line_col_label;
create temp table P2_page_line_col_label as
select * from (
-- this is the dimf_fs part of monthly_update for page 2 only
-- manually setting the relevant year/year_month, 
-- the granularity of sypfflout and sypffxmst is year
select 2017 as the_year, 201701 as year_month, flpage, flflne, coalesce(c.fxmcol, -1) as col,
  case -- multi valued labels with large internal block of spaces
    when line_label = 'ASSETS                 AMOUNT' 
      then 'ASSETS'
    when line_label = 'EXPENSES                                                                                                 NEW SALES/PN'
      then 'EXPENSES'
    else line_label
  end as line_label, current_date, true
from ( -- year, page, line, line_label
  select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))::citext as line_label
  from (
    select flcyy, flpage, flflne,       
    case
      when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
      else fldata
    end as line_label
    from arkona.ext_eisglobal_sypfflout
--     where flcode = 'GM'
--       and flcyy > 2010 
    where flflsq = 0 
      and flflne > 0
      and flpage < 18) a
  group by flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1))) b
left join ( -- columns
  select fxmcyy, fxmpge, fxmlne, fxmcol
  from arkona.ext_eisglobal_sypffxmst
--   where fxmcyy > 2010
  group by fxmcyy, fxmpge, fxmlne, fxmcol) c  on b.flcyy = c.fxmcyy and b.flpage = c.fxmpge and b.flflne = c.fxmlne
where b.flcyy = 2017

) x where flpage = 2
order by flpage, flflne, col

select * from P2_page_line_col_label order by flflne, col

-- ok, from fact_fs_monthly_update, here is a combination of all the page 2 line/label/col with the relevant gm account
-- all (except line 61, 62, 64) of which are < accounts 
-- accounts 097,098,099 already configured in dim_fs_account: select * from fin.dim_fs_account where gm_account in ('097','098','099')
select a.the_year, a.year_month, a.flpage, a.flflne, a.col, a.line_label, b.fxmact
from P2_page_line_col_label a
left join  arkona.ext_eisglobal_sypffxmst b on a.flpage = b.fxmpge and a.flflne = b.fxmlne and a.col = b.fxmcol
  and fxmpge = 2 and fxmcyy = 2017 
where a.flflne not in (61,62,64)
order by a.flflne, col

-- here are the lines not yet covered in fin.dim_fs_account
select a.the_year, a.year_month, a.flpage, a.flflne, a.col, a.line_label, b.fxmact, c.gl_account
from P2_page_line_col_label a
left join arkona.ext_eisglobal_sypffxmst b on a.flpage = b.fxmpge and a.flflne = b.fxmlne and a.col = b.fxmcol
  and fxmpge = 2 and fxmcyy = 2017 
left join fin.dim_fs_account c on b.fxmact = c.gm_account 
where c.gm_Account is null 
order by a.flflne

-- which page 2 lines do not have a configuration for store (eg line 2)
-- so, thinking, the gm_account for P2L2C2 would be P2L2C2
-- select a.the_year, a.year_month, a.flpage, a.flflne, a.col, a.line_label, b.fxmact
select distinct a.flflne
from P2_page_line_col_label a
left join  arkona.ext_eisglobal_sypffxmst b on a.flpage = b.fxmpge and a.flflne = b.fxmlne and a.col = b.fxmcol
  and fxmpge = 2 and fxmcyy = 2017 
where a.flflne not in (61,62,64)
  and col <> -1 -- sub total lines
  and not exists (
    select 1 
    from P2_page_line_col_label
    where the_year = a.the_year
      and year_month = a.year_month
      and flpage = a.flpage
      and flflne = a.flflne
      and col in (1,2))
order by a.flflne

      
-- what about situations like P2L2C2 (store level) P2L25C7, P2L33C11, P2L34C11, etc which is not in spyffxmst
-- as well as sub total lines (7, 17, 40, 49 ...)
-- leaning toward implementing the existing


Dim_FS for store level where there is no system configuration, 201101 - 201702
Done, 4/10, 370 rows inserted
-- Line 4 (eg) there is no config for fixed (col 11), but there is no value, so don't config a column
insert into fin.dim_fs (the_year, year_month,page,line,col,line_label,row_from_date, current_row)
-- thru_date & row_reason: defaults
select the_year, year_month, page, line, 1 as col, line_label, current_date, true
from fin.dim_Fs
where page = 2
  and line in (2,4,5,6,57)
group by the_year, year_month, page, line, line_label order by year_month, page, line

dim_fs for subtotal lines
select * from fin.dim_fs where col = -1 and page = 2

Done, 4/10, 1702 rows inserted
-- Line 4 (eg) there is no config for fixed (col 11), but there is no value, so don't config a column
insert into fin.dim_fs (the_year, year_month,page,line,col,line_label,row_from_date, current_row)
-- thru_date & row_reason: defaults
select the_year, year_month, 2, flflne, col, line_label, current_date, true
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b  
  union
  select 2017, 201701
  union
  select 2017, 201702) x
cross join (
  select a.flpage, a.flflne, 1 as col, a.line_label
  from P2_page_line_col_label a
  where a.flflne in (7,17,40,49,55,56,58,60,63,65)) z 
union 
select the_year, year_month, 2, flflne, col, line_label, current_date, true
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b  
  union
  select 2017, 201701
  union
  select 2017, 201702) x
cross join (
  select a.flpage, a.flflne, 7 as col, a.line_label
  from P2_page_line_col_label a
  where a.flflne in (7,17,40,49,55,56,58)) z where year_month = 201701
union 
select the_year, year_month, 2, flflne, col, line_label, current_date, true
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b  
  union
  select 2017, 201701
  union
  select 2017, 201702) x
cross join (
  select a.flpage, a.flflne, 11 as col, a.line_label
  from P2_page_line_col_label a
  where a.flflne in (17,40,49,55,56,58)) z




select * from fin.dim_fs where page = 2 and line in (4,5,6) order by line, col, year_month





-- continue with backfilling ------------------------------------------------------------------------------------------
-- dim_fs_account -----------------------------------------------------------------------------------------------------
-- line 2 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
-- thru_date
select 'P2L2C1', gl_account, current_date, true, 'Page 2'
-- select store, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where b.year_month = 201701
  and (
    (b.page = 16 and b.line between 21 and 61) or -- fixed
    (b.page = 16 and b.line between 1 and 20) or --  uc
    (b.page between 5 and 15) or -- nc
    (b.page = 17 and b.line between 1 and 21)) --f&i  
group by gl_account;
-- line 4 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L4C1', gl_account, current_date, true, 'Page 2'
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
where b.year_month = 201701
  and page = 2
  and line = 4
group by gl_account;
-- line 5 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L5C1', gl_account, current_date, true, 'Page 2'
-- select store, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
where b.year_month = 201701
  and page = 2
  and line = 5
group by gl_account;
-- line 6 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L6C1', gl_account, current_date, true, 'Page 2'
-- select store, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
where b.year_month = 201701
  and page = 2
  and line = 6
group by gl_account;

-- line 7 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L7C1', gl_account, current_date, true, 'Page 2'
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
where year_month = 201701
  and page = 2
  and line between 4 and 6
  and col = 7
group by gl_account;

-- line 7 col 7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L7C7', gl_account, current_date, true, 'Page 2'
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
where year_month = 201701
  and page = 2
  and line between 4 and 6
  and col = 7
group by gl_account;

-- line 17 col 11
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L17C11', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 8 and 16
  and col = 11 
-- group by store, col
group by gl_account;

-- line 17 col 7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L17C7', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 8 and 16
  and col = 7
-- group by store, col
group by gl_account;

-- line 17 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L17C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account in ('P2L17C7', 'P2L17C11')
group by gl_account;

-- line 40 col 11
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L40C11', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 18 and 39
  and col = 11 
-- group by store, col
group by gl_account;

-- line 40 col 7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L40C7', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 18 and 39
  and col = 7
-- group by store, col
group by gl_account;

-- line 40 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L40C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account in ('P2L40C7', 'P2L40C11')
group by gl_account;

-- line 49 col 11
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L49C11', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 41 and 48
  and col = 11 
-- group by store, col
group by gl_account;

-- line 49 col 7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L49C7', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 41 and 48
  and col = 7
-- group by store, col
group by gl_account;

-- line 49 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L49C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account in ('P2L49C7', 'P2L49C11')
group by gl_account;

-- line 55 col 11
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L55C11', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 41 and 54
  and col = 11 
-- group by store, col
group by gl_account;

-- line 55 col 7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L55C7', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 41 and 54
  and col = 7
-- group by store, col
group by gl_account;

-- line 55 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L55C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account in ('P2L55C7', 'P2L55C11')
group by gl_account;

-- line 56 col 11
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L56C11', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 8 and 54
  and col = 11 
-- group by store, col
group by gl_account;

-- line 56 col 7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L56C7', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 8 and 54
  and col = 7
-- group by store, col
group by gl_account;

-- line 56 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L56C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account in ('P2L56C7', 'P2L56C11')
group by gl_account;

-- line 57 col 11
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select '<G&AF', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 4 and 54
  and col = 11 
-- group by store, col
group by gl_account;

-- line 57 col 7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select '<G&AV', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 4 and 54
  and col = 7
-- group by store, col
group by gl_account;

-- line 57 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L57C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account in ('<G&AF', '<G&AV')
group by gl_account;


-- line 58 col 11
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L58C11', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 2 and 54
  and col = 11 
-- group by store, col
group by gl_account;

-- line 58 col 7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L58C7', gl_account, current_date, true, 'Page 2'
-- select store, col, sum(amount) -- quick totals check
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key -- incl to do quick totals check
where year_month = 201701
  and page = 2
  and line between 2 and 54
  and col = 7
-- group by store, col
group by gl_account;

-- line 58 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L58C1', gl_account, current_date, true, 'Page 2'
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key 
where year_month = 201701
  and page = 2
  and line between 2 and 54
  and col = 1
group by gl_account;

-- line 59 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select '<TOIN', gl_account, current_date, true, 'Page 2'
from fin.dim_Fs_account
where gm_account in ('902', '903', '905','909','910','952','953','955')
  and gl_account <> 'none'
group by gl_account;

-- line 60 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L60C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account in ('P2L58C1', '<TOIN')
group by gl_account;

-- line 63 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L63C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account in ('097','098')
union
select 'P2L63C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account = 'P2L60C1'
group by gl_account;

-- line 65 col 1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L65C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account in ('099')
union
select 'P2L65C1', gl_account, current_date, true, 'Page 2'
from fin.dim_fs_account
where gm_account = 'P2L63C1'
group by gl_account;

-- continue with backfilling ------------------------------------------------------------------------------------------
-- dim_fs_account -----------------------------------------------------------------------------------------------------
-- dim_fs_account finished 4/11 ---------------------------------------------------------------------------------------


-- ok, this does it all the way thru 201702
-- exclude 201702, use that as the test bed for the page 2 monthly update of a single month
-- drop table if exists store_gross;
-- create temp table store_gross as
-- line 2 col 1
done 4/12 29603 rows inserted
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 2 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L2C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- where b.year_month = 201701
where amount <> 0
  and (
      (b.page = 16 and b.line between 21 and 61) or -- fixed
      (b.page = 16 and b.line between 1 and 20) or --  uc
      (b.page between 5 and 15) or -- nc
      (b.page = 17 and b.line between 1 and 21)); --f&i  

-- line 4 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 4 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L4C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account = '<ex011v';

-- line 5 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 5 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L5C1' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account = '<ex013v';
-- and year_month = 201701 group by store  

-- line 6 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 6 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L6C1' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account = '<ex015v';
-- and year_month = 201701 group by store

-- line 7 col 7
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 7 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L7C7' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account in ('<ex011v','<ex013v','<ex015v');
-- and year_month = 201701 group by store

-- line 7 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 7 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L7C1' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account in ('<ex011v','<ex013v','<ex015v');
-- and year_month = 201701 group by store

-- line 17 col 11
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 17 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L17C11' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 16
  and col = 11;
-- and year_month = 201701 group by store

-- line 17 col 7
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 17 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L17C7' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 16
  and col = 7;
-- and year_month = 201701 group by store

-- line 17 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 17 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L17C1' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 16
  and col = 1;
-- and year_month = 201701 group by store

-- line 40 col 11
/*
insert fails: row(s) with null fs_account_key
missing account 25710 - but that is a new car account, should not be relevant to col 11
fucking struggled with this for a whole day, this is the poorly thought out solution: 
      has something to do with, routing changing over time (in ffpxrefdta),
      in eisglobal.sypffxmst 057A -> page 3, -57F -> page 4, 
      2011 -> 2915 routed to 057F
      2016 -> 2017 routed to 057A
  this query exposes the "missing" account: 
    select *
    from (
      select a.*, b.*
      from fin.fact_fs a
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month < 201702
      inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
      inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
      where amount <> 0
        and page = 2
        and line between 18 and 39
        and col = 7 ) a
    inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
    left join fin.dim_fs_account c on b.gl_account = c.gl_account
      and c.gm_account = 'P2L40C7'
    where c.fs_account_key is null 
inserting this additional row solves the immediate probem:
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select 'P2L40C11', '25710', current_date, true, 'Page 2'
*/
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 40 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L40C11' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 18 and 39
  and col = 11;
-- and year_month = 201701 group by store

-- line 40 col 7
-- -- select * -- the culprit is 16024
-- -- from (
-- --   select a.*, b.*
-- --   from fin.fact_fs a
-- --   inner join fin.dim_fs b on a.fs_key = b.fs_key
-- --     and b.year_month < 201702
-- --   inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- --   inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- --   where amount <> 0
-- --     and page = 2
-- --     and line between 18 and 39
-- --     and col = 7 ) a
-- -- inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
-- -- left join fin.dim_fs_account c on b.gl_account = c.gl_account
-- --   and c.gm_account = 'P2L40C7'
-- -- where c.fs_account_key is null; 
-- -- insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
-- -- select 'P2L40C7', '16024', current_date, true, 'Page 2';
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 40 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L40C7' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 18 and 39
  and col = 7;
-- and year_month = 201701 group by store

-- line 40 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 40 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L40C1' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 18 and 39
  and col = 1;

-- line 49 col 11  
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 49 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L49C11' and gl_account = c.gl_account),
  a.amount
-- select store, sum(amount) 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 48  -- and year_month = 201701 and col = 1 group by store
  and col = 11;

-- line 49 col 7
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 49 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L49C7' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 48 
  and col = 7;
  
-- line 49 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 49 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L49C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 48
  and col = 1;

-- line 55 col 11 
-- well, this is the first dup key value problem: (303615, 7, 83736) already exists.
-- this generated shit loads of dups, distinct worked
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 55 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L55C11' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 54
  and col = 11;

-- line 55 col 17
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 55 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L55C7' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 54
  and col = 7;
 
-- line 55 col 1, 7, 11  
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 55 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L55C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 41 and 54
  and col = 1;

-- line 56 col 11
-- -- select * -- the culprit is 25710
-- -- from (
-- --   select a.*, b.*
-- --   from fin.fact_fs a
-- --   inner join fin.dim_fs b on a.fs_key = b.fs_key
-- --     and b.year_month < 201702
-- --   inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- --   inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- --   where amount <> 0
-- --     and page = 2
-- --     and line between 8 and 54
-- --     and col = 11 ) a
-- -- inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
-- -- left join fin.dim_fs_account c on b.gl_account = c.gl_account
-- --   and c.gm_account = 'P2L56C11'
-- -- where c.fs_account_key is null; 
-- -- insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
-- -- select 'P2L56C11', '25710', current_date, true, 'Page 2';
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 56 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L56C11' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 54
  and col = 11;
  
-- line 56 col 7
-- -- select * -- the culprit is 16024
-- -- from (
-- --   select a.*, b.*
-- --   from fin.fact_fs a
-- --   inner join fin.dim_fs b on a.fs_key = b.fs_key
-- --     and b.year_month < 201702
-- --   inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- --   inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- --   where amount <> 0
-- --     and page = 2
-- --     and line between 8 and 54
-- --     and col = 7 ) a
-- -- inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
-- -- left join fin.dim_fs_account c on b.gl_account = c.gl_account
-- --   and c.gm_account = 'P2L56C7'
-- -- where c.fs_account_key is null; 
-- -- insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
-- -- select 'P2L56C7', '16024', current_date, true, 'Page 2';
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 56 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L56C7' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 54
  and col = 7;
  
-- line 56 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 56 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L56C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 54
  and col = 1;

-- line 57 col 11 (<G&AF)
-- -- select * -- the culprit is 25710
-- -- from (
-- --   select a.*, b.*
-- --   from fin.fact_fs a
-- --   inner join fin.dim_fs b on a.fs_key = b.fs_key
-- --     and b.year_month < 201702
-- --   inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- --   inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- --   where amount <> 0
-- --     and page = 2
-- --     and line between 4 and 54
-- --     and col = 11 ) a
-- -- inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
-- -- left join fin.dim_fs_account c on b.gl_account = c.gl_account
-- --   and c.gm_account = '<G&AF'
-- -- where c.fs_account_key is null; 
-- -- insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
-- -- select '<G&AF', '25710', current_date, true, 'Page 2';
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 57 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = '<G&AF' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 4 and 54
  and col = 11;
  
-- line 57 col 7 (<G&AV)
-- -- select * -- the culprit is 16024
-- -- from (
-- --   select a.*, b.*
-- --   from fin.fact_fs a
-- --   inner join fin.dim_fs b on a.fs_key = b.fs_key
-- --     and b.year_month < 201702
-- --   inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- --   inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- --   where amount <> 0
-- --     and page = 2
-- --     and line between 4 and 54
-- --     and col = 7 ) a
-- -- inner join fin.dim_fs_account b on a.fs_account_key = b.fs_account_key
-- -- left join fin.dim_fs_account c on b.gl_account = c.gl_account
-- --   and c.gm_account = '<G&AV'
-- -- where c.fs_account_key is null; 
-- -- insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
-- -- select '<G&AV', '16024', current_date, true, 'Page 2';
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 57 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = '<G&AV' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 4 and 54
  and col = 7;
  
-- line 57 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 57 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L57C1' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 4 and 54
  and col = 1;

-- line 58 col 11
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 58 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('<TFEG', '<G&AF') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and gm_account in ('<TFEG', '<G&AF');
--   and page = 2
--   and line between 2 and 54
--   and col = 11;
  
-- line 58 col 7
-- -- insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
-- -- select 'P2L58C7', '16024', current_date, true, 'Page 2';
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 58 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('<TVEG', '<G&AV') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 2 and 54
  and col = 7;
  
-- line 58 col 1
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 58 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('P2L57C1', 'P2L2C1') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 2 and 54
  and col = 1;

-- line 59 col 1 (<TOIN)  
-- generating (or revealing ?)2 rows, one with correct org one with none
/*
don't know how or when they got there, but this gets rid of them
delete
from fin.fact_fs 
where fs_org_key = 18
  and fs_key in (
    select fs_key
    from fin.dim_fs
    where page = 3 
      and line > 62)  
now, re-insert for <toin and we are fine
*/
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 59 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = '<TOIN' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 3
  and line between 63 and 68;

-- line 60 col 1  
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 60 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('P2L57C1', 'P2L2C1', '<TOIN') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 2 and 59
  and col = 1;;

-- line 65 col 1  
-- skipping bonuses, income taxes, L65 = L60 (for now at least)
insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 65 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('P2L57C1', 'P2L2C1', '<TOIN') and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month < 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 2 and 59
  and col = 1;
  
-- continue with backfilling --------------------------------------------------------------------------------------------
-- fact_fs / amounts ----------------------------------------------------------------------------------------------------

this is the gold list for page 2
select a.the_year, a.year_month, a.flpage, a.flflne, a.col, a.line_label, b.fxmact
from P2_page_line_col_label a
left join  arkona.ext_eisglobal_sypffxmst b on a.flpage = b.fxmpge and a.flflne = b.fxmlne and a.col = b.fxmcol
  and fxmpge = 2 and fxmcyy = 2017 
where a.flflne not in (61,62,64)
order by a.flflne, col

line 5 col 1
line 6 col 1
line 7 col 1, 7
line 17 col 1, 7, 11
line 40 col 1, 7, 11
line 49 col 1, 7, 11
line 55 col 1, 7, 11
line 56 col 1, 7, 11
line 57 col 1, 7 (<G&AV), 11 (<G&AF)
line 58 col 1, 7, 11
line 59 col 1 (<TOIN)
line 60 col 1
line 63 col 1
line 65 col 1




select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where gm_account = '<EX011V'
order by year_month






insert into fin.dim_fs_org (market,store,area,department,sub_department) values 
('grand forks', 'ry1', 'general','none','none'),
('grand forks', 'ry2', 'general','none','none')


-- -- -- every month has a none store, god damnit, starting in 201505 and older there is an amount, fuck
-- -- -- choosing to say fuck it for now
-- -- -- select year_month, store, sum(amount)
-- -- -- from store_gross a
-- -- -- inner join fin.dim_fs b on a.fs_key = b.fs_key
-- -- -- inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- -- -- inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- -- -- group by year_month, store
-- -- -- order by year_month, store
-- -- 
-- -- select distinct year_month, page, line, gl_account, amount, e.*
-- -- from store_gross a
-- -- inner join fin.dim_fs b on a.fs_key = b.fs_key
-- -- inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- -- inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- -- left join fin.dim_account e on c.gl_account = e.account
-- -- where d.store = 'none'
-- --   and amount <> 0
-- -- order by year_month



-- -- -- 
-- -- -- uh oh starting to mind fuck, have to redo all of page 2 for 201702, what does that mean
-- -- -- can i simply rely on values and associations from previous month ???
-- -- -- yes, all of page 2 every  month
-- -- -- no, current month for dim_Fs, fim_Fs_org
-- -- -- oh wait, first i am doing the backfilling of the missing stuff for previous months


Line 7: Total Variable     
  gm_account: P2L7C7
  gl_accounts: 
select distinct gl_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where year_month = 201702
  and page = 3
  and line between 4 and 6
     


select a.the_year, a.year_month, a.flpage, a.flflne, a.col, a.line_label, b.fxmact
from P2_page_line_col_label a
left join  arkona.ext_eisglobal_sypffxmst b on a.flpage = b.fxmpge and a.flflne = b.fxmlne and a.col = b.fxmcol
  and fxmpge = 3 and fxmcyy = 2017 
order by a.flflne, col

    

-- -- diversion 201702 statement, control level
-- return this whold fucking thing in initial call, let them "query" it in client side javascript ???
-- -- 59804 rows, 28 sec 
-- select b.year_month, market, d.store, area, d.department, sub_department, page, line, line_label,
--   col, c.gl_account, 
--   f.control, sum(f.amount) as gl_amount
-- from fin.fact_fs a
-- inner join fin.dim_fs b on a.fs_key = b.fs_key
-- inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- inner join fin.dim_account e on c.gl_account = e.account
-- inner join fin.fact_gl f on e.account_key = f.account_key
-- inner join dds.dim_date g on f.date_key = g.date_key
--   and g.year_month = b.year_month
-- where b.year_month = 201702
--   and d.store = 'ry1'
-- group by b.year_month, market, d.store, area, d.department, sub_department, page, line, 
--   line_label, col, c.gl_account, f.control
-- having sum(a.amount) <> 0



