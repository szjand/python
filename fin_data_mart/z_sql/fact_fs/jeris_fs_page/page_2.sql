﻿-- fact_fs does not include header lines or sub total lines, so dim_fs has to be base table

-- page 2 is almost all formula accounts
-- those accounts are not currently associated with an fs_org row
-- or gl accounts

drop table if exists p2;
create temp table p2 as
select a.page, a.line, a.col, a.line_label, c.gm_account, c.gl_account, b.amount
from fin.dim_fs a
left join fin.fact_fs b on a.fs_key = b.fs_key
left join fin.dim_fs_account c on b.fs_account_key = c.fs_account_key
where a.page = 2
  and a.year_month = 201702
order by a.line, a.col;  

select * from p2

-- initially i am thinking this exercise is about attributing gl_accounts to formula accounts
-- ok, for these expense lines, page 2 amounts = page 3 + page 4 amounts
select c.line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
  and b.store = 'ry1'
inner join fin.dim_fs c on a.fs_key = c.fs_key
  and c.year_month = 201702
  and c.page in (3,4)
  and c.line between 1 and 55
group by c.line  

-- here are line, col, gm_account, gl_account from page 3 and 4
drop table if exists p3_4;
create temp table p3_4 as
select c.page, c.line, c.col, d.gm_account, d.gl_account
from fin.fact_fs a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
  and b.store = 'ry1'
inner join fin.dim_fs c on a.fs_key = c.fs_key
  and c.year_month = 201702
  and c.page in (3,4)
  and c.line between 1 and 55
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
order by c.page, c.line, c.col;  

select * from p3_4

-- cool, a strong subset of expense formula accounts and the constituent gm/gl accounts
-- careful now
-- page 2 has multiple columns: 1:store - 7:variable - 11:fixed
select *
from p2 a
left join p3_4 b on a.line = b.line
where b.page is not null
  and 
    case 
      when a.line < 8 then a.col = 7
      else a.col = 1
    end
order by a.line, b.gm_account, b.gl_account

-- accounts for all lines except 1, 2, 61, 62, 64
-- 61, 62, 64 are regular accounts and are already populated
-- just need 1 and 2
select *
from p2 a
left join p3_4 b on a.line = b.line
where 
  case 
    when a.line < 8 then a.col = 7
    else a.col = 1
  end
order by a.line, b.gm_account, b.gl_account

-- other goofy accounts (&ARVA30O, *UA, etc)
-- fleet, memos ...
-- not worried about these for now
select *
from fin.dim_fs_account a
left join fin.fact_fs b on a.fs_account_key = b.fs_account_key
left join fin.dim_fs c on b.fs_key = c.fs_key
  and c.year_month = 201702
where left(a.gm_account, 1) in ('&','*')
  and c.fs_key is not null
order by page,line 


-- lets get lines 1 & 2
-- variable
select b.year_month, 
  sum(case when col = 1 then -amount end) as sales,
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
  and ( -- variable page 3 lines 1 & 2
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20)) -- f&i
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
group by b.year_month
having sum(amount) <> 0
order by b.year_month;


-- fixed
-- exclude parts split
select b.year_month, 
  sum(case when col in (1,2) then -amount end) as sales, -- sublet and shop supplies in col 2
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
  and (
    (b.page = 16 and b.line between 20 and 60))
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.area = 'fixed'
  and c.store = 'ry1'
group by b.year_month
having sum(amount) <> 0

-- and combine them both = total dealership
-- bingo, the dough is right on
select b.year_month, 
  sum(case when col in (1,2) then -amount end) as sales_line_1,
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross_line_2
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
  and ( -- variable page 3 lines 1 & 2
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20) -- f&i
    or
    (b.page = 16 and b.line between 20 and 60))
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
group by b.year_month
having sum(amount) <> 0
order by b.year_month;

-- ok, let's get all the accounts
-- select b.year_month, 
--   sum(case when col in (1,2) then -amount end) as sales_line_1,
--   sum(case when col = 3 then -amount end) as cogs,
--   sum(-amount) as gross_line_2
drop table if exists page_2_accounts;
create temp table page_2_accounts as
select d.gl_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
  and ( -- variable page 3 lines 1 & 2
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20) -- f&i
    or
    (b.page = 16 and b.line between 20 and 60)) --fixed
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
group by d.gl_account;

select * from page_2_accounts

-- pow it adds up
select sum(a.amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201702
inner join fin.dim_account c on a.account_key = c.account_key  
inner join page_2_accounts d on c.account = d.gl_account
where a.post_status = 'Y'


-- expenses
select s.line, s.line_label, sum(t.total) as amount
from (
  select line, line_label
  from p2
  group by line, line_label) s
left join (
  select b.line, sum(amount) as total
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201702
    and b.page between 3 and 4
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.store = 'ry1'
  where b.line between 4 and 55  
  group by b.line) t on s.line = t.line
where s.line > 2  
group by s.line, s.line_label
order by s.line


-- sales & gross
select 1.0 as line,
  sum(case when col in (1,2) then -amount end) as sales_line_1
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
  and ( -- variable page 3 lines 1 & 2
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20) -- f&i
    or
    (b.page = 16 and b.line between 20 and 60))
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
union
select 2.0 as line,
  sum(-amount) as gross_line_2
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
  and ( -- variable page 3 lines 1 & 2
    (b.page between 5 and 15) -- new cars
    or
    (b.page = 16 and b.line between 1 and 14) -- used cars
    or
    (b.page = 17 and b.line between 1 and 20) -- f&i
    or
    (b.page = 16 and b.line between 20 and 60))
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'

-- and here it all is
drop table if exists page_2;
create table page_2 as
select s.line, s.line_label, sum(t.amount) as amount
from (
  select line, line_label
  from p2
  group by line, line_label) s
left join (-- sales / gross
  select 1.0 as line,
    sum(case when col in (1,2) then -amount end) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201702
    and ( -- variable page 3 lines 1 & 2
      (b.page between 5 and 15) -- new cars
      or
      (b.page = 16 and b.line between 1 and 14) -- used cars
      or
      (b.page = 17 and b.line between 1 and 20) -- f&i
      or
      (b.page = 16 and b.line between 20 and 60))
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.store = 'ry1'
  union
  select 2.0 as line,
    sum(-amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201702
    and ( -- variable page 3 lines 1 & 2
      (b.page between 5 and 15) -- new cars
      or
      (b.page = 16 and b.line between 1 and 14) -- used cars
      or
      (b.page = 17 and b.line between 1 and 20) -- f&i
      or
      (b.page = 16 and b.line between 20 and 60))
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.store = 'ry1') t on s.line = t.line  
where s.line < 3  
group by s.line, s.line_label
union -- expenses
select s.line, s.line_label, sum(t.total) as amount
from (
  select line, line_label
  from p2
  group by line, line_label) s
left join (
  select b.line, sum(amount) as total
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201702
    and b.page between 3 and 4
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
    and c.store = 'ry1'
  where b.line between 4 and 55  
  group by b.line) t on s.line = t.line
where s.line > 2  
group by s.line, s.line_label
order by line;

select * from page_2

-- 3/10 time to turn it into json 
select 
  case 
    when line between 1 and 2 then 'Sales'
    when line between 4 and 7 then 'Variable'
  end as block, 
  a.*
 from page_2 a

drop table if exists page_2_full;
create temp table page_2_full as
select 'March'::citext as month, '2017'::citext as year,
  case
    when line between 1 and 2 then 'Sales'::citext
    when line between 4 and 7 then 'Variable'::citext
    when line between 8 and 17 then 'Personnel'::citext
    when line between 18 and 40 then 'Semi-Fixed'::citext
    when line between 41 and 55 then 'Fixed'::citext
    when line between 56 and 65 then 'Total'::citext
  end as block,
  a.*, 
  round(100 * (amount / (select amount from page_2 where line = 2)), 2) as percent
from (
  select line, line_label,
    case
      when line = 7 then (select sum(amount) from page_2 where line between 4 and 7)
      when line = 17 then (select sum(amount) from page_2 where line between 8 and 16)
      when line = 40 then (select sum(amount) from page_2 where line between 18 and 39)
      when line = 49 then (select sum(amount) from page_2 where line between 41 and 48)
      when line = 55 then (select sum(amount) from page_2 where line between 41 and 54)
      when line = 56 then (select sum(amount) from page_2 where line between 8 and 54)
      when line = 57 then (select sum(amount) from page_2 where line between 4 and 54)
      when line = 58 then (select sum(-amount) from page_2 where line between 4 and 54) + (select amount from page_2 where line = 2)
      else amount
    end as amount 
  from page_2
  where line <> 3) a

select * from page_2_full

select a.*, round(1.0 * amount/1541120::numeric(12,2), 4)
from fin.page_2_v1 a
order by line

update fin.page_2_v1
set percentage = round(1.0 * amount/1541120::numeric(12,2), 4)
where line > 2

select * from fin.page_2_v1  order by line

alter table fin.page_2_v1
alter column percentage type numeric(6,4)



