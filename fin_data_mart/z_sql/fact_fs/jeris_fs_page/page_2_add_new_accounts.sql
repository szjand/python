﻿-- this is the porton of page_2_v6.sql that exposes the missing accounts for lines 4 - 54
select c.fs_key, b.fs_org_key, d.fs_account_key, b.amount
-- select *
from (
  select *  -- base_2
  from arkona.ext_eisglobal_sypffxmst 
  where fxmpge = 2
    and fxmlne between 4 and 54   
    and fxmcyy = 2017) a
left join (
  -- expense line of page 3 & 4 (line 4 - 54)
  -- by year_month/store/page/line/col/gl_account::amount
  -- base_1
  select store, page, line, col, gl_account, d.fs_org_key, sum(amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page in (3,4)
    and b.line between 4 and 54
    and d.store <> 'none'
    and b.year_month = 201708
  group by store, page, line, col, gl_account, d.fs_org_key) b on a.fxmlne = b.line
    and 
      case 
        when a.fxmlne < 8 then 1 = 1
        when a.fxmcol = 1 then 1 = 1
        when a.fxmcol = 7 then b.page = 3
        when a.fxmcol = 11 then b.page = 4
      end
left join fin.dim_fs c on a.fxmpge = c.page
  and a.fxmlne = c.line
  and a.fxmcol = c.col
  and c.year_month = 201708
left join fin.dim_fs_account d on a.fxmact = d.gm_account
  and b.gl_account = d.gl_account
where fs_Account_key is null; 

-- generate an insert from this query

insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select fxmact, b.gl_account, '07/01/2017',true, 'Page 2 new accounts'
from (
  select *  -- base_2
  from arkona.ext_eisglobal_sypffxmst 
  where fxmpge = 2
    and fxmlne between 4 and 54   
    and fxmcyy = 2017) a
left join (
  -- expense line of page 3 & 4 (line 4 - 54)
  -- by year_month/store/page/line/col/gl_account::amount
  -- base_1
  select store, page, line, col, gl_account, d.fs_org_key, sum(amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page in (3,4)
    and b.line between 4 and 54
    and d.store <> 'none'
    and b.year_month = 201708
  group by store, page, line, col, gl_account, d.fs_org_key) b on a.fxmlne = b.line
    and 
      case 
        when a.fxmlne < 8 then 1 = 1
        when a.fxmcol = 1 then 1 = 1
        when a.fxmcol = 7 then b.page = 3
        when a.fxmcol = 11 then b.page = 4
      end
left join fin.dim_fs c on a.fxmpge = c.page
  and a.fxmlne = c.line
  and a.fxmcol = c.col
  and c.year_month = 201708
left join fin.dim_fs_account d on a.fxmact = d.gm_account
  and b.gl_account = d.gl_account
where fs_Account_key is null;   


--- P2L2C1
1. needed to run first part of page_2_v6 query stand alone to populate fin.dim_fs with P2 rows for current month
2. start with the P2L2C1 query
then: 

insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L2C1', gl_account, '07/01/2017', true, 'P2L2C1 new account'
  from (-- modify it to generate the relevant gl_accounts  -- add  gl_account from dim_fs_account in output
  select 
     (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 2 and col = 1),
    a.fs_org_key, 
    (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L2C1' and gl_account = c.gl_account) as fs_account_key,
    a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201708
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where amount <> 0
    and (
        (b.page = 16 and b.line between 21 and 61) or -- fixed
        (b.page = 16 and b.line between 1 and 20) or --  uc
        (b.page between 5 and 15) or -- nc
        (b.page = 17 and b.line between 1 and 21))) x where fs_account_key is null;

    
-- P2L40C7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L40C7', gl_account, '07/01/2017', true, 'P2L40C7 new account'
from (
  select 
    (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 40 and col = 7),
    a.fs_org_key, 
    (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L40C7' and gl_account = c.gl_account) as fs_account_key,
    a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month  = 201708
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where amount <> 0
    and page = 2
    and line between 18 and 39
    and col = 7) x where fs_account_key is null 

-- P2L40C1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L40C1', gl_account, '07/01/2017', true, 'P2L40C1 new account'
from (
  select 
    (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 40 and col = 1),
    a.fs_org_key, 
    (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L40C1' and gl_account = c.gl_account),
    a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201707
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where amount <> 0
    and page = 2
    and line between 18 and 39
    and col = 1) x where fs_account_key is null 

-- P2L49C7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L49C7', gl_account, '07/01/2017', true, 'P2L49C7 new account'
from (
  select 
    (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 49 and col = 7),
    a.fs_org_key, 
    (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L49C7' and gl_account = c.gl_account),
    a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201707
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where amount <> 0
    and page = 2
    and line between 41 and 48 
    and col = 7) x where fs_account_key is null

-- P2L49C1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L49C1', gl_account, '07/01/2017', true, 'P2L49C1 new account'
from (
  select 
    (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 49 and col = 1),
    a.fs_org_key, 
    (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L49C1' and gl_account = c.gl_account),
    a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201707
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where amount <> 0
    and page = 2
    and line between 41 and 48
    and col = 1) x where fs_account_key is null

-- P2L55C7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L55C7', gl_account, '07/01/2017', true, 'P2L55C7 new account'
from (
  select distinct 
    (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 55 and col = 7),
    a.fs_org_key, 
    (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L55C7' and gl_account = c.gl_account),
    a.amount, c.gl_Account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201707
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where amount <> 0
    and page = 2
    and line between 41 and 54
    and col = 7) x where fs_account_key is null


-- P2L55C1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L55C1', gl_account, '07/01/2017', true, 'P2L55C1 new account'
from (
  select distinct 
    (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 55 and col = 1),
    a.fs_org_key, 
    (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L55C1' and gl_account = c.gl_account),
    a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201707
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where amount <> 0
    and page = 2
    and line between 41 and 54
    and col = 1) x where fs_account_key is null

-- P2L56C7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L56C7', gl_account, '07/01/2017', true, 'P2L56C7 new account'
from (
  select distinct 
    (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 56 and col = 7),
    a.fs_org_key, 
    (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L56C7' and gl_account = c.gl_account),
    a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201707
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where amount <> 0
    and page = 2
    and line between 8 and 54
    and col = 7) x where fs_account_key is null


-- P2L56C1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L56C1', gl_account, '07/01/2017', true, 'P2L56C1 new account'
from (
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 56 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L56C1' and gl_account = c.gl_account),
  a.amount, c.gl_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201707
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 8 and 54
  and col = 1) x where fs_account_key is null
  

-- P2L57C7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select '<G&AV', gl_account, '07/01/2017', true, 'P2L57C7 new account'
from (
  select distinct 
    (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 57 and col = 7),
    a.fs_org_key, 
    (select fs_account_key from fin.dim_fs_account where gm_account = '<G&AV' and gl_account = c.gl_account),
    a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201707
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where amount <> 0
    and page = 2
    and line between 4 and 54
    and col = 7) x where fs_account_key is null

-- select * from fin.dim_fs_account where gm_account = 'P2L57C7'
-- select * from fin.dim_fs_Account where gl_account = '17402w'
-- 17402W
-- 18802W
-- 18202W
-- 
update fin.dim_fs_account
set gm_account = '<G&AV'
where gl_account = '18202W'
  and gm_account = 'P2L57C7'
  
-- P2L57C1
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L57C1', gl_account, '07/01/2017', true, 'P2L57C1 new account'
from (
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 57 and col = 1),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L57C1' and gl_account = c.gl_account),
  a.amount, c.gl_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201707
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 4 and 54
  and col = 1) x where fs_account_key is null
  
-- P2L58C7
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date, current_row,row_reason)
select 'P2L58C7', gl_account, '07/01/2017', true, 'P2L58C7 new account'
from (
select distinct 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 58 and col = 7),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account in ('<TVEG', '<G&AV') and gl_account = c.gl_account),
  a.amount, c.gl_account
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201707
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 2 and 54
  and col = 7) x where fs_account_key is null




this was a fucking hassle
the following query had to be run after each fix
as well as the monthly update and page_2_v6 up to the point of failure

need a better way to do page 2

     
delete 
from fin.fact_fs
where fs_key in (
  select fs_key
  from fin.dim_fs
  where year_month = 201708
    and page = 2);

delete from fin.dim_fs where year_month = 201708 and page = 2  

select 
  (select fs_key from fin.dim_fs where year_month = b.year_month and page = 2 and line = 40 and col = 11),
  a.fs_org_key, 
  (select fs_account_key from fin.dim_fs_account where gm_account = 'P2L40C11' and gl_account = c.gl_account),
  a.amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201708
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where amount <> 0
  and page = 2
  and line between 18 and 39
  and col = 11;
