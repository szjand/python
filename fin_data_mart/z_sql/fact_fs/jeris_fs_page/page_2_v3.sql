﻿-- well, not sure yet exactly what i did, but it appears that i managed thouroughly fuck up 
-- all the page 2 data in v2

-- looks like a managed to zap all fact_fs rows related to page 2
-- except for lines 61, 62, 64
select a.page, a.line, a.col, a.line_label, b.fs_key, c.gm_account, c.gl_account, b.amount
from fin.dim_fs a
left join fin.fact_fs b on a.fs_key = b.fs_key
left join fin.dim_fs_account c on b.fs_account_key = c.fs_account_key
where a.page = 2
  and a.year_month = 201702
  and b.fs_key is null 
order by line, col

-- 
-- drop table if exists fin.dim_fs_account cascade;
-- create table if not exists fin.dim_fs_account (
--   fs_account_key serial primary key,
--   gm_account citext not null,
--   gl_account citext not null,
--   row_from_date date not null,
--   row_thru_date date not null default '9999-12-31'::date,
--   current_row boolean not null,
--   row_reason citext not null default 'New Row'::citext,
-- constraint dim_fs_account_nk unique (gm_account, gl_account));
-- comment on table fin.dim_fs_account is
-- 'where gl_account = none:
--   if the first char is <, & or * it is a "formula" account
--   if not it is a gm account not routed in ffpxrefdta';
-- comment on column fin.dim_fs_account.gm_account is 'source: arkona.ext_eis_global_sypffxmst.fxmact';
-- comment on column fin.dim_fs_account.gl_account is 'source: arkona.ext_ffpxrefdta.g_l_acct_number';  
-- create index on fin.dim_fs_account(gm_account);
-- create index on fin.dim_fs_account(gl_account);
-- -- ah by not joining on year, that's how i left out the gm_account/none for
-- -- gm accounts that were routed in some years but not in others
-- insert into fin.dim_fs_account (gm_account, gl_account,row_from_date, current_row)
select a.fxmact, 
  coalesce(
    case 
      when b.g_l_acct_number like '%SPLT%' then replace(b.g_l_acct_number,'SPLT','')::citext
      else b.g_l_acct_number
    end, 'none') as gl_account,
    '01/01/2016', true
from arkona.ext_eisglobal_sypffxmst a
left join (
  select g_l_acct_number, factory_account, factory_financial_year
  from arkona.ext_ffpxrefdta 
  group by g_l_acct_number, factory_account, factory_financial_year) b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
where left(a.fxmact, 1) in ('<','&','*')
group by a.fxmact, b.g_l_acct_number;

select * from arkona.ext_eisglobal_sypffxmst limit 100 -- year page line col fxmact (gm account)

-- dim_fs_account is cool, as far as complete coverage of gm accounts
-- checked for all sypffxmst years
-- all lines except 61 - 64, gm_account starts with <
select * 
from arkona.ext_eisglobal_sypffxmst a
left join (
  select distinct gm_account
  from fin.dim_fs_account) b on a.fxmact = b.gm_account
where a.fxmcyy = 2017 
  and a.fxmpge = 2
--   and left(a.fxmact, 1) not like '<%'
--   and b.gm_account is null 
order by a.fxmlne, a.fxmcol

-- lines not configured in sypffxmst: 3, 7, 
with the_series as (
  select * 
  from generate_series(1, 64) as series)
select a.*, c.line_label
from the_series a  
left join (
  select distinct fxmlne
  from arkona.ext_eisglobal_sypffxmst a
  where a.fxmcyy = 2017 
    and a.fxmpge = 2) b on a.series = b.fxmlne  
left join fin.dim_fs c on a.series = c.line
  and c.year_month = 201702
  and c.page = 2
where b.fxmlne is null    

select * from  fin.dim_fs where year_month = 201702 and line = 2
/*
ok, got to fix this, but it is also a good opportunity to, at least for page 2, 
to configure sub/total lines
*/
  
-- more than one gm account per line
-- if i add col the the base grouping, there are still a few (eg P16L1 a column of gross: sales + cost))
select page, line, col from (
select page, line, col, gm_account from (
select a.*, b.page, b.line, b.col, c.gm_account, c.gl_account, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key 
inner join fin.dim_fs_org d on a.fs_org_key =  d.fs_org_key
  and d.store = 'ry1' 
inner join fin.dim_account e on c.gl_account = e.account --order by page, line
  and e.current_row = true  
) x group by page, line, col, gm_account 
) y group by page, line, col having count(*) > 1

--------------------------------------------------------------------------------------------------------------------
ok, feeling ok, lets get started
lets start line by line for 201702 only
when thats working generalize for all months


select * 
from arkona.ext_eisglobal_sypffxmst a
where a.fxmcyy = 2017 
  and a.fxmpge = 2
order by a.fxmlne, a.fxmcol

line 1
  col 2: <TNU :: store
  col 7: <TVS :: variable
  col 11: <TFS :: fixed




lets start with fixed 
total fixed P6L62 
in sypffxmst it is coded as <TFOH
so <TFS = <TFOH



variable: only gives me total used, nothing from lines 15, 16
-- fs 10,328,968
select sum(amount) -- 12,876,221
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201702
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account in ( -- just the sales accounts for variable page 6
    select distinct d.gl_account
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201702
      and b.page = 16
      and b.line between 1 and 16
       and b.col in (1,2)
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'ry1')
where a.post_status = 'y'  




-----------------------------------------------------------------------------------------------------------------------------------
-- P2L1C11 Fixed SALES: <TFS
delete from fin.dim_fs_account where gm_account = '<TFS';
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select distinct '<TFS', d.gl_account, current_date, true,'Page 2'
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.page = 16
  and b.line between 21 and 59
   and b.col in (1,2)
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
where f.area = 'fixed' ;
  
-- insert into fin.fact_fs
drop table if exists p2_tfs;
create temp table p2_tfs as
-- drop table if exists p2_tfoh;
-- create temp table p2_tfoh as
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  sum(coalesce(f.amount, 0)) as amount  
-- select *  
from (-- need to fashion a sudo xfm_fs_1
  select 2017 as the_year, 201702 as year_month, a.fxmpge as page, a.fxmlne as line, a.fxmcol as col, a.fxmact, 
    coalesce(b.gl_account) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  left join fin.dim_fs_account b on a.fxmact = b.gm_account
  where a.fxmpge = 2
--   where a.fxmpge = 16
    and a.fxmcyy = 2017
    and a.fxmact = '<TFS') a
--     and a.fxmact = '<TFOH') a    
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.fxmact = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case 
      when d.department = 'Body Shop' then e.department = 'body shop'
      when d.department = 'Car Wash' then e.sub_Department = 'car wash'
      when d.department = 'Detail' then e.sub_department = 'detail'
      when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
      -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
      when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
      when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
      when d.department = 'Parts' then e.department = 'parts'
      when d.department = 'Quick Lane' then e.sub_department = 'pdq'
      when d.department = 'Service' then e.sub_department = 'mechanical'
      when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201702
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
where d.department_code not in ('ad','gn','lr')
  and not (d.store_code = 'RY2' and d.department_code in ('bs','re'))  
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key); 
  
-- ok, no relevant rows in fact_fs
select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  and c.gm_account = '<TFS'

insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select fs_key,fs_org_key,fs_account_key,amount
from p2_tfs;

select d.store, d.area, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  and c.gm_account = '<TFS'
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
group by d.store, d.area  
    
select d.store, d.area, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where b.year_month = 201701
  and b.page = 2
  and b.line = 1
  and b.col = 11
  and d.area = 'fixed'
group by d.store, d.area

--------------------------------------------------------------------------------------------------------------------------
-- P2L1C7 Variable SALES: <TVS ****************************
delete from fin.fact_Fs
-- select * from fin.fact_fs
where fs_account_key in (
  select fs_account_key
  from fin.dim_fs_account
  where gm_account = '<TVS');
  
delete from fin.dim_fs_account where gm_account = '<TVS';
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select distinct '<TVS', d.gl_account, current_date, true,'Page 2'
from fin.fact_fs a  -- the fact exposes the relationship between dimensions (org & acct)
inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 201702
  and (
    (b.page = 16 and b.line in (1,2,3,4,5,8,10) and b.col = 1) --uc
    or
    (b.page between 5 and 15 and b.col = 1) -- nc
    or
    (b.page = 17 and b.line between 1 and 21 and b.col = 1)) --f&i
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
and f.area = 'variable';

drop table if exists p2_tvs;
create temp table p2_tvs as
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  sum(coalesce(f.amount, 0)) as amount  
from (-- need to fashion a sudo xfm_fs_1
  select 2017 as the_year, 201702 as year_month, a.fxmpge as page, a.fxmlne as line, a.fxmcol as col, a.fxmact, 
    coalesce(b.gl_account) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  left join fin.dim_fs_account b on a.fxmact = b.gm_account
  where a.fxmpge = 2
    and a.fxmcyy = 2017
    and a.fxmact = '<TVS') a
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.fxmact = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case 
      when d.department = 'Body Shop' then e.department = 'body shop'
      when d.department = 'Car Wash' then e.sub_Department = 'car wash'
      when d.department = 'Detail' then e.sub_department = 'detail'
      when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
      -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
      when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
      when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
      when d.department = 'Parts' then e.department = 'parts'
      when d.department = 'Quick Lane' then e.sub_department = 'pdq'
      when d.department = 'Service' then e.sub_department = 'mechanical'
      when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201702
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
where d.department_code not in ('ad','gn','lr')
  and not (d.store_code = 'RY2' and d.department_code in ('bs','re'))  
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key);   


select * from p2_tvs

-- all good
select c.store, c.area,/* c.department, c.sub_department, b.line, */round(sum(a.amount)), count(*)
from p2_tvs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
group by c.store, c.area--, c.department, c.sub_department--, b.line
order by c.store, c.area--, c.department--, c.sub_department, b.line

-- ok, no relevant rows in fact_fs
select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  and c.gm_account = '<TVS'


insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select fs_key,fs_org_key,fs_account_key,amount
from p2_tvs;

-- so far so good
select store, area, gm_account, sum(amount)
from fin.fact_fs a
inner join fin.dim_Fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  and c.gm_Account in ('<tfs','<tvs')
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key    
group by store, area, gm_account


-- now store
--------------------------------------------------------------------------------------------------------------------------
-- P2L1 Store:SALES:<TNU ****************************
-- which should simply be all accounts in <tfs & <tvs

select *
-- delete
from fin.fact_fs a
where fs_account_key in (
  select fs_account_key
  from fin.dim_fs_account 
  where gm_account = '<TNU');


delete from fin.dim_fs_account where gm_account = '<TNU';
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select distinct '<TNU', gl_account, current_date, true,'Page 2'
from fin.dim_fs_account 
where gm_account in ('<tfs','<tvs');


drop table if exists p2_tnu;
create temp table p2_tnu as
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  sum(coalesce(f.amount, 0)) as amount  
from (-- need to fashion a sudo xfm_fs_1
  select 2017 as the_year, 201702 as year_month, a.fxmpge as page, a.fxmlne as line, a.fxmcol as col, a.fxmact, 
    coalesce(b.gl_account) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  left join fin.dim_fs_account b on a.fxmact = b.gm_account
  where a.fxmpge = 2
    and a.fxmcyy = 2017
    and a.fxmact = '<TNU') a
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.fxmact = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case 
      when d.department = 'Body Shop' then e.department = 'body shop'
      when d.department = 'Car Wash' then e.sub_Department = 'car wash'
      when d.department = 'Detail' then e.sub_department = 'detail'
      when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
      -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
      when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
      when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
      when d.department = 'Parts' then e.department = 'parts'
      when d.department = 'Quick Lane' then e.sub_department = 'pdq'
      when d.department = 'Service' then e.sub_department = 'mechanical'
      when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201702
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
where d.department_code not in ('ad','gn','lr')
  and not (d.store_code = 'RY2' and d.department_code in ('bs','re'))  
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key);   


-- all good
select c.store,/* c.area, c.department, c.sub_department, b.line, */round(sum(a.amount)), count(*)
from p2_tnu a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
group by c.store--, c.area--, c.department--, c.sub_department, b.line
order by c.store--, c.area--, c.department--, c.sub_department, b.line  

-- ok, no relevant rows in fact_fs
select *
delete
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  and c.gm_account = '<TNU'


insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select fs_key,fs_org_key,fs_account_key,amount
from p2_tnu;

-- not sure what to do about this, specifiying only the store includes all 3 columns thereby doubling the total
-- i believe some queries are simply going to have to include columns
-- the problem is that is hidden info and not consistent
-- select * from fin.dim_Fs where year_month = 201702 and page = 2 and line = 1
select sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
  and b.page = 2
  and b.line = 1
  and b.col = 2
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store = 'ry2'







so, i have 2 approaches to departmental attribution
  1. dim_account: part of the definition
  2. fs: location on fs (page, line) determines department, routing attributes accounts

conflicts
what i have not clearly thought out yet is why does my approach to page 2 accounts expose the conflict
because in the generation of <tnu and <tvs i use dim_account to generate the department and the fs for the sub_department 
in that big cased join

-- holy cow, a bunch of fin/sales
-- fin/sales fixed, the rest looks like service/accessories, don't yet know what to do with this
drop table if exists fin_conflict;
create temp table fin_conflict as
select e.account, d.department as fact_dept, d. sub_department, e.department_code
from (
  select distinct b.gl_account, c.store, c.department, c.sub_department
  from fin.fact_fs a
  inner join fin.dim_fs_account b on a.fs_Account_key = b.fs_account_key
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  where department <> 'none') d
left join (
  select account, store_Code, department_code, department
  from fin.dim_account  
  where current_row = true 
    and department_code not in ('ad','gn','lr')) e on d.gl_account = e.account 
where (
  (d.department = 'sales' and e.department_code not in ('uc','nc'))
  or
  (d.department = 'body shop' and e.department_code <> 'bs')
  or
  (d.department = 'finance' and e.department_code <> 'fi')
  or 
  (d.department = 'parts' and e.department_code <> 'pd')
  or
  (d.department = 'service' and e.department_code not in ('sd','re','ql','cw')))
and d.department = 'finance' 
  

------------------------------------------------------------------------------------------------------------------------------------ 
------------------------------------------------------------------------------------------------------------------------------------


-- P2L2C11 Fixed GROSS: <TFEG ****************************
delete from fin.dim_fs_account where gm_account = '<TFEG';
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
-- when i get to backfilling, take out the year_month, dim_fs_account is not restricted by date
-- valid combinations are contigent upon the fact rows
select distinct '<TFEG', d.gl_account, current_date, true,'Page 2'
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 201702
  and b.page = 16
  and b.line between 21 and 61
--    and b.col = 3 -- line 2 is gross all accounts
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.area = 'fixed';

drop table if exists p2_tfeg;
create temp table p2_tfeg as
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  sum(coalesce(f.amount, 0)) as amount  
from (-- need to fashion a sudo xfm_fs_1
  select 2017 as the_year, 201702 as year_month, a.fxmpge as page, a.fxmlne as line, a.fxmcol as col, a.fxmact, 
    coalesce(b.gl_account) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  left join fin.dim_fs_account b on a.fxmact = b.gm_account
  where a.fxmpge = 2
    and a.fxmcyy = 2017
    and a.fxmact = '<TFEG') a    
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.fxmact = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case 
      when d.department = 'Body Shop' then e.department = 'body shop'
      when d.department = 'Car Wash' then e.sub_Department = 'car wash'
      when d.department = 'Detail' then e.sub_department = 'detail'
      when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
      -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
      when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
      when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
      when d.department = 'Parts' then e.department = 'parts'
      when d.department = 'Quick Lane' then e.sub_department = 'pdq'
      when d.department = 'Service' then e.sub_department = 'mechanical'
      when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201702
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
where d.department_code not in ('ad','gn','lr')
  and not (d.store_code = 'RY2' and d.department_code in ('bs','re'))  
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key); 


-- ok, no relevant rows in fact_fs
select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  and c.gm_account = '<TFEG'
    

insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select fs_key,fs_org_key,fs_account_key,amount
from p2_tfeg;

-- P2L2C7 Variable GROSS: <TVEG ****************************
delete 
from fin.fact_fs
where fs_account_key in (
  select fs_account_key
  from fin.dim_fs_account
  where gm_account = '<TVEG');

  
delete from fin.dim_fs_account where gm_account = '<TVEG';
insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
-- when i get to backfilling, take out the year_month, dim_fs_account is not restricted by date
-- valid combinations are contigent upon the fact rows
select distinct '<TVEG', d.gl_account, current_date, true,'Page 2'
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
--   and b.year_month = 201702
  and (
    (b.page = 16 and b.line between 1 and 20) -- in (1,2,3,4,5,8,10)) --uc
    or
    (b.page between 5 and 15) -- nc
    or
    (b.page = 17 and b.line between 1 and 21)) --f&i
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.area = 'variable';

drop table if exists p2_tveg;
create temp table p2_tveg as
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, 
  sum(coalesce(f.amount, 0)) as amount  
from (-- need to fashion a sudo xfm_fs_1
  select 2017 as the_year, 201702 as year_month, a.fxmpge as page, a.fxmlne as line, a.fxmcol as col, a.fxmact, 
    coalesce(b.gl_account) as gl_account
  from arkona.ext_eisglobal_sypffxmst a
  left join fin.dim_fs_account b on a.fxmact = b.gm_account
  where a.fxmpge = 2
    and a.fxmcyy = 2017
    and a.fxmact = '<TVEG') a
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.fxmact = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case 
      when d.department = 'Body Shop' then e.department = 'body shop'
      when d.department = 'Car Wash' then e.sub_Department = 'car wash'
      when d.department = 'Detail' then e.sub_department = 'detail'
      when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
      -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
      when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
      when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
      when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
      when d.department = 'Parts' then e.department = 'parts'
      when d.department = 'Quick Lane' then e.sub_department = 'pdq'
      when d.department = 'Service' then e.sub_department = 'mechanical'
      when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201702
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
where d.department_code not in ('ad','gn','lr')
  and not (d.store_code = 'RY2' and d.department_code in ('bs','re'))  
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key);    

-- 
select store, sum(amount) from p2_tveg a inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key group by store

-- ok, no relevant rows in fact_fs
select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201702
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  and c.gm_account = '<TVEG'
    

insert into fin.fact_fs(fs_key,fs_org_key,fs_account_key,amount)
select fs_key,fs_org_key,fs_account_key,amount
from p2_tveg;

-- store sales
select store, page, line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where b.page = 2
  and b.line = 1
  and b.year_month = 201702
  and b.col < 3
group by store, page, line


-- store gross
select store, page, line, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where b.page = 2
  and b.line = 2
  and b.year_month = 201702
  and b.col > 3
group by store, page, line


  
