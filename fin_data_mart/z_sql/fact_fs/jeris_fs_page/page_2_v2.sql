﻿back fill  fin.dim_Fs_Account with gl_Accounts for page 2 formula accounts
select min(year_month), max(year_month) from fin.dim_fs -- 201101 -> 201702

-- year_month/line/col/gm_account where gm_account like '<%' and gl_account = none for page 2
drop table if exists p2;
create temp table p2 as
select a.year_month, a.page, a.line, a.col, a.line_label, c.gm_account, c.gl_account, b.amount
from fin.dim_fs a
left join fin.fact_fs b on a.fs_key = b.fs_key
left join fin.dim_fs_account c on b.fs_account_key = c.fs_account_key
where a.page = 2
  and a.year_month = 201702
--   and gl_account = 'none'
order by year_month, a.line, a.col;  

select * from fin.dim_fs_account

select *
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
where b.page = 2
--   and year_month = 201702
order by line  

select *
from fin.dim_fs
where year_month = 201702
order by page, line, col

-- here are year_month, line, col, gm_account, gl_account from page 3 and 4
-- line 3 - 54
drop table if exists p3_4;
create temp table p3_4 as
select c.year_month, c.page, c.line, c.col, d.gm_account, d.gl_account
from fin.fact_fs a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
--   and b.store = 'ry1'
inner join fin.dim_fs c on a.fs_key = c.fs_key
--   and c.year_month = 201702
  and c.page in (3,4)
  and c.line between 1 and 55
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
order by c.year_month, c.page, c.line, c.col;  

create index on p2(line);
create index on p2(col);
create index on p3_4(line);
create index on p2(year_month);
create index on p3_4(year_month);

select * from p2

drop table if exists p2_4_54;
create temp table p2_4_54 as
select left(a.year_month::text, 4):: integer as the_year, a.year_month, 
  2 as page, a.line, a.col, a.gm_account, b.gl_account
from p2 a
left join p3_4 b on a.line = b.line
  and a.year_month = b.year_month
where 
  case 
    when a.line < 8 then a.col = 7
    else a.col = 1
  end
  and a.line between 4 and 54;

  
-- line 1 & 2
-- all accounts apply to line 2
drop table if exists line_1_2;
create temp table line_1_2 as
select c.year_month, c.page, c.line, c.col, d.gm_account, d.gl_account,
  case when e.account_type_code in ('4','7') then 1 end as line_1
from fin.fact_fs a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
--   and b.store = 'ry1'
inner join fin.dim_fs c on a.fs_key = c.fs_key
--   and c.year_month = 201702
  and ( -- variable page 3 lines 1 & 2
    (c.page between 5 and 15) -- new cars
    or
    (c.page = 16 and c.line between 1 and 14) -- used cars
    or
    (c.page = 17 and c.line between 1 and 20) -- f&i
    or
    (c.page = 16 and c.line between 20 and 60)) -- fixed excluding parts split
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key  
left join fin.dim_account e on d.gl_account = e.account
order by c.year_month, c.page, c.line, c.col;  
create index on line_1_2(year_month);
create index on line_1_2(line_1);

select * from p2 where line in (1,2)
select * from line_1_2

select distinct col from line_1_2
-- 
-- drop table if exists p2_1_2;
-- create temp table p2_1_2 as
-- select a.year_month, a.line, a.col, a.gm_account, b.gl_account
-- from p2 a
-- left join line_1_2 b on a.year_month = b.year_month
--   and
--     case
--       when a.line = 1 then b.line_1 = 1
--       else 1 = 1
--     end
-- where a.line in (1,2);  

select * from p2

drop table if exists p2_1_2;
create temp table p2_1_2 as
select left(a.year_month::text, 4):: integer as the_year, a.year_month, 
  2 as page, a.line, a.col, a.gm_account, b.gl_account
from p2 a
left join line_1_2 b on a.year_month = b.year_month
where a.line = 1
  and b.line_1 = 1 
union all
select left(a.year_month::text, 4):: integer as the_year, a.year_month, 
  2 as page, a.line, a.col, a.gm_account, b.gl_account
from p2 a
left join line_1_2 b on a.year_month = b.year_month
where a.line = 2;


create index on p2_1_2(the_year);
create index on p2_1_2(year_month);
create index on p2_1_2(line);
create index on p2_1_2(col);
create index on p2_1_2(gm_account);
create index on p2_4_54(gl_account);
create index on p2_4_54(the_year);
create index on p2_4_54(year_month);
create index on p2_4_54(line);
create index on p2_4_54(col);
create index on p2_4_54(gm_account);
create index on p2_4_54(gl_account);

select * from p2 where year_month = 201702 order by line, col

select * from arkona.ext_eisglobal_sypffxmst where fxmcyy = 2017 order by fxmpge, fxmlne, fxmcol

select *
from (
  select page,line,col,gm_account,gl_account from p2_4_54 where year_month = 201702
  union all
  select page,line,col,gm_account,gl_account from p2_1_2 where year_month = 201702) a
except 
select *
from (
  select page,line,col,gm_account,gl_account from p2_4_54 where year_month = 201601
  union all
  select page,line,col,gm_account,gl_account from p2_1_2 where year_month = 201601) b

select '201702', a.*
from (
  select page,line,col,gm_account,gl_account from p2_4_54 where year_month = 201702
  union all
  select page,line,col,gm_account,gl_account from p2_1_2 where year_month = 201702) a
where line = 2
  and gl_Account in ('164501','180601','180801')
union
select '201702', a.*
from (
  select page,line,col,gm_account,gl_account from p2_4_54 where year_month = 201601
  union all
  select page,line,col,gm_account,gl_account from p2_1_2 where year_month = 201601) a
where line = 2
  and gl_Account in ('164501','180601','180801')

-- little bit of mind fucking going on here, but i think i am ok, work it and and verify with line totals
-- first add all the combinations to fin.dim_fs_account

/* this has been done, does not need to be done again
delete from fin.dim_fs_account where row_reason = 'Page 2';

insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select gm_account,gl_account,current_date,true, 'Page 2'
from (
  select gm_account,gl_account from p2_4_54
  union 
  select gm_account,gl_account from p2_1_2) a
group by gm_account, gl_account
on conflict do nothing;
*/
  
-- ok going ahead, but some how i am missing line 2 col 2
-- don't know wtf is going on
-- from fact_fs_monthly_update, this is fin.xfm_fs_1 trimmed down to page 2
-- fields needed: year, year_month,page,line,col,gm_account,gl_account
-- this is the samee as fin.xfm_fs_1 in fact_fs_monthly_update
select the_year, year_month, page, line, col, gm_account, gl_account
from (
  select * from p2_4_54 where year_month = 201702
  union all
  select * from p2_1_2 where year_month = 201702) aa

-- this i where the fs_fact rows are generated

-- insert into fin.fact_fs
drop table if exists fs_p2_201702;
create temp table fs_p2_201702 as
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, sum(coalesce(f.amount, 0)) as amount
-- select *
from (
  select the_year, year_month, page, line, col, gm_account, gl_account
  from (
    select * from p2_4_54 where year_month = 201702
    union all
    select * from p2_1_2 where year_month = 201702) aa) a
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.gm_account = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case -- parts split
      when b.page = 4 and b.line = 59 then 
        case
          when c.gm_account in ('467S','667S', '468S','668S' ) then e.sub_department = 'mechanical'
          when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
          when c.gm_account in ('477s','677s') then e.department = 'body shop'
          when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
          when c.gl_account = '246701D' then e.sub_department = 'pdq'
        end 
      else   
        case 
          when d.department = 'Body Shop' then e.department = 'body shop'
          when d.department = 'Car Wash' then e.sub_Department = 'car wash'
          when d.department = 'Detail' then e.sub_department = 'detail'
          when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
          -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
          when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
          when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
          when d.department = 'Parts' then e.department = 'parts'
          when d.department = 'Quick Lane' then e.sub_department = 'pdq'
          when d.department = 'Service' then e.sub_department = 'mechanical'
          when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
        end
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201702
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'    
where d.department_code not in ('ad','gn','lr')
  and not (d.store_code = 'RY2' and d.department_code in ('bs','re'))
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key);   


select * from fs_p2_201702 where fs_org_key = 18
select * from fin.dim_fs_account where fs_account_key in (1040,1869)
select * from fin.dim_fs_account where gm_account = '<tfeg'
-- a bunch of rows in fs_fact with the now bogus gl_account = none rows from dim_fs_fact
-- get rid of them
create temp table rows_to_delete as
select a.*
from fin.fact_fs a
inner join fin.dim_fs_Account b on a.fs_account_key = b.fs_account_key
  and b.gl_account = 'none'
where amount = 0  

delete from fin.fact_fs a using rows_to_delete
where a.fs_key = rows_to_delete.fs_key
  and a.fs_org_key = rows_to_delete.fs_org_key
  and a.fs_account_key = rows_to_delete.fs_account_key

-- then get rid of the account rows 
delete from fin.dim_fs_account
where gm_account in (
  select gm_account
  from fin.dim_fs_account a
  where exists (
    select 1
    from fin.dim_fs_account 
    where gm_account = a.gm_account
      and gl_account = 'none')
  and exists (
    select 1
    from fin.dim_fs_account
    where gm_account = a.gm_account
      and gl_account <> 'none') order by gm_account)
and gl_account = 'none'      


-- lines 1 & 2 fucked up bad -- looks like it as a column issue believe lines 1 and 2 will have to be done separately for 
--    each org level, eg, <TNU: Store, <TVS: area:variable, <TFS: area:fixed
--    where is org and gm_account matched up?
-- only goes through line 54
-- only ry1: fixed - earlier temp tables were limited to ry1
-- in addition to ry1 & ry2 rows, there is a none row -- fixed with the above 2 deletes
select b.page, b.line, b.line_label, c.store,/*c.area, c.department, c.sub_department,*/ round(sum(a.amount), 0) as amount
-- select *
from fs_p2_201702 a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
group by b.page, b.line, b.line_label, c.store--,c.area, c.department, c.sub_department
order by c.store, b.line--, c.area, c.department,c.sub_department

select b.page, b.line, b.line_label, c.store,/*c.area, c.department, c.sub_department,*/ round(sum(a.amount), 0) as amount
-- select *
from fs_p2_201702 a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
where col = 2
group by b.page, b.line, b.line_label, c.store--,c.area, c.department, c.sub_department
order by c.store, b.line--, c.area, c.department,c.sub_department


select a.*, c.* 
from p2_1_2 a
inner join fin.dim_account b on a.gl_Account = b.account
inner join fin.fact_gl c on b.account_key = c.account_key
inner join dds.dim_date d on c.date_key = d.date_key
  and d.year_month = a.year_month
where a.year_month = 201702
  and a.gl_Account <> 'none'

drop table if exists fs_p2_201702;
create temp table fs_p2_201702 as
select b.fs_key, coalesce(e.fs_org_key, ee.fs_org_key) as fs_org_key, c.fs_account_key, sum(coalesce(f.amount, 0)) as amount
from p2_1_2 a
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.gm_account = c.gm_account
  and a.gl_account = c.gl_account  
left join fin.dim_account d on c.gl_account = d.account  
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case -- parts split
      when b.page = 4 and b.line = 59 then 
        case
          when c.gm_account in ('467S','667S', '468S','668S' ) then e.sub_department = 'mechanical'
          when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
          when c.gm_account in ('477s','677s') then e.department = 'body shop'
          when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
          when c.gl_account = '246701D' then e.sub_department = 'pdq'
        end 
      else   
        case 
          when d.department = 'Body Shop' then e.department = 'body shop'
          when d.department = 'Car Wash' then e.sub_Department = 'car wash'
          when d.department = 'Detail' then e.sub_department = 'detail'
          when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
          -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
          when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
          when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
          when d.department = 'Parts' then e.department = 'parts'
          when d.department = 'Quick Lane' then e.sub_department = 'pdq'
          when d.department = 'Service' then e.sub_department = 'mechanical'
          when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
        end
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201702
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth    
left join fin.dim_fs_org ee on 1 = 1
  and ee.market = 'none'   
where a.year_month = 201702
  and d.department_code not in ('ad','gn','lr')
  and not (d.store_code = 'RY2' and d.department_code in ('bs','re'))
  and d.account = '1429301'
group by a.year_month, b.fs_key, c.fs_account_key, 
  coalesce(e.fs_org_key, ee.fs_org_key);   

select *
from p2_1_2 a
left join fin.dim_fs b on a.year_month = b.year_month
  and a.page = b.page
  and a.line = b.line
  and a.col = b.col
left join fin.dim_fs_account c on a.gm_account = c.gm_account
  and a.gl_account = c.gl_account 
left join fin.dim_account d on c.gl_account = d.account   
left join fin.dim_Fs_org e on d.store_code = e.store
  and
    case -- parts split
      when b.page = 4 and b.line = 59 then 
        case
          when c.gm_account in ('467S','667S', '468S','668S' ) then e.sub_department = 'mechanical'
          when c.gm_account in ('478s','678s') then e.sub_department = 'pdq'
          when c.gm_account in ('477s','677s') then e.department = 'body shop'
          when c.gl_account = '246700D' then e.sub_department = 'mechanical' 
          when c.gl_account = '246701D' then e.sub_department = 'pdq'
        end 
      else   
        case 
          when d.department = 'Body Shop' then e.department = 'body shop'
          when d.department = 'Car Wash' then e.sub_Department = 'car wash'
          when d.department = 'Detail' then e.sub_department = 'detail'
          when d.department = 'Finance' and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Finance' and b.line > 10 then e.department = 'finance' and e.sub_department = 'used'
          -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
          when d.department = 'New Vehicle' and b.page = 17 and b.line < 10 then e.department = 'finance' and e.sub_department = 'new'
          when d.department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then e.department = 'finance' and e.sub_department = 'used'      
          when d.department = 'New Vehicle' then e.department = 'sales' and e.sub_department = 'new'
          when d.department = 'Parts' then e.department = 'parts'
          when d.department = 'Quick Lane' then e.sub_department = 'pdq'
          when d.department = 'Service' then e.sub_department = 'mechanical'
          when d.department = 'Used Vehicle' then e.department = 'sales' and e.sub_department = 'used'
        end
    end
left join (
  select b.yearmonth, aa.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201702
  group by b.yearmonth, aa.account) f on a.gl_account = f.account
    and b.year_month = f.yearmonth   
where c.gl_account in ( '1429301','1429001','1629001','1629301')

select * from fin.dim_fs_account where fs_Account_key = 11175  
select * from fin.dim_account where account = '1429301'

  select b.yearmonth, aa.account, sum(a.amount) as amount
--   select sum(a.amount)
  from fin.fact_gl a
  inner join fin.dim_account aa on a.account_key = aa.account_key
    and aa.account in ( '1429301','1429001','1629001','1629301')
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
    and b.yearmonth = 201702
  group by b.yearmonth, aa.account