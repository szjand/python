﻿
drop table if exists fin.dim_fs_account cascade;
create table if not exists fin.dim_fs_account (
  fs_account_key serial primary key,
  gm_account citext not null,
  gl_account citext not null,
  row_from_date date not null,
  row_thru_date date not null default '9999-12-31'::date,
  current_row boolean not null,
  row_reason citext not null default 'New Row'::citext,
constraint dim_fs_account_nk unique (gm_account, gl_account));
comment on table fin.dim_fs_account is
'where gl_account = none:
  if the first char is <, & or * it is a "formula" account
  if not it is a gm account not routed in ffpxrefdta';
comment on column fin.dim_fs_account.gm_account is 'source: arkona.ext_eis_global_sypffxmst.fxmact';
comment on column fin.dim_fs_account.gl_account is 'source: arkona.ext_ffpxrefdta.g_l_acct_number';  
create index on fin.dim_fs_account(gm_account);
create index on fin.dim_fs_account(gl_account);
-- ah by not joining on year, that's how i left out the gm_account/none for
-- gm accounts that were routed in some years but not in others
insert into fin.dim_fs_account (gm_account, gl_account,row_from_date, current_row)
select a.fxmact, 
  coalesce(
    case 
      when b.g_l_acct_number like '%SPLT%' then replace(b.g_l_acct_number,'SPLT','')::citext
      else b.g_l_acct_number
    end, 'none') as gl_account,
    '01/01/2016', true
from arkona.ext_eisglobal_sypffxmst a
left join (
  select g_l_acct_number, factory_account, factory_financial_year
  from arkona.ext_ffpxrefdta 
  group by g_l_acct_number, factory_account, factory_financial_year) b on a.fxmact = b.factory_account
    and a.fxmcyy = b.factory_financial_year
group by a.fxmact, b.g_l_acct_number;