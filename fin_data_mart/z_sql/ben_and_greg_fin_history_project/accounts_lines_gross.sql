﻿
-- 12/6/17  -- added 201711

drop table if exists unit_counts cascade;
create temp table unit_counts as
-- include stocknumber on each row
select year_month, store as store_code, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select b.year_month, d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month between 201201 and 201711 ----------------------------------------------------------------------------
  inner join fin.dim_account c on a.account_key = c.account_key
-- add journal
  inner join fin.dim_journal aa on a.journal_key = aa.journal_key
    and aa.journal_code in ('VSN','VSU')
  inner join ( -- d: fs gm_account page/line/acct description
    select b.year_month, f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month between 201201 and 201711 -------------------------------------------------------------------------
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account and b.year_month = d.year_month
  where a.post_status = 'Y') h
group by year_month, store, page, line, line_label, control;

drop table if exists accounts_lines cascade;
create temp table accounts_lines as
-- from sales_consultant_payroll.py AccountsRoutes
-- accounts/page/line/store/year
select distinct a.fxmcyy as the_year,
  case coalesce(b.consolidation_grp, '1') when '2' then 'RY2' else 'RY1' end::citext as store_code,
  a.fxmpge as page, a.fxmlne::integer as line, trim(b.g_l_acct_number) as gl_account
from arkona.ext_eisglobal_sypffxmst a
inner join arkona.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
where a.fxmcyy between 2012 and 2017
  and trim(a.fxmcde) = 'GM'
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_code = 'GM'
  and trim(b.factory_account) <> '331A'
  and b.company_number = 'RY1'
  and b.g_l_acct_number <> ''
  and (
    (a.fxmpge = 3 and a.fxmlne = 67) or
    (a.fxmpge between 5 and 15) or
    (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
    (a.fxmpge = 17 and a.fxmlne between 1 and 19));
create unique index on accounts_lines(the_year, store_code, gl_account);   
create index on accounts_lines(the_year);
create index on accounts_lines(gl_account);

-- from sales_consultant_payroll.py DealsAccountingDetail
-- insert into sls.deals_accounting_detail
-- did a check on 2012, looks good
drop table if exists accounting_detail cascade;
create temp table accounting_detail as
select a.trans, a.seq, c.the_date, c.year_month, a.control,
  aa.journal_code, b.account_type, b.department,
  d.store_code, d.page, d.line, d.gl_Account, b.description as account_description,
  a.amount
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join dds.dim_date c on a.date_key = c.date_key
inner join fin.dim_account b on a.account_key = b.account_key
  and c.the_date between b.row_from_Date and b.row_thru_date
inner join accounts_lines d on b.account = d.gl_account
  and c.the_year = d.the_year
where post_status = 'Y'
  and c.year_month between 201201 and 201711; ----------------------------------------------------------------------------------------------------------------

-- use this as base for left joins
drop table if exists pages_lines cascade;
create temp table pages_lines as
select a.the_year * 100 + the_month as year_month, store_code, page, line
from (  
  select the_year, store_code, page, line
  from accounts_lines
  group by the_year, store_code, page, line) a
cross join (
  select * from generate_series(1,12,1) as the_month) b
group by year_month,store_code, page,line;  
create unique index on pages_lines(year_month,store_code,page,line); 


-- ry1 201701, new_oper_gross: 103969, new_fs_gross: 31383, holy shit, i know the fs gross is correct, need to drill into oper_gross
-- big difference
-- ok, this seems to be getting into aftermarket and is confusing the hell out of me, aftermarket seems to be controlled
-- by last 6 of vin
-- fuck operational gross, stick to fs

-- ok, the rabbit hole of counts matching is ok with this
-- shit, did inner join in y and now 67 rows of mismatch
-- but of course there will be, those are deliveries not the fs count
-- eg ry1 201708 fs count 223, y query = 233 with 5 back ons, 233 - 5 = 228 the actual count, 228 - 5 = 223
-- i am thinking, fuck it, close enough
-- 11/8/17 v2 added fi income

select x.*, y.retail_trade, y.retail_purch, y.whlsl_Trade, y.whlsl_purch, y.total_trade, y.total_purch, 
  y.under_10k_retail, y.over_10k_retail, y.under_10k_whlsl, y.over_10k_whlsl, y.under_10k_total, y.over_10k_total
from (
  select 
    aa.store_code, aa.year_month, 
    sum(case when aa.page between 5 and 15 then bb.the_count else 0 end) as new_count,
    sum(case when aa.page between 5 and 15 then cc.fs_gross else 0 end) as new_gross,
    (sum(case when aa.page between 5 and 15 then cc.fs_gross else 0 end)/sum(case when aa.page between 5 and 15 then bb.the_count else 0 end))::integer as new_pvr,
    sum(case when aa.page = 16 and aa.line between 1 and 6 then bb.the_count else 0 end) as used_retail_count,
    sum(case when aa.page = 16 and aa.line between 1 and 6 then cc.fs_gross else 0 end) as used_retail_gross,
    (sum(case when aa.page = 16 and aa.line between 1 and 6 then cc.fs_gross else 0 end)/sum(case when aa.page = 16 and aa.line between 1 and 6 then bb.the_count else 0 end))::integer as used_retail_pvr,
    sum(case when aa.page = 16 and aa.line between 8 and 11 then bb.the_count else 0 end) as used_whlsl_count,
    sum(case when aa.page = 16 and aa.line between 8 and 11 then cc.fs_gross else 0 end) as used_whlsl_gross,
    (sum(case when aa.page = 16 and aa.line between 8 and 11 then cc.fs_gross else 0 end)/sum(case when aa.page = 16 and aa.line between 1 and 6 then bb.the_count else 0 end))::integer as used_whlsl_pvr,
    sum(case when aa.page = 17 and aa.line = 3 then -cc.fs_gross else 0 end) as new_fi_chg_back,
    sum(case when aa.page = 17 and aa.line = 13 then -cc.fs_gross else 0 end) as used_fi_chg_back,
    sum(case when aa.page = 17 and aa.line = 9 then -cc.fs_gross else 0 end) as new_fi_comp,
    sum( -- layout changed in 2014
      case when aa.page = 17 then 
        case 
          when aa.year_month < 201401 and aa.line = 18 then -cc.fs_gross 
          when aa.year_month > 201312 and aa.line = 19 then -cc.fs_gross
          else 0 
        end
      end) as used_fi_comp,
    sum(case when aa.page = 17 and aa.line between 1 and 9 then cc.fs_gross else 0 end) as new_fi_inc,
    sum(case when aa.page = 17 and aa.line between 11 and 19 then cc.fs_gross else 0 end) as used_fi_inc,
    sum(case when aa.page = 17 then cc.fs_gross else 0 end) as total_fi_inc,      
    sum(case when aa.page = 3 then cc.fs_gross else 0 end) as doc_fees
  from pages_lines aa
  left join (
    select a.year_month, a.store_code, a.page, a.line, sum(a.unit_count) as the_count
    from unit_counts a
    group by a.year_month, a.store_code, a.page, a.line) bb on aa.year_month = bb.year_month
      and aa.store_code = bb.store_code
      and aa.page = bb.page 
      and aa.line = bb.line
  left join (
    select a.year_month, a.store_code, a.page, a.line, sum(-a.amount)::integer as fs_gross
    from accounting_detail a
    group by a.year_month, a.store_code, a.page, a.line) cc on aa.year_month = cc.year_month
      and aa.store_code = cc.store_code
      and aa.page = cc.page 
      and aa.line = cc.line
  group by aa.store_code, aa.year_month) x 
left join (  
  select a.year_month, a.store_code, 
    sum(case when right(trim(a.control), 1) in ('A','B','C','D','E') and a.line between 1 and 6 then unit_count * 1 else 0 end) as retail_trade,
    sum(case when right(trim(a.control), 1) not in ('A','B','C','D','E') and a.line between 1 and 6 then unit_count * 1 else 0 end) as retail_purch,
    sum(case when right(trim(a.control), 1) in ('A','B','C','D','E') and a.line between 8 and 11 then unit_count * 1 else 0 end) as whlsl_trade,  
    sum(case when right(trim(a.control), 1) not in ('A','B','C','D','E') and a.line between 8 and 11 then unit_count * 1 else 0 end) as whlsl_purch,
    sum(case when right(trim(a.control), 1) in ('A','B','C','D','E') then unit_count * 1 else 0 end) as total_trade,
    sum(case when right(trim(a.control), 1) not in ('A','B','C','D','E') then unit_count * 1 else 0 end) as total_purch,
    sum(case when abs(b.amount) <= 10000 and a.line between 1 and 6 then unit_count * 1 else 0 end) as under_10k_retail,
    sum(case when abs(b.amount) > 10000 and a.line between 1 and 6 then unit_count * 1 else 0 end) as over_10k_retail,
    sum(case when abs(b.amount) <= 10000 and a.line between 8 and 11 then unit_count * 1 else 0 end) as under_10k_whlsl,
    sum(case when abs(b.amount) > 10000 and a.line between 8 and 11 then unit_count * 1 else 0 end) as over_10k_whlsl,
    sum(case when abs(b.amount) <= 10000 then unit_count * 1 else 0 end) as under_10k_total,
    sum(case when abs(b.amount) > 10000 then unit_count * 1 else 0 end) as over_10k_total
  -- select *  
  from (
    select year_month, store_code, line, control, sum(unit_count) as unit_count
    from unit_counts
    where page = 16
    group by year_month, store_code, line, control
    having sum(unit_count) > 0) a
    full outer join (
    select store_code, year_month, line, control, sum(amount) as amount
    from accounting_detail
    where account_type = 'sale'
      and journal_code = 'vsu'
      and page = 16
    group by store_code, year_month, line, control
    having sum(amount) < 0) b on a.control = b.control and a.year_month = b.year_month
  group by a.year_month, a.store_code) y on x.store_code = y.store_code and x.year_month = y.year_month
where x.year_month < 201711  
order by x.store_code, x.year_month    




-- 11/8/17 v3 broke out new fleet
-- greg complaining about new counts, need to break out aftermarket, fleet, internal, retail


/*
-- this is easier than looking at each fs
select store, year_month, 
  sum(case when page between 5 and 15 and line in (22,23) then amount else 0 end) as int_gross,
  sum(case when page between 5 and 15 and line in (43,44) then amount else 0 end) as fleet_gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
where store <> 'none'
group by store, year_month
order by store, year_month
*/


-- 12/6/17  -- added 201711
-- create temp table wtf as
select x.*, y.retail_trade, y.retail_purch, y.whlsl_Trade, y.whlsl_purch, y.total_trade, y.total_purch, 
  y.under_10k_retail, y.over_10k_retail, y.under_10k_whlsl, y.over_10k_whlsl, y.under_10k_total, y.over_10k_total
from (
  select 
    aa.store_code, aa.year_month, 
    sum(case when aa.page between 5 and 15 and aa.line not in ('22','23','43','44','47') then bb.the_count else 0 end) as new_retail_count,
    sum(case when aa.page between 5 and 15 and aa.line not in ('22','23','43','44','47') then cc.fs_gross else 0 end) as new__retail_gross,
    case
      when sum(case when aa.page between 5 and 15 and aa.line not in ('22','23','43','44','47') then bb.the_count else 0 end) = 0 then 0
      else (sum(case when aa.page between 5 and 15 and aa.line not in ('22','23','43','44','47') then cc.fs_gross else 0 end)
              /sum(case when aa.page between 5 and 15 and aa.line not in ('22','23','43','44','47') then bb.the_count else 0 end))::integer 
    end as new_retail_pvr,
    sum(case when aa.page between 5 and 15 and aa.line in ('22','23') then bb.the_count else 0 end) as new_internal_count,
    sum(case when aa.page between 5 and 15 and aa.line in ('22','23') then cc.fs_gross else 0 end) as new__internal_gross,
    case
      when sum(case when aa.page between 5 and 15 and aa.line in ('22','23') then bb.the_count else 0 end) = 0 then 0
      else (sum(case when aa.page between 5 and 15 and aa.line in ('22','23') then cc.fs_gross else 0 end)
              /sum(case when aa.page between 5 and 15 and aa.line in ('22','23') then bb.the_count else 0 end))::integer 
    end as new_internal_pvr,
    sum(case when aa.page between 5 and 15 and aa.line in ('43','44') then bb.the_count else 0 end) as new_fleet_count,
    sum(case when aa.page between 5 and 15 and aa.line in ('43','44') then cc.fs_gross else 0 end) as new__fleet_gross,
    case
      when sum(case when aa.page between 5 and 15 and aa.line in ('43','44') then bb.the_count else 0 end) = 0 then 0
      else (sum(case when aa.page between 5 and 15 and aa.line in ('43','44') then cc.fs_gross else 0 end)
              /sum(case when aa.page between 5 and 15 and aa.line in ('43','44') then bb.the_count else 0 end))::integer 
    end as new_fleet_pvr,      
    sum(case when aa.page between 5 and 15 and aa.line = '47' then cc.fs_gross else 0 end) as aftmkt_gross,
    sum(case when aa.page = 16 and aa.line between 1 and 6 then bb.the_count else 0 end) as used_retail_count,
    sum(case when aa.page = 16 and aa.line between 1 and 6 then cc.fs_gross else 0 end) as used_retail_gross,

    case
      when sum(case when aa.page = 16 and aa.line between 1 and 6 then bb.the_count else 0 end) = 0 then 0
      else (sum(case when aa.page = 16 and aa.line between 1 and 6 then cc.fs_gross else 0 end)
            /sum(case when aa.page = 16 and aa.line between 1 and 6 then bb.the_count else 0 end))::integer
    end as used_retail_pvr,
    
    sum(case when aa.page = 16 and aa.line between 8 and 11 then bb.the_count else 0 end) as used_whlsl_count,
    sum(case when aa.page = 16 and aa.line between 8 and 11 then cc.fs_gross else 0 end) as used_whlsl_gross,

    case
      when sum(case when aa.page = 16 and aa.line between 1 and 6 then bb.the_count else 0 end) = 0 then 0
      else (sum(case when aa.page = 16 and aa.line between 8 and 11 then cc.fs_gross else 0 end)
          /sum(case when aa.page = 16 and aa.line between 1 and 6 then bb.the_count else 0 end))::integer 
    end as used_whlsl_pvr,
    
    sum(case when aa.page = 17 and aa.line = 3 then -cc.fs_gross else 0 end) as new_fi_chg_back,
    sum(case when aa.page = 17 and aa.line = 13 then -cc.fs_gross else 0 end) as used_fi_chg_back,
    sum(case when aa.page = 17 and aa.line = 9 then -cc.fs_gross else 0 end) as new_fi_comp,
    sum( -- layout changed in 2014
      case when aa.page = 17 then 
        case 
          when aa.year_month < 201401 and aa.line = 18 then -cc.fs_gross 
          when aa.year_month > 201312 and aa.line = 19 then -cc.fs_gross
          else 0 
        end
      end) as used_fi_comp,
    sum(case when aa.page = 17 and aa.line between 1 and 9 then cc.fs_gross else 0 end) as new_fi_inc,
    sum(case when aa.page = 17 and aa.line between 11 and 19 then cc.fs_gross else 0 end) as used_fi_inc,
    sum(case when aa.page = 17 then cc.fs_gross else 0 end) as total_fi_inc,      
    sum(case when aa.page = 3 then cc.fs_gross else 0 end) as doc_fees
  from pages_lines aa
  left join (
    select a.year_month, a.store_code, a.page, a.line, sum(a.unit_count) as the_count
    from unit_counts a
    group by a.year_month, a.store_code, a.page, a.line) bb on aa.year_month = bb.year_month
      and aa.store_code = bb.store_code
      and aa.page = bb.page 
      and aa.line = bb.line
  left join (
    select a.year_month, a.store_code, a.page, a.line, sum(-a.amount)::integer as fs_gross
    from accounting_detail a
    group by a.year_month, a.store_code, a.page, a.line) cc on aa.year_month = cc.year_month
      and aa.store_code = cc.store_code
      and aa.page = cc.page 
      and aa.line = cc.line
  group by aa.store_code, aa.year_month) x 
left join (  
  select a.year_month, a.store_code, 
    sum(case when right(trim(a.control), 1) in ('A','B','C','D','E') and a.line between 1 and 6 then unit_count * 1 else 0 end) as retail_trade,
    sum(case when right(trim(a.control), 1) not in ('A','B','C','D','E') and a.line between 1 and 6 then unit_count * 1 else 0 end) as retail_purch,
    sum(case when right(trim(a.control), 1) in ('A','B','C','D','E') and a.line between 8 and 11 then unit_count * 1 else 0 end) as whlsl_trade,  
    sum(case when right(trim(a.control), 1) not in ('A','B','C','D','E') and a.line between 8 and 11 then unit_count * 1 else 0 end) as whlsl_purch,
    sum(case when right(trim(a.control), 1) in ('A','B','C','D','E') then unit_count * 1 else 0 end) as total_trade,
    sum(case when right(trim(a.control), 1) not in ('A','B','C','D','E') then unit_count * 1 else 0 end) as total_purch,
    sum(case when abs(b.amount) <= 10000 and a.line between 1 and 6 then unit_count * 1 else 0 end) as under_10k_retail,
    sum(case when abs(b.amount) > 10000 and a.line between 1 and 6 then unit_count * 1 else 0 end) as over_10k_retail,
    sum(case when abs(b.amount) <= 10000 and a.line between 8 and 11 then unit_count * 1 else 0 end) as under_10k_whlsl,
    sum(case when abs(b.amount) > 10000 and a.line between 8 and 11 then unit_count * 1 else 0 end) as over_10k_whlsl,
    sum(case when abs(b.amount) <= 10000 then unit_count * 1 else 0 end) as under_10k_total,
    sum(case when abs(b.amount) > 10000 then unit_count * 1 else 0 end) as over_10k_total
  -- select *  
  from (
    select year_month, store_code, line, control, sum(unit_count) as unit_count
    from unit_counts
    where page = 16
    group by year_month, store_code, line, control
    having sum(unit_count) > 0) a
    full outer join (
    select store_code, year_month, line, control, sum(amount) as amount
    from accounting_detail
    where account_type = 'sale'
      and journal_code = 'vsu'
      and page = 16
    group by store_code, year_month, line, control
    having sum(amount) < 0) b on a.control = b.control and a.year_month = b.year_month
  group by a.year_month, a.store_code) y on x.store_code = y.store_code and x.year_month = y.year_month
where x.year_month < 201712 ------------------------------------------------------------------------------------------------------------
order by x.store_code, x.year_month    



