﻿-- page 7 accounts
select distinct f.store, b.line, b.col, b.line_label, d.gl_account, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201710
  and b.page = 17
--   and b.line between 18 and 40
--   and b.col = 5
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  and f.store in ('ry1','ry2')
order by f.store, b.line, d.gl_account


select *
from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2017
  and fxmpge = 1
order by fxmlne, fxmcol

-- line 32
select *
from arkona.ext_ffpxrefdta
where factory_financial_year = 2017
  and factory_Account = '322'


select *
from fin.dim_account
where account = '124010'


select b.year_month,a.*, c.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
-- where c.account = '124010'
where b.year_month = 201710
limit 500


select *
from fin.dim_account
where account = '123100'


-- inventory accounts
select *
from fin.dim_account
where account_Type = 'asset' 
  and department_code = 'nc'
  and typical_balance = 'debit'

select *
from fin.dim_account
where account_Type = 'asset' 
  and department_code = 'uc'
  and typical_balance = 'debit'  



select b.year_month,a.*, c.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
where c.account = '123101'
  and control = '28739'
-- where b.year_month = 201710  



select b.year_month,a.*, c.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
where c.account = '126104'
  and b.year_month = 201710


select b.year_month,a.*, c.*
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
where control = '20851xx'
order by account
  

2/12/18
-- the lines of interest 20 thru 24 are all aggregate accounts, <CARS, etc
-- as such, they are of no use to me
-- but this is not too bad, use a sanitized list of all inventory accounts from inpmast
select * 
from arkona.ext_eisglobal_Sypffxmst
where fxmcyy = 2018
  and fxmpge = 1
  and fxmlne between 20 and 26
  and fxmcol < 6
order by fxmlne  

-- close enuf
drop table if exists inv_accts;
create temp table inv_accts as
select b.account_key, b.account, b.description, count(*)
from arkona.xfm_inpmast a
left join fin.dim_Account b on a.inventory_account = b.account 
where a.inventory_account is not null
  and b.description is not null
  and b.description not like 'C/S%'
group by b.account_key, b.account, b.description
having count(*) > 1;

select * from inv_accts order by account

-- so this says 1262 vehicles in inventory
select control, b.account, b.description, max(c.the_date) as last_date, sum(amount) as amount
from fin.fact_gl a
inner join inv_accts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where a.post_status = 'Y'
group by control, b.account, b.description
having sum(amount) > 0
order by max(c.the_date)



select b.account, b.description, count(*)
from fin.fact_gl a
inner join inv_accts b on a.account_key = b.account_key
inner join dds.dim_date c on a.date_key = c.date_key
where a.post_status = 'Y'
group by b.account, b.description
having sum(amount) > 0
order by b.account
  