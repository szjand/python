# encoding=utf-8
"""
09/04/20
    try pdftotext
    https://pypi.org/project/pdftotext/
    requires sudo apt install build-essential libpoppler-cpp-dev pkg-config python3-dev
    and pdftotext successfully installed

    https://www.howtogeek.com/228531/how-to-convert-a-pdf-file-to-editable-text-using-the-command-line-in-linux/
"""

import pdftotext
pdf_dir = '/mnt/hgfs/E/python projects/pdf_diff/pdfs/'
with open (pdf_dir + 'CRV_HP-Z30.pdf', 'rb') as f:
    pdf = pdftotext.PDF(f)

print(len(pdf))

for page in pdf:
    print(page)

