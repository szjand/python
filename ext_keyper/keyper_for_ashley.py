import pyodbc
import csv
import smtplib
from email.mime.text import MIMEText

sql_con = pyodbc.connect('DRIVER={SQL Server};Server=10.134.196.201;port=1433\keyperserver;'
                         'Network Library=DBMSSOCN;Database=KeyperData;uid=sa;pwd=Key539737;')
sql_cur = sql_con.cursor()

file_name = 'files/ext_keyper.txt'

# # production query
sql = """
    select 'stock #' as stock_number,'checked out by' as checked_out_by, 'checked out at' as checked_out_at
    union
    select a.asset_name as stock_number, b.first_name + ' ' + b.last_name as "checked out by",
      convert(nvarchar(MAX), dateadd(hour, -5, a.transaction_date), 100) as "checked out at"
    from dbo.asset_transactions a
    left join dbo.[user] b on a.user_id = b.user_id
    left join dbo.asset c on a.asset_id = c.asset_id
    left join dbo.issue_reason d on a.issue_reason_id = d.issue_reason_id
    where transaction_date > convert(datetime, convert(nvarchar(MAX), getdate(), 101) + ' 10:00:00 PM') -- 5 PM CST
      and a.system_id = 27 -- used car keyper
      and a.asset_status = 'Out' -- removed
      and c.asset_Status = 'Out' -- and not yet returned
    order by stock_number desc
"""
#
# test query lots of rows
# sql = """
#     select 'stock #' as stock_number,'checked out by' as checked_out_by, 'checked out at' as checked_out_at
#     union
#     select a.asset_name as stock_number, b.first_name + ' ' + b.last_name as "checked out by",
#       convert(nvarchar(MAX), dateadd(hour, -5, a.transaction_date), 100) as "checked out at"
#     from dbo.asset_transactions a
#     left join dbo.[user] b on a.user_id = b.user_id
#     left join dbo.asset c on a.asset_id = c.asset_id
#     left join dbo.issue_reason d on a.issue_reason_id = d.issue_reason_id
#     where transaction_date > convert(datetime, convert(nvarchar(MAX), getdate() -1, 101) + ' 10:00:00 PM') -- 5 PM CST
#       and a.system_id = 27 -- used car keyper
#       and a.asset_status = 'Out' -- removed
#       -- and c.asset_Status = 'Out' -- and not yet returned
#     order by stock_number desc
# """
sql_cur.execute(sql)
# write the file as tab delimited, for clean embedding in email body
with open(file_name, 'wb') as f:
    writer = csv.writer(f, delimiter='\t')
    writer.writerows(sql_cur)
sql_cur.close()
sql_con.close()
# proceess based on the number of lines in the file, 1 line means header only, no results
if sum(1 for line in open(file_name)) > 1:
    try:
        COMMASPACE = ', '
        sender = 'jandrews@cartiva.com'
        recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
        # recipients = ['test@cartiva.com', 'jandrews@cartiva.com', 'afontaine@rydellcars.com']
        with open(file_name, 'r') as fp:
            outer = MIMEText(fp.read())
        outer['Subject'] = 'Keys checked out last night'
        outer['To'] = COMMASPACE.join(recipients)
        outer['From'] = sender
        outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
        # this works with Python2
        e = smtplib.SMTP('mail.cartiva.com')
        try:
            e.sendmail(sender, recipients, outer.as_string())
        except smtplib.SMTPException:
            print "Error: unable to send email"
        e.quit()
    except Exception, error:
        print error
else:
    try:
        COMMASPACE = ', '
        sender = 'jandrews@cartiva.com'
        recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
        # recipients = ['test@cartiva.com', 'jandrews@cartiva.com', 'afontaine@rydellcars.com']
        outer = MIMEText('Really, there were no keys checked out last night')
        outer['Subject'] = 'There were no keys checked out last night'
        outer['To'] = COMMASPACE.join(recipients)
        outer['From'] = sender
        outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
        # this works with Python2
        e = smtplib.SMTP('mail.cartiva.com')
        try:
            e.sendmail(sender, recipients, outer.as_string())
            # print "Successfully sent email"
        except smtplib.SMTPException:
            print "Error: unable to send email"
        e.quit()
    except Exception, error:
        print error
