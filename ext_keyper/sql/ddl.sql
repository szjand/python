﻿create schema keys;
comment on schema keys is 'Keyper key system data, schema name is plural to avoid using a keyword';

drop table if exists keys.ext_keyper cascade;
create table keys.ext_keyper (
  stock_number citext not null,
  transaction_date timestamptz not null,
  message citext not null,
  asset_status citext not null,
  created_date date not null,
  cabinet citext not null,
  first_name citext not null,
  last_name citext not null,
  issue_reason citext not null default 'none',
  primary key (stock_number,transaction_date));
comment on table keys.ext_keyper is 'scraped data from tables in keyper system: asset_transactions,asset,syste,user,issue_reason';  
comment on column keys.ext_keyper.stock_number is 'dbo.asset_transactions.asset_name';
comment on column keys.ext_keyper.transaction_date is 'dbo.asset_transactions.transaction_date, converted from UTC';
comment on column keys.ext_keyper.message is 'dbo.asset_transactions.message';
comment on column keys.ext_keyper.asset_status is 'dbo.asset_transactions.asset_status';
comment on column keys.ext_keyper.created_date is 'dbo.asset.created_date';
comment on column keys.ext_keyper.cabinet is 'dbo.system.name';
comment on column keys.ext_keyper.first_name is 'dbo.user.first_name';
comment on column keys.ext_keyper.last_name is 'dbo.user.last_name';
comment on column keys.ext_keyper.issue_reason is 'dbo.issue_reason.name';
create index on keys.ext_keyper(created_date);
create index on keys.ext_keyper(issue_reason);

drop table if exists keys.ext_keyper_tmp cascade;
create table keys.ext_keyper_tmp (
  stock_number citext not null,
  transaction_date timestamptz not null,
  message citext not null,
  asset_status citext not null,
  created_date date not null,
  cabinet citext not null,
  first_name citext not null,
  last_name citext not null,
  issue_reason citext not null default 'none',
  primary key (stock_number,transaction_date));
comment on table keys.ext_keyper is 'scraped data from tables in keyper system for periodic processing';  


select a.inpmast_stock_number, b.*
from arkona.xfm_inpmast a
inner join keys.ext_keyper b on a.inpmast_stock_number = b.stock_number
inner join (
  select a.stock_number, max(a.transaction_date) as transaction_date
  from keys.ext_keyper a
  inner join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
    and b.current_row = true
    and b.status = 'I'
  group by a.stock_number) c on b.stock_number = b.stock_number 
    and b.transaction_date = c.transaction_date  
where a.status = 'I'
  and a.current_row = true
order by a.inpmast_stock_number


select a.stock_number, max(a.transaction_date) as transaction_date
from keys.ext_keyper a
inner join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
  and b.current_row = true
  and b.status = 'I'
group by a.stock_number
order by a.stock_number    


select x.*, current_date - transaction_date::date
from (
  select a.*, row_number() over (partition by stock_number order by transaction_date desc)
  from keys.ext_keyper a
  inner join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
    and b.status = 'I'
    and b.current_row = true) x
where row_number =  1
  and asset_status = 'out'
order by current_date - transaction_date::date desc


-- keys checked out for more than 9 days
-- still some issues, auction, ws buffer ... not sure about it
select upper(stock_number) as stock_number, last_name || ', '|| first_name as checked_out_by, 
  issue_reason, current_date - transaction_date::date as days_checked_out--, c.status
from (
  select a.*, row_number() over (partition by stock_number order by transaction_date desc)
  from keys.ext_keyper a
  inner join arkona.xfm_inpmast b on a.stock_number = b.inpmast_stock_number
    and b.status = 'I'
    and b.current_row = true) x
left join (
  select a.stocknumber, b.status, b.fromts::date
  from ads.ext_vehicle_inventory_items a
  left join ads.ext_vehicle_inventory_item_Statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
    and b.thruts::date > current_date
    and b.category not like '%Recon%'
    and b.category not like '%RB%'
    and b.category not in ( 'RawMaterials','RMFlagFLR')) c on x.stock_number = c.stocknumber    
where row_number =  1
  and asset_status = 'out'
  and current_date - transaction_date::date between 10 and 100
  and issue_reason <> 'auction'
  and coalesce(c.status, 'wtf') not like '%Auction'
order by current_date - transaction_date::date desc


-- function keys.update_ext_keyper()
-- insert into keys.ext_keyper (stock_number,transaction_date,message,asset_status,
--   created_date,cabinet,first_name,last_name,issue_reason)
select a.stock_number,a.transaction_date,a.message,a.asset_status,
  a.created_date,a.cabinet,a.first_name,a.last_name,a.issue_reason
from keys.ext_keyper_tmp a
left join keys.ext_keyper b on a.stock_number = b.stock_number
  and a.transaction_date = b.transaction_date  
where b.stock_number is null;




select *
from arkona.xfm_inpmast
where inpmast_vin = '3GCUKREC0JG475177'









