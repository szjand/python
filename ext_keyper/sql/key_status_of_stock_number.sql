﻿
select * from keys.ext_keyper where stock_number = '31792R'



select 
  case
    when asset_status = 'In' then coalesce(cabinet, 'Unknown')
    when asset_status = 'Out' then 'Out to: ' || coalesce (first_name || ' ' || last_name, 'Unknown') 
  end as status
from keys.ext_keyper 
where stock_number = 'G34815'
order by transaction_date desc
limit 1


do
$$
declare
  _stock_number citext := 'G34815';
begin
  drop table if exists wtf;

  if 
    create temp table wtf as
    select count(*)
    from keys.ext_keyper 
    where stock_number = _stock_number
  then 
    '???'
  end if;
end
$$;

drop function keys.get_location(_stock_number citext)


create or replace function keys.get_key_location(_stock_number citext)
  returns text as
$BODY$
/*
select keys.get_key_location('G34464');
*/
begin
if not exists (
  select 1 
  from keys.ext_keyper
  where stock_number = _stock_number) 
then
  return (
    select '???');
else
  return (
    select 
      case
        when asset_status = 'In' then coalesce(cabinet, 'Unknown')
        when asset_status = 'Out' then 'Out to: ' || coalesce (first_name || ' ' || last_name, 'Unknown')
      end as status
    from keys.ext_keyper 
    where stock_number = _stock_number
    order by transaction_date desc
    limit 1);
end if;
end
$BODY$
language plpgsql;     


select keys.get_location('G33711B')

