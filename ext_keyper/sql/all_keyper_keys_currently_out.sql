﻿create table keys.keys_currently_checked_out (
  stock_number citext not null,
  transaction_date timestamp with time zone not null,
  out_from citext not null,
  out_to citext not null,
  issue_reason citext not null,
  days_out numeric(8,2) not null,
  primary key (stock_number));
comment on table keys.keys_currently_checked_out is 'truncated and repopulated daily (luigi) to generate a spreadsheet for ben of
all keys currently checked out of the keyper system. When this was started, we were looking at ~200 keys checked out at the
end of each day';  

create or replace function keys.get_keys_currently_checked_out()
returns void as
$BODY$
/*
  select keys.get_keys_currently_checked_out();
*/
truncate keys.keys_currently_checked_out;
insert into keys.keys_currently_checked_out
select upper(aa.stock_number) as stock_number, aa.transaction_date, aa.cabinet as out_from, aa.first_name || ' ' || aa.last_name out_to, aa.issue_reason,
  round((extract(epoch from now() - aa.transaction_date)/86400)::numeric, 2) as days_out
from keys.ext_keyper aa
join (
  select a.stock_number, max(a.transaction_date) as transaction_date
  from keys.ext_keyper a
  join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
    and b.status = 'I'
  group by a.stock_number) bb on aa.stock_number = bb.stock_number
  and aa.transaction_date = bb.transaction_date
where aa.asset_Status = 'out'    
order by aa.cabinet, aa.last_name;
$BODY$
LANGUAGE sql;


select * from keys.keys_currently_checked_out
