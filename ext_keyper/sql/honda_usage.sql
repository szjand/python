﻿drop table if exists honda;
create temp table honda as
select b.year_month, a.last_name, a.first_name, a.asset_status, a.issue_reason, count(*)
-- select a.*
from keys.ext_keyper a
join dds.dim_date b on a.transaction_date::date = b.the_Date
join sls.personnel c on a.last_name = c.last_name
  and c.store_code = 'ry2'
  and c.end_date > current_date
where a.cabinet in ('hnda-sales','hnda-svc')
  and b.year_month between 201801 and 201809
  and a.asset_Status = 'out'
group by b.year_month, a.last_name, a.first_name, a.issue_reason, a.asset_status, c.last_name
order by a.last_name, b.year_month;


select issue_reason, count(*) from honda group by issue_reason

select year_month, first_name || ' ' || last_name as name, 
  coalesce(sum(count) filter (where issue_reason = 'demo'), 0) as demo,
  coalesce(sum(count) filter (where issue_reason = 'move'), 0) as move,
  coalesce(sum(count) filter (where issue_reason = 'none'), 0) as none,
  coalesce(sum(count) filter (where issue_reason = 'fence-walk'), 0) as fence_walk
from honda
where year_month = 201805  
group by year_month, first_name || ' ' || last_name
union
select year_month, first_name || ' ' || last_name,
  coalesce(sum(count) filter (where issue_reason = 'demo'), 0) as demo,
  coalesce(sum(count) filter (where issue_reason = 'move'), 0) as move,
  coalesce(sum(count) filter (where issue_reason = 'none'), 0) as none,
  coalesce(sum(count) filter (where issue_reason = 'fence-walk'), 0) as fence_walk
from honda
where year_month = 201806 
group by year_month, first_name || ' ' || last_name
union
select year_month, first_name || ' ' || last_name as name, 
  coalesce(sum(count) filter (where issue_reason = 'demo'), 0) as demo,
  coalesce(sum(count) filter (where issue_reason = 'move'), 0) as move,
  coalesce(sum(count) filter (where issue_reason = 'none'), 0) as none,
  coalesce(sum(count) filter (where issue_reason = 'fence-walk'), 0) as fence_walk
from honda
where year_month = 201807  
group by year_month, first_name || ' ' || last_name
union
select year_month, first_name || ' ' || last_name as name, 
  coalesce(sum(count) filter (where issue_reason = 'demo'), 0) as demo,
  coalesce(sum(count) filter (where issue_reason = 'move'), 0) as move,
  coalesce(sum(count) filter (where issue_reason = 'none'), 0) as none,
  coalesce(sum(count) filter (where issue_reason = 'fence-walk'), 0) as fence_walk
from honda
where year_month = 201808 
group by year_month, first_name || ' ' || last_name
union
select year_month, first_name || ' ' || last_name as name, 
  coalesce(sum(count) filter (where issue_reason = 'demo'), 0) as demo,
  coalesce(sum(count) filter (where issue_reason = 'move'), 0) as move,
  coalesce(sum(count) filter (where issue_reason = 'none'), 0) as none,
  coalesce(sum(count) filter (where issue_reason = 'fence-walk'), 0) as fence_walk
from honda
where year_month = 201809  
group by year_month, first_name || ' ' || last_name
order by year_month, name



select *
from honda
where first_name = 'tyler'


select a.*
from keys.ext_keyper a
join dds.dim_date b on a.transaction_date::date = b.the_Date
join sls.personnel c on a.last_name = c.last_name
  and c.store_code = 'ry2'
  and c.end_date > current_date
  and c.first_name = 'tyler'
where a.cabinet in ('hnda-sales','hnda-svc')
  and b.year_month = 201809
  and a.asset_Status = 'out'