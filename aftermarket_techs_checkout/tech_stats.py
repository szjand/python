# encoding=utf-8
import db_cnx
import smtplib
from email.mime.text import MIMEText
import datetime

body = ''
# format date as Thursday 01/04/18
subject = "Aftermarket tech checkout data for " + (datetime.date.today() - datetime.timedelta(days=1)).strftime("%A %x")
with db_cnx.ads_dds() as dds_con:
    with dds_con.cursor() as dds_cur:
        sql = """
            SELECT -- clockhours yesterday
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              2, 'Tech Clock Hours', SUM(clockhours) AS clockhours
            FROM edwClockHoursFact a
            INNER JOIN (
              SELECT b.employeekey
              FROM dimtech a
              inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
                AND b.active = 'active'
                AND b.currentrow = true
              WHERE a.active = true
                AND a.flagdeptcode = 'mr'
                AND a.currentrow = true
                AND a.employeenumber <> 'NA'
                AND a.storecode = 'RY1'
                AND a.employeenumber <> '1117960') aa on a.employeekey = aa.employeekey
            INNER JOIN day bb on a.datekey = bb.datekey
              AND bb.thedate = curdate() - 1
            UNION
            SELECT -- flaghours yesterday
              (SELECT curdate() FROM system.iota),
              (SELECT curtime() FROM system.iota),
              CASE d.paymenttype
                WHEN 'customer pay' THEN 3
                WHEN 'internal' THEN 4
                WHEN 'warranty' THEN 5
                WHEN 'service contract' THEN 6
              END AS seq,
              trim (d.paymenttype) + ' Flag Hours', round(SUM(flaghours), 1)
            FROM factrepairorder a
            INNER JOIN day b on a.closedatekey = b.datekey
              AND b.datetype = 'DATE'
              AND b.thedate = curdate() -1
            INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
              AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
            INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
            GROUP by d.paymenttype
        """
        dds_cur.execute(sql)
        for row in dds_cur.fetchall():
            with db_cnx.ads_dds() as save_con:
                with save_con.cursor() as save_cur:
                    insert_str = ("""insert into andrew_checkout values ('""" + str(row[0]) + "','" +
                                  str(row[1]) + "'," + str(row[2]) + ",'" + str(row[3]) + "'," + str(row[4]) + ")")
                    save_cur.execute(insert_str)