﻿/*
Hey Jon!


I was hoping to get on the email list that you send out at 4pm and 5am, 
(which I see I am already getting now, so thank you) 
But hoping you could possibly send me a breakout of just our two 
Aftermarket Tech’s daily flagged hours.   Alex Waldbauer and Lucas Kiefer.
If you need any more info, please let me know!

Thank You!!

Dan


mimic the andrew_checkout email for just the 2 techs
mail it once a day

populate the base TABLE IN a function, FROM the python file just run the function

this will be a bit different than andrews checkout


date 	  	  tech 	   			 clock hours 	flag hours
12/07/2018 	  Alex Waldbauer	 	   8		   10
12/07/2018	  Lucas Kiefer			   7.6		   7

		  
*/
can do this in postgres, clockhours and repair orders and employees luigid nightly
SELECT *
FROM andrew_checkout
WHERE the_date = curdate()
  AND the_time > '04:00:00'
ORDER BY seq  


select b.name, a.*
from sls.xfm_fact_clock_hours a
join ads.ext_edw_employee_dim b on a.employee_number = b.employeenumber
  and b.currentrow
where a.the_date between '12/01/2018' and current_date - 1
  and a.clock_hours <> 0
  and a.employee_number in ('1146989','178050')
order by b.name, a.the_date


select *
from ads.ext_Dim_tech
where employeenumber in ('1146989','178050')


select b.employeenumber, c.the_date, sum(a.flaghours) as flag_hours
from ads.ext_fact_repair_order a
join ads.ext_dim_tech b on a.techkey = b.techkey
  and b.employeenumber in ('1146989','178050')
join dds.dim_date c on a.closedatekey = c.date_key
where c.the_date between '12/01/2018' and current_date - 1
group by b.employeenumber, c.the_date

  
select b.employeenumber, c.the_date, sum(a.flaghours) as flag_hours
from ads.ext_fact_repair_order a
join ads.ext_dim_tech b on a.techkey = b.techkey
  and b.employeenumber in ('1146989','178050')
join dds.dim_date c on a.closedatekey = c.date_key
where c.the_date = current_date - 1
group by b.employeenumber, c.the_date


select the_date, name,  clock_hours, flag_hours
from (
  select b.name, b.employeenumber, round(a.clock_hours, 2) as clock_hours
  from sls.xfm_fact_clock_hours a
  join ads.ext_edw_employee_dim b on a.employee_number = b.employeenumber
    and b.currentrow
  where a.the_date = current_date - 1
    and a.clock_hours <> 0
    and a.employee_number in ('1146989','178050')) d
left join (
  select b.employeenumber, c.the_date, sum(a.flaghours) as flag_hours
  from ads.ext_fact_repair_order a
  join ads.ext_dim_tech b on a.techkey = b.techkey
    and b.employeenumber in ('1146989','178050')
  join dds.dim_date c on a.closedatekey = c.date_key
  where c.the_date = current_date - 1
  group by b.employeenumber, c.the_date) e on d.employeenumber = e.employeenumber  

