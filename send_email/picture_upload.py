import adsdb
import smtplib
from email.mime.text import MIMEText
import datetime

body = ''
frmt = "%A %x"
# yesterdays date formatted Tuesday 07/14/15
the_date = ((datetime.date.today()) - datetime.timedelta(days=1)).strftime(frmt)
adsCon = adsdb.connect(DataSource='\\\\172.17.196.12:6363\\advantage\\dpsvseries\\dpsvseries.add',
                       userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
                       TrimTrailingSpaces='TRUE')
adsCursor = adsCon.cursor()
adsCursor.execute("""
    SELECT COUNT(*)
    FROM viipictureuploads a
    WHERE CAST(a.uploadts AS sql_date) = curdate() - 1
""")
numrows = adsCursor.fetchone()[0]
if numrows != 0:
    adsCursor.execute("""
        SELECT LEFT([uploaded by], 20) AS [upload by], [stock #],
          CAST([upload time] AS sql_time) AS [upload time], pics
        FROM (
          SELECT c.fullname AS [Uploaded By],
            b.stocknumber AS [Stock #], a.uploadts AS [Upload Time], COUNT(*) AS Pics
          FROM viipictureuploads a
          INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
          INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
          INNER JOIN viipictures f on b.VehicleInventoryItemID = f.VehicleInventoryItemID
          LEFT JOIN people c on a.uploadedby = c.partyid
          WHERE CAST(uploadts AS sql_date) = curdate() - 1
          GROUP BY CAST(uploadts AS sql_date), e.dayname, c.fullname,
            b.stocknumber, a.uploadts) x
        ORDER BY CAST([upload time] AS sql_time)
    """)
    result = adsCursor.fetchall()
    # http://stackoverflow.com/questions/8366276/writing-a-list-of-tuples-to-a-text-file-in-python
    body = 'Pictures for ' + str(numrows) + ' vehicles uploaded on ' + the_date + '\n'
    for t in result:
        body += '   '.join(str(s) for s in t) + '\n'

# Define email addresses to use
# this is just header info, as a string
# addr_to = 'jandrews@cartiva.com, jandrews@rydellcars.com'
addr_to = ('jandrews@cartiva.com, bcahalan@rydellcars.com, acrane@rydellcars.com, '
           'jgardner@rydellcars.com, sbergh@rydellcars.com')
addr_from = 'jandrews@cartiva.com'
# this is the actual list of email recipients
# receivers = ['jandrews@cartiva.com', 'jandrews@rydellcars.com']
receivers = (['jandrews@cartiva.com', 'bcahalan@rydellcars.com', 'acrane@rydellcars.com',
              'jgardner@rydellcars.com', 'sbergh@rydellcars.com'])

# Declare SMTP email server details
smtp_server = 'mail.cartiva.com'

# Construct email
if numrows == 0:
    message = MIMEText('No pictures uploaded on ' + the_date)
else:
    message = MIMEText(body)
message['To'] = addr_to
message['From'] = addr_from
if numrows == 0:
    message['Subject'] = 'No pictures uploaded on ' + the_date
else:
    message['Subject'] = 'Pictures uploaded on ' + the_date
# Send email via SMTP server
e = smtplib.SMTP(smtp_server)
try:
    e.sendmail(addr_from, receivers, message.as_string())
    print "Successfully sent email"
except smtplib.SMTPException:
    print "Error: unable to send email"
e.quit()
