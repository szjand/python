# encoding=utf-8
"""

"""

3/31/19 THESE WERE ALL MOVED TO LUIGI

import db_cnx
import ftplib

pg_con = None
try:
    server = 'grab.cartiva.com'
    username = 'jandrews'
    password = 'Andrews1954'
    file_name = 'WBLInvent.CSV'
    # directory to which the file will be downloaded
    ftp_target = 'stpautos/wbl_inventory/'
    ftp = ftplib.FTP(server)
    ftp.login(username, password)
    ftp.cwd('stpautos/')
    with open(ftp_target + file_name, 'wb') as fhandle:
        ftp.retrbinary('RETR ' + file_name, fhandle.write)
    ftp.quit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ftp_inv.ext_stpautos_wbl")
            with open(ftp_target + file_name, 'r') as io:
                pg_cur.copy_expert("""copy ftp_inv.ext_stpautos_wbl from stdin 
                                      with csv HEADER encoding 'latin-1'""", io)
            sql = """
                insert into ftp_inv.stpautos_wbl
                select current_date, a.*
                from ftp_inv.ext_stpautos_wbl a;
            """
            pg_cur.execute(sql)
    print 'passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()
