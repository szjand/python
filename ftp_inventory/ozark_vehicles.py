# encoding=utf-8
"""


1/3/19
    2 days in a row i perplexed by the vehicle file not being availing in the morning
    revisited the ftp site and realized that file comes @ 9PM
    this need to run either at the end of the day, or after midnight for the previous day
    i need to clean up my act about when this runs and what date it specifies
    leaning toward making this part of the main luigi task which runs at 2:30 am
    which means the date specified in this script needs to be current date - 1
3/30/19
    automate sends us files with the date encoded, eg,  20190328_TRC65.zip
    so i have been making a separate download directory for each day
    don't need that history,
    want to deploy to luigi, so refactor to download the file and delete it daily
    puting the contents of _batch_vehc.txt only in db, don't have column headers for the other files
    3/29, seems to work, just need to figure out directory structure on luigi server
"""

3/31/19 THESE WERE ALL MOVED TO LUIGI

import db_cnx
import ftplib
import zipfile
from datetime import datetime, timedelta
import os
import glob

pg_con = None
try:
    server = 'grab.cartiva.com'
    username = 'jandrews'
    password = 'Andrews1954'
    date_string = str((datetime.now() - timedelta(1)).strftime('%Y%m%d'))
    # file_match = date_string + '*'
    file_match = date_string + '*.zip'
    # directory to which the file will be downloaded
    # ftp_target = 'ozark/files/' + date_string + '/'
    ftp_target = 'ozark/files/automate/'
    zip_file_name = ftp_target + date_string + '_TRC65.zip'
    vehicles_file_name = ftp_target + date_string + '_TRC65_batch_vehc.txt'
    # os.mkdir(ftp_target)
    ftp = ftplib.FTP(server)
    ftp.login(username, password)
    ftp.cwd('automate/')
    for filename in ftp.nlst(file_match):
        fhandle = open(ftp_target + filename, 'wb')
        ftp.retrbinary('RETR ' + filename, fhandle.write)
        fhandle.close()
    ftp.quit()
    zipfile.ZipFile(zip_file_name).extractall(ftp_target)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ftp_inv.ext_ozark_vehicles")
            with open(vehicles_file_name, 'r') as io:
                pg_cur.copy_expert("""copy ftp_inv.ext_ozark_vehicles from stdin
                                      with csv HEADER DELIMITER as '|' encoding 'latin-1'""", io)
            sql = """
                insert into ftp_inv.ozark_vehicles
                select current_date, a.*
                from ftp_inv.ext_ozark_vehicles a;
            """
            pg_cur.execute(sql)
    # print 'passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
    files = glob.glob(ftp_target + '*')
    for f in files:
        os.remove(f)
except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()
