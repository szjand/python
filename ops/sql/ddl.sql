﻿CREATE TABLE ops.arkona_production_vs_report_tables
(
  table_name citext NOT NULL,
  date_format citext NOT NULL,
  date_field citext,
  CONSTRAINT arkona_production_vs_report_tables_pkey PRIMARY KEY (table_name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ops.arkona_production_vs_report_tables
  OWNER TO rydell;

insert into ops.arkona_production_vs_report_Tables values
('rydedata.glptrns','the_date','gtdate'),
('rydedata.pdppdet','the_date_decimal','ptdate'),
('rydedata.pdpphdr','the_date_decimal','ptdate'),
('rydedata.pypclockin','the_date','yiclkind'),
('rydedata.sdprdet','the_date_decimal','ptdate'),
('rydedata.sdprhdr','the_date_decimal','ptdate');

CREATE TABLE ops.arkona_production_vs_report
(
  the_date date NOT NULL,
  ts timestamp with time zone NOT NULL DEFAULT now(),
  table_name citext NOT NULL,
  production_count bigint NOT NULL DEFAULT 0,
  report_1_count bigint NOT NULL DEFAULT 0,
  report_2_count bigint not null default 0,
  CONSTRAINT arkona_production_vs_report_pk PRIMARY KEY (the_date, table_name),
  CONSTRAINT arkona_production_vs_report_table_name_fkey FOREIGN KEY (table_name)
      REFERENCES ops.arkona_production_vs_report_tables (table_name) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ops.arkona_production_vs_report
  OWNER TO rydell;
