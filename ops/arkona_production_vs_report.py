import pypyodbc
# import psycopg2
import datetime
import db_cnx
# to be run at ~ 4:00 AM daily, record counts for tables pdpphdr, pdppdet, sdprhdr, sdprdet,
# glptrns, pypclockin production server vs report2 for the previous day
# pgCon = psycopg2.connect("host='10.130.196.88' dbname='cartiva' user='rydell' password='cartiva'")
pgCon = db_cnx.ops()
pgCur = pgCon.cursor()
tableCur = pgCon.cursor()
# yesterday's count
# the_date = str(datetime.date.today() - datetime.timedelta(days=1))
# today's count
the_date = str(datetime.date.today())
# the_date = '2018-02-01'
the_date_decimal = str(the_date).replace('-', '')
prodCon = db_cnx.arkona_production()
prodCur = prodCon.cursor()
# reportCon = db_cnx.arkona_report()
reportCon = db_cnx.dealertrack_report1()
reportCur = reportCon.cursor()

table_sql = "select * from jon.arkona_production_vs_report_tables"
tableCur.execute(table_sql)
for record in tableCur:
    sql = "select '" + record[0] + "', count(*) from " + record[0] + " where " + record[2] + " = "
    # http://stackoverflow.com/questions/23578230/python-ternary-conditional-for-joining-string
    sql += the_date_decimal if record[1] == 'the_date_decimal' else "'" + the_date + "'"
    # print sql
    reportCur.execute(sql)
    reportRow = reportCur.fetchone()
    prodCur.execute(sql)
    prodRow = prodCur.fetchone()
    sql = """
        insert into jon.arkona_production_vs_report(the_Date, table_name, report_count)
        values('%s','%s',%s)
    """ % (the_date, reportRow[0], reportRow[1])
    pgCur.execute(sql)
    sql = """
        update jon.arkona_production_vs_Report
        set production_count = %s
        where the_date = '%s'
        and table_name = '%s'
    """ % (prodRow[1], the_date, reportRow[0])
    pgCur.execute(sql)
    pgCon.commit()
tableCur.close()
pgCur.close()
pgCon.close()
prodCur.close()
prodCon.close()
reportCur.close()
reportCon.close()
