# encoding=utf-8
"""
 to be run at ~ 4:00 AM daily, record counts for tables pdpphdr, pdppdet, sdprhdr, sdprdet,
 glptrns, pypclockin production server vs REPORT1.DMS.DEALERTRACK.COM vs REPORT2.DMS.DEALERTRACK.COM
 for the previous day for the previous day
 refactored on 2/2/18: report2 was down, but report1 was not, so rather than redirect everything to the
 production server, perhaps one of the report servers will be functioning
"""
import datetime
import db_cnx
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os
import ops

prod_con = None
rep_1_con = None
rep_2_con = None
pg_1_con = None
pg_2_con = None
pg_server = '173'

# yesterday's count
the_date = str(datetime.date.today() - datetime.timedelta(days=1))
# today's count
# the_date = str(datetime.date.today())
the_date_decimal = str(the_date).replace('-', '')

try:
    with db_cnx.new_pg(pg_server) as pg_1_con:
        with pg_1_con.cursor() as pg_1_cur:
            sql = "select * from ops.arkona_production_vs_report_tables"
            pg_1_cur.execute(sql)
            for record in pg_1_cur:
                sql = "select '" + record[0] + "', count(*) from " + record[0] + " where " + record[2] + " = "
                # http://stackoverflow.com/questions/23578230/python-ternary-conditional-for-joining-string
                sql += the_date_decimal if record[1] == 'the_date_decimal' else "'" + the_date + "'"
                with db_cnx.arkona_production() as prod_con:
                    with prod_con.cursor() as prod_cur:
                        prod_cur.execute(sql)
                        prod_row = prod_cur.fetchone()
                with db_cnx.dealertrack_report1() as rep_1_con:
                    with rep_1_con.cursor() as rep_1_cur:
                        rep_1_cur.execute(sql)
                        rep_1_row = rep_1_cur.fetchone()
                with db_cnx.dealertrack_report2() as rep_2_con:
                    with rep_2_con.cursor() as rep_2_cur:
                        rep_2_cur.execute(sql)
                        rep_2_row = rep_2_cur.fetchone()
                with db_cnx.new_pg(pg_server) as pg_2_con:
                    with pg_2_con.cursor() as pg_2_cur:
                        sql = """
                            insert into ops.arkona_production_vs_report(the_date, table_name, production_count)
                            values('%s','%s',%s)
                        """ % (the_date, prod_row[0], prod_row[1])
                        pg_2_cur.execute(sql)
                        sql = """
                            update ops.arkona_production_vs_Report
                            set report_1_count = %s
                            where the_date = '%s'
                            and table_name = '%s'
                        """ % (prod_row[1], the_date, rep_1_row[0])
                        pg_2_cur.execute(sql)
                        sql = """
                            update ops.arkona_production_vs_Report
                            set report_2_count = %s
                            where the_date = '%s'
                            and table_name = '%s'
                        """ % (prod_row[1], the_date, rep_2_row[0])
                        pg_2_cur.execute(sql)

except Exception as error:
    print(error)
    # ops.email_error(task, '', error)
# try:
#     COMMASPACE = ', '
#     sender = 'jandrews@cartiva.com'
#     recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
#     # recipients = ['test@cartiva.com', 'jandrews@cartiva.com', 'bcahalan@rydellcars.com']
#     outer = MIMEMultipart()
#     outer['Subject'] = 'Sales Scorecard'
#     outer['To'] = COMMASPACE.join(recipients)
#     outer['From'] = sender
#     outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
#     attachments = [spreadsheet]
#     for file in attachments:
#         try:
#             with open(file, 'rb') as fp:
#                 msg = MIMEBase('application', "octet-stream")
#                 msg.set_payload(fp.read())
#             encoders.encode_base64(msg)
#             msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(file))
#             outer.attach(msg)
#         except Exception:
#             raise
#     composed = outer.as_string()
#     # this works with Python2
#     e = smtplib.SMTP('mail.cartiva.com')
#     try:
#         e.sendmail(sender, recipients, composed)
#     except smtplib.SMTPException:
#         print("Error: unable to send email")
#     e.quit()
# except Exception as error:
#     print(error)