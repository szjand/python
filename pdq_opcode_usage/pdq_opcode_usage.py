# coding=utf-8
import db_cnx
import csv
import ops
import openpyxl as xl
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os

ads_con = None


#  pdq_opcode_usage_spreadsheet
task = 'pdq_opcode_usage_spreadsheet'
file_name = 'files/pdq_opcode_usage.csv'
try:
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                FROM (
                SELECT '0pcode' as opcode FROM system.iota) z
                LEFT JOIN (
                SELECT mmmdd FROM day WHERE thedate = curdate() - 7) b on 1 = 1
                LEFT JOIN (
                SELECT mmmdd FROM day WHERE thedate = curdate() - 6) c on 1 = 1
                LEFT JOIN (
                SELECT mmmdd FROM day WHERE thedate = curdate() - 5) d on 1 = 1
                LEFT JOIN (
                SELECT mmmdd FROM day WHERE thedate = curdate() - 4) e on 1 = 1
                LEFT JOIN (
                SELECT mmmdd FROM day WHERE thedate = curdate() - 3) f on 1 = 1
                LEFT JOIN (
                SELECT mmmdd FROM day WHERE thedate = curdate() - 2) g on 1 = 1
                LEFT JOIN (
                SELECT mmmdd FROM day WHERE thedate = curdate() - 1) h on 1 = 1
                LEFT JOIN (
                  SELECT 'MTD' FROM system.iota) i on 1 = 1
                UNION
                SELECT y.*, z.mtd
                FROM (
                  SELECT a.opcode, "g","f","e","d","c","b","a"
                  FROM dimopcode a
                  LEFT JOIN (
                    SELECT c.opcode,
                      cast(SUM(CASE WHEN thedate = curdate() - 7 THEN 1 ELSE 0 END) AS sql_char) AS "g",
                      cast(SUM(CASE WHEN thedate = curdate() - 6 THEN 1 ELSE 0 END) AS sql_char) AS "f",
                      cast(SUM(CASE WHEN thedate = curdate() - 5 THEN 1 ELSE 0 END) AS sql_char) AS "e",
                      cast(SUM(CASE WHEN thedate = curdate() - 4 THEN 1 ELSE 0 END) AS sql_char) AS "d",
                      cast(SUM(CASE WHEN thedate = curdate() - 3 THEN 1 ELSE 0 END) AS sql_char) AS "c",
                      cast(SUM(CASE WHEN thedate = curdate() - 2 THEN 1 ELSE 0 END) AS sql_char) AS "b",
                      cast(SUM(CASE WHEN thedate = curdate() - 1 THEN 1 ELSE 0 END) AS sql_char) AS "a"
                    FROM factrepairorder a
                    INNER JOIN day b on a.closedatekey = b.datekey
                      AND b.thedate BETWEEN curdate() - 7 AND curdate() - 1
                    INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
                      AND pdqcat1 <> 'N/A'
                    INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey
                    INNER JOIN dimservicetype e on a.servicetypekey = e.servicetypekey
                    WHERE a.storecode = 'ry1'
                      AND e.servicetypecode IN ('QS','QL')
                    GROUP BY c.opcode) x on a.opcode = x.opcode
                  WHERE a.pdqcat1 <> 'N/A') y
                LEFT JOIN (
                  SELECT c.opcode, cast(COUNT(*) AS sql_char) AS mtd
                  FROM factrepairorder a
                  INNER JOIN day b on a.closedatekey = b.datekey
                    AND b.MonthOfYear = month(curdate())
                    AND b.TheYear = year(curdate())
                  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
                    AND pdqcat1 <> 'N/A'
                  INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey
                  INNER JOIN dimservicetype e on a.servicetypekey = e.servicetypekey
                  WHERE a.storecode = 'ry1'
                    AND e.servicetypecode IN ('QS','QL')
                  GROUP BY c.opcode) z on y.opcode = z.opcode
                ORDER BY opcode;
            """
            ads_cur.execute(sql)
            with open(file_name, 'w') as f:
                csv.writer(f).writerows(ads_cur)
    wb = xl.Workbook()
    ws = wb.active
    with open(file_name) as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            ws.append(row)
        f.close()
    wb.save('files/pdq_opcode_usage.xlsx')
except Exception, error:
    print error
    ops.email_error(task, '', error)

#  email
task = 'email'
try:
    COMMASPACE = ', '
    sender = 'jandrews@cartiva.com'
    # recipients = ['test@cartiva.com', 'jandrews@cartiva.com']
    recipients = ['jonfandrews@gmail.com', 'jandrews@cartiva.com','nneumann@rydellcars.com']
    outer = MIMEMultipart()
    outer['Subject'] = 'PDQ Opcode Usage'
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    # outer.attach(MIMEText(body))
    attachments = ['files/pdq_opcode_usage.xlsx']
    for file in attachments:
        try:
            with open(file, 'rb') as fp:
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read())
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(file))
            outer.attach(msg)
        except:
            # print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
            raise
    composed = outer.as_string()
    # this works with Python2
    e = smtplib.SMTP('mail.cartiva.com')
    try:
        e.sendmail(sender, recipients, composed)
        # print "Successfully sent email"
    except smtplib.SMTPException:
        print "Error: unable to send email"
    e.quit()
except Exception, error:
    print error
    ops.email_error(task, '', error)
