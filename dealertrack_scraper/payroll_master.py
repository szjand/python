# encoding=utf-8
"""
"""

from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
import requests
from bs4 import BeautifulSoup as bs
import csv


# pdf_dir = '/mnt/hgfs/E/python_projects/carfax/pdf_dir/'
html_dir = '/home/jon/carfax/'
user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None

def setup_driver():
    chromedriver_path = '/usr/bin/chromedriver'
    options = webdriver.ChromeOptions()
    profile = {}
    options.add_experimental_option("prefs", profile)
    # options.add_argument("--headless")
    options.add_argument("--window-size=1920x1080")
    driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
    return driver


def get_secret(category, platform):
    modified_platform = platform.replace(' ', '+')
    url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
           (modified_platform, category))
    response = requests.get(url, auth=(user, passcode))
    return response.json()


def login(driver):
    # credentials = get_secret('Cartiva', 'Dealertrack')
    # resp_dict = credentials
    # # enter username
    # driver.find_element_by_id('username').send_keys(resp_dict.get('username'))
    # # click the next button
    # driver.find_element_by_xpath('//*[@id="signIn"]').click()
    # # enter password
    # driver.find_element_by_id('password').send_keys(resp_dict.get('secret'))
    # # click the sign in button
    # driver.find_element_by_xpath('//*[@id="signIn"]').click()

    # try my credentials
    username = 'jandr561'
    password = 'fuckyou44'
    # enter username
    driver.find_element_by_id('username').send_keys(username)
    # click the next button
    driver.find_element_by_xpath('//*[@id="signIn"]').click()
    # enter password
    driver.find_element_by_id('password').send_keys(password)
    # click the sign in button
    driver.find_element_by_xpath('//*[@id="signIn"]').click()

    WebDriverWait(driver, 20).until(ec.presence_of_element_located(('xpath', '//*[@id="$ACODE"]/input')))

    # click Payroll
    driver.find_element_by_xpath('//*[@id="appButton6"]')
    # click 40. Employees
    driver.find_element_by_xpath('//*[@id="Text3"]/ol/li')
    # click the employee number cell
    driver.find_element_by_xpath('//*[@id="YMEMPN.1"]')
    # click the OK button
    driver.find_element_by_xpath('//*[@id="btnSubmit"]').click()

def main():
    driver = setup_driver()
    try:
        driver.get('https://login.dealertrack.com/public/login.fcc?TYPE=33554432&REALMOID=06-2ed3eb37-fc1d-'
                   '47ed-b8f9-e42520e39658&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=-SM-'
                   '9ke%2fKrunP4nZuxCd6xRlzGvdmJpaaJVjfp86JLZk4zZW1%2fTPlsk0Hyjy0w7MB88V&TARGET=-SM-'
                   'HTTP%3a%2f%2flogin1%2edealertrack%2ecom%3a6100%2fDMS_SSO%2fDMS_SSO_'
                   'Launch%2easpx%3fdealerid%3dx')
        login(driver)
    finally:
        driver.quit()


if __name__ == '__main__':
    main()